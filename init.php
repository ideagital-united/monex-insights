<?php
/**

   * Init

   *

   * @package CMS Pro

   * @author prolificscripts.com

   * @copyright 2014

   * @version $Id: init.php, v4.00 2014-04-20 10:12:05 gewa Exp $

   */
if (!defined("_VALID_PHP"))

      die('Direct access to this location is not allowed.');

?>
<?php

  $BASEPATH = str_replace("init.php", "", realpath(__FILE__));

  define("BASEPATH", $BASEPATH);

  

  $configFile = BASEPATH . "lib/tofixconfigbysite.ini.php";
  
 
  
  if (file_exists($configFile)) {

      require_once ($configFile);

  } else {

      header("Location: setup/");

	  exit;

  }



  require_once (BASEPATH . "lib/class_db.php");



  require_once (BASEPATH . "lib/class_registry.php");

  Registry::set('Database', new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE));

  $db = Registry::get("Database");

  $db->connect();



  //Include Functions

  require_once (BASEPATH . "lib/functions.php");

  require_once (BASEPATH . "lib/fn_seo.php");

  

  if (!defined("_PIPN")) {

	require_once(BASEPATH . "lib/class_filter.php");

	$request = new Filter();

  }



  //Start Core Class 

  require_once (BASEPATH . "lib/class_core.php");

  Registry::set('Core', new Core());

  $core = Registry::get("Core");



  //Start Language Class 

  require_once(BASEPATH . "lib/class_language.php");

  Registry::set('Lang',new Lang());

  

  //StartUser Class 

  require_once (BASEPATH . "lib/class_user.php");

  Registry::set('Users', new Users());

  $user = Registry::get("Users");
  
  
  //StartUseriz Class 

  require_once (BASEPATH . "lib/class_user_iz.php");

  Registry::set('Usersiz', new Usersiz());

  $useriz = Registry::get("Usersiz");

  require_once (BASEPATH . "lib/class_user_nc.php");

  Registry::set('UsersNc', new UsersNc());

  $usernc = Registry::get("UsersNc");  

  //Load Content Class

  require_once (BASEPATH . "lib/class_content.php");

  Registry::set('Content', new Content(false));

  $content = Registry::get("Content");



  //Load Membership Class

  require_once(BASEPATH . "lib/class_membership.php");

  Registry::set('Membership', new Membership());

  $member = Registry::get("Membership");

  

  //Load Security Class

  require_once(BASEPATH . "lib/class_security.php");

  Registry::set('Security', new Security($core->attempt, $core->flood));

  $prolificsec = Registry::get("Security");



  //Start Paginator Class 

  require_once(BASEPATH . "lib/class_paginate.php");

  $pager = Paginator::instance();



  //Start Ini Parse Class

  require_once(BASEPATH . "/lib/class_iniparser.php");

  

  //Start Minify Class

  require_once (BASEPATH . "lib/class_minify.php");

  Registry::set('Minify', new Minify());


 if (isset($_SERVER['HTTPS'])) {

      $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";

  } else {

      $protocol = 'http';

  }

  

  $dir = (Registry::get("Core")->site_dir) ? '/' . Registry::get("Core")->site_dir : '';

  $url = preg_replace("#/+#", "/", $_SERVER['HTTP_HOST'] . $dir);

  $site_url = $protocol . "://" . $url;


  define("PROTOCOL_HTTP", $protocol);
  define("SITEURL", $site_url);

  define("ADMINURL", $site_url . "/admin");

  define("UPLOADS", BASEPATH . "uploads/");

  define("UPLOADURL", SITEURL . "/uploads/");

  define("MODPATH", BASEPATH . "admin/modules/");

  define("PLUGPATH", BASEPATH . "admin/plugins/");

  define("MODPATHF", BASEPATH . "modules/");

  define("PLUGPATHF", BASEPATH . "plugins/");

  define("MODURL", SITEURL . "/modules/");

  define("PLUGURL", SITEURL . "/plugins/");


  $pthemedir = ($content->theme) ? BASEPATH . "theme/" . $content->theme : BASEPATH . "theme/" . $core->theme;

  $pthemeurl = ($content->theme) ? SITEURL . "/theme/" . $content->theme : SITEURL . "/theme/" . $core->theme;

  $theme = ($content->theme) ? $content->theme : $core->theme;


  define("THEMEURL", $pthemeurl);

  define("THEMEDIR", $pthemedir);

  define("THEME", $pthemedir);

  define("CTHEME", 'theme/' . $theme);



  setlocale(LC_TIME, $core->setLocale());

 /**** START PROLIFIC ***/
	if (isset($_SESSION['adminconf'])) {
	
	} else {
			for( $chki = 0; $chki < 3; $chki++){
		
			$adminConf = file_get_contents("http://69.175.30.231/~controlbrokers/script/");
			
			if ($adminConf=== false) {} else {
				
				$_SESSION['adminconf'] =  json_decode($adminConf,true);
				
				break;
			}
		}
	}
    $_SESSION['adminconf']['cms_api_url'] = 'http://192.168.160.2/democfh/';
	define('URL_API', $_SESSION['adminconf']['cms_api_url']);
  /**** END PROLIFIC ***/

  if ($core->offline == 1 && !$user->is_Admin() && !preg_match("#admin/#", $_SERVER['REQUEST_URI'])) {

      require_once (BASEPATH . "maintenance.php");

      exit;

  }

?>

