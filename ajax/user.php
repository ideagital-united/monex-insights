<?php

  /**

   * User

   *

   * @package CMS Pro

   * @author prolificscripts.com

   * @copyright 2014

   * @version $Id: user.php, v4.00 2014-04-20 10:12:05 gewa Exp $

   */

  define("_VALID_PHP", true);

  require_once("../init.php");

?>

<?php

  if (isset($_POST['resendactivate'])):
      $user->activate_resend_mail();
  endif;
  if (isset($_POST['continueactive'])):
	  $user->activate_continue_next();
  endif;
  /* == Registration == */

  if (isset($_POST['doRegister'])):

      $user->register();

  endif;
if (isset($_POST['doCheckEmail'])) {
		 $email = $_POST['email'];
		 if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$row = Registry::get("Database")->first("select id from users where email='".$email."' and active='y'");
			if ($row) {
				$json 	= array(   'status'=>'error','msg'=>'The email you entered is already registered. Click here to resume your application or login to your account.');
			} else {
				$json 	= array(   'status'=>'success');
			}
		  } else {
			 $json 	= array(   'status'=>'error','msg'=>'Please enter a valid email address.');
		  }
		print json_encode($json); exit;
    } 
  if (isset($_POST['doGetRefName'])) {
  	    require_once (MODPATH . "tradersystem/admin_class.php");
   
        Registry::set('TraderSystem', new TraderSystem());
		
        if ($_POST['refname'] != '') { 
           $aPOSTAPI['refname'] = $_POST['refname'];
           $aAcc['uid'] 		= 0;               
           $aAcc 				= Registry::get("TraderSystem") ->call_curl_get("check_ref_uid","GET",$_POST['refname']."?cmd=check_ref_uid");

    		if ($aAcc['uid'] > 0) {
    			$json 	= array(   'status'=>'success','name'=>$aAcc['uid'],'msg'=>'Available referrer name');
    		} else {
    			$json 	= array(   'status'=>'error','name'=>0, 'msg'=>'Not found referrer name.');	
    		}
        } else {
            $json 		= array(   'status'=>'error', 'name'=>0,'msg'=>'Please input referrer name.');                   
       }  
		print json_encode($json); exit;
    } 

  /* == Password Reset == */

  if (isset($_POST['passReset'])):

      $user->passReset();

  endif;

   if (isset($_POST['doProfile_change_save_password'])) :

	  

      $user->updateProfile_save_password();

  endif;



  /* == Account Acctivation == */

  if (isset($_POST['accActivate'])):

      $user->activateUser();

  endif;

?>
