<?php
  /**
   * Login Module
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<!-- Start Login Module -->
<?php if($user->logged_in):?>
<strong><?php echo Lang::$word->_WELCOME;?>:</strong>&nbsp; <a href="<?php echo doUrl(false, $core->account_page, "page");?>"><?php echo $user->name;?></a> &bull; <a href="<?php echo SITEURL;?>/logout.php"><?php echo Lang::$word->_N_LOGOUT;?></a>
<?php else:?>
<form action="<?php echo doUrl(false, $core->login_page, "page");?>" method="post" class="prolific small form" name="login_form_mod">
  <div class="field">
    <input placeholder="<?php echo Lang::$word->_USERNAME;?>" name="username" type="text" maxlength="20">
  </div>
  <div class="field">
    <input placeholder="<?php echo Lang::$word->_PASSWORD;?>" name="password" type="password" maxlength="20">
  </div>
  <div class="field">
    <button name="submit" type="submit" class="prolific button"><?php echo Lang::$word->_UA_LOGINNOW;?></button>
  </div>
  <input name="doLogin" type="hidden" value="1">
</form>
<?php endif;?>
<!-- End Login Module /-->