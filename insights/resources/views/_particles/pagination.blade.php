@if ($paginator->lastPage() > 1)
<div class="col-md-12 text-left">
    <a href="{{ $paginator->url(1) }}" class="{{ ($paginator->currentPage() == 1) ? ' button-o' : 'button' }} button-sm button-circle button-gray">Prev</a>

    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <a href="{{ $paginator->url($i) }}" class="button button-sm button-circle {{ ($paginator->currentPage() == $i) ? ' button-pasific' : 'button-gray' }}">{{ $i }}</a>
    @endfor

    <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? 'button-o' : 'button' }} button-sm button-circle button-gray">Next</a>
</div>
@endif
