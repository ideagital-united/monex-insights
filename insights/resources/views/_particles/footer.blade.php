<div id="botwidget">
	<div class="prolific-grid">

		<section class="botwidget clearfix">
			<div class="columns">

				<div class="screen-100 phone-100">
					<div class="botwidget-wrap add menu footer">

						<div class="widget-body">
							<div id="footer" class="footer-two pt50 bg-dark">
								<div class="container-fluid">
									<div class="container">
										<div class="row">
											<div class="clearfix bb-solid-1 pb35">
												<!-- footer about start -->
												<div class="col-md-6 col-xs-12">
													<div class="footer-brand">
														<img src="https://www.monexsecurities.com.au/theme/freedomcfd/assets/media/logo/logo-monex-w.png" alt="logo" width="150px">
													</div>
													<button class="button-o button-green button-circle">
														<a href="https://www.monexsecurities.com.au/page/before-register/" target="_blank">
															<h7 class="color-green">OPEN AN ACCOUNT</h7>
														</a>
													</button>
												</div>
												<!-- footer about end -->
												<!-- footer about start -->
												<div class="col-md-3 col-xs-12">
													<a href="https://www.monexsecurities.com.au/page/before-register/" target="_blank"></a>
													<ul class="list-caret">
														<li>
															<a href="https://www.monexsecurities.com.au/page/why-monex/" target="_blank">Why Monex</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/page/fee/" target="_blank">Fees</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/page/support/" target="_blank">Support</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/page/faq/" target="_blank">FAQ</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/page/about-monex/">About Us</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/page/our-contact-info/">Contact Us</a>
														</li>
													</ul>
												</div>
												<!-- footer about end -->
												<!-- footer about start -->
												<div class="col-md-3 col-xs-12">
													<ul class="list-caret">
														<li>
															<a href="https://www.monexsecurities.com.au/uploads/Terms-and-Conditions.pdf" target="_blank">Terms and Conditions</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/uploads/Financial-Services-Guide.pdf" target="_blank">Financial Services Guide</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/uploads/Fees-and-Charges.pdf" target="_blank">Fees and Charges Schedules</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/uploads/Equities-Brochure.pdf" target="_blank">Equities Brochure</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/page/privacy/" target="_blank">Privacy Policy</a>
														</li>
														<li>
															<a href="https://www.monexsecurities.com.au/page/complaints/" target="_blank">Compliant Handling Policy</a>
														</li>
													</ul>
												</div>
												<!-- footer about end -->
											</div>
										</div>
										<!-- row end -->
									</div>
									<!-- container end -->
								</div>
								<!-- container-fluid end -->
								<div class="container-fluid pt20">
									<div class="container">
										<div class="row">
											<!-- copyright start -->
											<div class="col-md-6 col-sm-6 col-xs-6 pull-left">
												<p>
													© Monex Securities Australia Pty Ltd 2017
												</p>
											</div>
											<!-- copyright end -->
											<!-- footer bottom start -->
											<div class="col-md-6 col-sm-6 col-xs-6 pull-right">
												<p class="text-right">
													<a href="https://www.facebook.com/MonexAustralia" target="_blank " class="mr20 ">
														<i class="fa fa-facebook
                                                            " aria-hidden="true "></i>
													</a>
													<a href="https://twitter.com/MonexAustralia" target="_blank" class="mr20">
														<i class="fa fa-twitter" aria-hidden="true"></i>
													</a>
													<a href="https://www.linkedin.com/company/18164522/" target="_blank
                                                            " class="mr50 ">
														<i class="fa fa-linkedin " aria-hidden="true "></i>
													</a>
												</p>
											</div>
											<!-- footer bottom end -->
											<div class="col-sm-12 ">
												<p>
													This Internet website (
													<a href="http://monexsecurities.com.au/ ">monexsecurities.com.au</a>) and information and services referred to in the ‘website’ are directed to and available
													for Australian residents. Our website is issued and made available in Australia through Monex Securities Australia
													Pty Ltd ABN 84 142 210 179 and AFSL No. 363972 (‘Monex AU’, “us’ ‘we’ ‘our’). &nbsp;
												</p>
												<p>
													This website contains material in relation to the Financial Products and services available in Australia offered by Monex
													AU. By accessing this website you agree to be bound by our Terms and Conditions. These terms and conditions
													are subject to change without prior notice. It is your responsibility to review for any amendments of our terms
													and conditions that may occur from time to time published on our website. Do not access this website if you
													do not agree with these terms and conditions set out herein.
												</p>
												<p>
													The information, content and material on this website is for information purposes only. It is believed to be reliable at
													the time of publication however Monex AU does not warrant its completeness, timeliness or accuracy. The information
													and materials contained in this website are subject to change without notice.
												</p>
												<p>
													This website is given for general information only. As general information, no consideration or evaluation is given of the
													investment objectives or financial situation of any particular person. Trading and investing involves substantial
													financial risk. All customers of Monex AU should make their own evaluation of the merits and suitability of
													any financial products and/or advice or seek specific personal advice as to the appropriateness of engaging
													in any activity referred to in this website in light of their own particular financial circumstances and objectives.
												</p>
												<p>
													Please&nbsp;
													<a href="https://www.monexsecurities.com.au/page/disclaimer-details/ " target="_blank ">click here</a> to read our full disclaimer.
												</p>
											</div>
											<!-- row end -->
										</div>
										<!-- container end -->
									</div>
									<!-- container-fluid end -->
								</div>
								<div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>