<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title> {{getcong('site_name')}} Admin</title>

	<link href="{{ URL::asset('upload/'.getcong('site_favicon')) }}" rel="shortcut icon" type="image/x-icon" />
	<link rel="stylesheet" href="{{ URL::asset('admin_assets/css/style.css') }}">
	<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

	<script src="{{ URL::asset('admin_assets/js/jquery.js') }}"></script>s
	<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->

</head>

<body class="sidebar-push  sticky-footer">

	<!-- BEGIN TOPBAR -->

	@include("admin.topbar")

	<!-- END TOPBAR -->

	<!-- BEGIN SIDEBAR -->

	@include("admin.sidebar")

	<!-- END SIDEBAR -->
	<div class="container-fluid">

		@yield("content")

		<div class="footer">
			<a href="{{ URL::to('admin/dashboard') }}" class="brand">
				{{getcong('site_name')}}
			</a>
			<ul>

			</ul>
		</div>
	</div>


	<div class="overlay-disabled"></div>


	<!-- Plugins -->
	<script src="{{ URL::asset('admin_assets/js/plugins.min.js') }}"></script>


	<!-- Loaded only in index.html for demographic vector map-->
	<script src="{{ URL::asset('admin_assets/js/jvectormap.js') }}"></script>

	<!-- App Scripts -->
	{{-- <script src="{{ URL::asset('admin_assets/js/scripts.js') }}"></script> --}}



	{{-- <script src="{{ URL::asset('admin_assets/js/summernote/dist/app.js') }}"></script> --}}

	<script>
		$('.summernote').summernote({
			toolbar: [
			['para', ['style', 'ul', 'ol', 'paragraph']],
			['style', ['bold', 'italic', 'underline', 'clear']],
			['insert', ['table', 'picture', 'link', 'video']],
			['misc', ['codeview', 'fullscreen']]
			],
			tabsize: 2,
			height: 300,
			disableDragAndDrop: false,
			shortcuts: true,
			onImageUpload: function(image, editor, $editable){
				uploadImage(image[0], $(this));
			},
			// onMediaDelete : function($target, editor, $editable) {
			// 	removeImage($target[0].src);
			// 	$target.remove();
			// }
		});

		
		
		function uploadImage(image) {
			var formData = new FormData();
			formData.append("_token", "{!! csrf_token() !!}");
			formData.append("image", image);
			formData.append("upload_file", true);

			$.ajax({
				url: "{{ route('media.upload') }}",
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				type: "post",
				success: function(res) {
					var image = $('<img>').attr('src', res.url);
					$('.summernote').summernote("insertNode", image[0]);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log('jqXHR:');
					console.log(jqXHR);
					console.log('textStatus:');
					console.log(textStatus);
					console.log('errorThrown:');
					console.log(errorThrown);
				}
			});
		}

		function removeImage(imageUrl) {
			var formData = new FormData();
			formData.append("_token", "{!! csrf_token() !!}");
			formData.append("image_url", imageUrl);
			$.ajax({
				url: "{{ route('media.remove') }}",
				cache: false,
				contentType: false,
				processData: false,
				data: formData,
				type: "post",
				success: function(data) {
					console.log(data);
				},
				error: function(data) {
					console.log(data);
				}
			});
		}
	</script>


</body>

</html>


