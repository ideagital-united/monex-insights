@extends("frontend.blog")

@section('head_title', 'Page not found! | '.getcong('site_name') )

@section('head_url', Request::url())

@section("content")

		<div class="cover-container">
			
			<div class="masthead clearfix">
				<div class="inner">
					<!-- Navigation Area
					===================================== -->
					<nav class="navbar navbar-pasific navbar-mp megamenu navbar-fixed-top">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
									<i class="fa fa-bars"></i>
								</button>
								<a class="navbar-brand page-scroll" href="#page-top">
									<img src="assets/img/logo/logo-default.png" alt="logo" class="">
									<span class="color-dark">Pasific</span>
								</a>
							</div>

							<div class="navbar-collapse collapse navbar-main-collapse">
								<ul class="nav navbar-nav">
									<li><a href="mp-index-carousel-1.html"><span class="color-dark">Home</span> </a>
									</li><li><a href="page-about-1.html"><span class="color-dark">About</span> </a>
									</li><li><a href="page-feature-1.html"><span class="color-dark">Feature</span> </a>
									</li><li><a href="page-service-1.html"><span class="color-dark">Service</span> </a>
									</li><li><a href="blog-posts-one-masonry-center-3col.html"><span class="color-dark">Blog</span> </a>
									</li><li><a href="page-contact-1.html" class="mr50"><span class="color-dark">Contact</span> </a>
								</li></ul>

							</div>
						</div>
					</nav>

				</div>
			</div>
			
			<div id="formLogin" class="inner cover text-center animated fadeIn visible" data-animation="fadeIn" data-animation-delay="100">
				<br>
				<i class="under-construction color-pasific"></i><i class="under-construction color-dark"></i><br>
				<h1 class="font-montserrat cover-heading mb20 mt20">Oops..<br>Nothing here!</h1><br><br>
				<h1 style="font-size: 200px" class="color-pasific"><strong>404</strong></h1>
				<br>
			</div>

		</div>

	{{-- <div class="row pt50 pb50">
		<div class="col-md-12">
			<h1 class="text-center">
					<small class="heading heading-icon-o heading-icon-circle center-block">
                            <i class="fa fa-search"></i>
                        </small>                            
				404
				<small class="heading-desc">
					THAT PAGE DOESN’T EXIST!
				</small>
				<small class="heading heading-solid center-block"> </small>
			</h1>
		</div>
	</div> --}}

@endsection
