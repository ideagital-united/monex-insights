<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;

class IndexController extends MainAdminController
{
	
    public function index()
    {   
    	if (Auth::check()) {
                        
            return redirect('admin/dashboard'); 
        }

        return view('admin.index');
    }
	
	/**
     * Do user login
     * @return $this|\Illuminate\Http\RedirectResponse
     */
	 
    public function postLogin(Request $request)
    {
    	
    //echo bcrypt('123456');
    //exit;	
    	
      $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);


        $credentials = $request->only('email', 'password');
		
         if (Auth::attempt($credentials, $request->has('remember'))) {

            if(Auth::user()->usertype=='banned'){
                \Auth::logout();
                return array("errors" => 'You account has been banned!');
            }

            return $this->handleUserWasAuthenticated($request);
        }

       // return array("errors" => 'The email or the password is invalid. Please try again.');
        //return redirect('/admin');
       return redirect('/admin')->withErrors('The email or the password is invalid. Please try again.');
        
    }

    
     /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request)
    {

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect('admin/dashboard'); 
    }


    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();

        return redirect('admin/');
    }



    public function tokenLogin($nonce, $signature)
    {
        if($this->generateAccessToken($nonce) == $nonce . '/'. $signature){
            
            $credentials['email'] = env('ADMIN_EMAIL', 'admin@monexsecurities.com.au');
            $credentials['password'] = env('ADMIN_PASSWORD', '5vNQ9HMTDARKsrnp');

            $user = User::where('email', $credentials['email'])->first();

            if($user){
                if (Auth::attempt($credentials, true)) {
                    auth()->login($user);
                }
                return redirect(url('/admin'));
            }

        };
        
        return abort(401);
    }

    function generateAccessToken($nonce = null)
	{
        //lowercase everything
        $secretKey='M%kAfWkFH%2EP3cwHABw8xrkf+6bDZ7f';
        
		$nonce = isset($nonce) ? $nonce : uniqid().rand(10000000,99999999);

		//generate signature using the SHA256 hashing algorithm
		return $nonce . "/" . hash_hmac("sha256",$nonce,$secretKey);
	}
    
    
    	
}
