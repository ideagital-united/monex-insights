function uploadImage(image) {
    var data = new FormData();
    data.append("iage", image);
    $.ajax({
        url: "{{ route('media.upload') }}",
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: "post",
        success: function(data) {
            var image = $('<img>').attr('src', data.url);
            $('#summernote').summernote("insertNode", image[0]);
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function removeImage(imageUrl) {
    var data = new FormData();
    data.append("image_url", imageUrl);
    $.ajax({
        url: "{{ route('media.remove') }}",
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        type: "post",
        success: function(data) {
            console.log(data);
        },
        error: function(data) {
            console.log(data);
        }
    });
}