-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 23, 2018 at 06:26 AM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monex_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_slug`, `status`) VALUES
(1, 'TECHNOLOGY', 'technology', 1),
(2, 'BUSINESS', 'business', 1),
(3, 'SPORTS', 'sports', 1),
(4, 'WORLD', 'world', 1),
(5, 'MOVIES', 'movies', 1),
(6, 'LIFE STYLE', 'life-style', 1),
(7, 'POLITICS', 'politics', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_10_061754_create_news_table', 1),
('2015_12_10_064734_create_categories_table', 1),
('2015_12_10_065428_create_settings_table', 1),
('2015_12_10_065829_create_widgets_table', 1),
('2016_01_11_085709_create_views_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider_news` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `featured_news` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `status` int(11) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `cat_id`, `user_id`, `title`, `slug`, `description`, `image_title`, `image`, `source`, `tags`, `slider_news`, `featured_news`, `status`, `views`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'นี่คือข่าวแรกสำหรับทดสอบไง', 'this-is-first-news-for-testing', 'เนื้อหายาวๆๆ อ่านกันดูนะ', 'ชื่อรูป', 'this-is-first-news-for-testing_1521712965', 'https://ideagital.com', 'tag1,tag2', 'no', 'no', 1, 1, '2018-03-22 03:02:45', '2018-03-22 19:46:18'),
(2, 6, 1, 'นี่คือข่าวทีสองนะสำหรับทดสอบ', 'this-is-secode-news-for-testing', 'เนื้อหา มากๆๆๆๆๆๆๆ ยาวๆๆๆๆๆๆๆๆ', '', 'this-is-secode-news-for-testing_1521713344', 'http://www.fury.news/wp-content/uploads/2016/06/breaking-.jpg', 'แท็ก', 'no', 'no', 1, 1, '2018-03-22 03:09:04', '2018-03-22 20:20:27'),
(3, 6, 1, 'This is the third of news for testing only', 'this-is-third-news', 'lorem ipsum all sdaa', '', 'this-is-third-news_1521713607', '', '', 'no', 'no', 1, 1, '2018-03-22 03:12:35', '2018-03-22 20:03:52');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_style` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_favicon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_header_code` text COLLATE utf8_unicode_ci NOT NULL,
  `site_footer_code` text COLLATE utf8_unicode_ci NOT NULL,
  `site_copyright` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addthis_share_code` text COLLATE utf8_unicode_ci NOT NULL,
  `disqus_comment_code` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook_comment_code` text COLLATE utf8_unicode_ci NOT NULL,
  `slider_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_post_columns` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_post_columns` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags_post_columns` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `search_post_columns` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `single_post_column` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bg_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_style`, `site_name`, `site_email`, `site_logo`, `site_favicon`, `site_description`, `site_header_code`, `site_footer_code`, `site_copyright`, `addthis_share_code`, `disqus_comment_code`, `facebook_comment_code`, `slider_type`, `home_post_columns`, `category_post_columns`, `tags_post_columns`, `search_post_columns`, `single_post_column`, `bg_image`) VALUES
(1, 'blue', 'Viavi - News, Magazine, Blog Script', 'admin@admin.com', 'logo.png', 'favicon.png', 'viavi new demo site example meta description', '', '', 'Copyright © 2015 Viavi - News, Magazine, Blog Script. All rights reserved.', '', '', '', 'slider1', '2crs', '2crs', '2crs', '2crs', '1crs', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `usertype` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `image_icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `usertype`, `name`, `email`, `password`, `image_icon`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin@admin.com', '$2y$10$I6//iAWyvzpP9j7UJtJeEOgzD1s9jBaqTGTog8Mp2rU7bY.v/bCR2', 'admin-965bf2e0f3108983112bb705d2db4304', 'OH1ya3KG77', '2018-03-13 02:42:43', '2018-03-13 02:42:43');

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_id` int(11) NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`id`, `news_id`, `ip`) VALUES
(1, 3, '::1'),
(2, 1, '::1'),
(3, 2, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `about_news_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_news` text COLLATE utf8_unicode_ci NOT NULL,
  `social_media_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_media_code` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_dribbble` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `social_behance` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured_post_limit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `header_advertise` text COLLATE utf8_unicode_ci NOT NULL,
  `sidebar_advertise` text COLLATE utf8_unicode_ci NOT NULL,
  `recent_posts_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recent_posts_limit` int(11) NOT NULL,
  `popular_posts_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `popular_posts_limit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `about_news_title`, `about_news`, `social_media_title`, `social_media_code`, `contact_title`, `contact_address`, `contact_phone`, `contact_email`, `social_facebook`, `social_twitter`, `social_linkedin`, `social_dribbble`, `social_youtube`, `social_behance`, `featured_title`, `featured_post_limit`, `header_advertise`, `sidebar_advertise`, `recent_posts_title`, `recent_posts_limit`, `popular_posts_title`, `popular_posts_limit`) VALUES
(1, 'About the Magazine', 'Aenean ultricies mi vitae est. Mauris placerat eleifend leosit amet est.', 'Facebook Page', '', 'Contact Us', 'PO Box 16122 Collins Street West\\r\\nVictoria 8007 Australia', '+61 3 8376 6284', 'info@demosite.com', '', '', '', '', '', '', 'FEATURED NEWS', '5', '', '', 'RECENT POSTS', 5, 'POPULAR POSTS', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
