<?php
  /**
   * Sitemap Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: sitemap.tpl.php, v4.00 2014-04-16 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  $sitemap = $content->getSitemap();
?>
<div class="two columns gutters">
  <div class="row">
    <h3><?php echo Lang::$word->_N_PAGES;?></h3>
    <div class="prolific divided list">
      <?php foreach($sitemap as $srow):?>
      <div class="item"><i class="icon angle right"></i><a href="<?php echo doUrl(false, $srow->slug, "page");?>"><?php echo $srow->pgtitle;?></a></div>
      <?php endforeach;?>
      <?php unset($srow);?>
    </div>
  </div>
  <?php if($core->checkTable("mod_blog")):?>
  <?php $artrow = $content->getArticleSitemap();?>
  <div class="row">
    <h3><?php echo getValue("title" . Lang::$lang, "modules", "modalias = 'blog'");?></h3>
    <div class="prolific divided list">
      <?php foreach($artrow as $srow):?>
      <div class="item"><i class="icon angle right"></i><a href="<?php echo doUrl(false, $srow->slug, "blog-item");?>"><?php echo $srow->atitle;?></a></div>
      <?php endforeach;?>
      <?php unset($srow);?>
    </div>
  </div>
  <?php endif;?>
  <?php if($core->checkTable("mod_digishop")):?>
  <?php $digirow = $content->getDigishopSitemap();?>
  <div class="row">
    <h3><?php echo getValue("title" . Lang::$lang, "modules", "modalias = 'digishop'");?></h3>
    <div class="prolific divided list">
      <?php foreach($digirow as $srow):?>
      <div class="item"><i class="icon angle right"></i><a href="<?php echo doUrl(false, $srow->slug, "digishop-item");?>"><?php echo $srow->dtitle;?></a></div>
      <?php endforeach;?>
      <?php unset($srow);?>
    </div>
  </div>
  <?php endif;?>
  <?php if($core->checkTable("mod_portfolio")):?>
  <?php $digirow = $content->getPortfolioSitemap();?>
  <div class="row">
    <h3><?php echo getValue("title" . Lang::$lang, "modules", "modalias = 'portfolio'");?></h3>
    <div class="prolific divided list">
      <?php foreach($digirow as $srow):?>
      <div class="item"><i class="icon angle right"></i><a href="<?php echo doUrl(false, $srow->slug, "portfolio-item");?>"><?php echo $srow->ptitle;?></a></div>
      <?php endforeach;?>
      <?php unset($srow);?>
    </div>
  </div>
  <?php endif;?>
</div>