<?php
/**
* Login Template
*
* @package CMS Pro
* @author prolificscripts.com
* @copyright 2014
* @version $Id: login.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
*/

if (!defined("_VALID_PHP"))
die('Direct access to this location is not allowed.');
?>
<?php
if ($user->logged_in)
redirect_to(doUrl(false, $core->account_page, "page"));

if (isset($_POST['doLogin']))
: $result = $user->login($_POST['username'], $_POST['password']);
/* Login Successful */
if ($result)
: redirect_to(doUrl(false, $core->account_page, "page"));
endif;
endif;
?>

<div id="formLogin" class="container-fluid pt40 pb40 bb-dashed-1 inner cover text-center animated" data-animation="fadeIn" data-animation-delay="100">
  <div class="row">
    <div class="container">
      <div class="row">
            <div class="col-md-12 text-center">
              <h1 class="font-size-normal">
                <small><span aria-hidden="true" class="icon-global"></span> Login</small>
                <?php echo Lang::$word->_UA_TITLE2;?> <i class="small icon asterisk"></i>
                <small class="heading heading-solid center-block"></small>
            
              </h1>
              <div class="row">
                <div class="col-md-6 col-md-offset-3" ">
                  <?php print Filter::$showMsg; ?>
                </div>
              </div>
              
            </div>
            <div class="col-md-6 col-md-offset-3 ">
              <h4 class="text-center mb20"></h4>
              <form  method="post" id="login-form" name="login_form" class="form-horizontal" >
                <div class="form-group">

                  <label for="inputEmail3" class="col-sm-3 control-label">Email Address</label>
                  <div class="col-sm-9">
                    <input id="login_username" value="<?php echo (isset($_SESSION['email_activate'])) ?$_SESSION['email_activate']:''; ?>" name="username" class="form-control" placeholder="Email Address" type="text">
                    <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Email"> -->
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label"><?php echo Lang::$word->_PASSWORD;?></label>
                  <div class="col-sm-9">
                    <input name="password" id="login_password" class="form-control" placeholder="<?php echo Lang::$word->_PASSWORD;?>" type="password">
                      <input name="doLogin" type="hidden" value="1">
                    <!-- <input type="password" class="form-control" id="inputPassword3" placeholder="Password"> -->
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-8">
                    <div class="content-right">
                      <button name="submit" type="submit" class="button button-sm button-success"><?php echo Lang::$word->_UA_LOGINNOW;?></button> <a href="<?php echo doUrl(false, $core->register_page, "page");?>" class="  right-space"/>  <?php echo Lang::$word->_UA_CLICKTOREG;?></a>

                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group text-center">
                  <div class="col-sm-offset-2 col-sm-8">
                    <h4><a href="#" id="showFormRegister">Forgot your password?</a></h4>
                    <!--
                    <small>Enter your username and email address below to reset your password.
                    A verification token will be sent to your email address. Once you have received the token, 
                    you will be able to choose a new password for your account.</small>
                    -->
                  </div>
                </div>
              </form>
            </div>
          </div>
    </div>
  </div>
</div>



<div id="formRegister"  class="inner cover  hidden  animated" data-animation="fadeIn" data-animation-delay="100">

          <div class="row">
            <div class="container">
            
                

              <div class="col-md-12 text-center">
                <h1 class="font-size-normal">
                  <small><span aria-hidden="true" class="  icon-key"></span> Forgot password </small>
                  Forgot your password?
                  <small class="heading heading-solid center-block"></small>
                   </h1>
                   <div class="row"><p>    Enter your email address and Captcha Code below to reset your password.
                    An activation token will be sent to your email address. Once you have received the token, 
                    you will be able to choose a new password for your account.</p>
                    </div>
               
<div class="row">
                <div class="col-md-6 col-md-offset-3" id="msgholder">
                 
                </div>
              </div>
              </div>
                <div class="col-md-6 col-md-offset-3 ">
                  <h4 class="text-center mb20"></h4>
                  <form class="form-horizontal" name ="formRegisterfrm" id ="formRegisterfrm" >
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Email Address</label>
                      <div class="col-sm-9">
                        <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Username"> -->
                        <input name="uname" class="form-control" placeholder="Email Address" type="text">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label"><img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-append" /> <i class="icon-prepend icon unhide"></i></label>
                      <div class="col-sm-9">
                        <div class="content-right">
                          <input class="form-control" name="captcha"  placeholder="<?php echo Lang::$word->_UA_PASS_RTOTAL;?>" type="text">
                      </div>
                    </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-sm-offset-1 col-sm-10 text-center">
                        <a class="active homepage" href="<?php echo SITEURL;?>/page/login">
                          <button  type="button" name="dosubmit_back" id="btn_dis" class="button button-sm button-default"><?php echo Lang::$word->_FM_BACK;?></button>
                        </a>

                        <button data-url="/ajax/user.php" type="button" name="dosubmit" class="button button-sm button-success"><?php echo Lang::$word->_UA_PASS_RSUBMIT;?></button>
                        <input name="passReset" type="hidden" value="1">
                        </div>
                      </div>
                    <!-- <hr> -->
                    <!-- <div class="form-group text-center">
                      <div class="col-sm-offset-1 col-sm-10">
                        <h4><a href="#" id="showFormLogin" onclick="">You have an account?</a></h4>
                        <small>Enter your account and password to login.</small>
                        </div>
                      </div> -->
                  </form>
                    </div>
               
              </div>
            </div>
            </div>


          <script>
              $(function(){

                  $("#showFormRegister").on('click',function(){
                      $("#formLogin").addClass("hidden");
                      $("#formRegister").removeClass("hidden");
                  });
                  $("#showFormLogin").on('click',function(){
                      $("#formLogin").removeClass("hidden");
                      $("#formRegister").addClass("hidden");
                  })
                  
                  $('button[name=dosubmit]').on('click', function () {
			          posturl = $(this).data('url');
			          var btnclick = $(this);
			          btnclick.html("CHECKING...");
			          $.ajax({
			              url 	: "<?php echo SITEURL;?>" + posturl,
			              type 	: "post",
			              data	: $('#formRegisterfrm').serialize(),
			              dataType : "json",
			              success	: function(json) {
			                  	console.log(json)
			                  	$("#msgholder").html(json.message);
			                  	 btnclick.html("SUBMIT");
			                	if(json.status == 'success'){
			                   		if (json.gotopage) {
							      		if (json.delaytime) {
							          		var timeset = json.delaytime*1;
							           		setTimeout(function(){
			                               		window.location.href = SITEURL + "/" + json.gotopage;    
			                            	}, timeset);
							      		} else {
							           		window.location.href = SITEURL + "/" + json.gotopage; 
							      		}				  						
							    	}	
			                	}
			              }
			          })

      			});
      			
              })

              // $('.navbar-pasific').removeAttr('data-action')

          </script>

