<?php
  /**
   * Register Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: register.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  if ($user->logged_in)
      redirect_to($core->account_page);
?>
<?php if(!$core->reg_allowed):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_NOMORE_REG);?>
<?php elseif($core->user_limit !=0 and $core->user_limit == countEntries("users")):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_MAX_LIMIT);?>
<?php else:?>
    

<p class="content-center"><i class="information icon"></i> <?php echo Lang::$word->_UA_INFO4. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></p>
<div class="columns">
  <div class="screen-60 phone-100 push-center">
    <div class="prolific form secondary segment">
      <form id="prolific_form" name="prolific_form" method="post">
        <h3>Introduce Yourself</h3>
        <div class="two fields">
          <div class="field">
            <label>Country of Residence<i class="icon-append icon asterisk"></i></label>
             <label class="input">
                <?php
                $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' order by id");
                ?>
                <select name="country_residence" id="country_residence" >
                  <?php
                  $codecc = "";
                  foreach($cc as $kc => $vc){
                  ?>
                  <option value="<?php echo $vc->country;?>"><?php echo $vc->country;?></option>
                  <?php
                  }
                  ?>
                </select>
              </label>
          </div>
        </div>
        <div class="two fields show_notice_cc_residence" style="display:none;">
          <div class="field">
            <label>Sorry. We are currently unable to process applications from residents outside Australia.
            	Please register your Emial Address. We will notify you once we start accepting applications.</label>
            <label>Email Address</label>
             <label class="input">
               <input type="text" name="email_residence" id = "email_residence" value="E-mail Address"> 
               <button  type="button" id="sendmail_residence" name="sendmail_residence" class="prolific positive button">Register</button>                
              </label>
             <div id ="msg_residence"></div>
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label>Title<i class="icon-append icon asterisk"></i></label>
            <label class="input">
              <select name="title_name">
              	<option value="">Please Select</option>
              	<option value="Mr.">Mr.</option>
              	<option value="Mrs.">Mrs.</option>
              	<option value="Ms.">Ms.</option>
              	<option value="Dr.">Dr.</option>
              </select>
            </label>
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label><?php echo Lang::$word->_UR_FNAME;?><i class="icon-append icon asterisk"></i></label>
            <label class="input"><i class="icon-prepend icon user"></i> <i class="icon-append icon asterisk"></i>
              <input name="fname" placeholder="<?php echo Lang::$word->_UR_FNAME;?>" type="text">
            </label>
          </div>
        </div>
		<div class="two fields">
          <div class="field">
            <label><?php echo Lang::$word->_UR_MNAME;?>(<?php echo Lang::$word->_OPTTIONAL;?>)</label>
            <label class="input"><i class="icon-prepend icon user"></i>
              <input name="middle_name" placeholder="<?php echo Lang::$word->_UR_MNAME;?>" type="text">
            </label>
          </div>
        </div>
        
        <div class="two fields">
          <div class="field">
            <label>Family Name<i class="icon-append icon asterisk"></i></label>
            <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>
              <input name="lname" placeholder="<?php echo Lang::$word->_UR_LNAME;?>" type="text">
            </label>
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label>Gender (<?php echo Lang::$word->_OPTTIONAL;?>)</label>
            <label class="input"><i class="icon-prepend icon user"></i>
             <select name="gender">
              	<option value="Female">Female</option>
              	<option value="Male">Male</option>
              	<option value="Not">Prefer not to provide</option>
              </select>
            </label>
          </div>
        </div>
        <div class="two fields">
            <div class="field">
              <label><?php echo Lang::$word->_UR_EMAIL; // echo Lang::$word->_USERNAME; ?></label>
              <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>
                <input name="email" id="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" type="text">
                <i class="icon check" id='email_ok' style="display:none;"></i><span id="email_msg"></span>
              </label>
            </div>
        </div>

          <div class="two fields">
          <div class="field">
            <label><?php echo Lang::$word->_PASSWORD;?></label>
            <?php echo Lang::$word->_UR_PASSWORD_R4;?>
            <label class="input"><i class="icon-prepend icon lock"></i> <i class="icon-append icon asterisk"></i>
              <input name="pass" placeholder="<?php echo Lang::$word->_PASSWORD;?>" type="password">
              
              <p><i class="icon check" id='pwd_ok_1' style="display:none;"></i> At least <?php echo PWD_LEANG;?> character</p>
              <p><i class="icon check" id='pwd_ok_2' style="display:none;"></i> At least <?php echo PWD_LEANG_UPPER;?> capital letter</p>
              <p><i class="icon check" id='pwd_ok_4' style="display:none;"></i> At least <?php echo PWD_LEANG_LOWER;?> lowercase letter</p>
              <p><i class="icon check" id='pwd_ok_3' style="display:none;"></i> At least <?php echo PWD_LEANG_DIGI;?> number</p>
            </label>
          </div>
          </div>
       <div class="two fields">
          <div class="field">
            <label><?php echo Lang::$word->_UA_PASSWORD2;?></label>
            <label class="input"><i class="icon-prepend icon lock"></i><i class="icon-append icon asterisk"></i>
              <input name="pass2" placeholder="<?php echo Lang::$word->_UA_PASSWORD2;?>" type="password">
            </label>
          </div>
        </div>
        <div class="two fields">
            <div class="field newform">
              <label><?php echo Lang::$word->_COUNTRY_CODE;?></label>
              <label class="input">
                <?php
                $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' order by id");
                ?>
                <select name="country_code" id="listcountry" >
                  <?php
                  $codecc = "";
                  foreach($cc as $kc => $vc){
                  ?>
                  <option value="<?php echo $vc->country_code?>"><?php echo $vc->country_code;?></option>
                  <?php
                  }
                  ?>
                </select>
              </label>
            </div>
            <div class="field newform">
              <label><?php echo Lang::$word->_MOBILE_PHONE_NUMBER;?></label>
              <label class="input"><i class="icon-prepend icon user"></i> <i class="icon-append icon asterisk"></i>
                <input name="mobile" id="mobile" placeholder="<?php echo Lang::$word->_MOBILE_PHONE_NUMBER;?>" type="text" value="">
              </label>
            </div>
          </div>
      
        <div class="two fields">
          <div class="field">
            <label>Residential Address (Not PO Box)  <i class="icon-append icon asterisk"></i></label>
          </div>
        </div>
         <div class="two fields">
          <div class="field">
            <label>Address line 1 <i class="icon-append icon asterisk"></i></label>
            <label class="input">
              <input name="addr1" placeholder="Address 1" type="text">
            </label>
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label>Address line 2 <i class="icon-append icon asterisk"></i></label>
            <label class="input">
              <input name="addr2" placeholder="Address 2" type="text">
            </label>
          </div>
        </div>
           <div class="two fields">
          <div class="field">
            <label>Suburb, Town or City(<?php echo Lang::$word->_OPTTIONAL;?>)</label>
            <label class="input">
              <input name="state" placeholder="State / Province" type="text">
            </label>
          </div>
        </div>
         <div class="two fields">
          <div class="field">
            <label>Post code  <i class="icon-append icon asterisk"></i></label>
            <label class="input">
              <input name="post_code" id="post_code" placeholder="Post Code" type="text">
            </label>
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label>Suburb, Town or City(<?php echo Lang::$word->_OPTTIONAL;?>)</label>
            <label class="input">
              <input name="state" placeholder="State / Province" type="text">
            </label>
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label>Country <i class="icon-append icon asterisk"></i></label>
            <label class="input">
                <?php
		        $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' order by id");
		        ?>
		        <select name="country" >
		          <?php
		          $codecc = "";
		          foreach($cc as $kc => $vc){
		          ?>
		          <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
		          <?php
		          }
		          ?>
		        </select>
            </label>
          </div>
        </div>
        
        <div class="two fields">
          <div class="field">
            <label>Country of Citizenship <i class="icon-append icon asterisk"></i><input type="checkbox" id="country_citizen_more" name="country_citizen_more" value="1">I have more than one citizenship</label>
            <label class="input">
                <?php
		        $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' order by id");
		        ?>
		        <select name="country_citizen" >
		          <?php
		          $codecc = "";
		          foreach($cc as $kc => $vc){
		          ?>
		          <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
		          <?php
		          }
		          ?>
		        </select>
            </label>
            <label class="input " id="more_country_citizen" style="display:none;">
                <?php
		        $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' order by id");
		        ?>
		        <select name="more_country_citizen" >
		          <?php
		          $codecc = "";
		          foreach($cc as $kc => $vc){
		          ?>
		          <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
		          <?php
		          }
		          ?>
		        </select>
            </label>
          </div>
        </div>
        <!--
        <div class="two fields">
          <div class="field">
            <label>Country of Birth<i class="icon-append icon asterisk"></i><input type="checkbox" id="country_birth_same" value="same_country_residence">Same as Country of residence</label>
            <label class="input">
                <?php
		        $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' order by id");
		        ?>
		        <select name="country_birth" >
		          <?php
		          $codecc = "";
		          foreach($cc as $kc => $vc){
		          ?>
		          <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
		          <?php
		          }
		          ?>
		        </select>
            </label>
          </div>
        </div>
        -->
         <div class="two fields">
          <div class="field">
            <label>Date of Birth<i class="icon-append icon asterisk"></i>(You must be over 18 years old)</label>
            <label class="input">
                
		        <select name="birth_dd" >
		         <?php for($idd=1; $idd <=31; $idd++){?>
		         	<option value="<?php echo $idd;?>"><?php echo $idd;?></option>
		         <?php } ?>
		        </select>
		         <select name="birth_mm" >
		          <?php for($imm=1; $imm <=12; $imm++){?>
		         	<option value="<?php echo $imm;?>"><?php echo $imm;?></option>
		         <?php } ?>
		        </select>
		         <select name="birth_yy" >
		          <?php 
		          $iyy = date("Y");
				  $iyymax = $iyy-18;
				  $iyymin = $iyy-80;
		          for($iyy=$iyymin; $iyy <=$iyymax; $iyy++){
		          
		          ?>
		         	<option value="<?php echo $iyy;?>"><?php echo $iyy;?></option>
		         <?php } ?>
		        </select>
            </label>
          </div>
        </div>
        
         <div class="two fields">
          <div class="field">
            <label>Purpose of opening an account with Monex AU<i class="icon-append icon asterisk"></i></label>
            <label class="input">
		        <select name="purpose_monex_au" >
		         <option value="For personal investment">For personal investment</option>
		         <option value="For diversification into international markets">For diversification into international markets</option>
		         <option value="For short-term speculative trading">For short-term speculative trading</option>
		         <option value="For hedging of other investments">For hedging of other investments</option>
		         <option value="For managing money on behalf of others">For managing money on behalf of others</option>
		         <option value="For educational purposes">For educational purposes</option>
		         <option value="Other">Other</option>
		        </select>
		        
            </label>
          </div>
        </div>  
        <div class="two fields" id="other_purpose_monex_au">
          <div class="field">
            <label>If "Other", please specify.</label>
            <label class="input">
                <input name="purpose_other" placeholder="-" type="text">
            </label>
          </div>
        </div>
          <div class="two fields">
          <div class="field">
            <label>Employment Status<i class="icon-append icon asterisk"></i></label>
            <label class="input">
                
		        <select name="employment" >
		         <option value="Full time">Full time</option>
		         <option value="Part time">Part time</option>
		         <option value="Unemployed">Unemployed</option>
		         <option value="Student">Student</option>
		         <option value="Retired">Retired</option>
		         <option value="Other">Other</option>
		        </select>
		        
            </label>
          </div>
        </div>  
        
        <div class="two fields" id="other_employment">
          <div class="field">
            <label>If "Other", please specify.</label>
            <label class="input">
                <input name="employment_other" placeholder="Post Code" type="text">
            </label>
          </div>
        </div>
         <div class="two fields">
          <div class="field">
            <label>Occupation<i class="icon-append icon asterisk"></i></label>
            <label class="input">
                
		        <select name="occupation" >
		         <option value="Full time">Full time</option>
		         <option value="Part time">Part time</option>
		        </select>
		        
            </label>
          </div>
        </div>  
        <!--
        <div class="two fields" id="other_occupation">
          <div class="field">
            <label>If "Other", please specify.</label>
            <label class="input">
                <input name="occupation_other" placeholder="Post Code" type="text">
            </label>
          </div>
        </div>
        -->
               <div class="two fields">
          <div class="field">
            <label>Country of Employer<i class="icon-append icon asterisk"></i>
            	<!--<input type="checkbox" id="country_employer_same" value="same_country_residence">Same as Country of residence-->
            	</label>
            <label class="input">
                <?php
		        $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' order by id");
		        ?>
		        <select name="country_employer" >
		          <?php
		          $codecc = "";
		          foreach($cc as $kc => $vc){
		          ?>
		          <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
		          <?php
		          }
		          ?>
		        </select>
            </label>
          </div>
        </div>
        <div class="two fields">
          <div class="field">
            <label><input type="checkbox" value="1" name="isagree">
            	I hereby agree to the <a href="#">Terms & Conditions</a> and <a href="#">Privacy Policy</a> and that we may contact you regarding this application. 
            </label>
            
          </div>
        </div>
      
        
       
        
        <div class="field">
          <label><?php echo Lang::$word->_UA_REG_RTOTAL;?></label>
          <label class="input"><img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-append"> <i class="icon-prepend icon unhide"></i>
            <input type="text" name="captcha">
          </label>
        </div>
        <a class="active homepage" href="<?php echo SITEURL;?>">	
        	<button  type="button" name="dosubmit_back" id="btn_dis" class="prolific button disabled "><?php echo Lang::$word->_FM_BACK;?></button> 
        </a> 
        <button data-url="/ajax/user.php" type="button" id="btn_ok" name="dosubmit" class="prolific positive button submit"><?php echo Lang::$word->_UA_REG_ACC;?></button>                
        <input name="doRegister" type="hidden" value="1">
      </form>
    </div>
    <div id="msgholder"></div>
    
   
    
    
  </div>
</div>


<?php endif;?>

  
<script type="text/javascript">
var pwd_length 	= "<?php echo PWD_LEANG;?>";
var pwd_upper 	= "<?php echo PWD_LEANG_UPPER;?>";
var pwd_digi 	= "<?php echo PWD_LEANG_DIGI;?>";
var pwd_lower 	= "<?php echo PWD_LEANG_LOWER;?>";
pwd_length 		= pwd_length*1;
pwd_upper 		= pwd_upper*1;
pwd_digi 		= pwd_digi*1;
pwd_lower		= pwd_lower*1;
$( document ).ready(function() {
  // $('.submit').click(function(e){
  //   e.preventDefault()
  // })



  // $('form').find('input').each(function(){
  //   if(!$(this).prop('required')){

  //   }else{
  //     if($(this).val() == ""){
  //       $(this).closest('.form-group').addClass('has-error');
  //     }else{
  //       $(this).closest('.form-group').removeClass('has-error');
  //     }
  //   }
  // })

$('#country_residence').change(function(){
	var chkcc_residence = $(this).val();
	if (chkcc_residence != 'Australia') {
		$('.show_notice_cc_residence').show();
	} else {
		$('.show_notice_cc_residence').hide();
	}
});
$('input[name="pass"]').keyup(function(){
    var thisval 		= $(this).val();
    var lengthstr 		= thisval.length;
    var lengthdigi 		= thisval.length - thisval.replace(/[1-9]/g, '').length;
    var lengthupper 	= thisval.length - thisval.replace(/[A-Z]/g, '').length;
    var lengthlower 	= thisval.length - thisval.replace(/[a-z]/g, '').length;
    if (lengthstr >= pwd_length) {
    	$('#pwd_ok_1').show();	
    } else {
    	$('#pwd_ok_1').hide();	
    }
    if (lengthupper >= pwd_upper) {
    	$('#pwd_ok_2').show();	
    } else {
    	$('#pwd_ok_2').hide();	
    }
    if (lengthdigi >= pwd_digi) {
    	$('#pwd_ok_3').show();	
    } else {
    	$('#pwd_ok_3').hide();	
    }
    if (lengthlower >= pwd_lower) {
    	$('#pwd_ok_4').show();	
    } else {
    	$('#pwd_ok_4').hide();	
    }
});
//msg_residence
$('#sendmail_residence').click(function(){
	 var valemailres = $("#email_residence").val();
    $.ajax({
      url: SITEURL + "/ajax/user.php" ,
      type: 'POST',
      data:{doCheckEmailResiden : 1, emailres:valemailres},
      dataType :'json'
    }).done(function(obj) {
        $('#msg_residence').html(obj.msg);
        if (obj.status=='success') {
            window.location.href = SITEURL;
        }
    });
});
$('#email').keyup(function(){
    var valemail = $("#email").val();
    //email_ok
    $.ajax({
      url: SITEURL + "/ajax/user.php" ,
      type: 'POST',
      data:{doCheckEmail : 1, email:valemail},
      dataType :'json'
    }).done(function(obj) {
       if (obj.status=='success') {
            $('#email_ok').show().removeClass('delete').addClass('check');
            $('#email_msg').html('');
       } else {
            $('#email_ok').show().removeClass('check').addClass('delete');
            $('#email_msg').html(obj.msg);
       }
    });
});

$('#country_birth_same').click(function(){
    if($(this).is(":checked")){
    	$("select[name=country_birth]").val($("select[name=country]").val()).trigger("chosen:updated");
    } 
});
$('#country_citizen_more').click(function(){
	if($(this).is(":checked")){
    	$("#more_country_citizen").show();
    } else {
    	$("#more_country_citizen").hide();
    }
});
$('#country_employer_same').click(function(){
	if($(this).is(":checked")){
    $("select[name=country_employer]").val($("select[name=country]").val()).trigger("chosen:updated");
    } 
});
});
</script>

