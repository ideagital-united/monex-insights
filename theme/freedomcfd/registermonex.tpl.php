<style>
  .mag-lef{
    margin-left: -16px
  }

</style>

<?php
  /**
   * Register Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: register.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  if ($user->logged_in)
      redirect_to($core->account_page);

?>
<?php if(!$core->reg_allowed):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_NOMORE_REG);?>
<?php elseif($core->user_limit !=0 and $core->user_limit == countEntries("users")):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_MAX_LIMIT);?>
<?php else:?>

  <div class="container">
    <form id="prolific_form" name="prolific_form" method="post">
      <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
        <h2>
          Register
          <small class="heading heading-solid center-block"></small>
        </h2>
        <p>
 Welcome to Monex Australia! We look forward to having you on board.
Please fill out the form below to help us verify your email and identity online.<br><br>
After you click "REGISTER ACCOUNT",  you will receive an email containing a verification link that enables you to continue the application in our secure Client Portal.<br><br>
We are asking your personal details including financial and tax information to comply with international obilgations. 
We take the security of all your data very seriously and we will keep your information safe and confidential. 
        </p>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-6">
          <div style="text-align: right;">
            <p>Country of Residence <span class="color-pasific">*</span></p>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
          <div >
                    <?php
			$cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' and ga=1 order by id");
                    ?>
                    <select name="country_residence" id="country_residence" class="form-control" >
                      <?php
                      $codecc = "";
                      foreach($cc as $kc => $vc){
                      ?>
                      <option  value="<?php echo $vc->country;?>"><?php echo $vc->country;?></option>
                      <?php
                      }
                      ?>
                    </select>

          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
          <div>

          </div>

        </div>
      </div>

      <div class="row show_notice_cc_residence" style="display:none;">
              <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <div class="form-group alert alert-warning" style="text-align: center;">
                  Sorry, We are currently unable to process applications from residents outside Australia. Please register your Email Address. We will notify you once we start accepting applications.
                </div>
              </div>
              
            </div>

        <div class="row show_notice_cc_residence" style="display:none;">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
              <p>Email Address</p>
            </div>
          </div>
          <div class="col-md-4" style="text-align: center;">
                <input type="text" class="form-control" autocomplete="off" name="email_residence" id = "email_residence" placeholder ="E-mail Address">
          </div>
          <div class="col-md-4" style="text-align: left;">
              <button  type="button" id="sendmail_residence" autocomplete="off" name="sendmail_residence" class="btn btn-info">Register</button>
          </div>
        </div>
        <div class="row email_residence_msg" style="display:none;">
                  <div class="col-md-4 col-sm-4 col-xs-6">
                    <div >
                     
                    </div>
                  </div>
                  <div class="col-md-4">
                        <div class="alert alert-success" id="email_residence_ok" style="display:none;">
                        </div>
                        <div class="alert alert-warning" id="email_residence_err" style="display:none;">
                        </div>
                  </div>
                 
                </div>



        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Title <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">

              <div >
                      <select name="title_name" class="form-control">
                      <option value="">Please Select</option>
                      <option value="Mr.">Mr.</option>
                      <option value="Mrs.">Mrs.</option>
                      <option value="Ms.">Ms.</option>
                      <option value="Dr.">Dr.</option>
                    </select>
               </div>

            </div>
          </div>


        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Given Name <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div style="text-align: center;">
                      <input name="fname" class="form-control" autocomplete="off" placeholder="<?php echo Lang::$word->_UR_FNAME;?>" type="text">
               </div>
          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Middle Name (Optional)</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div style="text-align: center;">
                      <input name="middle_name" class="form-control" autocomplete="off" placeholder="<?php echo Lang::$word->_UR_MNAME;?>" type="text">
               </div>
          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Family Name <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div style="text-align: center;">
                      <input name="lname" class="form-control" autocomplete="off" placeholder="Family Name" type="text">
               </div>
          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Gender (Optional)</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div>
              <select name="gender" class="form-control">
			 	<option value="">Please Select</option>
				<option value="Female">Female</option>
                <option value="Male">Male</option>
                <option value="Prefer not to provide">Prefer not to provide</option>
              </select>
               </div>
          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Email Address <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div style="text-align: center;">
                    <input name="email" class="form-control" autocomplete="off" id="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" type="text">
                    
               </div>
          </div>
        </div>
        
        <div class="row hide_notice_cc_residence"  id='email_ok' style="display:none;">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">

            </div>
          </div>

          <div class="col-md-4">
                <div class="alert alert-warning" id="email_msg">
                </div>
          </div>
        </div>


        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
              <p>Create Password <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div style="text-align: left;">
                      <input name="pass" id="pass" class="form-control" placeholder="<?php echo Lang::$word->_PASSWORD;?>" type="password">
			<i class="fa fa-check color-success" id='pwd_ok_1' style="display:none;"></i> At least <?php echo PWD_LEANG;?> characters<br>
                      <i class="fa fa-check color-success" id='pwd_ok_2' style="display:none;"></i> At least <?php echo PWD_LEANG_UPPER;?> capital letter<br>
                      <i class="fa fa-check color-success" id='pwd_ok_4' style="display:none;"></i> At least <?php echo PWD_LEANG_LOWER;?> lowercase letter<br>
                      <i class="fa fa-check color-success" id='pwd_ok_3' style="display:none;"></i> At least <?php echo PWD_LEANG_DIGI;?> number<br>
               </div>
          </div>
        </div>
 <div class="row hide_notice_cc_residence">&nbsp;</div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Re-enter password <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div style="text-align: center;">
                      <input name="pass2" class="form-control" placeholder="<?php echo Lang::$word->_UA_PASSWORD2;?>" type="password">
               </div>
          </div>
        </div>
<div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Mobile Phone Number</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">

          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Country code</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div >
				<input type="text" name="country_code" value="61" readonly="true">
               </div>
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Mobile Phone Number <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <div style="text-align: center;">
                      <input name="mobile" id="mobile" class="form-control" placeholder="MOBILE PHONE NUMBER" type="text" value="">
               </div>
          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Residential Address (Not PO Box)</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">

          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Address line 1 <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <input name="addr1" placeholder="Address 1" class="form-control" type="text">
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Address line 2 (Optional)</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <input name="addr2" placeholder="Address 2" class="form-control" type="text">
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Suburb/City and State<span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <input name="state" class="form-control" placeholder="Suburb/City and State" type="text">
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Post code <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
              <input name="post_code" class="form-control" id="post_code" placeholder="Post Code" type="text">
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Country</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
	<input type="text" name="country" value="Australia" readonly="true">
          </div>


        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Country of Citizenship <span class="color-pasific">*</span></p>

            </div>
          </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
               <select name="country_citizen" >
              <?php
              $codecc = "";
              foreach($cc as $kc => $vc){
              ?>
              <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
              <?php
              }
              ?>
            </select>
            <input   type="checkbox" id="country_citizen_more" name="country_citizen_more" value="1"><label>&nbsp;&nbsp; I have more than one citizenship</label>

          </div>
          </div>
            <div class="form-group">
     <div id="more_country_citizen" class="row hide_notice_cc_residence"  style="display:none;">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">

            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
                   <?php
                $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' and ga=1  order by id");
                ?>
                <select  class="form-control" name="more_country_citizen" >
                	<option value="">Please Select</option> 
                  <?php
                  $codecc = "";
                  foreach($cc as $kc => $vc){
                  ?>
                  <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
                  <?php
                  }
                  ?>
                </select>
          </div>
          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6 ">
            <div style="text-align: right;">
               <p>Date of Birth <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div  style="text-align: left;" class="col-md-4 col-sm-4 col-xs-6">

            <div class="col-md-4 col-sm-4 col-xs-12 ">
              <div class="form-group">
                 <select class="form-control" name="birth_dd" >
                 <option value="">DATE</option>
                 <?php for($idd=1; $idd <=31; $idd++){?>
                  <option value="<?php echo $idd;?>"><?php echo $idd;?></option>
                 <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group">
                 <select class="form-control" name="birth_mm" >
                 <option value="">MONTH</option>
                  <?php for($imm=1; $imm <=12; $imm++){?>
                  <option value="<?php echo $imm;?>"><?php echo $imm;?></option>
                 <?php } ?>
                </select>
            </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="form-group">
                 <select class="form-control" name="birth_yy" >
                 <option value="">YEAR</option>
                  <?php
                  $iyy = date("Y");
                  $iyymax = $iyy-18;
                  $iyymin = $iyy-100;
                  for($iyy=$iyymin; $iyy <=$iyymax; $iyy++){
                  ?>
                  <option value="<?php echo $iyy;?>"><?php echo $iyy;?></option>
                 <?php } ?>
                </select>
                </div>
                </div>
          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Purpose of opening an account with Monex AU <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
		 <select class="form-control" name="purpose_monex_au" onchange="checkopt($(this),'purpose_monex');" >
		 <option value="">Please Select</option> 
                <option value="For personal investment">For personal investment</option>
                 <option value="For diversification into international markets">For diversification into international markets</option>
                 <option value="For short-term speculative trading">For short-term speculative trading</option>
                 <option value="For hedging of other investments">For hedging of other investments</option>
                 <option value="For managing money on behalf of others">For managing money on behalf of others</option>
                 <option value="For educational purposes">For educational purposes</option>
                 <option value="Other">Other</option>
                </select>
          </div>
        </div>
	<div class="row hide_notice_cc_residence purpose_monex" style="display:none;">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>If "Other", please specify.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
            <input name="purpose_other" class="form-control" placeholder="" type="text">
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Employment Status <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
		<select  class="form-control" name="employment"  onchange="checkopt($(this),'other_employment');">
                 <option value="">Please Select</option> 
				<option value="Full time">Full time</option>
                 <option value="Part time">Part time</option>
                 <option value="Unemployed">Unemployed</option>
                 <option value="Student">Student</option>
                 <option value="Retired">Retired</option>
                 <option value="Other">Other</option>
                </select>
          </div>

        </div>
	<div class="row hide_notice_cc_residence other_employment" style="display:none;">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>If "Other", please specify.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
          <input name="employment_other" class="form-control" placeholder="" type="text">

          </div>
        </div>

        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Occupation</p>
            </div>
          </div>
         <div class="col-md-4 col-sm-4 col-xs-6" >
          	<input name="occupation" class="form-control forcase01" placeholder="Occupation" type="text">
         	
			<input name="occupationtxt" class="form-control forcase02" placeholder="Occupation" type="text" style="display:none;">
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Country of Employer</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
          <?php
                $cc = $db->fetch_all("SELECT * from bk_country_code where  id != '' and ga=1  order by id");
                ?>
                <select class="form-control"  name="country_employer" >
                	<option value="">Please Select</option>
                  <?php
                  $codecc = "";
                  foreach($cc as $kc => $vc){
                  ?>
                  <option value="<?php echo $vc->country?>"><?php echo $vc->country;?></option>
                  <?php
                  }
                  ?>
                </select>
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
               <p>Agreement <span class="color-pasific">*</span></p>
            </div>
          </div>
          <div class="col-md-6 col-xs-6">
            <input type="checkbox" value="1" name="isagree">
            I hereby acknowledge and agree to the Terms of use of this website, 
            <a href="<?php echo SITEURL."/uploads/Terms-and-Conditions.pdf";?>" target="_blank" class="btn-link">Terms and conditions</a>,  
            <a href="<?php echo SITEURL."/uploads/Financial-Services-Guide.pdf";?>" target="_blank"  class="btn-link">Financial Services Guide</a> and 
            <a href="<?php echo SITEURL."/uploads/Privacy-Policy.pdf";?>"  target="_blank" class="btn-link">Privacy Policy</a>. 
            I consent to be contacted by Monex Securities Australia Pty Ltd as required regarding this application to open an account. 
            You consent to (unless you notify us in writing otherwise) your personal information being used by us to send you marketing information about other products and services offered or promoted by us
            , via telephone, electronic messages like email, online and other means, 
            for the purpose of marketing our and third parties goods and services and other services, 
            unless you notify us in writing that you do not wish your personal information to be used for this purpose.
          </div>
        </div>
      <div class="row hide_notice_cc_residence">&nbsp;</div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
              <p><?php echo Lang::$word->_UA_REG_RTOTAL;?> <span class="color-pasific">*</span></p>

            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: center;">
              <div class="col-sm-6">
                <input type="text" class="form-control" name="captcha">

              </div>
              <div class="col-sm-6">
                <img id="captchaa" src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-append">
                <a href="javascript:void(0)" id="recaptcha"><i class="fa fa-repeat" aria-hidden="true"></i></a>
              </div>

            </div>
          </div>
        </div>


        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">

            </div>
          </div>
          <div class="col-md-6">
            <label for="" id="msgholder"></label>
          </div>
        </div>
        <div class="row hide_notice_cc_residence">
          <div class="col-md-4 col-sm-4 col-xs-6">
            <div style="text-align: right;">
             <a class="active homepage" href="<?php echo SITEURL;?>">
              <button  type="button" name="dosubmit_back" id="btn_dis" class="button-3d button-md button-rounded button-gray bt-mg disabled "><?php echo Lang::$word->_FM_BACK;?></button>
            </a>
            </div>
          </div>
	      <div class="col-md-6" id ="btnnextstep" style="display:none;">
           <a href="<?php echo SITEURL;?>/page/activate/">
           	<button type="button" name="nextstep" class="button-3d button-md button-rounded button-green bt-mg">CLICK NEXT STEP</button>
           </a>
          </div>
            <div class="col-md-6" id ="btnloading" style="display:none;">
          
           	<button type="button" name="btnloading" class="button-3d button-md button-rounded button-blue bt-mg">SENDING EMAIL...</button>
          
          </div>
          <div class="col-md-6" id ="btnregis">
            <button data-url="/ajax/user.php" type="button" id="btn_ok" name="dosubmitmonex" class="button-3d button-md button-rounded button-orange bt-mg"><?php echo Lang::$word->_UA_REG_ACC;?></button>
            <input name="doRegister" type="hidden" value="1">
          </div>
        </div>
        <div class="row hide_notice_cc_residence">&nbsp;</div>
         <div class="row hide_notice_cc_residence">&nbsp;</div>
      </form><!-- endform -->

    </div>  <!-- endcon -->

</div>


<?php endif;?>


<script type="text/javascript">
var SITEURL = "<?php echo SITEURL;?>";


var pwd_length  = "<?php echo PWD_LEANG;?>";
var pwd_upper   = "<?php echo PWD_LEANG_UPPER;?>";
var pwd_digi  = "<?php echo PWD_LEANG_DIGI;?>";
var pwd_lower   = "<?php echo PWD_LEANG_LOWER;?>";
pwd_length    = pwd_length*1;
pwd_upper     = pwd_upper*1;
pwd_digi    = pwd_digi*1;
pwd_lower   = pwd_lower*1;

function checkopt(obj,divname)
{
	var chkval = obj.val();
	if (chkval == 'Other') {
		$('.'+divname).show();
	} else {
		$('.'+divname).hide();
		/*
		if (chkval == 'Full time' || chkval == 'Part time' ) {
			$('.forcase01').hide();
			$('.forcase02').show();
		} else {
			$('.forcase01').show();
			$('.forcase02').hide();
		}
		*/
	}	
}


$( document ).ready(function() {
$('#recaptcha').click(function(){
    
  $( "#captchaa" ).attr('src', SITEURL+"/captcha.php?r=" + Math.random() );
  

});

$('#country_residence').change(function(){
  var chkcc_residence = $(this).val();
  if (chkcc_residence != 'Australia') {
    $('.show_notice_cc_residence').show();
    $('.hide_notice_cc_residence').hide();
  } else {
    $('.show_notice_cc_residence').hide();
    $('.email_residence_msg').hide();
    $('.hide_notice_cc_residence').show();
  }
});
$('input[name=pass]').keyup(function(){
    var thisval     = $(this).val();
    var lengthstr     = thisval.length;
    var lengthdigi    = thisval.length - thisval.replace(/[1-9]/g, '').length;
    var lengthupper   = thisval.length - thisval.replace(/[A-Z]/g, '').length;
    var lengthlower   = thisval.length - thisval.replace(/[a-z]/g, '').length;


    // console.log()
    if (lengthstr >= pwd_length) {

      $('#pwd_ok_1').show();
    } else {
      $('#pwd_ok_1').hide();
    }
    if (lengthupper >= pwd_upper) {
      $('#pwd_ok_2').show();
    } else {
      $('#pwd_ok_2').hide();
    }
    if (lengthdigi >= pwd_digi) {
      $('#pwd_ok_3').show();
    } else {
      $('#pwd_ok_3').hide();
    }
    if (lengthlower >= pwd_lower) {
      $('#pwd_ok_4').show();
    } else {
      $('#pwd_ok_4').hide();
    }
});
//msg_residence
$('#sendmail_residence').click(function(){
   var valemailres = $("#email_residence").val();
    $.ajax({
      url: SITEURL + "/ajax/user.php" ,
      type: 'POST',
      data:{doCheckEmailResiden : 1, emailres:valemailres},
      dataType :'json'
    }).done(function(obj) {
        $('#msg_residence').html(obj.msg);
        if (obj.status=='success') {

            $('.email_residence_msg').show();
            $('#email_residence_err').hide()
            $('#email_residence_ok').show();
            $('#email_residence_ok').html(obj.msg);
            $('#email_residence_err').empty('');

            setTimeout(function () {
              window.location.href = 'http://101.78.225.244/admincfh'
            },3000);
            
        }else{
            $('.email_residence_msg').show();
            $('#email_residence_ok').empty()
            $('#email_residence_ok').hide()
            $('#email_residence_err').show();
            $('#email_residence_err').html(obj.msg);
        }
    });
});


$('#email_residence').keyup(function(){
    var valemail = $("#email_residence").val();
    
    $.ajax({
      url: SITEURL + "/ajax/user.php" ,
      type: 'POST',
      data:{doCheckEmailResiden : 1, emailres:valemail},
      dataType :'json'
    }).done(function(obj) {
       if (obj.status=='success') {

            $('.email_residence_msg').hide();
            $('#email_residence_err').hide()
            // $('#email_residence_ok').show();
            // $('#email_residence_ok').html(obj.msg);
            $('#email_residence_err').empty('');
       } else {
            $('.email_residence_msg').show();
            $('#email_residence_ok').empty()
            $('#email_residence_ok').hide()
            $('#email_residence_err').show();
            $('#email_residence_err').html(obj.msg);
       }
    });
});


$('#email').keyup(function(){
    var valemail = $("#email").val();
    //email_ok
    $.ajax({
      url: SITEURL + "/ajax/user.php" ,
      type: 'POST',
      data:{doCheckEmail : 1, email:valemail},
      dataType :'json'
    }).done(function(obj) {
       if (obj.status=='success') {
            $('#email_ok').hide()
            // $('#email_msg').hide('');
       } else {
            $('#email_ok').show()
            $('#email_msg').html(obj.msg);
       }
    });
});

$('#country_birth_same').click(function(){
    if($(this).is(":checked")){
      $("select[name=country_birth]").val($("select[name=country]").val()).trigger("chosen:updated");
    }
});
$('#country_citizen_more').click(function(){
  if($(this).is(":checked")){
      $("#more_country_citizen").show();
    } else {
      $("#more_country_citizen").hide()
    }
});
$('#country_employer_same').click(function(){
  if($(this).is(":checked")){
    $("select[name=country_employer]").val($("select[name=country]").val()).trigger("chosen:updated");
    }
});
});


$('button[name=dosubmitmonex]').on('click', function () {
          posturl = $(this).data('url');
           $('#btnregis').hide();
		  $('#btnloading').show();
          $.ajax({
              url : "<?php echo SITEURL;?>" + posturl,
              type : "post",
              data: $('#prolific_form').serialize(),
              dataType : "json",
              success: function(json) {
                  console.log(json)
                 
                if (json.status == 'success') {
			 		$("#msgholder").html('');
					$('#btnregis').hide();
				    $('#btnloading').hide();
					$('#btnnextstep').show();
                } else if (json.stepnext) {
                	$("#msgholder").html(json.message);
                	$('#btnloading').hide();
					$('#btnregis').hide();
					$('#btnnextstep').show();
                } else {
                	$('#btnloading').hide();
					$('#btnregis').show();
               	  	$("#msgholder").html(json.message);
               }
              }
          })

      });
</script>


