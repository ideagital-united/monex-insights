<?php
 /**
  * Header
  *
  * @package CMS Pro
  * @author prolificscripts.com
  * @copyright 2014
  * @version $Id: header.php, v4.00 2014-04-20 10:12:05 gewa Exp $
  */
 if (!defined("_VALID_PHP"))
     die('Direct access to this location is not allowed.');
?>
<!doctype html>
<head>
		<?php echo $content->getMeta(); ?>
		
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/core/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/core/animate.min.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/main/main.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/main/setting.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/main/hover.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/magnific/magic.min.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/magnific/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/magnific/magnific-popup-zoom-gallery.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/owl-carousel/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/color/green.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/icon/font-awesome.css">
        <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/icon/et-line-font.css">
        <!-- <link rel="stylesheet" href="<?php echo THEMEURL;?>/assets/css/main/app.css">  -->


<link rel="stylesheet" href="<?php echo PROTOCOL_HTTP;?>://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.css">
<script src="<?php echo PROTOCOL_HTTP;?>://code.jquery.com/jquery-1.11.3.min.js"></script>
<style type="text/css">
 .register-modal{
    width: 350px;
    top:100px;
}

.nav-boderno{
      border-bottom: 0px !important;

}


</style>
<script type="text/javascript">
var SITEURL = "<?php echo SITEURL; ?>";
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DWQBSC');</script>
<!-- End Google Tag Manager -->

</head>
<body  id="page-top" data-spy="scroll" data-target=".navbar" data-offset="100">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DWQBSC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

        <a href="#page-top" class="go-to-top">
            <i class="fa fa-long-arrow-up"></i>
        </a>


        <!-- Navigation Area
        ===================================== -->
        <nav class="navbar navbar-fixed-top">
            <div class="top-navbar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <ul class="list list-inline">
                                <li>
					<?php if($user->logged_in){?>
                                	<a class="item" href="<?php echo doUrl(false, $core->account_page, "page");?>"><i class="icon danger user"></i> <b><?php echo Lang::$word->_WELCOME;?>: <?php echo $user->username;?>!</b></a>
                                	<?php }else{ ?>
                                	<small>Welcome: Guest!</small>
                                	<?php } ?>
					</li>
                            </ul>
                            <ul class="list list-inline">
                                <li><a href="<?php echo SITEURL;?>/page/our-contact-info"><i class="fa fa-phone text-primary mr5" aria-hidden="true"></i>CONTACT US</a></li>
                                <!-- <li><a href="#">LOGOUT</a></li> -->
                                <?php if($user->logged_in){?>
                                 <li><a class="item" href="<?php echo SITEURL;?>/logout.php"><?php echo Lang::$word->_N_LOGOUT;?></a></li>
                                <?php }else{ ?>
                                <li><a href="<?php echo SITEURL;?>/page/before-register"  class="text-primary">SIGN UP</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-sm-3">
                            <ul class="list list-inline top-navbar-right">
                                <li>
                                    <a href="#">
                                        <div class="list-icon">
                                            <img src="<?php echo THEMEURL;?>/assets/media/icons/uk-flag.png" alt="UK">
                                        </div>
                                        <small>LANGUAGE</small>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="<?php echo SITEURL;?>">
                                    <img src="<?php echo SITEURL;?>/uploads/monex-b.png" alt="logo" width="150px">
                                </a>
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse navbar-right">
                         <?php 
                                	$mainmenu = $content->getMenuList(); 
                                	$content->getMenu($mainmenu,0, "menu", "nav navbar-nav");
                                	?>
<!--
				       <ul class="nav navbar-nav">
                                    <li class="hidden"><a href="#page-top"></a></li>
                                    <li><a href="<?php echo SITEURL;?>">HOME</a></li>
                                    <li><a href="#portfolioGrid">WHY MONEX</a></li>
                                    <li><a href="#service">BROKERAGE FEES</a></li>
                                    <li><a href="<?php echo SITEURL;?>/page/about-freedomcfd">ABOUT US</a></li>
                                    <li><a href="<?php echo SITEURL;?>/page/our-contact-info">CONTACT US</a></li>
                           <?php if(!$user->logged_in){?>
			             <li><a href="<?php echo SITEURL;?>/page/login"><i class="fa fa-lock fa-fw"></i>Login</a></li>
			 <?php }?>
                                </ul>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </nav>

