<?php

  /**

   * Right Sidebar Layout

   *

   * @package CMS Pro

   * @author prolificscripts.com

   * @copyright 2014

   * @version $Id: right.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $

   */

  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>


  <?php if($content->getAccess() && $sql = Content::getContentPlugins($row->{'body' . Lang::$lang}) != '' || $page = Content::getAccesPages($row) != ''){?>
       <div id="page" class="bg-gray pt100 pb100" style="padding-bottom:0px;">

            <div class="prolific-grid">
              <div class="prolific-content-full clearfix">
                <?php echo Content::getContentPlugins($row->{'body' . Lang::$lang});?>

                <?php if($page = Content::getAccesPages($row)){?>
                   <?php include($page);?>
                <?php } ?>
            </div>

          </div>
        </div>

  <?php } ?>

  <?php if($content->getAccess() != '' && $sql == '' && $page == ''){?>
  <!-- home2 -->
  
    <?php
    include 'home2.tpl.php'; 
    ?>

  <?php } ?>

  <!-- Bottom Widgets -->

<div id="botwidget">

  <div class="prolific-grid">

    <?php 
    include(THEMEDIR . "/bottom_widget.tpl.php");
    ?>

  </div>

</div>

<!-- Bottom Widgets /-->
