<?php

  /**

   * Contact Form

   *

   * @package CMS Pro

   * @author prolificscripts.com

   * @copyright 2014

   * @version $Id: contact_form.php, v4.00 2014-04-20 10:12:05 gewa Exp $

   */

  if (!defined("_VALID_PHP"))

      die('Direct access to this location is not allowed.');

?>
<style media="screen">
  .mg-w{
    margin-top: 15px;
  }
</style>

<div class="container-fluid pt40 pb40 bb-dashed-1 inner cover text-center animated" data-animation="fadeIn" data-animation-delay="100">
  <div class="row">
    <div class="container">
      <div class="row">
              <h1 class="font-size-normal">
                <div class="col-md-12 text-center">
                  <h1 class="font-size-normal">
                    Contact us
                    <small class="heading heading-solid center-block"></small>
                  </h1>
                </div>
                <div class="col-md-8 col-md-offset-2 text-center">
                  <p class="lead">
                      Looking for help on something related ? Let me know. I'll get back to you as soon as I can. Thanks!
                  </p>
                </div>
              </h1>
            </div>
             <div class="col-md-6 col-md-offset-3 ">
             	<div id="msgholder"></div>
             </div>
            <div class="col-md-6 col-md-offset-3 ">
            <div class="loader " 
            style="display: none; width: 100%; height: 100%; background-image: none; background-repeat: repeat; 
            background-attachment: scroll; background-clip: border-box; background-origin: padding-box; background-position: 0% 0%; background-size: auto auto; position: absolute; z-index: 999999999; background-color: rgb(245, 247, 249) !important;">
            	<img src="<?php echo SITEURL;?>/uploads/loader-large.gif"></div>
              <h4 class="text-center mb20"></h4>
              <form id="prolific_form" name="prolific_form" method="post" class="form-horizontal" >
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label"><?php echo Lang::$word->_CF_NAME;?></label>
                  <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="<?php echo Lang::$word->_CF_NAME;?>" value="<?php if ($user->logged_in) echo $user->name;?>" name="name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label"><?php echo Lang::$word->_CF_EMAIL;?>*</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control"  placeholder="<?php echo Lang::$word->_CF_EMAIL;?>" value="<?php if ($user->logged_in) echo $user->email;?>" name="email">

                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label"><?php echo Lang::$word->_CF_PHONE;?>*</label>
                  <div class="col-sm-9">
                            <input type="text" class="form-control"  placeholder="<?php echo Lang::$word->_CF_PHONE;?>" name="phone">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label"><?php echo Lang::$word->_CF_SUBJECT;?>*</label>
                  <div class="col-sm-9">
                    <select name="subject"  class="form-control" >

                      <option value=""><?php echo Lang::$word->_CF_SUBJECT_1;?></option>

                      <option value="General Enquiries on Our Services">General Enquiries on Our Services</option>

                      <option value="Technical Issue">Technical Issue</option>

                      <option value="Complaint">Complaint</option>

                      <option value="New Business with Us">New Business with Us</option>

                      <option value="Other">Other</option>


                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label"><?php echo Lang::$word->_CF_MSG;?>*</label>
                  <div class="col-sm-9">
                      <textarea placeholder="<?php echo Lang::$word->_CF_MSG;?>"  class="form-control" name="message"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-3 control-label"><?php echo Lang::$word->_CF_TOTAL;?>  <label class="input"><img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-append" /> <i class="icon-prepend icon unhide"></i></label></label>
                  <div class="col-sm-9">
                      <input  class="mg-w form-control" type="text" name="code">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-8">
                    <div class="content-right">
                      <button data-url="/ajax/sendmail.php" type="button" name="dosubmit"  class="button button-sm button-green"><?php echo Lang::$word->_CF_SEND;?></button>
                      <input name="processContact" type="hidden" value="1">
                    </div>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
  </div>
</div>
<!-- <div class="prolific form secondary segment">

  <form id="prolific_form" name="prolific_form" method="post">

    <div class="two fields">

      <div class="field">

        <label><?php echo Lang::$word->_CF_NAME;?></label>

        <label class="input"><i class="icon-append icon asterisk"></i>

          <input type="text" placeholder="<?php echo Lang::$word->_CF_NAME;?>" value="<?php if ($user->logged_in) echo $user->name;?>" name="name">

        </label>

      </div>

      <div class="field">

        <label><?php echo Lang::$word->_CF_EMAIL;?></label>

        <label class="input"><i class="icon-append icon asterisk"></i>

          <input type="text" placeholder="<?php echo Lang::$word->_CF_EMAIL;?>" value="<?php if ($user->logged_in) echo $user->email;?>" name="email">

        </label>

      </div>

    </div>

    <div class="two fields">

      <div class="field">

        <label><?php echo Lang::$word->_CF_PHONE;?></label>

        <label class="input">

          <input type="text" placeholder="<?php echo Lang::$word->_CF_PHONE;?>" name="phone">

        </label>

      </div>

      <div class="field">

        <label><?php echo Lang::$word->_CF_SUBJECT;?></label>

        <select name="subject">

          <option value=""><?php echo Lang::$word->_CF_SUBJECT_1;?></option>

          <option value="<?php echo Lang::$word->_CF_SUBJECT_2;?>"><?php echo Lang::$word->_CF_SUBJECT_2;?></option>

          <option value="<?php echo Lang::$word->_CF_SUBJECT_3;?>"><?php echo Lang::$word->_CF_SUBJECT_3;?></option>

          <option value="<?php echo Lang::$word->_CF_SUBJECT_4;?>"><?php echo Lang::$word->_CF_SUBJECT_4;?></option>

          <option value="<?php echo Lang::$word->_CF_SUBJECT_5;?>"><?php echo Lang::$word->_CF_SUBJECT_5;?></option>

          <option value="<?php echo Lang::$word->_CF_SUBJECT_6;?>"><?php echo Lang::$word->_CF_SUBJECT_6;?></option>

          <option value="<?php echo Lang::$word->_CF_SUBJECT_7;?>"><?php echo Lang::$word->_CF_SUBJECT_7;?></option>

        </select>

      </div>

    </div>

    <div class="field">

      <label><?php echo Lang::$word->_CF_MSG;?></label>

      <label class="textarea"><i class="icon-append icon asterisk"></i>

        <textarea placeholder="<?php echo Lang::$word->_CF_MSG;?>" name="message"></textarea>

      </label>

    </div>

    <div class="field">

      <label><?php echo Lang::$word->_CF_TOTAL;?></label>

      <label class="input"><img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-append" /> <i class="icon-prepend icon unhide"></i>

        <input type="text" name="code">

      </label>

    </div>

    <button data-url="/ajax/sendmail.php" type="button" name="dosubmit" class="prolific danger button"><?php echo Lang::$word->_CF_SEND;?></button>

    <input name="processContact" type="hidden" value="1">

  </form>

</div> -->

<!-- <div id="msgholder"></div> -->

