<?php

  /**

   * Index

   *

   * @package CMS Pro

   * @author prolificscripts.com

   * @copyright 2014

   * @version $Id: index.php, v4.00 2014-04-20 10:12:05 gewa Exp $

   */

  if (!defined("_VALID_PHP"))

      die('Direct access to this location is not allowed.');
?>

<?php include("header.tpl.php");?>

<!-- Full Layout -->
<?php include(THEMEDIR . "/full.tpl.php");?>
<!-- Full Layout /-->

<?php include("footer.tpl.php");?>

