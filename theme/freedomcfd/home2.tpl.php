
   <header class="intro parallax-window" data-parallax="scroll" data-speed="0.5" data-image-src="<?php echo THEMEURL;?>/assets/media/bg.png">
            <div class="intro-body">
               
                <div class="container">                    
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="brand-heading text-left text-capitalize mt100 animated" data-animation="fadeInUp" data-animation-delay="100">
                                <span class="color-light">Meet your new share<br>broker</span><span style="color:#aaddd8;"> with a difference</span>
                            </h1>
                            <h4 class="lead text-left color-light mt50 animated" data-animation="fadeInUp" data-animation-delay="100" >
                                <span>Ready for access to international markets a low cost? You’re too focussed on Australian stocks, the ‘Home Country Bias’ is a well-documented problem and Australians suffer more than most other nationalities. Finally, with one account, you can access listed securities in 12 international markets including Australia, USA and much of Asia (including China).</span>
                                <br><br>
                                <span class="color-green mt20">Find out why 1.8 M traders worldwide (with over USD 39 Billion in <?php echo THEMEURL;?>/assets) can’t be wrong.  </span>
                            </h4>
                            <p class="intro-text-big text-left color-light mt30 animated" data-animation="fadeInUp" data-animation-delay="200">
                                <a href="#" class="button button-md button-circle button-green button-md">FIND OUT MORE</a>
                            </p>
                            
                       </div>
                    </div>
                </div>
                
                <div class="intro-direction">
                    <a href="#welcome">
                        <div class="mouse-icon"><div class="wheel"></div></div>
                    </a>
                </div>
                
            </div>
        </header>

        <!-- Info Area
        ===================================== -->
        <div id="info-1" class="pb40 parallax-25" data-parallax="scroll" data-speed="0.5" data-image-src="<?php echo THEMEURL;?>/assets/media/bg2.png">
            <div class="container">
                <div class="row pt40">
                    <div class="col-md-12 text-center">
                        <h4 class="color-light">
                           Welcome to MONEX, the place where smart money people can find access to a truly<br>
                           international equities market at a fair price, to help un-lock global investment opportunities<br>
                           and manage risk through diversification to grow their wealth. 
                        </h4>
                    </div>   
                </div>
            </div>
        </div>
        
        <!-- General Content Area
        ===================================== -->
        <div class="pt100 pb100" style="background: url(<?php echo THEMEURL;?>/assets/media/bg3.png) 50% 30% no-repeat;">
            <div class="container">
                <div class="row">
                   
                    <!-- left content start -->
                    <div class="col-md-6 pb100">
                        <div class="general-content">
                            <h1 class="mb25 color-green">
                                Who are our clients?
                            </h1>
                            <h4>Our clients requiring global access to share at a low cost include:</h4>
                            <h4><i class="fa fa-angle-right color-green pr20"></i> Existing & Professional Traders</h4>
                            <h4><i class="fa fa-angle-right color-green pr20"></i> Institutional Partners - Asset Managers & Brokers</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!-- left content end -->
                    
                </div>
            </div>
        </div>

         <!-- General Content Area
        ===================================== -->
        <div id="general-content-1" class="parallax-window-3" data-parallax="scroll" data-speed="0.2" data-image-src="<?php echo THEMEURL;?>/assets/media/bg4.png">
            <div class="container pb100">
                <div class="row">
                    
                    <!-- left content start -->
                    <div class="col-md-6"></div>
                    <!-- left content end -->
        
                    <!-- right content start -->
                    <div class="col-md-5 col-md-push-1 pt100 animated" data-animation="fadeInUp" data-animation-delay="150">
                        <div class="general-content">
                            <h1 class="mb25">How we do it?</h1>
                            <h5><i class="fa fa-angle-right color-green pr20"></i>By leveraging the accumulated strengths of our global network of companies -  Monex Inc, TradeStation and BOOM, we have tapped into their in-house back-end system allowing us to access the lower execution costs to pass the savings on to you.</h5>
                            <h5><i class="fa fa-angle-right color-green pr20"></i>We created Monex Australia to fill a gap in the market that clearly needed to be filled. We give you access to a world of more than 40,000 shares and listed securities that otherwise cannot be reached from a single, low cost trading platform.
                            </h5>
                
                        </div>
                    </div>
                    <!-- right content end -->
                </div>
                
            </div>
             <div class="row pt25" style="background-color: rgba(66, 110, 107, 0.4);">
                <div class="container">
                    <div class="col-md-6 text-center animated"  data-animation="fadeInLeft" data-animation-delay="200">
                        <h1 class="color-light">Diversify your portfolio today</h1>
                    </div> 
                    <div class="col-md-6">
                         <p class="intro-text-big text-center color-light pt25 pb25 animated" data-animation="fadeInRight" data-animation-delay="200">
                                <a href="#" class="button button-lg button-circle button-green button-md">OPEN AN ACCOUNT</a>
                         </p>
                    </div> 
                </div>
            </div>
        </div>
        <div class="bg-gray pt50 pb100">
            <div class="container">
                <div class="row">
                    <h1 class="text-center pb50 color-green">                          
                        What our clients are saying
                    </h1>   
                </div>
                
                <div class="row">
                 
                    <div class="col-md-4 col-sm-4 col-xs-12 hover-wobble-vertical">                    
                        <div class="team team-one"  style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"> 
                            <img src="<?php echo THEMEURL;?>/assets/media/profile1.png" class="img-responsive">
                            <h4 class="color-green">One excellent broker</h4> 
                            <h5 class="m25">Monex – has an online platform that allows you to trade most Asian markets seamlessly in real time, right from a computer  ...through a multicurrency account, giving you the best prices and execution in real time on these exchanges.</h5>
                            <small>Karim</small>
                        </div>                    
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 hover-wobble-vertical">                    
                         <div class="team team-one"  style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">                 
                            <img src="<?php echo THEMEURL;?>/assets/media/profile2.png" class="img-responsive">
                            <h4 class="color-green">Excellent</h4> 
                            <h5 class="m25">I have to say that every single interaction with this platform is just a pleasure - I really appreciated your flexibility and your focus on getting things done in the most customer friendly manner!!!! you are quite a role model for the financial services industry!! </h5>
                            <small>Tom</small>
                        </div>                    
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 hover-wobble-vertical">                    
                         <div class="team team-one"  style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">        
                            <img src="<?php echo THEMEURL;?>/assets/media/profile3.png" class="img-responsive">
                            <h4 class="color-green">Superb</h4> 
                            <h5 class="m25">I have experience of 3 other platforms. One was good, one mediocre, and one terrible! This platform is far and away the easiest, simplest to understand, and the very best for efficiency and service: superb. </h5>
                            <small class="pt25">Janine</small>
                        </div>                    
                    </div>
                    
                    
                </div>

             </div>
        </div>
        <div class="pt100 pb100 bg-black">
            <div class="container">
                <div class="row">
                    <h2 class="text-center color-light">Super Competitive Pricing</h2>
                    <img src="<?php echo THEMEURL;?>/assets/media/bg5.png" width="100%">
                </div>
            </div>
        </div>

        <div class="pt50 pb50">
            <div class="container">
                <div class="row">
                    <h1 class="text-center color-green pb50 animated" data-animation="fadeInUp" data-animation-delay="100">If you have only invested in Australia then your <?php echo THEMEURL;?>/assets are in basket of less than 3% of global market caps.</h1>
                    <img src="<?php echo THEMEURL;?>/assets/media/chart.png" width="100%" class="img-responsive pr50 pl50 pb25">
                    <h4 class="text-center animated"  data-animation="fadeInUp" data-animation-delay="100">Source: Based on Vanguard Research 2016</h4>
                    <p class="intro-text-big text-center color-light pt25 pb25 animated" data-animation="fadeInUp" data-animation-delay="150">
                         <a href="#" class="button button-lg button-circle button-green button-md">OPEN AN ACCOUNT</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="pt100 pb100 bg-gray">
            <div class="container">
                <div class="row mb40 pb20">
                    <h1 class="text-center color-green animated" data-animation="fadeInUp" data-animation-delay="100">Why trade with Monex?</h1>
                    <h3 class="text-center">When you trade with Monex, you get all this and much more...</h3>
                </div>

                <div class="row">
                    <!-- Content Box Center Icon Circle with Background-->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon1.png" width="120px">
                            <h4 class="color-green">Multi-Market ETX<br>Stock Trading               
                            <p>We give access to exchange listed securities in 12 Countries across Asia and the Unites States, which covers more than 70%* of world market value.</p>
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon2.png" width="120px">
                            <h4 class="color-green">Competitive<br>Pricing</h4>               
                            <p class="pr10 pl10">You can enjoy great value of our service at a low commission rate.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon3.png" width="120px">
                            <h4 class="color-green">Powerful<br>Order Types</h4>               
                            <p class="pr10 pl10">We provide a collection of powerful online trading order types to fit your trading strategy. Simply pre-set your orders and let our system do the work for you.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon4.png" width="120px">
                            <h4 class="color-green">Multi-market<br>Watchlist</h4>               
                            <p class="pr10 pl10">Watchlist is a perfect tool to help you keep a close eye on your global investments. Add stocks across 15 stock exchanges to one watchlist.</p>
                        </div>
                    </div>                         
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon5.png" width="120px">
                            <h4 class="color-green">Integrated Account<br>Management</h4>               
                            <p class="pr10 pl10">Manage your investments across markets under one account number.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon6.png" width="120px">
                            <h4 class="color-green">Complete<br>Web Access</h4>               
                            <p class="pr10 pl10">Simply place all instructions over an Internet browser. No need to download any software and apps.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon7.png" width="120px">
                            <h4 class="color-green">Real-time<br>Account Updates</h4>               
                            <p class="pr10 pl10">Instant online updates for order status, portfolio holdings and funds available.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="content-box content-box-center content-box-icon-circle">
                            <img src="<?php echo THEMEURL;?>/assets/media/icon7.png" width="120px">
                            <h4 class="color-green">Mobile Friendly<br>Solutions</h4>               
                            <p class="pr10 pl10">Stay connected wherever you go with our mobile apps.</p>
                        </div>
                    </div>                         
                </div>
            </div>
        </div>
        <div class="pt50 pb50">
            <div class="container">
                <div class="row">
                    <h1 class="text-center color-green animated" data-animation="fadeInUp" data-animation-delay="100">Who is Monex?</h1>
                    <h4 class="text-center animated" data-animation="fadeInUp" data-animation-delay="100">...and why should you trust us to do business with?</h4>
                    <img src="<?php echo THEMEURL;?>/assets/media/map.png" width="100%" class="img-responsive">
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">                        
                            <i class="fa fa-angle-right color-green"></i>          
                            <p>Monex Group is an online brokerage business with 1.8 million clients worldwide with over USD 39 billion <?php echo THEMEURL;?>/assets under custody as a group.</p>
                        </div>
                        <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">                      
                            <i class="fa fa-angle-right color-green"></i>          
                            <p>Founded in 1999, the Monex Group is an online brokerage business with over 830 employees through 12 offices worldwide, who are passionate about one thing only –  to give our customers unmatched access to international share markets at reasonable costs.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">                       
                            <i class="fa fa-angle-right color-green"></i>          
                            <p>Reliability and ‘Above all Integrity’ in everything we do.  This simply means that we say what we do and do what we say.</p>
                        </div>
                        <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">                        
                            <i class="fa fa-angle-right color-green"></i>          
                            <p>Monex offers you cutting edge industry first digital applications and technology that allow you to trade in a way that it streamlines to your way and your personal trading devices.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">                        
                            <i class="fa fa-angle-right color-green"></i>          
                            <p>Quality service and client satisfaction are inherent in all that we do. Our service commitment delivers on the promise of a private, secure, fast and inexpensive international stock trading experience. </p>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
        <style type="text/css">
            .form-rounded-green {
                height: 44px;
                border-radius: 22px;
                padding: 5px 20px;
                font-weight: 300;
                font-size: 14px;
                background-color: #aab33b;
                box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.1), 0 6px 10px 0 rgba(0, 0, 0, 0.19);
                border: none;
                color: #fff;

            }
            .form-rounded-green.input-lg{

            }
            .button-o.button-light {
                background-color: #fff !important;
                color: #aab33b;
                padding: 12px 30px;
            }
        </style>
         <div id="info-1" class="pb40 parallax-25" data-parallax="scroll" data-speed="0.5" data-image-src="<?php echo THEMEURL;?>/assets/media/bg2.png">
            <div class="container">
                <div class="row pt40">
                    <div class="col-md-12 text-center">
                        <h2 class="color-light">
                           Yes I want to diversify my portfolio
                        </h2>
                        <h3 class="color-dask">
                           Find out how easy it is to start investing in international stocks at a low cost.  
                        </h3>

                    </div>  
                    <div class="col-md-8 col-md-offset-2" >
                        <div class="col-md-6 pt25">
                            <input class="form-control form-rounded-green" value="First Name*">
                        </div>
                        <div class="col-md-6 pt25">
                            <input class="form-control form-rounded-green" value="Last Name">
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-2" >
                        <div class="col-md-6 pt25">
                            <input class="form-control form-rounded-green" value="Email*">
                        </div>
                        <div class="col-md-6 pt25">
                            <input class="form-control form-rounded-green" value="Phone Number">
                        </div>
                    </div>
                    <div class="col-md-12 text-center pt50">
                        <button class="button-o button-light button-lg button-circle">Yes, I want to know more</button>   
                    </div>
                </div>
            </div>
        </div>



