<?php
  /**
   * Register Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: register.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  if ($user->logged_in)
      redirect_to($core->account_page);
?>
<?php if(!$core->reg_allowed):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_NOMORE_REG);?>
<?php elseif($core->user_limit !=0 and $core->user_limit == countEntries("users")):?>
<?php echo Filter::msgSingleAlert(Lang::$word->_UA_MAX_LIMIT);?>
<?php else:?>
    
  
 
<?php
function genOptionGroup($group,$isselect='',$val=''){
	if($group && $group != ''){
		$aG = explode(",",$group);
		return '<option value="'.$val.'" lev="'.$aG[2].'"  '.$isselect.'>'.$aG[1].'</option>';
	} else {
		return '';
	}
}



$adminConf 	= file_get_contents("http://65.60.60.131/~api2rfforex/product_control/script/");	
$aConf 		=  json_decode($adminConf,true);
$is_mt4  	= (issetvalue($aConf['mt4_api_url']) != '') 	? 1 : 0;
$is_ct 		= (issetvalue($aConf['ct_api_url']) != '') 	? 1 : 0;

$mt4_live_group_micro 		= (isset($_REQUEST['groupt']) && $_REQUEST['groupt'] == 'mt4micro') ? " selected " : "";
$mt4_live_group_mimi 		= (isset($_REQUEST['groupt']) && $_REQUEST['groupt'] == 'mt4mini') ? " selected " : "";
$mt4_live_group_standard 	= (isset($_REQUEST['groupt']) && $_REQUEST['groupt'] == 'mt4standard') ? " selected " : "";
$mt4_live_group_pro 		= (isset($_REQUEST['groupt']) && $_REQUEST['groupt'] == 'mt4pro') ? " selected " : "";

$mt4_demo_group 			= (isset($_REQUEST['groupt']) && $_REQUEST['groupt'] == 'mt4demo') ? " selected " : "";
?>

<p class="content-center"><i class="information icon"></i> <?php echo Lang::$word->_UA_INFO4. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></p>
<div class="columns">
  <div class="screen-60 phone-100 push-center">
    <div class="prolific form secondary segment">
      <form id="prolific_form" name="prolific_form" method="post">
        <h3><?php echo Lang::$word->_UA_SUBTITLE4;?></h3>
        
         <?php if(isset($_COOKIE['ref_name'])) {?>
          <div class="field">
          <label>Recommended Partners</label>
          <label class="input">
            <?php
         
            require_once (MODPATH . "tradersystem/admin_class.php");
           Registry::set('TraderSystem', new TraderSystem());
            $aGetRefName = Registry::get("TraderSystem")->call_curl_get("check_ref_uid", "GET",$_COOKIE['ref_name']);
           
            // print_r($aGetRefName);
            if($aGetRefName['uid']>0){
               // $aGetRefName = json_decode(json_encode($aGetRefName), true);
                echo $_COOKIE['ref_name']." <font color='green'><i class='check icon'></i> <b>Available referrer name</b></font>";    
              echo "<input type='hidden' name='ref_uid' value='".$aGetRefName['uid']."'>";           
            } else {
                echo "<font color='red'><i class='remove icon'></i> <b>Not found this referrer name.</b></font>";
            }
            ?>
          </label>
        </div>
        <?php } else {?>
          <?php // if(IB == 1) {?>  
        <div class="two fields">
          <div class="field">
          <label>Recommended Partners</label>
          <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>
              <input name="referal_name" id="referal_name" value="<? echo $_COOKIE['ref_name']; ?>" placeholder="Recommended Partners Name" type="text">
           </label>
           </div>
           
           <div class="field">
              <label>&nbsp;</label>
              <label class="input">
                <span id="res_chk_ref_name"></span>  
              </label>
           </div>
        </div>
        
        <div class="field">
            <label class="input">
            <button class="prolific info button" id="chk_ref_name"name="chk_ref_name" type="button" data-url="/ajax/user.php">Check Referral Name</button>
            </label>
        </div>
        <?php // } ?>
        <?php } ?>
        
        
        <div class="two fields">
            <div class="field">
              <label><?php echo Lang::$word->_UR_EMAIL; // echo Lang::$word->_USERNAME; ?></label>
              <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>
                <input name="email" id="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" type="text">
              </label>
            </div>
            
         <!--   <div class="field">
                  <label>&nbsp;</label>
                  <label class="input">
                    <span id="res_chk_email">Please input your email and click Check Email button</span>  
                  </label>
            </div> -->
        </div>
        
       <!-- <div class="field">
            <label class="input">
            <button class="prolific info button" id="chk_email" name="chk_email" type="button" data-url="/ajax/user.php">Check Email</button>
            </label>
        </div> -->
        
        
        <div class="two fields">
          <div class="field">
            <label><?php echo Lang::$word->_PASSWORD;?></label>
            <label class="input"><i class="icon-prepend icon lock"></i> <i class="icon-append icon asterisk"></i>
              <input name="pass" placeholder="<?php echo Lang::$word->_PASSWORD;?>" type="password">
            </label>
          </div>
          <div class="field">
            <label><?php echo Lang::$word->_UA_PASSWORD2;?></label>
            <label class="input"><i class="icon-prepend icon lock"></i><i class="icon-append icon asterisk"></i>
              <input name="pass2" placeholder="<?php echo Lang::$word->_UA_PASSWORD2;?>" type="password">
            </label>
          </div>
        </div>
     <!--   <div class="field" style="display: none">
          <label><?php echo Lang::$word->_UR_EMAIL;?></label>
          <label class="input"><i class="icon-prepend icon mail"></i><i class="icon-append icon asterisk"></i>
            <input name="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" type="text">
          </label>
        </div> -->
        <div class="two fields">
          <div class="field">
            <label><?php echo Lang::$word->_UR_FNAME;?></label>
            <label class="input"><i class="icon-prepend icon user"></i> <i class="icon-append icon asterisk"></i>
              <input name="fname" placeholder="<?php echo Lang::$word->_UR_FNAME;?>" type="text">
            </label>
          </div>
          <div class="field">
            <label><?php echo Lang::$word->_UR_LNAME;?></label>
            <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>
              <input name="lname" placeholder="<?php echo Lang::$word->_UR_LNAME;?>" type="text">
            </label>
          </div>
        </div>
        <?php
        $is_mt4 = 3;
        if ($is_mt4 == 1) {
        ?>
         <div class="fields">
         
          <div class="field">
            <label><?php echo Lang::$word->_MT4_GROUP_CREATE;?></label>
            <label class="input"><i class="icon-prepend icon user"></i>
            	<select name="mt4_group_create" id="mt4_group_create">
            	<?php
                 echo 
                '<option value="">'.Lang::$word->_PLEASE_SELECT_DATA.'</option>'
                .genOptionGroup($aConf['mt4_group_micro'],$mt4_live_group_micro,'mt4_group_micro')
				.genOptionGroup($aConf['mt4_group_mini'],$mt4_live_group_mimi,'mt4_group_mini')
				.genOptionGroup($aConf['mt4_group_standard'],$mt4_live_group_standard,'mt4_group_standard')
				.genOptionGroup($aConf['mt4_group_pro'],$mt4_live_group_pro,'mt4_group_pro')
				.genOptionGroup($aConf['mt4_group_demo'],$mt4_demo_group ,'mt4_group_demo')
				?>
				</select>
				
            </label>
          </div>
        </div>
        <?php
		}
        ?>
        <?php
        if ($is_ct == 1) {
        ?>
         <div class="fields">
          <div class="field">
            <label><?php echo Lang::$word->_CT_TYPE;?></label>
            <label class="input"><i class="icon-prepend icon user"></i> <i class="icon-append icon asterisk"></i>
              <select name="ct_type">
              	<option value=""><?php echo Lang::$word->_PLEASE_SELECT_DATA;?></option>
              	<option value="live">Live</option>
              	<option value="demo">Demo</option>
              
              </select>
            </label>
          </div>
         
        </div>
        <?php
		}
        ?>
        <?php echo $content->rendertCustomFields('register', false);?>
        <div class="field">
          <label><?php echo Lang::$word->_UA_REG_RTOTAL;?></label>
          <label class="input"><img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-append"> <i class="icon-prepend icon unhide"></i>
            <input type="text" name="captcha">
          </label>
        </div>
        <button data-url="/ajax/user.php" type="button" name="dosubmit" class="prolific danger button"><?php echo Lang::$word->_UA_REG_ACC;?></button>                
        <a class="right-space" href="<?php echo SITEURL;?>/page/login/">        	
        	<span class="prolific info button" type="button" name="Login">Login </span>        </a>        		
        	<a class="active homepage" href="<?php echo SITEURL;?>">		
        		<i class="home icon"></i><strong>Home Page</strong></a>		
        <input name="doRegister" type="hidden" value="1">
      </form>
    </div>
    <div id="msgholder"></div>
  </div>
</div>
<?php endif;?>

  
<script>

$('#chk_ref_name').click(function(){
    $('#res_chk_ref_name').html("<font color='blue'>waiting...</font>");
    var valrefname = $("#referal_name").val();
    $.ajax({
      url: SITEURL + "/ajax/user.php" ,
      type: 'POST',
      data:{doGetRefName : 1, refname:valrefname},
      dataType :'json'
    }).done(function(obj) {
       if (obj.status=='success') {
            $('#res_chk_ref_name').html("<font color='green'><i class='check icon'></i> <b>" + obj.msg + "</b></font><input type='hidden' name='ref_uid' value='" + obj.name + "'>");
       } else {
            $('#res_chk_ref_name').html("<font color='red'><b>" + obj.msg + "</b></font>");
       }
    });
});
/*
$('#chk_email').click(function(){
    $('#res_chk_email').html("<font color='blue'>waiting...</font>");
    var valemail = $("#username").val();
    $.ajax({
      url: SITEURL + "/modules/tradersystem/ajax_user.php" ,
      type: 'POST',
      data:{doGetEmail : 1, email:valemail},
      dataType :'json'
    }).done(function(obj) {
       if (obj.status=='success') {
            $('#res_chk_email').html("<font color='green'><i class='check icon'></i> <b>"+obj.msg + "</b></font><input type='hidden' name='email' value='"+valemail+"'>");
            
       } else {
            $('#res_chk_email').html("<font color='red'><b>"+obj.msg + "</b></font>");
       }
    });
});*/
</script>
