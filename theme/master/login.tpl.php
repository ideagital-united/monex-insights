<?php
  /**
   * Login Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: login.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php

  if ($user->logged_in) {
      redirect_to(doUrl(false, $core->account_page, "page"));
  }
	
    
  if (isset($_POST['doLogin'])) {
      // :
      
      $result = $user->login($_POST['username'], $_POST['password']);
  /* Login Successful */
  
  
      if ($result) {
          redirect_to(doUrl(false, $core->account_page, "page"));
         exit;
     } // endif;
   
  }
  // endif;
?>
<div id="login">
  <div class="columns">
    <div class="screen-50 phone-100 push-center">
      <div class="nav clearfix"><a data-tab="#signin" class="active"><?php echo Lang::$word->_UA_TITLE2;?></a> <a data-tab="#reset"><?php echo Lang::$word->_UA_TITLE3;?></a></div>
      <div id="signin" class="section">
        <p><i class="information icon"></i> <?php echo Lang::$word->_UA_SUBTITLE2;?></p>
        <form method="post" id="login_form" name="login_form">
          <div class="columns">
            <div class="screen-30">
              <div class="item">
                <label><?php echo Lang::$word->_UA_TITLE2;?> <i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70">
              <div class="item">
                <input name="username" placeholder="<?php echo Lang::$word->_UA_TITLE2;?>" type="text">
              </div>
            </div>
            <div class="screen-30">
              <div class="item">
                <label><?php echo Lang::$word->_PASSWORD;?> <i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70">
              <div class="item">
                <input name="password" placeholder="<?php echo Lang::$word->_PASSWORD;?>" type="password">
              </div>
            </div>
            <div class="content-right"> <a href="<?php echo doUrl(false, $core->register_page, "page");?>" class="right-space"><?php echo Lang::$word->_UA_CLICKTOREG;?></a>
              <button name="submit" type="submit" class="prolific positive button"><?php echo Lang::$word->_UA_LOGINNOW;?></button> 				 				
             
		        &nbsp;&nbsp;&nbsp;&nbsp;
				<a class="active homepage" href="<?php echo SITEURL;?>">
				<i class="home icon"></i><strong>Home Page</strong></a>
              	<!--
              	<button name="bu_loginfacebook" id="bu_loginfacebook" onclick="signinwithfacebook()"type="button" class="prolific positive button">LOGIN WITH FACEBOOK</button>
             	-->
              <?php
              /*include THEMEDIR . "/loginfacebook.tpl.php";*/
              ?>
            </div>
            <input name="doLogin" type="hidden" value="1">
            <input name="idrun" id="idrun" type="hidden" value="">
          </div>
        </form>
        <?php print Filter::$showMsg;?> </div>
      <div id="reset" class="section">
        <p><i class="information icon"></i> <?php echo Lang::$word->_UA_SUBTITLE3;?></p>
        <form id="prolific_form" name="prolific_form" method="post" class="prolific form">
          <div class="columns">
            <div class="screen-30">
              <div class="item">
                <label><?php echo Lang::$word->_USERNAME;?> <i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70">
              <div class="item">
                <input name="uname" placeholder="<?php echo Lang::$word->_USERNAME;?>" type="text">
              </div>
            </div>
            <!--
            <div class="screen-30">
              <div class="item">
                <label><?php echo Lang::$word->_UR_EMAIL;?> <i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70">
              <div class="item">
                <input name="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" type="text">
              </div>
            </div>
           -->
             <div class="screen-30" style="display:none;">
              <div class="item">
                <label>Select Question <i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70" style="display:none;">
              <div class="item">
                <select name="question_pwd">
            	<option value="Best childhood friend?">Best childhood friend?</option>
				<option value="Anniversary?">Anniversary?</option>
				<option value="Favorite teacher?">Favorite teacher?</option>
				<option value="Favorite historical person?">Favorite historical person?</option>
				<option value="Name of first pet?">Name of first pet?</option>
            </select>
              </div>
            </div>
              <div class="screen-30"  style="display:none;">
              <div class="item">
                <label>Answer<i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70" style="display:none;">
              <div class="item">
                <input name="answer_pwd" placeholder="Answer" type="text">
              </div>
            </div>
            <div class="screen-30">
              <div class="item">
                <label><?php echo Lang::$word->_UA_PASS_RTOTAL;?> <i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70">
              <div class="item">
                <input name="captcha" placeholder="<?php echo Lang::$word->_UA_PASS_RTOTAL;?>" type="text">
                <img src="<?php echo SITEURL;?>/captcha.php" alt=""> </div>
            </div>
          </div>
          <button data-url="/ajax/user.php" type="button" name="dosubmit" class="prolific danger button"><?php echo Lang::$word->_UA_PASS_RSUBMIT;?></button>
          <input name="passReset" type="hidden" value="1">
        </form>
        <div id="msgholder"></div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
    $("#login .section").hide();
    $("#login .nav a:first").addClass("active").show();
    $("#login .section:first").show();
    $("#login .nav a").on('click', function () {
        $("#login .nav a").removeClass("active");
        $(this).addClass("active");
        $("#login .section").hide();
        var activeTab = $(this).data("tab");
        $(activeTab).show();
    });
});
// ]]>
</script>