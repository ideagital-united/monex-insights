<?php
  /**
   * Activation Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: activate.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  if ($user->logged_in)
      redirect_to(doUrl(false, $core->account_page, "page"));
?>
<?php 
if(get('resetp') && get('resetp') == 1){?>
  <div class="columns">
  <div class="screen-50 phone-100 push-center">
    <p><i class="information icon"></i> <?php echo Lang::$word->_RESETPWD_INFO1;?></p>
    <div class="prolific secondary segment form">
      <form id="prolific_form" name="prolific_form" method="post">
        <h3><?php echo Lang::$word->_RESET_PASSWORD;?></h3>
        <div class="field">
          <label><?php echo Lang::$word->_UR_EMAIL;?> <i class="small icon asterisk"></i></label>
          <input name="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" value="<?php if(get('email')) echo sanitize($_GET['email']);?>" type="text">
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_UA_TOKEN;?> <i class="small icon asterisk"></i></label>
          <input name="activate_token" placeholder="<?php echo Lang::$word->_UA_TOKEN;?>" value="<?php if(get('token')) echo sanitize($_GET['token']);?>" type="text">
        </div>
         <div class="field">
          <label><?php echo Lang::$word->_NEW_PASSWORD;?> <i class="small icon asterisk"></i></label>
          <input name="new_password" placeholder="******" value="" type="text">
        </div>
         <div class="field">
          <label><?php echo Lang::$word->_NEW_PASSWORD.'2';?> <i class="small icon asterisk"></i></label>
          <input name="new_password2" placeholder="******" value="" type="text">
        </div>
        <button data-url="/ajax/user.php" type="button" name="dosubmit" class="prolific positive button"><?php echo Lang::$word->_UA_ACTIVATE_ACC;?></button>
        <input name="doProfile_change_save_password" type="hidden" value="1">	
        	 		                
        <a class="right-space" href="<?php echo SITEURL;?>/page/registration">        	
        	<button class="prolific info button" type="button" name="Login">Register</button>        </a>        		
        <a class="active homepage" href="<?php echo SITEURL;?>">		
        	<i class="home icon"></i><strong>Home Page</strong></a>		
      </form>
    </div>
    <div id="msgholder"></div>
  </div>
</div>
<?php }else {?>
	<div class="columns">
  <div class="screen-50 phone-100 push-center">
    <p><i class="information icon"></i> <?php echo Lang::$word->_UA_INFO5;?></p>
    <div class="prolific secondary segment form">
      <form id="prolific_form" name="prolific_form" method="post">
        <h3><?php echo Lang::$word->_UA_SUBTITLE5;?></h3>
        <div class="field">
          <label><?php echo Lang::$word->_UR_EMAIL;?> <i class="small icon asterisk"></i></label>
          <input name="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" value="<?php if(get('email')) echo sanitize($_GET['email']);?>" type="text">
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_UA_TOKEN;?> <i class="small icon asterisk"></i></label>
          <input name="token" placeholder="<?php echo Lang::$word->_UA_TOKEN;?>" value="<?php if(get('token')) echo sanitize($_GET['token']);?>" type="text">
        </div>
        <button data-url="/ajax/user.php" type="button" name="dosubmit" class="prolific positive button"><?php echo Lang::$word->_UA_ACTIVATE_ACC;?></button>
        <input name="accActivate" type="hidden" value="1">
         <a class="right-space" href="<?php echo SITEURL;?>/page/registration">        	
        	<button class="prolific info button" type="button" name="Login">Register</button>        </a>        		
        <a class="active homepage" href="<?php echo SITEURL;?>">		
        	<i class="home icon"></i><strong>Home Page</strong></a>		
      </form>
    </div>
    <div id="msgholder"></div>
  </div>
</div>
<?php
}
?>
