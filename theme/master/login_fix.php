<?php 
 /**   * Header   *   *
  *  @package CMS Pro   
  * * @author prolificscripts.com   
  * * @copyright 2014   
  * * @version $Id: header.php, v4.00 2014-04-20 10:12:05 gewa Exp $   
**/
if (!defined("_VALID_PHP"))
	die('Direct access to this location is not allowed.');
?>
<!doctype html>
<head>
	<?php echo $content -> getMeta(); ?>
	<script type="text/javascript">
	var SITEURL =  "<?php echo SITEURL; ?>";
	</script>
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">
	<link href="<?php echo THEMEURL . '/cache/' . Minify::cache(array('css/base.css', 'css/button.css', 'css/image.css', 'css/icon.css', 'css/breadcrumb.css', 'css/popup.css', 'css/form.css', 'css/input.css', 'css/table.css', 'css/label.css', 'css/segment.css', 'css/message.css', 'css/divider.css', 'css/dropdown.css', 'css/list.css', 'css/header.css', 'css/menu.css', 'css/datepicker.css', 'css/progress.css', 'css/utility.css', 'css/comments.css', 'css/editor.css'), 'css'); ?>" rel="stylesheet" type="text/css" />
	<?php Content::getThemeStyle(); ?>
	<script src="<?php echo SITEURL; ?>/assets/jquery.js"></script>
	<script src="<?php echo SITEURL; ?>/assets/jquery-ui.js"></script>
	<script src="<?php echo SITEURL; ?>/assets/modernizr.mq.js"></script>
	<script src="<?php echo SITEURL; ?>/assets/global.js"></script>
	<script src="<?php echo SITEURL; ?>/assets/jquery.ui.touch-punch.js"></script>
	<script src="<?php echo THEMEURL; ?>/master.js"></script>
	<?php $content -> getPluginAssets($assets); ?>
	<?php $content -> getModuleAssets(); ?>
	<?php if($core->eucookie):?>
		<script type="text/javascript" src="<?php echo SITEURL; ?>/assets/eu_cookies.js"></script>
		<script type="text/javascript"> 
		$(document).ready(function () {    
			$("body").acceptCookies({        
				position: 'top',        
				notice: '<?php echo Lang::$word -> _EU_NOTICE; ?>',        
				accept: '<?php echo Lang::$word -> _EU_ACCEPT; ?>',        
				decline: '<?php echo Lang::$word -> _EU_DECLINE; ?>',        
				decline_t: '<?php echo Lang::$word -> _EU_DECLINE_T; ?>',        
				whatc: '<?php echo Lang::$word -> _EU_W_COOKIES; ?>'    
				})        
				});
				</script>
				<?php endif; ?>
				</head>
				<body<?php ($content -> _url[0] == "page") ? $core -> renderThemeBg() : null; ?>> 
				<header class="clearfix">
  <div class="top-bar" style="display:;">
    <div class="prolific secondary menu">
      <div class="prolific-grid">

        <?php if($core->showlogin):?>
        <!-- Login Start -->
        <?php if($user->logged_in):?>
        <a class="item" href="<?php echo doUrl(false, $core->account_page, "page");?>"><i class="icon user"></i> <b><?php echo Lang::$word->_WELCOME;?>: <?php echo $user->username;?>!</b></a>
        <?php if ($user->is_Admin() || isset($_SESSION['old'])):?>
        <a class="item" href="<?php echo SITEURL;?>/admin/"><i class="icon laptop"></i> Admin Panel</a>
        <?php endif;?>
        
        <!-- start prolific -->
        <?php if ($user->userfacebook==''):?>
        <?php
        /*include THEMEDIR . "/loginfacebook.tpl.php";*/
        ?>
        <!--
        <a class="prolific facebook button" id="connectwithfacebook" onclick="checkLoginState();"href="javascript:void(0);"><i class="icon facebook"></i> Connect with facebook</a>
        -->
        <?php endif;?>
        <?php if ($user->userfacebook!=''):?>
        <!--<a class="prolific facebook button " href="javascript:void(0);"><i class="icon facebook"></i> Connect with facebook complete</a>-->
        <?php endif;?>
        <!-- end prolific -->
        <a class="item" href="<?php echo SITEURL;?>/logout.php"><i class="icon unlock"></i> <?php echo Lang::$word->_N_LOGOUT;?></a>
        <?php else:?>
        <div class="item"><b><?php echo Lang::$word->_WELCOME;?> <?php echo $user->username;?></b> </div>
        <a class="item" href="<?php echo doUrl(false, $core->register_page, "page");?>"><?php echo Lang::$word->_UA_REGISTER;?></a> <a class="item" href="<?php echo doUrl(false, $core->login_page, "page");?>"><i class="lock icon"></i> <?php echo Lang::$word->_UA_LOGIN;?></a>
        <?php endif;?>
        <!--/ Login End -->
        <?php endif;?>
        
   
      </div>
    </div>
  </div>
  <div class="middle-bar" style="display:;">
    <div class="prolific-grid">
      <div class="prolific-content">
        <div class="logo"> <a href="<?php echo SITEURL;?>/"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a> </div>
       
      </div>
    </div>
  </div>
 
</header>
				<?php
require_once (THEMEDIR . "/login.tpl.php");
 ?> 
 <footer>
  <div class="prolific-grid" style="display:;">
    <div class="footer-wrap">
      <div class="prolific-content-full">
        <div class="columns horizontal-gutters">
          <div class="screen-40 tablet-40 phone-100">
            <div class="logo"><a href="<?php echo SITEURL;?>"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a> </div>
          </div>
          <div class="screen-60 tablet-60 phone-100">
            <div class="copyright">
              <div class="small-bottom-space"> <a href="<?php echo SITEURL;?>/" data-content="<?php echo Lang::$word->_HOME;?>"><i class="circular black inverted icon home link"></i></a> <a href="<?php echo SITEURL;?>/rss.php" data-content="Rss"><i class="circular black inverted icon rss link"></i></a> <a href="<?php echo doUrl(false, $core->sitemap_page, "page");?>" data-content="<?php echo Lang::$word->_SM_SITE_MAP;?>"><i class="circular black inverted icon map link"></i></a> </div>
              Copyright &copy;<?php echo date('Y').' <a href="'.SITEURL.'/">'.$core->site_name.'</a>';?> All Rights Reserved.</div>
          </div>
		</div>
		</br><p>The information on this website may only be copied with the express written permission of <?php echo ' <a href="'.SITEURL.'/">'.$core->site_name.'</a>';?>.</p>
<p>General Risk Warning: CFDs are leveraged products and as such loses may be more than the initial invested capital. Trading in CFDs carry a high level of risk thus may not be appropriate for all investors. The investment value can both increase and decrease and the investors may lose all their invested capital. Under no circumstances shall the Company have any liability to any person or entity for any loss or damage in whole or part caused by, resulting from, or relating to any transactions related to CFDs.</p>
      </div>
    </div>
  </div>
</footer>
 
 </body>  <!--bbbbbbbbbbbbbbbbbb-->