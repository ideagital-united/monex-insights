<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
    	 FB.login(function (response) {
              if (response.authResponse) {
                  console.log('Welcome!  Fetching your information.... ');
                  FB.api('/me', function (response) {
                  	  testAPI();
                      console.log('Good to see you, ' + response.name + '.');
                  });
              } else {
                  console.log('User cancelled login or did not fully authorize.');
              }
          });
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      //document.getElementById('status').innerHTML = 'Please log ' +'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
	function checkStatus(response)
	{
		if (response.status === 'connected') {
	        return true;
	    } else if (response.status === 'not_authorized') {
	        return false;
	    } else {
	    	return false;
	    }
	}
  	function checkLoginState() {
	    FB.getLoginStatus(function(response) {
	      var resstatus = checkStatus(response);
	      if (resstatus == true) {
		      	FB.api('/me', function(response) {
		  			var param = {"id":response.id,"doConnectfacebook":"1"};
		        	connectwithfacebook(param);
		    	});
	      	
	      	
	      } else {
	      	 	FB.login(function (response) {
	              if (response.authResponse) {
	                  console.log('Welcome!  Fetching your information.... ');
	                  FB.api('/me', function (response) {
	                  	var param = {"id":response.id,"doConnectfacebook":"1"};
	                  	  connectwithfacebook(param);
	                      console.log('Good to see you, ' + response.name + '.');
	                  });
					} else {
	                  console.log('User cancelled login or did not fully authorize.');
	              	}
				});
		}
		});
  	}
  	
  	function getme(){
	  	FB.api('/me', function(response) {
	  		var param = {"id":response.id,"doConnectfacebook":"1"};
	        return param;
	    });
  	}
	function ajax_signinwithfacebook(param) 
	{
		$.ajax({
			  url: SITEURL+"/modules/tradersystem/ajax_facebook.php",
			  type: "POST",
			  data: param,
			  dataType: "json"
		}).done(function(data) {
         	if(data.status == true){
	         	$('input[name="username"]').val(data.username);
	         	$('input[name="password"]').val(data.facebookid);
	         	$('input[name="idrun"]').val(data.idrun);
	         	$('button[name="submit"]').trigger('click')
         	} else {
         	  alert("process not complete!");
         	}
		});
  	}
	function signinwithfacebook()
	{
	  	FB.getLoginStatus(function(response) {
	      var resstatus = checkStatus(response);
	      if (resstatus == true) {
		      	FB.api('/me', function(response) {
		  			var param = {"id":response.id,"doFacebook":"1"};
		        	ajax_signinwithfacebook(param);
		    	});
	      	
	      	
	      } else {
	      	 FB.login(function (response) {
	              if (response.authResponse) {
	                  console.log('Welcome!  Fetching your information.... ');
	                  FB.api('/me', function (response) {
	                  	var param = {"id":response.id,"doFacebook":"1"};
	                  	  ajax_signinwithfacebook(param);
	                      console.log('Good to see you, ' + response.name + '.');
	                  });
	              } else {
	                  console.log('User cancelled login or did not fully authorize.');
	              }
	          });
	      }
	    });
	}
	function connectwithfacebook(param)
	{
		$.ajax({
			url: SITEURL+"/modules/tradersystem/ajax_facebook.php",
		  	type: "POST",
			  data: param,
			  dataType: "json"
		}).done(function(data) {
	         if(data.status == true){
	         	parent.location.reload(true); 
	         } else {
	         	alert("process not complete!");
	         }
		});
	}
//parent.location.reload(true); 
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '181506745308238',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.
/*
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
*/
  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.

  

</script>

<!-- checkLoginState -->

<!--
	id
	
	"730175019"
about
	
	"ด่วน ขายที่ดิน แถวสมุทรปราการ"
bio
	
	"เด็กดีแต่กำเนิด"
email
	
	"kataay_rabbit@hotmail.com"
favorite_teams
	
	[Object { id="535959589766439", name="Thaifutsal.com ข่าวสารฟุตซอลไทย"}]
first_name
	
	"Darawan"
gender
	
	"female"
last_name
	
	"Taorong"
link
	
	"http://www.facebook.com/730175019"
locale
	
	"th_TH"
name
	
	"Darawan Taorong"
quotes
	
	"ชอบคนจิง\r\nอะไรที่ควรจะทำ...อยากให้อับอาย อย่าทำเลย"
	-->