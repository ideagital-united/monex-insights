<?php
  /**
   * Footer
   *
   * @package Trader CMS
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: footer.php, v4.00 4011-04-20 10:12:05 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<!-- Footer -->
<footer>
  <div class="prolific-grid" style="">
    <div class="footer-wrap">
      <div class="prolific-content-full">
        <div class="columns horizontal-gutters">
          <div class="screen-40 tablet-40 phone-100">
            <div class="logo"><a href="<?php echo SITEURL;?>"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a> </div>
          </div>
          <div class="screen-60 tablet-60 phone-100">
            <div class="copyright">
              <div class="small-bottom-space"> <a href="<?php echo SITEURL;?>/" data-content="<?php echo Lang::$word->_HOME;?>"><i class="circular black inverted icon home link"></i></a> <a href="<?php echo SITEURL;?>/rss.php" data-content="Rss"><i class="circular black inverted icon rss link"></i></a> <a href="<?php echo doUrl(false, $core->sitemap_page, "page");?>" data-content="<?php echo Lang::$word->_SM_SITE_MAP;?>"><i class="circular black inverted icon map link"></i></a> </div>
              Copyright &copy;<?php echo date('Y').' <a href="'.SITEURL.'/">'.$core->site_name.'</a>';?> All Rights Reserved.</div>
          </div>
		</div>
		</br><p>The information on this website may only be copied with the express written permission of <?php echo ' <a href="'.SITEURL.'/">'.$core->site_name.'</a>';?>.</p>
<p>General Risk Warning: CFDs are leveraged products and as such loses may be more than the initial invested capital. Trading in CFDs carry a high level of risk thus may not be appropriate for all investors. The investment value can both increase and decrease and the investors may lose all their invested capital. Under no circumstances shall the Company have any liability to any person or entity for any loss or damage in whole or part caused by, resulting from, or relating to any transactions related to CFDs.</p>
      </div>
    </div>
  </div>
</footer>
<!-- Footer /-->
<?php if($core->analytics):?>
<!-- Google Analytics --> 
<?php echo cleanOut($core->analytics);?> 
<!-- Google Analytics /-->
<?php endif;?>
</body></html>