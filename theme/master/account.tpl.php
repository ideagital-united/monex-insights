<?php
//darawan not
  /**
   * Account Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: account.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  if (!$user->logged_in)
      redirect_to(doUrl(false, $core->login, "page"));
	  
  $listpackrow  = $member->getMembershipListFrontEnd();
  $mrow = $user->getUserMembership();
  $gatelist = $member->getGateways(true);
  $usr = $user->getUserData();
  //=== start prolific ===
  //$usr_dtl = $user->getUserDataDtl();
    $usr_dtl = Registry::get("TraderSystem") -> get_account_dtl();
  	$isShowFrm_posonal 	= false;
	$isShowFrm_addr 	= false;
	$isShowFrm_bank 	= false;
	// สำเนาบัตรชาชน
	if (isset($usr_dtl['is_doc_posonal']) && $usr_dtl['is_doc_posonal'] == 2) {
		$isShowFrm_posonal_text = "<font color='red'>Cancel</font>";
		$isShowFrm_posonal 		= true;
	} elseif (isset($usr_dtl['file_doc_posonal']) && $usr_dtl['file_doc_posonal'] != '' && isset($usr_dtl['is_doc_posonal']) && $usr_dtl['is_doc_posonal'] == 1){
		$isShowFrm_posonal_text = "<font color='green'>Confirmed</font>";
	} elseif (isset($usr_dtl['file_doc_posonal']) && $usr_dtl['file_doc_posonal'] != '') {
		$isShowFrm_posonal_text = "<font color='blue'>Waiting Confirm</font>";
	}  else {
		$isShowFrm_posonal_text = "";
		$isShowFrm_posonal 		= true;
	}
	
	//สำเนาทะบียนบ้าน
	if (isset($usr_dtl['is_doc_addr']) && $usr_dtl['is_doc_addr'] == 2) {
		$isShowFrm_addr_text = "<font color='red'>Cancel</font>";
		$isShowFrm_addr 		= true;
	} elseif (isset($usr_dtl['file_doc_addr']) && $usr_dtl['file_doc_addr'] != '' && isset($usr_dtl['is_doc_addr']) && $usr_dtl['is_doc_addr'] == 1){
		$isShowFrm_addr_text = "<font color='green'>Confirmed</font>";
	} elseif (isset($usr_dtl['file_doc_addr']) && $usr_dtl['file_doc_addr'] != '') {
		$isShowFrm_addr_text = "<font color='blue'>Waiting Confirm</font>";
	}  else {
		$isShowFrm_addr_text = "";
		$isShowFrm_addr 		= true;
	}
	
	//สำเนาทะบียนบ้าน
	if (isset($usr_dtl['is_doc_bank']) && $usr_dtl['is_doc_bank'] == 2) {
		$isShowFrm_bank_text = "<font color='red'>Cancel</font>";
		$isShowFrm_bank 		= true;
	} elseif (isset($usr_dtl['file_doc_bank']) && $usr_dtl['file_doc_bank'] != '' && isset($usr_dtl['is_doc_bank']) && $usr_dtl['is_doc_bank'] == 1){
		$isShowFrm_bank_text = "<font color='green'>Confirmed</font>";
	} elseif (isset($usr_dtl['file_doc_bank']) && $usr_dtl['file_doc_bank']!= '') {
		$isShowFrm_bank_text = "<font color='blue'>Waiting Confirm</font>";
	}  else {
		$isShowFrm_bank_text = "";
		$isShowFrm_bank 		= true;
	}
   //=== end prolific ===
?>
<h1 class="prolific double header"><span><?php echo Lang::$word->_UA_TITLE1;?></span></h1>
<p><i class="information icon"></i> <?php echo Lang::$word->_UA_INFO1;?></p>
<ul class="prolific tabs">
  <li><a data-tab="#uprofile"><i class="icon user"></i> <?php echo Lang::$word->_UA_SUBTITLE1;?></a></li>
  <li><a data-tab="#umember"><i class="icon certificate"></i> Upload Document</a></li>
  <li><a data-tab="#ubank"><i class="icon dollar"></i> Setting Bank Account</a></li>
</ul>
<form id="prolific_form" name="prolific_form" method="post">
<div id="uprofile" class="prolific tab content">
  <div class="prolific secondary form segment">
    
      <h3><?php echo Lang::$word->_UA_SUBTITLE1;?></h3>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_USERNAME;?></label>
          <input name="username" type="text" disabled="disabled" value="<?php echo $usr->username;?>">
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_PASSWORD;?></label>
          <input name="password" type="password">
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_UR_FNAME;?></label>
          <input name="fname" type="text" value="<?php echo $usr->fname;?>">
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_UR_LNAME;?></label>
          <input name="lname" type="text" value="<?php echo $usr->lname;?>">
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_UR_EMAIL;?></label>
          <label class="input">
          	<?php if($_SESSION['email']){echo $usr->email;echo "<input name='email' type='hidden' value='".$usr->email."'>";}else{?>
            <input name="email" type="text" value="<?php echo $usr->email;?>">
            <?php } ?>
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_UR_IS_NEWSLETTER;?></label>
          <div class="inline-group">
            <label class="radio">
              <input name="newsletter" type="radio" value="1" <?php echo getChecked($usr->newsletter, 1);?>>
              <i></i><?php echo Lang::$word->_YES;?></label>
            <label class="radio">
              <input name="newsletter" type="radio" value="0" <?php echo getChecked($usr->newsletter, 0);?>>
              <i></i><?php echo Lang::$word->_NO;?></label>
          </div>
        </div>
      </div>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_UR_AVATAR;?></label>
          <label class="input">
            <input type="file" name="avatar" class="filefield">
          </label>
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_UR_AVATAR;?></label>
          <div class="prolific avatar image">
            <?php if($usr->avatar):?>
            <img src="<?php echo UPLOADURL;?>avatars/<?php echo $usr->avatar;?>" alt="<?php echo $usr->username;?>">
            <?php else:?>
            <img src="<?php echo UPLOADURL;?>avatars/blank.png" alt="<?php echo $usr->username;?>">
            <?php endif;?>
          </div>
        </div>
        </div>
        
      <?php echo $content->rendertCustomFields('profile', $usr->custom_fields);?>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_UR_LASTLOGIN;?></label>
          <input type="text" value="<?php echo Filter::dodate("long_date", $usr->lastlogin);?>" name="lastlogin" disabled="disabled">
        </div>
        <div class="field">
          <label><?php echo Lang::$word->_UR_DATE_REGGED;?></label>
          <input type="text" value="<?php echo Filter::dodate("long_date", $usr->created);?>" name="lastlogin" disabled="disabled">
        </div>
      </div>
        <div class="field">
          <label><?php echo Lang::$word->_UR_BIO;?></label>
          <textarea name="info"><?php echo $usr->info;?></textarea>
        </div>
      <button data-url="/ajax/controller.php" type="button" name="dosubmit" class="prolific button"><?php echo Lang::$word->_UA_UPDATE;?></button>
      <input name="doProfile" type="hidden" value="1">
   
  </div>
  
</div>

<div id="umember" class="prolific tab content">
  <div class="prolific secondary form segment">
    
      
        <!-- start prolific --->
     
       <div class="two fields">
        <div class="field">
          <label>Personal ID</label>
          <label class="input">
            <?php if ($isShowFrm_posonal == false){?>
				<?php echo $isShowFrm_posonal_text;?>
				<a href="<?php echo UPLOADURL;?>file_doc/<?php echo $usr_dtl['file_doc_posonal'];?>" target="_blank"> <?php echo $usr_dtl['file_doc_posonal'];?></a>
			<?} else {?>
				<?php echo $isShowFrm_posonal_text;?>
                <input type="file" name="file_posonal" class="filefield">

			<?}?>
          </label>
      </div>
        <div class="field">
          <label>Personal address</label>
          <div class="prolific avatar image">
            <?php if ($isShowFrm_addr == false){?>
             	<?php echo $isShowFrm_addr_text;?>
             	<a href="<?php echo UPLOADURL;?>file_doc/<?php echo $usr_dtl['file_doc_addr'];?>" target="_blank"> <?php echo $usr_dtl['file_doc_addr'];?></a>
             <?} else {?>
             	<?php echo $isShowFrm_addr_text;?>
                <input type="file" name="file_addr" class="filefield">
             <?}?>
          </div>
        </div>
      </div>
        <!-- end prolific --->
      
      <button data-url="/ajax/controller.php" type="button" name="dosubmit" class="prolific button"><?php echo Lang::$word->_UA_UPDATE;?></button>
      <input name="doProfile" type="hidden" value="1">
   
  </div>

  </div>
  <div id="ubank" class="prolific tab content">
  <div class="prolific secondary form segment">
    
      
        <!-- start prolific --->
        <div class="two fields">
         <?php if ($isShowFrm_bank == false){?>
         	 <div class="field">
             <label>Bank Name : <?php echo $usr_dtl['bank_name'];?></label>
             <label>Country : <?php echo $usr_dtl['bank_country'];?></label>
             <label>Account Type : <?php echo $usr_dtl['bank_type'];?></label>
             <label>Account Number : <?php echo $usr_dtl['bank_account'];?></label>
             <label>SWIFT : <?php echo $usr_dtl['bank_swift'];?></label>
             <label>Routing Number : <?php echo $usr_dtl['bank_routing_number'];?></label>
             <label>IBAN : <?php echo $usr_dtl['bank_iban'];?></label>
             
             
             </div>
         <?php }else{?>
        <div class="field">
          <label>Bank Name</label>
          <label class="input">
             <?php
              echo select_lookup_table_main("acms_bank_thai","bank_name",$usr_dtl['bank_name'],"bank_code","bank_name","where itype_withdraw=1","---plese select bank name---");
             ?>
          </label>
          
          <label>Country</label>
          <label class="input">
             <?php
              echo select_lookup_table_main("acms_country_code","bank_country",$usr_dtl['bank_country'],"country","country","where country!=''","---plese select country---");
               ?>
          </label>
          
          <label>Account Type</label>
          <label class="input">
            <label for="AccountType_1"><input name="bank_type" type="radio" id="AccountType_1" value="current" <?php if($usr_dtl->bank_type =='' || $usr_dtl->bank_type=='current'){echo "selected";}?> >CURRENT</label>
            <label for="AccountType_2"><input name="bank_type" type="radio" id="AccountType_2" value="saving" <?php if($usr_dtl->bank_type=='saving'){echo "selected";}?> >SAVING</label>
          </label>
          
          <label>Account Number</label>
          <label class="input">
             <input type="text" name="bank_account" value="<?php echo $usr_dtl['bank_account'];?>">
          </label>
          
          <label>SWIFT </label>
          <label class="input">
             <input type="text" name="bank_swift" value="<?php echo $usr_dtl['bank_swift'];?>">
          </label>
          
           <label>Routing Number </label>
          <label class="input">
             <input type="text" name="bank_routing_number" value="<?php echo $usr_dtl['bank_routing_number'];?>">
          </label>
          
          <label>IBAN </label>
          <label class="input">
             <input type="text" name="bank_iban" value="<?php echo $usr_dtl['bank_iban'];?>">
          </label>
      </div>
      <?php }?>
      </div>
      
       <div class="fields">
        <div class="field">
          <label>Document Bank</label>
          <label class="input">
            <?php if ($isShowFrm_bank == false){?>
				<?php echo $isShowFrm_bank_text;?>
				<a href="<?php echo UPLOADURL;?>file_doc/<?php echo $usr_dtl['file_doc_posonal'];?>" target="_blank"> <?php echo $usr_dtl['file_doc_posonal'];?></a>
			<?} else {?>
				<?php echo $isShowFrm_bank_text;?>
                <input type="file" name="file_bank" class="filefield">

			<?}?>
          </label>
      </div>
   
      </div>
        <!-- end prolific --->
      
      <button data-url="/ajax/controller.php" type="button" name="dosubmit" class="prolific button"><?php echo Lang::$word->_UA_UPDATE;?></button>
      <input name="doProfile" type="hidden" value="1">
   
  </div>

  </div>
   </form>
   <div id="msgholder"></div>
  <!-- start member
  <div class="prolific secondary segment">
    <?php if($listpackrow ):?>
    <h3><?php echo Lang::$word->_MS_SUBTITLE3;?></h3>
    <?php $total = count($listpackrow);?>
    <?php $color = array("positive","info","warning","negative","purple","teal","black");?>
    <div id="mempacks">
      <div class="columns">
        <div class="<?php echo numberToWords($total);?> columns small-gutters">
          <?php foreach ($listpackrow as $i => $prow):?>
          <div class="row">
            <div class="prolific <?php echo $color[$i];?> segment">
              <h4 class=""><?php echo $prow->{'title'.Lang::$lang};?></h4>
              <p class="prolific 2 fluid buttons"> <span class="prolific <?php echo $color[$i];?> small button"><?php echo $core->formatMoney($prow->price);?></span> <span class="prolific <?php echo $color[$i];?> small button"><?php echo $prow->days . ' ' .$member->getPeriod($prow->period);?></span></p>
              <p class="item"><span class="prolific <?php echo $color[$i];?> small fluid button"><?php echo Lang::$word->_MS_RECURRING;?> <b><?php echo ($prow->recurring) ? '<i class="icon check"></i>' : '<i class="icon ban circle"></i>';?></b></span></p>
              <p><small><?php echo $prow->{'description'.Lang::$lang};?></small> </p>
              <a class="add-cart prolific <?php echo $color[$i];?> fluid button" data-price="<?php echo $prow->price;?>" data-id="<?php echo $prow->id;?>">
              <?php if($prow->price <> 0):?>
              <i class="icon dollar"></i>
              <?php endif;?>
              <?php echo ($prow->price <> 0) ? Lang::$word->_UA_BUY : Lang::$word->_UA_ACTIVATE;?></a> </div>
          </div>
          <?php endforeach;?>
        </div>
      </div>
      <?php endif;?>
    </div>
  </div> -->
  <!-- end member -->
  </div>
  <div id="gresults"></div>
</div>
<?php if(file_exists(MODPATHF . 'digishop/account.tpl.php')):?>
<div id="digishop" class="prolific tab content">
<?php require_once(MODPATHF . 'digishop/account.tpl.php');?>
</div>
<?php endif;?>

<?php if(file_exists(MODPATHF . 'blog/account.tpl.php')):?>
<div id="blog" class="prolific tab content">
<?php require_once(MODPATHF . 'blog/account.tpl.php');?>
</div>
<?php endif;?>
<?php if(file_exists(MODPATHF . 'booking/account.tpl.php')):?>
<div id="booking" class="prolific tab content">
<?php require_once(MODPATHF . 'booking/account.tpl.php');?>
</div>
<?php endif;?>
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
    $("#mempacks").on("click", "a.add-cart", function () {
        id = $(this).data('id');
        price = $(this).data('price');
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: SITEURL + "/ajax/controller.php",
            data: {
                addtocart: 1,
                id: id
            },
            success: function (json) {
                $("#gresults").html(json.message);

            }
        });
        return false;
    });
});
// ]]>
</script>