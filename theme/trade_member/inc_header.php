<!doctype html>
<html>
    <head>
    	<?php echo $content->getMeta(); ?>
    	<script type="text/javascript">
		var SITEURL = "<?php echo SITEURL; ?>";
		var THEMEURLTRADE = "<?php echo THEMEURLTRADE; ?>";
		</script>
		<meta charset="UTF-8">
		<title>Secure Backoffice</title>
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
		<!--
		<link rel="shortcut icon" href="favicon.ico" />
		-->
		<!-- bootstrap framework -->
		<link href="<?php echo THEMEURLTRADE;?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		
		<!-- custom icons -->
			<!-- font awesome icons -->
			<link href="<?php echo THEMEURLTRADE;?>assets/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
			<!-- ionicons -->	
			<link href="<?php echo THEMEURLTRADE;?>assets/icons/ionicons/css/ionicons.min.css" rel="stylesheet" media="screen">
			<!-- flags -->
			<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/icons/flags/flags.css">
		<!-- datatables -->
		<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/DataTables/media/css/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css">
		<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/DataTables/extensions/Scroller/css/dataTables.scroller.min.css">
				
	
	<!-- page specific stylesheets -->

		<!-- nvd3 charts -->
		<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/novus-nvd3/nv.d3.min.css">
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/select2/select2.css">
		<!-- bootstrap switches -->
		<link href="<?php echo THEMEURLTRADE;?>assets/lib/bootstrap-switch/build/css/bootstrap3/bootstrap-switch.css" rel="stylesheet">
		<!-- owl carousel -->
		<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/owl-carousel/owl.carousel.css">
				
		<!-- main stylesheet -->
		<link href="<?php echo THEMEURLTRADE;?>assets/css/style.css" rel="stylesheet" media="screen">
		
		<!-- google webfonts -->
		<link href='<?php echo PROTOCOL_HTTP; ?>://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css'>
		
		<!-- moment.js (date library) -->
		<script src="<?php echo THEMEURLTRADE;?>assets/lib/moment-js/moment.min.js"></script>
		
		

		
    </head>
    <body>