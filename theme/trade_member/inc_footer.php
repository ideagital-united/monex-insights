<!-- jQuery -->
<script src="<?php echo THEMEURLTRADE;?>assets/js/jquery.min.js"></script>
<!-- easing -->
<script src="<?php echo THEMEURLTRADE;?>assets/js/jquery.easing.1.3.min.js"></script>
<!-- bootstrap js plugins -->
<script src="<?php echo THEMEURLTRADE;?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- top dropdown navigation -->
<script src="<?php echo THEMEURLTRADE;?>assets/js/tinynav.js"></script>
<!-- perfect scrollbar -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js"></script>

<!-- common functions -->
<script src="<?php echo THEMEURLTRADE;?>assets/js/tisa_common.js"></script>

<!-- style switcher
<script src="<?php echo THEMEURLTRADE;?>assets/js/tisa_style_switcher.js"></script>
-->
<!-- page specific plugins -->
<!-- nvd3 charts -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/d3/d3.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/novus-nvd3/nv.d3.min.js"></script>
<!-- flot charts-->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.tooltip.min.js"></script>
<!-- clndr -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/underscore-js/underscore-min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/CLNDR/src/clndr.js"></script>
<!-- easy pie chart -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<!-- owl carousel -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/owl-carousel/owl.carousel.min.js"></script>

<!-- dashboard functions -->
<!--<script src="<?php echo THEMEURLTRADE;?>assets/js/apps/tisa_dashboard.js"></script>-->
<!-- multiselect, tagging -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/select2/select2.min.js"></script>
<!--  bootstrap switches -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/bootstrap-switch/build/js/bootstrap-switch.min.js"></script>
<!-- user profile functions -->
<script src="<?php echo THEMEURLTRADE;?>assets/js/apps/tisa_user_profile.js"></script>
<!-- form extended elements functions -->
<script src="<?php echo THEMEURLTRADE;?>assets/js/apps/tisa_extended_elements.js"></script>
<!-- textarea autosize -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/autosize/jquery.autosize.min.js"></script>
<!--  datepicker -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- date range picker -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- timepicker -->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!-- datatables 
-->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<!-- datatables functions -->
<script src="<?php echo THEMEURLTRADE;?>assets/js/apps/tisa_datatables.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/js/jquery_cookie.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/js/jquery.form.js"></script> 
<script src="<?php echo THEMEURLTRADE;?>master.js"></script>
<script type="text/javascript">
$('#deposittime').timepicker({
minuteStep: 1,
template: 'modal',
appendWidgetTo: '#modal_bank_transfer',
showSeconds: false,
showMeridian: false,
defaultTime: 'current'
});
/*
$( "input[name$='radio_switch1']" ).click(function() {
	var test = $(this).val();
	alert
$("div.desc").hide();
$("#pincode_setting_" + test).fadeIn();
//$( "#pincode_setting" ).fadeIn();
});*/

$(document).ready(function() {
    $("input[name$='radio_switch1']").change(function() {
        var test = $(this).val();
        //alert(test);
		$("form.desc").slideUp();
		$("#pincode_setting_" + test).slideDown();
    });
    
    
    $('#langmenu').on('click', 'a', function () {
		var target = $(this).attr('href');
		$.cookie("LANG_CMSPRO", $(this).data('lang'), {
			expires: 120,
			path: '/'
		});
		$('body').fadeOut(1000, function () {
			window.location.href = SITEURL + "/" + target;
		});
		return false
	});
    
    
    
});
</script>

</body>
</html>
</body>
</html>