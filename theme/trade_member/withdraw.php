<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>
						Make a Withdraw
						<span class="badge badge-success big">Avaliable funds in wallet is USD$ 56,446.50</span>
						</h3>
					</div>
					<div class="panel-body">
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Bank Transfer
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_bank_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Paypal
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Paysbuy
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Skrill
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Withdraw History</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Transection Number</th>
									<th>Date</th>
									<th>Type</th>
									<th>Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="modal_bank_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Make a Withdraw</h3>
			<div class="panel panel-default">
				<div class="panel-heading">Bank Transfer Information</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<td></td>
									<td>Bank name</td>
									<td>Account Number</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option2" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<form class="form-horizontal">
				<div class="form-group">
					<label for="user_lname" class="col-md-4 control-label">Amount</label>
					<div class="col-md-6 input-group">
						<span class="input-group-addon">USD $</span>
						<input class="form-control" id="appendedPrependedInput" type="text">
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
					<button class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</button>
				</div>
			</form>
			</div>
			
		</div>
	</div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="modal_e_currency_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Make a Withdraw</h3>
			<form class="form-horizontal">
				<div class="form-group">
					<div class="row">
						<label for="user_lname" class="col-md-4 control-label">Amount</label>
						<div class="col-md-6 input-group">
							<span class="input-group-addon">USD $</span>
							<input class="form-control" id="appendedPrependedInput" type="text">
						</div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
					<button class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>