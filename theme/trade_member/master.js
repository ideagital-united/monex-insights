  $(document).ready(function () {
     
      /* == Master Form == */
      $('body').on('click', 'button[name=dosubmit]', function () {
		  posturl = $(this).data('url');
		  var divshowmsg 	= $(this).data('divmsg') || 'msgholder';
		  var frmid 		= $(this).data('frm') || 'prolific_form';
		  function showResponse(json) {
		  	 
		  	  /*$('#showtitle').html(json.status);*/
			  if (json.status == "success") {
			  	  $('#msg-loading').modal("hide");
			  	  var chkfrm_step = 0;
			  	  if  (json.dtl) {
			  	  	 if (json.dtl.step) {
			  	  	 	if (json.dtl.step == "step2") {
			  	  	 		chkfrm_step = 1;
			  	  	 	}
			  	  	 }
			  	  }
			  	  if(chkfrm_step == 1) {
			  	  	 $('#list_pkid').val(json.id);
			  	  	 $('#frm_step2').modal("show");
			  	  	 $('#transfer_detail').html(json.message_dtl);
			  	  	 $('.bu_req_otp').trigger('click');
			  	  } else {
					  $("#"+divshowmsg).html(json.message);
				  	 
				  	  $('#msg-showwarningmsg').modal("show");
				  	  if (json.isfrmreset) {
				  	  	  $('.reqpwd_step1').hide();
				  	  	  $('.reqpwd_step2').show();
				  	  }
			  	  }
			  	  if (json.gotopage) {
					window.location.href = SITEURL + "/" + json.gotopage;
			  	  }
			  	  if (json.isRedirect) {
			  	  	location.reload(); 
			  	  }
				  
			  } else {
				 
				  $("#"+divshowmsg).html(json.message);
				  $('#msg-loading').modal("hide");
				  $('#msg-showwarningmsg').modal("show");
			  }
		  }

          function showLoader() {
          	  $('.modal').modal('hide');
          	  $('#msg-loading').modal("show");
              
          }
          var options = {
              target: "#msgholder",
              beforeSubmit: showLoader,
              success: showResponse,
              type: "post",
              url: SITEURL + posturl,
              dataType: 'json'
          };

          $('#'+frmid).ajaxForm(options).submit();
      });
      
      
      
 

      
  });
 