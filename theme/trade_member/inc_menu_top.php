		<!-- top bar -->
		<header class="navbar navbar-fixed-top" role="banner">
			<div class="container-fluid">
				<div class="navbar-header">
					<a href="<?php echo SITEURL;?>/" class="navbar-brand"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a>
					
				</div>
				<ul class="top_links">
					<li>
						<!--<a href="tasks_summary.html"><span><?php echo Lang::$word->_CURRENT_SERVER_TIME;?></span> </a>-->
						<!--<?php echo date("h:i:sa"); ?>-->
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="lang_menu" >
						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php
							$_SESSION['langnew'] = $_COOKIE['LANG_CMSPRO'];
							
							if ($_SESSION['langold'] != $_SESSION['langnew']) {
								$_SESSION['langold'] = $_SESSION['langnew'];
								$aGet = $tradersystem->getdata("*","language","where flag='".$_SESSION['langold']."'");
								
								$_SESSION['lang_theme'] = $aGet['flag_theme'];
								
							}
							if (isset($_SESSION['lang_theme']) && $_SESSION['lang_theme'] != '') {?>
								 <span class="flag-<?php echo$_SESSION['lang_theme']; ?>"></span> <span class="caret"></span>
							<?php }else{ ?>
								<span ><?php echo Core::$language;?></span> <span class="caret"></span>
							<?php }?>
							
							
						</a>
						<!--
							<a href="" class="item" data-lang="as" data-text="asia"><i class="icon flag"></i>asia</a>
							-->
						<ul class="dropdown-menu dropdown-menu-right"id="langmenu">
							<?php foreach($core->langList() as $lang):?>
							<li><a href="<?php echo str_replace("url=","",$_SERVER['QUERY_STRING']);?>"data-lang="<?php echo $lang->flag;?>" data-text="<?php echo $lang->name;?>"><span class="flag-<?php echo $lang->flag_theme;?>"></span><?php echo $lang->name;?></a></li>
							 <?php endforeach;?>
						</ul>
					</li>
					<li class="user_menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span class="navbar_el_icon ion-person"></span> <span class="caret"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-right">
							
							<li><a href="index.php?subpage=account_setting"><?php echo Lang::$word->_MANAGE_PROFILE;?></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</header>
		<!-- main content -->
		<div id="main_wrapper">
			<div class="page_bar clearfix">
				<div class="row">
					<div class="col-md-12">
						<div class="media">
							
							<?php if($user->avatar):?>
				            <img class="img-thumbnail pull-left"src="<?php echo UPLOADURL;?>avatars/<?php echo $user->avatar;?>" alt="<?php echo $user->username;?>">
				            <?php else:?>
				            <img class="img-thumbnail pull-left"src="<?php echo THEMEURLTRADE;?>assets/img/avatars/avatar_5.jpg" alt="<?php echo $user->username;?>">
				            <?php endif;?>
				            <!--
							<img class="img-thumbnail pull-left" src="assets/img/avatars/avatar_5.jpg" alt="">
							-->
							<?php 
							/*echo "<pre>";print_r($user);*/
							?>
							<div class="media-body">
								<h1 class="page_title"><span class="text-muted"><?php echo Lang::$word->_Hello;?> </span><?php echo $user->name;?></h1>
								<p><span class="text-muted"><?php echo Lang::$word->_REGISTRATION_DATE;?>:</span> <?php echo $_SESSION['user_regis'];?></p>
								
								<p>
									
									<span class="text-muted"><?php echo Lang::$word->_ACCOUNT_STATUS;?>:</span>  
									<?php switch($_SESSION['user_active']){
										case 'y' : echo "<span class='badge badge-success'>E-mail ".Lang::$word->__VERIFIED."</span>";break;
										case 't' : echo "<span class='badge badge-danger'>E-mail ".Lang::$word->_NOT_VERIFY."</span>";break;
										case 'b' : echo "<span class='badge badge-danger'>E-mail ".Lang::$word->_BANNED."</span>";break;
										default : echo "<span class='badge badge-warning'>E-mail ".Lang::$word->_PENDING."</span>";break;
									}
									?>
									
									<?php switch($_SESSION['usr_dtl']['user_info']['id_card_verified']){
										case 1 : echo "<span class='badge badge-success'>Personal ID ".Lang::$word->__VERIFIED."</span>";break;
										case 2 : echo "<span class='badge badge-danger'>Personal ID ".Lang::$word->__REJECTED."</span>";break;
										default : 
												if($_SESSION['usr_dtl']['user_info']['id_card_file_url'] == ''){
													echo "<span class='badge badge-danger'>Personal ID ".Lang::$word->_NOT_VERIFY."</span>";
												} else {
													echo "<span class='badge badge-warning'>Personal ID ".Lang::$word->_PENDING."</span>";
												}break;
									}
									?>
									<?php switch($_SESSION['usr_dtl']['user_info']['address_verified']){
										case 1 : echo "<span class='badge badge-success'>Personal Address ".Lang::$word->__VERIFIED."</span>";break;
										case 2 : echo "<span class='badge badge-danger'>Personal Address ".Lang::$word->__REJECTED."</span>";break;
										default : 
										        if($_SESSION['usr_dtl']['user_info']['address_file_url'] == ''){
													echo "<span class='badge badge-danger'>Personal Address ".Lang::$word->_NOT_VERIFY."</span>";
												} else {
													echo "<span class='badge badge-warning'>Personal Address ".Lang::$word->_PENDING."</span>";
												}break;
									}
									?>
										<?php switch($_SESSION['bank_verified']){
										case 1 : echo "<span class='badge badge-success'>Bank Account ".Lang::$word->__VERIFIED."</span>";break;
										case 0 : echo "<span class='badge badge-warning'>Bank Account ".Lang::$word->_PENDING."</span>";break;
										default :echo "<span class='badge badge-danger'>Bank Account ".Lang::$word->_NOT_VERIFY."</span>";break;
									}
									?>
									
								</p>
								
							</div>
						</div>
					</div>
				</div>
			</div>