<?php
$wallet_net 	= (isset($_SESSION['usr_dtl']['wallet_amt']['amount'])) ? number_format($_SESSION['usr_dtl']['wallet_amt']['amount'],2): 0.00;

?>
			<div class="page_content">
				<div class="container-fluid">
					<!--
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-danger"><center><strong>Important:</strong> You need to verify your mobile phone via SMS of Google Authenticator before you can make a withdraw.</center></div>
						</div>
					</div>
					-->
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-lg-3 col-md-6">
									<div class="panel panel-default">
										<div class="stat_box">
											<div class="stat_ico color_f"><span class="fa fa-usd"></span></div>
											<div class="stat_content">
												<span class="stat_count">$ <?php echo $wallet_net;?></span>
												<span class="stat_name"><?php echo Lang::$word->_TOTAL_ACCOUNT_BALANCE;?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-6">
									<div class="panel panel-default">
										<div class="stat_box">
											<div class="stat_ico color_g"><span class="fa fa-usd"></span></div>
											<div class="stat_content">
												<span class="stat_count">$ 0</span>
												<span class="stat_name"<?php echo Lang::$word->_TOTAL_COMMISSION;?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3 col-md-6">
									<div class="panel panel-default">
										<a href="#">
										<div class="stat_box ">
											<div class="stat_ico color_a"><span class="glyphicon glyphicon-download-alt"></span></div>
											<div class="stat_content">
												<span class="stat_count">MT4</span>
												<span class="stat_name">Download</span>
											</div>
										</div>
										</a>
									</div>
								</div>
								<div class="col-lg-3 col-md-6">
									<div class="panel panel-default">
										<a href="#">
										<div class="stat_box ">
											<div class="stat_ico color_d"><i class="ion-ios7-help-outline"></i></div>
											<div class="stat_content">
												<span class="stat_count">Help</span>
												<span class="stat_name">Go to Help Section</span>
											</div>
										</div>
										</a>
									</div>
								</div>
							</div>
								
						</div>

					</div>
					<div class="row">
					<div class="panel panel-default">
								<div class="panel-body">
									<div class="heading_b"><?php echo Lang::$word->_MY_PORTFOLIOS;?></div>
									<div class="row">
										<div class="col-md-7">
											<table class="table table-striped">
												<thead>
													<tr>
														<th><?php echo Lang::$word->_MY_PORTFOLIOS;?></th>
														<th class="col_md sub_col"><?php echo Lang::$word->_VALUE;?></th>
														<th class="col_md sub_col">% <?php echo Lang::$word->_VALUE;?></th>
													</tr>
												</thead>
												<tbody>
													<?php
													$listAcc = $_SESSION['usr_dtl']['trade_acc_list'];
													if (count($listAcc) > 0) {
														
														$aNameProduct = array('1'=>'MT4','2'=>'cTrader');
														foreach ($listAcc as $k => $v) {
															
															?>
															<tr>
															<td><a href="#"><?php echo $aNameProduct[$v['trader_product_type']];?>:<?php echo $v['trader_acc_login'];?></a></td>
															<td class="sub_col login-b" typep= "<?php echo $aNameProduct[$v['trader_product_type']];?>" acc="<?php echo $v['trader_acc_login'];?>">$0.00</td>
															<td class="sub_col" id="percent_<?php echo $v['trader_acc_login'];?>">0%</td>
															</tr>
															<?php
															
														}
													} else {
														?>
														<tr>
														<td colspan="3">NO DATA</td>
														</tr>
														<?php
													}
													?>
													<tr>
														<td><a href="#">MT4:231654</a></td>
														<td class="sub_col">$1,428</td>
														<td class="sub_col">54%</td>
													</tr>
													<tr>
														<td><a href="#">MT4:465231</a></td>
														<td class="sub_col">$858</td>
														<td class="sub_col">21%</td>
													</tr>
													<tr>
														<td><a href="#">MT4:789654</a></td>
														<td class="sub_col">$647</td>
														<td class="sub_col">11%</td>
													</tr>
													<tr>
														<td><a href="#">cTrader:465897</a></td>
														<td class="sub_col">$433</td>
														<td class="sub_col">6%</td>
													</tr>
													<tr>
														<td><a href="#">cTrader:465321</a></td>
														<td class="sub_col">$141</td>
														<td class="sub_col">2%</td>
													</tr>
												</tbody>
												<tfoot>
													<tr class="active">
														<td><b><a href="#">Total</a></b></td>
														<td class="sub_col"><b>$3,507</b></td>
														<td class="sub_col"><b>100%</b></td>
													</tr>
													
												</tfoot>
											</table>
										</div>
										<div class="col-md-5">
											<div id="flot_browsers" class="chart" style="height:240px;width:100%">
												
												<script>
												
												
													chart_browsers_data = [
														{ label: "MT4:231654", data: 1428, color: '#1f77b4' },
														{ label: "MT4:465231", data: 858, color: '#aec7e8' },
														{ label: "MT4:789654", data: 647, color: '#ff7f0e' },
														{ label: "cTrader:465897", data: 433, color: '#ffbb78' },
														{ label: "cTrader:465321", data: 141, color: '#2ca02c' }
													];
												
													
												</script>
											</div>
										</div>
									</div>
								</div>
					</div>
					</div>

					
				</div>
			</div>		
		</div>
<?php
function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}


?>
<script>
var chart_browsers_data_new = [];
var list_all = "<?php echo count($listAcc);?>";
list_all = list_all*1;

window.onload = function () { 
	var runall = 0;
	$('.login-b').each(function() {
		var acctrade = $(this).attr('acc');
		var typep = $(this).attr('typep');
		var pointcurrent = $(this);
		$.ajax({
		  url: SITEURL + "/modules/tradersystem/ajax_user.php" ,
		  type: 'POST',
		  data:{doGetBalanceTrade : 1,trader_acc_login:acctrade},
		  dataType :'json'
		}).done(function(obj) {
		   var netmoney = obj.balanceformat;
		   runall = runall+1;
		   
		   chart_browsers_data_new[] = {label:typep+":"+acctrade,data:netmoney,color:"#<?php echo random_color();?>"};
		   pointcurrent.html('$' + netmoney);
		   if (runall == list_all) {
		   	alert('OKKKK');
		   }
		});
	    
	});
}
</script>

