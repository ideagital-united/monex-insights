		<!-- side navigation -->
		<nav id="side_nav">
			<ul>
				<li>
					<a href="index.php"><span class="ion-speedometer"></span> <span class="nav_title"><?php echo Lang::$word->_N_DASH;?></span></a>
				</li>
				<li>
					<a href="#" class="<?php echo ($page=='account_setting' || $page=='trade_account') ? "menuleftactive":"";?>"><span class="ion-android-contact"></span> <span class="nav_title"><?php echo Lang::$word->_MY_DETAILS;?></span></a>
					<div class="sub_panel">
						<div class="side_inner">
							<h4 class="panel_heading"><?php echo Lang::$word->_MY_DETAILS;?></h4>
							<ul>
								<li><a href="index.php?subpage=account_setting"><?php echo Lang::$word->_Account_Settng;?></a></li>
								<li><a href="index.php?subpage=trade_account"><?php echo Lang::$word->_TRADE_ACCOUNT;?></a></li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<a href="#"class="<?php echo ($page=='deposit' || $page=='withdraw' || $page=='internal_transfer') ? "menuleftactive":"";?>"><span class="ion-ios7-briefcase"></span> <span class="nav_title"><?php echo Lang::$word->_CASHIER;?></span></a>
					<div class="sub_panel">
						<div class="side_inner">
							<h4 class="panel_heading"><?php echo Lang::$word->_CASHIER;?></h4>
							<ul>
								<li><a href="index.php?subpage=deposit"><?php echo Lang::$word->_MAKE_A_DEPOSIT;?></a></li>
								<li><a href="index.php?subpage=withdraw"><?php echo Lang::$word->_MAKE_A_WITHDRAW;?></a></li>
								<li><a href="index.php?subpage=internal_transfer"><?php echo Lang::$word->_MAKE_AN_INTERNAL_TRANSFER;?></a></li>
								<!--<li><a href="?page=transection_hist">Transaction History</a></li>-->
							</ul>
						</div>
					</div>
				</li>
			
				<li>
					<a href="#"class="<?php echo ($page=='security_back') ? "menuleftactive":"";?>"><span class="ion-locked"></span> <span class="nav_title"><?php echo Lang::$word->_SECURITY;?></span></a>
					<div class="sub_panel">
						<div class="side_inner">
							<h4 class="panel_heading"><?php echo Lang::$word->_SECURITY;?></h4>
							<ul>
								<li><a href="index.php?subpage=security_back"><?php echo Lang::$word->_SECURITY_SETTINGS;?></a></li>
								<!--<li><a href="?page=pin_code">Pin Code</a></li>
								<li><a href="?page=pass_quest">Password Question</a></li>-->
								<!--<a href="?subpage=test_secure">Test Security Token</a></li>-->
							</ul>
						</div>
					</div>
				</li>
				<li>
					<a href="#" class="<?php echo ($page=='report' || $page=='report_close_position'|| $page=='report_statement'|| $page=='report_log') ? "menuleftactive":"";?>"><span class="fa fa-list"></span> <span class="nav_title"><?php echo Lang::$word->_REPORTS;?></span></a>
					<div class="sub_panel">
						<div class="side_inner">
							<h4 class="panel_heading"><?php echo Lang::$word->_REPORTS;?></h4>
							<ul>
								<li><a href="index.php?subpage=report_new"><?php echo Lang::$word->_REPORTS;?></a></li>
								<li><a href="index.php?subpage=report_close_position"><?php echo Lang::$word->_MY_CLOSED_POSITION;?></a></li>
								<li><a href="index.php?subpage=report_statement"><?php echo Lang::$word->_MY_STATEMENT;?></a></li>
								<li><a href="index.php?subpage=report_log"><?php echo Lang::$word->_MY_ACTIVITY_LOG;?></a></li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<a href="<?php echo SITEURL;?>/logout.php"><span class="ion-power"></span> <span class="nav_title"><?php echo Lang::$word->_LOGOUT;?></span></a>
				</li>
				
			</ul>
		</nav>