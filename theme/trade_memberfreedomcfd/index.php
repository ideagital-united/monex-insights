<?php

require_once (MODPATH . "tradersystem/admin_class.php");
Registry::set('TraderSystem', new TraderSystem($content -> module_data));

$tradersystem = Registry::get("TraderSystem");

define("THEMEURLTRADE",SITEURL . "/theme/trade_member/");

include 'inc_header.php';

$isErr = false;
include_once(MODPATHF . 'tradersystem/define_by_api.php');
if (!isset($_SESSION['codephonebyip']) || $_SESSION['codephonebyip'] == '') {
	$license_key 	= "dyaDLw45oxre"; //QOP1ZHDDFrPB
	$ipaddress 		= $_SERVER['REMOTE_ADDR'];
	$query 			= "https://geoip.maxmind.com/a?l=" . $license_key . "&i=" . $ipaddress;
	$url 			= parse_url($query);
	$host 			= $url["host"];
	$path 			= $url["path"] . "?" . $url["query"];
	$timeout 		= 1;
	$fp 			= fsockopen ($host, 80, $errno, $errstr, $timeout)
	        		or die('Can not open connection to server.');
	if ($fp) {
	  fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
	  $buf = '';
	  while (!feof($fp)) {
	    $buf .= fgets($fp, 128);
	  }
	  $lines = explode("\n", $buf);
	  $data = $lines[count($lines)-1];
	  fclose($fp);
	} else {
		var_dump($data);
	  # enter error handing code here
	}
	$aGetCodePhone = $tradersystem->getdata("*","bk_country_code","where country_abb like '".$data."%'");
	if ($aGetCodePhone) {
		$_SESSION['codephonebyip'] = $aGetCodePhone['country_code'];
	} else {
		$_SESSION['codephonebyip'] = '66';
	}
	
}

$bbb = $tradersystem->getDataAccountGroup();

if (!$_SESSION['usr_dtl']) {
	$usr_dtl 				= $tradersystem->call_curl_get('user_info?cmd=user_info',"GET");
	$_SESSION['usr_dtl'] 	= $usr_dtl['message'];
	
}

$aImg = getValues('id_card_verified,address_verified,id_card_file_url,address_file_url', 'users', "id='".$_SESSION['uid']."'");
$_SESSION['usr_dtl']['user_info']['id_card_verified'] = isset($aImg->id_card_verified) ? $aImg->id_card_verified : 0;
$_SESSION['usr_dtl']['user_info']['address_verified'] = isset($aImg->address_verified) ? $aImg->address_verified : 0;
$_SESSION['usr_dtl']['user_info']['id_card_file_url'] = isset($aImg->id_card_file_url) ? $aImg->id_card_file_url : '';
$_SESSION['usr_dtl']['user_info']['address_file_url'] = isset($aImg->address_file_url) ? $aImg->address_file_url : '';

if(isset($_SESSION['usr_dtl']['user_info']['secure_tool_id'])){
	$secure_otp = $_SESSION['usr_dtl']['user_info']['secure_tool_id']*1;
	if ($secure_otp == 0) {
		$result = $tradersystem->call_curl("verify_secure_tool","POST",array('cmd'=>'verify_secure_tool','secure_tool_id'=>1,'token'=>'token','token_ref'=>'token'));
		
		$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 1;
		$_SESSION['usr_dtl']['user_info']['enable_email'] = 1;
		$_SESSION['usr_dtl']['user_info']['email_verified'] = 1;
	}
}

	
$wallet = $tradersystem->call_curl_get('wallet?cmd=wallet',"GET");

if ($wallet['isSuccess'] == 1) {
	$_SESSION['usr_dtl']['wallet_amt']['amount'] = ($wallet['message']['amount'] * 1);
	
} else {
	$isErr = true;
}


if (!isset($_SESSION['bank_verified']) || $_SESSION['bank_verified'] != 1 ||  $_SESSION['bank_verified'] != 0) {
	$aGetBank 	= $tradersystem->getdata("id,uid ,is_verified","bk_document_user",
					"where uid='".$_SESSION['uid']."' and doc_type='bookbank' and is_verified=1",false);
					
	if ($aGetBank) {
		$_SESSION['bank_verified'] = 1;
	} else {
		$aGetBank 	= $tradersystem->getdata("id,uid ,is_verified","bk_document_user",
					"where uid='".$_SESSION['uid']."' and doc_type='bookbank' and is_verified=0");
		if ($aGetBank) {
			$_SESSION['bank_verified'] = 0;
		} else {
			$_SESSION['bank_verified'] = 2;
		}
	}
}



if (($tradersystem->check_premission_traderroom() == 1) && $isErr == false && $row->module_name && $modfile = Content::getModuleTheme($row->module_name)) 
{
	
	include 'inc_menu_top.php';
	
	
	$page = (isset($_GET["subpage"]) && $_GET["subpage"] != '') ? $_GET["subpage"] : '';	
	if ($page !='') {
		require_once (MODPATHF . $content->_url[1] . "/".$page.".php");
	} else {
		require_once (MODPATHF . $content->_url[1] . "/main.php");
	}

	include 'inc_menu_left.php'; 
	include 'inc_footer.php'; 

} else {
	$errmsg = Lang::$word->_UA_ACC_ERR1;
	include 'msg.php';
	redirect_to("/logout.php");
	exit;
    
}

?>
