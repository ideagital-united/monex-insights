<?php
  /**
   * Login Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: login.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  if ($user->logged_in)
      redirect_to(doUrl(false, $core->account_page, "page"));
	  
  if (isset($_POST['doLogin']))
      : $result = $user->login($_POST['username'], $_POST['password']);
  /* Login Successful */
  if ($result)
      : redirect_to(doUrl(false, $core->account_page, "page"));
  endif;
  endif;
?>
<div id="login">
  <div class="columns">
    <div class="screen-50 phone-100 push-center">
      <ul class="prolific tabs">
        <li><a data-tab="#signin" class="active"><?php echo Lang::$word->_UA_TITLE2;?></a></li>
        <li> <a data-tab="#reset"><?php echo Lang::$word->_UA_TITLE3;?></a></li>
      </ul>
      <div id="signin" class="prolific tab content">
        <div class="prolific bottom attached segment">
          <p><i class="information icon"></i> <?php echo Lang::$word->_UA_SUBTITLE2;?></p>
          <form method="post" id="login_form" name="login_form" class="prolific form">
            <div class="columns">
              <div class="screen-30">
                <div class="field">
                  <label><?php echo Lang::$word->_UA_TITLE2;?> <i class="small icon asterisk"></i></label>
                </div>
              </div>
              <div class="screen-70">
                <div class="field">
                  <input name="username" placeholder="<?php echo Lang::$word->_UA_TITLE2;?>" type="text">
                </div>
              </div>
              <div class="screen-30">
                <div class="field">
                  <label><?php echo Lang::$word->_PASSWORD;?> <i class="small icon asterisk"></i></label>
                </div>
              </div>
              <div class="screen-70">
                <div class="field">
                  <input name="password" placeholder="<?php echo Lang::$word->_PASSWORD;?>" type="password">
                </div>
              </div>
              <div class="content-right"> <a href="<?php echo doUrl(false, $core->register_page, "page");?>" class="right-space"><?php echo Lang::$word->_UA_CLICKTOREG;?></a>
                <button name="submit" type="submit" class="prolific positive button"><?php echo Lang::$word->_UA_LOGINNOW;?></button>
              </div>
              <input name="doLogin" type="hidden" value="1">
            </div>
          </form>
          <?php print Filter::$showMsg;?> </div>
      </div>
      <div id="reset" class="prolific tab content">
        <div class="prolific bottom attached segment">
          <p><i class="information icon"></i> <?php echo Lang::$word->_UA_SUBTITLE3;?></p>
          <form id="prolific_form" name="prolific_form" method="post" class="prolific form">
            <div class="columns">
              <div class="screen-30">
                <div class="field">
                  <label><?php echo Lang::$word->_USERNAME;?> <i class="small icon asterisk"></i></label>
                </div>
              </div>
              <div class="screen-70">
                <div class="field">
                  <input name="uname" placeholder="<?php echo Lang::$word->_USERNAME;?>" type="text">
                </div>
              </div>
              <!--
              <div class="screen-30">
                <div class="field">
                  <label><?php echo Lang::$word->_UR_EMAIL;?> <i class="small icon asterisk"></i></label>
                </div>
              </div>
              <div class="screen-70">
                <div class="field">
                  <input name="email" placeholder="<?php echo Lang::$word->_UR_EMAIL;?>" type="text">
                </div>
              </div>
              -->
              <div class="screen-30"style="display:none;">
              <div class="field">
                <label>Select Question <i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70"style="display:none;">
              <div class="field">
                <select name="question_pwd">
            	<option value="Best childhood friend?">Best childhood friend?</option>
				<option value="Anniversary?">Anniversary?</option>
				<option value="Favorite teacher?">Favorite teacher?</option>
				<option value="Favorite historical person?">Favorite historical person?</option>
				<option value="Name of first pet?">Name of first pet?</option>
            </select>
              </div>
            </div>
              <div class="screen-30"  style="display:none;">
              <div class="field">
                <label>Answer<i class="small icon asterisk"></i></label>
              </div>
            </div>
            <div class="screen-70"  style="display:none;">
              <div class="field">
                <input name="answer_pwd" placeholder="Answer" type="text">
              </div>
            </div>
              <div class="screen-30">
                <div class="field">
                  <label><?php echo Lang::$word->_UA_PASS_RTOTAL;?> <i class="small icon asterisk"></i></label>
                </div>
              </div>
              <div class="screen-70">
                <div class="field">
                  <label class="input"><img src="<?php echo SITEURL;?>/captcha.php" alt="" class="captcha-append" /> <i class="icon-prepend icon unhide"></i>
                    <input name="captcha" placeholder="<?php echo Lang::$word->_UA_PASS_RTOTAL;?>" type="text">
                  </label>
                </div>
              </div>
            </div>
            <button data-url="/ajax/user.php" type="button" name="dosubmit" class="prolific danger button"><?php echo Lang::$word->_UA_PASS_RSUBMIT;?></button>
            <input name="passReset" type="hidden" value="1">
          </form>
        </div>
        <div id="msgholder"></div>
      </div>
    </div>
  </div>
</div>