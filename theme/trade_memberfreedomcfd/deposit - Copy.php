
			<div class="page_content">
				<div class="container-fluid">
					<div class="row user_profile">
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading"><h3>SECURITY TYPE</h3></div>
								<div class="panel-body">
									Select the general security type for your Personal Area, which will apply to all of your trading accounts.
									<div class="row">
										<div class="col-sm-12">
											<div class="row bs_switch_wrapper">
												<div class="col-sm-12 col-md-12">
													<h4>E-mail - low security</h4>
													<input type="radio" class="bs_switch bs_switch_radio" name="radio_switch1" value="option 1">
													<span class="help-block">For each operation, an email containing a unique confirmation code is sent to the email address entered when you registered your Personal Area </span>
													
												</div>
												<div class="col-sm-12 col-md-12">
													<h4>Mobile phone - medium security</h4>
													<input type="radio" class="bs_switch bs_switch_radio" name="radio_switch2" value="option 2">
													<span class="help-block">For each operation, a text message containing a unique confirmation code is sent to the mobile phone number entered when you registered your Personal Area .</span>
												</div>
												<div class="col-sm-12 col-md-12">
													<h4>Keyfile - high security</h4>
													<input type="radio" class="bs_switch bs_switch_radio" name="radio_switch3" value="option 3">
													<span class="help-block">A special User Code is required to confirm each operation. The User Code must be generated using the program, key files, and key file password received during registration of your Personal Area.</span>
												</div>

											</div>
										</div>
									</div>
									<div class="row">
										<div class="text-center">
											<button class="btn btn-success"><i class="fa fa-save"></i> Save profile</button>
											<button class="btn btn-default"><i class="fa fa-trash-o fa-lg"></i> Delete</button>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		</div>