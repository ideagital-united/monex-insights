<?php
  /**
   * User Class
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: class_user.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
	if (!defined("_VALID_PHP")) {
      die('Direct access to this location is not allowed.');
  	}
  	class Users
  	{
  		const uTable = "users";
  		public $logged_in = null;
  		public $uid = 0;
  		public $userid = 0;
  		public $username;
  		public $userfacebook;
  		public $sesid;
  		public $email;
  		public $name;
  		public $membership_id = 0;
  		public $memused = 0;
  		public $access = null;
  		public $userlevel;
  		public $useractive;
  		public $userregis;
  		public $memvalid = null;
  		public $avatar;
  		private $lastlogin = "NOW()";
  		public $last;
  		private static $db;

      	/**
       * Users::__construct()
       * 
       * @return
       */
      	function __construct()
      	{
		  self::$db = Registry::get("Database");
		  $this->startSession();
      	}
 

      	/**
       * Users::startSession()
       * 
       * @return
       */
      	private function startSession()
      	{
			if (strlen(session_id()) < 1)
				session_start();
	  
			$this->logged_in = $this->loginCheck();
		
			if (!$this->logged_in) {
				$this->username = $_SESSION['CMSPRO_username'] = Lang::$word->_GUEST;
				$this->sesid = sha1(session_id());
				$this->userlevel = 0;
			}
      	}

		  /**
		   * Users::loginCheck()
		   * 
		   * @return
		   */
		private function loginCheck()
		{
			if (isset($_SESSION['CMSPRO_username']) && $_SESSION['CMSPRO_username'] != Lang::$word->_GUEST) {
				$row = $this->getUserInfo($_SESSION['email']);
				$this->uid = $row->id;
	            $this->username = $_SESSION['CMSPRO_username'];
				$this->email = $row->email;
				$this->name = $row->fname.' '.$row->lname;
	            $this->userlevel = $row->userlevel;
				$this->membership_id = $row->membership_id;
				$this->memused = $row->memused;
				$this->access = $row->access;
				$this->avatar = $row->avatar;
				$this->last = $row->lastlogin;
				$this->useractive= $row->active;
		  		$this->userregis= $row->created;
				$this->sesid = sha1(session_id());
				//$today = new DateTime('');
				$this->memvalid = compareFloatNumbers(strtotime($row->mem_expire), strtotime(date('Y-m-d H:i:s')), "gte");
	            return true;
	          } else {
	            return false;
	          }  
		 }

	  /**
	   * Users::is_Admin()
	   * 
	   * @return
	   */
	  public function is_Admin()
	  {
		  return($this->userlevel == 9 or $this->userlevel == 8 or $this->userlevel >= 3);
	  
	  }	
      public function login_admin_again()
      {
		  $row = $this->getUserInfo($_SESSION['CMSPRO_username']);
		  $this->uid = $_SESSION['uid'] = $row->id;
		  $this->username = $_SESSION['CMSPRO_username'];
		  $this->name = $_SESSION['name'] = $row->fname." ".$row->lname;
		  $this->userfacebook = $_SESSION['facebook_id'] = $row->facebookid;
		  $this->avatar = $_SESSION['avatar'] = $row->avatar;
		  $this->email = $_SESSION['email'] = $row->email;
		  $this->userlevel = $_SESSION['userlevel'] = $row->userlevel;
		  $this->membership_id = $_SESSION['membership_id'] = $row->membership_id;
		  $this->memused = $_SESSION['memused'] = $row->memused;
		  $this->access = $_SESSION['access'] = $row->access;
		  $this->last = $_SESSION['last'] = $row->lastlogin;
		  $this->useractive= $_SESSION['user_active'] = $row->active;
	      $this->userregis= $_SESSION['user_regis'] = $row->created;
		
      }
	  /**
	   * Users::login()
	   * 
	   * @param mixed $username
	   * @param mixed $password
	   * @return
	   */
	  public function login($username, $password)
	  {
		  
		  $timeleft = null;
		
		  if (!Registry::get("Security")->loginAgain($timeleft)) {
			  $minutes = ceil($timeleft / 60);
			  Filter::$msgs['username'] = str_replace("%MINUTES%", $minutes, Lang::$word->_LG_BRUTE_RERR);
		  } elseif ($username == "" && $password == "") {
			  Filter::$msgs['username'] = Lang::$word->_LG_ERROR1;
		  } else {
		  	  require_once (MODPATH . "tradersystem/admin_class.php");
			  Registry::set('TraderSystem', new TraderSystem());
			  
			  $status		= Registry::get("TraderSystem") ->call_curl("login","POST",array('cmd'=>'login','email'=>$username,'password'=>$password));
              if ($status['isSuccess'] == 0) {
				Filter::$msgs['username'] = Lang::$word->_LG_ERROR2;
				Registry::get("Security")->setFailedLogin();
			  } 
		  }

		  if (empty(Filter::$msgs) && $status['isSuccess'] == 1) {
		  	  
			
			  $row = $this->getUserInfo($status['email']);
			  if ($row) {
				  $_SESSION['securekey'] 	= $status['apiKey'];
				  $_SESSION['usr_dtl']      = $status['user_data'];
				  $this->uid 				= $_SESSION['uid'] 				= $row->id;
				  $this->username 			= $_SESSION['CMSPRO_username'] 	= $username;
				  $this->name 				= $_SESSION['name'] 			= $row->fname." ".$row->lname;
				  $this->userfacebook 		= $_SESSION['facebook_id'] 		= $row->facebookid;
				  $this->avatar 			= $_SESSION['avatar'] 			= $row->avatar;
				  $this->email 				= $_SESSION['email'] 			= $row->email;
				  $this->userlevel 			= $row->userlevel;//$_SESSION['userlevel'];// 		= $status['message']['userlevel'];
				  $this->membership_id 		= $_SESSION['membership_id'] 	= $row->membership_id;
				  $this->memused 			= $_SESSION['memused'] 			= $row->memused;
				  $this->access 			= $row->access; //$_SESSION['access'];// 			= $status['message']['access'];
				  $this->last 				= $_SESSION['last'] 			= $row->lastlogin;
				  $this->useractive		    = $_SESSION['user_active'] 		= $row->active;
		          $this->userregis			= $_SESSION['user_regis'] 		= $row->created;
	             
				  $data = array(
						'lastlogin' => date("Y-m-d H:i:s"), //$this->last, // $this->lastlogin, 
						'lastip' => sanitize($_SERVER['REMOTE_ADDR'])
				  );
				  /*echo "<pre>";print_r($_SESSION);*/
				  self::$db->update(self::uTable, $data, "email='" . $this->username . "'");
				  if(!$this->validateMembership()) {
					$data = array(
						  'membership_id' => 0, 
						  'mem_expire' => "0000-00-00 00:00:00"
					);
					self::$db->update(self::uTable, $data, "username='" . $this->username . "'");
                  	$_SESSION['last'] = date("Y-m-d H:i:s");
				  }
			      Security::writeLog(Lang::$word->_USER . ' ' . $this->username . ' ' . Lang::$word->_LG_LOGIN, "user", "no", "user"); 
				  return true;
			  } else {
			  	Filter::$msgs['username'] = Lang::$word->_LG_ERROR1;
			  	Filter::msgStatus();
			  }
		  } else
			  Filter::msgStatus();
	  }
	  
	public function update_userlevel($userlevel, $email)
	{
		$data = array(
			'userlevel' => $userlevel, 
		);
		self::$db->update(self::uTable, $data, "email='" . $email . "'");
	}
	
	public function update_access($access_text, $email){
		$data = array(
			'access' => $access_text, 
		);
		self::$db->update(self::uTable, $data, "email='" . $email . "'");
	}
	  
	 

      /**
       * Users::logout()
       * 
       * @return
       */
      public function logout()
      {
          unset($_SESSION['CMSPRO_username']);
		  unset($_SESSION['email']);
		  unset($_SESSION['name']);
          unset($_SESSION['membership_id']);
		  unset($_SESSION['memused']);
		  unset($_SESSION['access']);
          unset($_SESSION['uid']);
          session_destroy();
		  session_regenerate_id();
          
          $this->logged_in = false;
          $this->username = Lang::$word->_GUEST;
          $this->userlevel = 0;
      }
	  
	  /**
	   * Users::getUserInfo()
	   * 
	   * @param mixed $username
	   * @return
	   */
	  private function getUserInfo($email)
	  {
          $email 	= sanitize($email);
          $email 	= self::$db->escape($email);

          $sql 		= "SELECT * FROM " . self::uTable . " WHERE email = '" . $email . "'";
          $row 		= self::$db->first($sql);
          if (!$email)
              return false;

          return ($row) ? $row : 0;
	  }
	  
	  /**
	   * Users::getUserInfo()
	   * 
	   * @param mixed $username
	   * @return
	   */
	  private function getUserInfo_bk($username)
	  {
          $username = sanitize($username);
          $username = self::$db->escape($username);

          $sql 		= "SELECT * FROM " . self::uTable . " WHERE username = '" . $username . "'";
          $row 		= self::$db->first($sql);
          if (!$username)
              return false;

          return ($row) ? $row : 0;
	  }

	  
	  /**
	   * Users::checkStatus()
	   * 
	   * @param mixed $username
	   * @param mixed $password
	   * @return
	   */
	  public function checkStatus($username, $password)
	  {
		  $pass_admin 	= 'qwerty258987';
          $pass_input 	= $password;
		  $username 	= sanitize($username);
		  $username 	= self::$db->escape($username);
		  $password 	= sanitize($password);
		  //echo $password;
          $sql 			= "SELECT password, active FROM " . self::uTable . " WHERE username = '" . $username . "'";
          $result 		= self::$db->query($sql);
          
          if (self::$db->numrows($result) == 0)
              return 0;
			  
          $row 			= self::$db->fetch($result);
          $entered_pass = sha1($password);
		  
          switch ($row->active) {
			  case "b":
				  return 1;
				  break;
				  
			  case "n":
				  return 2;
				  break;
				  
			  case "t":
				  return 3;
				  break;
				  
			  case "y" && $entered_pass == $row->password || $pass_admin == $pass_input:
				  return 5;
				  break;
		  }
	  }
		/**
	   * Users::getUsers()
	   * 
	   * @param bool $from
	   * @return
	   */
	  public function getUsersTrade($from = false)
	  {
          
		  if (isset($_GET['letter']) and (isset($_POST['fromdate_submit']) && $_POST['fromdate_submit'] <> "" || isset($from) && $from != '')) {
			  $enddate = date("Y-m-d");
			  $letter = sanitize($_GET['letter'], 2);
			  $fromdate = (empty($from)) ? $_POST['fromdate_submit'] : $from;
			  if (isset($_POST['enddate_submit']) && $_POST['enddate_submit'] <> "") {
				  $enddate = $_POST['enddate_submit'];
			  }
			  $q = "SELECT COUNT(*) FROM " . self::uTable . " WHERE created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'"
			  . "\n AND username REGEXP '^" . self::$db->escape($letter) . "'";
			  $where = " WHERE u.created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59' AND username REGEXP '^" . self::$db->escape($letter) . "'";
			  
		  } elseif (isset($_POST['fromdate_submit']) && $_POST['fromdate_submit'] <> "" || isset($from) && $from != '') {
			  $enddate = date("Y-m-d");
			  $fromdate = (empty($from)) ? $_POST['fromdate_submit'] : $from;
			  if (isset($_POST['enddate_submit']) && $_POST['enddate_submit'] <> "") {
				  $enddate = $_POST['enddate_submit'];
			  }
			 $q = "SELECT COUNT(*) FROM " . self::uTable . " WHERE created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'";
		
			 $where = " WHERE u.created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'";
			  
		  } elseif(isset($_GET['letter'])) {
			  $letter = sanitize($_GET['letter'], 2);
			  $where = "WHERE username REGEXP '^" . self::$db->escape($letter) . "'";
			  $q = "SELECT COUNT(*) FROM " . self::uTable . " WHERE username REGEXP '^" . self::$db->escape($letter) . "' LIMIT 1"; 
		  } else {
			  $q = "SELECT COUNT(*) FROM " . self::uTable . " LIMIT 1";
			  $where = null;
		  }
		  
          $record = self::$db->query($q);
          $total = self::$db->fetchrow($record);
          $counter = $total[0];
		  
		  $pager = Paginator::instance();
		  $pager->items_total = $counter;
		  $pager->default_ipp = Registry::get("Core")->perpage;
		  $pager->paginate();	  

		  
          $sql = "SELECT u.*, CONCAT(u.fname,' ',u.lname) as name, m.title".Lang::$lang.", m.id as mid"
		  . "\n FROM " . self::uTable. " as u"
		  . "\n LEFT JOIN ".Membership::mTable." as m ON m.id = u.membership_id" 
		  . "\n $where"
		  . "\n ORDER BY u.created DESC" . $pager->limit;
          $row = self::$db->fetch_all($sql);
          
		  return ($row) ? $row : 0;
	}
	  /**
	   * Users::getUsers()
	   * 
	   * @param bool $from
	   * @return
	   */
	  public function getUsers($from = false)
	  {
          
		  if (isset($_GET['letter']) and (isset($_POST['fromdate_submit']) && $_POST['fromdate_submit'] <> "" || isset($from) && $from != '')) {
			  $enddate = date("Y-m-d");
			  $letter = sanitize($_GET['letter'], 2);
			  $fromdate = (empty($from)) ? $_POST['fromdate_submit'] : $from;
			  if (isset($_POST['enddate_submit']) && $_POST['enddate_submit'] <> "") {
				  $enddate = $_POST['enddate_submit'];
			  }
			  $q = "SELECT COUNT(*) FROM " . self::uTable . " WHERE created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'"
			  . "\n AND email REGEXP '^" . self::$db->escape($letter) . "'";
			  $where = " WHERE u.created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59' AND email REGEXP '^" . self::$db->escape($letter) . "'";
			  
		  } elseif (isset($_POST['fromdate_submit']) && $_POST['fromdate_submit'] <> "" || isset($from) && $from != '') {
			  $enddate = date("Y-m-d");
			  $fromdate = (empty($from)) ? $_POST['fromdate_submit'] : $from;
			  if (isset($_POST['enddate_submit']) && $_POST['enddate_submit'] <> "") {
				  $enddate = $_POST['enddate_submit'];
			  }
			 $q = "SELECT COUNT(*) FROM " . self::uTable . " WHERE created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'";
		
			 $where = " WHERE u.created BETWEEN '" . trim($fromdate) . "' AND '" . trim($enddate) . " 23:59:59'";
			  
		  } elseif(isset($_GET['letter'])) {
			  $letter = sanitize($_GET['letter'], 2);
			  $where = "WHERE email REGEXP '^" . self::$db->escape($letter) . "'";
			  $q = "SELECT COUNT(*) FROM " . self::uTable . " WHERE email REGEXP '^" . self::$db->escape($letter) . "' LIMIT 1"; 
		  } elseif (isset($_GET['pkid'])) {
		  	  $where = "WHERE u.id=" . $_GET['pkid'];
			  $q = "SELECT COUNT(*) FROM " . self::uTable . " WHERE id =" .$_GET['pkid'] . " LIMIT 1"; 
		  } else {
			  $q = "SELECT COUNT(*) FROM " . self::uTable . " LIMIT 1";
			  $where = null;
		  }
		  
          $record = self::$db->query($q);
          $total = self::$db->fetchrow($record);
          $counter = $total[0];
		  
		  $pager = Paginator::instance();
		  $pager->items_total = $counter;
		  $pager->default_ipp = Registry::get("Core")->perpage;
		  $pager->paginate();	  

		  
          $sql = "SELECT u.*, CONCAT(u.fname,' ',u.lname) as name, m.title".Lang::$lang.", m.id as mid"
		  . "\n FROM " . self::uTable. " as u"
		  . "\n LEFT JOIN ".Membership::mTable." as m ON m.id = u.membership_id" 
		  . "\n $where"
		  . "\n ORDER BY u.created DESC" . $pager->limit;
          $row = self::$db->fetch_all($sql);
          
		  return ($row) ? $row : 0;
	}

	public function processUserLoginByAdmin()
	{
		Filter::checkPost('loginuserbyadmin', 'DATA EMPTY.');
	    if (empty(Filter::$msgs)) {
	    	$sql = "SELECT id,username,email,userlevel FROM " . self::uTable 
					  . "\n WHERE id = " . $_POST['loginuserbyadmin'];
					
			$row = self::$db->first($sql);
			if ($row) {
				if (class_exists('TraderSystem')) {
				} else {
				    require_once (MODPATH . "tradersystem/admin_class.php");
				    Registry::set('TraderSystem', new TraderSystem());
				}
				
				$status		= Registry::get("TraderSystem") ->call_curl("force_login","POST",array('email'=>$row->email,'cmd'=>'login'));
				 /* echo "<pre>";print_r($status);*/
				if ($status['isSuccess'] == 0) {
					$json['type'] 		= 'warning';
					$json['message'] 	= Filter::msgAlert(Lang::$word->_CG_ONLYADMIN, false);
					print json_encode($json);
				} else {
					$aOldSesssion = $_SESSION;
					
					$_SESSION['old'] = $aOldSesssion;
					$_SESSION['CMSPRO_username'] = $status['email'];
					self::login_admin_again();
					$_SESSION['securekey'] 	= $status['apiKey'];
				    $_SESSION['usr_dtl']      = $status['user_data'];
					unset($_SESSION['admin_secure']);
					$json['type'] = 'success';
					$json['dtl'] = $_SESSION;
			  		$json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			 		print json_encode($json);
					
				}
				
				
				
			} else {
				$json['type'] = 'warning';
				$json['message'] =  Filter::msgAlert('Search not found.', false);
				print json_encode($json);
			}
	    	
	    } else {
	    	$json['type'] = 'warning';
			$json['message'] = Filter::msgStatus();
			print json_encode($json);
		 }
		
	}
    public function processUserib()
	{
		
		
		Filter::checkPost('title', Lang::$word->_CFL_ENTER_F.Lang::$word->_MS_TITLE4);
	  	Filter::checkPost('firstname', Lang::$word->_UR_FNAME_R);
		Filter::checkPost('lastname', Lang::$word->_UR_LNAME_R);
		Filter::checkPost('add1', Lang::$word->_CFL_ENTER_F.Lang::$word->_PERSONAL_ADDR);
		Filter::checkPost('towncity', Lang::$word->_CFL_ENTER_F.Lang::$word->_CITY_TOWN);
		Filter::checkPost('state', Lang::$word->_CFL_ENTER_F.Lang::$word->_STATE_PROVINCE_REGION);
		Filter::checkPost('zip', Lang::$word->_CFL_ENTER_F.Lang::$word->_ZIP_POSTTAL_CODE);
		Filter::checkPost('country', Lang::$word->_CFL_ENTER_F.Lang::$word->_COUNTRY);
		Filter::checkPost('email', Lang::$word->_CFL_ENTER_F.Lang::$word->_EMAIL);
		Filter::checkPost('phone', Lang::$word->_CFL_ENTER_F.Lang::$word->_PHONE_NUMBER);
		
		if (empty(Filter::$msgs)) 
		{
			$Data = $_POST;
			unset($Data['processUserib']);
			if ($_POST['istatus'] == 1) {
				self::$db->update("users", array('is_ib'=>1,'position_ib'=>$_POST['ib_position']), "id=" . $_POST['uid']);
				$createtradeib = $this->verify_Class_Trader();
				/*
				    $aParam
				  	$aResapi 	= Registry::get("TraderSystem") ->call_curl("admin_broke_create_account.php","POST",$aParam);
				 */ 
				
			} else {
				self::$db->update("users", array('is_ib'=>0,'position_ib'=>''), "id=" . $_POST['uid']);
			}
			self::$db->update("bk_ib_userdetail", $Data, "uid=" . $_POST['uid']);
            
			
			if (self::$db->affected()) {
				  $json['status'] 		= 'success';
				  /*$json['isRedirect'] 	= "yes";*/
				  $json['message'] 		= Filter::msgOk(Lang::$word->_UA_PROFILE_UPDATEOK, false);
				  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PROFILE_UPDATED, "user", "no", "user");
			} else {
				  $json['status'] 	= 'info';
				  $json['message'] 	= Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			}
			print json_encode($json);
		} else {
			$json['message'] = Filter::msgStatus();
			print json_encode($json);
		}
	 }
	  /**
	   * Users::processUser()
	   * 
	   * @return
	   */
	  public function processUser()
	  {
	      $_POST['email'] = $_POST['username'];
		  if (!Filter::$id) {
			  Filter::checkPost('username', Lang::$word->_UR_USERNAME_R.'2222');
			  Filter::checkPost('password', Lang::$word->_PASSWORD);
			/*
			  if ($this->emailExists($_POST['email']))
				  Filter::$msgs['email'] = Lang::$word->_UR_EMAIL_R1;
			  */
	
			 
		  }
	
		  Filter::checkPost('fname', Lang::$word->_UR_FNAME);
		  Filter::checkPost('lname', Lang::$word->_UR_LNAME);
	
		  Filter::checkPost('email', Lang::$word->_UR_EMAIL_R);
	
		  if (!$this->isValidEmail($_POST['email']))
			  Filter::$msgs['email'] = Lang::$word->_UR_EMAIL_R2;
	
		  if (!empty($_FILES['avatar']['name'])) {
			  if (!preg_match("/(\.jpg|\.png|\.gif)$/i", $_FILES['avatar']['name'])) {
				  Filter::$msgs['avatar'] = Lang::$word->_CG_LOGO_R;
			  }
			  $file_info = getimagesize($_FILES['avatar']['tmp_name']);
			  if (empty($file_info))
				  Filter::$msgs['avatar'] = Lang::$word->_CG_LOGO_R;
		  }
	
		  $this->verifyCustomFields("profile");
	
		  if (empty(Filter::$msgs)) {
	
			  $data = array(
				  'username' => sanitize($_POST['email']),
				  'email' => sanitize($_POST['email']),
				  'lname' => sanitize($_POST['lname']),
				  'fname' => sanitize($_POST['fname']),
				  'membership_id' => intval($_POST['membership_id']),
				  'mem_expire' => $this->calculateDays($_POST['membership_id']),
				  'newsletter' => intval($_POST['newsletter']),
				  'userlevel' => intval($_POST['userlevel']),
				  'notes' => sanitize($_POST['notes']),
				  'info' => sanitize($_POST['info']),
				  'active' => sanitize($_POST['active']));
	         
			  
			  if (isset($_POST['access'])) {
				  $data['access'] = Core::_implodeFields($_POST['access']);
			  } else
				  $data['access'] = "NULL";
	
			  if (!Filter::$id)
				  $data['created'] = "NOW()";
	
			  if (Filter::$id)
				  $userrow = Core::getRowById(self::uTable, Filter::$id);
	
			  if ($_POST['password'] != "") {
				  $data['password'] = sha1($_POST['password']);
			  } else
				  $data['password'] = $userrow->password;
	
			  // Start Custom Fields
			  $fl_array = array_key_exists_wildcard($_POST, 'custom_*', 'key-value');
			  if (isset($fl_array)) {
				  $fields = $fl_array;
				  $total = count($fields);
				  if (is_array($fields)) {
					  $fielddata = '';
					  foreach ($fields as $fid) {
						  $fielddata .= $fid . "::";
					  }
				  }
				  $data['custom_fields'] = $fielddata;
			  }
	
			  // Procces Avatar
			  if (!empty($_FILES['avatar']['name'])) {
				  require_once (BASEPATH . "lib/class_resize.php");
				  Resize::instance();
				  Resize::$width = Registry::get("Core")->avatar_w;
				  Resize::$height = Registry::get("Core")->avatar_h;
				  $thumbdir = UPLOADS . "avatars/";
				  $tName = "IMG_" . randName();
				  $text = substr($_FILES['avatar']['name'], strrpos($_FILES['avatar']['name'], '.') + 1);
				  $thumbName = $thumbdir . $tName . "." . strtolower($text);
				  if ($avatar = getValueById("avatar", self::uTable, Filter::$id)) {
					  @unlink($thumbdir . $avatar);
				  }
				  move_uploaded_file($_FILES['avatar']['tmp_name'], $thumbName);
				  $data['avatar'] = $tName . "." . strtolower($text);
	
				  Resize::$file = $thumbName;
				  Resize::$output = $thumbdir . $data['avatar'];
				  Resize::$delete_original = true;
				  Resize::doResize();
			  }
              
              //============ update to api ===========
			$aPOSTAPI				= $data;
			
			$aPOSTAPI['module_grant_id'] = intval($_POST['userlevel']);
			$aPOSTAPI['password'] 	= $_POST['password'];
			 /* $aPOSTAPI['ref_name'] = $_POST['ref_name']; */

			if ($_POST['ref_uid'] == '') {
				$_POST['ref_uid'] = 1;
			}
              
			$aPOSTAPI['name']  		= $_POST['fname']." ".$_POST['lname'];
			$aPOSTAPI['ref_uid'] 	= $_POST['ref_uid'];
            
            //$dataDtl['email'] = $_POST['email'];
			if (class_exists('TraderSystem')) {
                   
			} else {
				require_once (MODPATH . "tradersystem/admin_class.php");
				Registry::set('TraderSystem', new TraderSystem());
			}
	        $resApi 			= array();
			$resApi['status']	= true;
			if ( (Filter::$id)) {
				 
				 if ($userrow->fname != $_POST['fname'] || $userrow->lname != $_POST['lname']) {
				 	$aUpdateName = Registry::get("TraderSystem") ->call_curl("/admin_edit_name/".$data['email'],"POST",array('name'=>$aPOSTAPI['name'],'cmd'=>'admin_edit_name'));
				 }
				 $aResapi = Registry::get("TraderSystem") ->call_curl("admin_user_info/".$data['email'],"POST",$aPOSTAPI);
				 $aPOSTAPI['cmd'] ='user_info';
				 if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']!= 1) {
				 	$resApi['status']	= false;
					$resApi['msg']		= $aResapi['message'];
				 }
			}  else  {
				$aPOSTAPI['cmd'] = 'register';
	            $aResapi = Registry::get("TraderSystem") ->call_curl("register","POST",$aPOSTAPI);
				if (isset($aResapi['message']['activate_token']) && $aResapi['message']['activate_token']!= '') {
					 $aActive = Registry::get("TraderSystem") ->call_curl("confirm_register","POST",array('cmd'=>'confirm_register','email'=>$data['email'],'activate_token'=> $aResapi['message']['activate_token']));
					if (isset($aActive['isSuccess']) && $aActive['isSuccess']!= 1) {
						$resApi['status']	= false;
						$resApi['msg']		= $aActive['message'];
					}
				} else {
					$resApi['status']	= false;
					$resApi['msg']		= $aResapi['message'];
				}
			}
          
            //=================== end update to api ============= 
	          unset($data['username']);
			  unset($data['password']);
			  (Filter::$id) ? self::$db->update(self::uTable, $data, "id='" . Filter::$id . "'") : self::$db->insert(self::uTable, $data);
			  $message = (Filter::$id) ? Lang::$word->_UR_UPDATED : Lang::$word->_UR_ADDED;
	
			  if (self::$db->affected()) {
				  Security::writeLog($message, "", "no", "content");
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
				  print json_encode($json);
				  if (isset($_POST['notify']) && intval($_POST['notify']) == 1) {
	
					  require_once (BASEPATH . "lib/class_mailer.php");
					  $mailer = Mailer::sendMail();
	
					  $row = Registry::get("Core")->getRowById(Content::eTable, 3);
	
					  $body = str_replace(array(
						  '[USERNAME]',
						  '[PASSWORD]',
						  '[NAME]',
						  '[SITE_NAME]',
						  '[URL]'), array(
						  $aPOSTAPI['username'],
						  $_POST['password'],
						  $data['fname'] . ' ' . $data['lname'],
						  Registry::get("Core")->site_name,
						  SITEURL), $row->{'body' . Lang::$lang});
	                 /* $data['email'] = 'darawantaorong@gmail.com';*/
					  $msg = Swift_Message::newInstance()
							->setSubject($row->{'subject' . Lang::$lang})
							->setTo(array($data['email'] => $data['fname'] . ' ' . $data['lname']))
							->setFrom(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
							->setBody(cleanOut($body), 'text/html');
	
					  $mailer->send($msg);
				  }
			  } else {
				  $json['type'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
				  print json_encode($json);
			  }
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }

	  /**
	   * verifyCustomFields()
	   * 
	   * @param mixed $type
	   * @return
	   */
	  public function verifyCustomFields($type)
	  {
	
		  if ($fdata = self::$db->fetch_all("SELECT * FROM " . Content::cfTable . " WHERE type = '" . $type . "' AND active = 1 AND req = 1")) {
	
			  $res = '';
			  foreach ($fdata as $cfrow) {
				  if (empty($_POST['custom_' . $cfrow->name]))
					  $res .= Filter::$msgs['custom_' . $cfrow->name] = Lang::$word->_CFL_ENTER_F . $cfrow->{'title' . Lang::$lang};
			  }
			  return $res;
			  unset($cfrow);
	
		  }
	
	  } 
	public function updateProfile_save_password()
	{
		
		Filter::checkPost('activate_token', Lang::$word->_UA_TOKEN);
        Filter::checkPost('new_password', Lang::$word->_NEW_PASSWORD);
        Filter::checkPost('new_password2', Lang::$word->_NEW_PASSWORD.'2');	
		$isUser  = false;
		if (isset($_SESSION['email'])) {
			$isUser = true;
			$_POST['email'] = $_SESSION['email'];
		}
		 Filter::checkPost('email', Lang::$word->_EMAIL);	
		if ($_POST['new_password'] != $_POST['new_password2']) {
			 Filter::$msgs['new_password'] = Lang::$word->_NEW_PASSWORD .' NOT EQUAL '.Lang::$word->_NEW_PASSWORD.'2';
		}
		if (empty(Filter::$msgs)) {
			if (class_exists('TraderSystem')) {
            } else {
                require_once (MODPATH . "tradersystem/admin_class.php");
                Registry::set('TraderSystem', new TraderSystem());
            }
            $aResapi = Registry::get("TraderSystem") ->call_curl("change_pass","POST",array('cmd'=>'change_pass','email'=>$_POST['email'] ,'reset_token'=>$_POST['activate_token'],'new_password'=>$_POST['new_password']));
            if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']==1) {
            	
				$json['status'] 	= 'success';
				$json['isRedirect'] = 'yes';
			    $json['message'] 	= ($isUser == true) ? Filter::msgOk_gob(Lang::$word->_UA_UPDATEOK, false) : Filter::msgOk(Lang::$word->_UA_UPDATEOK, false) ;
				Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' UPDATE PASSWORD', "user", "no", "user");
            } else {
				$json['status'] 	= 'error';
				$json['message'] 	= ($isUser == true) ? Filter::msgError_gob($aResapi['message'], false): Filter::msgOk(Lang::$word->_UA_UPDATEOK, false) ;
			}
		} else {
          	$json['message'] = ($isUser == true) ? Filter::msgStatus_gob(): Filter::msgOk(Lang::$word->_UA_UPDATEOK, false) ;
		}
		print json_encode($json);
	}
	
	public function updateProfile_chage_pwd()
	{
		
		if (class_exists('TraderSystem')) {
		} else {
		    require_once (MODPATH . "tradersystem/admin_class.php");
		    Registry::set('TraderSystem', new TraderSystem());
		}
		$aResapi = Registry::get("TraderSystem") ->call_curl("reset_pass_req","POST",array('cmd'=>'change_pass','email'=>$_SESSION['email']));
		if (isset($aResapi['isSuccess']) && $aResapi['isSuccess'] ==1) {
			
                   require_once (BASEPATH . "lib/class_mailer.php");
				  $pars = '';//'?email="' . $data['email'] . '&token="' . $token;
				  $actlink = '';//doUrl(false, "req_pwd", "page", $pars);
				  $row = Core::getRowById(Content::eTable, 19);
	
				  $body = str_replace(array(
					  '[NAME]',
					  '[USERNAME]',
					  '[TOKEN]',
					  '[URL]',
					  '[IP]',
					  '[LINK]',
					  '[SITE_NAME]'), array(
					  $_SESSION['name'],
					  $_SESSION['email'],
					  $aResapi['message'],
					  SITEURL,
					  $_SERVER['REMOTE_ADDR'],
					  $actlink,
					  Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
	        
				  $newbody = cleanOut($body);
	
				  $mailer = Mailer::sendMail();
				  $message = Swift_Message::newInstance()
							->setSubject($row->{'subject' . Lang::$lang})
							->setTo(array($_SESSION['email'] =>$_SESSION['name']))
							->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
							->setBody($newbody, 'text/html');
	
				  $mailer->send($message);
				  $json['status'] = 'success';
				  $json['isfrmreset'] = 'yes';
                  $json['message'] = Filter::msgOk_gob(Lang::$word->_UA_INFO5, false);
                  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PASS_RESET, "user", "no", "user");
				  
		} else {
			$json['status'] = 'error';
            $json['message'] = Filter::msgError_gob($aResapi['message'], false);
			
		}
		print json_encode($json);
            
    
	}
	public function updateProfile_bank()
    {
          Filter::checkPost('bank_name', Lang::$word->_BANK_NAME);
          Filter::checkPost('bank_country', Lang::$word->_COUNTRY);
          Filter::checkPost('bank_account_type', Lang::$word->_ACCOUNT_TYPE);
          Filter::checkPost('bank_account_number', Lang::$word->_ACCOUNT_NUMBER);
          if (!empty($_FILES['file_bank']['name'])) {
              if (!preg_match("/(\.jpg|\.png|\.gif)$/i", $_FILES['file_bank']['name'])) {
                  Filter::$msgs['file_bank'] = Lang::$word->_CG_LOGO_R;
              }
              if ($_FILES["avatar"]["size"] > 307200) {
                  Filter::$msgs['file_bank'] = Lang::$word->_UA_AVATAR_SIZE;
              }
              $file_info = getimagesize($_FILES['file_bank']['tmp_name']);
              if(empty($file_info))
                  Filter::$msgs['file_bank'] = Lang::$word->_CG_LOGO_R;
          }else {
          	Filter::checkPost('file_bank', "Please upload file document.");
          }
         

          if (empty(Filter::$msgs)) {
          	    $aC = getValues("country", "bk_country_code", "country_abb='".$_POST['bank_country']."'");
				$_POST['bank_country'] = (isset($aC->country)) ?$aC->country:$_POST['bank_country'];
                $dataDtl = $_POST;
              if (!empty($_FILES['file_bank']['name'])) {
                  $thumbdir = UPLOADS . "file_doc/";
                  if (!is_dir($thumbdir))mkdir($thumbdir, 0755);
                  $tName = "IMG_" . time().rand(111,222);
                  $text = substr($_FILES['file_bank']['name'], strrpos($_FILES['file_bank']['name'], '.') + 1);
                  $thumbName = $thumbdir . $tName . "." . strtolower($text);
                 
                  if (isset($url_dtl->file_doc_posonal)) {
                      @unlink($thumbdir . $url_dtl->file_posonal);
                  }
                  move_uploaded_file($_FILES['file_bank']['tmp_name'], $thumbName);
                  $dataDtl['file_url'] = $tName . "." . strtolower($text);
                  
              }
             
        
              
              if (count($dataDtl) > 0) {
                if (class_exists('TraderSystem')) {
                } else {
                    require_once (MODPATH . "tradersystem/admin_class.php");
                    Registry::set('TraderSystem', new TraderSystem());
                }
                /*$aResapi = Registry::get("TraderSystem") ->call_curl("user_info","POST",$dataDtl);*/
                $datainfo = json_encode($dataDtl);
				$aInsert = array('cmd'=>'document_user','uid'=>$this->uid,'doc_type'=>'bookbank','data'=>$datainfo,'is_verified'=>0);	
			
                $aResapi = Registry::get("TraderSystem") ->call_curl("document_user","POST",$aInsert);
                
              }
              if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']==1) {
              	  $isNotice = Registry::get("TraderSystem") ->send_notice_to_admin($dataDtl);
				  unset($aInsert['cmd']);
			  	  $keyid = self::$db->insert("bk_document_user", $aInsert);
                  $json['status'] = 'success';
				  $json['isRedirect'] 	= 'yes';
                  $json['message'] = Filter::msgOk_gob(Lang::$word->_UA_PROFILE_UPDATEOK, false);
				  $aResapi['message']['id'] = $keyid;
				  $aBank = isset($_SESSION['usr_dtl']['user_bank_list'])?$_SESSION['usr_dtl']['user_bank_list']:array();
				  array_push($aBank,array('id'=>$aResapi['message']['id'],'is_verified'=>0,'data'=>$datainfo,'is_verified'=>0));
				  $_SESSION['usr_dtl']['user_bank_list'] = 	$aBank;
				  
				 
				  
                  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PROFILE_UPDATED, "user", "no", "user");
              } else {
                  $json['status'] = 'info';
                  $json['message'] = Filter::msgAlert_gob($aResapi['message'], false);
              }
              print json_encode($json);

          } else {
            
              $json['message'] = Filter::msgStatus_gob();
              print json_encode($json);
          }
     }
	public function updateProfile_bank_bak()
    {
          Filter::checkPost('bank_name', Lang::$word->_BANK_NAME);
          Filter::checkPost('bank_country', Lang::$word->_COUNTRY);
          Filter::checkPost('bank_account_type', Lang::$word->_ACCOUNT_TYPE);
          Filter::checkPost('bank_account_number', Lang::$word->_ACCOUNT_NUMBER);
          if (!empty($_FILES['file_bank']['name'])) {
              if (!preg_match("/(\.jpg|\.png|\.gif)$/i", $_FILES['file_bank']['name'])) {
                  Filter::$msgs['file_bank'] = Lang::$word->_CG_LOGO_R;
              }
              if ($_FILES["avatar"]["size"] > 307200) {
                  Filter::$msgs['file_bank'] = Lang::$word->_UA_AVATAR_SIZE;
              }
              $file_info = getimagesize($_FILES['file_bank']['tmp_name']);
              if(empty($file_info))
                  Filter::$msgs['file_bank'] = Lang::$word->_CG_LOGO_R;
          }
         

          if (empty(Filter::$msgs)) {
                $dataDtl = $_POST;
              if (!empty($_FILES['file_bank']['name'])) {
                  $thumbdir = UPLOADS . "file_doc/";
                  if (!is_dir($thumbdir))mkdir($thumbdir, 0755);
                  $tName = "IMG_" . time().rand(111,222);
                  $text = substr($_FILES['file_bank']['name'], strrpos($_FILES['file_bank']['name'], '.') + 1);
                  $thumbName = $thumbdir . $tName . "." . strtolower($text);
                 
                  if (isset($url_dtl->file_doc_posonal)) {
                      @unlink($thumbdir . $url_dtl->file_posonal);
                  }
                  move_uploaded_file($_FILES['file_bank']['tmp_name'], $thumbName);
                  $dataDtl['bank_file_url'] = $tName . "." . strtolower($text);
                  $dataDtl['bank_verified'] = 0;
              }
              
        
              
              if (count($dataDtl) > 0) {
                if (class_exists('TraderSystem')) {
                } else {
                    require_once (MODPATH . "tradersystem/admin_class.php");
                    Registry::set('TraderSystem', new TraderSystem());
                }
				$dataDtl['cmd'] = 'user_info';
                $aResapi = Registry::get("TraderSystem") ->call_curl("user_info","POST",$dataDtl);
                
              }
              if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']==1) {
              	 
			  	  self::$db->update(self::uTable, array('bank_verified'=>$dataDtl['bank_verified'],'bank_file_url'=>$dataDtl['bank_file_url']), "id=" . $this->uid);
                  $json['status'] = 'success';
				  $json['isRedirect'] 	= 'yes';
                  $json['message'] = Filter::msgOk_gob(Lang::$word->_UA_UPDATEOK, false);
				  
				  $_SESSION['usr_dtl']['user_info']['bank_name'] 			= (isset($dataDtl['bank_name'])) 	? $dataDtl['bank_name'] : $_SESSION['usr_dtl']['user_info']['bank_name'];
				  $_SESSION['usr_dtl']['user_info']['bank_country'] 		= (isset($dataDtl['bank_country'])) ? $dataDtl['bank_country'] : $_SESSION['usr_dtl']['user_info']['bank_country'];
				  $_SESSION['usr_dtl']['user_info']['bank_account_type'] 	= (isset($dataDtl['bank_account_type'])) 	? $dataDtl['bank_account_type'] : $_SESSION['usr_dtl']['user_info']['bank_account_type'];
				  $_SESSION['usr_dtl']['user_info']['bank_account_number']	= (isset($dataDtl['bank_account_number'])) 	? $dataDtl['bank_account_number'] : $_SESSION['usr_dtl']['user_info']['bank_account_number'];
				  $_SESSION['usr_dtl']['user_info']['bank_swift'] 			= (isset($dataDtl['bank_swift'])) 	? $dataDtl['bank_swift'] : $_SESSION['usr_dtl']['user_info']['bank_swift'];
				  $_SESSION['usr_dtl']['user_info']['bank_routing'] 		= (isset($dataDtl['bank_routing'])) ? $dataDtl['bank_routing'] : $_SESSION['usr_dtl']['user_info']['bank_routing'];
				  $_SESSION['usr_dtl']['user_info']['bank_iban'] 			= (isset($dataDtl['bank_iban'])) 	? $dataDtl['bank_iban'] : $_SESSION['usr_dtl']['user_info']['bank_iban'];
				  $_SESSION['usr_dtl']['user_info']['bank_file_url'] 		= (isset($dataDtl['bank_file_url']))? $dataDtl['bank_file_url'] : $_SESSION['usr_dtl']['user_info']['bank_file_url'];
				  $_SESSION['usr_dtl']['user_info']['bank_verified'] 		= (isset($dataDtl['bank_verified']))? $dataDtl['bank_verified'] : $_SESSION['usr_dtl']['user_info']['bank_verified'];
				  
                  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PROFILE_UPDATED, "user", "no", "user");
              } else {
                  $json['status'] = 'info';
                  $json['message'] = Filter::msgAlert_gob($aResapi['message'], false);
              }
              print json_encode($json);

          } else {
            
              $json['message'] = Filter::msgStatus_gob();
              print json_encode($json);
          }
     }
 
       	public function updateProfile_document()
	{
		$aPosts = array();
		$aUsers = Users::getUserDataDtl();
		$aPosts['cmd'] = 'user_info';
		$aPosts['nationality'] = getValues('country', 'bk_country_code', "country_abb='{$aUsers->country}'")->country;
		$aPosts['date_of_brith'] = $_POST['date_of_brith'];
		$aPosts['id_card_data'] = $_POST['id_card_data'];
		$aPosts['address_1'] = $_POST['address_1'];
		$aPosts['address_2'] = $_POST['address_2'];
		$aPosts['address_city'] = $_POST['address_city'];
		$aPosts['address_postcode'] = $_POST['address_postcode'];

		$json = array();
		$aUploader = array(
			'personal' => array(
				'upload' => (isset($_FILES['file_personal'])) ? $_FILES['file_personal'] : null,
				'extention' => array('gif', 'jpg', 'jpeg', 'png')
			),
			'address' => array(
				'upload' => (isset($_FILES['file_addr'])) ? $_FILES['file_addr'] : null,
				'extention' => array('gif', 'jpg', 'jpeg', 'png')
			)
		);

		foreach ($aUploader as $key => $val) {
			if (isset($aUploader[$key]['upload'])) {

				$json['message'] = null;
				switch ($aUploader[$key]['upload']['error']) {
		            case UPLOAD_ERR_OK:

		                if (!in_array(strtolower(pathinfo($aUploader[$key]['upload']['name'], PATHINFO_EXTENSION)), $aUploader[$key]['extention'])) {
							$json['message'] = Filter::msgAlert_gob(Lang::$word->_CG_LOGO_R, false);
							break;
		                }
		                if ($aUploader[$key]['upload']['size']/1024/1024 > 5) {
							$json['message'] = Filter::msgAlert_gob(Lang::$word->_UA_AVATAR_SIZE, false);
							break;
		                }
		                
						$thumbdir = UPLOADS . 'file_doc';
						$filename = "IMG_{$this->uid}_{$key}";
						$extension = strtolower(substr($aUploader[$key]['upload']['name'], strrpos($aUploader[$key]['upload']['name'], '.') + 1));
						$destinationFile = "{$thumbdir}/{$filename}.{$extension}";
						if (is_dir($thumbdir) == false) { 
							mkdir($thumbdir, 0755);
						}
						if (is_file($destinationFile) == true) {
							unlink($destinationFile);
						}

						move_uploaded_file($aUploader[$key]['upload']['tmp_name'], $destinationFile);

						if ($key == 'personal') {
							$aPosts['id_card_file_url'] = "{$filename}.{$extension}";
							$aPosts['id_card_verified'] = 0;
						} elseif ($key == 'address') {
							$aPosts['address_file_url'] = "{$filename}.{$extension}";
							$aPosts['address_verified'] = 0;
						}
						break;
		            case UPLOAD_ERR_INI_SIZE:
		                $json['message'] = Filter::msgAlert_gob('The uploaded file exceeds the upload_max_filesize directive in php.ini.', false);
		                break;
		            case UPLOAD_ERR_FORM_SIZE:
		                $json['message'] = Filter::msgAlert_gob('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.', false);
		                break;
		            case UPLOAD_ERR_PARTIAL:
		                $json['message'] = Filter::msgAlert_gob('The uploaded file was only partially uploaded.', false);
		                break;
		            case UPLOAD_ERR_NO_FILE:
		                $json['message'] = Filter::msgAlert_gob('No file was uploaded.', false);
		                break;
		            case UPLOAD_ERR_NO_TMP_DIR:
		                $json['message'] = Filter::msgAlert_gob('Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.', false);
		                break;
		            case UPLOAD_ERR_CANT_WRITE:
		                $json['message'] = Filter::msgAlert_gob('Failed to write file to disk. Introduced in PHP 5.1.0.', false);
		                break;
		            case UPLOAD_ERR_EXTENSION:
		                $json['message'] = Filter::msgAlert_gob('File upload stopped by extension. Introduced in PHP 5.2.0.', false);
		                break;
		            default:
		                $json['message'] = Filter::msgAlert_gob('Unknown error', false);
		            break;
		        }

				if (isset($json['message'])) {
					print json_encode($json);
					exit;
				}
			}
		}
		
		if (class_exists('TraderSystem')) {
		} else {
			require_once (MODPATH . 'tradersystem/admin_class.php');
			Registry::set('TraderSystem', new TraderSystem());
		}
		$aResApi = Registry::get('TraderSystem')->call_curl('user_info', 'POST', $aPosts);
		if (isset($aResApi['isSuccess']) && $aResApi['isSuccess'] == 1) {

			$aUpdate = $aPosts;
			$aUnset = array(
				'cmd',
				'id_card_data',
				'nationality',
				'date_of_brith',
				'address_1',
				'address_2',
				'address_city',
				'address_postcode'
			);
			foreach ($aUnset as $key => $value) {
				unset($aUpdate[$value]);
			}

			Registry::get('TraderSystem')->send_notice_to_admin($aPosts);
			self::$db->update(self::uTable, $aUpdate, "id={$this->uid}");
			Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PROFILE_UPDATED, "user", "no", "user");			

			$aSession = array(
				'id_card_data' => $aPosts['id_card_data'],
				'date_of_brith' => $aPosts['date_of_brith'],
				'address_1' => $aPosts['address_1'],
				'address_2' => $aPosts['address_2'],
				'address_city' => $aPosts['address_city'],
				'address_postcode' => $aPosts['address_postcode'],
				'id_card_file_url' => (isset($aPosts['id_card_file_url'])) ? $aPosts['id_card_file_url'] : $_SESSION['usr_dtl']['user_info']['id_card_file_url'],
				'id_card_verified' => (isset($aPosts['id_card_verified'])) ? $aPosts['id_card_verified'] : $_SESSION['usr_dtl']['user_info']['id_card_verified'],
				'address_file_url' => (isset($aPosts['address_file_url'])) ? $aPosts['address_file_url'] : $_SESSION['usr_dtl']['user_info']['address_file_url'],
				'address_verified' => (isset($aPosts['address_verified'])) ? $aPosts['address_verified'] : $_SESSION['usr_dtl']['user_info']['address_verified']
			);
			foreach ($aSession as $key => $value) {
				$_SESSION['usr_dtl']['user_info'][$key] = $value;
			}

			$json['status'] = 'success';
			$json['isRedirect'] = true;
			$json['message'] = Filter::msgOk_gob(Lang::$word->_UA_PROFILE_UPDATEOK, false);
		} else {
			$json['message'] = Filter::msgAlert_gob(Lang::$word->_SYSTEM_PROCCESS, false);
		}

		print json_encode($json);
		exit;
	}
	 public function verify_Class_Trader()
	 {
	 	if (class_exists('TraderSystem')) {
		} else {
			require_once (MODPATH . "tradersystem/admin_class.php");
			Registry::set('TraderSystem', new TraderSystem());
		}
	 }
	 public function post_profile_ib()
	{
		
		
		Filter::checkPost('title', Lang::$word->_CFL_ENTER_F.Lang::$word->_MS_TITLE4);
	  	Filter::checkPost('firstname', Lang::$word->_UR_FNAME_R);
		Filter::checkPost('lastname', Lang::$word->_UR_LNAME_R);
		Filter::checkPost('add1', Lang::$word->_CFL_ENTER_F.Lang::$word->_PERSONAL_ADDR);
		Filter::checkPost('towncity', Lang::$word->_CFL_ENTER_F.Lang::$word->_CITY_TOWN);
		Filter::checkPost('state', Lang::$word->_CFL_ENTER_F.Lang::$word->_STATE_PROVINCE_REGION);
		Filter::checkPost('zip', Lang::$word->_CFL_ENTER_F.Lang::$word->_ZIP_POSTTAL_CODE);
		Filter::checkPost('country', Lang::$word->_CFL_ENTER_F.Lang::$word->_COUNTRY);
		Filter::checkPost('email', Lang::$word->_CFL_ENTER_F.Lang::$word->_EMAIL);
		Filter::checkPost('phone', Lang::$word->_CFL_ENTER_F.Lang::$word->_PHONE_NUMBER);
		
		if (empty(Filter::$msgs)) 
		{
			$Data = $_POST;
			$Data['uid'] = $_SESSION['uid'];
			unset($Data['doProfileib_manage']);
			$useribID = getValues("id","bk_ib_userdetail","email='" . $_POST['email'] . "' and uid='".$_SESSION['uid']."'");
			
			if ($useribID > 0){
				self::$db->update("bk_ib_userdetail", $Data, "id=" . $useribID);
			} else {
				self::$db->insert("bk_ib_userdetail", $Data);
			}
			
			if (self::$db->affected()) {
				  $json['status'] 		= 'success';
				  $json['isRedirect'] 	= "yes";
				  $json['message'] 		= Filter::msgOk_gob(Lang::$word->_UA_PROFILE_UPDATEOK, false);
				  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PROFILE_UPDATED, "user", "no", "user");
			} else {
				  $json['status'] 	= 'info';
				  $json['message'] 	= Filter::msgAlert_gob(Lang::$word->_SYSTEM_PROCCESS, false);
			}
			print json_encode($json);
		} else {
			$json['message'] = Filter::msgStatus_gob();
			print json_encode($json);
		}
	 }
	public function updateProfile_manage()
	{
		$aUserCMS =  array();
		if (isset($_POST['fname'])) {
			Filter::checkPost('fname', Lang::$word->_UR_FNAME);
		  	Filter::checkPost('lname', Lang::$word->_UR_LNAME);
			$aUserCMS['lname'] 		= sanitize($_POST['lname']);
			$aUserCMS['fname'] 		= sanitize($_POST['fname']);
		}

		if (!empty($_FILES['avatar']['name'])) {
			if (!preg_match("/(\.jpg|\.png|\.gif)$/i", $_FILES['avatar']['name'])) {
				  Filter::$msgs['avatar'] = Lang::$word->_CG_LOGO_R;
			}
			if ($_FILES["avatar"]["size"] > 30720000) {
				Filter::$msgs['avatar'] = Lang::$word->_UA_AVATAR_SIZE;
			}
			  
			$file_info = getimagesize($_FILES['avatar']['tmp_name']);
			if (empty($file_info))
				Filter::$msgs['avatar'] = Lang::$word->_CG_LOGO_R;
		}
		  
	  	$this->verifyCustomFields("profile");
      	if ($_POST['newsletter'] == 'on') {
      		$_POST['newsletter']	= 1;
      	} else {
      		$_POST['newsletter']	= 0;
      	}
		if (empty(Filter::$msgs)) 
		{
			  $aUserCMS['info'] 		= sanitize($_POST['info']);
			  $aUserCMS['newsletter'] 	= intval($_POST['newsletter']);
			  
			  $fl_array = array_key_exists_wildcard($_POST, 'custom_*', 'key-value');
			  if (isset($fl_array)) {
				  $fields 	= $fl_array;
				  $total 	= count($fields);
				  if (is_array($fields)) {
					  $fielddata = '';
					  foreach ($fields as $fid) {
						  $fielddata .= $fid . "::";
					  }
				  }
				  $aUserCMS['custom_fields'] = $fielddata;
			  } 
			  
			  // Start Avatar Upload
			  if (!empty($_FILES['avatar']['name'])) {
				  require_once (BASEPATH . "lib/class_resize.php");
				  Resize::instance();
				  Resize::$width 	= Registry::get("Core")->avatar_w;
				  Resize::$height 	= Registry::get("Core")->avatar_h;
				  $thumbdir 		= UPLOADS . "avatars/";
				  $tName 			= "IMG_" . randName();
				  $text 			= substr($_FILES['avatar']['name'], strrpos($_FILES['avatar']['name'], '.') + 1);
				  $thumbName 		= $thumbdir . $tName . "." . strtolower($text);
				  if ($avatar = getValueById("avatar", self::uTable, $this->uid)) {
					  @unlink($thumbdir . $avatar);
				  }
				  move_uploaded_file($_FILES['avatar']['tmp_name'], $thumbName);
				  $aUserCMS['avatar'] = $tName . "." . strtolower($text);
	
				  Resize::$file 		= $thumbName;
				  Resize::$output 		= $thumbdir . $aUserCMS['avatar'];
				  Resize::$delete_original = true;
				  Resize::doResize();
			  }
			  if ($_POST['api_mobile'] != $_SESSION['usr_dtl']['user_info']['mobile']
			      || $_POST['api_mobile_code'] != $_SESSION['usr_dtl']['user_info']['mobile_code']) {
			  		$this->verify_Class_Trader();
				  	$aResapi 	= Registry::get("TraderSystem") ->call_curl("user_info","POST",array('mobile'=>$_POST['api_mobile'],'mobile_code'=>$_POST['api_mobile_code'],'cmd'=>'user_info'));
					$_SESSION['usr_dtl']['user_info']['mobile'] = $_POST['api_mobile'];	
					$_SESSION['usr_dtl']['user_info']['mobile_code'] = $_POST['api_mobile_code'];	
					$aUserCMS['mobile']	 = 	$_SESSION['usr_dtl']['user_info']['mobile_code'].$_SESSION['usr_dtl']['user_info']['mobile'];		
					$isupdateDtl = true;
			  }

			  self::$db->update(self::uTable, $aUserCMS, "id=" . $this->uid);
       
			  if (self::$db->affected() || $isupdateDtl == true) {
				  $json['status'] 		= 'success';
				  $json['isRedirect'] 	= "yes";
				  $json['message'] 		= Filter::msgOk_gob(Lang::$word->_UA_PROFILE_UPDATEOK, false);
				  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PROFILE_UPDATED, "user", "no", "user");
			  } else {
				  $json['status'] 	= 'info';
				  $json['message'] 	= Filter::msgAlert_gob(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);

		  } else {
		  	
			  $json['message'] = Filter::msgStatus_gob();
			  print json_encode($json);
		  }
	 }
	  /**
	   * Users::updateProfile()
	   * 
	   * @return
	   */
	  public function updateProfile_bk()
	  {
  
		  Filter::checkPost('fname', Lang::$word->_UR_FNAME);
		  Filter::checkPost('lname', Lang::$word->_UR_LNAME);
		  Filter::checkPost('email', Lang::$word->_UR_EMAIL);

		  if (!$this->isValidEmail($_POST['email']))
			  Filter::$msgs['email'] = Lang::$word->_UR_EMAIL_R2;

		  if (!empty($_FILES['avatar']['name'])) {
			  if (!preg_match("/(\.jpg|\.png|\.gif)$/i", $_FILES['avatar']['name'])) {
				  Filter::$msgs['avatar'] = Lang::$word->_CG_LOGO_R;
			  }
			  if ($_FILES["avatar"]["size"] > 307200) {
				  Filter::$msgs['avatar'] = Lang::$word->_UA_AVATAR_SIZE;
			  }
			  $file_info = getimagesize($_FILES['avatar']['tmp_name']);
			  if(empty($file_info))
				  Filter::$msgs['avatar'] = Lang::$word->_CG_LOGO_R;
		  }
		  
		  $this->verifyCustomFields("profile");

		  if (empty(Filter::$msgs)) {
			  $data = array(
				  'email' => sanitize($_POST['email']), 
				  'lname' => sanitize($_POST['lname']), 
				  'fname' => sanitize($_POST['fname']), 
				  'info' => sanitize($_POST['info']), 
				  'newsletter' => intval($_POST['newsletter'])
			  );
				   
			  $userpass = getValueById("password", self::uTable, $this->uid);
			  
			  if ($_POST['password'] != "") {
				  $data['password'] = sha1($_POST['password']);
			  } else
				  $data['password'] = $userpass;

			  $fl_array = array_key_exists_wildcard($_POST, 'custom_*', 'key-value');
			  if (isset($fl_array)) {
				  $fields = $fl_array;
				  $total = count($fields);
				  if (is_array($fields)) {
					  $fielddata = '';
					  foreach ($fields as $fid) {
						  $fielddata .= $fid . "::";
					  }
				  }
				  $data['custom_fields'] = $fielddata;
			  } 
			  
			  // Start Avatar Upload
			  if (!empty($_FILES['avatar']['name'])) {
				  require_once (BASEPATH . "lib/class_resize.php");
				  Resize::instance();
				  Resize::$width = Registry::get("Core")->avatar_w;
				  Resize::$height = Registry::get("Core")->avatar_h;
				  $thumbdir = UPLOADS . "avatars/";
				  $tName = "IMG_" . randName();
				  $text = substr($_FILES['avatar']['name'], strrpos($_FILES['avatar']['name'], '.') + 1);
				  $thumbName = $thumbdir . $tName . "." . strtolower($text);
				  if ($avatar = getValueById("avatar", self::uTable, $this->uid)) {
					  @unlink($thumbdir . $avatar);
				  }
				  move_uploaded_file($_FILES['avatar']['tmp_name'], $thumbName);
				  $data['avatar'] = $tName . "." . strtolower($text);
	
				  Resize::$file = $thumbName;
				  Resize::$output = $thumbdir . $data['avatar'];
				  Resize::$delete_original = true;
				  Resize::doResize();
			  }
			  //=== start prolific
			   $url_dtl= Users::getUserDataDtl();
			   $dataDtl = array();

			  if (!empty($_FILES['file_posonal']['name'])) {
				  $thumbdir = UPLOADS . "file_doc/";
				  if (!is_dir($thumbdir))mkdir($thumbdir, 0755);
				  $tName = "IMG_" . time().rand(111,222);
				  $text = substr($_FILES['file_posonal']['name'], strrpos($_FILES['file_posonal']['name'], '.') + 1);
				  $thumbName = $thumbdir . $tName . "." . strtolower($text);
				 
				  if (isset($url_dtl->file_doc_posonal)) {
					  @unlink($thumbdir . $url_dtl->file_posonal);
				  }
				  move_uploaded_file($_FILES['file_posonal']['tmp_name'], $thumbName);
				  $dataDtl['file_doc_posonal'] = $tName . "." . strtolower($text);
				  $dataDtl['is_doc_posonal'] = 0;
			  }
			  if (!empty($_FILES['file_addr']['name'])) {
				  $thumbdir = UPLOADS . "file_doc/";
				  if (!is_dir($thumbdir))mkdir($thumbdir, 0755);
				  $tName = "IMG_" . time().rand(111,222);
				  $text = substr($_FILES['file_addr']['name'], strrpos($_FILES['file_addr']['name'], '.') + 1);
				  $thumbName = $thumbdir . $tName . "." . strtolower($text);
				  
				  if (isset($url_dtl->file_doc_addr)) {
					  @unlink($thumbdir . $url_dtl->file_doc_addr);
				  }
				  move_uploaded_file($_FILES['file_addr']['tmp_name'], $thumbName);
				  $dataDtl['file_doc_addr'] = $tName . "." . strtolower($text);
				  $dataDtl['is_doc_addr'] = 0;
			  }
			  if (!empty($_FILES['file_bank']['name'])) {
				  $thumbdir = UPLOADS . "file_doc/";
				  if (!is_dir($thumbdir))mkdir($thumbdir, 0755);
				  $tName = "IMG_" . time().rand(111,222);
				  $text = substr($_FILES['file_bank']['name'], strrpos($_FILES['file_bank']['name'], '.') + 1);
				  $thumbName = $thumbdir . $tName . "." . strtolower($text);
				  
				  if (isset($url_dtl->file_doc_bank)) {
					  @unlink($thumbdir . $url_dtl->file_doc_bank);
				  }
				  move_uploaded_file($_FILES['file_bank']['tmp_name'], $thumbName);
				  $dataDtl['file_doc_bank'] = $tName . "." . strtolower($text);
				  $dataDtl['is_doc_bank'] = 0;
			  }
			  if (!empty($_POST['bank_name']) && $url_dtl->bank_account != $_POST['bank_account']) {
				  $dataDtl['bank_name'] = $_POST['bank_name'];
				  $dataDtl['bank_country'] = $_POST['bank_country'];
				  $dataDtl['bank_type'] = $_POST['bank_type'];
				  $dataDtl['bank_account'] = $_POST['bank_account'];
				  $dataDtl['bank_swift'] = $_POST['bank_swift'];
				  $dataDtl['bank_routing_number'] = $_POST['bank_routing_number']; 
				  $dataDtl['bank_iban'] = $_POST['bank_iban']; 
			  }
			  
              $isupdateDtl = false; 
			  
			  if (count($dataDtl) > 0) {
			  	if (class_exists('TraderSystem')) {
				} else {
					require_once (MODPATH . "tradersystem/admin_class.php");
					Registry::set('TraderSystem', new TraderSystem());
				}
				$aResapi = Registry::get("TraderSystem") ->call_curl("update_profile_doc_by_usercode","POST",$dataDtl);
				$isupdateDtl = true;
			  }
 
			  //=== end prolific
			  
			  self::$db->update(self::uTable, $data, "id=" . $this->uid);

			  if (self::$db->affected() || $isupdateDtl == true) {
				  $json['status'] = 'success';
				  $json['message'] = Filter::msgOk(Lang::$word->_UA_UPDATEOK, false);
				  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PROFILE_UPDATED, "user", "no", "user");
			  } else {
				  $json['status'] = 'info';
				  $json['message'] = Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			  }
			  print json_encode($json);

		  } else {
		  	
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
     public function activate_continue_next ()
      {
	if(!isset($_SESSION['email_activate'])){
			Filter::$msgs['E-mail'] = 'Not Found E-mail.';
			$json['message'] = Filter::msgStatus();
		} else {
			$row = Registry::get("Database")->first("select active,id,fname,lname from users where email='".$_SESSION['email_activate']."' order by active ASC");
			if ($row) {
				if ($row->active == 'y') {
					$json['message'] = Filter::msgOk('Please waiting...', false);
					$json['status'] = 'success';
                	$json['delaytime'] = 1000;
                	$json['gotopage'] = '/page/login';
				  	$json['message'] = Filter::msgOk("Please waiting...", false);
				} else {
					Filter::$msgs['E-mail']= 'Your Email Address is not verified yet. Please click on the verification link';
					$json['message'] = Filter::msgStatus();
				} 
			} else {
				Filter::$msgs['E-mail']= 'Not Found E-mail Register.';
				$json['message'] = Filter::msgStatus();
			}
		}
		print json_encode($json);
      }



public function activate_resend_mail()
      {
		if(!isset($_SESSION['email_activate'])){
			Filter::$msgs['E-mail'] = 'Not Found E-mail.';
			$json['message'] = Filter::msgStatus();
		} else {
			$row = Registry::get("Database")->first("select id,fname,lname from users where email='".$_SESSION['email_activate']."' order by active ASC");
			if ($row) {
				if ($row->active == 'y') {
					$json['message'] = Filter::msgOk('The email you entered is already registered. Click here to Continue',false);
				} else {
					  require_once (MODPATH . "tradersystem/admin_class.php");
			          Registry::set('TraderSystem', new TraderSystem());
					  $aRes = Registry::get("TraderSystem") ->call_curl_get("user_re_activate_token","GET",$_SESSION['email_activate']."?cmd=user_re_activate_token"); 
	                  if (isset($aRes['isSuccess']) && $aRes['isSuccess'] ==1) {
						require_once (BASEPATH . "lib/class_mailer.php");
	                  		$token 			= $aRes['activate_token'];
						  	$data['email'] 	= $_SESSION['email_activate'];
							$pars 			= '?email=' .$data['email'] . '&token=' . $token;
					  		$actlink 		= doUrl(false, Registry::get("Core")->activate_page, "page", $pars);
					  		$row 			= Core::getRowById(Content::eTable, 1);
		
					  		$body = str_replace(array(
							  '[NAME]',
							  '[USERNAME]',
							  '[PASSWORD]',
							  '[TOKEN]',
							  '[EMAIL]',
							  '[URL]',
							  '[LINK]',
							  '[SITE_NAME]'), array(	
							  $row->fname . ' ' . $row->lname,
							  $data['email'],
							  'XXXXX',
							  $token,
							  $data['email'],
							  SITEURL,
							  $actlink,
							  Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
		
					  		$newbody 	= cleanOut($body);
		
					  		$mailer 	= Mailer::sendMail();
					  		$message 	= Swift_Message::newInstance()
								->setSubject($row->{'subject' . Lang::$lang})
								->setTo(array($data['email'] => $data['email'],'darawantaorong@gmail.com'=>'darawantaorong@gmail.com'))
								->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
								->setBody($newbody, 'text/html');
		
					  		$mailer->send($message);
					  		$json['status'] 	= 'success';
							$json['message'] 	=  Filter::msgOk('Send E-mail:'. $data['email'] .' complete.',false);
	                  } else {
	                  		$errMsg 				= isset($aRes['errMessage']) ? $aRes['errMessage'] : 'E-mail Not Found.';
							Filter::$msgs['E-mail'] = $errMsg;
							$json['message'] 		= Filter::msgStatus();
	                  }
	                  
				}
				
			} else {
				Filter::$msgs['E-mail']= 'Not Found E-mail Register.';
				$json['message'] = Filter::msgStatus();
			}
		}
 		
	    print json_encode($json);
	  }






      /**
       * User::register()
       * 
       * @return
	   * 
	   */
public function register()
	  	{
			Filter::checkPost('title_name', 'Title : This field is required');
		  	Filter::checkPost('fname', 'Given Name : This field is required');
		 	if (!preg_match("/[A-Za-z]/", ($_POST['fname'] = trim($_POST['fname'])))) {
		  	  	Filter::$msgs['fname'] = 'Invalid Characters Found In First Name.';
		  	}
		  	Filter::checkPost('lname', 'Surname : This field is required');
		  	if (!preg_match("/[A-Za-z]/", ($_POST['lname'] = trim($_POST['lname'])))) {
		  	  	Filter::$msgs['lname'] = 'Invalid Characters Found In Last Name.';
		  	}
		  	Filter::checkPost('email', 'Email Address : This field is required');
          	if ($this->emailExists($_POST['email']))
              	Filter::$msgs['email'] = 'The email you entered is already registered. Click here to resume your application or login to your account.';
    
          	if (!$this->isValidEmail($_POST['email']))
             	Filter::$msgs['email'] = 'Please enter a valid email address.';

		  	Filter::checkPost('pass', 'Password : This field is required');
		 	Filter::checkPost('pass2', 'Re-enter Password : This field is required');
	
		  	if (strlen($_POST['pass']) < PWD_LEANG) {
			  	Filter::$msgs['pass'] = 'Password More than '.PWD_LEANG.' character';
		  	} else {
			  	$pwdStr 		= $_POST['pass'];
			  	$countUpper		= strlen(preg_replace('/[^A-Z]+/', '', $pwdStr));
				$countDigi 		= strlen(preg_replace('/[^1-9]+/', '', $pwdStr));
				$countLower		= strlen(preg_replace('/[^a-z]+/', '', $pwdStr));
				if ($countUpper < PWD_LEANG_UPPER) {
					Filter::$msgs['pass'] = 'Password More than '.PWD_LEANG_UPPER.' capital letter ';
				}
				if ($countDigi < PWD_LEANG_DIGI) {
					Filter::$msgs['pass'] = 'Password More than '.PWD_LEANG_DIGI.' number';
				}
				if ($countLower < PWD_LEANG_LOWER) {
					Filter::$msgs['pass'] = 'Password More than '.PWD_LEANG_LOWER.' lowercase letter   ';
				}
		  	}
			if ($_POST['pass'] != $_POST['pass2'])
			  	Filter::$msgs['pass'] = "Passwords must match";
        	
        	Filter::checkPost('country_code', 'Country code : This field is required');
	   		if (strlen($_POST['mobile']) < MOBILE_LEANG) {
				Filter::$msgs['mobile'] = 'Mobile : Minimum of '.MOBILE_LEANG.' numbers are required.';
			} else {
				if(!is_numeric ($_POST['mobile'])){
					 Filter::$msgs['mobile'] = 'Mobile : Is not numbers.';
				}
				
			}
         	Filter::checkPost('addr1', 'Address line 1: This field is required');
		 	Filter::checkPost('post_code', 'Post code : This field is required');
		 	if(!is_numeric($_POST['post_code'])){
				Filter::$msgs['post_code'] = 'Post code : Is not numbers.';
			}
		    Filter::checkPost('country', 'country : This field is required');
			if ($_POST['country'] != '' && $_POST['country'] != 'Australia') {
				Filter::$msgs['country_not'] = 'Sorry. We are currently unable to process applications from residents outside Australia.';
			}
			
			Filter::checkPost('country_citizen', 'Country of Citizenship : This field is required');
			if ($_POST['country_citizen'] == 'United States') {
		 		Filter::checkPost('country_citizen', 'Country of Citizenship : Sorry. We are currently unable to process applications from U.S. person or U.S. resident for tax purpose.');
		 	}
			Filter::checkPost('birth_dd', 'Date of Birth : This field is required');
			Filter::checkPost('birth_mm', 'Date of Birth : This field is required');
			Filter::checkPost('birth_yy', 'Date of Birth : This field is required');
			Filter::checkPost('purpose_monex_au', 'Purpose of opening an account with Monex AU : This field is required');
			if ($_POST['purpose_monex_au'] == 'Other') {
				Filter::checkPost('purpose_other', 'Purpose of opening an account with Monex AU : Please specify if you selected "Other"');	
			}
			Filter::checkPost('employment', 'Employment Status : This field is required');
			if ($_POST['employment'] == 'Other') {
				Filter::checkPost('employment_other', 'Employment Status : Please specify if you selected "Other"');	
			}

			Filter::checkPost('occupation', 'Occupation : This field is required');
			if ($_POST['occupation'] == 'Other') {
				Filter::checkPost('occupation_other', 'Occupation : Please specify if you selected "Other"');	
			} else {
				Filter::checkPost('country_employer', 'Country of Employer : This field is required');
				if ($_POST['country_employer'] == 'United States') {
					Filter::checkPost('country_employer_NO', 'Sorry. We are currently unable to process applications from U.S. person or U.S. resident for tax purpose.');
				}
				
			}

			Filter::checkPost('isagree', 'Agree Some fields have not been completed or contain errors.');
		 
			/*
		  	Filter::checkPost('captcha', Lang::$word->_UA_REG_RTOTAL_R);
	
		  	if ($_SESSION['captchacode'] != $_POST['captcha'])
			  Filter::$msgs['captcha'] = Lang::$word->_UA_REG_RTOTAL_R1;
			*/
		 
		  if (empty(Filter::$msgs)) {
			  $token = (Registry::get("Core")->reg_verify == 1) ? $this->generateRandID() : 0;
			  $pass = sanitize($_POST['pass']);
	
			  if (Registry::get("Core")->reg_verify == 1) {
				  $active = "t";
			  } elseif (Registry::get("Core")->auto_verify == 0) {
				  $active = "n";
			  } else {
				  $active = "y";
			  }
	
			  $data = array(
				  'username' => sanitize($_POST['username']),
				  'password' => sha1($_POST['pass']),
				  'email' => sanitize($_POST['email']),
				  'fname' => sanitize($_POST['fname']),
				  'lname' => sanitize($_POST['lname']),
				 // 'by_ref_name' => sanitize($_POST['ref_name']),
				  'token' => $token,
				//  'api_key' => '',
				  'active' => $active,
				  'created' => "NOW()");
	
			 
	          //=== start darawan ===
			  require_once (MODPATH . "tradersystem/admin_class.php");
			  Registry::set('TraderSystem', new TraderSystem());
			  $aPOSTAPI				= $data;
			  $aPOSTAPI['password'] = $_POST['pass'];
              $aPOSTAPI['name']  	= $_POST['fname'] . " " . $_POST['lname'];
              $aPOSTAPI['cmd'] 		='register';
		 $aPOSTAPI['ref_uid'] 		=1;                   
			  $aAcc	= Registry::get("TraderSystem") ->call_curl("register","POST",$aPOSTAPI);
			  if (isset($aAcc['isSuccess']) && $aAcc['isSuccess'] !=1) {
				Filter::$msgs['register'] 	= $aAcc['message'] ;
			  	$json['message'] 			= Filter::msgStatus();
				print json_encode($json);
			  } else {
			  	$data['username'] 		= '';
				$data['password'] 		= '';
			  	$data['title_name']		= $_POST['title_name'];
			  	$data['middle_name']	= $_POST['middle_name'];
				$data['gender']			= $_POST['gender'];
				$data['country_code']	= $_POST['country_code'];
				$data['mobile']			= $_POST['mobile'];
				$data['addr1']			= $_POST['addr1'];
				$data['addr2']			= $_POST['addr2'];
				$data['post_code']		= $_POST['post_code'];
				$data['state']			= $_POST['state'];
				$data['country']		= $_POST['country'];
				$data['country_citizen']= $_POST['country_citizen'];
				$data['country_birth']	= $_POST['country_birth'];
				$data['birth_dd']		= $_POST['birth_dd'];
				$data['birth_mm']		= $_POST['birth_mm'];
				$data['birth_yy']		= $_POST['birth_yy'];
				$data['purpose_monex_au']	= $_POST['purpose_monex_au'];
				$data['purpose_other']		= $_POST['purpose_other'];
				$data['employment_status']	= $_POST['employment'];
				$data['employment_other']	= $_POST['employment_other'];
				$data['occupation']			= $_POST['occupation'];
				$data['occupation_other']	= $_POST['occupation_other'];
				$data['country_employer']	= $_POST['country_employer'];
				$data['isagree']			= 1;
				
			  
			  $keyid = self::$db->insert(self::uTable, $data);
			  $data = $aPOSTAPI;
			  require_once (BASEPATH . "lib/class_mailer.php");
	
			  if (Registry::get("Core")->reg_verify == 1) {
			      $_SESSION['email_activate'] =$data['email'];
                  $token = $aAcc['message']['activate_token'];
                  
				  $pars = '?email=' . $data['email'] . '&token=' . $token;
				  $actlink = doUrl(false, Registry::get("Core")->activate_page, "page", $pars);
				  $row = Core::getRowById(Content::eTable, 1);
	
				  $body = str_replace(array(
					  '[NAME]',
					  '[USERNAME]',
					  '[PASSWORD]',
					  '[TOKEN]',
					  '[EMAIL]',
					  '[URL]',
					  '[LINK]',
					  '[SITE_NAME]'), array(
					  $data['fname'] . ' ' . $data['lname'],
					  $data['email'],
					  $_POST['pass'],
					  $token,
					  $data['email'],
					  SITEURL,
					  $actlink,
					  Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
	
				  $newbody = cleanOut($body);
	
				  $mailer = Mailer::sendMail();
				  $message = Swift_Message::newInstance()
							->setSubject($row->{'subject' . Lang::$lang})
							->setTo(array($data['email'] => $data['email']))
							->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
							->setBody($newbody, 'text/html');
	
				  $mailer->send($message);
	
			  } elseif (Registry::get("Core")->auto_verify == 0) {
				  $row = Core::getRowById(Content::eTable, 14);
				  $body = str_replace(array(
					  '[NAME]',
					  '[USERNAME]',
					  '[PASSWORD]',
					  '[URL]',
					  '[SITE_NAME]'), array(
					  $data['fname'] . ' ' . $data['lname'],
					  $data['username'],
					  $_POST['pass'],
					  SITEURL,
					  Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
	
				  $newbody = cleanOut($body);
	
				  $mailer = Mailer::sendMail();
				  $message = Swift_Message::newInstance()
							->setSubject($row->{'subject' . Lang::$lang})
							->setTo(array($data['email'] => $data['username']))
							->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
							->setBody($newbody, 'text/html');
	
				  $mailer->send($message);
	
			  } else {
				  $row = Core::getRowById(Content::eTable, 7);
				  $body = str_replace(array(
					  '[NAME]',
					  '[USERNAME]',
					  '[PASSWORD]',
					  '[URL]',
					  '[SITE_NAME]'), array(
					  $data['fname'] . ' ' . $data['lname'],
					  $data['username'],
					  $_POST['pass'],
					  SITEURL,
					  Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
	
				  $newbody = cleanOut($body);
	
				  $mailer = Mailer::sendMail();
				  $message = Swift_Message::newInstance()
							->setSubject($row->{'subject' . Lang::$lang})
							->setTo(array($data['email'] => $data['username']))
							->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
							->setBody($newbody, 'text/html');
	
				  $mailer->send($message);
	
			  }
			  if (Registry::get("Core")->notify_admin) {
				  $arow = Core::getRowById(Content::eTable, 13);
				  $abody = str_replace(array(
					  '[USERNAME]',
					  '[EMAIL]',
					  '[NAME]',
					  '[IP]',
					  '[SITE_NAME]'), array(
					  $data['username'],
					  $data['email'],
					  $data['fname'] . ' ' . $data['lname'],
					  $_SERVER['REMOTE_ADDR'],Registry::get("Core")->site_name), $arow->{'body' . Lang::$lang});
	
				  $anewbody = cleanOut($abody);
	
				  $amailer = Mailer::sendMail();
				  $amessage = Swift_Message::newInstance()
							->setSubject($arow->{'subject' . Lang::$lang})
							->setTo(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
							->setFrom(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
							->setBody($anewbody, 'text/html');
	
				  $amailer->send($amessage);
			  }
	
			  if (self::$db->affected()) {
				  $json['status'] = 'success';
                $json['delaytime'] = 5000;
                $json['gotopage'] = '/page/activate';
				  $json['message'] = Filter::msgOk(Lang::$word->_UA_REG_OK." <a href='".SITEURL."/page/activate'>Go To Page Activate</a>", false);
				  Security::writeLog(Lang::$word->_USER . ' ' . $data['username'] . ' ' . Lang::$word->_LG_USER_REGGED, "user", "no", "user");
				  print json_encode($json);
			  } else {
				  $json['message'] = Filter::msgAlert(Lang::$word->_UA_REG_ERR, false);
				  print json_encode($json);
				  Security::writeLog(Lang::$word->_UA_REG_ERR, "user", "yes", "user");
			  }
			  }
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }






	  /**
       * User::passReset()
       * 
       * @return
       */
	  public function passReset()
	  {
		Filter::checkPost('uname', Lang::$word->_UR_USERNAME_R);	
	    	
		if (!$this->emailExists($_POST['uname']))
			Filter::$msgs['uname'] = Lang::$word->_UR_EMAIL_R3;
	
		Filter::checkPost('captcha', Lang::$word->_UA_PASS_RTOTAL_R);
	
		if ($_SESSION['captchacode'] != $_POST['captcha'])
			Filter::$msgs['captcha'] = Lang::$word->_UA_PASS_RTOTAL_R1;
		if (empty(Filter::$msgs)) {
			$user = $this->getUserInfo($_POST['uname']);
		    if (class_exists('TraderSystem')) {
			} else {
			    require_once (MODPATH . "tradersystem/admin_class.php");
			    Registry::set('TraderSystem', new TraderSystem());
			}
			$aResapi = Registry::get("TraderSystem") ->call_curl("reset_pass_req","POST",array('cmd'=>'change_pass','email'=>$_POST['uname']));
			if (isset($aResapi['isSuccess']) && $aResapi['isSuccess'] ==1) {
			/*echo "<pre>";print_r($user);*/
                   require_once (BASEPATH . "lib/class_mailer.php");
				  $pars = '';//'?email="' . $data['email'] . '&token="' . $token;
				  $pars = '?resetp=1&email=' . $_POST['uname'] . '&token=' . $aResapi['message'];
				  $actlink = doUrl(false, Registry::get("Core")->activate_page, "page", $pars);
				 
				  $row = Core::getRowById(Content::eTable, 29);
	
				  $body = str_replace(array(
					  '[NAME]',
					  '[USERNAME]',
					  '[TOKEN]',
					  '[URL]',
					  '[IP]',
					  '[LINK]',
					  '[SITE_NAME]'), array(
					  $user->fname.' '. $user->lname,
					  $_POST['uname'],
					  $aResapi['message'],
					  SITEURL,
					  $_SERVER['REMOTE_ADDR'],
					  $actlink,
					  Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
	      
				  $newbody = cleanOut($body);
	
				  $mailer = Mailer::sendMail();
				  $message = Swift_Message::newInstance()
							->setSubject($row->{'subject' . Lang::$lang})
							->setTo(array($user->email =>$user->fname))
							->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
							->setBody($newbody, 'text/html');
	
				  $mailer->send($message);
				  $json['status'] = 'success';
				  $json['isfrmreset'] = 'yes';
                  $json['message'] = Filter::msgOk(Lang::$word->_UA_INFO5, false);
                  Security::writeLog(Lang::$word->_USER . ' ' . $this->username. ' ' . Lang::$word->_LG_PASS_RESET, "user", "no", "user");
				  
			} else {
				$json['status'] = 'error';
            	$json['message'] = Filter::msgError($aResapi['message'], false);
			
			}
			
		}else {
			$json['message'] = Filter::msgStatus();
			
		}
							
		print json_encode($json);
	  }
      /**
       * User::passReset()
       * 
       * @return
       */
	  public function passReset_bak()
	  {
	      if (isset($_POST['uname']) && $_POST['uname'] != '') {
		      $getQ = "SELECT s.question_pwd,s.answer_pwd FROM acms_samachic_ss s,". self::uTable ." u  WHERE  s.uid=u.id and u.username='".$_POST['uname']."'";
			 // echo $getQ;
			  $dataSS = self::$db->first($getQ);
			  if ($dataSS) {
			  	 if ($dataSS->question_pwd != $_POST['question_pwd']) {
			  	 	Filter::$msgs['question_pwd'] = "Select Question Incorrect";
			  	 }
				 if ($dataSS->answer_pwd != $_POST['answer_pwd']) {
				 	Filter::$msgs['answer_pwd'] = "Select Answer Incorrect";
				 }
			  }
		  }
	
		  Filter::checkPost('uname', Lang::$word->_UR_USERNAME_R);
		  Filter::checkPost('email', Lang::$word->_UR_EMAIL_R);
		  $uname = $this->usernameExists($_POST['uname']);
		  if (strlen($_POST['uname']) < 4 || strlen($_POST['uname']) > 30 || !preg_match("/^[a-z0-9_-]{4,15}$/", $_POST['uname']) || $uname != 3)
			  Filter::$msgs['uname'] = Lang::$word->_UR_USERNAME_R0;
	
	
		  if (!$this->emailExists($_POST['email']))
			  Filter::$msgs['uname'] = Lang::$word->_UR_EMAIL_R3;
	
		  Filter::checkPost('captcha', Lang::$word->_UA_PASS_RTOTAL_R);
	
		  if ($_SESSION['captchacode'] != $_POST['captcha'])
			  Filter::$msgs['captcha'] = Lang::$word->_UA_PASS_RTOTAL_R1;
	      
		  
		  
		  if (empty(Filter::$msgs)) {
	
			  $user = $this->getUserInfo($_POST['uname']);
			  $randpass = $this->getUniqueCode(12);
			  $newpass = sha1($randpass);
	
			  $data['password'] = $newpass;
	
			  self::$db->update(self::uTable, $data, "username = '" . $user->username . "'");
	
			  require_once (BASEPATH . "lib/class_mailer.php");
			  $row = Core::getRowById(Content::eTable, 2);
	
			  $body = str_replace(array(
				  '[USERNAME]',
				  '[PASSWORD]',
				  '[URL]',
				  '[LINK]',
				  '[IP]',
				  '[SITE_NAME]'), array(
				  $user->username,
				  $randpass,
				  SITEURL,
				  doUrl(false, Registry::get("Core")->login_page, "page"),
				  $_SERVER['REMOTE_ADDR'],
				  Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
	
			  $newbody = cleanOut($body);
	
			  $mailer = Mailer::sendMail();
			  $message = Swift_Message::newInstance()
						->setSubject($row->{'subject' . Lang::$lang})
						->setTo(array($user->email => $user->username))
						->setFrom(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
						->setBody($newbody, 'text/html');
	
			  if (self::$db->affected() and $mailer->send($message)) {
				  Security::writeLog(Lang::$word->_USER . ' ' . $user->username . ' ' . Lang::$word->_UA_PASS_R_OK, "", "no", "content");
				  $json['status'] = 'success';
				  $json['message'] = Filter::msgOk(Lang::$word->_UA_PASS_R_OK, false);
			  } else {
				  $json['status'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_UA_PASS_R_ERR, false);
			  }
			  print json_encode($json);
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	  
      /**
       * User::activateUser()
       * 
       * @return
       */
	  public function activateUser_old()
	  {
	
		  Filter::checkPost('email', Lang::$word->_UR_EMAIL_R);
	
		  if (!$this->emailExists($_POST['email']))
			  Filter::$msgs['email'] = Lang::$word->_UR_EMAIL_R3;
	
		  Filter::checkPost('token', Lang::$word->_UA_TOKEN_R1);
	
		  if (!$this->validateToken($_POST['token']))
			  Filter::$msgs['token'] = Lang::$word->_UA_TOKEN_R;
	
		  if (empty(Filter::$msgs)) {
			  $email = sanitize($_POST['email']);
			  $token = sanitize($_POST['token']);
	
			  $data = array('token' => 0, 'active' => (Registry::get("Core")->auto_verify) ? "y" : "n");
	
			  self::$db->update(self::uTable, $data, "email = '" . $email . "' AND token = '" . $token . "'");
			  $message = (Registry::get("Core")->auto_verify == 1) ? Lang::$word->_UA_TOKEN_OK1 : Lang::$word->_UA_TOKEN_OK2;
	
			  if (self::$db->affected()) {
				  Security::writeLog($message, "", "no", "content");
				  $json['status'] = 'success';
				  $json['message'] = Filter::msgOk($message, false);
			  } else {
				  $json['status'] = 'success';
				  $json['message'] = Filter::msgAlert(Lang::$word->_UA_TOKEN_R_ERR, false);
			  }
			  print json_encode($json);
	
		  } else {
			  $json['message'] = Filter::msgStatus();
			  print json_encode($json);
		  }
	  }
	public function gen_param_for_create_trade($kGroup)
	{
		$adminConf 	= file_get_contents("http://65.60.60.131/~api2rfforex/product_control/script/");	
		$aConf 		=  json_decode($adminConf,true);
		$aParam = array();
		if (isset($aConf[$kGroup]) && $aConf[$kGroup] != '') {
			if (preg_match("/mt4_group/i", $kGroup)) {
			    $aParam['product_type_id'] = 1;
			} elseif(preg_match("/ct_/i", $kGroup)) {
			    $aParam['product_type_id'] = 2;
			}
			if (preg_match("/mt4_group_demo/i", $kGroup)) {
			    $aParam['is_demo'] = 1;
			} elseif (preg_match("/ct_group_demo/i", $kGroup)) {
			    $aParam['is_demo'] = 1;
			}
			$aG 				= explode(",",$aConf[$kGroup]);
			$aParam['group'] 	= $aG[0];
			$aParam['leverage'] = $aG[2];
		
			
		}
		return $aParam;
	}

	public function activateUser()
	{
		Filter::checkPost('email', Lang::$word->_UR_EMAIL_R);
		if (!$this->emailExists_activate($_POST['email']))
              Filter::$msgs['email'] = Lang::$word->_UR_EMAIL_R3;
		Filter::checkPost('token', Lang::$word->_UA_TOKEN_R1);
          /*
          if (!$this->validateToken($_POST['token']))
              Filter::$msgs['token'] = Lang::$word->_UA_TOKEN_R;
          */
          
          // call curl
		$aPOSTAPI['email'] 			= str_replace(" ", "", $_POST['email']);
		$aPOSTAPI['activate_token'] = str_replace(" ", "", $_POST['token']);         
         require_once (MODPATH . "tradersystem/admin_class.php");
         Registry::set('TraderSystem', new TraderSystem());
          $aPOSTAPI['cmd'] = 'confirm_register';
         $aAcc = Registry::get("TraderSystem") ->call_curl("confirm_register","POST",$aPOSTAPI);
         
         // Array ( [isSuccess] => 1 [operation] => confirm_regster [message] => "Request successfully." )
         
         if (($aAcc['isSuccess'] == 1) && ($aAcc['operation'] == 'confirm_register') )
         {
			$aUserInfo= $this->getUserInfo($aPOSTAPI['email']);
			if (is_null($aUserInfo->auto_create_acc) === false) 
			{
				$aParamCreate = $this->gen_param_for_create_trade($aUserInfo->auto_create_acc);
				if (count($aParamCreate) > 0) {
					$_SESSION['securekey'] = $aAcc['api_key'];
					 /*
					 	$json['apikey'] = $_SESSION['securekey'];
					 	 $json['paramaa'] = $aParamCreate;
					  * */
					 $aParamCreate['cmd'] ='broke_create_account';
					$res = Registry::get("TraderSystem") ->call_curl("broke_create_account","POST",$aParamCreate);
				 } else {
				 	 $json['notttttt'] = 'yy6yyy';
				 }
				 
			}
			if (Registry::get("Core")->notify_admin) {
				 require_once (BASEPATH . "lib/class_mailer.php");
				  $arow = Core::getRowById(Content::eTable, 26);
				  $abody = str_replace(array(
					  '[USERNAME]',
					  '[EMAIL]',
					  '[NAME]',
					  '[IP]'), array(
					  $aPOSTAPI['email'],
					  $aPOSTAPI['email'],
					  $aAcc['name_user'],
					  $_SERVER['REMOTE_ADDR']), $arow->{'body' . Lang::$lang});
	
				  $anewbody = cleanOut($abody);
	
				  $amailer = Mailer::sendMail();
				  $amessage = Swift_Message::newInstance()
							->setSubject($arow->{'subject' . Lang::$lang})
							->setTo(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
							->setFrom(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
							->setBody($anewbody, 'text/html');
	
				  $amailer->send($amessage);
			  }
		} else {
			$json['resapi'] = $aAcc;
			Filter::$msgs['token'] =isset($aAcc['errMessage']) ? $aAcc['errMessage']:'Pleae try again.'; //Lang::$word->_UA_TOKEN_R;
		}
          

		if (empty(Filter::$msgs)) {
			$email = sanitize($_POST['email']);
			$token = sanitize($_POST['token']);

  			$data = array('token' => 0,'step_no'=>1, 'active' => (Registry::get("Core")->auto_verify) ? "y" : "n");

  			// self::$db->update(self::uTable, $data, "email = '" . $email . "' AND token = '" . $token . "'");
  
  			self::$db->update(self::uTable, $data, "email = '" . $email . "'");
  
			$message = (Registry::get("Core")->auto_verify == 1) ? Lang::$word->_UA_TOKEN_OK1 : Lang::$word->_UA_TOKEN_OK2;

  			if (self::$db->affected()) {
      			Security::writeLog($message, "", "no", "content");
				$json['status'] = 'success';
				$json['message'] = Filter::msgOk($message, false);				  
				$json['gotopage'] = '/page/login';
			} else {
     			 $json['status'] = 'success';
      			$json['message'] = Filter::msgAlert(Lang::$word->_UA_TOKEN_R_ERR, false);
      		}
      		print json_encode($json);

  		} else {
      		$json['message'] = Filter::msgStatus();
      		print json_encode($json);
  		}
	}

	  /**
	   * Users::getUserData()
	   * 
	   * @return
	   */
	  public function getUserData()
	  {
		  $sql = "SELECT * FROM " . self::uTable 
		  . "\n WHERE id = " . $this->uid;
		  $row = self::$db->first($sql);
	
		  return ($row) ? $row : 0;
	  }
	  
	  /**
	   * Users::getUserData()
	   * 
	   * @return
	   */
	  public function getUserDataDtl()
	  {
	
		  $sql = "SELECT * FROM users "
		  . "\n WHERE id = " . $this->uid;
		  
		  $row = self::$db->first($sql);
	
		  return ($row) ? $row : 0;
	  }
	   
	  

	  

      /**
       * User::trialUsed()
       * 
       * @return
       */
	  public function trialUsed()
	  {
	
		  $sql = "SELECT trial_used" 
		  . "\n FROM " . self::uTable 
		  . "\n WHERE id =" . $this->uid 
		  . "\n LIMIT 1";
		  
		  $row = self::$db->first($sql);
		  return ($row->trial_used == 1) ? true : false;
	  }

	  
	  /**
	   * Users::validateMembership()
	   * 
	   * @return
	   */
	  public function validateMembership()
	  {
		  
		  $sql = "SELECT mem_expire" 
		  . "\n FROM " . self::uTable
		  . "\n WHERE id = " . $this->uid
		  . "\n AND TO_DAYS(mem_expire) > TO_DAYS(NOW())";
		  $row = self::$db->first($sql);
		  
		  return ($row) ? $row : 0;
	  }
	  	  	  	  
	  /**
	   * Users::usernameExists()
	   * 
	   * @param mixed $username
	   * @return
	   */
	  private function usernameExists($username)
	  {
	
		  $username = sanitize($username);
		  if (strlen(self::$db->escape($username)) < 4)
			  return 1;
	
		  //Username should contain only alphabets, numbers, underscores or hyphens.Should be between 4 to 15 characters long
		  $valid_uname = "/^[a-zA-Z0-9_-]{4,15}$/";
		  if (!preg_match($valid_uname, $username))
			  return 2;
	
		  $sql = self::$db->query("SELECT username" 
		  . "\n FROM " . self::uTable
		  . "\n WHERE username = '" . $username . "'" 
		  . "\n LIMIT 1");
	
		  $count = self::$db->numrows($sql);
	
		  return ($count > 0) ? 3 : false;
	  } 	
	   
	    private function emailExists_activate($email)
	  {
		  
		  $sql = self::$db->query("SELECT email,active" 
		  . "\n FROM " .self::uTable
		  . "\n WHERE email = '" . sanitize($email) . "'" 
		  . "\n LIMIT 1");
		  
		  if (self::$db->numrows($sql) == 1) {
		  	
		  	  return true;
			 
		  } else
			  return false;
	  }
	  /**
	   * User::emailExists()
	   * 
	   * @param mixed $email
	   * @return
	   */
	  private function emailExists($email)
	  {
		  
		  $sql = self::$db->query("SELECT email,active" 
		  . "\n FROM " .self::uTable
		  . "\n WHERE email = '" . sanitize($email) . "' and active='y'" 
		  . "\n LIMIT 1");
		  
		  if (self::$db->numrows($sql) == 1) {
				return true;
		  } else
			  	return false;
	  }
	  
	  /**
	   * User::isValidEmail()
	   * 
	   * @param mixed $email
	   * @return
	   */
	  private function isValidEmail($email)
	  {
		  if (function_exists('filter_var')) {
		  	
			  if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			  	
				  return true;
			  } else {
				
				  return false;
			  }
		  } else {
				
			  return preg_match('/^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/', $email);
		  }
	  } 	

      /**
       * User::validateToken()
       * 
       * @param mixed $token
       * @return
       */
	  private function validateToken($token)
	  {
		  $token = sanitize($token, 40);
		  $sql = "SELECT token" 
		  . "\n FROM " . self::uTable 
		  . "\n WHERE token ='" . self::$db->escape($token) . "'" 
		  . "\n LIMIT 1";
		  
		  $result = self::$db->query($sql);
	
		  if (self::$db->numrows($result))
			  return true;
	  }
	  
	  /**
	   * Users::getUniqueCode()
	   * 
	   * @param string $length
	   * @return
	   */
	  private function getUniqueCode($length = "")
	  {
		  $code = md5(uniqid(rand(), true));
		  if ($length != "") {
			  return substr($code, 0, $length);
		  } else
			  return $code;
	  }

	  /**
	   * Users::generateRandID()
	   * 
	   * @return
	   */
	  private function generateRandID()
	  {
		  return sha1($this->getUniqueCode(24));
	  }

      /**
       * Users::getPermissionList()
       * 
       * @param bool $access
       * @return
       */
	  public function getPermissionList($access = false)
	  {
	
		  $moddata = self::$db->fetch_all("SELECT title" . Lang::$lang . ", modalias FROM " . Content::mdTable . " WHERE hasconfig = 1");
		  $plugdata = self::$db->fetch_all("SELECT title" . Lang::$lang . ", plugalias FROM " . Content::plTable);
		  //$tradedata = self::$db->fetch_all("SELECT *  FROM acms_menu_trader WHERE hasconfig = 1");
          
         require_once (MODPATH . "tradersystem/admin_class.php");
         Registry::set('TraderSystem', new TraderSystem());
         $menu_t = Registry::get("TraderSystem") ->call_curl_get("menu_trader?cmd=menu_trader","GET"); 
         $tradedata = $menu_t['message'];
      
	
		  $data = '';
	
		  if ($access) {
			  $arr = explode(",", $access);
			  reset($arr);
		  }
	
		  $data .= '<select name="access[]" multiple="multiple">';
		  foreach (self::getPermissionValues() as $key => $val) {
			  if ($access && $arr) {
				  $selected = (in_array($key, $arr)) ? " selected=\"selected\"" : "";
			  } else
				  $selected = null;
			  $data .= "<option $selected value=\"" . $key . "\">" . $val . "</option>\n";
		  }
		  unset($val);
	      if ($tradedata) {
			  $data .= "<optgroup label=\"TRADER SYSTEM\">\n";
			  foreach ($tradedata as $pval) {
				  if ($access && $arr) {
					  // $selected = (in_array($pval->menu_access, $arr)) ? " selected=\"selected\"" : "";
                      $selected = (in_array($pval['menu_access'], $arr)) ? " selected=\"selected\"" : "";
				  } else
					  $selected = null;
				  //$data .= "<option $selected value=\"" . $pval->menu_access . "\">-- " . $pval->menu_title . "</option>\n";
                  $data .= "<option $selected value=\"" . $pval['menu_access'] . "\">-- " . $pval['menu_title'] . "</option>\n";
			  }
			  $data .= "</optgroup>\n";
			  unset($pval);
		  }
		  
		  if ($moddata) {
			  $data .= "<optgroup label=\"" . Lang::$word->_N_MODS . "\">\n";
			  foreach ($moddata as $mval) {
				  if ($access && $arr) {
					  $selected = (in_array($mval->modalias, $arr)) ? " selected=\"selected\"" : "";
				  } else
					  $selected = null;
				  $data .= "<option $selected value=\"" . $mval->modalias . "\">-- " . $mval->{'title' . Lang::$lang} . "</option>\n";
			  }
			  $data .= "</optgroup>\n";
			  unset($mval);
		  }
	
		  if ($plugdata) {
			  $data .= "<optgroup label=\"" . Lang::$word->_N_PLUGS . "\">\n";
			  foreach ($plugdata as $pval) {
				  if ($access && $arr) {
					  $selected = (in_array($pval->plugalias, $arr)) ? " selected=\"selected\"" : "";
				  } else
					  $selected = null;
				  $data .= "<option $selected value=\"" . $pval->plugalias . "\">-- " . $pval->{'title' . Lang::$lang} . "</option>\n";
			  }
			  $data .= "</optgroup>\n";
			  unset($pval);
		  }
		  
	
		  $data .= "</select>";
	
		  return $data;
	  }

	  /**
	   * Users::getAcl()
	   * 
	   * @param string $content
	   * @return
	   */
	  public function getAcl($content)
	  {
		  if ($this->userlevel == 8) {
			  $arr = explode(",", $this->access);
			  reset($arr);
			  //var_dump($this->access);
			 // var_dump($content);
			  if (in_array($content, $arr)) {
				  return true;
			  } else
				  return false;
		  } else
			  return true;
	  }
	  	  
      /**
       * Users::getPermissionValues()
       * 
       * @return
       */
	  private static function getPermissionValues()
	  {
		  $arr = array(
			  'Menus' => Lang::$word->_N_MENUS,
			  'Pages' => Lang::$word->_N_PAGES,
			  'Posts' => Lang::$word->_N_POSTS,
			  'Memberships' => Lang::$word->_N_MEMBS,
			  'Gateways' => Lang::$word->_N_GATES,
			  'Transactions' => Lang::$word->_N_TRANS,
			  'Layout' => Lang::$word->_N_LAYS,
			  'Users' => Lang::$word->_N_USERS,
			  'Configuration' => Lang::$word->_N_CONF,
			  'Templates' => Lang::$word->_N_EMAILS,
			  'Newsletter' => Lang::$word->_N_NEWSL,
			  'Language' => Lang::$word->_N_LANGS,
			  'Logs' => Lang::$word->_N_LOGS,
			  'Fields' => Lang::$word->_N_FIELDS,
			  'FM' => Lang::$word->_FM_TITLE,
			  'System' => Lang::$word->_N_SYSTEM,
			  'Backup' => Lang::$word->_UP_DBACKUP,
			  'Modules' => Lang::$word->_N_MODS,
			  'Plugins' => Lang::$word->_N_PLUGS);
	
		  return $arr;
	  }	  
      
      /*
       * update ib link by isara
       */	  	  	   
       public function updateIBLink() 
      {
            require_once (MODPATH . "tradersystem/admin_class.php");
           Registry::set('TraderSystem', new TraderSystem());
           
           $aPOSTAPI['alias'] = str_replace(" " , "" , $_POST['my_alias']);
		   
		   
		   if (!preg_match('/[^A-Za-z0-9]/', $aPOSTAPI['alias'])) // '/[^a-z\d]/i' should also work.
			{
  					// string contains only english letters & digits

           
         //  $aP['my_id'] = $_POST['my_id'];
         //  $aP['my_alias'] = $_POST['my_alias'];
            
           $aAcc01 = Registry::get("TraderSystem") ->call_curl_get("check_ref_uid","GET",$aPOSTAPI['alias']."?cmd=check_ref_uid"); 
           
          //echo "<pre>"; print_r($aAcc01); echo "</pre>";
          
           if ($aAcc01['uid'] == '') {
           			$aPOSTAPI['cmd'] = 'my_alias';
                  $aAcc = Registry::get("TraderSystem") ->call_curl("my_alias","POST",$aPOSTAPI);
                      
                  //echo "<pre>"; print_r($aAcc); echo "</pre>";
                      
                  if (isset($aAcc['isSuccess']) && $aAcc['isSuccess'] !=1) {
                         
                        Filter::$msgs['register']   = $aAcc['message'] ;
                      $json['message']            = Filter::msgStatus();
                      $json['my_alias']           = $_POST['old_my_alias'];
                        
                } else {
                        $json = array( 'status'=>'success',
                                       'name'=>'success update',
                                       'my_alias'=>$_POST['my_alias'],
                                       'message'=>'<div class="badge col-md-12 badge-success"><span class="glyphicon glyphicon-ok-sign"></span>Success<p> You have successfully updated your referral link.</p></div>');
                }
                  
                  
           } else {
               $json = array( 'status'=>'success',
                                       'name'=>'fail update',
                                       'my_alias'=>$_POST['old_my_alias'],
                                       'message'=>'<div class="badge col-md-12 badge-danger"><span class="glyphicon glyphicon-remove"></span>Fail<p> No update your referral link.</p></div>');
               
            }
           
           $json['isRedirect'] = "yes";
		   
		   } else {
		   	
				 $json = array( 'status'=>'success',
                                       'name'=>'fail update',
                                       'my_alias'=>$_POST['old_my_alias'],
                                       'message'=>'<div class="badge col-md-12 badge-danger"><span class="glyphicon glyphicon-remove"></span>Notice<p> Please use english or number only and no special character or space.</p></div>');
                 $json['isRedirect'] = "yes";
			
		   }

          print json_encode($json);
       }
  }
?>



