<?php

   require_once "class_user.php";
  /**
   * User Class
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: class_user.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  class Usersiz extends Users
  {
      const uTable = "users";
      private static $db;
      

      /**
       * Users::__construct()
       * 
       * @return
       */
      function __construct()
      {
          self::$db = Registry::get("Database");
         // $this->startSession();
      }
      
      
    public function getUserGroup()
    {
        
         require_once (MODPATH . "tradersystem/admin_class.php");
        Registry::set('TraderSystem', new TraderSystem());
        $aResapi = Registry::get("TraderSystem") ->call_curl_get("user_group?cmd=user_group","GET");
        
        if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']==1) {
 
                  $json['status'] = 'success';
                $json['message'] = $aResapi['message'];
                
        } else {
                  
                  $json['status'] = 'info';
                $json['message'] = 'Error';
                
        }
         //print json_encode($json);
         return $json;
       
    }
      
      
      public function getUserGroupByTypeId($type_id)
     {
        
         require_once (MODPATH . "tradersystem/admin_class.php");
        Registry::set('TraderSystem', new TraderSystem());
        $aResapi = Registry::get("TraderSystem") ->call_curl_get("user_group_by_id","GET",$type_id."?cmd=user_group");
        
        if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']==1) {
 
                  $json['status'] = 'success';
                $json['message'] = $aResapi['message'];
                
        } else {
                  
                  $json['status'] = 'info';
                $json['message'] = 'Error';
                
        }
         //print json_encode($json);
         return $json;
       
    }
     
     public function updateUserGroup($type_id,$type_name,$str)
     {
         $arr = array();
        $arr['type_id'] = $type_id;
        $arr['type_name'] = $type_name;
        $arr['limit_access'] = $str;
		$arr['cmd'] = 'user_group';
        
        
         require_once (MODPATH . "tradersystem/admin_class.php");
        Registry::set('TraderSystem', new TraderSystem());
        $aResapi = Registry::get("TraderSystem") ->call_curl("user_group","POST",$arr);
        
        if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']==1) {
 
                  $json['status'] = 'success';
                $json['message'] = $aResapi['message'];
                
        } else {
                  
                  $json['status'] = 'info';
                $json['message'] = 'Error';
                
        }
         //print json_encode($json);
         return $json;
     }
     
     public function addUserGroup($type_name,$str)
     {
         $arr = array();
        //$arr['type_id'] = $type_id;
        $arr['type_name'] = $type_name;
        $arr['limit_access'] = $str;
        $arr['cmd'] = 'user_group';
        
        
         require_once (MODPATH . "tradersystem/admin_class.php");
        Registry::set('TraderSystem', new TraderSystem());
        $aResapi = Registry::get("TraderSystem") ->call_curl("user_group","POST",$arr);
        
        if (isset($aResapi['isSuccess']) && $aResapi['isSuccess']==1) {
 
                  $json['status'] = 'success';
                $json['message'] = $aResapi['message'];
                
        } else {
                  
                  $json['status'] = 'info';
                $json['message'] = 'Error';
                
        }
         //print json_encode($json);
         return $json;
     }

     public function getCommissionByTradeAcc($to_user_email,$from_trade_acc_login)
     {
         //$arr = array();
       
          $sql = "SELECT * from bk_comm_paid_history 
          				where to_user = '".$to_user_email."' 
          					and trader_acc_login = '".$from_trade_acc_login."'
          					and paid_status = 1 ";
        // echo $sql; 
         $row = self::$db->fetch_all($sql);
         return ($row) ? $row : 0;
     }
     
     
     public function getVolumeByTradeAccPending($trade_acc_login,$min_date,$max_date)
     {
         //$arr = array();
       
          $sql = "SELECT sum(volume) as sum_volume 
          				from bk_closed_ordermt4 
          			where login = '".$trade_acc_login."' 
          				and from_unixtime(close_time) >= '".$min_date."'
          				and from_unixtime(close_time) <= '".$max_date."'
          				
          				and is_demo = 0
          				and cmd in ('op_buy' , 'op_sell')
          		 "; // and is_calc = 0 
        // echo $sql; 
         $row = self::$db->fetch_all($sql);
         return ($row) ? $row : 0;
     }
      
  }