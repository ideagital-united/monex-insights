<?php
require_once "class_user.php";
/**
* User Class
*
* @package CMS Pro
* @author prolificscripts.com
* @copyright 2010
* @version $Id: class_user.php, v2.00 2011-04-20 10:12:05 gewa Exp $
*/

if (!defined("_VALID_PHP"))
die('Direct access to this location is not allowed.');

class UsersNc extends Users
{
	const uTable = "users";
	const api_url = "http://qr-easy.net/broker_api_v2/trunk/";
	const version = "v1/";
	private static $db;
	/**
	* Users::__construct()
	* 
	* @return
	*/
	function __construct()
	{
		self::$db = Registry::get("Database");
	// $this->startSession();
	}
	
	function broker_login($data) {
		//print_r($data);
		
		$string = http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."admin_login");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		//return $obj;
 		if($obj["isSuccess"] == 1){
			$_SESSION['admin_secure']['admin_api_key'] = $obj['message']['admin_api_key'];
			$_SESSION['admin_secure']['grant_name']= $obj['message']['grant_detail']['type_name'];
			$_SESSION['admin_secure']['grant_access']= $obj['message']['grant_detail']['limit_access'];
			return true;
		} else {
			return false;
		}
	}
	
	function broker_logout($api_key) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."admin_logout?cmd=admin_logout");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
 		if($obj["isSuccess"] == 1){
			unset($_SESSION['admin_secure']);
			return true;
		} else {
			return false;
		}
	}
	
	function list_user_group($api_key) {
		/*echo "<br>url=".URL_API;
		echo "<br>api=".$api_key;*/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."user_group?cmd=user_group");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8'));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
 		if($obj["isSuccess"] == 1){
			return $obj;
		} else {
			return false;
		}
	}
	
	function set_admin($data){
		$string = http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."set_admin?cmd=set_admin");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		//return $obj;
 		if($obj["isSuccess"] == 1){
			return $obj;
		} else {
			return false;
		}
	}
	
	function set_secure_tool($data,$api_key){
		
		$string = http_build_query($data);
       
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."secure_tool");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		//return $obj;
		if($obj["isSuccess"] == 1){
			return $obj;
		} else {
			return false;
		}
	}

	function verify_secure_tool($data,$api_key){
		//print_r($data);
		$string = http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."verify_secure_tool");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		//return $obj;
		if($obj["isSuccess"] == 1){
			return $obj;
		} else {
			return false;
		}
	}
	
	function reqotp($data,$api_key){
		$string = http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."reqotp");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		//return $obj;
		if($obj["isSuccess"] == 1){
			return $obj;
		} else {
			return false;
		}
	}
	
	function resetsecure($data,$api_key){
		$string = http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API."resetsecure");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		//return $obj;
		if($obj["isSuccess"] == 1){
			return $obj;
		} else {
			return $obj;
		}
	}

    function setmobile($data,$api_key){
        $string = http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,  URL_API."update_user_mobile");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
        $r = curl_exec($ch);
        curl_close($ch);
        $obj = json_decode($r,true);
        //return $obj;
        if($obj["isSuccess"] == 1){
            return $obj;
        } else {
            return false;
        }
    }
}