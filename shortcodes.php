<?php
  /**
   * Features
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: index.php, v1.00 2014-05-18 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("init.php");
  
  $assets = false;
?>
<?php include(THEMEDIR."/header.tpl.php");?>
<link href="<?php echo SITEURL;?>/assets/highliter.css" rel="stylesheet" type="text/css"/>
<div id="features" class="prolific-grid main-content">
  <div class="prolific-content">
    <div class="prolific icon message"><i class="information letter icon"></i>
      <div class="content">This page contains some of the CMS pro features, tutorials, and other useful documentation.<br />
        Below is the list of pages where you can find useful information about CMS pro usage.</div>
    </div>
    <div id="altnav" class="vspace">
      <div class="prolific five fluid buttons"> <a data-help="features" class="prolific teal button">Basic Features</a> <a data-help="requirements" class="prolific positive button">System Requirements</a> <a data-help="install" class="prolific info button">Installation Manual</a> <a data-help="transfer" class="prolific purple button">Transferring Site</a> <a data-help="vids" class="prolific warning button">Video Tutorials</a> </div>
      <div class="prolific divider"></div>
      <h3>Shortcodes</h3>
      <div class="prolific four fluid buttons"> <a data-help="grid" class="prolific button">Grid</a> <a data-help="buttons" class="prolific button">Buttons</a> <a data-help="dividers" class="prolific button">Dividers</a> <a data-help="lists" class="prolific button">Lists</a> </div>
      <div class="prolific small divider"></div>
      <div class="prolific four fluid buttons"> <a data-help="labels" class="prolific button">Labels</a> <a data-help="segments" class="prolific button">Segments</a> <a data-help="icons" class="prolific button">Icons</a> <a data-help="messages" class="prolific button">Messages</a> </div>
      <div class="prolific small divider"></div>
      <div class="prolific four fluid buttons"> <a data-help="progress" class="prolific button">Progress Bars</a> <a data-help="tabs" class="prolific button">Tabs</a> <a data-help="carousel" class="prolific button">Carousel</a> <a data-help="tooltips" class="prolific button">Tooltips</a> </div>
    </div>
    <div id="docs">
      <h2 class="small-bottom-space">CMS pro Features</h2>
      <div class="prolific segment">
        <h3><i class="laptop icon"></i> Design Sites Your Way<i class="arrow down icon push-right"></i></h3>
        <div class="box-content">
          <p> Build your websites with the custom templates and unique look. CMS pro uses a PHP "de facto" standard - html templates, that separates presentation from business logic. That helps you avoid the HTML/PHP code mess.<br>
            <br>
            You may use ready-to-use included templates or order a new one from CMS pro team of professionals. You can choose a template from 3rd party store and our team will adapt it to use with CMS pro. Al least you can create your own template, no PHP knowledge required!<br>
            <br>
            Along with the custom look of your website based on CMS pro you may order custom functionality. Your satisfaction is our priority. </p>
        </div>
      </div>
      <div class="prolific segment">
        <h3><i class="umbrella icon"></i> Tons of Plugins/Modules<i class="arrow down icon push-right"></i></h3>
        <div class="box-content">
          <p> CMS pro is a scalable software that has a modular structure. The basic core has a full set of functionality needed to operate a website that can be transformed into a powerful portal by means of plugins/modules.<br>
            <br>
            Add the new abilities when you need them! Just install the appropriate plugin to get the new functionality. For example, if you need a portfolio - just install the portfolio module! There are text and video tutorials in our knowledgebase to help you get familiar with CMS pro plugins/modules quickly.<br>
            <br>
            'Installing a module/plugin' sounds seriously, but you have nothing to worry about. It's not that complicated task as it might sound. You only need to perform a few easy steps to get your module/plugin installed. If you want out team to install all available module/plugin for you that could be done for a small additional fee.<br>
            <br>
            Currently we have about 20+ plugins/modules. Below you can find a list of the plugins/modules included:</p>
          <div class="prolific relaxed divided list">
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header">Accordion Menu</div>
                <div class="description">Vertical accordion menu</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header">News Slider</div>
                <div class="description">Latest news ticker</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header">Image Slider</div>
                <div class="description">jQuery responsive touch enabled image slider.</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header">jQuery Tabs</div>
                <div class="description">Responsive tabbed content</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Event Manager</div>
                <div class="description">Manage list of your site related events</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Latest Twitts</div>
                <div class="description">Shows your latest twitts with caching capabilities.</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header">Elastic Slider</div>
                <div class="description">Elastic responsive image slider</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Gallery</div>
                <div class="description">Image gallery with thumbnail navigation</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Ajax Poll</div>
                <div class="description">jQuery Ajax poll plugin</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Video Slider</div>
                <div class="description">Youtube/Vimeo player with playlist support</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Donation Plugin</div>
                <div class="description">Collect donations via paypal</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Upcoming Event</div>
                <div class="description">Setup upcoming event with cauntdown capabilities</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Google Maps</div>
                <div class="description">Built in support for multiple google maps</div>
              </div>
            </div>
            <div class="item"> <i class="checkmark icon"></i>
              <div class="content">
                <div class="header"> Advert Plugin</div>
                <div class="description">Create unlimited campaigns. Support for google Ads/html and images</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="prolific segment">
        <h3><i class="terminal icon"></i> Full Source Editing<i class="arrow down icon push-right"></i></h3>
        <div class="box-content">
          <p> We do not encrypt sensitive CMS pro files in any way. CMS pro code is fully open and you can make any changes. You can hire our team to make modifications for you or find any 3rd party developer to take care of your requests.<br>
            <br>
            CMS pro is written using PHP5 object oriented concepts, it's easy to extend and add extra functionality. </p>
        </div>
      </div>
      <div class="prolific segment">
        <h3><i class="chat icon"></i> Multi Lingual CMS<i class="arrow down icon push-right"></i></h3>
        <div class="box-content">
          <p> CMS pro uses UTF-8 charset that technically allows to translate it to any language. No matter if it's English, Russian, Hebrew or Arabic CMS pro can handle any content easily. Both fronted parts and admin panel could be easily translated. </p>
        </div>
      </div>
      <div class="prolific segment">
        <h3><i class="dashboard icon"></i> Powerful Dashboard<i class="arrow down icon push-right"></i></h3>
        <div class="box-content">
          <p> CMS pro comes with the powerful admin panel that allows to manage all the content on your website pages. You don't need any technical knowledge at all to operate your website. You only need a browser and basic skills to work in CMS pro admin dashboard.<br>
            <br>
            The Admin Dashboard is logically divided into several sections that are responsible for their own functionality set. Below is the list of Admin Dashboard functionality:</p>
          <ul class="prolific list">
            <li>Configure the website in own appropriate way</li>
            <li>Edit pages content using powerful WYSIWYG editor</li>
            <li>Modify your existing menus and create new menus</li>
            <li>Install and modify plugins</li>
            <li>Backup, restore and optimize database tables</li>
            <li>Visually control plugins using drag&amp;drop technique</li>
            <li>Manage files</li>
            <li>View various statistics &amp; reports</li>
            <li>Control user accounts, and setup privileges for them</li>
            <li>... and many more</li>
          </ul>
        </div>
      </div>
      <div class="prolific segment">
        <h3><i class="globe icon"></i> SEO Friendly<i class="arrow down icon push-right"></i></h3>
        <div class="box-content">
          <p> CMS pro URLs are seo and human friendly so you don't need to purchase additional modules, or hire a 3rd party SEO manager to give you the best results. The script does this automatically.<br>
            <br>
            We spent many hours reading the most respective SEO advisors to prepare the good HTML structure for you. Each page has the most informative header (h1), titles can be also changed.<br>
            <br>
            Administrator can change meta data for each page.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
    /* == Features == */
    $("#altnav a").click(function () {
        $('#altnav a.active').not(this).removeClass('active');
        $(this).toggleClass("active");
		var option = $(this).data('help');
        $("#docs").load('help/' + option + '.html',{cache:false}).fadeIn('slow');
		$('html, body').animate({
            scrollTop: 0
        }, 600);
    });
	
    $('body').on('click', '#features h3', function () {
        var answer = $(this).next('.box-content');
        var active = $(this).children('i.arrow');

        if (answer.is(':visible')) {
            answer.slideUp(100, function () {
                answer.slideUp();
                active.removeClass('vertically flipped')
            });
        } else {
            answer.fadeIn(300, function () {
                answer.slideDown();
                active.addClass('vertically flipped')
            });
        }

        return false;
    });
});
// ]]>
</script>
<?php include(THEMEDIR."/footer.tpl.php");?>
