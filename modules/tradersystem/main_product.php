<form class="form-horizontal" id="prolific_form_product" name="prolific_form_product" method="post">
<div class="form-group">
	<div class="col-md-12">
		<div class="heading_b">Choose Your Products</div>
		<span class="help-block">[Explain here that we want to provide you with suitable products]</span>
	</div>
</div>
<div class="fields">
		<div class="field">
		<label>Product: Which products would you like to trade?</label>
		<div class="inline-group">
            <label class="radio">
              <input name="product_type" type="radio" value="3" >
              <i></i>Both-Share Trading and FX</label>
            <label class="radio">
              <input name="product_type" type="radio" value="1">
              <i></i>Share Trading</label>
            <label class="radio">
              <input name="product_type" type="radio" value="2">
              <i></i>FX</label>
          </div>
	</div>
</div>

<div class="fields for_share_trading">
		<div class="field">
		<label>Share Trading Account</label>
	</div>
</div>
<div class="fields for_share_trading">
		<div class="field">
		<label>Years of experience in Share Trading</label>
		<select name ="share_trade_experience" class="form-control">
			<option value="">Please Select</option>
			<option value="0">0 years</option>
			<option value="1">1-2 years</option>
			<option value="3">3-5 years</option>
			<option value="5">5+ years</option>
		</select>
	</div>
</div>
<div class="fields for_share_trading">
		<div class="field">
		<label>Which currency would you like to use to fund your Share Trading account?</label>
		<select name ="share_trade_currency" class="form-control">
			<option value="">Please Select</option>
			<option value="AUD">AUD</option>
			<option value="USD">USD</option>
			<option value="HKD">HKD</option>
			<option value="EUR">EUR</option>
			<option value="SIN">SIN</option>
			<option value="JPY">JPY</option>
		</select>
	</div>
</div>

<div class="fields for_fx">
		<div class="field">
		<label>FX account</label>
	</div>
</div>
<div class="fields for_fx">
		<div class="field">
		<label>Years of experience in FX</label>
		<select name ="fx_experience" class="form-control">
			<option value="">Please Select</option>
			<option value="0">0 years</option>
			<option value="1">1-2 years</option>
			<option value="3">3-5 years</option>
			<option value="5">5+ years</option>
		</select>
	</div>
</div>
<div class="fields for_fx">
		<div class="field">
		<label>Which currency would you like to use to fund your FX?</label>
		<select name ="fx_currency" class="form-control">
			<option value="">Please Select</option>
			<option value="AUD">AUD</option>
			<option value="USD">USD</option>
			<option value="HKD">HKD</option>
			<option value="EUR">EUR</option>
			<option value="SIN">SIN</option>
			<option value="JPY">JPY</option>
		</select>
	</div>
</div>
<div class="fields for_fx">
		<div class="field">
		<label>Leverage</label>
		<div class="inline-group">
            <label class="radio">
              <input name="fx_leverage" type="radio" value="50" checked="checked">
              <i></i>50:1</label>
            <label class="radio">
              <input name="fx_leverage" type="radio" value="100" >
              <i></i>100:1</label>
            <label class="radio">
              <input name="fx_leverage" type="radio" value="200" >
              <i></i>200:1</label>
          </div>
          
          <div class="inline-group">
            <label class="checkbox">
              <input name="fx_margin_ok" type="checkbox" value="1">
              <i></i>[Understanding of Margin Call]</label>
            <label class="checkbox">
              <input name="fx_losses_ok" type="checkbox" value="1" >
              <i></i>[Understanding that losses may exceed deposits]</label>
          </div>
	</div>
</div>
<div class="fields">
		<div class="field">
		<label>Level of Income</label>
		<select name ="level_income"  class="form-control">
			<option value="">Please Select</option>
			<option value="19999">Less than 20,000</option>
			<option value="20000">20,000 - 50,0000</option>
			<option value="50000">50,000 - 100,0000</option>
			<option value="100000">100,000 - 200,0000</option>
			<option value="2000100">+200,000</option>
		</select>
	</div>
</div>
<div class="text-center">
	<input name="doProfile_product" type="hidden" value="1">
		<button data-url="/modules/tradersystem/ajax_profile.php" data-frm="prolific_form_product"  type="button" name="dosubmit" class="prolific button btn btn-success">
		Continue <i class="fa fa-long-arrow-right"></i></button>
</div>
</form>
