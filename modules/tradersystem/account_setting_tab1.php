<?php
$isLock = 	$tradersystem->chk_verify();
?>
<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post">
<div class="form-group">
	<div class="col-md-12">
		<div class="heading_b"><?php echo Lang::$word->_STANDARD_DETAIL;?></div>
	</div>
</div>
<div class="form-group">
	<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_USERNAME;?></label>
	<div class="col-md-10">
		<input type="text" name="username" id="profile_username" readonly="" class="form-control" value="<?php echo $user->username;?>">
	</div>
</div>

<div class="form-group">
	<label for="user_password" class="col-md-2 control-label"><?php echo Lang::$word->_UR_EMAIL;?></label>
	<div class="col-md-10">
		<input type="email" id="user_password" readonly="" class="form-control" value="<?php echo $usr->email;?>">
	</div>
</div>
<div class="form-group">
	<label for="user_password" class="col-md-2 control-label"><?php echo Lang::$word->_UR_IS_NEWSLETTER;?></label>
	<div class="col-md-10">
		
		<input type="checkbox" name="newsletter" <?php echo getChecked($usr->newsletter, 1);?> class="bs_switch">
	</div>
</div>
<div class="form-group">
	<div class="col-md-12">
		<div class="heading_b"><?php echo Lang::$word->_PERSONAL_DETAIL;?></div>
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label"><?php echo Lang::$word->_UR_FNAME;?></label>
	<div class="col-md-10">
		<?php if($isLock==1){echo "<span class='form-control'>".$usr->fname."</span>";}else{?>
		<input name="fname" type="text" value="<?php echo $usr->fname;?>" id="fname" class="form-control" >
		<?php } ?>
	</div>
</div>
<div class="form-group">
	<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_UR_LNAME;?></label>
	<div class="col-md-10">
		<?php if($isLock==1){echo "<span class='form-control'>".$usr->lname."</span>";}else{?>
		<input  name="lname" type="text" value="<?php echo $usr->lname;?>" class="form-control" >
		<?php } ?>
	</div>
</div>
<div class="form-group">
	<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_CF_PHONE;?></label>
	<div class="col-md-10">
		<?php if(issetvalue($_SESSION['usr_dtl']['user_info']['sms_verified']) == 1){echo "<span class='form-control'>".$_SESSION['usr_dtl']['user_info']['mobile']."</span>";}else{?>
		<input  name="api_mobile" type="text" placeholder="<?php echo $_SESSION['codephonebyip']."XXXXXXXXX"?>"    value="<?php echo $_SESSION['usr_dtl']['user_info']['mobile'];?>" class="form-control" >
		<?php } ?>
	</div>
</div>

<div class="form-group">
	<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_UR_AVATAR;?></label>
	<div class="col-md-10">
		<input type="file" name="avatar"id="avatar" class="form-control" value="Zieme">
	</div>
</div>
<div class="form-group">
	<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_UR_LASTLOGIN;?></label>
	<div class="col-md-10">
		<input type="datetime" id="user_last_login" class="form-control" value="<?php echo $usr->lastlogin;?>" readonly>
	</div>
</div>
<div class="form-group">
	<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_UR_DATE_REGGED;?></label>
	<div class="col-md-10">
		<input type="datetime" id="user_register_date" class="form-control" value="<?php echo $usr->created;?>" readonly>
	</div>
</div>
<div class="form-group" style="display:none">
	<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_UR_BIO;?></label>
	<div class="col-md-10">
		<textarea name="boi" id="exmpl_textarea_autosize" cols="30" rows="4" class="form-control autosize"></textarea>
	</div>
</div>
<?php 
echo $content->rendertCustomFields('profile', $usr->custom_fields);
?>

<div class="text-center">
	<input name="doProfile_manage" type="hidden" value="1">
	
	<button data-url="/ajax/controller.php"  type="button" name="dosubmit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_UA_UPDATE;?></button>
</div>
</form>