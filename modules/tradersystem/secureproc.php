<?php
session_start();
define("_VALID_PHP", true);
require_once("../../init.php");
require_once (MODPATH . "tradersystem/admin_class.php");
Registry::set('TraderSystem', new TraderSystem());
include_once(MODPATHF . 'tradersystem/define_by_api.php');
//print_r($_POST);

if($_POST) {
    
	$api_key = $_SESSION['securekey'];
	$command = $_POST['command'];
	$option = $_POST['option'];
	$backward = $_POST['backward'];
    
	switch ($command)
	{
		case 'select_option' :
			switch ($option)
			{
				case 1:
					$data = array('tool_id'=>$option);
					$result = $usernc->set_secure_tool($data,$api_key);
					//print_r($result);
					if($result['isSuccess'] == 1){
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 1;
						$_SESSION['usr_dtl']['user_info']['enable_email'] = null;
						$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
						$email_to = $result['to'];
						$sent_token = $result['message'];
						unset($result['message']);
						$r = http_build_query($result);
						header("Location: ".$backward."&".$r);
				//start sent mail
						require_once (BASEPATH . "lib/class_mailer.php");
						$token = $sent_token;
						$row = Core::getRowById(Content::eTable, 21);
						$body = str_replace(array('[USERNAME]','[TOKEN]','[URL]','[SITE_NAME]'),array($email_to,$token,SITEURL,Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
						$newbody = cleanOut($body);
						$mailer = Mailer::sendMail();
						$message = Swift_Message::newInstance()
						->setSubject($row->{'subject' . Lang::$lang})
						->setTo(array($email_to => $email_to))
						->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
						->setBody($newbody, 'text/html');
						$mailer->send($message);
						header("Location: $backward");
				// end send mail
					} else {
						$r = http_build_query($result);
						header("Location: ".$backward."&".$r);
					}
				break;
                
				case 2:
					$data = array('tool_id'=>$option);
					$result = $usernc->set_secure_tool($data,$api_key);
					if($result['isSuccess'] == 1){
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 2;
						$_SESSION['usr_dtl']['user_info']['google_auth_use'] = null;
						$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null;
						$_SESSION['usr_dtl']['user_info']['google_auth_key'] = $result['message'];
						unset($result['message']);
						$r = http_build_query($result);
						header("Location: ".$backward."&".$r);
					} else {
						$r = http_build_query($result);
						header("Location: ".$backward."&".$r);
					}
				break;
				
				case 3:
					if($_SESSION['usr_dtl']['user_info']['mobile'] == null) {
					    
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 3;
						header("Location: ".$backward);
                        
					} else {
					    
						$data = array('tool_id'=>$option);
                        
						$result = $usernc->set_secure_tool($data,$api_key);
                        
                     //   echo "<pre>"; print_r($result); echo "</pre>";
                    
						if($result['isSuccess'] == 1) {
							$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 3;
							$_SESSION['usr_dtl']['user_info']['enable_sms'] = null;
							$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
                            
                         $res = Registry::get("TraderSystem")->send_sms_infobip("OTP for activate SMS Security is ".$result['message'],$result['to']);
                            
                         header("Location: ".$backward);
                            
							//$r = http_build_query($result);
							//header("Location: ".$backward."&".$r);
						} else {
							$r = http_build_query($result);
							header("Location: ".$backward."&".$r);
						}
					}
				break;
			}
		break ;
        
		case 'verify_token':
			$token = $_POST['token'];
			$data = array('token'=>$token);
			$result = $usernc->verify_secure_tool($data,$api_key);
			if($result['isSuccess'] == 1){
				switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id'])
				{
					case 1:
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 1;
						$_SESSION['usr_dtl']['user_info']['enable_email'] = 1;
						$_SESSION['usr_dtl']['user_info']['email_verified'] = 1;
					break;
					case 2:
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 2;
						$_SESSION['usr_dtl']['user_info']['google_auth_use'] = 1;
						$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = 1;
					break;
					case 3:
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 3;
						$_SESSION['usr_dtl']['user_info']['enable_sms'] = 1;
						$_SESSION['usr_dtl']['user_info']['sms_verified'] = 1;
					break;
				}
				$r = http_build_query($result);
				header("Location: ".$backward."&".$r);
			} else {
			    
				//$r = http_build_query($result);
				header("Location: ".$backward."&setsms=fail");
			}
		break;
        
		case 'setmobile':
            
			$mobile = $_POST['mobile'];
			$data = array('mobile'=>$mobile);
			$result = $usernc->setmobile($data,$api_key);
            
			if($result['isSuccess'] == 1) {
			    
				$_SESSION['usr_dtl']['user_info']['mobile'] = $mobile;
				$data = array('tool_id'=>$option);
				$result = $usernc->set_secure_tool($data,$api_key);
                
				if($result['isSuccess'] == 1) {
				    
					$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 3;
					$_SESSION['usr_dtl']['user_info']['enable_sms'] = null;
					$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
					
					//$r = http_build_query($result);
					//header("Location: ".$backward."&".$r);
					$res = Registry::get("TraderSystem")->send_sms_infobip("OTP for activate SMS Security is ".$result['message'],$result['to']);
                  header("Location: ".$backward);

				} else {
					$r = http_build_query($result);
					header("Location: ".$backward."&".$r);
				}
			} else {
				$r = http_build_query($result);
				header("Location: ".$backward."&".$r);
			}
		break;
        
		case 'reset':
			$_SESSION['reset'] = 1;
			header("Location: ".$backward);
		break;
	}
} else {
	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
}

?>