<?php
  /**
   * Account Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: account.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
 if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
 if (!$user->logged_in)
      redirect_to(doUrl(false, $core->login, "page"));

 
$usr = $user->getUserData();
//$_SESSION['api']['securekey'] = '7ee4e58685b63946acde07e58de4e9ab';

/*echo "<pre>";print_r($_SESSION);*/
$usr_dtl 			= $_SESSION['usr_dtl']['user_info'];


function chk_document_upload($isvalid,$isfile)
{
	$usr_dtl 			= $_SESSION['usr_dtl']['user_info'];
	$aRes 				= array();
	$aRes['isshow'] 	= false;
	$aRes['istxt'] 		='';//_VIEW_IMAGE
$aImg = getValues($isfile, 'users', "id='".$_SESSION['uid']."'");

if (isset($aImg->$isfile) && $aImg->$isfile != '' && isset($usr_dtl[$isvalid]) && $usr_dtl[$isvalid] == 1){		
		$aRes['istxt'] = '<span><p class="badge badge-success"><i class="glyphicon glyphicon-ok"></i> '.Lang::$word->__VERIFIED.'</p>'
						.' <a href="'.UPLOADURL.'file_doc/'.$usr_dtl[$isfile].'" target="_blank"><span aria-hidden="true" class="glyphicon glyphicon-camera"></span>  '.Lang::$word->_VIEW_IMAGE.'</a></span>';
	} elseif (isset($aImg->$isfile) && $aImg->$isfile != '') {		
		$aRes['istxt'] = '<span><p class="badge badge-warning"><i class="glyphicon glyphicon-time"></i> '.Lang::$word->_PENDING.'</p>'
						.' <a href="'.UPLOADURL.'file_doc/'.$usr_dtl[$isfile].'" target="_blank"><span aria-hidden="true" class="glyphicon glyphicon-camera"></span>  '.Lang::$word->_VIEW_IMAGE.'</a></span>';
	}  else {
		$aRes['isshow']= true;
	}
	
	return $aRes;
}

// สำเนาบัตรชาชน
$aPerson 				= chk_document_upload('id_card_verified','id_card_file_url');
$isShowFrm_posonal 		= $aPerson['isshow'];
$isShowFrm_posonal_text = $aPerson['istxt'];
//สำเนาทะบียนบ้าน
$aAddr 					= chk_document_upload('address_verified','address_file_url');
$isShowFrm_addr 		= $aAddr['isshow'];
$isShowFrm_addr_text 	= $aAddr['istxt'];


?>

<div class="page_content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"><?php echo Lang::$word->_Account_Settng;?></div>
					<div class="panel-body">
						<div class="user_profile">
							
							<ul class="nav nav-tabs" id="tabs_a">
								<li class="active"><a data-toggle="tab" href="#home_a"><?php echo Lang::$word->_MANAGE_PROFILE;?></a></li>
								<li><a data-toggle="tab" href="#profile_a"><?php echo Lang::$word->_VERIFICATION;?></a></li>
								<li><a data-toggle="tab" href="#other_a"><?php echo Lang::$word->_BANK_ACCOUNT;?></a></li>
								<li><a data-toggle="tab" href="#pwd_a"><?php echo Lang::$word->_RESET_PASSWORD;?></a></li>
							</ul>
							<div class="tab-content" id="tabs_content_a">
								<div id="home_a" class="tab-pane fade in active">
									<?php
									include "account_setting_tab1.php";
									?>
								</div>
								<div id="profile_a" class="tab-pane fade">
									<?php
									include "account_setting_tab2.php";
									?>
								</div>
								<div id="other_a" class="tab-pane fade">
									<?php
									include "account_setting_tab3.php";
									?>
								</div>
								<div id="pwd_a" class="tab-pane fade">
									<?php
									include "account_setting_tab4.php";
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php
include "alert_msg.php";
?>
<script>
function set_bankother()
{
	var chkbank_name = $("#bank_name").val();
	if (chkbank_name == 'other') {
		$("#bank_name").hide();
		$("#bank_name_other").show();
	}
	
}
function set_new_bank_name()
{
	//$("#mySelect").append('<option value=1>My option</option>');
	var chkbank_name = $("#bank_name_other").val();
	$("#bank_name").append('<option value="'+chkbank_name+'">'+chkbank_name+'</option>');
	$('#bank_name option[value='+chkbank_name+']').attr('selected','selected');
	$("#bank_name").show();
	$("#bank_name_other").hide();
}
</script>
