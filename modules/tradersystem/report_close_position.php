<?php
// first line of PHP
$defaultTimeZone='UTC';
if(date_default_timezone_get()!=$defaultTimeZone) { date_default_timezone_set($defaultTimeZone); }

$is_mt4  	= (issetvalue($_SESSION['adminconf']['mt4_api_url']) != '') 	? 1 : 0;
$is_ct 		= (issetvalue($_SESSION['adminconf']['ct_api_url']) != '') 		? 1 : 0;


/*
// somewhere in the code
function _date($format="r", $timestamp=false, $timezone=false)
{
    $userTimezone = new DateTimeZone(!empty($timezone) ? $timezone : 'GMT');
    $gmtTimezone = new DateTimeZone('GMT');
    $myDateTime = new DateTime(($timestamp!=false?date("r",(int)$timestamp):date("r")), $gmtTimezone);
    $offset = $userTimezone->getOffset($myDateTime);
    return date($format, ($timestamp!=false?(int)$timestamp:$myDateTime->format('U')) + $offset);
}

// Example 
echo 'System Date/Time: '.date("Y-m-d | h:i:sa").'<br>';
echo 'New York Date/Time: '._date("Y-m-d | h:i:sa", false, 'America/New_York').'<br>';
echo 'Belgrade Date/Time: '._date("Y-m-d | h:i:sa", false, 'Europe/Belgrade').'<br>';
echo 'Belgrade Date/Time: '._date("Y-m-d | h:i:sa", 514640700, 'Europe/Belgrade').'<br>';

*/

?>
<?php 

$usr_login		= $_SESSION['usr_dtl']['trade_acc_list'];
$alistAcc_login = array();
foreach($usr_login as $kacc => $vacc ){
	array_push($alistAcc_login,$vacc['trader_acc_login']);
}
if (count($alistAcc_login) > 0){
	$listselctlogin = join(",",$alistAcc_login);
} else {
	$listselctlogin = '""';
}
if($is_mt4 == 1) {
$sql 	= "SELECT * FROM `bk_closed_ordermt4` 
                where login in (".$listselctlogin.") 
                  AND cmd not in ('OP_BALANCE')
           order by order_id DESC ";
$data 	= Registry::get("Database")->fetch_all($sql);
$alistReport  = ($data) ? $data : 0;
//  and (cmd = 'OP_SELL'  or cmd = 'OP_BUY')  
}
if ($is_ct ==1){
$sql2    = "SELECT * FROM `bk_closed_ctrade` 
                where login in (".$listselctlogin.") 
                and trade_side >= 1 
                and trade_side <= 2    
           order by deal_id DESC ";
           
          // echo $sql2;
           
$data2   = Registry::get("Database")->fetch_all($sql2);
$alistReport2  = ($data2) ? $data2 : 0;
}

?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>My closed position</h3></div>
					
					
					<?php if($is_mt4 == 1) {?>
					<div class="panel-body">
					    
					    
					     <div class="form-group">
                            <div class="col-md-12">
                                <div class="heading_b">MT4</div>
                            </div>
                        </div>
					    
						<table  id="dt_tableTools_lv1" class="table table-striped">
							<thead>
								<tr class="success">
									<th>ORDER ID</th>
									<th>LOGIN</th>
									<th>VOLUME</th>
									<th>SYMBOL</th>
									<th>ORDER TYPE</th>
									<th>PROFIT</th>
									<th>OPEN TIME (UTC)</th>
									<th>CLOSE TIME (UTC)</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($alistReport) {
								        
								    					    
									
									foreach($alistReport as $k => $v) {
										
										$close_time = "";	
								    	$cmd = "";	
										$symbol = "";	
									    
                                    $open_time = date('Y-m-d H:i:s',$v->open_time);
									            
									    if ($v->close_time > 0) {    
									       $close_time = date('Y-m-d H:i:s',$v->close_time);
									    } else {
									        $close_time = "-";
									    }
									    
                                        $volume = $v->volume/100;
                                        
                                    $cmd = str_replace("OP_", "", $v->cmd);
									
									    if ($v->symbol != '') {
									    	$symbol = $v->symbol;
									    }
									
									?>
										<tr>
										<td align="center"><?php echo $v->order_id; ?></td>
										<td align="center"><?php echo $v->login; ?></td>
										<td align="right"><?php echo number_format($volume,2); ?></td>
										<td align="center"><?php echo $symbol; ?></td>
										<td align="center"><?php echo $cmd; ?></td>
										<td align="right"><?php echo number_format($v->profit,2); ?></td>
										<td align="center"><?php echo $open_time; ?></td>
										<td align="center"><?php echo $close_time; ?></td>
								</tr>
									<?php }
								}
								
								?>
								
							</tbody>
						</table>
					</div>
					<?php } ?>
					
					<hr />
        
        
                    <?php if ($is_ct ==1){?>
        
                    <div class="panel-body">
                        
                         <div class="form-group">
                            <div class="col-md-12">
                                <div class="heading_b">cTrader</div>
                            </div>
                        </div>
                        
                        
                        <table id="dt_tableTools_lv2" class="table table-striped">
                            <thead>
                                <tr class="success">
                                    <th>ORDER ID</th>
                                    <th>LOGIN</th>
                                    <th>VOLUME</th>
                                    <th>SYMBOL</th>
                                    <th>ORDER TYPE</th>
                                    <th>PROFIT</th>
                                 <!--   <th>OPEN TIME (UTC)</th>  -->
                                    <th>CLOSE TIME (UTC)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                //echo "<pre>"; print_r($alistReport2);  echo "</pre>";
                                
                                if ($alistReport2) {
                                        
                                    $close_time2 = "";   
                                    $cmd2 = "";                              
                                    
                                    foreach($alistReport2 as $k2 => $v2){
                                                
                                        /* if ($v2->close_time > 0) {    
                                           $close_time2 = date('Y-m-d H:i:s',$v2->utc_last_update_time);
                                        } else {
                                                $close_time2 = "-";
                                       } */
                                       
                                     //  $open_time2 = $v2->UTC_LAST_UPDATE_DATETIME;
                                       
                                       $close_time2 = $v2->UTC_LAST_UPDATE_DATETIME;
                                        
                                   $volume2 = $v2->CLOSED_VOLUME/10000000;
                                        
                                     //   $cmd2 = str_replace("OP_", "", $v->trade_side);
                                     
                                     if($v2->TRADE_SIDE == 1) {
                                         $cmd2 = "BUY";
                                 } else if ($v2->TRADE_SIDE == 2) {
                                         $cmd2 = "SELL";
                                 } else {
                                         $cmd2 = "-";
                                 }
                                 
                                 $delta = $v2->DELTA/100;
                                 
                                     $profit2 = number_format($delta,2);
                                     
                                    
                                    ?>
                                        <tr>
                                        <td align="center"><?php echo $v2->DEAL_ID; ?></td>
                                        <td align="center"><?php echo $v2->LOGIN; ?></td>
                                        <td align="right"><?php echo number_format($volume2,2); ?></td>
                                        <td align="center"><?php echo $v2->NAME; ?></td>
                                        <td align="center"><?php echo $cmd2; ?></td>
                                        <td align="right"><?php echo number_format($profit2,2); ?></td>
                                     <!--   <td align="right"><?php echo $open_time2; ?></td>  -->
                                        <td align="center"><?php echo $close_time2; ?></td>
                                </tr>
                                    <?php }
                                }
                                
                                ?>
                               
                            </tbody>
                        </table>
                    </div>
               
					<?php } ?>
					
					
					
					
				</div>
			</div>
		</div>
		
		
		
		
		
		
	</div>
</div>
</div>
<?php
include "alert_msg.php";
?>


<script>
window.onload = function () { 

	$('.bu_req_otp').click(function() {
		var divcodekey = $(this).data('divshow');
		$.ajax({
		  url: SITEURL + "/modules/tradersystem/ajax_user.php" ,
		  type: 'POST',
		  data:{doRequestOtp : 1},
		  dataType :'json'
		}).done(function(obj) {
		   var codekey = obj.key_code;
		   //<input type="hidden" name="key_verify" id="key_verify">
		   $("."+divcodekey).html("<input type='hidden' name='key_verify' value='"+codekey+"'>"+codekey);
		});
	    
	});
	$('#amount').keyup(function() {
		var usd_val = $(this).val();
		usd_val 	= usd_val*1;
		
		$('#amount_local').val((usd_val*rate_withdraw).toFixed(2));
	});
	$('#amount_local').keyup(function() {
		var local_val = $(this).val();
		local_val 	= local_val*1;
		$('#amount').val((local_val/rate_withdraw).toFixed(2));
	    
	});
	function toFixed(value, precision) {
	    var power = Math.pow(10, precision || 0);
	    return String(Math.round(value * power) / power);
	}
	
}


</script>