<?php
if (isset($_REQUEST['clear'])) {
	$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = null;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
}
if( ($_SESSION['usr_dtl']['user_info']['email_verified'] == 1) 
	|| ($_SESSION['usr_dtl']['user_info']['google_auth_verified'] == 1) 
	|| ($_SESSION['usr_dtl']['user_info']['sms_verified'] == 1)) {

$type_tool_id = $_SESSION['usr_dtl']['user_info']['secure_tool_id'];
?>

<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3><?php echo Lang::$word->_NOW_SECURITY_USE; ?></h3></div>
					<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post">
						<input type="hidden" name="secure_tool_id" value="<?php echo $type_tool_id;?>">
					<div class="panel-body">
						 <?php echo Lang::$word->_NOW_SECURITY_USE; ?>
						<div class="row">
							<div class="col-sm-12">
								<div class="row bs_switch_wrapper">
									<?php if($type_tool_id == 1){?>
									<div class="col-sm-12 col-md-12">
										<h4>E-mail</h4>
										<?php  echo $_SESSION['email'];?>
										
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO2;?> </span>
										
									</div>
									<div class="col-md-10  stephide steptype3" style="display:none;">
											
											<div class="form-group">
												<label for="user_lname" class="col-md-4 control-label security_main_input">Token from E-mail</label>
												<div class="input-group col-md-4">
													<span class="input-group-addon codeotp_offline" id="basic-addon3"><input type="hidden" id="token" name="token"></span>
													<input type="text" class="form-control" name="token" aria-describedby="basic-addon3">
												</div>
												<div class="row  col-md-8">
													<div class="text-center">
														<button data-divshow="codeotp_offline" type="button"  class="prolific button btn btn-alert bu_req_otp_reset" name="bu_req_otp_reset" >
															<i class="fa fa-refresh"></i> Request new</button>
														<button data-url="/modules/tradersystem/ajax_secure.php" type="button" name="dosubmit_secure" class="prolific button btn btn-success">
															<i class="fa fa-save"></i> SAVE</button>
														<button  type="button" name="bu_back" id="bu_back"class="prolific button btn btn-success">
														<i class="fa fa-save"></i>BACK</button>
													</div>
												</div>	
											</div>
										</div>
									<?php }elseif($type_tool_id == 2){?>
									<div class="col-sm-12 col-md-12">
										<h4><?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH;?></h4>
										
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO5;?></span>
										
									</div>
									<div class="col-md-10  stephide steptype3" style="display:none;">
											
											<div class="form-group">
												<label for="user_lname" class="col-md-4 control-label security_main_input">Code</label>
												<div class="input-group col-md-4">
													<input type="text" class="form-control" name="token" aria-describedby="basic-addon3">
												</div>
												<div class="row  col-md-8">
													<div class="text-center">
														
														<button data-url="/modules/tradersystem/ajax_secure.php" type="button" name="dosubmit_secure" class="prolific button btn btn-success">
															<i class="fa fa-save"></i> SAVE</button>
														<button  type="button" name="bu_back" id="bu_back"class="prolific button btn btn-success">
														<i class="fa fa-save"></i>BACK</button>
													</div>
												</div>	
											</div>
										</div>
									<?php }elseif($type_tool_id == 3){?>
									<div class="col-sm-12 col-md-12">
										<h4><?php echo Lang::$word->_MOBILE_PHONE_SMS;?></h4>
										<?php  echo $_SESSION['usr_dtl']['user_info']['mobile_code'].$_SESSION['usr_dtl']['user_info']['mobile'];?>
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
									</div>
										<!-- start new -->
							
										<div class="col-md-2 text-center stephide steptype3" style="display:none;">
											<img src="http://freedomcfd.com/new/assets/img/sms_incoming.png">
										</div>
										<div class="col-md-10  stephide steptype3" style="display:none;">
											
											<div class="form-group">
												<label for="user_lname" class="col-md-4 control-label security_main_input">Token from SMS</label>
												<div class="input-group col-md-4">
													<span class="input-group-addon codeotp_offline" id="basic-addon3"><input type="hidden" id="token" name="token"></span>
													<input type="text" class="form-control" name="token" aria-describedby="basic-addon3">
												</div>
												<div class="row  col-md-8">
													<div class="text-center">
														<button data-divshow="codeotp_offline" type="button"  class="prolific button btn btn-alert bu_req_otp_reset" name="bu_req_otp_reset" >
															<i class="fa fa-refresh"></i> Request new SMS</button>
														<button data-url="/modules/tradersystem/ajax_secure.php" type="button" name="dosubmit_secure" class="prolific button btn btn-success">
															<i class="fa fa-save"></i> SAVE</button>
														<button  type="button" name="bu_back" id="bu_back"class="prolific button btn btn-success">
														<i class="fa fa-save"></i>BACK</button>
													</div>
												</div>	
											</div>
										</div>
									<!-- end new -->
									<?php }?>
								
									<!--
									<div class="form-group stephide steptype3 " style="display:none;">
										<label for="user_lname" class="col-md-2 control-label">Code</label>
										<div class="col-md-10 input-group">
											<span class="input-group-addon codeotp_offline"></span>
											<input class="form-control" id="token" name="token" placeholder"OTP" type="text">
										</div>
									</div>
									-->
									<!-- for mail -->
									<?php if($type_tool_id == 1666){?>
									<div class="form-group stephide steptype3 " style="display:none;">
										<label for="user_lname" class="col-md-2 control-label"></label>
										<div class="col-md-10 ">
											<input type="hidden" name="" value="">
											<button  data-divshow="codeotp_offline" type="button" name="bu_req_otp_reset" class="prolific button btn btn-success bu_req_otp_reset">
             	                            <i class="glyphicon glyphicon-refresh"></i> Request Code Again</button>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="text-center btn_step1">
								<button type="button" name="reset_security" id="reset_security" class="prolific button btn btn-success">
				                    <i class="fa fa-save"></i> <?php echo Lang::$word->_RESET_SECURE;?>
				                </button>
							</div>
							<div class="text-center btn_step2" style="display:none;">
								<input name="command" value="doSecureSetup_reset" type="hidden">
								<button data-url="/modules/tradersystem/ajax_secure.php" type="button" name="dosubmit_secure" class="prolific button btn btn-success">
									<i class="fa fa-save"></i> SAVE</button>
								<button  type="button" name="bu_back" id="bu_back"class="prolific button btn btn-success">
								<i class="fa fa-save"></i>BACK</button>
								
							</div>
						</div>
					</div>
				    </form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php

	} else {
		include "security_empty.php";
?>

<?php } ?>
<?php
include "alert_msg.php";
$reloadurl = str_replace("url=","",$_SERVER['QUERY_STRING']);
$reloadurl = str_replace("&amp;","?",$reloadurl);
?>
<script>
var thissiteurl = "<?php echo $reloadurl;?>";
window.onload = function () {
    $('#reset_security').click(function(){
    	$('.steptype3').show();
    	$('.btn_step1').hide();
    	if ($('.bu_req_otp_reset').get(0)) {
    		$('.bu_req_otp_reset').trigger('click');
    	}
    	
    });
    $('#bu_back').click(function(){
    	$('.steptype3,.btn_step2').hide();
    	$('.btn_step1').show();
    });
    $('.bu_req_otp_reset').click(function() {
		var divcodekey = $(this).data('divshow');
		
		$.ajax({
		  url: SITEURL + "/modules/tradersystem/ajax_user.php" ,
		  type: 'POST',
		  data:{doRequestotp_reset : 1},
		  dataType :'json'
		}).done(function(obj) {
		   var codekey = obj.key_code;
		   //<input type="hidden" name="key_verify" id="key_verify">
		   $("."+divcodekey).html("<input type='hidden' name='key_verify' value='"+codekey+"'>"+codekey);
		});
	    
	});
	$('input[name="secure_type"]').click(function(){
		$('.stephide').hide();
		var obj = $(this).val();
		/***** email ****/
		if (obj == '1') {
			$('.btnmain').show();
			$('.bu_btnsave').show();
		} else if (obj == '2') {
			$('.btnmain').show();
			/***** google ****/
			$('#msg-loading').modal("show");
			$.ajax({
			  url: SITEURL + "/modules/tradersystem/ajax_secure.php" ,
			  type: 'POST',
			  data:{command:'doRequestotp_google'},
			  dataType :'json'
			}).done(function(obj) {
			   $('#msg-loading').modal("hide");
			   $('.steptype2').show();
			   if (obj.status == 'success') {
			   	$('#auth_token').val(obj.auth_token);
			   	$('#scangoogle').attr('src',obj.imgsrc);
			   	
			   }
			});
			
			$('.bu_btnsave').show();
		} else if (obj == '3') {
			/***** sms ****/
			$('.btnmain').hide();
			$('.steptype3').show();
			if ($('#mobile').val() != '' && $('#token').val() == '') {
				$('.bu_req_otp').trigger('click');
			} else {
				$('.stepopt').hide();
			}
			$('.bu_btnsave').show();
		}
	});
	$('#mobile').blur(function(){
		if ($('#mobile').val() == ''){
			$('.stepopt').hide();
		} else {
			$('.stepopt').show();
		}
	});
	$('.bu_req_otp').click(function() {
		$('.modal').modal('hide');
		$('#msg-loading').modal("show");
		var divcodekey = $(this).data('divshow');
		var txtmobile = $('#mobile').val();
		var txtmobilecode = $('#mobile_code').val();
		
		$.ajax({
		  url: SITEURL + "/modules/tradersystem/ajax_user.php?<?php echo time();?>" ,
		  type: 'POST',
		  data:{doRequestotp_SMS : 1,activity:'setting_secure',phone:txtmobile,secure_tool_id:3,phonecode:txtmobilecode,send_to:txtmobile,vvaa:222222},
		  dataType :'json'
		}).done(function(obj) {
			if (obj.status == 'error') {
				$("#msgholder").html(obj.message);
				$('#msg-loading').modal("hide");
				$('#msg-showwarningmsg').modal("show");
			} else {
				$('#msg-loading').modal("hide");
		   var codekey = obj.key_code;
		   //<input type="hidden" name="key_verify" id="key_verify">
		   $("."+divcodekey).html("<input type='hidden' name='key_verify' value='"+codekey+"'>"+codekey);
		   $('.bu_req_otp').html('<i class="glyphicon glyphicon-refresh"></i> Resend Code Again');
			}
			
		   
		});
	    
	});
	
	 /* == Master Form == */
      $('body').on('click', 'button[name=dosubmit_secure]', function () {
		  posturl = $(this).data('url');
		  var divshowmsg 	= $(this).data('divmsg') || 'msgholder';
		  var frmid 		= $(this).data('frm') || 'prolific_form';
		  function showResponse(json) {
		  	 
		  	  /*$('#showtitle').html(json.status);*/
			  if (json.status == "success") {
			  	  $('#msg-loading').modal("hide");
			  	  var chkfrm_step = 0;
			  	  if  (json.dtl) {
			  	  	 if (json.dtl.step) {
			  	  	 	if (json.dtl.step == "step2") {
			  	  	 		chkfrm_step = 1;
			  	  	 	}
			  	  	 }
			  	  }
			  	  if(chkfrm_step == 1) {
			  	  	 $('#list_pkid').val(json.id);
			  	  	 $('#frm_step2').modal("show");
			  	  	 $('#transfer_detail').html(json.message_dtl);
			  	  	 $('.bu_req_otp').trigger('click');
			  	  } else {
					  $("#"+divshowmsg).html(json.message);
				  	 
				  	  $('#msg-showwarningmsg').modal("show");
				  	  if (json.isfrmreset) {
				  	  	  $('.reqpwd_step1').hide();
				  	  	  $('.reqpwd_step2').show();
				  	  }
			  	  }
			  	  if (json.gotopage) {
			  	  		$('body').fadeOut(1000, function () {
							window.location.href = SITEURL + "/" + json.gotopage;
						});
					
			  	  }
			  	  if (json.isRedirect) {
			  	  		$('body').fadeOut(1000, function () {
							window.location.href = SITEURL + "/" +thissiteurl;
						});
			  	  	/*location.reload(); */
			  	  }
				  
			  } else {
				 
				  $("#"+divshowmsg).html(json.message);
				  $('#msg-loading').modal("hide");
				  $('#msg-showwarningmsg').modal("show");
			  }
		  }

          function showLoader() {
          	  $('.modal').modal('hide');
          	  $('#msg-loading').modal("show");
              
          }
          var options = {
              target: "#msgholder",
              beforeSubmit: showLoader,
              success: showResponse,
              type: "post",
              url: SITEURL + posturl,
              dataType: 'json'
          };

          $('#'+frmid).ajaxForm(options).submit();
      });
	
	
}
</script>

