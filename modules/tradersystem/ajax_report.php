<?php
date_default_timezone_set('UTC'); 

$usr_login		= $_SESSION['usr_dtl']['trade_acc_list'];
$alistAcc_login = array();
$aShowType 		= array(
					'OP_BUY'=>'buy',
					'OP_SELL'=>'sell',
					'OP_BUY_LIMIT'=>'buy limit',
					'OP_SELL_LIMIT'=>'sell limit',
					'OP_BUY_STOP'=>'buy stop',
					'OP_SELL_STOP'=>'sell stop',
					'OP_BALANCE'=>'balance'
					);

foreach($usr_login as $kacc => $vacc ){
	array_push($alistAcc_login, $vacc['trader_acc_login']);
}

$listselctlogin = join(",", $alistAcc_login);
$sql 	= "
	SELECT 
		FROM_UNIXTIME( c.close_time ) as viewclosetime, FROM_UNIXTIME( c.open_time ) as viewopentime, c.* 
	FROM 
		`bk_closed_ordermt4` c
	WHERE
		c.login in (" . $listselctlogin . ")    
	ORDER BY 
		c.order_id DESC 
	LIMIT 0,20
	";
$data 			= Registry::get("Database")->fetch_all($sql);
$alistReport  	= ($data) ? $data : 0;

$sqlopen 	= "
	SELECT 
		FROM_UNIXTIME( c.close_time ) as viewclosetime, FROM_UNIXTIME( c.open_time ) as viewopentime, c.* 
	FROM 
		`bk_closed_ordermt4` c
	WHERE
		c.login in (" . $listselctlogin . ")  
		AND c.cmd != 'OP_BALANCE'
		AND c.close_time =0
	ORDER BY 
		c.order_id DESC 
	LIMIT 0,20
	";
$dataopen 			= Registry::get("Database")->fetch_all($sqlopen);
$alistOpen  	= ($dataopen) ? $dataopen : 0;

$sqlworking 	= "
	SELECT 
		FROM_UNIXTIME( c.close_time ) as viewclosetime, FROM_UNIXTIME( c.open_time ) as viewopentime, c.* 
	FROM 
		`bk_closed_ordermt4` c
	WHERE
		c.login in (" . $listselctlogin . ") 
		AND c.cmd in ('OP_BUY_LIMIT','OP_SELL_LIMIT')
		AND c.close_time = 0
	ORDER BY 
		c.order_id DESC
	LIMIT 0,20
	";
$dataworking 			= Registry::get("Database")->fetch_all($sqlworking);
$alistWorking 	= ($dataworking) ? $dataworking : 0;

?>

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Close Transaction</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Ticket</th>
									<th>Account</th>
									<th>Open Time</th>
									<th>Type</th>
									<th>Volume</th>
									<th>Item</th>
									<th>Price</th>
									<th>Close Time</th>
									<th>Price</th>
									<th>Profit</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($alistReport){
									
									foreach ($alistReport as $kreport => $vreport) {
										?>
										<tr>
											<td><?php echo $vreport->order_id;?></td>
											<td><?php echo $vreport->login;?></td>
											<td><?php echo substr($vreport->viewopentime,0,-7);?></td>
											<td><?php echo $aShowType[$vreport->cmd];?></td>
											<td><?php echo $vreport->volume;?></td>
											<td><?php echo $vreport->symbol;?></td>
											<td><?php echo $vreport->open_price;?></td>
											<td><?php echo substr($vreport->viewclosetime,0,-7);?></td>
											<td><?php echo $vreport->close_price;?></td>
											<td align="right"><?php echo (($vreport->profit > 0)? "<font color='#64b92a'>":"<font color='#c0392b'>") .number_format($vreport->profit,2)."</font>";?></td>
										</tr>
										<?php
									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		