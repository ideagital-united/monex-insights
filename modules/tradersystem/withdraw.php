<?php 
$wallet_net 	= (isset($_SESSION['usr_dtl']['wallet_amt']['amount'])) ? number_format($_SESSION['usr_dtl']['wallet_amt']['amount'],2): 0.00;

$aGateway 		= $tradersystem->call_curl_get('gateway?cmd=gateway', "GET");
/*echo "<pre>";print_r($aGateway);*/
$aGateway		= (isset($aGateway['isSuccess']) && $aGateway['isSuccess'] == 1 && count($aGateway['message']) > 0) ? $aGateway['message'] : false;



$aBank 			= $tradersystem->call_curl_get("bank?bank","GET",'');
$aBank			= (isset($aBank['isSuccess']) && $aBank['isSuccess'] == 1 && count($aBank['message']) > 0) ? $aBank['message'] : false;


$usr_dtl 		= $_SESSION['usr_dtl']['user_info'];
$_SESSION['is_otp'] = 1;
$isUsedOtp 		= $_SESSION['is_otp'];

$alistHistory   = $tradersystem->list_history("withdraw",false);

//=== START RATE EX 
$sqlrate = "SELECT * FROM bk_exchn_rate_new where status=1";
$aRate = $db->fetch_all($sqlrate);
if (!$aRate) {
	$aRate = false;
}
//=== END RATE EX

?>
<script>
var rate_withdraw = 1;

</script>

<style>
 .displaypay 
 {
  width: 151px;
  height: 50px;
  background:white;
  -webkit-background-size: contain;
    -moz-background-size: contain;
    -o-background-size: contain;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center center;
 }
 </style>


<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>
						<?php echo Lang::$word->_MAKE_A_WITHDRAW;?>
						<span class="badge badge-success big"><?php echo Lang::$word->_AVALIABLE_FUNDS_IN_WALLET;?> USD$ <?php echo $wallet_net;?></span>
						</h3>
					</div>
					<div class="panel-body">
						<?php
						if ( $aGateway === false) {
							echo "NO DATA";
						} else {
							$content = '';
							foreach($aGateway as $k => $v)
							{
								$form_url = BASEPATH . "modules/tradersystem/gateways/" . $v['dir'] . "/form_withdraw.tpl.php";
								/*echo $form_url;*/
								if (file_exists($form_url)){
									ob_start();
									include ($form_url);
									$content .= ob_get_contents();
									ob_end_clean();
								}
							}
							echo $content;
						}
						
						?>
				
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3><?php echo Lang::$word->_WITHDRAW_HISTORY;?></h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th><?php echo Lang::$word->_TRANSECTION_NUMBER;?></th>
									<th><?php echo Lang::$word->_LG_WHEN;?></th>
									<th><?php echo Lang::$word->_LG_TYPE;?></th>
									<th><?php echo Lang::$word->_TR_AMOUNT;?></th>
									<th><?php echo Lang::$word->_SYS_DBSTATUS;?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($alistHistory) {
									$aStatus = array(
									'1' => 'success',
									'2' => 'danger',
									'3' => 'danger',
									'0' => 'warning'
									);
									$textStatus = array(
									'0' => 'Processing',
									'1' => 'Success',
									'2' => 'Canceled',
									'3' => 'Not Verify'
									);
									foreach($alistHistory as $k => $v){
										$v->money = -$v->money;
									?>
										<tr>
										<td><?php echo $v->trans_id;?></td>
										<td><?php echo $v->save_date;?></td>
										<td><?php echo $v->gateway_dtl;?></td>
										<td>$<?php echo number_format($v->money,2);?></td>
										
										<td class="<?php echo $aStatus[$v->status]?>"><?php echo $textStatus[$v->status];?></td>
								</tr>
									<?php }
								}
								
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php
include "alert_msg.php";
?>


<script>
window.onload = function () { 
	$('#bank_id').change(function(){
		var setval = $(this).val();
		setval = setval*1;
		var current = obj_abount_bank[setval];
		$('#bank_set_name').html(current.bank_name);
		$('#bank_set_country').html(current.bank_country);
		$('#bank_set_country').html(current.bank_country);
		$('#bank_set_account_type').html(current.bank_account_type);
		$('#bank_set_account_number').html(current.bank_account_number);
		$('#bank_set_swift').html(current.bank_swift);
		$('#bank_set_routing').html(current.bank_routing);
		$('#bank_set_iban').html(current.bank_iban);
		if (current.bank_currency_local) {
			$('#currency_name').html(obj_currency[current.bank_currency_local].currency);
			rate_withdraw = obj_currency[current.bank_currency_local].withdraw *1;
			$('.set_currency_local').show();
		} else {
			$('.set_currency_local').hide();
		}
	});
	
	
	$('.bu_req_otp').click(function() {
		var divcodekey = $(this).data('divshow');
		$.ajax({
		  url: SITEURL + "/modules/tradersystem/ajax_user.php" ,
		  type: 'POST',
		  data:{doRequestOtp : 1},
		  dataType :'json'
		}).done(function(obj) {
		   if (obj.status != 'success') {
		   		$('#frm_step2').modal("hide");
		   		$("#msgholder").html(obj.message);
		   		$('#msg-showwarningmsg').modal("show");
		   } else {
			   var codekey = obj.key_code;
			   //<input type="hidden" name="key_verify" id="key_verify">
			   $("."+divcodekey).html("<input type='hidden' name='key_verify' value='"+codekey+"'>"+codekey);
		   }
		});
	    
	});
	$('#amount').keyup(function() {
		var usd_val = $(this).val();
		usd_val 	= usd_val*1;
		
		$('#amount_local').val((usd_val*rate_withdraw).toFixed(2));
	});
	$('#amount_local').keyup(function() {
		var local_val = $(this).val();
		local_val 	= local_val*1;
		$('#amount').val((local_val/rate_withdraw).toFixed(2));
	    
	});
	function toFixed(value, precision) {
	    var power = Math.pow(10, precision || 0);
	    return String(Math.round(value * power) / power);
	}
	
}


</script>