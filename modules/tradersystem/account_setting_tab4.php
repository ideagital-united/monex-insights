
	<div class="form-group">
		<div class="col-md-12">
			<div class="heading_b"><?php echo Lang::$word->_RESET_PASSWORD;?></div>
			<span class="help-block"><?php echo Lang::$word->_RESETPWD_INFO2;?></span>
		</div>
	</div>
	<div class="form-group reqpwd_step1">
		<div class="col-md-12">
			&nbsp;
		
		</div>
	</div>
	<div class="text-center reqpwd_step1">
		<form class="form-horizontal" id="prolific_form4" name="prolific_form4" method="post">
	    <input name="doProfile_change_pwd" type="hidden" value="1">
        <button data-url="/ajax/controller.php" data-frm="prolific_form4"  type="button" name="dosubmit" class="prolific button btn btn-success">
            <i class="fa fa-save"></i><?php echo Lang::$word->_REQUEST_PASSWORD;?></button>
        </form>
		
	</div>
	<form class="form-horizontal" id="prolific_form5" name="prolific_form5" method="post">
	<div class="form-group ste reqpwd_step2" style="display:none;">
		<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_UA_TOKEN;?></label>
		<div class="col-md-10">
			<input type="text" id="activate_token" name="activate_token" class="form-control" value="">
		</div>
	</div>
	<div class="form-group reqpwd_step2" style="display:none;">
		<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_NEW_PASSWORD;?></label>
		<div class="col-md-10">
			<input type="password" name="new_password"id="new_password" class="form-control" value="">
		</div>
	</div>
	<div class="form-group reqpwd_step2" style="display:none;">
		<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_NEW_PASSWORD;?>2</label>
		<div class="col-md-10">
			<input type="password" name="new_password2"id="new_password2" class="form-control" value="">
		</div>
	</div>
    <div class="text-center reqpwd_step2" style="display:none;">
	    <input name="doProfile_change_save_password" type="hidden" value="1">
        <button data-url="/ajax/controller.php" data-frm="prolific_form5"  type="button" name="dosubmit" class="prolific button btn btn-success">
            <i class="fa fa-save"></i><?php echo Lang::$word->_RESET_PASSWORD;?></button>
		
	</div>
	</form>
	
	
	
