<?php
  /**
   * Account Template
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: account.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
   
 if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
 if (!$user->logged_in) {
      redirect_to(doUrl(false, $core->login, "page"));
 }
 
$usr = getValues('*','users',' email = "'.$_SESSION['email'].'"');
/*
echo "<pre>";
print_r($usr);
*/
?>

<div class="page_content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"><?php echo Lang::$word->_Account_Settng;?></div>
					<div class="panel-body">
						<div class="user_profile">
							
							<ul class="nav nav-tabs" id="tabs_a">
								<li ><a data-toggle="tab" href="#intro"><?php echo Lang::$word->_MANAGE_PROFILE;?></a></li>
								<li class="active"><a data-toggle="tab" href="#product">Choose Your Product</a></li>
								<li><a data-toggle="tab" href="#acknowledge">Acknowledgements</a></li>
							</ul>
							<div class="tab-content" id="tabs_content_a">
								<div id="intro" class="tab-pane fade">
									<?php
									include "main_intro.php";
									?>
								</div>
								<div id="product" class="tab-pane fade in active">
									<?php
									include "main_product.php";
									?>
								</div>
								<div id="acknowledge" class="tab-pane fade">
									<?php
									include "main_acknow.php";
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php
include "alert_msg.php";
?>

