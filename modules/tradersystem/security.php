<?php
define("_VALID_PHP", true);
require_once("init.php");
$api_key = $_SESSION['securekey'];
$secure_tool_id = $_SESSION['usr_dtl']['user_info']['secure_tool_id'];
$sms_verified = $_SESSION['usr_dtl']['user_info']['sms_verified'];
$email_verified = $_SESSION['usr_dtl']['user_info']['email_verified'];
$google_auth_verified = $_SESSION['usr_dtl']['user_info']['google_auth_verified'];

$secure_setup_form = BASEPATH . 'modules/tradersystem/secure/secure_setup_form.php';
$secure_reset_form = BASEPATH . 'modules/tradersystem/secure/secure_reset_form.php';
$secure_detail_form = BASEPATH . 'modules/tradersystem/secure/secure_detail_form.php';
$verify_secure_form_email = BASEPATH . 'modules/tradersystem/secure/verify_secure_form_email.php';
$verify_secure_form_google = BASEPATH . 'modules/tradersystem/secure/verify_secure_form_google.php';
$verify_secure_form_sms = BASEPATH . 'modules/tradersystem/secure/verify_secure_form_sms.php';
//echo $_SERVER['HTTP_HOST'];
//print_r($_POST);
/* echo "<pre>";
print_r($_SESSION);

echo "</pre>"; */
if($_POST['secure_setup_form'] == 1){
	$tool_id = $_POST['radio_switch1'];
	$data = array('tool_id'=>$tool_id);
	$result = $usernc->set_secure_tool($data,$api_key);
	//print_r($result);
	if($result['isSuccess'] == 1){
		$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = $tool_id;
		$secure_tool_id = $tool_id;
		//print_r($result); //make email
		$method = $result['method'];
		switch ($method){
			case 'Email':
				$email_to = $result['to'];
				$sent_token = $result['message'];
				//start sent mail
require_once (BASEPATH . "lib/class_mailer.php");
$token = $sent_token;
$row = Core::getRowById(Content::eTable, 21);
$body = str_replace(array('[USERNAME]','[TOKEN]','[URL]','[SITE_NAME]'),array($email_to,$token,SITEURL,Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
$newbody = cleanOut($body);
$mailer = Mailer::sendMail();
$message = Swift_Message::newInstance()
->setSubject($row->{'subject' . Lang::$lang})
->setTo(array($email_to => $email_to))
->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
->setBody($newbody, 'text/html');
$mailer->send($message);
				// end send mail
				break;
			case 'Google Authenticator':
				$secret = $result['message'];
				$_SESSION['usr_dtl']['user_info']['google_auth_key'] = $secret;
				break;
			case 'SMS':
				$sms_to = $result['to'];
				$sms_token = $result['message'];
				echo "Dummy Token : ".$sms_token;
				break;
		}
	} else {
		//print_r($result);
 		$msg = "Please check if the mobile number has been set up before use this option.";
		echo $request->msgError_gob($msg);
		//echo $request->msgError_gob(Lang::$word->_SYSERROR);
	}
} elseif ($_POST['verify_secure_form'] == 1) {
	
	$token = $_POST['token'];
	$data = array('token'=>$token);
	$result = $usernc->verify_secure_tool($data,$api_key);
	if($result['isSuccess'] == 1){
		$msg = $result['message'];
		switch ($msg){
			case 'Verified by email.':
				$sms_verified = null;
				$email_verified = 1;
				$google_auth_verified = null;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = 1;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
				break;
			case 'Verified by Google2FA.':
				$sms_verified = null;
				$email_verified = null;
				$google_auth_verified = 1;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = 1; 
				break;
			case 'Verified by SMS.':
				$sms_verified = 1;
				$email_verified = null;
				$google_auth_verified = null;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = 1;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
				break;				
		}
	} else {
		$msg = $result['errMessage'];
		echo $request->msgError_gob($msg);
	}
} elseif ($_POST['request_new_email_token'] == 1){
	//do sent token
	echo "dummy text : resend token again";
} elseif ($_POST['secure_reset_form'] == 1){
	$status = 4;
} elseif ($_POST['reset_token_req'] == 1){
	$status = 4;
	$data = array('request_activity'=>'reset_secure');
	$reqopt_result = $usernc->reqotp($data,$api_key);
} elseif ($_POST['resetseure_send'] == 1){
//validate token
	$otp_token = $_POST['reset_token'];
	$data = array('key_code'=>$otp_token);
	$reset_result = $usernc->resetsecure($data,$api_key);
	if($reset_result['isSuccess'] == 1){
		$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = null;
		$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
		$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
		$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
		$sms_verified = null;
		$email_verified = null;
		$google_auth_verified = null;
		$status = 2;
		?>
		<script langquage='javascript'>window.location.href = "<?php //echo $_SERVER['HTTP_HOST']; ?>";</script>
		<?php
	} else {
 		$msg = "Invalid OTP or Request expired";
		echo $request->msgError_gob($msg);
	}
	
}

if($status == 4){
	$status = 4;
} else {
	if($sms_verified == 1||$email_verified == 1||$google_auth_verified == 1){
		$status = 1; //all set, do nothing
	} else {
		if($secure_tool_id == 1||$secure_tool_id == 2||$secure_tool_id == 3){
			$status = 3; //resume.
		} else {
			$status = 2; //nonset do all from start
		}
	}
}


//echo "status = ".$status;
/* echo "<pre>";
print_r($_SESSION);
echo "</pre>"; */
?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					
<?php
switch ($status) {
    case 1:
		include($secure_detail_form);
        break;
    case 2:
        include($secure_setup_form);
        break;
    case 3:
		switch ($secure_tool_id){
			case 1:
				include($verify_secure_form_email);
				break;
			case 2:
				include($verify_secure_form_google);
				break;
			case 3:
				include($verify_secure_form_sms);
				break;
		}
        break;
	case 4:
		include($secure_reset_form);
		break;
}

if($status != 4 && $status != 2){
	?>
	<div class="panel-heading"><h3><?php echo Lang::$word->_RESET_SECURE;?></h3></div>
	<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post" target=''>
		<div class="panel-body">
			<div class="row">
				<div class="text-center">
					<input name="secure_reset_form" type="hidden" value="1">
					<input type="submit" value="<?php echo Lang::$word->_REQ_RESET_SECURE;?>" class="prolific button btn btn-success" class="fa fa-save">
				</div>
			</div>
		</div>
	</form>
	<?php
} else {
}
?>



					
				</div>
			</div>
		</div>
	</div>
</div>
