<?php
$prolific_fix_google = true;

?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3></div>
					<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post">
					<div class="panel-body">
						<?php echo Lang::$word->_SECURITY_INFO;?>
						<div class="row">
							<div class="col-sm-12">
								<div class="row bs_switch_wrapper">
									<div class="col-sm-12 col-md-12">
										<label class="radio-inline">
											<input type="radio" name="secure_type" value="1" id="option_mail" ><?php echo Lang::$word->_EMAIL;?>
											<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO_EMAIL;?></span>
										</label>
									</div>
							<?php if (isset($_SESSION['adminconf']['sms_infobip_sender']) && $_SESSION['adminconf']['sms_infobip_sender'] != '') {?>
									<div class="col-sm-12 col-md-12">
										
										<label class="radio-inline">
											<input type="radio" name="secure_type" value="3" id="option_sms" ><?php echo Lang::$word->_MOBILE_PHONE_SMS;?>
											<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO_SMS;?></span>
										</label>
										<!-- start new -->
										<div class="form-group stephide steptype3" style="display:none;">
											<div class="col-md-12">
												<div class="heading_b security_main_input">Please insert your mobile phone number</div>
											</div>
										</div>
										<div class="col-md-2 text-center stephide steptype3" style="display:none;">
											<img src="http://freedomcfd.com/new/assets/img/sms_incoming.png">
										</div>
										<div class="col-md-10  stephide steptype3" style="display:none;">
											<div class="form-group">
												<label for="user_lname" class="col-md-4 control-label security_main_input">Your mobile phone number</label>
												<div class="col-md-1">
													<input type="text" value="<?php echo $_SESSION['codephonebyip'];?>" class="form-control " name="mobile_code" id="mobile_code">
												</div>
												<div class="col-md-3">
													<input type="text" value="" class="form-control" placeholder="XXXXXXXXX" name="mobile" id="mobile">
												</div>

											</div>
											<div class="form-group">
												<label for="user_lname" class="col-md-4 control-label security_main_input">Token from SMS</label>
												<div class="input-group col-md-4">
													<span class="input-group-addon codeotp_offline" id="basic-addon3"><input type="hidden" id="token" name="token">318546</span>
													<input type="text" class="form-control" name="token" aria-describedby="basic-addon3">
												</div>
												<div class="row  col-md-8">
													<div class="text-center">
														<button data-divshow="codeotp_offline" type="button"  class="prolific button btn btn-alert bu_req_otp" name="bu_req_otp" ><i class="fa fa-refresh"></i> Request new SMS</button>
														<button data-url="/modules/tradersystem/ajax_secure.php" type="button" name="dosubmit_secure" class="prolific button btn btn-success">
														<i class="fa fa-save"></i><?php echo Lang::$word->_SAVE_SETTING;?></button>
													</div>
												</div>	
											</div>
										</div>
										<!--end new-->
										    
											
										
									</div>
									<?php } ?>
									<?php 
									if($prolific_fix_google==true){
									?>
									<div class="col-sm-12 col-md-12">
										<label class="radio-inline">
											<input type="radio" name="secure_type" value="2" id="option_google" ><?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH;?>
											<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO_GOOGLE_AUTH;?></span>
										</label>
										
										<div class="form-group stephide steptype2" style="display:none;">
										<div class="col-md-12">
										<img id="scangoogle" src="" />
										<span class="help-block"><?php echo Lang::$word->_GA_SETUP;?></span>
										<a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=th" target="_blank"><img src="<?php echo UPLOADURL?>Google-Play-Badge.png" width="150" /></a>
							
										<a href="https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="<?php echo UPLOADURL?>App-Store-Badge.png" width="150" /></a>
										</div>
										</div>
										<div class="form-group stephide steptype2" style="display:none;">
										<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_GA_TOKEN;?></label>
										<div class="col-md-5">
										<input type="hidden" name="auth_token" id="auth_token" value="" class="form-control">
										<input type="text" name="token_google" id="token_google" class="form-control">
										</div>
										</div>
										
									</div>
									<?php
									}
									?>
								</div>
							</div>
						</div>
						<div class="row stephide bu_btnsave">
							<div class="text-center">
								<input name="command" value="doSecureSetup_part1" type="hidden">
								<button data-url="/modules/tradersystem/ajax_secure.php" type="button" name="dosubmit_secure" class="prolific button btn btn-success btnmain">
									<i class="fa fa-save"></i><?php echo Lang::$word->_SAVE_SETTING;?></button>
							</div>
						</div>
					</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
</div>
</div>