<?php 
$wallet_net 	= (isset($_SESSION['usr_dtl']['wallet_amt']['amount'])) ? number_format($_SESSION['usr_dtl']['wallet_amt']['amount'],2): 0.00;


$alistHistory   = $tradersystem->list_history("statement",false);
?>
<script>
var rate_withdraw = 30;
</script>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>My statement</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>-</th>
									<th>Money in</th>
									<th>Money out</th>
									<th>Money net</th>
									<th>Type</th>
									<th>Date</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($alistHistory) {
									$aStatus = array(
									'1' => 'success',
									'2' => 'danger',
									'0' => 'warning',
									'3' => 'danger',
									'4' => 'warning',
									'10' => 'warning',
									'11' => 'warning'
									);
									$textStatus = array(
									'0' => 'Processing',
									'1' => 'Success',
									'2' => 'Canceled',
									'3' => 'Not Verify',
									'4' => 'Processing',
									'10' => 'Processing',
									'11' => 'Processing',
									
									);
									$i=0;
									foreach($alistHistory as $k => $v){
										$i++;
										if ($v->money > 0) {
											$money_in = "$".number_format($v->money,2);
											$money_out = "-";
										} else {
											$v->money = -$v->money;
											$money_in = "-";
											$money_out = "$".number_format($v->money,2);
										}
										if ($v->trans_type == 'wallet') {
											$type = $v->gateway_dtl;
											
											if ($type == 'Commission') {
												$comments = explode('{', $v->comment);
												$type = $type. " (".$comments[0].")";
											}
										} else {
											$type = $v->deposit_comment;
										}
										
									?>
										<tr>
										<td><?php echo $i;?></td>
										<td><?php echo $money_in;?></td>
										<td><?php echo $money_out;?></td>
										<td>$<?php echo number_format($v->money_total,2);?></td>
										<td><?php echo $type;?></td>
										<td><?php echo $v->save_date;?></td>
										<td class="<?php echo $aStatus[$v->status]?>"><?php echo $textStatus[$v->status];?></td>
								</tr>
									<?php }
								}
								
								?>
								<!--
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
								-->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
