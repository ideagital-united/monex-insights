<form class="form-horizontal" id="prolific_form2" name="prolific_form" method="post">
	<div class="form-group">
		<div class="col-md-12">
			<div class="heading_b">Upload Documents</div>
			
             <span class="help-block">Please submit two of the following documents in order to successfully complete your application.</span>
              <span class="help-block">You photo ID must be valid and clearly visible, and your proof of address must not be older than X mounths.</span>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-5 alert alert-info">
			<dt>1. Government-issued Photo ID <i class="fa fa-user"></i> </dt>
			<li> Passport</li>
			<li> Driver's License</li>
			<li> Government Issued ID Card</li>
		</div>
		<div class="col-md-2">
			<h2>+</h2>
		</div>
		<div class="col-md-5 alert alert-info">
			<dt>2. Proof of Address</dt>
			<li> Utilities bill</li>
			<li> Credit card statement</li>
			<li> Bank Statement</li>
		</div>
	</div>
	
	<div class="form-group">
		<label for="id_card_data" class="col-md-2 control-label">1.Government-issued Photo ID</label>
	</div>
		<div class="fields col-md-12 ">
			<div class="field  col-md-4">
			<label>Document Type</label>
			<select name ="doc1_type[]" class="form-control">
			<option value="">Please Select</option>
			<option value="passport">Passport</option>
			<option value="driverlnc">Driver License</option>
			<option value="idcard">National ID card</option>
		    </select>
			</div>
			
			<div class="field col-md-4">
			<label>ID issue Country</label>
			<select  name="doc1_country[]" class="form-control">
				<?php
				echo $tradersystem->list_select_option_by_table("bk_country_code",$usr->country ,"country_abb","country","",Lang::$word->_PLEASE_SELECT_COUNTRY_CODE);
				?>
				
			</select>
			</div>
			
			<div class="field col-md-4">
			<label>ID Number</label>
			<input type="text" name="doc1_id[]" value="" placeholder="ID Number">
			</div>
		</div>
		<div class="fields">
			<div class="field">
			<label>Upload</label>
		    <input type="file" name="doc1_img[]" class="form-control">
			</div>
			
		</div>

	
	<div class="form-group adddoc1">
		<label for="id_card_data" class="col-md-2 control-label">
			<span id="add_file_doc1">+ Add another file</span>
		</label>
	</div>
	<div class="form-group">
		<label for="id_card_data" class="col-md-2 control-label"><?php echo Lang::$word->_DATE_OF_BIRTH;?></label>
		<div class="col-md-10">
			<!--
			<div class="input-group date ts_datepicker" data-date-format="dd-mm-yyyy" >
				<input class="form-control" name="date_of_brith" type="text" value="<?php echo $usr_dtl['date_of_brith'];?>">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
			-->
			<div class="input-group date " data-date-format="dd-mm-yyyy" >
				<input class="form-control" name="date_of_brith" placeholder="dd-mm-yyyy" type="text" value="<?php echo $usr_dtl['date_of_brith'];?>">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div>
	</div>
	
	<!--
		<span><p class="badge badge-success"><i class="glyphicon glyphicon-ok"></i> Verified</p>  <a href="http://demov2.brokers.solutions/uploads/file_doc/IMG_1434707124192.jpg" target="_blank"><span aria-hidden="true" class="glyphicon glyphicon-camera"></span>  View Image</a></span>
		-->
	<div class="form-group">
		<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_DOCUMENT_PERSONAL_ID;?></label>
			<?php if ($isShowFrm_posonal == false){?>
				<div class="col-md-10 has-warning">
					<?php echo $isShowFrm_posonal_text;?>
				</div>
			<?php } else {?>
				<div class="col-md-10 has-warning">
					<label class="control-label" for="inputWarning"><?php echo Lang::$word->_DOCUMENT_CONTROL_INFO1;?></label>
					<input type="file" name="file_personal" class="form-control">
				</div>
			<?php }?>
	</div>
	<div class="form-group">
		<label for="address_1" class="col-md-2 control-label"><?php echo Lang::$word->_PERSONAL_ADDR;?></label>
		<div class="col-md-10">
			<input type="text" id="address_1" name="address_1" class="form-control" value="<?php echo $usr_dtl['address_1'];?>">
		</div>
	</div>
	<div class="form-group">
		<label for="address_2" class="col-md-2 control-label"><?php echo Lang::$word->_PERSONAL_ADDR;?>2</label>
		<div class="col-md-10">
			<input type="text" id="address_2" name="address_2" class="form-control" value="<?php echo $usr_dtl['address_2'];?>">
		</div>
	</div>
	<div class="form-group">
		<label for="address_city" class="col-md-2 control-label"><?php echo Lang::$word->_CITY_TOWN;?></label>
		<div class="col-md-10">
			<input type="text" id="address_city" name="address_city" class="form-control" value="<?php echo $usr_dtl['address_city'];?>">
		</div>
	</div>
	<div class="form-group">
		<label for="address_postcode" class="col-md-2 control-label"><?php echo Lang::$word->_POSTCODE;?></label>
		<div class="col-md-10">
			<input type="text" id="address_postcode" name="address_postcode" class="form-control" value="<?php echo $usr_dtl['address_postcode'];?>">
		</div>
	</div>
	<div class="form-group">
		<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_COUNTRY;?></label>
		<div class="col-md-10">
			<select id="s2_ext_value" name="country" class="form-control" >
				<?php
				echo $usr->country;
				echo $tradersystem->list_select_option_by_table("bk_country_code",$usr->country ,"country_abb","country","",Lang::$word->_PLEASE_SELECT_COUNTRY_CODE);
				?>
				
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="user_lname2" class="col-md-2 control-label"><?php echo Lang::$word->_PERSONAL_ADDR;?></label>
		<?php if ($isShowFrm_addr == false){?>
			<div class="col-md-10 has-warning">
				<?php echo $isShowFrm_addr_text;?>
			</div>
		<?php } else {?>
			<div class="col-md-10 has-warning">
				<label class="control-label" for="inputWarning"><?php echo Lang::$word->_DOCUMENT_CONTROL_INFO1;?></label>
				<input type="file" name="file_addr" class="form-control">
			</div>
		<?php }?>
	</div>
	
	<div class="text-center">
		<input name="doProfile_document" type="hidden" value="1">
		<button data-url="/ajax/controller.php" data-frm="prolific_form2" type="button" name="dosubmit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_UPDATE_DOCUMENT;?></button>
		
	</div>
</form>
