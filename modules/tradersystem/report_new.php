<?php
date_default_timezone_set('UTC'); 
$is_mt4  	= (issetvalue($_SESSION['adminconf']['mt4_api_url']) != '') 	? 1 : 0;
$is_ct 		= (issetvalue($_SESSION['adminconf']['ct_api_url']) != '') 	? 1 : 0;

$usr_login		= $_SESSION['usr_dtl']['trade_acc_list'];

$aShowType 		= array(
					'OP_BUY'=>'buy',
					'OP_SELL'=>'sell',
					'OP_BUY_LIMIT'=>'buy limit',
					'OP_SELL_LIMIT'=>'sell limit',
					'OP_BUY_STOP'=>'buy stop',
					'OP_SELL_STOP'=>'sell stop',
					'OP_BALANCE'=>'balance'
					);
$view_type 		= isset($_GET['tab']) ? $_GET['tab'] : 'mt4';			
$view_account 	= isset($_POST['frm_by_acc']) ? $_POST['frm_by_acc'] : '';

$frmSelect	= "<select name='frm_by_acc' id='frm_by_acc'>";
$numRowAcc = count($usr_login);
if ($numRowAcc > 0){
	foreach ($usr_login as $kacc => $vacc ) {
		if ($vacc['trader_product_type'] == 1 && $view_type == 'mt4') {
			if( $view_account == '' ) $view_account = $vacc['trader_acc_login'];
			$setv 		= ($view_account == $vacc['trader_acc_login']) ? " selected" : "";
			$frmSelect 	.= "<option value='" . $vacc['trader_acc_login'] . "' " . $setv . ">" . $vacc['trader_acc_login'] . "</option>";
		} elseif ($vacc['trader_product_type'] == 2 && $view_type == 'ct') {
			if( $view_account == '' ) $view_account = $vacc['trader_acc_login'];
			$setv 		= ($view_account == $vacc['trader_acc_login']) ? " selected" : "";
			$frmSelect 	.= "<option value='" . $vacc['trader_acc_login'] . "' " . $setv . ">" . $vacc['trader_acc_login'] . "</option>";
		}
	}
	$frmSelect 		.= "</select>";
	$selecttabmt4 	= $selecttabct = 0;
	
	if ($view_type == 'mt4') {
		$selecttabmt4 	= 1;
	} elseif ($view_type == 'ct') {
		$selecttabct 	= 1;
	}
	$alistReport  	= $tradersystem->report_get_report_mt4('all',$view_account);
	$alistOpen  	= $tradersystem->report_get_report_mt4('open',$view_account);
	$alistWorking 	= $tradersystem->report_get_report_mt4('working',$view_account);
}

?>

<div class="page_content">
	<div class="container-fluid">
		<!-- start tab-->
		<?php if ($numRowAcc == 0){?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger"><center><strong><?php echo Lang::$word->_LG_IMPORTANCE;?>:</strong> <?php echo Lang::$word->_NO_DATA;?></center></div>
			</div>
		</div>
		<?php } else {	?>
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="user_profile">
						<ul class="nav nav-tabs" id="tabs_a">
							<li class="<?php if($selecttabmt4 == 1) echo "active";?>"><a href="index.php?subpage=report_main&tab=mt4">MetaTrade 4</a></li>
							<?php if($is_ct == 1){?>
							<li class="<?php if($selecttabct == 1) echo "active";?>"><a href="index.php?subpage=report_main&tab=ct">cTrader</a></li>
							<?php }?>
						</ul>
						<div class="tab-content" id="tabs_content_a">
							<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="btn-group">
						<form name="frm_search_acc" id="frm_search_acc" method="POST">
								<?php echo $frmSelect;?>
								</form>
					</div>
					<div class="panel-heading">
						<h3><?php echo Lang::$word->_REPORT;?>:<?php echo $view_account;?></h3>	
					</div>
					<div class="panel-body report_left">
						<p><?php echo Lang::$word->_DAILY;?>:<span id="display_dd">0.00%</span></p>
						<p><?php echo Lang::$word->_MONTHLY;?>:<span id="display_mm">0.00%</span></p>
						<p class="heading_b"></p>
						<p><?php echo Lang::$word->_BALANCE;?>:<span id="display_balance">$0.00</span></p>
						<p><?php echo Lang::$word->_EQUITY;?>:<span id="display_eq">$0.00</span></p>
						<p class="heading_b"></p>
						<p><?php echo Lang::$word->_DEPOSITS;?>:<span id="display_deposit">$0.00</span></p>
						<p><?php echo Lang::$word->_WITHDRAWALS;?>:<span id="display_withdraw">$0.00</span></p>
						<hr>
						
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="nvd3_cumulativeLine" class="chart" style="width:100%;height:354px"><svg></svg></div>
						<h6><b><?php echo Lang::$word->_REPORT_INFO1;?></b></h6>
						<form class="form-horizontal" name="frm_search_by_day">
							<div class="form-group">
								<div class="col-sm-4 ">
									<input class="form-control" type="text" placeholder="Start date" id="dpStart" name= "dpStart11"  data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
								<div class="col-sm-4 ">
									<input class="form-control" type="text" placeholder="End date" id="dpEnd" name="dpEnd11"  data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
								<div class="col-sm-4 "><button type="button" class="btn btn-success col-sm-12" id="btn_search_by_date">Display Data From This Range</button></div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Close Transaction</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Ticket</th>
									<th>Account</th>
									<th>Open Time</th>
									<th>Type</th>
									<th>Volume</th>
									<th>Item</th>
									<th>Price</th>
									<th>Close Time</th>
									<th>Price</th>
									<th>Profit</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($alistReport){
									
									foreach ($alistReport as $kreport => $vreport) {
										if ($vreport->cmd == 'OP_BALANCE') continue;
										?>
										<tr>
											<td><?php echo $vreport->order_id;?></td>
											<td><?php echo $vreport->login;?></td>
											<td><?php echo substr($vreport->viewopentime,0,-7);?></td>
											<td><?php echo $aShowType[$vreport->cmd];?></td>
											<td><?php echo $vreport->volume;?></td>
											<td><?php echo $vreport->symbol;?></td>
											<td><?php echo $vreport->open_price;?></td>
											<td><?php echo substr($vreport->viewclosetime,0,-7);?></td>
											<td><?php echo $vreport->close_price;?></td>
											<td align="right"><?php echo (($vreport->profit > 0)? "<font color='#64b92a'>":"<font color='#c0392b'>") .number_format($vreport->profit,2)."</font>";?></td>
										</tr>
										<?php
									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="display:none;">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Open Trades</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools2" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Ticket</th>
									<th>Account</th>
									<th>Open Time</th>
									<th>Type</th>
									<th>Volume</th>
									<th>Item</th>
									<th>Price</th>
									<th>Close Time</th>
									<th>Price</th>
									<th>Profit</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($alistOpen){
									
									foreach ($alistOpen as $kreport => $vreport) {
										?>
										<tr>
											<td><?php echo $vreport->order_id;?></td>
											<td><?php echo $vreport->login;?></td>
											<td><?php echo substr($vreport->viewopentime,0,-7);?></td>
											<td><?php echo $aShowType[$vreport->cmd];?></td>
											<td><?php echo $vreport->volume;?></td>
											<td><?php echo $vreport->symbol;?></td>
											<td><?php echo $vreport->open_price;?></td>
											<td><?php echo substr($vreport->viewclosetime,0,-7);?></td>
											<td><?php echo $vreport->close_price;?></td>
											<td align="right"><?php echo (($vreport->profit > 0)? "<font color='#64b92a'>":"<font color='#c0392b'>") .number_format($vreport->profit,2)."</font>";?></td>
										</tr>
										<?php
									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row"  style="display:none;">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Working Orders</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools2" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Ticket</th>
									<th>Account</th>
									<th>Open Time</th>
									<th>Type</th>
									<th>Volume</th>
									<th>Item</th>
									<th>Price</th>
									<th>Close Time</th>
									<th>Price</th>
									<th>Profit</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($alistWorking){
									
									foreach ($alistWorking as $kreport => $vreport) {
										?>
										<tr>
											<td><?php echo $vreport->order_id;?></td>
											<td><?php echo $vreport->login;?></td>
											<td><?php echo substr($vreport->viewopentime,0,-7);?></td>
											<td><?php echo $aShowType[$vreport->cmd];?></td>
											<td><?php echo $vreport->volume;?></td>
											<td><?php echo $vreport->symbol;?></td>
											<td><?php echo $vreport->open_price;?></td>
											<td><?php echo substr($vreport->viewclosetime,0,-7);?></td>
											<td><?php echo $vreport->close_price;?></td>
											<td align="right"><?php echo (($vreport->profit > 0)? "<font color='#64b92a'>":"<font color='#c0392b'>") .number_format($vreport->profit,2)."</font>";?></td>
										</tr>
										<?php
									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
						</div>
						
						
						
					</div>
				</div>
			</div>
		</div>
		<!-- end tab -->
		<?php } ?>
		
		
	</div>
</div>
</div>

<script>
var view_account = "<?php echo $view_account;?>";
window.onload = function () { 
	function get_report() {
		$.ajax({
			  url: SITEURL + "/modules/tradersystem/ajax_user.php" ,
			  type: 'POST',
			  data:{doReportDetail : 1,acc:view_account},
			  dataType :'json'
			}).done(function(obj) {
				$('#display_dd').html(obj.daily);
				$('#display_mm').html(obj.monly);
				$('#display_balance').html('$'+obj.balnace);
				$('#display_eq').html('$'+obj.equity);
				$('#display_deposit').html('$'+obj.deposit);
				$('#display_withdraw').html('$'+obj.withdraw);
			});
	}
	get_report();
/*
	var chkchange = '';
	$('.inputdate').change(function(){
		if ($(this).val() != chkchange) {
			chkchange = $(this).val();
			cumulativeTestDatadd();
			
		}
	});
	*/
	$('#btn_search_by_date').click(function(){
		cumulativeTestDatadd();
	});
	$('#frm_by_acc').change(function() {
	  this.form.submit();
	});
	function cumulativeTestDatadd() {
					var chart_browsers_data_new = [];
					$.ajax({
					  url: SITEURL + "/modules/tradersystem/ajax_user.php" ,
					  type: 'POST',
					  data:{doBuildReportnew : 1,dtstart:$('#dpStart').val(),dtend:$('#dpEnd').val(),acc:view_account},
					  dataType :'json'
					}).done(function(obj) {
						if ($('#nvd3_cumulativeLine').length) {
				nv.addGraph(function() {
				    var chart = nv.models.linePlusBarChart()
				      .margin({top: 30, right: 60, bottom: 50, left: 70})
				      .x(function(d,i) { return i })
				      .y(function(d) { return d[1] })
				      .color(["#64B92A","#C0392B","#0000FF"])
				      ;

				    chart.xAxis
				      .showMaxMin(false)
				      .tickFormat(function(d) {
				        var dx = cumulativeTestData()[0].values[d] && cumulativeTestData()[0].values[d][0] || 0;
				        return d3.time.format('%x')(new Date(dx))
				      });

				    chart.y1Axis
				      .tickFormat(d3.format(',f'));
 					
				    chart.y2Axis
				      .tickFormat(function(d) { return '$' + d3.format(',f')(d) });

				    chart.bars.forceY([0]);

				    d3.select('#nvd3_cumulativeLine svg')
				      .datum(obj)
				      .transition().duration(500)
				      .call(chart)
				      ;

				    nv.utils.windowResize(chart.update);

				    return chart;
				});
			
				function cumulativeTestData() {
					return obj;
					return [
						{
						"key" : "Deposits",
						"bar": true,
						"values" : [ [ 1136005200000 , 0.0] , [ 1138683600000 , 0.0] , [ 1141102800000 , 0.0] , [ 1143781200000 , 0] , [ 1146369600000 , 0] , [ 1149048000000 , 0] , [ 1151640000000 , 0] , [ 1154318400000 , 0] , [ 1156996800000 , 0] , [ 1159588800000 , 3899486.0] , [ 1162270800000 , 3899486.0] , [ 1164862800000 , 3899486.0] , [ 1167541200000 , 3564700.0] , [ 1170219600000 , 3564700.0] , [ 1172638800000 , 3564700.0] , [ 1175313600000 , 2648493.0] , [ 1177905600000 , 2648493.0] , [ 1180584000000 , 2648493.0] , [ 1183176000000 , 2522993.0] , [ 1185854400000 , 2522993.0] , [ 1188532800000 , 2522993.0] , [ 1191124800000 , 2906501.0] , [ 1193803200000 , 2906501.0] , [ 1196398800000 , 2906501.0] , [ 1199077200000 , 2206761.0] , [ 1201755600000 , 2206761.0] , [ 1204261200000 , 2206761.0] , [ 1206936000000 , 2287726.0] , [ 1209528000000 , 2287726.0] , [ 1212206400000 , 2287726.0] , [ 1214798400000 , 2732646.0] , [ 1217476800000 , 2732646.0] , [ 1220155200000 , 2732646.0] , [ 1222747200000 , 2599196.0] , [ 1225425600000 , 2599196.0] , [ 1228021200000 , 2599196.0] , [ 1230699600000 , 1924387.0] , [ 1233378000000 , 1924387.0] , [ 1235797200000 , 1924387.0] , [ 1238472000000 , 1756311.0] , [ 1241064000000 , 1756311.0] , [ 1243742400000 , 1756311.0] , [ 1246334400000 , 1743470.0] , [ 1249012800000 , 1743470.0] , [ 1251691200000 , 1743470.0] , [ 1254283200000 , 1519010.0] , [ 1256961600000 , 1519010.0] , [ 1259557200000 , 1519010.0] , [ 1262235600000 , 1591444.0] , [ 1264914000000 , 1591444.0] , [ 1267333200000 , 1591444.0] , [ 1270008000000 , 1543784.0] , [ 1272600000000 , 1543784.0] , [ 1275278400000 , 1543784.0] , [ 1277870400000 , 1309915.0] , [ 1280548800000 , 1309915.0] , [ 1283227200000 , 1309915.0] , [ 1285819200000 , 1331875.0] , [ 1288497600000 , 1331875.0] , [ 1291093200000 , 1331875.0] , [ 1293771600000 , 1331875.0] , [ 1296450000000 , 1154695.0] , [ 1298869200000 , 1154695.0] , [ 1301544000000 , 1194025.0] , [ 1304136000000 , 1194025.0] , [ 1306814400000 , 1194025.0] , [ 1309406400000 , 1194025.0] , [ 1312084800000 , 1194025.0] , [ 1314763200000 , 1244525.0] , [ 1317355200000 , 475000.0] , [ 1320033600000 , 475000.0] , [ 1322629200000 , 475000.0] , [ 1325307600000 , 690033.0] , [ 1327986000000 , 690033.0] , [ 1330491600000 , 690033.0] , [ 1333166400000 , 514733.0] , [ 1335758400000 , 514733.0]]
						},
						{
						"key" : "Withdrawals",
						"bar": true,
						"values" : [ [ 1136005200000 , -1271000.0] , [ 1138683600000 , -1271000.0] , [ 1141102800000 , -1271000.0] , [ 1143781200000 , 0] , [ 1146369600000 , 0] , [ 1149048000000 , 0] , [ 1151640000000 , 0] , [ 1154318400000 , 0] , [ 1156996800000 , 0] , [ 1159588800000 , -3899486.0] , [ 1162270800000 , -3899486.0] , [ 1164862800000 , -3899486.0] , [ 1167541200000 , -3564700.0] , [ 1170219600000 , -3564700.0] , [ 1172638800000 , -3564700.0] , [ 1175313600000 , -2648493.0] , [ 1177905600000 , -2648493.0] , [ 1180584000000 , 2648493.0] , [ 1183176000000 , 2522993.0] , [ 1185854400000 , 2522993.0] , [ 1188532800000 , 2522993.0] , [ 1191124800000 , 2906501.0] , [ 1193803200000 , 2906501.0] , [ 1196398800000 , 2906501.0] , [ 1199077200000 , 2206761.0] , [ 1201755600000 , 2206761.0] , [ 1204261200000 , 2206761.0] , [ 1206936000000 , 2287726.0] , [ 1209528000000 , 2287726.0] , [ 1212206400000 , 2287726.0] , [ 1214798400000 , 2732646.0] , [ 1217476800000 , 2732646.0] , [ 1220155200000 , 2732646.0] , [ 1222747200000 , 2599196.0] , [ 1225425600000 , 2599196.0] , [ 1228021200000 , 2599196.0] , [ 1230699600000 , 1924387.0] , [ 1233378000000 , 1924387.0] , [ 1235797200000 , 1924387.0] , [ 1238472000000 , 1756311.0] , [ 1241064000000 , 1756311.0] , [ 1243742400000 , 1756311.0] , [ 1246334400000 , 1743470.0] , [ 1249012800000 , 1743470.0] , [ 1251691200000 , 1743470.0] , [ 1254283200000 , 1519010.0] , [ 1256961600000 , 1519010.0] , [ 1259557200000 , 1519010.0] , [ 1262235600000 , 1591444.0] , [ 1264914000000 , 1591444.0] , [ 1267333200000 , 1591444.0] , [ 1270008000000 , 1543784.0] , [ 1272600000000 , 1543784.0] , [ 1275278400000 , 1543784.0] , [ 1277870400000 , 1309915.0] , [ 1280548800000 , 1309915.0] , [ 1283227200000 , 1309915.0] , [ 1285819200000 , 1331875.0] , [ 1288497600000 , 1331875.0] , [ 1291093200000 , 1331875.0] , [ 1293771600000 , 1331875.0] , [ 1296450000000 , 1154695.0] , [ 1298869200000 , 1154695.0] , [ 1301544000000 , 1194025.0] , [ 1304136000000 , 1194025.0] , [ 1306814400000 , 1194025.0] , [ 1309406400000 , 1194025.0] , [ 1312084800000 , 1194025.0] , [ 1314763200000 , 1244525.0] , [ 1317355200000 , 475000.0] , [ 1320033600000 , 475000.0] , [ 1322629200000 , 475000.0] , [ 1325307600000 , 690033.0] , [ 1327986000000 , 690033.0] , [ 1330491600000 , 690033.0] , [ 1333166400000 , 514733.0] , [ 1335758400000 , 514733.0]]
						},
						{
						"key" : "Balance",
						"values" : [ [ 1136005200000 , 71.89] , [ 1138683600000 , 75.51] , [ 1141102800000 , 68.49] , [ 1143781200000 , 62.72] , [ 1146369600000 , 70.39] , [ 1149048000000 , 59.77] , [ 1151640000000 , 57.27] , [ 1154318400000 , 67.96] , [ 1156996800000 , 67.85] , [ 1159588800000 , 76.98] , [ 1162270800000 , 81.08] , [ 1164862800000 , 91.66] , [ 1167541200000 , 84.84] , [ 1170219600000 , 85.73] , [ 1172638800000 , 84.61] , [ 1175313600000 , 92.91] , [ 1177905600000 , 99.8] , [ 1180584000000 , 121.191] , [ 1183176000000 , 122.04] , [ 1185854400000 , 131.76] , [ 1188532800000 , 138.48] , [ 1191124800000 , 153.47] , [ 1193803200000 , 189.95] , [ 1196398800000 , 182.22] , [ 1199077200000 , 198.08] , [ 1201755600000 , 135.36] , [ 1204261200000 , 125.02] , [ 1206936000000 , 143.5] , [ 1209528000000 , 173.95] , [ 1212206400000 , 188.75] , [ 1214798400000 , 167.44] , [ 1217476800000 , 158.95] , [ 1220155200000 , 169.53] , [ 1222747200000 , 113.66] , [ 1225425600000 , 107.59] , [ 1228021200000 , 92.67] , [ 1230699600000 , 85.35] , [ 1233378000000 , 90.13] , [ 1235797200000 , 89.31] , [ 1238472000000 , 105.12] , [ 1241064000000 , 125.83] , [ 1243742400000 , 135.81] , [ 1246334400000 , 142.43] , [ 1249012800000 , 163.39] , [ 1251691200000 , 168.21] , [ 1254283200000 , 185.35] , [ 1256961600000 , 188.5] , [ 1259557200000 , 199.91] , [ 1262235600000 , 210.732] , [ 1264914000000 , 192.063] , [ 1267333200000 , 204.62] , [ 1270008000000 , 235.0] , [ 1272600000000 , 261.09] , [ 1275278400000 , 256.88] , [ 1277870400000 , 251.53] , [ 1280548800000 , 257.25] , [ 1283227200000 , 243.1] , [ 1285819200000 , 283.75] , [ 1288497600000 , 300.98] , [ 1291093200000 , 311.15] , [ 1293771600000 , 322.56] , [ 1296450000000 , 339.32] , [ 1298869200000 , 353.21] , [ 1301544000000 , 348.5075] , [ 1304136000000 , 350.13] , [ 1306814400000 , 347.83] , [ 1309406400000 , 335.67] , [ 1312084800000 , 390.48] , [ 1314763200000 , 384.83] , [ 1317355200000 , 381.32] , [ 1320033600000 , 404.78] , [ 1322629200000 , 382.2] , [ 1325307600000 , 405.0] , [ 1327986000000 , 456.48] , [ 1330491600000 , 542.44] , [ 1333166400000 , 599.55] , [ 1335758400000 , 583.98]]
						}
					]
					
					;
				}
			}
					});
					}
					cumulativeTestDatadd();
	
	//$(function() {
		// nvd3 charts
		//tisa_nvd3_charts.init();
	//	tisa_nvd3_charts.cumulativeLine();
	//})
	
	// nvd3 charts
	//tisa_nvd3_charts = {
		//cumulativeLine: function() {
			
		
}
</script>
