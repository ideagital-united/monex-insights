<?php
  /**
   * User
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: user.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
	define("_VALID_PHP", true);

  	require_once("../../init.php");
    if (!$user->logged_in)
      redirect_to("../index.php");  
   require_once (MODPATH . "tradersystem/admin_class.php");
   
   Registry::set('TraderSystem', new TraderSystem());
  
   include_once(MODPATHF . 'tradersystem/define_by_api.php');

 	// require_once("init_ajax.php");
 	function random_color_part() {
	    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}
	
	function random_color() {
	    return random_color_part() . random_color_part() . random_color_part();
	}
	
	if (isset($_POST['doChangeTradeMasterPwd'])){
		Filter::checkPost('accountlogin', "Not enough data to work.");
		Filter::checkPost('password1', "Not enough data to work.");
		Filter::checkPost('password2', "Not enough data to workr.");
		Filter::checkPost('product_type_id', "Not enough data to work.");
		if (empty(Filter::$msgs)) {
			$aValid = Registry::get("TraderSystem")->checkOwnAccounttrade($_SESSION['uid'],$_POST['accountlogin']);
			if ($aValid == 1) {
				if ($_POST['password1'] != $_POST['password2']) {
					$json['status'] 	='error';
					$json['message'] 	= Filter::msgError_gob('Passwords do not match.', false);
				} else {
					$aPraram['cmd'] 			= 'broke_change_pass';
					$aPraram['login'] 			= $_POST['accountlogin'];
					$aPraram['password'] 		= $_POST['password2'];
					if ($_POST['product_type_id'] == '1'){
						$res 		= Registry::get("TraderSystem")->call_curl("broke_change_pass","POST",$aPraram);
					}
					if (isset($res['isSuccess']) && $res['isSuccess'] == 1) {
						$_SESSION['changepass'][$_POST['accountlogin']] = $_POST['password2'];
						$json['status'] 	='success';
						$json['isRedirect'] ='yes';
						$json['message'] 	= Filter::msgOk_gob(Lang::$word->_UA_UPDATEOK, false);
						
					} else {
						$json['status'] 	='error';
						$json['message'] 	= Filter::msgError_gob($res['message'], false);
					}
				}
			} else {
				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob('ACCOUNT TRADE INCORRECT.', false);
			}
	    } else {
			$json['message'] = Filter::msgStatus_gob();
			
		}
	    print json_encode($json);
		exit;
	}
	
	if (isset($_POST['doReportDetail'])){
		$json 		= Registry::get("TraderSystem") ->report_get_detail();
	    print json_encode($json);
		exit;
	}
	
 	if (isset($_POST['doAccountTradeBalance'])){
 		$_POST['bank_code'] 	= 'ACCOUNT_TRADE';
		$_POST['trans_type'] 	= $_POST['trader_acc_login'];
		$_POST['amount'] 		= $_POST['amount'];
 		$chktype = $_POST['itype'];
		$moneynet = $_POST['amount']*1;
		if ($moneynet <= 0) {
			$json['status'] 	='error';
			$json['message'] 	= Filter::msgError_gob('Amount more than 0.', false);
			print json_encode($json);
			exit;
		}
 		$aValid = Registry::get("TraderSystem")->checkOwnAccounttrade($_SESSION['uid'],$_POST['trans_type']);
 		if ($chktype == 'toway') {
			$aValid2 = Registry::get("TraderSystem")->checkOwnAccounttrade($_SESSION['uid'],$_POST['trader_acc_loginto']);
			if ($aValid==1 && $aValid2==1){
				$_POST['itype'] = 'withdraw';
	 			$json 					= Registry::get("TraderSystem") ->save_transaction();
				$_POST['itype'] 		= 'deposittradetoway';
				$_POST['trans_type'] 	= $_POST['trader_acc_loginto'];
				$json 					= Registry::get("TraderSystem") ->save_transaction();
			} else {
				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob('ACCOUNT TRADE INCORRECT.', false);
			}
 			
 		} else {
 			if ($aValid==1) {
 				$json 				= Registry::get("TraderSystem") ->save_transaction();
 			} else {
 				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob('ACCOUNT TRADE INCORRECT.', false);
 			}
 			
		}
	    print json_encode($json);
		exit;
 	}
	if (isset($_POST['doBuildReport'])){
		$json 		= Registry::get("TraderSystem") ->report_data_builer_graph_user();
	    print json_encode($json);
		exit;
 	}
 	if (isset($_POST['doBuildReportnew'])){
		$json 		= Registry::get("TraderSystem") ->report_data_builer_graph_user_new();
	    print json_encode($json);
		exit;
 	}
 	if (isset($_POST['doWithdraw_payment'])){
		$json 		= Registry::get("TraderSystem") ->save_transaction_withdraw();
	    print json_encode($json);
		exit;
 	}
	if (isset($_POST['withdraw_step2'])){
		$json 		= Registry::get("TraderSystem") ->save_transaction_withdraw_step2();
	    print json_encode($json);
		exit;
 	}
	if (isset($_POST['internal_step2'])){
		$json 		= Registry::get("TraderSystem") ->save_transaction_internal_step2();
	    print json_encode($json);
		exit;
 	}
	
	
	
 	if (isset($_POST['doDeposit_payment'])){
		$json 		= Registry::get("TraderSystem") ->save_transaction_deposit();
		print json_encode($json);
		exit;
		
 	}	
 	if (isset($_POST['doSecure_user'])){
		$res = Registry::get("TraderSystem") ->call_curl("secure_tool","POST",array('cmd'=>'secure_tool','tool_id'=>$_POST['type_otp']));
		if (isset($res['isSuccess']) && $res['isSuccess'] == 1 && isset($res['msg']) && $res['msg'] != '') {
			$json['status'] 	='success';
			$json['isRedirect'] ='yes';
			$json['message'] 	= Filter::msgOk_gob(Lang::$word->_UA_UPDATEOK, false);
		} else {
			$json['status'] 	='error';
			$json['message'] 	= Filter::msgError_gob(Lang::$word->_UA_UPDATEOK, false);
		}
		print json_encode($json);
		exit;
		
 	}
	if (isset($_POST['doRequestotp_reset'])){
		$param['key_token'] 		= rand_strb(6);
		$param['key_code']  		= rand_int(6);
		$param['secure_tool_id'] 	= $_SESSION['usr_dtl']['user_info']['secure_tool_id'];
		$aSecure					= Registry::get("TraderSystem") ->get_secure_personal();
		if ($aSecure['type'] == 'email') {
			$param['send_to'] = $_SESSION['email']; 	
		} elseif($aSecure['type'] == 'sms'){
			$param['send_to'] =  $_SESSION['usr_dtl']['user_info']['mobile_code'].$_SESSION['usr_dtl']['user_info']['mobile'];
		} elseif($aSecure['type'] == 'google') {
			$param['send_to'] =  'google';
		} else{
			$param['send_to'] =  'not found';
		}
		$param['activity'] =  'reset_secure';
		$param['cmd'] =  'otp';
		$res = Registry::get("TraderSystem") ->call_curl("secureotp","POST",$param);
		if ($aSecure['type'] == 'email') {
			$sendmail 			= Registry::get("TraderSystem") ->send_mail("optsend",18,$param);
		} elseif($aSecure['type'] == 'google') {
			$param['key_token'] = 'GAuth';
		} elseif($aSecure['type'] == 'sms') {
			
			$res = Registry::get("TraderSystem")->send_sms_infobip("Two-Step Verifycation Ref:".$param['key_token']." is ".$param['key_code'],$param['send_to']);
			 
		}
		$json['key_code'] 	= $param['key_token'];
		$json['status'] 	= 'success';	  	  	
	    print json_encode($json); exit;
	}
	if (isset($_POST['doRequestotp_SMS'])){
		Filter::checkPost('phone', "Please enter Your Mobile Number.");
		if (empty(Filter::$msgs)) {
			$_POST['key_token'] = rand_strb(6);
			$_POST['key_code']  = rand_int(6);
			$_POST['cmd'] ='otp';
			$_POST['send_to'] =$_POST['phonecode'].$_POST['phone'];
			$res 				= Registry::get("TraderSystem") ->call_curl("secureotp","POST",$_POST);
			if (isset($res['isSuccess']) && $res['isSuccess'] == 1) {
	        	$aSendSms 		= Registry::get("TraderSystem") ->send_sms_infobip("Two-Step Verifycation Ref:".$_POST['key_token']." is ".$_POST['key_code'],$_POST['send_to']);
				if ($aSendSms['res'] === true) {
					$json['key_code'] 	= $_POST['key_token'];
					$json['status'] 	= 'success';
					$_SESSION['mobile_mem'] = $_POST['send_to'];
					$json['message'] 	= Filter::msgOk_gob(Lang::$word->_UA_UPDATEOK, false);
					Security::writeLog(Lang::$word->_USER . ' ' . $_SESSION['uid'].':'.$_SESSION['name']. 'setting sms opt', "user", "no", "user");	  	
				} else {
					$json['status'] 	= 'error';
					$json['message'] 	= Filter::msgError_gob($aSendSms['msg'], false);
				} 	
			} else {
				$json['status'] 	= 'error';
				$txterr 			= isset($res['errMessage']) ? $res['errMessage'] : 'NOT SEND SMS.';
				$json['message'] 	= Filter::msgError_gob($txterr, false);
			}
		    print json_encode($json); 
		} else {
			$json['message'] = Filter::msgStatus_gob();
			print json_encode($json);
		}
	   	exit;
	}
	if (isset($_POST['doGetBalanceTrade'])){
	   if (isset($_POST['typep']) && $_POST['typep'] == 2) {
	   	   $res = 120;
		   //$res = Registry::get("TraderSystem") ->getBalanceByLogin($_POST['trader_acc_login'],2);
	   } else {
	   	   $res = Registry::get("TraderSystem") ->getBalanceByLogin($_POST['trader_acc_login']);
	   }
		
	   if (is_array($res)) {
		 $json['msg'] = $res['message'];
			$json['dtl'] = $res;
			$json['balance'] = 0.00;
			$json['balanceformat'] = 0;
			$json['colorset']='#'.random_color();
	   } else {
	   		$json 		= array('balance'=>number_format($res,2),'balanceformat'=>$res,'colorset'=>'#'.random_color());
	   }
	   print json_encode($json);exit;
	}
    
    /* create mt4 */
	if (isset($_POST['doCreatemt4'])) {
		if($_SESSION['process_create_account'] != 'ready'){
			$json['status'] 	='error';
			$json['isRedirect'] ='yes';
			$json['message'] 	= Filter::msgError_gob("Please waiting.", false);
		} elseif ($_POST['is_accept'] == 1) {
		    $_SESSION['process_create_account'] = 'processing';
			$_POST['fixtest']   = 0;
			
			$_POST['cmd'] = 'broke_create_account';
		  	$res = Registry::get("TraderSystem") ->call_curl("broke_create_account_cfh","POST",$_POST);
		/*	echo "<pre>=========";print_r($res);*/
			if (isset($res['isSuccess']) && $res['isSuccess'] == 1 && isset($res['msg']) && $res['msg'] != '') {
				$fin['uid'] = $_SESSION['uid'];
				$fin['trader_product_type'] =1 ;
				$fin['trader_acc_login'] = $res['msg']['ClientAccountID'];
				$fin['equlity'] =0 ;
				$fin['balance'] =0 ;
				$fin['net_profit'] =0 ;
				$fin['credit'] = 0;
				$resinsert = Registry::get("Database")->insert('bk_trader_acc', $fin);
				$_SESSION['usr_dtl']['trade_acc_list'] = $res['trade_list'];
				
				$json['status'] 	='success';
				$json['isRedirect'] ='yes';
				$json['message'] 	= Filter::msgOk_gob(Lang::$word->_UPDATED_ACCOUNT_TRADE, false);
			} else {
				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob($res['message'], false);
			}
		} else {
			$json['status'] 	='error';
			$json['message'] 	= Filter::msgError_gob("Please accept teams and conditions.", false);
		}	
		$json['urlapi'] = URL_API;
        $json['detail'] = $res;
		if ($json['status'] != 'success') {
			$_SESSION['process_create_account'] = 'ready';
		}
	    print json_encode($json); exit;
	}

    
     
   /* create ctrade */
	if (isset($_POST['doCreatect'])) {
	    if($_SESSION['process_create_account'] != 'ready'){
			$json['status'] 	='error';
			$json['isRedirect'] ='yes';
			$json['message'] 	= Filter::msgError_gob("Please waiting.", false);
		}elseif ($_POST['is_accept'] == 1) {
		    $_SESSION['process_create_account'] = 'processing';
			//$_POST['group'] 		= "CT001";
			$_POST['leverage'] 		= '500';
			$_POST['fixtest'] 		= 1;
            $_POST['cmd'] = 'broke_create_account_ct';
		  	$res = Registry::get("TraderSystem") ->call_curl("broke_create_account_ct","POST",$_POST);
			
			if (isset($res['isSuccess']) && $res['isSuccess'] == 1 && isset($res['msg']) && $res['msg'] != '') {
				
				
				$_SESSION['usr_dtl']['trade_acc_list'] = $res['trade_list'];
				
				$json['status'] 	='success';
				$json['isRedirect'] ='yes';
				$json['message'] 	= Filter::msgOk_gob(Lang::$word->_UPDATED_ACCOUNT_TRADE, false);
			} else {
				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob($res['message'], false);
			}
		} else {
			$json['status'] 	='error';
			$json['message'] 	= Filter::msgError_gob("Please accept teams and conditions.", false);
		}	
		$json['detail'] = $res;
		if ($json['status'] != 'success') {
			$_SESSION['process_create_account'] = 'ready';
		}
	    print json_encode($json); exit;
	}


	if (isset($_POST['doRequestOtp'])){
		
		$param['key_token'] 		= rand_strb(6);
		$param['key_code']  		= rand_int(6);
		$param['secure_tool_id'] 	= $_SESSION['usr_dtl']['user_info']['secure_tool_id'];
		$aSecure					= Registry::get("TraderSystem") ->get_secure_personal();
		if ($aSecure['type'] == 'email') {
			$param['send_to'] = $_SESSION['email']; 	
		} elseif($aSecure['type'] == 'sms'){
			$param['send_to'] =  $_SESSION['usr_dtl']['user_info']['mobile_code'].$_SESSION['usr_dtl']['user_info']['mobile'];
		} elseif($aSecure['type'] == 'google') {
			$param['send_to'] =  'google';
		} else{
			$param['send_to'] =  'not found';
		}
		$param['activity'] =  (isset($_POST['activity'])) ? $_POST['activity'] : 'withdraw';
		$param['cmd'] ='otp';
		$res = Registry::get("TraderSystem") ->call_curl("secureotp","POST",$param);
		if (isset($res['isSuccess']) && $res['isSuccess'] == 1) {
			if ($aSecure['type'] == 'email') {
				$sendmail 			= Registry::get("TraderSystem") ->send_mail("optsend",18,$param);
			} elseif($aSecure['type'] == 'google') {
				$param['key_token'] = 'GAuth';
			} elseif($aSecure['type'] == 'sms') {
				$res = Registry::get("TraderSystem")->send_sms_infobip("Two-Step Verifycation Ref:".$param['key_token']." is ".$param['key_code'],$param['send_to']);
			}
			$json['key_code'] 	= $param['key_token'];
			$json['status'] 	= 'success';
		} else {
				$json['status'] 	= 'error';
				$txterr 			= isset($res['errMessage']) ? $res['errMessage'] : 'NOT SEND SMS.';
				$json['message'] 	= Filter::msgError_gob($txterr, false);
		}
			  	  	
	    print json_encode($json); exit;
	}

	if (isset($_POST['doGetRefName'])) {
        if ($_POST['refname'] != '') { 
           $aPOSTAPI['refname'] = $_POST['refname'];
           $aAcc['uid'] 		= 0;               
           $aAcc 				= Registry::get("TraderSystem") ->call_curl_get("check_ref_uid","GET",$_POST['refname']."?cmd=check_ref_uid");

    		if ($aAcc['uid'] > 0) {
    			$json 	= array(   'status'=>'success','name'=>$aAcc['uid'],'msg'=>'Available referrer name');
    		} else {
    			$json 	= array(   'status'=>'error','name'=>0, 'msg'=>'Not found referrer name.');	
    		}
        } else {
            $json 		= array(   'status'=>'error', 'name'=>0,'msg'=>'Please input referrer name.');                   
       }  
		print json_encode($json); exit;
    } 
    
    
    if (isset($_POST['doGetEmail'])) {
        if ($_POST['email'] != '') {
            $aAcc = Registry::get("TraderSystem") ->call_curl_get("check_exist_email","GET",$_POST['email']."?cmd=check_exist_email");
          if ($aAcc['id'] <= 0) {
             if (preg_match('/^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,12}$/', $_POST['email'])) 
             {
              	 $json = array('status'=>'success','name'=>'valid email', 'msg'=>'Email available');
             } else {
                 $json = array( 'status'=>'error','name'=>'invalid email','msg'=>'Entered Email Address Is Not Valid'); 
              }
              
           } else {
				$json = array(   'status'=>'error', 'name'=>'invalid email', 'msg'=>'This email is exist');    
          }
        } else {
            $json = array( 'status'=>'error', 'name'=>'email empty', 'msg'=>'Please input email.');                     
        }       
        print json_encode($json);
		exit;
                
    }


?>
