<?php
$aGateway 		= $tradersystem->call_curl_get('gateway?cmd=gateway', "GET");
/*echo "<pre>";print_r($_SESSION['usr_dtl']['deposit_bank_list']);*/
$aGateway		= (isset($aGateway['isSuccess']) && $aGateway['isSuccess'] == 1 && count($aGateway['message']) > 0) ? $aGateway['message'] : false;


$wallet_net 	= (isset($_SESSION['usr_dtl']['wallet_amt']['amount'])) ?$_SESSION['usr_dtl']['wallet_amt']['amount']: 0.00;
		
	
$aBankBroker	= (isset($_SESSION['usr_dtl']['deposit_bank_list']) && count($_SESSION['usr_dtl']['deposit_bank_list']) > 0) ? $_SESSION['usr_dtl']['deposit_bank_list'] : false;


$listAcc		= (isset($_SESSION['usr_dtl']['trade_acc_list'])  && count($_SESSION['usr_dtl']['trade_acc_list']) > 0) ? $_SESSION['usr_dtl']['trade_acc_list'] : false;

$listAcc2 = $listAcc;
$a = count($listAcc2);
$b = 0;
do {
    if($listAcc2[$b]['is_demo'] == 1){
		unset($listAcc2[$b]);
	}
	$b++;
} while ($b <= $a);

$alistHistory   = $tradersystem->list_history("deposit",false);


//=== START RATE EX 
$sqlrate = "SELECT * FROM bk_exchn_rate_new where status=1";
$aRate = $db->fetch_all($sqlrate);
if (!$aRate) {
	$aRate = false;
}
//=== END RATE EX

?>
<script>
var rate_deposit = 1;
</script>
<?php
//=== START RATE EX 
?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3><?php echo Lang::$word->_MAKE_DEPOSIT;?>  [Wallet $<?php echo number_format($wallet_net,2);?>]</h3></div>
					<div class="panel-body">
						<?php
						if ( $aGateway === false) {
							echo "NO DATA";
						} else {
							$content = '';
							foreach($aGateway as $k => $v)
							{
								$form_url = BASEPATH . "modules/tradersystem/gateways/" . $v['dir'] . "/form.tpl.php";
								/*echo $form_url;*/
								if (file_exists($form_url)){
									ob_start();
									include ($form_url);
									$content .= ob_get_contents();
									ob_end_clean();
								}
							}
							echo $content;
						}
						
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3><?php echo Lang::$word->_DEPOSIT_HISTORY;?></h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th><?php echo Lang::$word->_TRANSECTION_NUMBER;?></th>
									<th><?php echo Lang::$word->_TRANS_TYPE;?></th>
									<th><?php echo Lang::$word->_RELEASE_DATE;?></th>
									<th><?php echo Lang::$word->_SERVICE_CHANNEL;?></th>
									<th><?php echo Lang::$word->_TR_AMOUNT;?></th>
									<th><?php echo Lang::$word->_SYS_DBSTATUS;?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($alistHistory) {
									$aStatus = array(
									'1' => 'success',
									'2' => 'danger',
									'0' => 'warning',
									'10' => 'warning',
									'11' => 'warning'
									);
									$textStatus = array(
									'0' => 'Processing',
									'1' => 'Successed',
									'2' => 'Canceled',
									'10' => 'Pending',
									'11' => 'Processing'
									);
									$ii = 1;
									foreach($alistHistory as $k => $v){
										
									?>
										<tr>
										<td><?php echo $ii;?></td>
										<td><?php echo ($v->trans_type == 'wallet')?"wallet":"Withdrawal from :".$v->trans_type;?></td>
										<td><?php echo $v->save_date;?></td>
										<td><?php echo $v->gateway_dtl;?></td>
										<td>$<?php echo number_format($v->money,2);?></td>
										
										<td class="<?php echo $aStatus[$v->status]?>"><?php echo $textStatus[$v->status];?></td>
								</tr>
									<?php
									$ii++;
									 }
								}
								
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="modal fade" id="modal_notactive">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			
			<h3>Payment Method Not Activated</h3>
			
		</div>
	</div>
</div>
</div>
<?php
include "alert_msg.php";
?>