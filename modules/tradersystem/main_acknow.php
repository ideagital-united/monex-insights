<form class="form-horizontal" id="prolific_form_acknow" name="prolific_form_acknow" method="post">
<div class="form-group">
	<div class="col-md-12">
		<div class="heading_b">Acknowledgements</div>
		<span class="help-block">I have read and understood  the following legal documentation:</span>
	</div>
</div>
<div class="fields">
		<div class="field">
		<label></label>          
          <div class="inline-group">
            <label class="checkbox">
              <div class="col-md-6"><input name="ack_finance" type="checkbox" value="1"> <i></i>Financial Services Guide (FSG)</div>
              <div class="col-md-6"><a href="#" target="_blank">Download FSG</a></div>
            </label>
            
            <label class="checkbox">
              <div class="col-md-6"> <input name="ack_nda" type="checkbox" value="1" ><i></i>Product Disclosure Statement(PDS)</div>
              <div class="col-md-6"><a href="#" target="_blank">Download PDS</a></div>
            </label>
         
          
           	<label class="checkbox">
           	   <div class="col-md-6"><input name="ack_agree" type="checkbox" value="1" ><i></i>Terms and Conditions</div>
               <div class="col-md-6"><a href="#" target="_blank">Download Terms and Conditions</a> </div>
           	</label>
          </div>
	</div>
</div>
<div class="fields">
		<div class="field">
		<label></label>          
          <div class="inline-group">
            <label class="checkbox">
              <input name="act_acknowledge" type="checkbox" value="1">
              <i></i>[Acknowledgement should also contain a staatement that the information profvided is complete, true & correct. 
              Also, that the source of funds, operation of the account and any withdrawals from the account are in no way
              related to any money laundering, terrotism financing or other illegal activities.]</label>
            <label class="checkbox">
              <input name="act_under_risk" type="checkbox" value="1" >
              <i></i>[Understanding of risks]</label>
          </div>
          </div>
</div>

<div class="text-center">
	<input name="doProfile_acknow" type="hidden" value="1">
	<button  onclick ="tabproduct();"type="button" name="bu_back" class="prolific button btn">
		<i class="fa fa-long-arrow-left"></i> Back </button>
	<button data-url="/modules/tradersystem/ajax_profile.php" data-frm="prolific_form_acknow"  type="button" name="dosubmit" class="prolific button btn btn-success">
		Submit Application <i class="fa fa-long-arrow-right"></i></button>
</div>
</form>

<script type="text/javascript">
function tabproduct() {
	$('.nav-tabs a[href="#product"]').tab('show');
}
</script>
