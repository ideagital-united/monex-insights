<?php
function data_uri($file, $mime) 
{  
  $contents = file_get_contents($file);
  $base64   = base64_encode($contents); 
  return ('data:' . $mime . ';base64,' . $base64);
}

if($_SERVER['HTTPS'] == 'on'){
	$protocol = 'https';
} else {
	$protocol = 'http';
}

if(isset($_SESSION['usr_dtl']['user_info']['google_auth_key'])){
	$url = $protocol."://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/".$_SESSION['CMSPRO_username']."+at+demo?secret=".$_SESSION['usr_dtl']['user_info']['google_auth_key'];
} else {
	
}

$backward = $protocol."://".$_SERVER['HTTP_HOST']."/".$_GET['url']."?subpage=".$_GET['subpage'];
// $backward=demov2.brokers.solutions/page/tradersystem/index.php?subpage=security_new
//$backward = $protocol."://".$_GET['url']."?subpage=".$_GET['subpage'];

//echo "<br />backward=".$backward;

?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">

<?php
if( ($_SESSION['usr_dtl']['user_info']['email_verified'] == 1) || ($_SESSION['usr_dtl']['user_info']['google_auth_verified'] == 1) || ($_SESSION['usr_dtl']['user_info']['sms_verified'] == 1)) {
?>

<div class="panel-heading"><h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3></div>


<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post" target=''>
    
    
	<div class="panel-body">
	    
	   
	    
	    
<?php
switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id']) {
	case 1:
		?>
		
		<div class="col-sm-12 col-md-12">
		    <?php echo Lang::$word->_NOW_SECURITY_USE; ?>
		</div>
		
		
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_EMAIL;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO2;?></span>
			
			
		    <div><br /></div>  
            
            <div id="reset_button">
                <a href='index.php?subpage=reset_secure'><button type="button" name="reset_security" class="prolific button btn btn-success">
                    <i class="fa fa-save"></i> <?php echo Lang::$word->_RESET_SECURE;?>
                </button></a>
            </div>
			
			
		</div>
		

            
		<?php
	break;
	case 2:
		?>
		
		<div class="col-sm-12 col-md-12">
            <?php echo Lang::$word->_NOW_SECURITY_USE; ?>
        </div>
        
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO5;?></span>
		    
		    <div><br /></div>  
		    
		    <div id="reset_button">
		        <a href='index.php?subpage=reset_secure'><button type="button" name="reset_security" class="prolific button btn btn-success">
		            <i class="fa fa-save"></i> <?php echo Lang::$word->_RESET_SECURE;?>
		        </button></a>
		    </div>
		
		</div>
		
		
		
		<?php
	break;
	case 3:
		?>
		
		<div>
            <?php echo Lang::$word->_NOW_SECURITY_USE; ?>
        </div>
        
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_MOBILE_PHONE_SMS;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
			
			<div><br /></div>  
            
            <div id="reset_button">
                <a href='index.php?subpage=reset_secure'><button type="button" name="reset_security" class="prolific button btn btn-success">
                    <i class="fa fa-save"></i> <?php echo Lang::$word->_RESET_SECURE;?>
                </button></a>
            </div>
            
		</div>
		<?php
	break;
} 
?>
	</div>
</form>
<?php
} else {
	switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id'])
	{
		case 1:
			?>
			<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_EMAIL;?></h3></div>
			<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
			<div class="form-group">
			<div class="col-md-12">
			<span class="help-block"><?php echo Lang::$word->_EMAIL_SEND_TOKEN;?></span>
			</div>
			</div>
			<div class="form-group">
			<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_EMAIL_TOKEN;?></label>
			<div class="col-md-5">
			<input type="text" name="token" id="token" class="form-control">
			</div>
			</div>
			<div class="text-left">
			<input type="hidden" name="backward" value="<?php echo $backward;?>">
			<input name="command" type="hidden" value="verify_token">
			<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
			</div>
			</form>
			<?php
		break;
		case 2:
			?>
			<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_GA;?></h3></div>
			<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
			<div class="form-group">
			<div class="col-md-12">
			<img src="<?php echo data_uri($url,'image/png'); ?>" />
			<span class="help-block"><?php echo Lang::$word->_GA_SETUP;?></span>
			<a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=th" target="_blank"><img src="<?php echo UPLOADURL?>Google-Play-Badge.png" width="150" /></a>

			<a href="https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="<?php echo UPLOADURL?>App-Store-Badge.png" width="150" /></a>
			</div>
			</div>
			<div class="form-group">
			<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_GA_TOKEN;?></label>
			<div class="col-md-5">
			<input type="text" name="token" id="token" class="form-control">
			</div>
			</div>
			<div class="text-left">
			<input type="hidden" name="backward" value="<?php echo $backward;?>">
			<input name="command" type="hidden" value="verify_token">
			<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
			</div>
			</form>
			<?php
		break;
		case 3:
			if( ($_SESSION['usr_dtl']['user_info']['mobile'] == null) || ($_SESSION['usr_dtl']['user_info']['mobile'] == '') ){
				//create set mobile form here
				//hidden, option = 3, command = setmob
				//text, mobile = [user input]
				?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_SMS;?></h3></div>
				
				<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
				
				<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
				
				<div class="form-group stepsms">
				    <div class="col-md-12">
				        <div class="heading"><?php echo Lang::$word->_PLEASE_INSERT_YOUR_MOBILE_NUMBER;?>
				            
				         <?php echo Lang::$word->_MOBILE_NUMBER_INTER_INFO; ?>   
 
				        </div>
				    </div>
				</div>

				<div class="form-group stepsms">
				        <label for="mobile" class="col-md-2 control-label"><?php echo Lang::$word->_YOUR_PHONE_NUMBER;?></label>
				    <div class="col-md-10 ">
				        <input type="text" id="mobile" name="mobile" maxlength="15" class="form-control">
				    </div>
				</div>
				
				<input type="hidden" name="option" value="3">
				<input type="hidden" name="backward" value="<?php echo $backward;?>">
				<input name="command" type="hidden" value="setmobile">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_NEXT;?></button>
				</div>
				</form>
				
				<?php
			} else {
				?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_SMS; ?></h3></div>
				<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php echo Lang::$word->_SMS_SEND_TOKEN; ?></span>
				</div>
				</div>
				
				<?php if ($_GET['setsms'] == 'fail') { ?>
                <div class="form-group">
                    <div class="col-md-2"> </div>
                    <div class="col-md-10"><span style="color:#FF0000;text-align: center;"><?php echo Lang::$word->_WRONG_OTP; ?></span></div>
                </div>
                <?php } ?>
                
				<div class="form-group">
				<label for="token" class="col-md-2 control-label"><?php echo Lang::$word->_SMS_TOKEN; ?></label>
				<div class="col-md-10">
				<input type="text" name="token" id="token" class="form-control" maxlength="8">
				</div>
				</div>
				<div class="text-left">
				<input type="hidden" name="backward" value="<?php echo $backward;?>">
				<input name="command" type="hidden" value="verify_token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
				</div>
				</form>
				<?php
			}
		break;
		default:
			?>
			<div class="panel-heading">
			    <h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3>   
			</div>
			
			<div class="panel-body"><?php echo Lang::$word->_SECURITY_INFO; ?>
			
			
			<div>
                <br /><br />
            </div>    
			    

			<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post" action="/modules/tradersystem/secureproc.php">
    			
    			<div class="row">
                    <div class="col-sm-12">
                        <div class="row bs_switch_wrapper">
                            
                            
                            <div class="col-sm-12 col-md-12">
                                 
		                         <label for="email_select">
		                             <h4><input type="radio" name="option" id="email_select" value="1" />
		                             <?php echo Lang::$word->_EMAIL_SECURE_OPTION; ?></h4>
		                         </label>
		                         <span><?php echo Lang::$word->_SECURITY_INFO_EMAIL; ?></span>
		                    </div>
		                    
		                    <div class="col-sm-12 col-md-12">
		                        <br />
		                    </div>
		                    
		                    
		                    <div class="col-sm-12 col-md-12">
                                 
                                 <label for="sms_select">
                                     <h4><input type="radio" name="option" id="sms_select" value="3" /> 
                                     <?php echo Lang::$word->_MOBILE_PHONE_SMS; ?></h4>
                                 </label>
                                 <span><?php echo Lang::$word->_SECURITY_INFO_SMS; ?></span>
                            </div>
                            
                            <div class="col-sm-12 col-md-12">
                                <br />
                            </div>
    			                    
    			                    
    			            <div class="col-sm-12 col-md-12">
    			                 
    			                 <label for="ga_select">
    			                     <h4><input type="radio" name="option" id="ga_select" value="2" />
    			                     <?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH; ?></h4>
    			                 </label>
    			                 <span><?php echo Lang::$word->_SECURITY_INFO_GOOGLE_AUTH; ?></span>
    			            </div>
    			            
    			            
    			            <div class="col-sm-12 col-md-12">
                                <br /><br />
                            </div>
                            
    			            
    			            
    			            
    			            <div class="col-sm-12 col-md-12">
 
                                <div class="text-left">
                                    <!-- <button class="btn btn-success"><i class="fa fa-save"></i> Save Setting</button>
                                    -->
                                    <button class="btn btn-success" onclick="this.form.submit();"><i class="fa fa-angle-double-right"></i> <?php echo Lang::$word->_NEXT; ?></button>
                                    <!-- <input type="submit" value="<?php echo Lang::$word->_NEXT; ?>" class="btn btn-success"> -->
                                    <button class="btn btn-warning" onclick="this.form.reset();return false;"><i class="fa fa-refresh"></i> <?php echo Lang::$word->_CANCEL; ?></button>
                                    <!-- <input type="reset" value="<?php echo Lang::$word->_CANCEL; ?>" class="btn btn-warning"> -->
                                </div>
                            
                        
                        
                    			<input type="hidden" name="command" value="select_option">
                    			<input type="hidden" name="backward" value="<?php echo $backward;?>">
                    			
                    	    </div>
    			
    			
    			       </div>
    			   </div>
    			</div>
			</form>
			
			
			</div>
			
			
			
			<?php
	}
}
?>

				</div>
			</div>
		</div>
	</div>
</div>
</div>

