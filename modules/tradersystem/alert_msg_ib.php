<!-- warning msg -->
<div class="modal fade" id="msg-showwarningmsg">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="javascript:window.location.reload()">X</button>
			 <h3><span id="showtitle"></span></h3> -->
			<form class="form-horizontal">
				<div class="form-group">
					<div id="msgholder">##MSG##</div>
					
					
				</div>
				<div class="text-center">
					<button class="btn btn-danger" data-dismiss="modal" onclick="javascript:window.location.reload()"><i class="fa fa-chevron-left"></i> Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>

<!-- loading -->
<div class="modal fade" id="msg-loading">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<div class="text-center"><img class=""src="<?php echo THEMEURLTRADE;?>assets/img/Preloader_9.gif" alt="loading"></div>
		</div>
	</div>
</div>
</div>