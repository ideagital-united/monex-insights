<div class="panel-heading"><h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3></div>
<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post" target=''>
	<div class="panel-body">
<?php
switch ($secure_tool_id) {
	case 1:
		?>
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_EMAIL;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO2;?></span>
		</div>
		<?php
	break;
	case 2:
		?>
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO5;?></span>
		</div>
		<?php
	break;
	case 3:
		?>
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_MOBILE_PHONE_SMS;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
		</div>
		<?php
	break;
} 
?>
	</div>
</form>