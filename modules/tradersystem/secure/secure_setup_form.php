<div class="panel-heading"><h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3></div>
<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post" target=''>
	<div class="panel-body">
	<?php echo Lang::$word->_SECURITY_INFO;?>
		<div class="row">
			<div class="col-sm-12">
				<div class="row bs_switch_wrapper">
					<div class="col-sm-12 col-md-12">
						<h4><?php echo Lang::$word->_EMAIL;?></h4>
							<input type="radio" class="bs_switch_radio" name="radio_switch1" value="1" >
						<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO2;?></span>
					</div>
					<div class="col-sm-12 col-md-12">
						<h4><?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH;?></h4>
							<input type="radio" class="bs_switch_radio" name="radio_switch1" value="2">
						<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO5;?></span>
					</div>
					<div class="col-sm-12 col-md-12">
						<h4><?php echo Lang::$word->_MOBILE_PHONE_SMS;?></h4>
							<input type="radio" class="bs_switch_radio" name="radio_switch1" value="3">
						<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="text-center">
				<input name="secure_setup_form" type="hidden" value="1">
				<input type="submit" value="<?php echo Lang::$word->_SAVE_SETTING;?>" class="prolific button btn btn-success" class="fa fa-save">
			</div>
		</div>
	</div>
</form>