<?php
function data_uri($file, $mime) 
{  
  $contents = file_get_contents($file);
  $base64   = base64_encode($contents); 
  return ('data:' . $mime . ';base64,' . $base64);
}

$url = "http://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/".$_SESSION['CMSPRO_username']."+at+demo?secret=".$_SESSION['usr_dtl']['user_info']['google_auth_key'];

?>

<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_GA;?></h3></div>
<form class="form-horizontal" id="prolific_form" name="input_token" method="post">
<div class="form-group">
	<div class="col-md-12">
		<img src="<?php echo data_uri($url,'image/png'); ?>" />
		<span class="help-block"><?php echo Lang::$word->_GA_SETUP;?></span>
		<a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=th" target="_blank"><img src="<?php echo UPLOADURL?>/Google-Play-Badge.png" width="150" /></a>
		
		<a href="https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="<?php echo UPLOADURL?>/App-Store-Badge.png" width="150" /></a>
	</div>
</div>
<div class="form-group">
	<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_GA_TOKEN;?></label>
	<div class="col-md-5">
		<input type="text" name="token" id="token" class="form-control">
	</div>
</div>

<div class="text-left">
	<input name="verify_secure_form" type="hidden" value="1">
	<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
</div>
</form>