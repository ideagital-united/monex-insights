<?php 
if(isset($reqopt_result)){
	print_r($reqopt_result);
	$secure_tool_id = $reqopt_result['message']['secure_tool_id'];
	if($secure_tool_id == 1){
		//send email
		$token_key = $reqopt_result['message']['key_token'];
		$token_code = $reqopt_result['message']['key_code'];
		$send_to = $reqopt_result['message']['send_to'];
				//start sent mail
require_once (BASEPATH . "lib/class_mailer.php");
$row = Core::getRowById(Content::eTable, 23);
$body = str_replace(array('[USERNAME]','[KEY]','[TOKEN]','[URL]','[SITE_NAME]'),array($send_to,$token_key,$token_code,SITEURL,Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
$newbody = cleanOut($body);
$mailer = Mailer::sendMail();
$message = Swift_Message::newInstance()
->setSubject($row->{'subject' . Lang::$lang})
->setTo(array($send_to => $send_to))
->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
->setBody($newbody, 'text/html');
$mailer->send($message);
				// end send mail
	} else {
		//pending***** send sms
	}
} else {
}
if($secure_tool_id == 2){
} else {
	?>
<div class="panel-heading"><h3>Request OTP</h3></div>
<form class="form-horizontal" id="prolific_form" name="reset_token_req" method="post">
<div class="form-group">
	<div class="col-md-12">
	<?php
	switch ($secure_tool_id)
	{
		case 1:
		?><span class="help-block">Click this button will send OTP to your email.</span><?php
		break;
		case 3:
		?><span class="help-block">Click this button will send OTP as SMS to your mobile number.</span><?php
		break;
	}
	?>
	</div>						
</div>
<div class="form-group">
</div>
	<div class="text-left">
		<input name="reset_token_req" type="hidden" value="1">
			<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i>Request OTP</button>
	</div>
</form>
	<?php
}
?>


<div class="panel-heading"><h3>Resetting Security</h3></div>
<div class="col-md-12"><span class="help-block"></span></div>
<form class="form-horizontal" id="prolific_form" name="resetseure_send" method="post">
<div class="form-group">
	<div class="col-md-12">
	<?php
	switch ($secure_tool_id)
	{
		case 1:
		?><span class="help-block">Please check your email after request OTP.</span><?php
		break;
		case 2:
		?><span class="help-block">Use generated OTP from your registered device</span><?php
		break;
		case 3:
		?><span class="help-block">Please check your SMS after request OTP.</span><?php
		break;
	}
	?>
	</div>						
</div>
<div class="form-group">
	<label for="profile_username" class="col-md-3 control-label">Please input your OTP here</label>
	<div class="col-md-5">
		<div class="input-group">
			<span class="input-group-addon" style="text-align: left">
			<?php 
				if(isset($token_key)){
					echo $token_key;
				} else {
					echo "Please Request OTP";
				} 
				?>
			</span>
			<input class="form-control" name="reset_token" id="reset_token" type="text" placeholder="OTP Token">
		</div>
	</div>
</div>

	<div class="text-left">
		<input name="resetseure_send" type="hidden" value="1">
			<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
	</div>
</form>