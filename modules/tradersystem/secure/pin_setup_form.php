<div class="panel-heading"><h3><?php echo Lang::$word->_OPTIONAL_SECURITY_SETTING;?></h3></div>
<form class="form-horizontal" id="prolific_form2" name="prolific_form2" method="post">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<div class="row bs_switch_wrapper">
					<div class="col-sm-12 col-md-12">
						<h4><?php echo Lang::$word->_IP_CHECK;?></h4>
						<input type="radio" class="bs_switch bs_switch_radio" name="radio_switch2" value="option 1">
						<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO6;?>
						<?php echo Lang::$word->_SECURITY_INFO7;?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="text-center">
				<input name="ip_check_setup" type="hidden" value="1">
				<button type="submit" name="dosubmit" class="prolific button btn btn-success">
					<i class="fa fa-save"></i><?php echo Lang::$word->_SAVE_SETTING;?>
				</button>
			</div>
		</div>
	</div>
</form>