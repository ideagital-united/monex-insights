<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_SMS;?></h3></div>
<form class="form-horizontal" id="prolific_form" name="input_token" method="post">
<div class="form-group">
	<div class="col-md-12">
		<span class="help-block"><?php echo Lang::$word->_SMS_SEND_TOKEN;?></span>
	</div>
</div>
<div class="form-group">
	<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_SMS_TOKEN;?></label>
	<div class="col-md-5">
		<input type="text" name="token" id="token" class="form-control">
	</div>
</div>

<div class="text-left">
	<input name="verify_secure_form" type="hidden" value="1">
	<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
</div>
</form>


<div class="panel-heading"><h3><?php echo Lang::$word->_NO_TOKEN_GET;?></h3></div>
<form class="form-horizontal" id="prolific_form" name="not_received" method="post">
<div class="form-group">
	<div class="col-md-12">
		<span class="help-block"><?php echo Lang::$word->_NO_TOKEN_GET_INFO_SMS;?></span>
	</div>
</div>
<div class="form-group">
	<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_SENT_TOKEN_SMS;?></label>
	<div class="col-md-5">
		<?php echo $_SESSION['usr_dtl']['user_info']['mobile'];?>
	</div>
</div>

<div class="text-left">
	<input name="request_new_email_token" type="hidden" value="1">
	<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_REQUEST_OPT;?></button>
</div>
</form>