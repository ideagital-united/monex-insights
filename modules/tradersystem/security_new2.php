<?php
function data_uri($file, $mime) 
{  
  $contents = file_get_contents($file);
  $base64   = base64_encode($contents); 
  return ('data:' . $mime . ';base64,' . $base64);
}

if($_SERVER['HTTPS'] == 'on'){
	$protocol = 'https';
} else {
	$protocol = 'http';
}

if(isset($_SESSION['usr_dtl']['user_info']['google_auth_key'])){
	$url = $protocol."://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/".$_SESSION['CMSPRO_username']."+at+demo?secret=".$_SESSION['usr_dtl']['user_info']['google_auth_key'];
} else {
	
}

$backward = $_SERVER['HTTP_HOST']."/".$_GET['url']."?subpage=".$_GET['subpage'];

?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">

<?php
if($_SESSION['usr_dtl']['user_info']['email_verified'] == 1||$_SESSION['usr_dtl']['user_info']['google_auth_verified']||$_SESSION['usr_dtl']['user_info']['sms_verified'] ==1){
?>
<div class="panel-heading"><h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3></div>
<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post" target=''>
	<div class="panel-body">
<?php
switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id']) {
	case 1:
		?>
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_EMAIL;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO2;?></span>
		</div>
		<?php
	break;
	case 2:
		?>
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO5;?></span>
		</div>
		<?php
	break;
	case 3:
		?>
		<div class="col-sm-12 col-md-12">
			<h4><?php echo Lang::$word->_MOBILE_PHONE_SMS;?></h4>
			<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
		</div>
		<?php
	break;
} 
?>
	</div>
</form>
<?php
} else {
	switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id'])
	{
		case 1:
			?>
			<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_EMAIL;?></h3></div>
			<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
			<div class="form-group">
			<div class="col-md-12">
			<span class="help-block"><?php echo Lang::$word->_EMAIL_SEND_TOKEN;?></span>
			</div>
			</div>
			<div class="form-group">
			<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_EMAIL_TOKEN;?></label>
			<div class="col-md-5">
			<input type="text" name="token" id="token" class="form-control">
			</div>
			</div>
			<div class="text-left">
			<input type="hidden" name="backward" value="<?php echo $backward;?>">
			<input name="command" type="hidden" value="verify_token">
			<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
			</div>
			</form>
			<?php
		break;
		case 2:
			?>
			<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_GA;?></h3></div>
			<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
			<div class="form-group">
			<div class="col-md-12">
			<img src="<?php echo data_uri($url,'image/png'); ?>" />
			<span class="help-block"><?php echo Lang::$word->_GA_SETUP;?></span>
			<a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=th" target="_blank"><img src="<?php echo UPLOADURL?>/Google-Play-Badge.png" width="150" /></a>

			<a href="https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="<?php echo UPLOADURL?>/App-Store-Badge.png" width="150" /></a>
			</div>
			</div>
			<div class="form-group">
			<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_GA_TOKEN;?></label>
			<div class="col-md-5">
			<input type="text" name="token" id="token" class="form-control">
			</div>
			</div>
			<div class="text-left">
			<input type="hidden" name="backward" value="<?php echo $backward;?>">
			<input name="command" type="hidden" value="verify_token">
			<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
			</div>
			</form>
			<?php
		break;
		case 3:
			if($_SESSION['usr_dtl']['user_info']['mobile'] == null|| $_SESSION['usr_dtl']['user_info']['mobile'] == ''){
				//create set mobile form here
				//hidden, option = 3, command = setmob
				//text, mobile = [user input]
				?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_SMS;?></h3></div>
				<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
				<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
				<div class="form-group stepsms">
				<div class="col-md-12">
				<div class="heading_b"><?php echo Lang::$word->_PLEASE_INSERT_YOUR_MOBILE_NUMBER;?></div>
				</div>
				</div>
				<div class="form-group stepsms">
				<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_YOUR_PHONE_NUMBER;?></label>
				<div class="col-md-10 ">
				<input type="text" id="mobile" name="mobile" class="form-control">
				</div>
				</div>
				<input type="hidden" name="option" value="3">
				<input type="hidden" name="backward" value="<?php echo $backward;?>">
				<input name="command" type="hidden" value="setmobile">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
				</div>
				</form>
				<?php
			} else {
				?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_SECURE_BY_SMS;?></h3></div>
				<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/secureproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php echo Lang::$word->_SMS_SEND_TOKEN;?></span>
				</div>
				</div>
				<div class="form-group">
				<label for="email_token" class="col-md-2 control-label"><?php echo Lang::$word->_SMS_TOKEN;?></label>
				<div class="col-md-5">
				<input type="text" name="token" id="token" class="form-control">
				</div>
				</div>
				<div class="text-left">
				<input type="hidden" name="backward" value="<?php echo $backward;?>">
				<input name="command" type="hidden" value="verify_token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
				</div>
				</form>
				<?php
			}
		break;
		default:
			?>
			<div class="panel-heading"><h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3></div>
			<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post" action="/modules/tradersystem/secureproc.php">
			<input type="radio" name="option" id="email_select" value="1" />
			<label for="email_select">Email</label>
			<input type="radio" name="option" id="ga_select" value="2" />
			<label for="email_select">Google</label>
			<input type="radio" name="option" id="sms_select" value="3" />
			<label for="email_select">SMS</label>
			<input type="hidden" name="command" value="select_option">
			<input type="hidden" name="backward" value="<?php echo $backward;?>">
			<input type="submit" value="Submit">
			</form>
			<?php
	}
}
?>

<!-- start darawan -->
<div class="panel-heading"><h3>Optional Security Settings</h3></div>
<div class="panel-body">
	<div class="row">
		<div class="col-sm-12">
			<div class="row bs_switch_wrapper">
				<div class="col-sm-12 col-md-12">
					<h4>IP Check</h4>
					<input type="radio" class="bs_switch bs_switch_radio" name="radio_switch2" value="option 1">
					<span class="help-block">To preventing and detecting unauthorized use of your account.
					Prevention measures help you to stop unauthorized users (also known as "intruders") from your account by send you OPT everytime the system detect IP change.</span>
					
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="text-center">
			<button class="btn btn-success"><i class="fa fa-save"></i> Save Setting</button>
		</div>
	</div>
</div>
</div>

<!-- end datawan -->
				</div>
			</div>
		</div>
		
	</div>
</div>
</div>




