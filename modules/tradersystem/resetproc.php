<?php
//Array ( [backward] => demov2.brokers.solutions/page/tradersystem/index.php?subpage=reset_secure [command] => req-token [Submit] => )
//print_r($_POST);
session_start();
define("_VALID_PHP", true);
require_once("../../init.php");
require_once (MODPATH . "tradersystem/admin_class.php");
Registry::set('TraderSystem', new TraderSystem());
include_once(MODPATHF . 'tradersystem/define_by_api.php');

if($_POST){
	$api_key = $_SESSION['securekey'];
	$backward = $_POST['backward'];
    $backward_sms_fail = $_POST['backward_sms_fail'];
	$command = $_POST['command'];
	//req-token
	switch ($command)
	{
		case 'req-token':
			switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id'])
			{
				case 1:
					$data = array('request_activity'=>'reset_secure');
					$reqopt_result = $usernc->reqotp($data,$api_key);
					$token_key = $reqopt_result['message']['key_token'];
					$token_code = $reqopt_result['message']['key_code'];
					$send_to = $reqopt_result['message']['send_to'];
					//start send email
					require_once (BASEPATH . "lib/class_mailer.php");
					$row = Core::getRowById(Content::eTable, 23);
					$body = str_replace(array('[USERNAME]','[KEY]','[TOKEN]','[URL]','[SITE_NAME]'),array($send_to,$token_key,$token_code,SITEURL,Registry::get("Core")->site_name), $row->{'body' . Lang::$lang});
					$newbody = cleanOut($body);
					$mailer = Mailer::sendMail();
					$message = Swift_Message::newInstance()
					->setSubject($row->{'subject' . Lang::$lang})
					->setTo(array($send_to => $send_to))
					->setFrom(array(Registry::get("Core")->site_email =>Registry::get("Core")->site_name))
					->setBody($newbody, 'text/html');
					$mailer->send($message);
					//end send email
					$_SESSION['secure_reset'] = 1;
					header("Location: ".$backward);
				break;
				case 2:
					$_SESSION['secure_reset'] = 1;
					header("Location: ".$backward);
				break;
				case 3:
					$data = array('request_activity'=>'reset_secure');
					$reqopt_result = $usernc->reqotp($data,$api_key);
					$token_key = $reqopt_result['message']['key_token'];
					$token_code = $reqopt_result['message']['key_code'];
					$send_to = $reqopt_result['message']['send_to'];
					$_SESSION['secure_reset'] = 1;
                  
                  $res = Registry::get("TraderSystem")->send_sms_infobip("OTP for reset SMS Security referrence token ".$token_key." is ".$token_code,$send_to);
          
                  $_SESSION['reset_sms_key_token'] = $token_key;
                    
					//$r = http_build_query($reqopt_result); // use while demo not send real SMS
					//header("Location: ".$backward."&".$r); // use while demo not send real SMS
					header("Location: ".$backward);
				break;
			}
		break;
		case 'verify-token':
			//$otp_token = $_POST['reset_token'];
			$otp_token = $_POST['reset_token'];
			$data = array('key_code'=>$otp_token);
			$reset_result = $usernc->resetsecure($data,$api_key);
			if($reset_result['isSuccess'] == 1){
				$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = null;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
				unset($_SESSION['secure_reset']);
				$r = http_build_query($reset_result);
				header("Location: ".$backward."&".$r);
			} else {
			   
				if ($reset_result['sendNewSMS'] == 1) {

                    // create sms and sent to user
                    
                    $data = array('request_activity'=>'reset_secure');
                  $reqopt_result = $usernc->reqotp($data,$api_key);
                  $token_key = $reqopt_result['message']['key_token'];
                  $token_code = $reqopt_result['message']['key_code'];
                  $send_to = $reqopt_result['message']['send_to'];
                  $_SESSION['secure_reset'] = 1;
                  
                  $res = Registry::get("TraderSystem")->send_sms_infobip("OTP for reset SMS Security referrence token ".$token_key." is ".$token_code,$send_to);
          
                  $_SESSION['reset_sms_key_token'] = $token_key;
                  
                    // end sent new otp
				    
				} 
                
                header("Location: ".$backward_sms_fail."&resetsms=fail&new_sms=".$reset_result['sendNewSMS']);
				
			}
		break;
	}
} else {
	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
}
?>