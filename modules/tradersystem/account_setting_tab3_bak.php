<form class="form-horizontal" id="prolific_form3" name="prolific_form3" method="post">
										<div class="form-group">
											<div class="col-md-12">
												<div class="heading_b"><?php echo Lang::$word->_BANK_DETAIL;?></div>
											</div>
										</div>
										<div class="form-group">
											<label for="profile_username" class="col-md-2 control-label">Bank Name</label>
											<div class="col-md-10">
											    <?php
											    $aBank = $tradersystem->call_curl_get("bank","GET",'');
	                                           if(isset($aBank['isSuccess']) && $aBank['isSuccess'] ==1){
	                                                    $aBank = $aBank;
	                                           } else {
	                                                    $aBank = array();
                                                }
											   
											   
											    ?>
											    
											    <select id="bank_name" name="bank_name"class="form-control" onchange="set_bankother();">
												<?php
                                                    echo $tradersystem->list_select_option_by_array($aBank,$usr_dtl['bank_name'],"bank_code","bank_name",Lang::$word->_PLEASE_SELECT_DATA);
                                                ?>
                                                <option value="other"> Other Bank </option>
                                                </select>
                                                <input type="text" name="bank_name_other" class="form-control"  onblur="set_new_bank_name();"  id="bank_name_other" placeholder="bank name other" value="" style="display:none;">
                                                
											</div>
										</div>
										<div class="form-group">
											<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_COUNTRY;?></label>
											<div class="col-md-10">
												<!--<select id="bank_country" name="bank_country"class="form-control" >-->
													<select id="s3_ext_value" name="bank_country" class="form-control" >
													<?php
													echo $tradersystem->list_select_option_by_table("bk_country_code",$usr_dtl['bank_country'],"country_abb","country","",Lang::$word->_PLEASE_SELECT_COUNTRY_CODE);
													?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_ACCOUNT_TYPE;?></label>
											<div class="col-md-10">
												<label class="radio-inline">
													<input type="radio" name="bank_account_type" id="inline_optionsRadios1"  <?php echo getChecked($usr_dtl['bank_account_type'], "saving");?> value="saving">
													<?php echo Lang::$word->_SAVING;?>
												</label>
												<label class="radio-inline">
													<input type="radio" name="bank_account_type" id="inline_optionsRadios2" value="current" <?php echo getChecked($usr_dtl['bank_account_type'], "current");?>>
													<?php echo Lang::$word->_CURRENT;?>
												</label>
											</div>
										</div>
										<div class="form-group">
											<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_ACCOUNT_NUMBER;?></label>
											<div class="col-md-10">
												<input type="text" id="bank_account_number" name="bank_account_number" class="form-control" value="<?php echo $usr_dtl['bank_account_number'];?>">
											</div>
										</div>
										<div class="form-group">
											<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_SWIFT;?></label>
											<div class="col-md-10">
												<input type="text" name="bank_swift"id="bank_swift" class="form-control" value="<?php echo $usr_dtl['bank_swift'];?>">
											</div>
										</div>
										<div class="form-group">
                                            <label for="bank_routing_number" class="col-md-2 control-label"><?php echo Lang::$word->_ROUTING_NUMBER;?></label>
                                            <div class="col-md-10">
                                                <input type="text" id="bank_routing"name="bank_routing" class="form-control" value="<?php echo $usr_dtl['bank_routing'];?>">
                                            </div>
                                        </div>
										<div class="form-group">
											<label for="bank_iban" class="col-md-2 control-label"><?php echo Lang::$word->_IBAN;?></label>
											<div class="col-md-10">
												<input type="text" name="bank_iban" id="bank_iban"class="form-control" value="<?php echo $usr_dtl['bank_iban'];?>">
											</div>
										</div>
										<div class="form-group">
											<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_BANK_DOCUMENT;?></label>
											<div class="col-md-10 has-warning">
											    
											    <?php if ($isShowFrm_bank == false){?>
                                                    <label class="control-label" for="inputWarning"><?php echo $isShowFrm_bank_text;?></label>
                                                    <a href="<?php echo UPLOADURL;?>file_doc/<?php echo $usr_dtl['bank_file_url'];?>" target="_blank"> <?php echo $usr_dtl['bank_file_url'];?></a>
                                                <?} else {?>
                                                    <label class="control-label" for="inputWarning"><?php echo Lang::$word->_DOCUMENT_CONTROL_INFO1 .$isShowFrm_bank_text;?></label>
                                                    <input type="file" name="file_bank" class="form-control">
                                    
                                                <?}?>
                                                <!--
												<label class="control-label" for="inputWarning">The maximum file size is 5 MB. Only file Type (JPG, GIF, PNG, PDF) are allowed.</label>
												<input type="file" id="user_lname" class="form-control" value="Zieme">
												-->
											</div>
										</div>
										
										<div class="text-center">
										    <input name="doProfile_bank" type="hidden" value="1">
                                            <button data-url="/ajax/controller.php" data-frm="prolific_form3"  type="button" name="dosubmit" class="prolific button btn btn-success">
                                                <i class="fa fa-save"></i><?php echo Lang::$word->_UPDATE_BANK_DETAIL;?></button>
											
										</div>
									</form>