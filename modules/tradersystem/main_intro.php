<div class="form-group">
	<label for="profile_username" class="col-md-2 control-label">Title</label>
	<div class="col-md-10">
		<input type="text" name="username" id="profile_username" readonly="" class="form-control" value="<?php echo $usr->title_name;?>">
	</div>
</div>

<div class="form-group">
	<label for="user_password" class="col-md-2 control-label"><?php echo Lang::$word->_UR_FNAME;?></label>
	<div class="col-md-10">
		<input type="email" id="user_password" readonly="" class="form-control" value="<?php echo $usr->fname;?>">
	</div>
</div>
<div class="form-group">
	<label for="user_password" class="col-md-2 control-label"><?php echo Lang::$word->_UR_MNAME;?></label>
	<div class="col-md-10">
		<input type="email" id="user_password" readonly="" class="form-control" value="<?php echo $usr->middle_name;?>">
	</div>
</div>

<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Surname</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->lname;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Gender</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->gender;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label"><?php echo Lang::$word->_UR_EMAIL;?></label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->email;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label"><?php echo Lang::$word->_COUNTRY_CODE;?></label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->country_code;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label"><?php echo Lang::$word->_MOBILE_PHONE_NUMBER;?></label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->mobile;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Residential Address (Not PO Box)</label>
	<div class="col-md-10">
		<textarea name="boi" id="exmpl_textarea_autosize" cols="30" rows="4" class="form-control autosize">
			<?php echo $usr->addr1 . " " . $usr->addr2;?>
		</textarea>
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Post code</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->post_code;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">State / Province </label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->state;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Country</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->country;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Country of Citizenship</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->country_citizen;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Country of Birth</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->country_birth;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Date of Birth</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->birth_dd."-".$usr->birth_mm."-".$usr->birth_yy;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-12 control-label">Purpose of opening an account with Monex AU</label>
	<div class="col-md-12">
		<input name="fname" type="text" value="<?php echo $usr->purpose_monex_au;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">If "Other", please specify.</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->fnpurpose_otherame;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Employment Status</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->employment_status;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">If "Other", please specify.</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->employment_other;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Occupation</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->occupation;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">If "Other", please specify.</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->occupation_other;?>" id="fname" class="form-control" >
	</div>
</div>
<div class="form-group">
	<label for="user_fname" class="col-md-2 control-label">Country of Employer</label>
	<div class="col-md-10">
		<input name="fname" type="text" value="<?php echo $usr->country_employer;?>" id="fname" class="form-control" >
	</div>
</div>

