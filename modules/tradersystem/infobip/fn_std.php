<?php
session_start();
// Common constants:

define('PUSH_LOG_DIRECTORY', MODPATHF . 'tradersystem/infobip/pushloggdnid_log');
if(!is_dir(PUSH_LOG_DIRECTORY)) {
    echo 'Please create a writable directory named222 "' . PUSH_LOG_DIRECTORY . '"';
    die();
}

if(!is_writable(PUSH_LOG_DIRECTORY)) {
    echo 'The directory named "' . PUSH_LOG_DIRECTORY . '" is not writable';
    die();
}
#require_once 'yapd/dbg.php';

// Include configuration constants:


// Checking preconditions:
if(!defined('ONEAPI_LIBRARY_PATH')) {
    echo 'Please fill ONEAPI_LIBRARY_PATH in conf.php';
    die();
}
if(!defined('USERNAME') || !USERNAME) {
    echo 'Please fill USERNAME in conf.php';
    die();
}
if(!defined('PASSWORD') || !PASSWORD) {
    echo 'Please fill PASSWORD in conf.php';
    die();
}

// Include OneApi library:
require_once MODPATHF . 'tradersystem/infobip/oneapi/client.php';


function send_sms($from='',$to='',$messagebody='',$notifyURL='')
{
	$smsClient 		= new SmsClient(USERNAME, PASSWORD);
	$message 		= new SMSRequest();
	$message->senderAddress = ( $from == '' ) ? SENDER_NAME : $from;
	$message->address 		= $to;
	$message->message 		= $messagebody;
	$message->notifyURL 	= ( $from == '' ) ? NOTIFY_URL : $notifyURL;
	
	try {
    	$result = $smsClient->sendSMS($message);
	    return $result;;
	} catch(Exception $e) {

	    return $e->getMessage();
	}
	
}
function check_delivery_status($clientCorrelator)
{
	$smsClient = new SmsClient(USERNAME, PASSWORD);

	try {
	    $result = $smsClient->queryDeliveryStatus($clientCorrelator);
		$result = json_encode($result,true);
	    $result = json_decode($result,true);
	    return $result;
	} catch(Exception $e) {
	    echo $e->getMessage();
	    return;
	}
}





// ============================================================
// The following are just utility function for this sample app:
// ============================================================

function decodeFormParam($p) {
    return get_magic_quotes_gpc() ? stripslashes($p) : $p;    
}

function showFormMessage() {
    if(@$_GET['alert']) {
        echo '<div style="background-color:orange;margin:10px;padding:10px;">';
        echo decodeFormParam($_GET['alert']);
        echo '</div>';
    } elseif(@$_GET['success']) {
        echo '<div style="background-color:yellow;margin:10px;padding:10px;">';
        echo decodeFormParam($_GET['success']);
        echo '</div>';
    }
}

function redirectWithFormError($url, $message) {
    redirectToForm(false, $url, $message);
}

function redirectWithFormSuccess($url, $message) {
    redirectToForm(true, $url, $message);
}

function redirectToForm($success, $url, $message) {
    if(DEBUG)
         $message .= "<hr/>" . implode('<br/>', Logs::getLogs());

    $message .= '<hr><a href="index.php">home</a>';

    if($success)
        header('Location: ' . $url . '?success=' . urlencode($message));
    else
        header('Location: ' . $url . '?alert=' . urlencode($message));

    $_SESSION['params'] = array();
    foreach($_GET as $key => $value)
        $_SESSION['params'][$key] = $value;
    foreach($_POST as $key => $value)
        $_SESSION['params'][$key] = $value;

    die();
}

function getFormParam($name) {
    return isset($_REQUEST[$name]) ? decodeFormParam($_REQUEST[$name]) : (
            isset($_SESSION['params'][$name]) ? $_SESSION['params'][$name] : ''
        );
}
