<?php
  /**
   * User
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: user.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
	define("_VALID_PHP", true);

  	require_once("../../init.php");
    
   	require_once (MODPATH . "tradersystem/admin_class.php");
   
   	Registry::set('TraderSystem', new TraderSystem());
   	include_once(MODPATHF . 'tradersystem/define_by_api.php');
   
   	$action = (isset($_POST['command']) && $_POST['command'] != '') ? $_POST['command'] : '';
	switch($action) {
		case 'doSecureSetup_part1': ajax_doSecureSetup_part1();break;
		case 'doRequestotp_google':ajax_doRequestotp_google();break;
		case 'doSecureSetup_reset':ajax_doSecureSetup_reset();break;
		default : break;
	}
	
	function data_uri($file, $mime) 
	{  
	  $contents = file_get_contents($file);
	  $base64   = base64_encode($contents); 
	  return ('data:' . $mime . ';base64,' . $base64);
	}
	function ajax_doSecureSetup_reset()
	{
		Filter::checkPost('token', "Please input token.");
		if (empty(Filter::$msgs)) {
			$result = Registry::get("TraderSystem") ->call_curl('resetsecure',"POST",array('key_code'=>$_POST['token'],'cmd'=>'resetsecure'));
			if($result['isSuccess'] == 1){
				$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = null;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
				$json['status'] 	= 'success';
				$json['isRedirect'] = 'yes';
				$json['message'] 	= Filter::msgOk_gob(Lang::$word->_SS_UPDATED, false);
				
			} else {
				$json['status'] 	= 'error';
				$json['message'] 	= Filter::msgError_gob($result['errMessage'], false);
			}
		} else {
			$json['message'] = Filter::msgStatus_gob();
		}
		
	    print json_encode($json);
	}
 	function ajax_doRequestotp_google()
	{
		$data = array('tool_id'=>$option);
		$result = Registry::get("TraderSystem") ->call_curl('secure_tool',"POST",array('tool_id'=>2,'cmd'=>'secure_tool'));
		
		if($result['isSuccess'] == 1){
			if($_SERVER['HTTPS'] == 'on'){
				$protocol = 'https';
			} else {
				$protocol = 'http';
			}
			$json['status'] 	= 'success';
			$json['auth_token'] = $result['message'];
			/*$nameuser = explode(" ",$_SESSION['name']);
			$nameuser = join("+",$nameuser);
			 * 
			 */
			$nameuser = $_SESSION['email'];
			$url = $protocol."://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/".Registry::get("Core")->site_name.":".$nameuser."?secret=".$result['message'];
			
			$json['imgsrc'] = data_uri($url,'image/png');
			
		} else {
			$json['status'] 	= 'error';
		}
		print json_encode($json);
	}
	
	function ajax_doSecureSetup_part1()
	{
		Filter::checkPost('secure_type', "Please select security type.");
		if (empty(Filter::$msgs)) {
			$secure_type = $_POST['secure_type'];
			if ($secure_type == '1') {
				if (empty(Filter::$msgs)) {
					
					$result = Registry::get("TraderSystem") ->call_curl("verify_secure_tool","POST",array('cmd'=>'verify_secure_tool','secure_tool_id'=>$_POST['secure_type'],'token'=>'token','token_ref'=>'token'));
					
					if($result['isSuccess'] == 1){
						$json['status'] 	= 'success';
					    $json['isRedirect'] = 'yes';
						$json['message'] 	= Filter::msgOk_gob(Lang::$word->_SS_UPDATED, false);
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 1;
						$_SESSION['usr_dtl']['user_info']['enable_email'] = 1;
						$_SESSION['usr_dtl']['user_info']['email_verified'] = 1;
						
						Security::writeLog(Lang::$word->_USER . ' ' . $_SESSION['uid'].':'.$_SESSION['name']. 'setting mail', "user", "no", "user");	  
					} else {
						$json['status'] 	= 'error';
						$json['message'] 	= Filter::msgError_gob($result['errMessage'], false);
					}
				} else {
					$json['message'] = Filter::msgStatus_gob();
				}
				
			} elseif ($secure_type == '2') {
				Filter::checkPost('token_google', "Please input token.");
				Filter::checkPost('auth_token', "Data not found.");
				if (empty(Filter::$msgs)) {
					
					$result = Registry::get("TraderSystem") ->call_curl("verify_secure_tool","POST",array('cmd'=>'verify_secure_tool','secure_tool_id'=>$_POST['secure_type'],'token'=>$_POST['token_google'],'token_ref'=>$_POST['auth_token']));
					
					if($result['isSuccess'] == 1){
						$json['status'] 	= 'success';
					    $json['isRedirect'] = 'yes';
						$json['message'] 	= Filter::msgOk_gob(Lang::$word->_SS_UPDATED, false);
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] = 2;
						$_SESSION['usr_dtl']['user_info']['google_auth_use'] = 1;
						$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = 1;
						$_SESSION['usr_dtl']['user_info']['google_auth_key'] = $result['message'];
						
						Security::writeLog(Lang::$word->_USER . ' ' . $_SESSION['uid'].':'.$_SESSION['name']. 'setting google', "user", "no", "user");	  
					} else {
						$json['status'] 	= 'error';
						$json['message'] 	= Filter::msgError_gob($result['errMessage'], false);
					}
				} else {
					$json['message'] = Filter::msgStatus_gob();
				}
			} elseif ($secure_type == '3') {
				Filter::checkPost('token', "Please input token.");
				Filter::checkPost('key_verify', "Please request otp.");
				if (empty(Filter::$msgs)) {
					
					$result = Registry::get("TraderSystem") ->call_curl("verify_secure_tool","POST",array(
					'cmd'=>'verify_secure_tool',
					'secure_tool_id'=>$_POST['secure_type'],
					'token'=>$_POST['token'],
					'token_ref'=>$_POST['key_verify'],
					'mobilecode'=>$_POST['mobile_code'],
					'mobile'=>$_POST['mobile'],
					
					));
					
					if($result['isSuccess'] == 1){
						$json['status'] 	= 'success';
					    $json['isRedirect'] = 'yes';
						$json['message'] 	= Filter::msgOk_gob(Lang::$word->_SS_UPDATED, false);
						$_SESSION['usr_dtl']['user_info']['secure_tool_id'] 	= 3;
						$_SESSION['usr_dtl']['user_info']['enable_sms'] 		= 1;
						$_SESSION['usr_dtl']['user_info']['sms_verified'] 		= 1;
						$_SESSION['usr_dtl']['user_info']['mobile']	            = $_POST['mobile'];
						$_SESSION['usr_dtl']['user_info']['mobile_code']	    = $_POST['mobile_code'];
						
						Security::writeLog(Lang::$word->_USER . ' ' . $_SESSION['uid'].':'.$_SESSION['name']. 'setting sms opt', "user", "no", "user");	  
					} else {
						$json['status'] 	= 'error';
						$json['message'] 	= Filter::msgError_gob($result['errMessage'], false);
					}
				} else {
					$json['message'] = Filter::msgStatus_gob();
				}
			}
		} else {
			$json['message'] = Filter::msgStatus_gob();
		}
		
	    print json_encode($json);
	}
	
 	
 	

?>