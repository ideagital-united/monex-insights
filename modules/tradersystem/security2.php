<?php
define("_VALID_PHP", true);
require_once("init.php");
$api_key = $_SESSION['securekey'];
$secure_tool_id = $_SESSION['usr_dtl']['user_info']['secure_tool_id'];
$sms_verified = $_SESSION['usr_dtl']['user_info']['sms_verified'];
$email_verified = $_SESSION['usr_dtl']['user_info']['email_verified'];
$google_auth_verified = $_SESSION['usr_dtl']['user_info']['google_auth_verified'];

$secure_setup_form = BASEPATH . 'modules/tradersystem/secure/secure_setup_form.php';
$secure_detail_form = BASEPATH . 'modules/tradersystem/secure/secure_detail_form.php';
$verify_secure_form_email = BASEPATH . 'modules/tradersystem/secure/verify_secure_form_email.php';
$verify_secure_form_google = BASEPATH . 'modules/tradersystem/secure/verify_secure_form_google.php';
$verify_secure_form_sms = BASEPATH . 'modules/tradersystem/secure/verify_secure_form_email_sms.php';

print_r($_POST);

if($_POST['secure_setup_form'] == 1){
	$tool_id = $_POST['radio_switch1'];
	$data = array('tool_id'=>$tool_id);
	$result = $usernc->set_secure_tool($data,$api_key);
	//print_r($result);
} elseif ($_POST['verify_secure_form'] == 1) {
	
	$token = $_POST['token'];
	$data = array('token'=>$token);
	$result = $usernc->verify_secure_tool($data,$api_key);
	if($result['isSuccess'] == 1){
		$msg = $result['message'];
		switch ($msg){
			case 'Verified by email.':
				$sms_verified = null;
				$email_verified = 1;
				$google_auth_verified = null;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = 1;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
				break;
			case 'Verified by Google2FA.':
				$sms_verified = null;
				$email_verified = null;
				$google_auth_verified = 1;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = 1; 
				break;
			case 'Verified by SMS.':
				$sms_verified = 1;
				$email_verified = null;
				$google_auth_verified = null;
				$_SESSION['usr_dtl']['user_info']['sms_verified'] = 1;
				$_SESSION['usr_dtl']['user_info']['email_verified'] = null;
				$_SESSION['usr_dtl']['user_info']['google_auth_verified'] = null; 
				break;				
		}
	} else {
		$msg = $result['errMessage'];
		echo $request->msgError_gob($msg);
	}
} elseif ($_POST['request_new_email_token'] == 1){
	//do sent token
	echo "dummy text : resend token again";
	
}

if($sms_verified == 1||$email_verified == 1||$google_auth_verified == 1){
	$status = 1; //all set, do nothing
} else {
	if($secure_tool_id == 1||$secure_tool_id == 2||$secure_tool_id == 3){
		$status = 3; //resume.
	} else {
		$status = 2; //nonset do all from start
	}
}
/* echo "<pre>";
print_r($_SESSION);
echo "</pre>"; */
?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					
<?php
switch ($status) {
    case 1:
		include($secure_detail_form);
        break;
    case 2:
        include($secure_setup_form);
        break;
    case 3:
		switch ($secure_tool_id){
			case 1:
				include($verify_secure_form_email);
				break;
			case 2:
				include($verify_secure_form_google);
				break;
			case 3:
				include($verify_secure_form_sms);
				break;
		}
        break;
}
?>
				</div>
			</div>
		</div>
	</div>
</div>
