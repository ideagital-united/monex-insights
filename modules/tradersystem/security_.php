<?php


?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3><?php echo Lang::$word->_SECURITY_TYPE;?></h3></div>
					<form class="form-horizontal" id="prolific_form" name="prolific_form" method="post">
					<div class="panel-body">
						<?php echo Lang::$word->_SECURITY_INFO;?>
						<div class="row">
							<div class="col-sm-12">
								<div class="row bs_switch_wrapper">
									<div class="col-sm-12 col-md-12">
										<h4><?php echo Lang::$word->_EMAIL;?></h4>
										<input type="radio" class="bs_switch_radio" name="type_otp" value="1" >
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO2;?></span>
										
									</div>
									<?php if(secure_sms == 1){?>
									<div class="col-sm-12 col-md-12">
										<h4><?php echo Lang::$word->_MOBILE_PHONE_SMS;?></h4>
										<input type="radio" class="bs_switch_radio" name="type_otp" value="3">
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO4;?></span>
											<div class="form-group stepsms">
												<div class="col-md-12">
													<div class="heading_b"><?php echo Lang::$word->_PLEASE_INSERT_YOUR_MOBILE_NUMBER;?></div>
												</div>
											</div>
											<div class="form-group stepsms">
												<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_YOUR_PHONE_NUMBER;?></label>
												<div class="col-md-10 ">
													<input type="text" id="phone" name="phone" class="form-control" value="+6681452536">
												</div>
											</div>
											<div class="row stepsms" >
												<div class="text-center">
													<button data-url="/modules/tradersystem/ajax_user.php" type="button" id="do_request_otp" name="do_request_otp" class="prolific button btn btn-success">
														<i class="fa fa-save"></i><?php echo Lang::$word->_REQUEST_OPT;?></button>
												</div>
											</div>
											
											<div class="form-group stepsms_step2">
												<div class="col-md-12">
													<div class="heading_b"><?php echo Lang::$word->_PLEASE_INSERT_ENTER_OPT;?></div>
												</div>
											</div>
											<div class="form-group stepsms_step2">
												<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_OTP;?></label>
												<div class="col-md-10 ">
													<input type="hidden" value="" name="code_key" id="code_key">
													<input type="text" id="code_otp" name="code_otp" class="form-control" value="" placeholder="OPT-CODE">
												</div>
											</div>
											<div class="row stepsms_step2" >
												<div class="text-center">
													<button data-url="/modules/tradersystem/ajax_user.php" type="button" id="do_check_otp" name="do_check_otp" class="prolific button btn btn-success">
														<i class="fa fa-save"></i><?php echo Lang::$word->_SAVE_SETTING;?></button>
												</div>
											</div>
										
									</div>
									<?php }?>
									<?php if (secure_google == 1) {?>
									<div class="col-sm-12 col-md-12">
										<h4><?php echo Lang::$word->_MOBILE_PHONE_GOOGLE_AUTH;?></h4>
										<input type="radio" class="bs_switch_radio" name="type_otp" value="2">
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO5;?></span>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
						<div class="row stepsave" >
							<div class="text-center">
								<input name="doSecure_user" type="hidden" value="1">
								<button data-url="/modules/tradersystem/ajax_user.php" type="button" name="dosubmit" class="prolific button btn btn-success">
									<i class="fa fa-save"></i><?php echo Lang::$word->_SAVE_SETTING;?></button>
							</div>
						</div>
					</div>
					</form>
					<div class="panel-heading"><h3><?php echo Lang::$word->_OPTIONAL_SECURITY_SETTING;?></h3></div>
					<form class="form-horizontal" id="prolific_form2" name="prolific_form2" method="post">
					<div class="panel-body">
						<!-- start pincode -->
						<div class="row">
							
						<div class="col-sm-12 col-md-12" id="pincode_setting_radio" >
										<h4><?php echo Lang::$word->_PINCODE;?></h4>
										<input type="radio" class="bs_switch_radio" name="radio_switch1" value="2">
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO3;?></span>
										
											<div class="form-group">
												<div class="col-md-12">
													<div class="heading_b"><?php echo Lang::$word->_PLEASE_CRATE_YOUR_PIN;?></div>
												</div>
											</div>
											<div class="form-group">
												<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_YOU_CREATE_PINCODE;?></label>
												<div class="col-md-10 ">
													<input type="text" id="user_lname" class="form-control" value="Zieme">
												</div>
											</div>
											<div class="form-group">
												<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_PLEASE_RE_ENTER_PINCODE;?></label>
												<div class="col-md-10 ">
													<input type="text" id="user_lname" class="form-control" value="Zieme">
												</div>
											</div>
									</div></div>
						<!-- end pin code -->
						<div class="row">
							<div class="col-sm-12">
								<div class="row bs_switch_wrapper">
									<div class="col-sm-12 col-md-12">
										<h4><?php echo Lang::$word->_IP_CHECK;?></h4>
										<input type="radio" class="bs_switch bs_switch_radio" name="radio_switch2" value="option 1">
										<span class="help-block"><?php echo Lang::$word->_SECURITY_INFO6;?>
										<?php echo Lang::$word->_SECURITY_INFO7;?>
										
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="text-center">
								<input name="doSecure_user" type="hidden" value="1">
								<button data-url="/modules/tradersystem/ajax_user.php" data-frm="prolific_form2"  type="button" name="dosubmit" class="prolific button btn btn-success">
									<i class="fa fa-save"></i><?php echo Lang::$word->_SAVE_SETTING;?></button>
								
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php
include "alert_msg.php";
?>
<script>
window.onload = function () { 
	$('.stepsms,.stepsms_step2').hide();
	$('input[name="type_otp"]').change(function() {
		var chkval = $(this).val();
		if (chkval == 3) {
			$('.stepsms').show();
			$('.stepsave,.stepsms_step2').hide();
			
			
		} else {
			$('.stepsms,.stepsms_step2').hide();
			$('.stepsave').hide();
		}
	    
	});
	
	
	$('#do_check_otp').click(function(){
		$('.modal').modal('hide');
        $('#msg-loading').modal("show");
        var otp = $('#code_otp').val();
        var otp_key = $('#code_key').val();
		$.ajax({
		  url		: SITEURL + "/modules/tradersystem/ajax_user.php" ,
		  type		: 'POST',
		  data		:{doRequestotp_SMS : 1,phone:fixphone},
		  dataType 	:'json'
		}).done(function(json) {
		  	$("#msgholder").html(json.message);
		  	$('#msg-loading').modal("hide");
		  	$('#msg-showwarningmsg').modal("show");
		  	if (json.status == 'success') {
		  		$('#code_key').val(json.key_code);
		  		$('.stepsms').hide();
		  		$('.stepsms_step2').show();
		  	} else {
		  		
		  	}
		  	
		  	  
		});
	});
	$('#do_request_otp').click(function(){
		$('.modal').modal('hide');
        $('#msg-loading').modal("show");
        var fixphone = $('#phone').val();
		$.ajax({
		  url		: SITEURL + "/modules/tradersystem/ajax_user.php" ,
		  type		: 'POST',
		  data		:{doRequestotp_SMS : 1,phone:fixphone},
		  dataType 	:'json'
		}).done(function(json) {
		  	$("#msgholder").html(json.message);
		  	$('#msg-loading').modal("hide");
		  	$('#msg-showwarningmsg').modal("show");
		  	if (json.status == 'success') {
		  		$('#code_key').val(json.key_code);
		  		$('.stepsms').hide();
		  		$('.stepsms_step2').show();
		  	} else {
		  		
		  	}
		  	
		  	  
		});
	});
}
</script>