<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">

<?php

if($_SERVER['HTTPS'] == 'on'){
    $protocol = 'https';
} else {
    $protocol = 'http';
}

$backward = $protocol."://".$_SERVER['HTTP_HOST']."/".$_GET['url']."?subpage=".$_GET['subpage'];

$backward2 = $protocol."://".$_SERVER['HTTP_HOST']."/".$_GET['url']."?subpage=security_new";
$_SESSION['secure_reset'] = 1;
if($_SESSION['secure_reset'] == 1) {
    
	switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id'])
	{
		case 1:
			?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_RESETTING_SECURITY; ?></h3></div>
				<div class="col-md-12"><span class="help-block"></span></div>
				<form class="form-horizontal" id="prolific_form" name="resetseure_send" method="post" action="/modules/tradersystem/resetproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php echo Lang::$word->_PLEASE_CHECK_EMAIL_OTP; ?></span>
				</div>						
				</div>
				
				<div class="form-group">
				<label for="profile_username" class="col-md-3 control-label"><?php echo Lang::$word->_PLEASE_INSERT_ENTER_OPT; ?></label>
				<div class="col-md-5">
				<div class="input-group">
				<span class="input-group-addon" style="text-align: left"><?php echo Lang::$word->_OTP; ?></span>
				<input class="form-control" name="reset_token" id="reset_token" type="text" placeholder="<?php echo Lang::$word->_OTP; ?>">
				</div>
				</div>
				</div>
				
				<div class="text-left">
				<input type="hidden" name="backward" value="<?php echo $backward2; ?>">
				<input name="command" type="hidden" value="verify-token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
				</div>
				</form>
			<?php
		break;
		case 2:
			?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_RESETTING_SECURITY; ?></h3></div>
				<div class="col-md-12"><span class="help-block"></span></div>
				<form class="form-horizontal" id="prolific_form" name="resetseure_send" method="post" action="/modules/tradersystem/resetproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php echo Lang::$word->_USE_GEN_OTP_FROM_YOUR_REGIS_DIVICE; ?></span>
				</div>						
				</div>
				<div class="form-group">
				<label for="reset_token" class="col-md-3 control-label"><?php echo Lang::$word->_PLEASE_INSERT_ENTER_OPT; ?></label>
				<div class="col-md-5">
				<div class="input-group">
				<span class="input-group-addon" style="text-align: left"><?php echo Lang::$word->_OTP; ?></span>
				<input class="form-control" name="reset_token" id="reset_token" type="text" placeholder="<?php echo Lang::$word->_OTP; ?>">
				</div>
				</div>
				</div>
				<div class="text-left">
				<input type="hidden" name="backward" value="<?php echo $backward2; ?>">
				<input name="command" type="hidden" value="verify-token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
				</div>
				</form>
			<?php
		break;
		case 3:
			?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_RESETTING_SECURITY; ?></h3></div>
				<div class="col-md-12"><span class="help-block"></span></div>
				<form class="form-horizontal" id="prolific_form" name="resetseure_send" method="post" action="/modules/tradersystem/resetproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php echo Lang::$word->_PLEASE_CHECK_YOUR_SMS_AFTER_REQ_OTP; ?></span>
				</div>						
				</div>
				
				<?php if ($_GET['resetsms'] == 'fail') { ?>
				<div class="form-group">
				    <div class="col-md-4"> </div>
				    <div class="col-md-8"><span style="color:#FF0000;text-align: center;"><?php echo Lang::$word->_WRONG_OTP; ?></span></div>
				</div>
				<?php } ?>
				<div class="form-group">
				<label for="reset_token" class="col-md-4 control-label"><?php echo Lang::$word->_PLEASE_INSERT_ENTER_OPT; ?> <?php echo Lang::$word->_WITH_REFER_TOKEN; ?> <strong><font color='#FF0000'><?php echo $_SESSION['reset_sms_key_token']; ?></font></strong></label>
				<div class="col-md-8">
				<div class="input-group">
				<span class="input-group-addon" style="text-align: left"><?php echo Lang::$word->_OTP; ?></span>
				<input class="form-control" name="reset_token" id="reset_token" type="text" placeholder="<?php echo Lang::$word->_OTP; ?>">
				</div>
				</div>
				</div>
				<div class="text-left">
				<input type="hidden" name="backward" value="<?php echo $backward2; ?>">
				<input type="hidden" name="backward_sms_fail" value="<?php echo $backward; ?>">
				<input name="command" type="hidden" value="verify-token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_SUBMIT;?></button>
				</div>
				</form>
			<?php
		break;
	}

} else {
    
	if( ($_SESSION['usr_dtl']['user_info']['sms_verified'] == 1) || ($_SESSION['usr_dtl']['user_info']['email_verified'] == 1) || ($_SESSION['usr_dtl']['user_info']['google_auth_verified'] == 1)) {
		//echo "user full mode";
		switch ($_SESSION['usr_dtl']['user_info']['secure_tool_id'])
		{
			case 1:
				?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_RESET_SECURE_BY_EMAIL; ?></h3></div>
				<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/resetproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php echo Lang::$word->_SEND_RESET_TOKEN_EMAIL; ?></span>
				</div>
				</div>
				<div class="text-center">
				<input type="hidden" name="backward" value="<?php echo $backward;?>">
				<input name="command" type="hidden" value="req-token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_PROCEED; ?></button>
				</div>
				</form>
				<?php
			break;
			case 2:
				?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_RESET_SECURE_BY_GOOGLE_AUTH; ?></h3></div>
				<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/resetproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php Lang::$word->_PROCEED_TO_RESET_GOOGLE_AUTH; ?></span>
				</div>
				</div>
				<div class="text-center">
				<input type="hidden" name="backward" value="<?php echo $backward;?>">
				<input name="command" type="hidden" value="req-token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_PROCEED; ?></button>
				</div>
				</form>
				<?php
			break;
			case 3:
				?>
				<div class="panel-heading"><h3><?php echo Lang::$word->_RESET_SECURE_BY_SMS; ?></h3></div>
				<form class="form-horizontal" id="prolific_form" name="input_token" method="post" action="/modules/tradersystem/resetproc.php">
				<div class="form-group">
				<div class="col-md-12">
				<span class="help-block"><?php echo Lang::$word->_SEND_RESET_TOKEN_TO_YOUR_MOBILE; ?></span>
				</div>
				</div>
				<div class="text-center">
				<input type="hidden" name="backward" value="<?php echo $backward;?>">
				<input name="command" type="hidden" value="req-token">
				<button type="summit" name="Submit" class="prolific button btn btn-success"><i class="fa fa-save"></i> <?php echo Lang::$word->_PROCEED; ?></button>
				</div>
				</form>
				<?php
			break;
		}
	} else {
		echo Lang::$word->_PLEASE_CLICK_RESET_SECURE_BUTTON;
	}
}
?>
				</div>
			</div>
		</div>
	</div>
</div>