<?php
  /**
   * Moneybookers Form
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: form.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>




<div class="prolific button">
  <form action="https://www.moneybookers.com/app/payment.pl" method="post" id="mb_form" name="mb_form">
    <input class="displaypay" typepay="skrill" type="image" src="<?php echo SITEURL.'/modules/tradersystem/gateways/skrill/skrill_big.png';?>" name="button" title="Pay With Moneybookers" alt="" />
   
    <!-- start form -->
    <div class="display_showhide" id="display_skrill" style="display:none;">
    	 <div class="field">

          <label>Amount (USD)</label>

          <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>

			<input name="amount" id="amount" placeholder="0.00" type="text">

          </label>

        </div>
    <input type="hidden" name="pay_to_email" value="<?php echo $grows->extra;?>" />
    <input type="hidden" name="return_url" value="<?php echo SITEURL.'/page/tradersystem/index.php?subpage=deposit2';?>" />
    <input type="hidden" name="cancel_url" value="<?php echo SITEURL.'/page/tradersystem/index.php?subpage=deposit2';?>" />
    <input type="hidden" name="status_url" value="<?php echo SITEURL;?>/modules/tradersystem/gateways/skrill/ipn.php" />
    <input type="hidden" name="merchant_fields" value="session_id, item, custom" />
    <input type="hidden" name="item" value="deposit_<?php echo $_SESSION['CMSPRO_username']; ?>" />
    <input type="hidden" name="session_id" value="<?php echo md5(time())?>" />
    <input type="hidden" name="custom" value="<?php echo $_SESSION['CMSPRO_username'];?>" />
    <input type="hidden" name="currency" value="<?php echo ($grows->extra2) ? $grows->extra2 : $core->currency;?>" />
    <input type="hidden" name="detail1_description" value="deposit to trader system" />
    <input type="hidden" name="detail1_text" value="deposit to trader system" />
    <button  type="submit" name="bu_send" class="prolific danger button">Make Deposit</button>
    </div>
    <!-- end form -->
  </form>
</div>
