<div class="">
<div class="col-lg-6 bank_visual">
	
<div class="col-sm-4">
	<input class="displaypay" typepay="offline" type="image" 
	src="<?php echo SITEURL.'/modules/tradersystem/gateways/paypal/paypal_big.png';?>" 
	name="button"  width="163" height="54" title="Offline Payment" alt="">
</div>
	<div class="col-sm-4">
		<?php echo $v['displayname'];?>
	</div>

<div class="col-sm-4">
	<a href="#" data-toggle="modal" data-target="#modal_notactive">
	<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i><?php echo Lang::$word->_MAKE_DEPOSIT;?></button>
	</a>
</div>
								
</div>
</div>


<!-- modal -->
<div class="modal fade" id="modal_paypal">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Make a Deposit</h3>
			<form class="form-horizontal">
				<div class="form-group">
					<div class="row">
						<label for="user_lname" class="col-md-4 control-label">Amount</label>
						<div class="col-md-6 input-group">
							<span class="input-group-addon">USD $</span>
							<input class="form-control" id="appendedPrependedInput" type="text">
						</div>
					</div>
					<div class="row">
						<label for="user_lname" class="col-md-4 control-label">Select account To Deposit</label>
						
						<div class=" col-md-6" style="padding:0;">
							<select  class="form-control" >
								<option value="wallet"><?php echo Lang::$word->_WALLET;?></option>
								<?php
								if ($listAcc != false ){
									foreach ($listAcc as $k => $v) {?>
										<option value="<?php echo $v['trader_acc_login'];?>"><?php echo $v['trader_acc_login'];?></option>
									<?php }
								}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Deposit</button>
					<button class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>