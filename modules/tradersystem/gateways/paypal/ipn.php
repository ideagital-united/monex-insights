<?php
  /**
   * PayPal IPN
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: ipn.php, v4.00 2014-04-12 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  define("_PIPN", true);
  define("TESTADMIN", true);
 
//=== start test data
if (TESTADMIN == true) {
$_SERVER['REQUEST_METHOD'] 		= 'POST';
$_POST['payment_status'] 		= 'Completed';
$_POST['business'] 				= 'darawantaorong@gmail.com';
$_POST['mc_currency']			= 'USD';
$_POST['item_number']			= 'dararabbit';
$_POST['mc_gross']				= 150;
$_POST['txn_id']				= '61E67681CH3238416';
$_POST['payment_date']			= '20:12:59 Jan 13, 2009 PST';
}
//=== end test data ===

  
  ini_set('log_errors', true);
  ini_set('error_log', dirname(__file__) . '/ipn_errors.log');

  if (isset($_POST['payment_status'])) {
      require_once ("../../../../init.php");
	  
      require_once (BASEPATH . "lib/class_pp.php");

      $pp = Core::getRow(Membership::gTable, "name", "paypal");

      $listener = new IpnListener();
      $listener->use_live = $pp->live;
      $listener->use_ssl = true;
      $listener->use_curl = false;

      try {
          $listener->requirePostMethod();
          $ppver = $listener->processIpn();
      }
      catch (exception $e) {
          error_log($e->getMessage(), 3, "pp_errorlog.log");
          exit(0);
      }

      $payment_status = $_POST['payment_status'];
      $receiver_email = $_POST['business'];
	  $mc_currency = $_POST['mc_currency'];
	  $username = $_POST['item_number'];
     // list($membership_id, $user_id) = explode("_", $_POST['item_number']);
      $mc_gross = $_POST['mc_gross'];
      $txn_id = $_POST['txn_id'];

       $sqlgetu = "SELECT * FROM " . Users::uTable . " WHERE username='" . $username."'";
       $users = $db->first($sqlgetu);
      //$getxn_id = Core::verifyTxnId($txn_id);
      //$price = getValueById("price", Users::uTable, $username);
      //$v1 = compareFloatNumbers($mc_gross, $price, "=");
      
      if (TESTADMIN == true) {
      		$ppver = true;
      }
      if ($ppver) {
          if ($_POST['payment_status'] == 'Completed') {
          	  require_once (MODPATH . "tradersystem/admin_class.php");
	          Registry::set('TraderSystem', new TraderSystem($content -> module_data));
			  
			  $wallet_total = Registry::get("TraderSystem")->validate_wallet_user($users->id);
              if ($receiver_email == $pp->extra && isset($users->username) && $users->username == $username && $wallet_total != false) {
              	    $aDate = $listener->datetimepaypal($_POST['payment_date']);
				   // echo '<pre>';print_r($aDate);
                  	$fin['user_id'] 			=   $users->id;
					$fin['money_in'] 		    = $mc_gross;
					$fin['money_out'] 			= 0;
					$fin['money_total'] 		= $wallet_total+($mc_gross*1);
					$fin['save_date'] 		= date("Y-m-d H:i:s");
					$fin['gateway_id']		= $pp->id;
					$fin['gateway_dtl']		= $pp->displayname;
					$fin['money_local'] 	= 0;
					$fin['thai_bank'] 		= 'paypal';
					$fin['trans_date'] 		= $aDate['date'];
					$fin['trans_time'] 		=  $aDate['time'];
					$fin['save_date'] 		= date("Y-m-d H:i:s");
					$fin['status'] 	= '1';
					$fin['deposit_comment'] 	= $listener->getResponse();
					$fin['trans_type'] = "deposit_from_paypal_to_wallet";
                    $sql_add_transaction 	=  Registry::get("TraderSystem")->i_insert('acms_wallet_transaction',$fin);
                    if(intval($users->id) && $users->id!='' && $users->id > 0){
                    	$res_update_wallet = Registry::get("TraderSystem")->i_update("acms_wallet", "user_id", array('money'=>($fin['money_total']*1)), $users->id);
					}
                  /* == Notify Administrator == */
                  require_once (BASEPATH . "lib/class_mailer.php");
                  $row2 = Core::getRowById(Content::eTable, 5);

                  $body = str_replace(array(
                      '[USERNAME]',
                      '[ITEMNAME]',
                      '[PRICE]',
                      '[STATUS]',
                      '[PP]',
                      '[IP]'), array(
                      $username,
                      'deposit_'.$username,
                      $core->formatMoney($mc_gross),
                      "Completed",
                      "PayPal",
                      $_SERVER['REMOTE_ADDR']), $row2->{'body' . Lang::$lang}
					  );

                  $newbody = cleanOut($body);

                  $mailer = Mailer::sendMail();
                  $message = Swift_Message::newInstance()
							->setSubject($row2->{'subject' . Lang::$lang})
							->setTo(array($core->site_email => $core->site_name))
							->setFrom(array($core->site_email => $core->site_name))
							->setBody($newbody, 'text/html');

                  $mailer->send($message);
                  Security::writeLog($username . ' ' . Lang::$word->_LG_PAYMENT_OK . ' ' . $row->{'title' . Lang::$lang}, "", "yes", "payment");
              }

          } else {
              /* == Failed Transaction= = */
              require_once (BASEPATH . "lib/class_mailer.php");
              $row2 = Core::getRowById(Content::eTable, 6);
              $itemname = getValueById("title" . Lang::$lang, Membership::mTable, $membership_id);
              $username = getValueById("username", Users::uTable, (int)$user_id);

              $body = str_replace(array(
                  '[USERNAME]',
                  '[ITEMNAME]',
                  '[PRICE]',
                  '[STATUS]',
                  '[PP]',
                  '[IP]'), array(
                  $username,
                  $itemname,
                  $core->formatMoney($mc_gross),
                  "Failed",
                  "PayPal",
                  $_SERVER['REMOTE_ADDR']), $row2->{'body' . Lang::$lang}
				  );

              $newbody = cleanOut($body);

              $mailer = Mailer::sendMail();
              $message = Swift_Message::newInstance()
						->setSubject($row2->{'subject' . Lang::$lang})
						->setTo(array($core->site_email => $core->site_name))
						->setFrom(array($core->site_email => $core->site_name))
						->setBody($newbody, 'text/html');

              $mailer->send($message);

              Security::writeLog(Lang::$word->_LG_PAYMENT_ERR . $username, "", "yes", "payment");

          }
      }
  }
?>