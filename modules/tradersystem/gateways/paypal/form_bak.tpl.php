<?php
  /**
   * Paypal Form
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: form.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<div class="prolific button">
  <?php $url = ($grows->live) ? 'www.paypal.com' : 'www.sandbox.paypal.com';?>
  <form action="https://<?php echo $url;?>/cgi-bin/webscr" method="post" id="pp_form" name="pp_form">
    <input class="displaypay" typepay="paypal" type="image" src="<?php echo SITEURL.'/modules/tradersystem/gateways/paypal/paypal_big.png';?>" name="button" title="Pay With Paypal" alt="" />
    <!-- start form -->
    <div class="display_showhide" id="display_paypal" style="display:none;">
    <div class="field">

          <label>Amount (USD)</label>

          <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>

			<input name="amount" id="amount" placeholder="0.00" type="text">

          </label>

        </div>
    <input type="hidden" name="cmd" value="_xclick" />
 
    <input type="hidden" name="business" value="<?php echo $grows->extra;?>" />
    <input type="hidden" name="item_name" value="deposit_<?php echo $_SESSION['CMSPRO_username']; ?>" />
    <input type="hidden" name="item_number" value="<?php echo $_SESSION['CMSPRO_username'];?>" />
    <input type="hidden" name="return" value="<?php echo SITEURL.'/page/tradersystem/index.php?subpage=deposit2';?>" />
    <input type="hidden" name="rm" value="2" />
    <input type="hidden" name="notify_url" value="<?php echo SITEURL.'/modules/tradersystem/gateways/'.$grows->dir;?>/ipn.php" />
    <input type="hidden" name="cancel_return" value="<?php echo SITEURL.'/page/tradersystem/index.php?subpage=deposit2';?>" />
    <input type="hidden" name="no_note" value="1" />
    <input type="hidden" name="currency_code" value="<?php echo ($grows->extra2) ? $grows->extra2 : $core->currency;?>" />
    <button  type="submit" name="bu_send" class="prolific danger button">Make Deposit</button>
    </div>
    <!-- end form -->
  </form>
</div>