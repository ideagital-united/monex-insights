<div class="">
<div class="col-lg-5 bank_visual">
	
<div class="col-sm-4">
	<input class="displaypay" typepay="smsrealtime" type="image" 
	src="<?php echo SITEURL.'/modules/tradersystem/gateways/smsrealtime/smsrealtime.jpg';?>" 
	name="button"  width="163" height="54" title="Deposit Auto Payment" alt="">
</div>
	<div class="col-sm-4">
		<?php echo $v['displayname'];?>
	</div>

<div class="col-sm-4">
	<a href="#" data-toggle="modal" data-target="#modal_smsrealtime">
	<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i><?php echo Lang::$word->_MAKE_DEPOSIT;?></button>
	</a>
</div>
								
</div>
</div>
<?php
$smsref = (isset($_SESSION['usr_dtl']['wallet_amt']['sms_ref_no']) && $_SESSION['usr_dtl']['wallet_amt']['sms_ref_no'] != '') 
			? $_SESSION['usr_dtl']['wallet_amt']['sms_ref_no']
			: "NOT REF NO."; 
?>

<!-- modal -->
<div class="modal fade" id="modal_smsrealtime">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<div class="panel panel-default">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<div class="heading_b"><h3>Make a Deposit</h3></div>
			<form class="form-horizontal">
				<div class="form-group">
					<div class="row">
						<!-- START CONTEMT HEAD -->
						<h4>AUTOMATIC DEPOSIT</h4>
						<!-- EMD  CONTEMT HEAD -->
					</div>
					<div class="row">
						<!-- START CONTEMT DETAIL -->
						
						<h4>Guideline</h4>
						<h5>คู่มือการใช้งาน</h5>
						<br>

						<p>This payment channels For banks in Thailand only.</p>
						<p>ช่องทางการชำระเงินนี้ใช้สำหรับธนาคารในประเทศไทยเท่านั้น</p>
						<u>Please Trasfer money to:</u>
						<u>กรุณาโอนเงินมาที่บัญชีด้านล่างนี้</u>
						<br>
						<h4><b>Kasikorn Bank</b> </h4>
    					<h4>Account Name : Tanainan Lorjirasuwan</h4>
    					<h4>Account Number : <b>007-829-6208</b></h4>
    					<br>
						<p>Here’s the list of available banks:</p>
						<p>นี่คือรายการที่คุณสามารถทำได้</p>
						<br>	
						<p>1. <img src="<?php echo THEMEURL;?>/images/bank-tmb.jpg" alt="tmb bank">Transactions via ATM of TMB Bank (in English).</p>
						          <p> &nbsp; &nbsp;&nbsp;ทำรายการผ่านตู้ ATM ของธนาคาร ทหารไทย (ภาษาอังกฤษ)</p>
						<br>
						<p>2. <img src="<?php echo THEMEURL;?>/images/bank-ayudhya.jpg" alt="tmb bank">Transactions via ATM of Ayudhya Bank  (in Thai).</p>
						          <p>&nbsp; &nbsp;&nbsp;ทำรายการผ่านตู้ ATM ของธนาคาร กรุงศรี (ภาษาไทย)</p>
						<br>
						<p>3. <img src="<?php echo THEMEURL;?>/images/bank-ksb.jpg" alt="tmb bank">Transactions via ATM of Kasikornbank (English and Thai).</p>
						          <p>&nbsp; &nbsp;&nbsp;ทำรายการผ่านอินเตอร์เน็ตแบงค์กิ้ง ของธนาคาร กสิกรไทย</p>
						<br>
						<p>4. <img src="<?php echo THEMEURL;?>/images/bank-ksb.jpg" alt="tmb bank">Transactions via Internet Banking of Kasikornbank.</p>
						         <p>&nbsp; &nbsp;&nbsp;ทำรายการผ่านตู้ ATM ของธนาคาร กสิกรไทย (ภาษาอังกฤษ และ ภาษาไทย)</p>
						<br>
						<p>When you make a transfer please send SMS to <b>08-800-800-89</b></p>
						<p>เมื่อคุณทำรายการโอน กรุณาเลือกส่ง SMS ไปที่เบอร์ <b>08-800-800-89</b></p>
						<br>
						<p>and put This Your Reference No.<b><?php echo $smsref;?></b>  to Reference number in the process.</p>
						<p>และใส่หมายเลขอ้างอิงของคุณ นี้  <b><?php echo $smsref;?> </b> ใส่เข้าไปยังช่องหมายเลขอ้างอิงในช่องทางที่คุณเลือก</p>
						<br>
						<br>
						<!-- EMD  CONTEMT DETAIL -->
					
					</div>
				</div>
				
			</form>
			</div>
		</div>
	</div>
</div>
</div>