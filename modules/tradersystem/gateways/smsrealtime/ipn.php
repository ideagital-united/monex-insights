<?php
  /**
   * Skrill IPN
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: ipn.php, v4.00 2014-04-14 15:14:02 gewa Exp $
   */
  define("_VALID_PHP", true);
  define("TESTADMIN", true);
  require_once ("../../../../init.php");

  /* only for debuggin purpose. Create logfile.txt and chmot to 0777
  ob_start();
  echo '<pre>';
  print_r($_POST);
  echo '</pre>';
  $logInfo = ob_get_contents();
  ob_end_clean();
  
  $file = fopen('logfile.txt', 'a');
  fwrite($file, $logInfo);
  fclose($file);
  */

//=== start test data

if (TESTADMIN == true) {
$_POST['status'] 		= 2;
$_POST['md5sig'] 		= '3ADBC33B22A3B55D48D90F83E35E0A06';
$_POST['merchant_id']			= '121212';
$_POST['pay_to_email']			= 'darawantaorong@gmail.com';
$_POST['mb_amount']				= 150;
$_POST['mb_transaction_id']				= '61E67681CH3238416';
$_POST['currency']			= 'USD';
$_POST['amount']			= 150;
$_POST['transaction_id']			= '20:12:59 Jan 13, 2009 PST';
$_POST['pay_from_email']			= 'hh@gmail.com';
$_POST['mb_currency']			= 'USD';
$_POST['custom'] = 'dararabbit';// username;
}
//=== end test data ===
  
  
  
  /* Check for mandatory fields */
  $r_fields = array(
      'status',
      'md5sig',
      'merchant_id',
      'pay_to_email',
      'mb_amount',
      'mb_transaction_id',
      'currency',
      'amount',
      'transaction_id',
      'pay_from_email',
      'mb_currency');
  $mb_secret = getValue("extra3", Membership::gTable, "name = 'skrill'");
   $pp = Core::getRow(Membership::gTable, "name", "skrill");
  $txtresponse = '';
  foreach ($r_fields as $f)
      if (!isset($_POST[$f])) die();
	  else $txtresponse .='&' .$f.'='.$_POST[$f];

  /* Check for MD5 signature */
  $md5 = strtoupper(md5($_POST['merchant_id'] . $_POST['transaction_id'] . strtoupper(md5($mb_secret)) . $_POST['mb_amount'] . $_POST['mb_currency'] . $_POST['status']));
  echo '<br>md='.$md5;
  if ($md5 != $_POST['md5sig'])
      die();
   $username = $_POST['custom'];
   $receiver_email = $_POST['pay_to_email'];
  $sqlgetu = "SELECT * FROM " . Users::uTable . " WHERE username='" . $username."'";
  $users = $db->first($sqlgetu);
  $mc_gross = $_POST['mb_amount'] ;
  $resultpay = true;
  if (intval($_POST['status']) == 2) {
  	
	 // start prolific
	   require_once (MODPATH . "tradersystem/admin_class.php");
	          Registry::set('TraderSystem', new TraderSystem($content -> module_data));
			  
			  $wallet_total = Registry::get("TraderSystem")->validate_wallet_user($users->id);
              if (isset($users->username) && $users->username == $username && $wallet_total != false) {
              	echo'MMMMMMMMMMMMMMMMMMMMMMM';
              	    $aDate = list($dd,$mm,$yy,$time) = explode(" ",date('d F Y H:i:s',time()));
				  list($hh,$minu) = explode(":",$time);
				   // echo '<pre>';print_r($aDate);
                  	$fin['user_id'] 			=   $users->id;
					$fin['money_in'] 		    = $mc_gross;
					$fin['money_out'] 			= 0;
					$fin['money_total'] 		= $wallet_total+($mc_gross*1);
					$fin['save_date'] 		= date("Y-m-d H:i:s");
					$fin['gateway_id']		= $pp->id;
					$fin['gateway_dtl']		= $pp->displayname;
					$fin['money_local'] 	= 0;
					$fin['thai_bank'] 		= 'skrill';
					$fin['trans_date'] 		= $dd." ".$mm." ".$yy;
					$fin['trans_time'] 		=  $hh.":".$minu;
					$fin['save_date'] 		= date("Y-m-d H:i:s");
					$fin['status'] 	= '1';
					$fin['deposit_comment'] 	= $txtresponse;
					$fin['trans_type'] = "deposit_from_skrill_to_wallet";
                    $sql_add_transaction 	=  Registry::get("TraderSystem")->i_insert('acms_wallet_transaction',$fin);
                    if(intval($users->id) && $users->id!='' && $users->id > 0){
                    	$res_update_wallet = Registry::get("TraderSystem")->i_update("acms_wallet", "user_id", array('money'=>($fin['money_total']*1)), $users->id);
					/* == Notify Administrator == */
                  require_once (BASEPATH . "lib/class_mailer.php");
                  $row2 = Core::getRowById(Content::eTable, 5);

                  $body = str_replace(array(
                      '[USERNAME]',
                      '[ITEMNAME]',
                      '[PRICE]',
                      '[STATUS]',
                      '[PP]',
                      '[IP]'), array(
                      $username,
                      'deposit_'.$username,
                      $core->formatMoney($mc_gross),
                      "Completed",
                      "PayPal",
                      $_SERVER['REMOTE_ADDR']), $row2->{'body' . Lang::$lang}
					  );

                  $newbody = cleanOut($body);

                  $mailer = Mailer::sendMail();
                  $message = Swift_Message::newInstance()
							->setSubject($row2->{'subject' . Lang::$lang})
							->setTo(array($core->site_email => $core->site_name))
							->setFrom(array($core->site_email => $core->site_name))
							->setBody($newbody, 'text/html');

                  $mailer->send($message);
                  Security::writeLog($username . ' ' . Lang::$word->_LG_PAYMENT_OK . ' ' . $row->{'title' . Lang::$lang}, "", "yes", "payment");
					} else {
						$resultpay= false;
					}
	 // end prolific
	 
	
	
	
	
	  
     



  } else {
      $resultpay= false;

  }
  }else{
  	 $resultpay= false;
  }
  
  if ($resultpay == false) {
  	 /* == Failed Transaction= = */
              require_once (BASEPATH . "lib/class_mailer.php");
              $row2 = Core::getRowById(Content::eTable, 6);
             
              

              $body = str_replace(array(
                  '[PP]',
                  '[USERNAME]',
                  '[ITEM]',
                  '[PRICE]',
                  '[STATUS]',
                  
                  '[IP]'), array(
                   "Skrill",
                  $username,
                  'deposit_from_skrill_to_wallet',
                  $core->formatMoney($mc_gross),
                  "Failed",
                 
                  $_SERVER['REMOTE_ADDR']), $row2->{'body' . Lang::$lang}
				  );

              $newbody = cleanOut($body);

              $mailer = Mailer::sendMail();
              $message = Swift_Message::newInstance()
						->setSubject($row2->{'subject' . Lang::$lang})
						->setTo(array($core->site_email => $core->site_name))
						->setFrom(array($core->site_email => $core->site_name))
						->setBody($newbody, 'text/html');

              $mailer->send($message);

              Security::writeLog(Lang::$word->_LG_PAYMENT_ERR . $username, "", "yes", "payment");
  }

?>