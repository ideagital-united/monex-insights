<?php
  /**
   * Paypal Form
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: form.tpl.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
?>
<div class="prolific button">
	<input class="displaypay" typepay="paysbuy" type="image" src="<?php echo SITEURL.'/modules/tradersystem/gateways/paysbuy/paysbuy_big.png';?>" name="button" title="Pay With PaySbuy" alt="" />
    <!-- start form -->
    <div class="display_showhide" id="display_paysbuy" style="display:none;">
    	
  <form name="frmbeforesubmitpaysbuy" id="frmbeforesubmitpaysbuy" method="post">
  	 <input type="hidden" name="gateway_id" value="<?php echo $grows->id;?>">  
  	 <div class="field">
          <label>Amount (USD)</label>
          <label class="input"><i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>
			<input name="amt" id="amt" placeholder="0.00" type="text">
          </label>
        </div>
        <button data-frmname="frmbeforesubmitpaysbuy" data-url="/modules/tradersystem/ajax_gateway.php" type="button" name="dosubmit_gateway" class="prolific danger button">Make Deposit</button>
  </form>
  
  <?php $url = ($grows->live) ? 'www.paysbuy.com' : 'demo.paysbuy.com';?>
  <form action="https://<?php echo $url;?>/paynow.aspx" method="post" id="paysbuy_form" name="paysbuy_form">
	<input name="amt" id="amt" placeholder="0.00" type="hidden">
    <input type="hidden" name="psb" value="psb" />
    <input type="hidden" name="biz" value="<?php echo $grows->extra2;?>" />
  <!--  <input type="hidden" name="secureCode" value="<?php echo $grows->extra3;?>" />-->
    <input type="hidden" name="curr_type" value="US" />
    <input type="hidden" name="inv" value="" />
    <input type="hidden" name="itm" value="" />
    <input type="hidden" name="resp_front_url" value="<?php echo SITEURL.'/page/tradersystem/index.php?subpage=deposit2';?>" />
    <input type="hidden" name="resp_back_ur" value="" />
 
   
   
    <!-- end form -->
  </form>
   </div>
</div>