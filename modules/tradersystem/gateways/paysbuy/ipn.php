<?php
  /**
   * PayPal IPN
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: ipn.php, v4.00 2014-04-12 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  define("_PIPN", true);
  define("TESTADMIN", true);
 
//=== start test data

function datetimepaysbuy($datetime = '')
{
	//$datetime = '20:12:59 Jan 13, 2009 PST';
	$datetime  = strtotime($datetime);
		 //var_dump($datetime);
	$aRes = array();
	$aRes['date'] = '';
	$aRes['time'] = '';
	if ($datetime == '') {
		return $aRes;
	} else {
		list($dd,$mm,$yy,$time) = explode(" ",date('d F Y H:i:s',$datetime));
		list($hh,$minu) = explode(":",$time);
		//echo '<br>='.$hh.','.$minu;
		return array('date'=>$dd." ".$mm." ".$yy,'time'=>$hh.":".$minu);
	}
}

if (TESTADMIN == true) {
$_SERVER['REQUEST_METHOD'] 		= 'POST';
$_POST = array(
    "result" => '001_44545454', 
    "apCode" => '8175219',
    "amt" => '10.00', 
    "fee" => '0.37',
    "method" => '05',
    "create_date" => '2/2/2015 1:27:51 PM',
    "payment_date" => '2/2/2015 1:28:44 PM'
);
}
//=== end test data ===

  
  ini_set('log_errors', true);
  ini_set('error_log', dirname(__file__) . '/ipn_errors.log');

  if (isset($_POST['result'])) {
      require_once ("../../../../init.php");
      $pp = Core::getRow(Membership::gTable, "name", "PaySbuy");
	  $invoiceNo = substr($_POST['result'],2);
	  $merchantEmail =$pp->extra2;
	  $strApCode = $_POST['apCode'];
      $url =  'www.paysbuy.com';
	  
		$url = "https://".$url."/getinvoicestatus/getinvoicestatus.asmx/GetInvoice?invoiceNo=$invoiceNo&merchantEmail=$merchantEmail&strApCode=$strApCode";
		//echo $url;
		$res = file_get_contents($url);

		$xml = simplexml_load_string($res);
		$json = json_encode($xml);
		$data = json_decode($json,TRUE);
      if (TESTADMIN == true) {
      	$data['StatusResult'] = "Accept";
		$data['AmountResult'] = $_POST['amt'];
		$data['MethodResult'] =  $_POST['method'];
	//echo 'ssssssssssssss';
      }
      if($data['StatusResult'] == "Accept"){
      	if($data['AmountResult'] == $_POST['amt'])
		{
			if($data['MethodResult'] == $_POST['method'])
			{
				$aInv = explode('_',$invoiceNo);
				$uid = $aInv[0];
				$mc_gross =  $_POST['amt']*1;
				$sqlgetu = "SELECT * FROM " . Users::uTable . " WHERE id='" . $uid."'";
       			$users = $db->first($sqlgetu);
				$username =  $users->username;
				require_once (MODPATH . "tradersystem/admin_class.php");
	          	Registry::set('TraderSystem', new TraderSystem($content -> module_data));
			  
			  	$wallet_total = Registry::get("TraderSystem")->validate_wallet_user($users->id);
				$aDate = datetimepaysbuy($_POST['payment_date']);
			   // echo '<pre>';print_r($aDate);
              	$fin['user_id'] 			=   $users->id;
				$fin['money_in'] 		    = $mc_gross;
				$fin['money_out'] 			= 0;
				$fin['money_total'] 		= $wallet_total+($mc_gross*1);
				$fin['save_date'] 		= date("Y-m-d H:i:s");
				$fin['gateway_id']		= $pp->id;
				$fin['gateway_dtl']		= $pp->displayname;
				$fin['money_local'] 	= 0;
				$fin['thai_bank'] 		= 'paysbuy';
				$fin['trans_date'] 		= $aDate['date'];
				$fin['trans_time'] 		=  $aDate['time'];
				$fin['save_date'] 		= date("Y-m-d H:i:s");
				$fin['status'] 	= '1';
				$fin['deposit_comment'] 	= '';
				$fin['trans_type'] = "deposit_from_paypal_to_wallet";
                $sql_add_transaction 	=  Registry::get("TraderSystem")->i_insert('acms_wallet_transaction',$fin);
                if(intval($users->id) && $users->id!='' && $users->id > 0){
                	$res_update_wallet = Registry::get("TraderSystem")->i_update("acms_wallet", "user_id", array('money'=>($fin['money_total']*1)), $users->id);
				}
				require_once (BASEPATH . "lib/class_mailer.php");
                  $row2 = Core::getRowById(Content::eTable, 5);

                  $body = str_replace(array(
                      '[USERNAME]',
                      '[ITEMNAME]',
                      '[PRICE]',
                      '[STATUS]',
                      '[PP]',
                      '[IP]'), array(
                      $username,
                      'deposit_'.$username,
                      $core->formatMoney($mc_gross),
                      "Completed",
                      "PaySbuy",
                      $_SERVER['REMOTE_ADDR']), $row2->{'body' . Lang::$lang}
					  );

                  $newbody = cleanOut($body);

                  $mailer = Mailer::sendMail();
                  $message = Swift_Message::newInstance()
							->setSubject($row2->{'subject' . Lang::$lang})
							->setTo(array($core->site_email => $core->site_name))
							->setFrom(array($core->site_email => $core->site_name))
							->setBody($newbody, 'text/html');

                  $mailer->send($message);
                  Security::writeLog($username . ' ' . Lang::$word->_LG_PAYMENT_OK . ' ' . $row->{'title' . Lang::$lang}, "", "yes", "payment");
			} else {
				error_log('data method not equal post method', 3, "pp_errorlog.log");
		        exit(0);
			}
		} else {
			error_log('data AmountResult not equal post amt', 3, "pp_errorlog.log");
		    exit(0);
		}
      } else {
          error_log('StatusResult not equal acceipt', 3, "pp_errorlog.log");
          exit(0);
      }
	  }

     
  
?>