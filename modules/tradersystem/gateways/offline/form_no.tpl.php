<div class="">
<div class="col-lg-5 bank_visual">
	
<div class="col-sm-4">
	<input class="displaypay" typepay="offline" type="image" 
	src="<?php echo SITEURL.'/modules/tradersystem/gateways/offline/offline_big.png';?>" 
	name="button"  width="163" height="54" title="Offline Payment" alt="">
</div>
	<div class="col-sm-4">
		<?php 
		$display_name = $v['displayname'];
		echo $v['displayname'];?>
	</div>

<div class="col-sm-4">
	<a href="#" data-toggle="modal" data-target="#modal_bank_transfer">
	<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i><?php echo Lang::$word->_MAKE_DEPOSIT;?></button>
	</a>
</div>
								
</div>
</div>

<!-- modal -->
<div class="modal fade" id="modal_bank_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<div class="panel panel-default">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<form class="form-horizontal" id="prolific_offline" name="prolific_offline" method="post">
			<h3><?php echo Lang::$word->_MAKE_DEPOSIT;?>  [Wallet $<?php echo number_format($wallet_net,2);?>]</h3>
				
				<div class="panel-heading"><?php echo Lang::$word->_Bank_Transfer_Information;?></div>
				<span class="help-block">
					<?php echo $v['extra_txt3'];?>
				</span>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<td><?php echo Lang::$word->_BANK_NAME;?></td>
									<td>Account Name</td>
									<td><?php echo Lang::$word->_ACCOUNT_NUMBER;?></td>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($aBankBroker === false) {?>
									<tr><td>NO DATA</td></tr>
								<?php }else {
									$aBandBank = array(
									'KASITHBK'=>'icon_kbank.png',
									'SICOTHBK'=>'icon_scb.png',
									'BKKBTHBK'=>'icon_bbl.png',
									'KRUNGTHAI'=>'icon_ktb.png'
									);
									foreach ($aBankBroker as $kb => $vb) {?>
										<tr>
											<td><img src="<?php echo THEMEURLTRADE;?>assets/img/<?php echo $aBandBank[$vb['swift']];?>" alt="<?php echo $vb['bank_name'];?>"></td>
											<td><?php echo $vb['bank_account_name'];?></td>
											<td><?php echo $vb['bank_account_number'];?></td>
										</tr>
									<?php }
								}
								?>
							</tbody>
						</table>
						
						<div class="text-center btn_clickpass">
                        <button type="button" name="bu_to_this" class="prolific button btn btn-success" onclick="$('.btn_clickpass').hide();$('.display_showdetail').show();">
                     	<i class="fa fa-angle-down"></i>Confirm Payment</button>
						</div>
						<div class="text-center display_showdetail" style="display:none;">
                        <button type="button" name="bu_to_this" class="prolific button btn btn-success" onclick="$('.display_showdetail').hide();$('.btn_clickpass').show();">
                     	<i class="fa fa-chevron-up"></i>Hide Detail</button>
						</div>
						
					
					</div>
				</div>
				
				<div class="form-group display_showdetail" style="display:none;">
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_BANK_NAME;?></label>
					
					<div class=" col-md-6" style="padding:0;">
						<select  class="form-control" name="bank_code" >
							<?php
							if ($aBankBroker != false ){
								foreach ($aBankBroker as $kb2 => $vb2) {
									?>
									<option value="<?php echo $vb2['bank_name']. ' ' .$vb2['bank_account_name'] .' ' .$vb2['bank_account_number'] ?>"><?php echo $vb2['bank_name'];?></option>
								<?php }
							}
							?>
						</select>
					</div>
					<?php 
					if ($aRate != false) {
						?>
						<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_CURRENCY_LOCAL;?></label>
					
						<div class=" col-md-6" style="padding:0;">
							<select  class="form-control" name="rate_exchange_id" id="rate_exchange_id">
								<?php
								foreach($aRate as $krow){?>
									<option value="<?php echo $krow->id?>" displayname ="<?php echo $krow->currency;?>" ratedeposit="<?php echo $krow->deposit;?>"><?php echo $krow->currency;?></option>
								<?php
								}
								?>
							</select>
						</div>
						<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_TR_AMOUNT;?></label>
						<div class="col-md-6 input-group">
							<span class="input-group-addon" id="display_currency"><?php echo $aRate['currency'];?></span>
							<input class="form-control" id="amount_local" name="amount_local" type="text">
						</div>
						<?php
					}
					?>
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_TR_AMOUNT;?></label>
					<div class="col-md-6 input-group">
						<span class="input-group-addon">USD $</span>
						<input class="form-control" id="amount" name="amount" type="text">
					</div>
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_SELECT_ACCOUNT_TO_DEPOSIT;?></label>
					
					<div class=" col-md-6" style="padding:0;">
						<select  class="form-control" name="trans_type" >
							<option value="wallet"><?php echo Lang::$word->_WALLET;?></option>
							<?php
							if ($listAcc != false ){
								foreach ($listAcc2 as $k => $v) {
									if ($v['is_demo'] == 1) continue;
									?>
									<option value="<?php echo $v['trader_acc_login'];?>"><?php echo $v['trader_acc_login'];?></option>
								<?php }
							}
							?>
						</select>
					</div>
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_LG_WHEN;?></label>
					<div class="col-md-6 input-group">
						<div class="input-group date ts_datepicker" data-date-format="dd-mm-yyyy" >
							<input class="form-control" name="trans_date" type="text">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_TIME;?></label>
					<div class="col-md-6 input-group">
						<div class="input-group bootstrap-timepicker">
							<input id="deposittime" name="trans_time" type="text" class="form-control">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
							</span>
						</div>
					</div>
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_TRANSFER_S_DOCUMENT;?></label>
					<div class="col-md-6 input-group">
						<input type="file" id="file_slip" name="file_slip" class="form-control" value="Zieme">
					</div>
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_REMARK;?></label>
					<div class="col-md-6 input-group">
						<textarea cols="30" rows="3" name="deposit_comment" class="form-control input-sm" placeholder="Message..."></textarea>
					</div>
				</div>
				<div class="text-center display_showdetail" style="display:none;">
					<input name="wallet_total" type="hidden" value="<?php echo $wallet_net;?>">
					<input name="doDeposit_payment" type="hidden" value="1">
					<input name="gateway_dtl" type="hidden" value="<?php echo $display_name;?>">
                     <button data-url="/modules/tradersystem/ajax_user.php" data-frm="prolific_offline"  type="button" name="dosubmit" class="prolific button btn btn-success">
                     	<i class="glyphicon glyphicon-plus"></i><?php echo Lang::$word->_MAKE_DEPOSIT;?></button>
					<button class="btn btn-danger"  data-dismiss="modal"><i class="fa fa-chevron-left"></i><?php echo Lang::$word->_FM_BACK;?></button>
				</div>
			
			</div>
			</form>
		</div>
	</div>
</div>
</div>

<script>
function rate_onchange()
{
	rate_deposit = $('#rate_exchange_id option:selected').attr('ratedeposit');
	var displayname = $('#rate_exchange_id option:selected').attr('displayname');
	rate_deposit = rate_deposit*1;
	$('#display_currency').html(displayname);
}
window.onload = function () { 
	$('#rate_exchange_id').change(function(){
		rate_onchange();
	});
	if($('#rate_exchange_id').get(0)){
		rate_onchange();
	}
	$('#amount').keyup(function() {
		var usd_val = $(this).val();
		usd_val 	= usd_val*1;
		
		$('#amount_local').val((usd_val*rate_deposit).toFixed(2));
	});
	$('#amount_local').keyup(function() {
		var local_val = $(this).val();
		local_val 	= local_val*1;
		$('#amount').val((local_val/rate_deposit).toFixed(2));
	    
	});
}

function toFixed(value, precision) {
    var power = Math.pow(10, precision || 0);
    return String(Math.round(value * power) / power);
}
</script>