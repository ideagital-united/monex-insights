<div class="">
<div class="col-lg-6 bank_visual">
	
<div class="col-sm-4">
	<input class="displaypay" typepay="offline" type="image" 
	src="<?php echo SITEURL.'/modules/tradersystem/gateways/wiretransfer/wiretransfer.jpg';?>" 
	name="button" title="Offline Payment" alt="">
</div>
	<div class="col-sm-4">
		<?php echo $v['displayname'];?>
	</div>

<div class="col-sm-4">
	<a href="#" data-toggle="modal" data-target="#modal_bank_transfer">
	<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i><?php echo Lang::$word->_MAKE_A_WITHDRAW;?></button>
	</a>	
</div>
								
</div>
</div>

<!-- modal -->
<?php
$aSecure 		= $tradersystem->get_secure_personal();
/*$aSecure['type'] = 'sms';*/

$input_code_opt = '<span id="transfer_detail"></span>';
$input_code_opt .= ' <div class="form-group_last">
					<label class="col-md-4 control-label" for="user_lname"> OTP </label>
					<div class="col-md-6 input-group">
						<input type="hidden" name="list_pkid" id="list_pkid" value="">
						<input type="hidden" name="withdraw_step2" value="1">
						<input class="form-control" id="code_verify" name="code_verify" placeholder"OTP" type="text">
						<span class="input-group-addon codeotp_offline"></span>
					</div>
				</div>';
				
if ($aSecure['type'] == 'email') {
	$frm_step = '<form class="form-horizontal" id="prolific_form_verify" name="prolific_form_verify" method="post">';
	$frm_step .='<div class="col-md-12 control-label " style="  text-align: center;" ><img src="'.THEMEURLTRADE.'assets/img/email.png" ></div>
	               <div class="text-center">'.Lang::$word->_EMAIL_SEND_TOKEN.'</div>';
	$frm_step .= $input_code_opt;
	
	
		$frm_step .='<div class="text-center">
	                   <button  data-divshow="codeotp_offline" type="button" name="bu_req_otp" class="prolific button btn btn-alert  bu_req_otp">
                     	<i class="glyphicon glyphicon-refresh"></i> Request Code Again</button>
	 					<button data-url="/modules/tradersystem/ajax_user.php" data-frm="prolific_form_verify"  type="button" name="dosubmit" class="prolific button btn btn-success">
                     	<i class="glyphicon glyphicon-check"></i> Verify</button>
	                </div>';
	$frm_step .='</form>';
	
	
} elseif ($aSecure['type'] == 'google') {
	$frm_step = '<form class="form-horizontal" id="prolific_form_verify" name="prolific_form_verify" method="post">';
	
	$frm_step .= '<div class="form-group">
						<div class="col-md-12 control-label " style="  text-align: center;" ><img src="'.THEMEURLTRADE.'assets/img/googleauth.PNG" ></div>
						<div class="col-md-12">'.Lang::$word->_SMS_SEND_TOKEN.'</div>
					</div>';
	
	$frm_step .= $input_code_opt;
	$frm_step .='<div class="text-center">
	 					<button data-url="/modules/tradersystem/ajax_user.php" data-frm="prolific_form_verify"  type="button" name="dosubmit" class="prolific button btn btn-success">
                     	<i class="glyphicon glyphicon-check"></i> Verify</button>    
	                   
	                </div>';
	$frm_step .='</form>';
	
} elseif ($aSecure['type'] == 'sms') {
	$frm_step = '<form class="form-horizontal" id="prolific_form_verify" name="prolific_form_verify" method="post">';
	$frm_step .= '<div class="form-group">
						<div class="col-md-12 control-label " style="  text-align: center;" ><img src="'.THEMEURLTRADE.'assets/img/sms.png" ></div>
						<div class="col-md-12">'.Lang::$word->_SMS_SEND_TOKEN.'</div>
					</div>';
	
	$frm_step .= $input_code_opt;
	$frm_step .='<div class="text-center">
	                   <button  data-divshow="codeotp_offline" type="button" name="bu_req_otp" class="prolific button btn btn-alert bu_req_otp">
                     	<i class="glyphicon glyphicon-refresh"></i> Request Code Again</button>
	 					<button data-url="/modules/tradersystem/ajax_user.php" data-frm="prolific_form_verify"  type="button" name="dosubmit" class="prolific button btn btn-success">
                     	<i class="glyphicon glyphicon-check"></i> Verify</button>    
	                </div>';
	$frm_step .='</form>';
} else {	
	$frm_step .='<div class="text-center">
	                   No Verify Step 2.
	                </div>';
}
echo builerDialogBox("frm_step2","Two-Step Verifycation",$frm_step);

$aValidFrm = $tradersystem->validate_show_frm_withdraw();

?>



<div class="modal fade" id="modal_bank_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<div class="panel panel-default">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3><?php echo Lang::$word->_MAKE_A_WITHDRAW;?></h3>
				<form class="form-horizontal" id="prolific_offline" name="prolific_offline" method="post">
				<div class="panel-heading"><?php echo Lang::$word->_Bank_Transfer_Information . $wallet_net .'USD';?></div>
				
				<?php 
					
				if ($aValidFrm['status'] == false) {?>
					<div class="form-group">
					<?php
					$err = "<ul>";
					foreach($aValidFrm['Msg'] as $vErr){
						$err .= "<li>".$vErr."</li>";
					}
					$err .= "</ul>";
					echo Filter::msgAlert_gob($err);
					?>
					</div>
					<div class="text-center">
					<a href="index.php?subpage=account_setting">
                    <button type="button" name="bu_dosubmit" class="prolific button btn btn-success">
                     	<i class="glyphicon glyphicon-link"></i><?php echo Lang::$word->_GO_TO_MENU_ACCOUNT_SETTING;?></button></a>
					<button class="btn btn-danger"  data-dismiss="modal"><i class="fa fa-chevron-left"></i><?php echo Lang::$word->_FM_BACK;?></button>
					</div>
				<?php }elseif (count($_SESSION['usr_dtl']['user_bank_list']) > 0) {
					
				?>
				<div class="form-group">
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_WITHDRAW_TO_BANK;?></label>
					<div class="col-md-6 input-group">
						<script>var obj_abount_bank = [];</script>
						<select name="bank_id" id="bank_id" class="form-control" >
						<option value=""><?php echo Lang::$word->_PLEASE_SELECT_DATA;?></option>
						<?php
						//chart_browsers_data_new_ct.push({label:typep+":"+acctrade,data:netmoney,color:obj.colorset});
						$aBank = $_SESSION['usr_dtl']['user_bank_list'];
						$i=0;
						foreach($aBank as $kBank => $vBank){
							if($vBank['is_verified'] !=1) continue;
							$aDtl = json_decode($vBank['data']);
							?>
							<script>obj_abount_bank.push(<?php echo $vBank['data'];?>);</script>
							<option value="<?php echo $i;?>"><?php echo $aDtl->bank_name;?></option>
							<?php
							$i++;
						}
						
						?>
						</select>
					</div>
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_BANK_NAME;?></label>
					<div class="col-md-8 input-group"><span class="form-control" id="bank_set_name"></div>
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_COUNTRY;?></label>
					<div class="col-md-6 input-group"><span class="form-control"id="bank_set_country"></span></div>
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_ACCOUNT_TYPE;?></label>
					<div class="col-md-6 input-group"><span class="form-control"id="bank_set_account_type"></span></div>
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_ACCOUNT_NUMBER;?></label>
					<div class="col-md-6 input-group"><span class="form-control"id="bank_set_account_number"></span></div>
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_SWIFT;?></label>
					<div class="col-md-6 input-group"><span class="form-control"id="bank_set_swift"></span></div>
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_ROUTING_NUMBER;?></label>
					<div class="col-md-6 input-group"><span class="form-control"id="bank_set_routing"></span></div>
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_IBAN;?></label>
					<div class="col-md-6 input-group"><span class="form-control"id="bank_set_iban"></span></div>
					
					
					<label for="user_lname" class="col-md-4 control-label"><?php echo Lang::$word->_TR_AMOUNT;?></label>
					<div class="col-md-6 input-group">
						<span class="input-group-addon">USD $</span>
						<input class="form-control" id="amount" name="amount" type="text">
					</div>
					<?php
					if ($aRate != false) {
						?>
						<script>var obj_currency = [];</script>
						
						<?php
						foreach($aRate as $krow2){
							$aCurrencyp = array();
							$aCurrencyp[$krow2->currency] = $krow2;
							?>
							<script>obj_currency.<?php echo $krow2->currency;?>=<?php echo json_encode($krow2);?>;</script>
							<?php
						}
					?>
					<!--
					<label for="user_lname" class="col-md-4 control-label set_currency_local" style="display:none;"><?php echo Lang::$word->_CURRENCY_LOCAL;?></label>
					<div class="col-md-6 input-group set_currency_local" style="display:none;">
						<span class="input-group-addon"  id="currency_name"></span>
						
					</div>
					-->
					<input class="form-control" id="amount_local" name="amount_local" type="hidden">
					<?php }?>
					
			
				</div>
				<div class="text-center">
					 <input name="doWithdraw_payment" type="hidden" value="1">
					 <input name="gateway_dtl" type="hidden" value="<?php echo $v['displayname'];?>">
					  <input name="trans_type" type="hidden" value="wallet">
					 
                     <button data-url="/modules/tradersystem/ajax_user.php" data-frm="prolific_offline"  type="button" name="dosubmit" class="prolific button btn btn-success">
                     	<i class="glyphicon glyphicon-plus"></i><?php echo Lang::$word->_MAKE_A_WITHDRAW;?></button>
					<button class="btn btn-danger"  data-dismiss="modal"><i class="fa fa-chevron-left"></i><?php echo Lang::$word->_FM_BACK;?></button>
				</div>
				<?php }else {?>
					<div class="form-group">
					<?php
					echo Filter::msgError_gob(Lang::$word->_PLEASE_SETTING_ABOUT_PROFILE_YOUR_BANK);
					?>
					</div>
					<div class="text-center">
					<a href="index.php?subpage=account_setting">
                    <button type="button" name="bu_dosubmit" class="prolific button btn btn-success">
                     	<i class="glyphicon glyphicon-link"></i><?php echo Lang::$word->_GO_TO_MENU_ACCOUNT_SETTING;?></button></a>
					<button class="btn btn-danger"  data-dismiss="modal"><i class="fa fa-chevron-left"></i><?php echo Lang::$word->_FM_BACK;?></button>
					</div>
				<?php } ?>
			</form>
			</div>
			
		</div>
	</div>
</div>
</div>

