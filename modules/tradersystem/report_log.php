<?php 
$sql 	= "
	SELECT 
		created,ip,type,message
	FROM 
		log
	WHERE 
		user_id ='".$_SESSION['CMSPRO_username']."' 
	ORDER BY
		created DESC ";
        
        //echo $sql;
$data 	= Registry::get("Database")->fetch_all($sql);
$alistReport  = ($data) ? $data : 0;
?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>My activity log</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Created</th>
									<th>IP</th>
									<th>Type</th>
									<th>Message</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if ($alistReport) {
									
									foreach($alistReport as $k => $v){
									
									?>
										<tr>
										<td><?php echo $v->created;?></td>
										<td><?php echo $v->ip;?></td>
										<td><?php echo $v->type;?></td>
										<td><?php echo $v->message;?></td>
								</tr>
									<?php }
								}
								
								?>
								<!--
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
								-->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>



