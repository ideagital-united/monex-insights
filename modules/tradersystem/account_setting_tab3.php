<?php
//=== START RATE EX 
$sqlrate = "SELECT * FROM bk_exchn_rate_new where status=1";
$aRate = $db->fetch_all($sqlrate);
if (!$aRate) {
	$aRate = false;
}
//=== END RATE EX
?>

<div id ="frmAddBankUser" style="display:none;">
<form class="form-horizontal" id="prolific_form3" name="prolific_form3" method="post" >
	<div class="form-group">
		<div class="col-md-12">
			<div class="heading_b"><?php echo Lang::$word->_BANK_DETAIL;?></div>
		</div>
	</div>
	<div class="form-group">
		<label for="profile_username" class="col-md-2 control-label">Bank Name *</label>
		<div class="col-md-10">
		    <?php
		    $aBank = $tradersystem->call_curl_get("bank?cmd=bank","GET",'');
           if(isset($aBank['isSuccess']) && $aBank['isSuccess'] ==1){
                    $aBank = $aBank['message'];
           } else {
                    $aBank = array();
            }
		   
		   
		    ?>
		    
		    <select id="bank_name" name="bank_name"class="form-control" onchange="set_bankother();">
			<?php
                echo $tradersystem->list_select_option_by_array($aBank,$usr_dtl['bank_name'],"bank_code","bank_code",Lang::$word->_PLEASE_SELECT_DATA);
            ?>
            <option value="other"> Other Bank </option>
            </select>
            <input type="text" name="bank_name_other" class="form-control"  onblur="set_new_bank_name();"  id="bank_name_other" placeholder="bank name other" value="" style="display:none;">
            
		</div>
	</div>
	<div class="form-group">
		<label for="user_lname" class="col-md-2 control-label"><?php echo Lang::$word->_COUNTRY;?> *</label>
		<div class="col-md-10">
			<!--<select id="bank_country" name="bank_country"class="form-control" >-->
				<select id="s3_ext_value" name="bank_country" class="form-control" >
				<?php
				echo $tradersystem->list_select_option_by_table("bk_country_code",$usr_dtl['bank_country'],"country_abb","country","",Lang::$word->_PLEASE_SELECT_COUNTRY_CODE);
				?>
			</select>
		</div>
	</div>
	<?php
	if ($aRate != false) {
		if (count($aRate)==1) {
			foreach($aRate as $krow){?>
		    <input type="hidden" name="bank_currency_local" value="<?php echo $krow->currency?>">
			<?php }
		} else {?>
			<div class="form-group">
				<label for="bank_currency_local" class="col-md-2 control-label"><?php echo Lang::$word->_CURRENCY_LOCAL;?></label>
				<div class="col-md-10">
					<!--<select id="bank_country" name="bank_country"class="form-control" >-->
						<select id="bank_currency_local" name="bank_currency_local" class="form-control" >
						<?php
						foreach($aRate as $krow){
							?>
							<option value="<?php echo $krow->currency?>"><?php echo $krow->currency?></option>
							<?php
						}
						?>
						</select>
						
				</div>
			</div>	
		<?php
		}
	}
	?>
	<div class="form-group">
		<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_ACCOUNT_TYPE;?> *</label>
		<div class="col-md-10">
			<label class="radio-inline">
				<input type="radio" name="bank_account_type" id="inline_optionsRadios1"  checked value="saving">
				<?php echo Lang::$word->_SAVING;?>
			</label>
			<label class="radio-inline">
				<input type="radio" name="bank_account_type" id="inline_optionsRadios2" value="current" >
				<?php echo Lang::$word->_CURRENT;?>
			</label>
		</div>
	</div>
	<div class="form-group">
		<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_ACCOUNT_NUMBER;?> *</label>
		<div class="col-md-10">
			<input type="text" id="bank_account_number" name="bank_account_number" class="form-control" value="">
		</div>
	</div>
	<div class="form-group">
		<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_SWIFT;?></label>
		<div class="col-md-10">
			<input type="text" name="bank_swift"id="bank_swift" class="form-control" value="">
		</div>
	</div>
	<div class="form-group">
        <label for="bank_routing_number" class="col-md-2 control-label"><?php echo Lang::$word->_ROUTING_NUMBER;?></label>
        <div class="col-md-10">
            <input type="text" id="bank_routing"name="bank_routing" class="form-control" value="">
        </div>
    </div>
	<div class="form-group">
		<label for="bank_iban" class="col-md-2 control-label"><?php echo Lang::$word->_IBAN;?></label>
		<div class="col-md-10">
			<input type="text" name="bank_iban" id="bank_iban"class="form-control" value="">
		</div>
	</div>
	<div class="form-group">
		<label for="profile_username" class="col-md-2 control-label"><?php echo Lang::$word->_BANK_DOCUMENT;?> *</label>
		<div class="col-md-10 has-warning">
                <label class="control-label" for="inputWarning"><?php echo Lang::$word->_DOCUMENT_CONTROL_INFO1 ;?></label>
                <input type="file" name="file_bank" class="form-control">
		</div>
	</div>
	
	<div class="text-center">
	    <input name="doProfile_bank" type="hidden" value="1">
        <button data-url="/ajax/controller.php" data-frm="prolific_form3"  type="button" name="dosubmit" class="prolific button btn btn-success">
            <i class="fa fa-save"></i><?php echo Lang::$word->_UPDATE_BANK_DETAIL;?></button>
		
	</div>
</form>
</div>
<!-- start lit bank other -->
<?php
function hlist(){
	return '<div class="row"><div class="col-lg-12"><div class="panel-body"><div class="col-lg-12">
		<div class="panel panel-default"><div class="panel-heading">Bank Account Details</div><div class="panel-body">';
}
function flist(){
	return '</div></div></div></div></div></div>';
}
function divAddButton(){
	return '<div class="col-sm-6 trade_acc_outbox" onclick="$(\'#frmAddBankUser\').show();$(\'.trade_acc_box_add\').hide();">
			<a href="javascript:void(0);">
				<div class="col-sm-12 trade_acc_box_add">
					<span class="glyphicon glyphicon-plus"></span><p>Add Bank Account</p>
				</div>
			</a>
		</div>';
}
if (count($_SESSION['usr_dtl']['user_bank_list']) > 0) {
	$aBank = $_SESSION['usr_dtl']['user_bank_list'];
	
	echo hlist();

	$is_pending = false;
	foreach($aBank as $kBank => $vBank){
		$aDtl = json_decode($vBank['data']);
		if ($aDtl->bank_name == 'other') {
			$aDtl->bank_name = $aDtl->bank_name_other;
		}
		?>
		<div class="col-sm-6 trade_acc_outbox">
			<div class="col-sm-12 trade_acc_box">
				<ul>
					<li><span><?php echo Lang::$word->_BANK_NAME;?></span><?php echo ($aDtl->bank_name != '') ? $aDtl->bank_name : '-';?></li>
					<li><span><?php echo Lang::$word->_COUNTRY;?></span><?php echo ($aDtl->bank_country!= '') ?$aDtl->bank_country :'-';?></li>
					<!--
					<li><span><?php echo Lang::$word->_CURRENCY_LOCAL;?></span><?php echo ($aDtl->bank_currency_local!= '') ?$aDtl->bank_currency_local :'-';?></li>
					-->
					<li><span><?php echo Lang::$word->_ACCOUNT_TYPE;?></span><?php echo ($aDtl->bank_account_type!= '') ?$aDtl->bank_account_type: '-';?></li>
					<li><span><?php echo Lang::$word->_ACCOUNT_NUMBER;?></span><?php echo ($aDtl->bank_account_number!= '') ?$aDtl->bank_account_number: '-';?></li>
					<li><span><?php echo Lang::$word->_SWIFT;?></span><?php echo ($aDtl->bank_swift!= '') ?$aDtl->bank_swift:'-' ;?></li>
					<li><span><?php echo Lang::$word->_ROUTING_NUMBER;?></span><?php echo ($aDtl->bank_routing!= '') ?$aDtl->bank_routing: '-';?></li>
					<li><span><?php echo Lang::$word->_IBAN;?></span><?php echo ($aDtl->bank_iban!= '') ?$aDtl->bank_iban:'-' ;?></li>
					<li><span><?php echo Lang::$word->_BANK_DOCUMENT;?></span><a href="<?php echo UPLOADURL;?>file_doc/<?php echo $aDtl->file_url;?>" target="_blank">Document File</a></li>
					<?php if ($vBank['is_verified'] == 0) {
					 $is_pending = true;
					?>
					<li><span><?php echo Lang::$word->_SYS_DBSTATUS;?></span><p class="badge badge-warning"><i class="glyphicon glyphicon-time"></i>  <?php echo Lang::$word->_PENDING;?></p></li>
					<?php }else{?>
					<li><span><?php echo Lang::$word->_SYS_DBSTATUS;?></span><p class="badge badge-success"><i class="glyphicon glyphicon-ok"></i>  <?php echo Lang::$word->__VERIFIED;?></p></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<?php
	}
 	if ($is_pending == false) {
 		echo divAddButton();
 	}
	
	echo flist();
	
} else {
	echo hlist();
	echo divAddButton();
	echo flist();
}
?>

							
							
<!-- end list bank other-->