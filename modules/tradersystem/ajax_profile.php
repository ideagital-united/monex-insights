<?php
  /**
   * User
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: user.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
	define("_VALID_PHP", true);

  	require_once("../../init.php");
    if (!$user->logged_in)
      redirect_to("../index.php");  
   require_once (MODPATH . "tradersystem/admin_class.php");
   
   Registry::set('TraderSystem', new TraderSystem());
  
   include_once(MODPATHF . 'tradersystem/define_by_api.php');

 if (isset($_POST['doProfile_product'])) 
 {
	
	Filter::checkPost('product_type', "Product : This field is require.");
	if (isset($_POST['product_type']) && ($_POST['product_type'] == 1  || $_POST['product_type'] == 3 )) {
		Filter::checkPost('share_trade_experience', "Years of experience in Share Trading : This field is require.");
		Filter::checkPost('share_trade_currency', "Currency would you like  : This field is require.");
	}
	if (isset($_POST['product_type']) && ($_POST['product_type'] == 2  || $_POST['product_type'] == 3 )) {
		Filter::checkPost('fx_experience', "Years of experience in FX : This field is require.");
		Filter::checkPost('fx_currency', "Currency would you like FX : This field is require.");
		Filter::checkPost('fx_leverage', "Leverage  : This field is require.");
		Filter::checkPost('fx_margin_ok', "Understanding margin call  : Please check the box.");
		Filter::checkPost('fx_losses_ok', "Understanding that losses : Please check the box.");
	}
	
	Filter::checkPost('level_income', "Level of Income  : This field is require.");
	if (empty(Filter::$msgs)) {
		$aData = array(
		'product_type' => $_POST['product_type'],
		'share_trade_experience' => $_POST['share_trade_experience'],
		'share_trade_currency' => $_POST['share_trade_currency'],
		'fx_experience' => $_POST['fx_experience'],
		'fx_currency' => $_POST['fx_currency'],
		'fx_leverage' => $_POST['fx_leverage'],
		'fx_margin_ok' => $_POST['fx_margin_ok'],
		'fx_losses_ok' => $_POST['fx_losses_ok'],
		'level_income' => $_POST['level_income'],
		'step_no' => 2
		);
		Registry::get("Database")->update('users', $aData, "email='" . $_SESSION['email'] . "'");
       
		if (Registry::get("Database")->affected()) {
			$json['status'] 		= 'success';
  			
 	 		$json['message'] 		= Filter::msgOk_gob(Lang::$word->_UA_PROFILE_UPDATEOK, false);
  		} else {
	  		$json['status'] 	= 'info';
  			$json['message'] 	= Filter::msgAlert_gob(Lang::$word->_SYSTEM_PROCCESS, false);
  		}
	} else {
		$json['message'] = Filter::msgStatus_gob();
	}
	print json_encode($json);
	exit;
	
 }

 if (isset($_POST['doProfile_acknow'])) 
 {
	
	Filter::checkPost('ack_finance', "Financial Services Guide : This field is require.");
	Filter::checkPost('ack_nda', "Product Disclosure Statement : This field is require.");
	Filter::checkPost('ack_agree', "Terms and Conditions : This field is require.");
	Filter::checkPost('act_acknowledge', "Acknowledgement : This field is require.");
	Filter::checkPost('act_under_risk', "Understanding of risks : This field is require.");
	
	if (empty(Filter::$msgs)) {
		$timenow  	= time();
		$aData 		= array(
			'ack_finance' 	=> $timenow,
			'ack_nda' 		=> $timenow,
			'ack_agree' 	=> $timenow,
			'act_acknowledge'=> $timenow,
			'act_under_risk' => $timenow
		);
		Registry::get("Database")->update('users', $aData, "email='" . $_SESSION['email'] . "'");
       
		if (Registry::get("Database")->affected()) {
			$json['status'] 		= 'success';
  			
 	 		$json['message'] 		= Filter::msgOk_gob(Lang::$word->_UA_PROFILE_UPDATEOK, false);
  		} else {
	  		$json['status'] 	= 'info';
  			$json['message'] 	= Filter::msgAlert_gob(Lang::$word->_SYSTEM_PROCCESS, false);
  		}
	} else {
		$json['message'] = Filter::msgStatus_gob();
	}
	print json_encode($json);
	exit;
	
 } 

?>
