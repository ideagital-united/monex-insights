<?php 
$is_mt4  	= (issetvalue($_SESSION['adminconf']['mt4_api_url']) != '') 	? 1 : 0;
$is_ct 		= (issetvalue($_SESSION['adminconf']['ct_api_url']) != '') 		? 1 : 0;

?>
<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3><?php echo Lang::$word->_DOWNLOAD;?></h3></div>
					<div class="panel-body">
						<div class="col-md-12">
							<div class="heading_a"></div>
							<ul class="nav nav-tabs download_tabs" id="tabs_a">
								<?php if ($is_mt4 == 1) { ?>
								<li class="active"><a data-toggle="tab" href="#MetaTrader"><?php echo Lang::$word->_METATRADER4;?></a></li>
								<?php } ?>
								<?php if ($is_ct == 1) { ?>
								<li ><a data-toggle="tab" href="#cTrader"><?php echo Lang::$word->_TRADER;?></a></li>
								<?php } ?>
							</ul>
							<div class="tab-content download_tabs_left" id="tabs_content_a">
								<?php if ($is_ct == 1) { ?>
								<div id="cTrader" class="tab-pane fade in active">
									<div class="tabbable tabs-left download_tabs_box">
										<ul class="nav nav-tabs" id="tabs_c">
											<li class="active"><a data-toggle="tab" href="#cTrader_Desktop"><?php echo Lang::$word->_TRADER_DESK;?></a></li>
											<li><a data-toggle="tab" href="#cTrader_Web"><?php echo Lang::$word->_TRADER_WEB;?></a></li>
											<li><a data-toggle="tab" href="#cTrader_Mobile_Web"><?php echo Lang::$word->_TRADER_MOBILE_WEB;?></a></li>
											<li><a data-toggle="tab" href="#cAlgo"><?php echo Lang::$word->_CALGO;?></a></li>
										</ul>
										<div class="tab-content" id="tabs_content_c">
											<div id="cTrader_Desktop" class="tab-pane fade in active">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/ctrader.gif" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/cTrader_Desktop.jpg" ></br>
														</div>
														<p>
															<b><?php echo Lang::$word->_TRADER;?></b> <?php echo Lang::$word->_PLATFROM_INFO1;?>
															</br><?php echo Lang::$word->_PLATFROM_INFO2;?>
														</p>
														<div class="text-center">
															<a href="https://www.spotware.ctrader.com/ctrader-spotware-setup.exe" class="downbuttons ctraderdownloadbtn contains_video" ></a>
														</div>
													</div>
												</div>
											</div>
											<div id="cTrader_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/ctweb.gif" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/ctweb5.png" ></br>
														</div>
														<p>
															<b><?php echo Lang::$word->_TRADER_WEB;?></b><?php echo Lang::$word->_PLATFROM_INFO3;?>
															</br><?php echo Lang::$word->_PLATFROM_INFO4;?>
														</p>
														<p>
															<div class="col-md-6">
																<ul>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO5;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO6;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO7;?></li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO8;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO9;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO10;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO11;?></li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<a href="http://ct.spotware.com/" class="downbuttons webctraderlaunch" target="_blank" ></a>
														</div>
													</div>
												</div>
											</div>
											<div id="cTrader_Mobile_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/ctrader_mobile_w.png" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/5-26379.gif" ></br>
														</div>
														<p>
															<?php echo Lang::$word->_PLATFROM_INFO12;?><b><?php echo Lang::$word->_TRADER_WEB;?></b> <?php echo Lang::$word->_PLATFROM_INFO12_1;?>
															</br><?php echo Lang::$word->_PLATFROM_INFO13;?>
														</p>
														<p>
															<div class="col-md-6">
																<ul>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO14;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO15;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO16;?></li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO17;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO18;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO19;?></li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<a class="downbuttons googledownload" href="https://play.google.com/store/apps/details?id=com.spotware.ct" target="_blank"></a>
															<a class="downbuttons appledownload" href="https://itunes.apple.com/us/app/spotware-ctrader/id767428811?ls=1&amp;mt=8" target="_blank"></a>
														</div>
													</div>
												</div>
											</div>
											<div id="cAlgo" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/calgo.gif" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/cAlgo_logo.png"  style="width:300px;"></br></br></br>
														</div>
														<p>
															<?php echo Lang::$word->_PLATFROM_INFO20;?><b><?php echo Lang::$word->_PLATFROM_INFO20_1;?></b> <?php echo Lang::$word->_PLATFROM_INFO20_2;?> <b> <?php echo Lang::$word->_PLATFROM_INFO20_3;?></b> using C#.
															</br><?php echo Lang::$word->_PLATFROM_INFO21;?>
														</p>
														<p>
															<div class="col-md-6">
																<ul >
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO22;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO23;?></li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul >
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO24;?></li>
																	<li>
																	<?php echo Lang::$word->_PLATFROM_INFO25;?></li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<a href="http://spotware.ctrader.com/calgo-spotware-setup.exe" class="downbuttons calgodownloadbtn " ></a>
														</div>
													</div>
												</div>
											</div> -->
										</div>
									</div>
								</div>
								<!--close ctrade -->
								<?php } ?>
								<?php if ($is_mt4 == 1) { ?>
								<div id="MetaTrader" class="tab-pane fade in active">
									<div class="tabbable tabs-left download_tabs_box">
										<ul class="nav nav-tabs" id="tabs_c">
											<li class="active"><a data-toggle="tab" href="#MetaTrader_Desktop"><?php echo Lang::$word->_MT4_DESKTOP;?></a></li>
											<!--<li><a data-toggle="tab" href="#MetaTrader_Web"><?php echo Lang::$word->_MT4_TAB;?></a></li>-->
											<!--<li><a data-toggle="tab" href="#MetaTrader_Mobile_Web">MetaTrader 4 iOS iPhone app</a></li>-->
											<li><a data-toggle="tab" href="#MT4_Android">MetaTrader 4 Android app</a></li>
											<li><a data-toggle="tab" href="#MT4_IOS">MetaTrader 4 iOS iPhone app</a></li>
										</ul>
										<div class="tab-content" id="tabs_content_c">
											<div id="MetaTrader_Desktop" class="tab-pane fade in active">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/mt4_01.png" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/meta4-page.png" ></br>
														</div>
														<p>
															<b><?php echo Lang::$word->_PLATFROM_INFO30;?></b>
															</br><?php echo Lang::$word->_PLATFROM_INFO31;?>
															</br><?php echo Lang::$word->_PLATFROM_INFO32;?>
														</p>
														<p>
															<div class="col-md-6">
																<ul>
																	<li><?php echo Lang::$word->_PLATFROM_INFO33;?></li>
																	<li><?php echo Lang::$word->_PLATFROM_INFO34;?></li>
																	<li><?php echo Lang::$word->_PLATFROM_INFO35;?></li>
																	<li><?php echo Lang::$word->_PLATFROM_INFO36;?></li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul>
																	<li><?php echo Lang::$word->_PLATFROM_INFO37;?></li>
																	<li><?php echo Lang::$word->_PLATFROM_INFO38;?></li>
																	<li><?php echo Lang::$word->_PLATFROM_INFO39;?></li>
																	<li><?php echo Lang::$word->_PLATFROM_INFO40;?></li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<button class="btn btn-success  btn-lg"><a href="http://www.freedomcfd.com/platform/FCFDglobal_MT4_setup.exe"><?php echo Lang::$word->_DOWNLOAD_MT4;?></a></button>
														</div>
													</div>
												</div>
											</div>

											<!--<div id="MetaTrader_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<?php echo Lang::$word->_IMAGE;?>
													</div>
													<div class="col-md-6">
														<?php echo Lang::$word->_PLATFROM_INFO42;?>
														</br><?php echo Lang::$word->_PLATFROM_INFO43;?>
												</div>
											</div>

											<div id="MetaTrader_Mobile_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/all-platform.png" >
														</div>
													</div>
													<div class="col-md-6">
														<?php echo Lang::$word->_PLATFROM_INFO42;?>
														<br><?php echo Lang::$word->_PLATFROM_INFO43;?>
													</div>
													<div class="text-center">
															<button class="btn btn-success  btn-lg"><?php echo Lang::$word->_DOWNLOAD_MT4;?></button>
													</div>
												</div>
											</div>-->

											<div id="MT4_Android" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/slide-mobile.jpg" >
														</div>
													</div>
													<div class="col-md-6">
														<h2>MetaTrader 4 Android app</h2>
														<P>Trade Forex from your smartphone or tablet!
														MetaTrader 4 is the world's most popular Forex trading platform. Choose from hundreds of brokers and thousands of servers to trade with your MetaTrader 4 Android app. Control your account, trade and analyze the Forex market using technical indicators and graphical objects.

														<b>TRADING</b>
														<ul>
														<i>Real-time quotes of financial instruments</i>
														<i>Full set of trade orders, including pending orders</i>
														<i>All types of trade execution</i>
														<i>Detailed trading history</i>
														</ul>

														<b>ADVANCED FUNCTIONALITY</b>
														<ul>
														<i>Fast switch between financial instruments on charts</i>
														<i>Sound notifications</i>
														<i>Customizable chart color scheme</i>
														<i>Trade levels visualizing the prices of pending order, as well as SL and TP values on the chart</i>
														<i>Free financial news — dozens of materials daily</i>
														<i>Chat with any registered MQL5.community user</i>
														<i>Support for push notifications from the desktop MetaTrader 4 platform and MQL5.community services</i>
														</ul>
														<b>TECHNICAL ANALYSIS</b>
														<ul>
														<i>Interactive real-time charts with zoom and scroll options</i>
														<i>30 most popular technical indicators</i>
														<i>24 analytical objects: lines, channels, geometric shapes, as well as Gann, Fibonacci and Elliott tools</i>
														<i>9 timeframes: M1, M5, M15, M30, H1, H4, D1, W1 and MN</i>
														<i>3 types of charts: bars, Japanese candlesticks and broken line</i>
														</ul>
														<p>Download MetaTrader 4 for Android on your smartphone or tablet and trade Forex anytime and anywhere in the world!</p>
														
														<div class="text-center">
															<button class="btn btn-success  btn-lg"><a href="https://play.google.com/store/apps/details?id=net.metaquotes.metatrader4&hl=en&utm_campaign=www.metatrader4.com"><?php echo Lang::$word->_DOWNLOAD_MT4;?></a></button>
													</div>
													</div>
													
												</div>
											</div>

											<div id="MT4_IOS" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="<?php echo THEMEURLTRADE;?>assets/img/platform/iphonebipadomt5_new.png" >
														</div>
													</div>
													<div class="col-md-6">
														<h2>MetaTrader 4 iOS iPhone app</h2>
														<p>We are offers traders the ability to deal on your market leading MetaTrader 4 True ECN 
														account with unrivalled execution speeds from your iPhone or iPad. The result is a mobile forex trading 
														platform that give you unlimited flexibility without comprising execution speed or quality.</p>

														<p>Our iPhone application a offers one click trading, the ability to customise your layouts, 
														trading from multiple in addition to advanced charts and analysis. Now traders have instant access 
														to the financial markets at their fingertips, with the ability to fully manage a portfolio on the move. 
														Our for iPhone use many of the advanced features of the iPhone, such as the landscape-to-portrait view changer 
														(a simple rotation of the iPhone).</p>
														<h4>Trade Forex on the go with your iPhone or iPad!</h4>
														<b>Features</b>
														<ul>
														<li>Real-time forex and CFD quotes</li>
														<li>Full set of trade orders, including pending orders</li>
														<li>Trade directly from the chart</li>
														<li>Support of all types of execution modes</li>
														<li>View your complete trading history</li>
														<li>Real-time interactive charts with zoom and scroll</li>
														<li>30 of the most popular technical indicators</li>
														<li>7 timeframes: M1, M5, M15, M30, H1, H4 and D1</li>
														<li>3 chart types: Bars, Japanese Candlesticks and Line</li>
														<li>Adjustable settings of technical indicators (colour, line width)</li>
														</ul>

													</div>
													<div class="text-center">
															<button class="btn btn-success  btn-lg"><a href="https://itunes.apple.com/en/app/metatrader-4/id496212596?hl=en&utm_campaign=www.metatrader4.com"><?php echo Lang::$word->_DOWNLOAD_MT4;?></a></button>
													</div>
												</div>
											</div>


											
										</div>
									</div>
								</div>
							</div>
								<?php } ?>
							<!-- close tab all -->
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>