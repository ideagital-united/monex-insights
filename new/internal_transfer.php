<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Avaliable funds in wallet is USD$ 56,446.50</h3></div>
					<div class="panel-body">
						<table  id="dt_basic" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Account Number</th>
									<th>Account Type</th>
									<th>Account Balance</th>
									<th>Operation</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 15%;">534153</td>
									<td style="width: 15%;">MetaTrader4</td>
									<td style="width: 15%;">USD$320.80</td>
									<td>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon">USD$</span>
												<input class="form-control" id="appendedPrependedInput" type="text">
												
											</div>
											
										</div>
										<button class="btn btn-success">Deposit to trading account</button>
										<button class="btn btn-danger">Withdraw to wallet</button>
									</td>
								</tr>
								<tr>
									<td style="width: 15%;">534153</td>
									<td style="width: 15%;">MetaTrader4</td>
									<td style="width: 15%;">USD$320.80</td>
									<td>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon">USD$</span>
												<input class="form-control" id="appendedPrependedInput" type="text">
												
											</div>
											
										</div>
										<button class="btn btn-success">Deposit to trading account</button>
										<button class="btn btn-danger">Withdraw to wallet</button>
									</td>
								</tr>
								<tr>
									<td style="width: 15%;">534153</td>
									<td style="width: 15%;">MetaTrader4</td>
									<td style="width: 15%;">USD$320.80</td>
									<td>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon">USD$</span>
												<input class="form-control" id="appendedPrependedInput" type="text">
												
											</div>
											
										</div>
										<button class="btn btn-success">Deposit to trading account</button>
										<button class="btn btn-danger">Withdraw to wallet</button>
									</td>
								</tr>
								<tr>
									<td style="width: 15%;">534153</td>
									<td style="width: 15%;">MetaTrader4</td>
									<td style="width: 15%;">USD$320.80</td>
									<td>
										<div class="col-md-4">
											<div class="input-group">
												<span class="input-group-addon">USD$</span>
												<input class="form-control" id="appendedPrependedInput" type="text">
												
											</div>
											
										</div>
										<button class="btn btn-success">Deposit to trading account</button>
										<button class="btn btn-danger">Withdraw to wallet</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Internal Transfer History</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Transection Number</th>
									<th>Date</th>
									<th>Type</th>
									<th>Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Deposit to account: 3458623</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Withdrow from account: 3458623</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Deposit to account: 3458623</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>