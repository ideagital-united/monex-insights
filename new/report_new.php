<div class="page_content">
	<div class="container-fluid">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="user_profile">
						<ul class="nav nav-tabs" id="tabs_a">
							<li class="active"><a data-toggle="tab" href="#mt4">MetaTrade 4</a></li>
							<li><a data-toggle="tab" href="#ctrader">cTrader</a></li>
						</ul>
						<!--Tab 01 Start-->
						<div class="tab-content" id="tabs_content_a">
						<div id="mt4" class="tab-pane">
							<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Report</h3>
					</div>
					<div class="panel-body report_left">
						<p>Daily:<span>2.51%</span></p>
						<p>Monthly:<span>2.51%</span></p>
						<p class="heading_b"></p>
						<p>Balance:<span>$2.51</span></p>
						<p>Equity:<span>$2.51</span></p>
						<p class="heading_b"></p>
						<p>Deposits:<span>$2.51</span></p>
						<p>Withdrawals:<span>$2.51</span></p>
						<hr>
						<h6><b>Select Data Range to Display.</b></h6>
						<form class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-6 ">
									<input class="form-control" type="text" placeholder="Start date" id="dpStart" data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
								<div class="col-sm-6 ">
									<input class="form-control" type="text" placeholder="End date" id="dpEnd" data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="nvd3_cumulativeLine" class="chart" style="width:100%;height:354px"><svg></svg></div>
					</div>
				</div>
			</div>
		</div>
		
		
						</div>
						</div>
						<!--Tab 01 end-->
						<!--Tab 02 Start-->
						<div class="tab-content" id="tabs_content_a">
						<div id="ctrader" class="tab-pane">
							<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Report CTARDE</h3>
					</div>
					<div class="panel-body report_left">
						<p>Daily:<span>2.51%</span></p>
						<p>Monthly:<span>2.51%</span></p>
						<p class="heading_b"></p>
						<p>Balance:<span>$2.51</span></p>
						<p>Equity:<span>$2.51</span></p>
						<p class="heading_b"></p>
						<p>Deposits:<span>$2.51</span></p>
						<p>Withdrawals:<span>$2.51</span></p>
						<hr>
						<h6><b>Select Data Range to Display.</b></h6>
						<form class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-6 ">
									<input class="form-control" type="text" placeholder="Start date" id="dpStart" data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
								<div class="col-sm-6 ">
									<input class="form-control" type="text" placeholder="End date" id="dpEnd" data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="nvd3_cumulativeLine2222222" class="chart" style="width:100%;height:354px"><svg></svg></div>
					</div>
				</div>
			</div>
		</div>
		
		
						</div>
						</div>
						
						<!--Tab 02 end-->
						</div>
</div>
</div>	
</div>					
		<!-- start data -->
		
	</div>
</div>
