<div class="page_content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-danger"><center><strong>Important:</strong> You need to verify your mobile phone via SMS of Google Authenticator before you can make a withdraw.</center></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-default lets_hover">
							<div class="stat_box">
								<div class="stat_ico color_f"><span class="fa fa-usd"></span></div>
								<div class="stat_content">
									<span class="stat_count">$ 11,035.24</span>
									<span class="stat_name">Total Account's Balance</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-default lets_hover">
							<div class="stat_box">
								<div class="stat_ico color_g"><span class="fa fa-usd"></span></div>
								<div class="stat_content">
									<span class="stat_count">$ 1,083.61</span>
									<span class="stat_name">Total Commission</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-default lets_hover">
							<a href="?page=download">
								<div class="stat_box ">
									<div class="stat_ico color_a"><span class="glyphicon glyphicon-download-alt"></span></div>
									<div class="stat_content">
										<span class="stat_count">Download</span>
										<span class="stat_name">Go to Download Section</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-default lets_hover">
							<a href="#">
								<div class="stat_box ">
									<div class="stat_ico color_d"><i class="ion-ios7-help-outline"></i></div>
									<div class="stat_content">
										<span class="stat_count">Help</span>
										<span class="stat_name">Go to Help Section</span>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="user_profile">
						<ul class="nav nav-tabs" id="tabs_a">
							<li class="active"><a data-toggle="tab" href="#mt4">MetaTrade 4</a></li>
							<li><a data-toggle="tab" href="#ctrader">cTrader</a></li>
						</ul>
						<!--Tab 01 Start-->
						<div class="tab-content" id="tabs_content_a">
							<div id="mt4" class="tab-pane fade in active">
								<div class="heading_b">MT4 Portfolios</div>
								<div class="row">
									<div class="col-md-7">
										<table class="table table-striped">
											<thead>
												<tr>
													<th>My Portfolios</th>
													<th class="col_md sub_col">Value</th>
													<th class="col_md sub_col">% Value</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><a href="#">MT4:231654</a></td>
													<td class="sub_col">$1,428</td>
													<td class="sub_col">54%</td>
												</tr>
												<tr>
													<td><a href="#">MT4:465231</a></td>
													<td class="sub_col">$858</td>
													<td class="sub_col">21%</td>
												</tr>
												<tr>
													<td><a href="#">MT4:789654</a></td>
													<td class="sub_col">$647</td>
													<td class="sub_col">11%</td>
												</tr>
												<tr>
													<td><a href="#">cTrader:465897</a></td>
													<td class="sub_col">$433</td>
													<td class="sub_col">6%</td>
												</tr>
												<tr>
													<td><a href="#">cTrader:465321</a></td>
													<td class="sub_col">$141</td>
													<td class="sub_col">2%</td>
												</tr>
											</tbody>
											<tfoot>
											<tr class="active">
												<td><b><a href="#">Total</a></b></td>
												<td class="sub_col"><b>$3,507</b></td>
												<td class="sub_col"><b>100%</b></td>
											</tr>
											
											</tfoot>
										</table>
									</div>
									<div class="col-md-5">
										<div id="flot_browsers" class="chart" style="height:240px;width:100%">
											<script>
												chart_browsers_data = [
													{ label: "MT4:231654", data: 1428, color: '#1f77b4' },
													{ label: "MT4:465231", data: 858, color: '#aec7e8' },
													{ label: "MT4:789654", data: 647, color: '#ff7f0e' },
													{ label: "cTrader:465897", data: 433, color: '#ffbb78' },
													{ label: "cTrader:465321", data: 141, color: '#2ca02c' }
												];
											</script>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--Tab MT4 End-->
					<!--Tab cTrader Start-->
					<div class="tab-content" id="tabs_content_a">
						<div id="ctrader" class="tab-pane">
							<div class="heading_b">McTrader Portfolios</div>
							<div class="row">
								<div class="col-md-7">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>My Portfolios</th>
												<th class="col_md sub_col">Value</th>
												<th class="col_md sub_col">% Value</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><a href="#">MT4:231654</a></td>
												<td class="sub_col">$1,428</td>
												<td class="sub_col">54%</td>
											</tr>
											<tr>
												<td><a href="#">MT4:465231</a></td>
												<td class="sub_col">$858</td>
												<td class="sub_col">21%</td>
											</tr>
											<tr>
												<td><a href="#">MT4:789654</a></td>
												<td class="sub_col">$647</td>
												<td class="sub_col">11%</td>
											</tr>
											<tr>
												<td><a href="#">cTrader:465897</a></td>
												<td class="sub_col">$433</td>
												<td class="sub_col">6%</td>
											</tr>
											<tr>
												<td><a href="#">cTrader:465321</a></td>
												<td class="sub_col">$141</td>
												<td class="sub_col">2%</td>
											</tr>
										</tbody>
										<tfoot>
										<tr class="active">
											<td><b><a href="#">Total</a></b></td>
											<td class="sub_col"><b>$3,507</b></td>
											<td class="sub_col"><b>100%</b></td>
										</tr>
										
										</tfoot>
									</table>
								</div>
								<div class="col-md-5">
									<div id="flot_browsers" class="chart" style="height:240px;width:100%">
										<script>
											chart_browsers_data = [
												{ label: "MT4:231654", data: 1428, color: '#1f77b4' },
												{ label: "MT4:465231", data: 858, color: '#aec7e8' },
												{ label: "MT4:789654", data: 647, color: '#ff7f0e' },
												{ label: "cTrader:465897", data: 433, color: '#ffbb78' },
												{ label: "cTrader:465321", data: 141, color: '#2ca02c' }
											];
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--Tab cTrader end-->
			</div>
		</div>
	</div>
</div>

</div>
</div>
</div>