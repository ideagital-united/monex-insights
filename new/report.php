<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-lg  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Select Account to Display <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Separated link</a></li>
						</ul>
					</div>
					<div class="panel-heading">
						<h4>MT4:4564512</h4>
					</div>
					<div class="panel-body report_left">
						<p>Daily:<span>2.51%</span></p>
						<p>Monthly:<span>2.51%</span></p>
						<p class="heading_b"></p>
						<p>Balance:<span>$2.51</span></p>
						<p>Equity:<span>$2.51</span></p>
						<p class="heading_b"></p>
						<p>Deposits:<span>$2.51</span></p>
						<p>Withdrawals:<span>$2.51</span></p>
						<hr>
						
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="nvd3_cumulativeLine" class="chart" style="width:100%;height:300px"><svg></svg></div>
						<h6><b>Select Data Range to Display.</b></h6>
						<form class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-4 ">
									<input class="form-control" type="text" placeholder="Start date" id="dpStart" data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
								<div class="col-sm-4 ">
									<input class="form-control" type="text" placeholder="End date" id="dpEnd" data-date-format="dd/mm/yyyy" data-date-autoclose="true">
								</div>
								<div class="col-sm-4 "><button type="button" class="btn btn-success col-sm-12">Display Data From This Range</button></div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Deposits History</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Transection Number</th>
									<th>Date</th>
									<th>Type</th>
									<th>Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Withdraw History</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools2" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Transection Number</th>
									<th>Date</th>
									<th>Type</th>
									<th>Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>