<div class="page_content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel-body">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">MetaTrader MT4 Account</div>
							<div class="panel-body">
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
										<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
										<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
										<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<a href="#" data-toggle="modal" data-target="#bs_modal_regular">
										<div class="col-sm-12 trade_acc_box_add">
											<span class="glyphicon glyphicon-plus"></span><p>OPEN NEW TRADING ACCOUNT</p>
										</div>
									</a>
								</div>
							</div>
							<div class="panel-heading">MetaTrader MT4 Demo Account</div>
							<div class="panel-body">
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
										<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
																				<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
																				<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<a href="#">
										<div class="col-sm-12 trade_acc_box_add">
											<span class="glyphicon glyphicon-plus"></span><p>OPEN NEW TRADING ACCOUNT</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">cTrader Account</div>
							<div class="panel-body">
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
																				<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
																				<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<a href="#">
										<div class="col-sm-12 trade_acc_box_add">
											<span class="glyphicon glyphicon-plus"></span><p>OPEN NEW TRADING ACCOUNT</p>
										</div>
									</a>
								</div>
							</div>
							<div class="panel-heading">cTrader Demo Account</div>
							<div class="panel-body">
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
																				<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
																				<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<div class="col-sm-12 trade_acc_box">
																				<ul>
											<li><span>Acc No</span>1452XXXX</li>
											<li><span>Acc Type</span>MINI</li>
											<li><span>Leverage</span>1:100</li>
											<li><span>Trader Password</span>ZAAAFDF</li>
											<li><span>Invest Password</span>ZAAAFDF</li>
											<li><span>Balance</span>1,250.23 USD</li>
											<li><span>Manage this account</span>
												<div class="btn-group dropup">
												  <button type="button" class="pop_change_data_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												    <i class="fa fa-pencil-square"></i>
												  </button>
												  <ul class="dropdown-menu pop_change_data_button_menu">
												    <li><a href="#">Action</a></li>
												    <li><a href="#">Another action</a></li>
												    <li><a href="#">Something else here</a></li>
												    <li><a href="#">Separated link</a></li>
												  </ul>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6 trade_acc_outbox">
									<a href="#">
										<div class="col-sm-12 trade_acc_box_add">
											<span class="glyphicon glyphicon-plus"></span><p>OPEN NEW TRADING ACCOUNT</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="bs_modal_regular">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>OPEN NEW TRADING ACCOUNT</h3>
			<form class="form-horizontal">
				<div class="form-group">
					<label for="user_lname" class="col-md-5 control-label">Select account Type</label>
					<div class="col-md-7">
						<select  class="form-control" >
							<option value="01">Mini (1:500)</option>
							<option value="02">Standard (1:300)</option>
							<option value="03">Pro (1:100)</option>
						</select>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Open Account</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>