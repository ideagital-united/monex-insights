<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>
						Make a Withdraw 
						<a href="#" data-toggle="modal" data-target="#frm_mail">mail</a>
						<a href="#" data-toggle="modal" data-target="#frm_sms">sms</a>
						<a href="#" data-toggle="modal" data-target="#frm_google">google</a>
						
						<span class="badge badge-success big">Avaliable funds in wallet is USD$ 56,446.50</span>
						</h3>
					</div>
					<div class="panel-body">
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Bank Transfer
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_bank_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Paypal
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Paysbuy
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Skrill
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
									</a>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Withdraw History</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Transection Number</th>
									<th>Date</th>
									<th>Type</th>
									<th>Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="modal_bank_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Make a Withdraw</h3>
			<div class="panel panel-default">
				<div class="panel-heading">Bank Transfer Information</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<td></td>
									<td>Bank name</td>
									<td>Account Number</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option2" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<form class="form-horizontal">
				<div class="form-group">
					<label for="user_lname" class="col-md-4 control-label">Amount</label>
					<div class="col-md-6 input-group">
						<span class="input-group-addon">USD $</span>
						<input class="form-control" id="appendedPrependedInput" type="text">
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
					<button class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</button>
				</div>
			</form>
			</div>
			
		</div>
	</div>
</div>
</div>
<!-- modal -->
<!-- start google -->
<div id="frm_google" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<div class="heading_b">Two-Step Verifycation</div>
				<form method="post" name="prolific_form_verify" id="prolific_form_verify" class="form-horizontal">
					<div class="form-group">
						<label class="col-md-5 control-label" for="user_lname"><img src="http://demov2.brokers.solutions/theme/trade_member/assets/img/googleauth.PNG" class="img-thumbnail "></label>
						<div class="col-md-7">
							Please use Google Authenticator to setup verification code otp. For android go to Google Play. For iOS got to App Store.
						</div>
					</div>
					<div class="text-center">
						<span id="transfer_detail">
							<table width="90%">
								<tbody>
									<tr>
										<td width="50%" class="text-right">From Account</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;Jane Doe</td>
									</tr>
									<tr>
										<td width="50%" class="text-right">Amount</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;7 USD</td>
									</tr>
									<tr>
										<td width="50%" class="text-right">Transfer On</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;June 26, 2015</td>
									</tr>
									<tr>
										<td width="50%" class="text-right">Withdraw To</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;BANKNAME : kasikorn
										<br>
										Account Number : 325362555</td>
									</tr>
								</tbody>
							</table></span>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="user_lname"> OTP </label>
						<div class="col-md-6 input-group">
							<input type="hidden" value="813" id="list_pkid" name="list_pkid">
							<input type="hidden" value="1" name="withdraw_step2">
							<span class="input-group-addon codeotp_offline"></span>
							<input type="text" placeholder"otp"="" name="code_verify" id="code_verify" class="form-control">
						</div>
					</div>
					<div class="text-center">
						<button class="prolific button btn btn-success" name="dosubmit" type="button" data-frm="prolific_form_verify" data-url="/modules/tradersystem/ajax_user.php">
							<i class="glyphicon glyphicon-check"></i> Verify
						</button>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- end google -->

<!-- start sms -->
<div id="frm_sms" class="modal fade" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<div class="heading_b">
					Two-Step Verifycation
				</div>
				<form method="post" name="prolific_form_verify" id="prolific_form_verify" class="form-horizontal">
					<div class="form-group">
						<div class="col-md-12 control-label " style="  text-align: center;" ><img src="http://demov2.brokers.solutions/theme/trade_member/assets/img/googleauth.PNG" ></div>
						<div class="col-md-12">
							Verify token send to your registered mobile. Please check your SMS for the token and fill the form below.
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">From Account</label>
						<div class="col-md-8">
							<p>Jane Doe</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Amount</label>
						<div class="col-md-8">
							<p>6 USD</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Transfer On</label>
						<div class="col-md-8">
							<p>June 26, 2015</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Withdraw To</label>
						<div class="col-md-8">
							<p>BANKNAME : kasikorn</p>
							<p>Account Number : 325362555</p>
						</div>
					</div>
					<div class="form-group_last">
						<label class="col-md-4 control-label" for="user_lname"> OTP </label>
						<div class="col-md-6 input-group">
							<input type="hidden" value="814" id="list_pkid" name="list_pkid">
							<input type="hidden" value="1" name="withdraw_step2">
							<span class="input-group-addon codeotp_offline">
								<input type="hidden" value="GAuth" name="key_verify">AQSWDE</span>
							<input type="text" placeholder"otp"="" name="code_verify" id="code_verify" class="form-control">
						</div>
					</div>
					<div class="text-center">
						<button class="prolific button btn btn-success" name="dosubmit" type="button" data-frm="prolific_form_verify" data-url="/modules/tradersystem/ajax_user.php">
							<i class="glyphicon glyphicon-check"></i> Verify
						</button>
						<button class="prolific button btn btn-success bu_req_otp" name="bu_req_otp" type="button" data-divshow="codeotp_offline">
							<i class="glyphicon glyphicon-refresh"></i> Request Code Again
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- end sms -->

<!-- start box email-->
<div id="frm_mail" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<div class="heading_b">Two-Step Verifycation</div>
				<form method="post" name="prolific_form_verify" id="prolific_form_verify" class="form-horizontal">
					<div class="text-center">
						Verify token send to your email. Please check your email for the token and fill the form below.
					</div>
					<div class="text-center">
						<span id="transfer_detail">
							<table width="90%">
								<tbody>
									<tr>
										<td width="50%" class="text-right">From Account</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;Jane Doe</td>
									</tr>
									<tr>
										<td width="50%" class="text-right">Amount</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;12 USD</td>
									</tr>
									<tr>
										<td width="50%" class="text-right">Transfer On</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;June 26, 2015</td>
									</tr>
									<tr>
										<td width="50%" class="text-right">Withdraw To</td><td class="text-left">&nbsp;&nbsp;:&nbsp;&nbsp;BANKNAME : ddddddddddd
										<br>
										Account Number : 111111</td>
									</tr>
								</tbody>
							</table></span>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="user_lname"> OTP </label>
						<div class="col-md-6 input-group">
							<input type="hidden" value="812" id="list_pkid" name="list_pkid">
							<input type="hidden" value="1" name="withdraw_step2">
							<span class="input-group-addon codeotp_offline">
								<input type="hidden" value="GAuth" name="key_verify">FDERGT</span>
							<input type="text" placeholder"otp"="" name="code_verify" id="code_verify" class="form-control">
						</div>
					</div>
					<div class="text-center">
						<button class="prolific button btn btn-success" name="dosubmit" type="button" data-frm="prolific_form_verify" data-url="/modules/tradersystem/ajax_user.php">
							<i class="glyphicon glyphicon-check"></i> Verify
						</button>
						<button class="prolific button btn btn-success bu_req_otp" name="bu_req_otp" type="button" data-divshow="codeotp_offline">
							<i class="glyphicon glyphicon-refresh"></i> Request Code Again
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- end box email-->


<div class="modal fade" id="modal_e_currency_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Make a Withdraw</h3>
			<form class="form-horizontal">
				<div class="form-group">
					<div class="row">
						<label for="user_lname" class="col-md-4 control-label">Amount</label>
						<div class="col-md-6 input-group">
							<span class="input-group-addon">USD $</span>
							<input class="form-control" id="appendedPrependedInput" type="text">
						</div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Withdraw</button>
					<button class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>