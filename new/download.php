<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Download</h3></div>
					<div class="panel-body">
						<div class="col-md-12">
							<div class="heading_a"></div>
							<ul class="nav nav-tabs download_tabs" id="tabs_a">
								<li class="active"><a data-toggle="tab" href="#cTrader">cTrader</a></li>
								<li><a data-toggle="tab" href="#MetaTrader">MetaTrader 4</a></li>
							</ul>
							<div class="tab-content download_tabs_left" id="tabs_content_a">
								<div id="cTrader" class="tab-pane fade in active">
									<div class="tabbable tabs-left download_tabs_box">
										<ul class="nav nav-tabs" id="tabs_c">
											<li class="active"><a data-toggle="tab" href="#cTrader_Desktop">cTrader Desktop</a></li>
											<li><a data-toggle="tab" href="#cTrader_Web">cTrader Web</a></li>
											<li><a data-toggle="tab" href="#cTrader_Mobile_Web">cTrader Mobile Web</a></li>
											<li><a data-toggle="tab" href="#cAlgo">cAlgo</a></li>
										</ul>
										<div class="tab-content" id="tabs_content_c">
											<div id="cTrader_Desktop" class="tab-pane fade in active">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/ctrader.gif" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/cTrader_Desktop.jpg" ></br>
														</div>
														<p>
															<b>cTrader</b> provides full STP access to the currency trading markets for professional and new traders.
															</br>We route your trades using STP networks, allowing you to trade directly with prominent global banks, freeing you from the inefficiencies of market maker models and opening the door to transparent trading in a live environment.
														</p>
														<div class="text-center">
															<a href="https://www.spotware.ctrader.com/ctrader-spotware-setup.exe" class="downbuttons ctraderdownloadbtn contains_video" ></a>
														</div>
													</div>
												</div>
											</div>
											<div id="cTrader_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/ctweb.gif" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/ctweb5.png" ></br>
														</div>
														<p>
															<b>cTrader Web</b> brings the transparent pricing, lightning-fast speed, and all the features of cTrader to your browser.
															</br>Now you can receive the best in FX trading, from any computer with internet access.
														</p>
														<p>
															<div class="col-md-6">
																<ul>
																	<li>
																	Trade from any location</li>
																	<li>
																	All major browsers supported</li>
																	<li>
																	Use the same account on cTrader and cTrader Web</li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul>
																	<li>
																	Transparent, level II pricing</li>
																	<li>
																	Advanced charting and technical analysis</li>
																	<li>
																	One-click trading options</li>
																	<li>
																	Mobile trading also available</li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<a href="http://ct.spotware.com/" class="downbuttons webctraderlaunch" target="_blank" ></a>
														</div>
													</div>
												</div>
											</div>
											<div id="cTrader_Mobile_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/ctrader_mobile_w.png" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/5-26379.gif" ></br>
														</div>
														<p>
															Trade and manage your account from anywhere in the world with <b>cTrader Mobile</b> Web for phones and tablets.
															</br>This portable version of cTrader can be accessed from any iOS or Android device, so you're never more than a touch away from the world's most liquid market.
														</p>
														<p>
															<div class="col-md-6">
																<ul>
																	<li>
																	Take your trading everywhere</li>
																	<li>
																	Open and close positions and orders</li>
																	<li>
																	Optimized for iOS and Chrome on Android</li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul>
																	<li>
																	Charts for all symbols</li>
																	<li>
																	Log in using the same account across all platforms</li>
																	<li>
																	Easy to use - Tap, slide and scale</li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<a class="downbuttons googledownload" href="https://play.google.com/store/apps/details?id=com.spotware.ct" target="_blank"></a>
															<a class="downbuttons appledownload" href="https://itunes.apple.com/us/app/spotware-ctrader/id767428811?ls=1&amp;mt=8" target="_blank"></a>
														</div>
													</div>
												</div>
											</div>
											<div id="cAlgo" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/calgo.gif" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/cAlgo_logo.png"  style="width:300px;"></br></br></br>
														</div>
														<p>
															cAlgo lets you build <b>trading cBots</b> and <b>custom technical indicators</b> using C#.
															</br>With a <b>direct access</b> environment, cAlgo gives you:
														</p>
														<p>
															<div class="col-md-6">
																<ul >
																	<li>
																	Fast execution ideal for algorithmic trading</li>
																	<li>
																	Advanced backtesting</li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul >
																	<li>
																	Integration with cTrader</li>
																	<li>
																	Dedicated community with forums and downloadable cBots and indicators</li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<a href="http://spotware.ctrader.com/calgo-spotware-setup.exe" class="downbuttons calgodownloadbtn " ></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="MetaTrader" class="tab-pane fade">
									<div class="tabbable tabs-left download_tabs_box">
										<ul class="nav nav-tabs" id="tabs_c">
											<li class="active"><a data-toggle="tab" href="#MetaTrader_Desktop">MetaTrader 4 Desktop</a></li>
											<li><a data-toggle="tab" href="#MetaTrader_Web">MetaTrader Web</a></li>
											<li><a data-toggle="tab" href="#MetaTrader_Mobile_Web">MetaTrader Mobile Web</a></li>
											<li><a data-toggle="tab" href="#cAlgo">cAlgo</a></li>
										</ul>
										<div class="tab-content" id="tabs_content_c">
											<div id="MetaTrader_Desktop" class="tab-pane fade in active">
												<div class="row">
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/mt4_01.png" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="text-center">
															<img src="assets/img/platform/meta4-page.png" ></br>
														</div>
														<p>
															<b>The MetaTrader 4 trading system</b>
															</br>The powerful MetaTrader 4 platform allows you to implement strategies of any complexity.
															</br>The Market and pending orders, Instant Execution and trading from a chart, stop orders and trailing stop, a tick chart and trading history - all these tools are at your disposal.
															With MetaTrader 4, trading becomes flexible and convenient.
														</p>
														<p>
															<div class="col-md-6">
																<ul>
																	<li>2 execution modes</li>
																	<li>2 market orders</li>
																	<li>4 pending orders</li>
																	<li>2 stop orders and a trailing stop</li>
																</ul>
															</div>
															<div class="col-md-6">
																<ul>
																	<li>Interactive charts</li>
																	<li>9 timeframes</li>
																	<li>33 analytical objects</li>
																	<li>30 technical indicators</li>
																</ul>
															</div>
														</p>
														</br></br></br>
														<div class="text-center">
															<button class="btn btn-success  btn-lg">Download MetaTrader 4</button>
														</div>
													</div>
												</div>
											</div>
											<div id="MetaTrader_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														image
													</div>
													<div class="col-md-6">
														MetaTrader provides full STP access to the currency trading markets for professional and new traders.
														</br>We route your trades using STP networks, allowing you to trade directly with prominent global banks, freeing you from the inefficiencies of market maker models and opening the door to transparent trading in a live environment.
													</div>
												</div>
											</div>
											<div id="MetaTrader_Mobile_Web" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														image
													</div>
													<div class="col-md-6">
														MetaTrader provides full STP access to the currency trading markets for professional and new traders.
														</br>We route your trades using STP networks, allowing you to trade directly with prominent global banks, freeing you from the inefficiencies of market maker models and opening the door to transparent trading in a live environment.
													</div>
												</div>
											</div>
											<div id="cAlgo" class="tab-pane fade in ">
												<div class="row">
													<div class="col-md-6">
														image
													</div>
													<div class="col-md-6">
														MetaTrader provides full STP access to the currency trading markets for professional and new traders.
														</br>We route your trades using STP networks, allowing you to trade directly with prominent global banks, freeing you from the inefficiencies of market maker models and opening the door to transparent trading in a live environment.
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>