<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Make a Deposit</h3></div>
					<div class="panel-body">
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Bank Transfer
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_bank_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Deposit</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Paypal
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Deposit</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Paysbuy
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Deposit</button>
									</a>
								</div>
								
							</div>
						</div>
						<div class="">
							<div class="col-lg-6 bank_visual">
								
								<div class="col-sm-4">
									Skrill
								</div>
								<div class="col-sm-4">
									dddddddddddd
								</div>
								<div class="col-sm-4">
									<a href="#" data-toggle="modal" data-target="#modal_e_currency_transfer">
										<button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Deposit</button>
									</a>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Deposit History</h3></div>
					<div class="panel-body">
						<table  id="dt_tableTools" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Transection Number</th>
									<th>Date</th>
									<th>Type</th>
									<th>Amount</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="success">Success</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="warning">Processing</td>
								</tr>
								<tr>
									<td>534153</td>
									<td>23 Jan 2012, 11:28</td>
									<td>Online banking payment in Thailand</td>
									<td>$320,80</td>
									<td class="danger">Fail</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="modal_bank_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Make a Deposit</h3>
			<div class="panel panel-default">
				<div class="panel-heading">Bank Transfer Information</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<td></td>
									<td>Bank name</td>
									<td>Account Number</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option2" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option3" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option4" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
								<tr>
									<td><input type="radio" name="optionsRadios" id="optionsRadios1" value="option5" checked=""></td>
									<td>ธนาคารไทยพาณิชย์ จำกัด (มหาชน)</td>
									<td>Brokers Solutions 257-208146-1 </td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<form class="form-horizontal">
				<div class="form-group">
					<label for="user_lname" class="col-md-4 control-label">Amount</label>
					<div class="col-md-6 input-group">
						<span class="input-group-addon">USD $</span>
						<input class="form-control" id="appendedPrependedInput" type="text">
					</div>
					<label for="user_lname" class="col-md-4 control-label">Amount</label>
					<div class="col-md-6 input-group">
						<span class="input-group-addon">THB ฿</span>
						<input class="form-control" id="appendedPrependedInput" type="text">
					</div>
					<label for="user_lname" class="col-md-4 control-label">Select account To Deposit</label>
					
					<div class=" col-md-6" style="padding:0;">
						<select  class="form-control" >
							<option value="01">Wallet</option>
							<option value="02">MT4:345335</option>
							<option value="03">Ctrader:53456452</option>
						</select>
					</div>
					<label for="user_lname" class="col-md-4 control-label">Date</label>
					<div class="col-md-6 input-group">
						<div class="input-group date ts_datepicker" data-date-format="dd-mm-yyyy" >
							<input class="form-control" type="text">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<label for="user_lname" class="col-md-4 control-label">Time</label>
					<div class="col-md-6 input-group">
						<div class="input-group bootstrap-timepicker">
							<input id="deposittime" type="text" class="form-control">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
							</span>
						</div>
					</div>
					<label for="user_lname" class="col-md-4 control-label">Transfer's Document</label>
					<div class="col-md-6 input-group">
						<input type="file" id="user_lname" class="form-control" value="Zieme">
					</div>
					<label for="user_lname" class="col-md-4 control-label">Remark</label>
					<div class="col-md-6 input-group">
						<textarea cols="30" rows="3" class="form-control input-sm" placeholder="Message..."></textarea>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Deposit</button>
					<button class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</button>
				</div>
			</form>
			</div>
			
		</div>
	</div>
</div>
</div>
<!-- modal -->
<div class="modal fade" id="modal_e_currency_transfer">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3>Make a Deposit</h3>
			<form class="form-horizontal">
				<div class="form-group">
					<div class="row">
						<label for="user_lname" class="col-md-4 control-label">Amount</label>
						<div class="col-md-6 input-group">
							<span class="input-group-addon">USD $</span>
							<input class="form-control" id="appendedPrependedInput" type="text">
						</div>
					</div>
					<div class="row">
						<label for="user_lname" class="col-md-4 control-label">Select account To Deposit</label>
						
						<div class=" col-md-6" style="padding:0;">
							<select  class="form-control" >
								<option value="01">Wallet</option>
								<option value="02">MT4:345335</option>
								<option value="03">Ctrader:53456452</option>
							</select>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Make a Deposit</button>
					<button class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</button>
				</div>
			</form>
		</div>
	</div>
</div>
</div>