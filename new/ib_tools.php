<div class="page_content">
	<div class="container-fluid">
		<div class="row user_profile">
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Affliliate Setting</h3></div>
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="profile_username" class="col-md-2 control-label">Your Custom Referal Name link</label>
								<div class="col-md-10">
									<div class="input-group">
										<span class="input-group-addon">https://xxx.com/index.php?reflink=</span>
										<input class="form-control" id="appendedPrependedInput" type="text" value="yourname">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="profile_username" class="col-md-2 control-label">Your Referal link</label>
								<div class="col-md-10">
									https://xxx.com/index.php?reflink=yourname
								</div>
							</div>
							<div class="form-group">
								<div class="text-center">
									<button class="btn btn-success"><i class="fa fa-save"></i> Update Data</button>
								</div>
							</form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Your clients, trade volume and commission detail</h3></div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-md-12">
								<div class="heading_b">Level 1 client(s)</div>
							</div>
						</div>
						<table  id="dt_tableTools_lv1" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Account Number</th>
									<th>Trade Volume</th>
									<th>Commission</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-md-12">
								<div class="heading_b">Level 2 client(s)</div>
							</div>
						</div>
						<table  id="dt_tableTools_lv2" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Account Number</th>
									<th>Trade Volume</th>
									<th>Commission</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-md-12">
								<div class="heading_b">Level 3 client(s)</div>
							</div>
						</div>
						<table  id="dt_tableTools_lv3" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Account Number</th>
									<th>Trade Volume</th>
									<th>Commission</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-md-12">
								<div class="heading_b">Level 4 client(s)</div>
							</div>
						</div>
						<table  id="dt_tableTools_lv4" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Account Number</th>
									<th>Trade Volume</th>
									<th>Commission</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-md-12">
								<div class="heading_b">Level 5 client(s)</div>
							</div>
						</div>
						<table  id="dt_tableTools_lv5" class="table table-striped">
							<thead>
								<tr class="success">
									<th>Account Number</th>
									<th>Trade Volume</th>
									<th>Commission</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">MetaTrader4:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
								<tr>
									<td style="width: 15%;">cTrader:534153</td>
									<td style="width: 15%;">434.12 Lots</td>
									<td style="width: 15%;">USD$320.80</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>