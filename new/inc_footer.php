<!-- jQuery -->
<script src="assets/js/jquery.min.js"></script>
<!-- easing -->
<script src="assets/js/jquery.easing.1.3.min.js"></script>
<!-- bootstrap js plugins -->
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!-- top dropdown navigation -->
<script src="assets/js/tinynav.js"></script>
<!-- perfect scrollbar -->
<script src="assets/lib/perfect-scrollbar/min/perfect-scrollbar-0.4.8.with-mousewheel.min.js"></script>
<!-- common functions -->
<script src="assets/js/tisa_common.js"></script>
<!-- style switcher
<script src="assets/js/tisa_style_switcher.js"></script>
-->
<!-- nvd3 charts -->
<script src="assets/lib/d3/d3.min.js"></script>
<script src="assets/lib/novus-nvd3/nv.d3.min.js"></script>
<!-- flot charts -->
<script src="assets/lib/flot/jquery.flot.min.js"></script>
<script src="assets/lib/flot/jquery.flot.time.min.js"></script>
<script src="assets/lib/flot/jquery.flot.tooltip.min.js"></script>
<script src="assets/lib/flot/jquery.flot.pie.min.js"></script>
<script src="assets/lib/flot/jquery.flot.crosshair.min.js"></script>
<script src="assets/lib/flot/jquery.flot.resize.min.js"></script>
<!-- clndr -->
<script src="assets/lib/underscore-js/underscore-min.js"></script>
<script src="assets/lib/CLNDR/src/clndr.js"></script>
<!-- easy pie chart -->
<script src="assets/lib/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<!-- owl carousel -->
<script src="assets/lib/owl-carousel/owl.carousel.min.js"></script>
<!-- dashboard functions -->
<script src="assets/js/apps/tisa_dashboard.js"></script>
<!-- multiselect, tagging -->
<script src="assets/lib/select2/select2.min.js"></script>
<!--  bootstrap switches -->
<script src="assets/lib/bootstrap-switch/build/js/bootstrap-switch.min.js"></script>
<!-- user profile functions -->
<script src="assets/js/apps/tisa_user_profile.js"></script>
<!-- form extended elements functions -->
<script src="assets/js/apps/tisa_extended_elements.js"></script>
<!-- textarea autosize -->
<script src="assets/lib/autosize/jquery.autosize.min.js"></script>
<!--  datepicker -->
<script src="assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- date range picker -->
<script src="assets/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- timepicker -->
<script src="assets/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<!-- datatables -->
<script src="assets/lib/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="assets/lib/DataTables/media/js/dataTables.bootstrap.js"></script>
<script src="assets/lib/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="assets/lib/DataTables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<!-- datatables functions -->
<script src="assets/js/apps/tisa_datatables.js"></script>
<script type="text/javascript">
$('#deposittime').timepicker({
minuteStep: 1,
template: 'modal',
appendWidgetTo: '#modal_bank_transfer',
showSeconds: false,
showMeridian: false,
defaultTime: 'current'
});
/*
$( "input[name$='radio_switch1']" ).click(function() {
	var test = $(this).val();
	alert
$("div.desc").hide();
$("#pincode_setting_" + test).fadeIn();
//$( "#pincode_setting" ).fadeIn();
});*/
$(document).ready(function() {
$("input[name$='radio_switch1']").change(function() {
var test = $(this).val();
//alert(test);
		$("form.desc").slideUp();
		$("#pincode_setting_" + test).slideDown();
});
});
</script>
<script type="text/javascript">
	
	$(function() {
		// nvd3 charts
		//tisa_nvd3_charts.init();
		tisa_nvd3_charts.cumulativeLine();
	})
	
	// nvd3 charts
	tisa_nvd3_charts = {
		cumulativeLine: function() {
			if ($('#nvd3_cumulativeLine').length) {
				nv.addGraph(function() {
				    var chart = nv.models.linePlusBarChart()
				      .margin({top: 30, right: 60, bottom: 50, left: 70})
				      .x(function(d,i) { return i })
				      .y(function(d) { return d[1] })
				      .color(["#64B92A","#C0392B","#0000FF"])
				      ;

				    chart.xAxis
				      .showMaxMin(false)
				      .tickFormat(function(d) {
				        var dx = cumulativeTestData()[0].values[d] && cumulativeTestData()[0].values[d][0] || 0;
				        return d3.time.format('%x')(new Date(dx))
				      });

				    chart.y1Axis
				      .tickFormat(d3.format(',f'));

				    chart.y2Axis
				      .tickFormat(function(d) { return '$' + d3.format(',f')(d) });

				    chart.bars.forceY([0]);

				    d3.select('#nvd3_cumulativeLine svg')
				      .datum(cumulativeTestData())
				      .transition().duration(500)
				      .call(chart)
				      ;

				    nv.utils.windowResize(chart.update);

				    return chart;
				});
			
				function cumulativeTestData() {
					return [
				{
				"key" : "Deposits",
				"bar": true,
				"values" : [ [ 1136005200000 , 0.0] , [ 1138683600000 , 0.0] , [ 1141102800000 , 0.0] , [ 1143781200000 , 0] , [ 1146369600000 , 0] , [ 1149048000000 , 0] , [ 1151640000000 , 0] , [ 1154318400000 , 0] , [ 1156996800000 , 0] , [ 1159588800000 , 3899486.0] , [ 1162270800000 , 3899486.0] , [ 1164862800000 , 3899486.0] , [ 1167541200000 , 3564700.0] , [ 1170219600000 , 3564700.0] , [ 1172638800000 , 3564700.0] , [ 1175313600000 , 2648493.0] , [ 1177905600000 , 2648493.0] , [ 1180584000000 , 2648493.0] , [ 1183176000000 , 2522993.0] , [ 1185854400000 , 2522993.0] , [ 1188532800000 , 2522993.0] , [ 1191124800000 , 2906501.0] , [ 1193803200000 , 2906501.0] , [ 1196398800000 , 2906501.0] , [ 1199077200000 , 2206761.0] , [ 1201755600000 , 2206761.0] , [ 1204261200000 , 2206761.0] , [ 1206936000000 , 2287726.0] , [ 1209528000000 , 2287726.0] , [ 1212206400000 , 2287726.0] , [ 1214798400000 , 2732646.0] , [ 1217476800000 , 2732646.0] , [ 1220155200000 , 2732646.0] , [ 1222747200000 , 2599196.0] , [ 1225425600000 , 2599196.0] , [ 1228021200000 , 2599196.0] , [ 1230699600000 , 1924387.0] , [ 1233378000000 , 1924387.0] , [ 1235797200000 , 1924387.0] , [ 1238472000000 , 1756311.0] , [ 1241064000000 , 1756311.0] , [ 1243742400000 , 1756311.0] , [ 1246334400000 , 1743470.0] , [ 1249012800000 , 1743470.0] , [ 1251691200000 , 1743470.0] , [ 1254283200000 , 1519010.0] , [ 1256961600000 , 1519010.0] , [ 1259557200000 , 1519010.0] , [ 1262235600000 , 1591444.0] , [ 1264914000000 , 1591444.0] , [ 1267333200000 , 1591444.0] , [ 1270008000000 , 1543784.0] , [ 1272600000000 , 1543784.0] , [ 1275278400000 , 1543784.0] , [ 1277870400000 , 1309915.0] , [ 1280548800000 , 1309915.0] , [ 1283227200000 , 1309915.0] , [ 1285819200000 , 1331875.0] , [ 1288497600000 , 1331875.0] , [ 1291093200000 , 1331875.0] , [ 1293771600000 , 1331875.0] , [ 1296450000000 , 1154695.0] , [ 1298869200000 , 1154695.0] , [ 1301544000000 , 1194025.0] , [ 1304136000000 , 1194025.0] , [ 1306814400000 , 1194025.0] , [ 1309406400000 , 1194025.0] , [ 1312084800000 , 1194025.0] , [ 1314763200000 , 1244525.0] , [ 1317355200000 , 475000.0] , [ 1320033600000 , 475000.0] , [ 1322629200000 , 475000.0] , [ 1325307600000 , 690033.0] , [ 1327986000000 , 690033.0] , [ 1330491600000 , 690033.0] , [ 1333166400000 , 514733.0] , [ 1335758400000 , 514733.0]]
				},
				{
				"key" : "Withdrawals",
				"bar": true,
				"values" : [ [ 1136005200000 , -1271000.0] , [ 1138683600000 , -1271000.0] , [ 1141102800000 , -1271000.0] , [ 1143781200000 , 0] , [ 1146369600000 , 0] , [ 1149048000000 , 0] , [ 1151640000000 , 0] , [ 1154318400000 , 0] , [ 1156996800000 , 0] , [ 1159588800000 , -3899486.0] , [ 1162270800000 , -3899486.0] , [ 1164862800000 , -3899486.0] , [ 1167541200000 , -3564700.0] , [ 1170219600000 , -3564700.0] , [ 1172638800000 , -3564700.0] , [ 1175313600000 , -2648493.0] , [ 1177905600000 , -2648493.0] , [ 1180584000000 , 2648493.0] , [ 1183176000000 , 2522993.0] , [ 1185854400000 , 2522993.0] , [ 1188532800000 , 2522993.0] , [ 1191124800000 , 2906501.0] , [ 1193803200000 , 2906501.0] , [ 1196398800000 , 2906501.0] , [ 1199077200000 , 2206761.0] , [ 1201755600000 , 2206761.0] , [ 1204261200000 , 2206761.0] , [ 1206936000000 , 2287726.0] , [ 1209528000000 , 2287726.0] , [ 1212206400000 , 2287726.0] , [ 1214798400000 , 2732646.0] , [ 1217476800000 , 2732646.0] , [ 1220155200000 , 2732646.0] , [ 1222747200000 , 2599196.0] , [ 1225425600000 , 2599196.0] , [ 1228021200000 , 2599196.0] , [ 1230699600000 , 1924387.0] , [ 1233378000000 , 1924387.0] , [ 1235797200000 , 1924387.0] , [ 1238472000000 , 1756311.0] , [ 1241064000000 , 1756311.0] , [ 1243742400000 , 1756311.0] , [ 1246334400000 , 1743470.0] , [ 1249012800000 , 1743470.0] , [ 1251691200000 , 1743470.0] , [ 1254283200000 , 1519010.0] , [ 1256961600000 , 1519010.0] , [ 1259557200000 , 1519010.0] , [ 1262235600000 , 1591444.0] , [ 1264914000000 , 1591444.0] , [ 1267333200000 , 1591444.0] , [ 1270008000000 , 1543784.0] , [ 1272600000000 , 1543784.0] , [ 1275278400000 , 1543784.0] , [ 1277870400000 , 1309915.0] , [ 1280548800000 , 1309915.0] , [ 1283227200000 , 1309915.0] , [ 1285819200000 , 1331875.0] , [ 1288497600000 , 1331875.0] , [ 1291093200000 , 1331875.0] , [ 1293771600000 , 1331875.0] , [ 1296450000000 , 1154695.0] , [ 1298869200000 , 1154695.0] , [ 1301544000000 , 1194025.0] , [ 1304136000000 , 1194025.0] , [ 1306814400000 , 1194025.0] , [ 1309406400000 , 1194025.0] , [ 1312084800000 , 1194025.0] , [ 1314763200000 , 1244525.0] , [ 1317355200000 , 475000.0] , [ 1320033600000 , 475000.0] , [ 1322629200000 , 475000.0] , [ 1325307600000 , 690033.0] , [ 1327986000000 , 690033.0] , [ 1330491600000 , 690033.0] , [ 1333166400000 , 514733.0] , [ 1335758400000 , 514733.0]]
				},
				{
				"key" : "Balance",
				"values" : [ [ 1136005200000 , 71.89] , [ 1138683600000 , 75.51] , [ 1141102800000 , 68.49] , [ 1143781200000 , 62.72] , [ 1146369600000 , 70.39] , [ 1149048000000 , 59.77] , [ 1151640000000 , 57.27] , [ 1154318400000 , 67.96] , [ 1156996800000 , 67.85] , [ 1159588800000 , 76.98] , [ 1162270800000 , 81.08] , [ 1164862800000 , 91.66] , [ 1167541200000 , 84.84] , [ 1170219600000 , 85.73] , [ 1172638800000 , 84.61] , [ 1175313600000 , 92.91] , [ 1177905600000 , 99.8] , [ 1180584000000 , 121.191] , [ 1183176000000 , 122.04] , [ 1185854400000 , 131.76] , [ 1188532800000 , 138.48] , [ 1191124800000 , 153.47] , [ 1193803200000 , 189.95] , [ 1196398800000 , 182.22] , [ 1199077200000 , 198.08] , [ 1201755600000 , 135.36] , [ 1204261200000 , 125.02] , [ 1206936000000 , 143.5] , [ 1209528000000 , 173.95] , [ 1212206400000 , 188.75] , [ 1214798400000 , 167.44] , [ 1217476800000 , 158.95] , [ 1220155200000 , 169.53] , [ 1222747200000 , 113.66] , [ 1225425600000 , 107.59] , [ 1228021200000 , 92.67] , [ 1230699600000 , 85.35] , [ 1233378000000 , 90.13] , [ 1235797200000 , 89.31] , [ 1238472000000 , 105.12] , [ 1241064000000 , 125.83] , [ 1243742400000 , 135.81] , [ 1246334400000 , 142.43] , [ 1249012800000 , 163.39] , [ 1251691200000 , 168.21] , [ 1254283200000 , 185.35] , [ 1256961600000 , 188.5] , [ 1259557200000 , 199.91] , [ 1262235600000 , 210.732] , [ 1264914000000 , 192.063] , [ 1267333200000 , 204.62] , [ 1270008000000 , 235.0] , [ 1272600000000 , 261.09] , [ 1275278400000 , 256.88] , [ 1277870400000 , 251.53] , [ 1280548800000 , 257.25] , [ 1283227200000 , 243.1] , [ 1285819200000 , 283.75] , [ 1288497600000 , 300.98] , [ 1291093200000 , 311.15] , [ 1293771600000 , 322.56] , [ 1296450000000 , 339.32] , [ 1298869200000 , 353.21] , [ 1301544000000 , 348.5075] , [ 1304136000000 , 350.13] , [ 1306814400000 , 347.83] , [ 1309406400000 , 335.67] , [ 1312084800000 , 390.48] , [ 1314763200000 , 384.83] , [ 1317355200000 , 381.32] , [ 1320033600000 , 404.78] , [ 1322629200000 , 382.2] , [ 1325307600000 , 405.0] , [ 1327986000000 , 456.48] , [ 1330491600000 , 542.44] , [ 1333166400000 , 599.55] , [ 1335758400000 , 583.98]]
				}
				]
				;
				}
			}
		}
	}
</script>
</body>
</html>