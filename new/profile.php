
			<div class="page_content">
				<div class="container-fluid">
					<div class="row user_profile">
						<!--  -->
						<div class="col-md-2">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="user_stat_item color_d" 
									data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Congratulation, Your email has been verified">
										<i class="ion-ios7-email-outline"></i>
										
										<span>Email</span>
										<i class="ion-checkmark-round"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="user_stat_item color_g"
									data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Alert, Your mobile phone has not been verified">
										<i class="fa fa-mobile"></i>
										<span>Mobile</span>
										<i class="ion-android-close"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="panel panel-default">
								<div class="panel-body">		
									<div class="user_stat_item color_b"
									data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Your personal identification document has been uploaded awaiting verification process">
										<i class="fa fa-user"></i>
										<span>I.D.</span>
										<i class="ion-loading-a"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="user_stat_item color_g"
									data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Alert, Your proof of address has not been verified">
										<i class="ion-android-earth"></i>
										<span>Address</span>
										<i class="ion-android-close"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="user_stat_item color_g"
									data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Alert, Your bank account has not been verified">
										<i class="glyphicon glyphicon-briefcase"></i>
										<span>Bank</span>
										<i class="ion-android-close"></i>
									</div>
								</div>
							</div>
						</div>
					<!-- -->
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">My Detail</div>
								<div class="panel-body">
									<div class="user_profile">
										<form class="form-horizontal">
											<div class="tabbable tabs-right">
												<ul class="nav nav-tabs">
													<li class="active"><a data-toggle="tab" href="#profile_general_pane" class="tab-default">General Info</a></li>
													<li><a data-toggle="tab" href="#profile_contact_pane" class="tab-default">Contact Info</a></li>
													<li><a data-toggle="tab" href="#profile_other_pane" class="tab-default">Other Info</a></li>
												</ul>
												<div class="tab-content">
													<div id="profile_general_pane" class="tab-pane active">
														<div class="form-group">
															<label for="profile_username" class="col-md-2 control-label">Username</label>
															<div class="col-md-10">
																<input type="text" id="profile_username" class="form-control" value="walton_zieme">
															</div>
														</div>
														<div class="form-group">
															<label for="user_fname" class="col-md-2 control-label">First Name</label>
															<div class="col-md-10">
																<input type="text" id="user_fname" class="form-control" value="Walton">
															</div>
														</div>
														<div class="form-group">
															<label for="user_lname" class="col-md-2 control-label">Last Name</label>
															<div class="col-md-10">
																<input type="text" id="user_lname" class="form-control" value="Zieme">
															</div>
														</div>
														<div class="form-group">
															<label for="user_password" class="col-md-2 control-label">Password</label>
															<div class="col-md-10">
																<input type="password" id="user_password" class="form-control" value="password">
															</div>
														</div>
													</div>
													<div id="profile_contact_pane" class="tab-pane">
														<div class="form-group">
															<div class="col-md-12">
																<div class="heading_b">Address</div>
															</div>
														</div>
														<div class="form-group">
															<label for="profile_city" class="col-md-2 control-label">City</label>
															<div class="col-md-10">
																<input type="text" id="profile_city" class="form-control">
															</div>
														</div>
														<div class="form-group">
															<label for="profile_country" class="col-md-2 control-label">Country</label>
															<div class="col-md-10">
																<input type="text" id="profile_country" class="form-control">
															</div>
														</div>
														<div class="form-group">
															<label for="profile_street" class="col-md-2 control-label">Street</label>
															<div class="col-md-10">
																<input type="text" id="profile_street" class="form-control">
															</div>
														</div>
														<div class="form-group">
															<div class="col-md-12">
																<div class="heading_b">Social</div>
															</div>
														</div>
														<div class="form-group">
															<label for="profile_email" class="col-md-2 control-label">Email</label>
															<div class="col-md-10">
																<input type="text" id="profile_email" class="form-control" value="lockman.ignacio@hotmail.com">
															</div>
														</div>
														<div class="form-group">
															<label for="profile_skype" class="col-md-2 control-label">Skype</label>
															<div class="col-md-10">
																<input type="text" id="profile_skype" class="form-control" value="walton_zieme">
															</div>
														</div>
													</div>
													<div id="profile_other_pane" class="tab-pane">
														<div class="form-group">
															<label for="user_languages" class="col-md-2 control-label">Signature</label>
															<div class="col-md-10">
																<textarea name="user_signature" id="user_signature" cols="30" rows="4" class="form-control"></textarea>
															</div>
														</div>
														<div class="form-group">
															<label for="user_languages" class="col-md-2 control-label">Languages</label>
															<div class="col-md-10">
																<input type="text" name="user_languages" id="user_languages" class="form-control" value="English,French">
															</div>
														</div>
														<div class="form-group">
															<label for="user_languages" class="col-md-2 control-label">Newsletter</label>
															<div class="col-md-10">
																<input type="checkbox" class="bs_switch" data-on-color="success" data-on-text="Yes" data-off-text="No">
															</div>
														</div>
													</div>
												</div>
												<hr>
												<div class="text-center">
													<button class="btn btn-success"><i class="fa fa-save"></i> Save profile</button>
													<button class="btn btn-default"><i class="fa fa-trash-o fa-lg"></i> Delete</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		</div>