<?php
  /**
   * Content
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: content.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once ("init.php");
 
  if ($content->_url[1] == "tradersystem"):
	   
	  $row = $content->renderPage();
	  $core->getVisitors(); // visitor counter
	  $aDir = explode("/",CTHEME);
	  $fileAddOnTheme = "theme/trade_member".$aDir[1]."/index.php";
	  if (is_file($fileAddOnTheme)) {
	  	require_once (THEMEDIR . "/../trade_member".$aDir[1]."/index.php");
	  } else {
		require_once (THEMEDIR . "/../trade_member/index.php");
	  }
  elseif ($content->_url[0] == "page" ):
	  
	     if ($user->logged_in && $content->_url[1] == "registration") {
		  	redirect_to(SITEURL . "/page/tradersystem");exit;
		  } 
				 
		if ($content->_url[1] == "dashboard" || $content->_url[1] == "dashborad") {
			redirect_to(SITEURL . "/page/tradersystem");
			exit;
		} elseif ($content->_url[1] == "registrationxxxx") {
			require_once (THEMEDIR . "/regis_fix.php");	  	
			exit;
			
			 
		} elseif ($content->_url[1] == "activatexxx") {
			require_once (THEMEDIR . "/activate_fix.php");	  	
			exit;	  	  
		} elseif ($content->_url[1] == "loginxxxx") {
			require_once (THEMEDIR . "/login_fix.php");	  	
			exit;	  	  
		}

		$row = $content->renderPage();
		if ($_SERVER['REMOTE_ADDR'] =='65.60.60.133') {
			if ($row->register == 1 && $content->_url[1] == 'registration') {
				$row->register  =0;
                    		$row->registermonex  =1;
			}
		}
		
		$plgresult = $content->getPluginLayoutFront();
		$widgettop = Content::countPlace($plgresult,"top");
      $widgetbottom = Content::countPlace($plgresult, "bottom");
      $widgetleft = Content::countPlace($plgresult, "left");
      $widgetright = Content::countPlace($plgresult, "right");
      $assets = Content::countPlace($plgresult, false, false);

      $totalleft = count($widgetleft);
      $totalright = count($widgetright);
      $totaltop = count($widgettop);
      $totalbot = count($widgetbottom);

      if ($row):
          $core->getVisitors(); // visitor counter
          require_once (THEMEDIR . "/index.tpl.php");
      else:
          redirect_to(SITEURL . "/404.php");
      endif;

  else:
	  $file = 'modules/' . $content->_url[0] . '/main.php';
	 
	  if (file_exists($file)):
		  $plgresult = $content->getPluginLayoutFront();
		  $widgettop = Content::countPlace($plgresult, "top");
		  $widgetbottom = Content::countPlace($plgresult, "bottom");
		  $widgetleft = Content::countPlace($plgresult, "left");
		  $widgetright = Content::countPlace($plgresult, "right");
		  $assets = Content::countPlace($plgresult, false, false);

		  $totalleft = count($widgetleft);
		  $totalright = count($widgetright);
		  $totaltop = count($widgettop);
		  $totalbot = count($widgetbottom);

		  require_once (THEMEDIR . "/mod_index.tpl.php");
	  else:
		  /*
		  if ($user->logged_in){
		  	redirect_to(SITEURL . "/page/tradersystem");exit;
		  } else {
		   redirect_to(SITEURL . "/page/registration");
		   exit;
		  }*/
		  redirect_to(SITEURL . "/404.php");
	  endif;
  endif;
?>
