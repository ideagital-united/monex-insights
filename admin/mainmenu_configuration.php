<?php if($user->getAcl("Configuration") or $user->getAcl("Templates") or $user->getAcl("Newsletter") or $user->getAcl("Language") or $user->getAcl("Maintenance") or $user->getAcl("Logs") or $user->getAcl("Backup") or $user->getAcl("FM") or $user->getAcl("Fields") or $user->getAcl("System")):?>
	<li><a class="mortar <?php echo (Filter::$do == 'config' or Filter::$do == 'templates' or Filter::$do == 'newsletter' or Filter::$do == 'language' or Filter::$do == 'maintenance' or Filter::$do == 'logs' or Filter::$do == 'fields' or Filter::$do == 'backup' or Filter::$do == 'filemanager' or Filter::$do == 'system') ? "expanded" : "collapsed";?>"><i class="icon setting"></i><span><?php echo Lang::$word->_N_CONF;?></span></a>
		<ul class="subnav">
		<?php if($user->getAcl("Configuration")):?>
			<li><a href="index.php?do=config" class="green<?php if (Filter::$do == 'config') echo " active";?>"><i class="icon setting"></i><span><?php echo Lang::$word->_CG_TITLE1;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Templates")):?>
			<li><a href="index.php?do=templates" class="green<?php if (Filter::$do == 'templates') echo " active";?>"><i class="icon mail"></i><span><?php echo Lang::$word->_N_EMAILS;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Newsletter")):?>
			<li><a href="index.php?do=newsletter" class="green<?php if (Filter::$do == 'newsletter') echo " active";?>"><i class="icon mail reply"></i><span><?php echo Lang::$word->_N_NEWSL;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Fields")):?>
			<li><a href="index.php?do=fields" class="green<?php if (Filter::$do == 'fields') echo " active";?>"><i class="icon tasks"></i><span><?php echo Lang::$word->_N_FIELDS;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Language")):?>
			<li><a href="index.php?do=language" class="green<?php if (Filter::$do == 'language') echo " active";?>"><i class="icon chat"></i><span><?php echo Lang::$word->_N_LANGS;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Maintenance")):?>
			<li><a href="index.php?do=maintenance" class="green<?php if (Filter::$do == 'maintenance') echo " active";?>"><i class="icon wrench"></i><span><?php echo Lang::$word->_N_SMTCN;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Backup")):?>
			<li><a href="index.php?do=backup" class="green<?php if (Filter::$do == 'backup') echo " active";?>"><i class="icon hdd"></i><span><?php echo Lang::$word->_N_BACK;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("FM")):?>
			<li><a href="index.php?do=filemanager" class="green<?php if (Filter::$do == 'filemanager') echo " active";?>"><i class="icon folder"></i><span><?php echo Lang::$word->_N_FM;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("System")):?>
			<li><a href="index.php?do=system" class="green<?php if (Filter::$do == 'system') echo " active";?>"><i class="icon laptop"></i><span><?php echo Lang::$word->_N_SYSTEM;?></span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Logs")):?>
			<li><a href="index.php?do=logs" class="green<?php if (Filter::$do == 'logs') echo " active";?>"><i class="icon shield"></i><span><?php echo Lang::$word->_N_LOGS;?></span></a></li>
		<?php endif;?>
		</ul>
	</li>
<?php endif;?>