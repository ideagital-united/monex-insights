<?php
  /**
   * Controller
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: controller.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  
  require_once("init.php");
  if (!$user->is_Admin())
    redirect_to("login.php");
  
	if (class_exists('TraderSystem')) {
	} else {
	    require_once (MODPATH . "tradersystem/admin_class.php");
	    Registry::set('TraderSystem', new TraderSystem());
	}
	
	if (isset($_POST['doReportDetail'])):
		$json 		= Registry::get("TraderSystem") ->report_get_detail_admin();
	    print json_encode($json);
		exit;
	endif;
	
	if (isset($_POST['doReportFinance'])):
		date_default_timezone_set('UTC'); 
		$json = array();
		$sql = "
		SELECT 
			*
		 FROM  
		 	bk_custom_detail
		 WHERE 
		 	id!=''
		";
		/*echo $sql;*/
		
		$obj = Registry::get("Database")->fetch_all($sql);
		if ($obj) {
			foreach ($obj as $k => $v) 
			{
				switch($v->fieldname)
				{
					case 'sum_account_trade_deposit':$json['display_deposit']= number_format($v->valuesis,2);break;
					case 'sum_account_trade_withdraw':$json['display_withdraw']= number_format($v->valuesis,2);break;
					case 'sum_mt4_balance':$json['display_balance_mt4']= number_format($v->valuesis,2);break;
					case 'sum_ct_balance':$json['display_balance_ct']= number_format($v->valuesis,2);break;
					case 'sum_wallet':$json['display_wallet_net']= number_format($v->valuesis,2);break;
					
					case 'sum_mt4_equlity':$json['display_eq']= number_format($v->valuesis,2);break;
					case 'percent_trade_daily':$json['display_dd']= number_format($v->valuesis,2);break;
					case 'percent_trade_month':$json['display_mm']= number_format($v->valuesis,2);break;
					
					case 'total_member':$json['display_total_member']= number_format($v->valuesis,0);break;
					case 'total_account_trade':$json['display_total_acc']= number_format($v->valuesis,0);break;
					case 'total_account_active':$json['display_total_acc_active']= number_format($v->valuesis,0);break;
					
					case 'total_order_all':$json['display_order_close']= number_format($v->valuesis,0);break;
					case 'total_order_opening':$json['display_order_opening']= number_format($v->valuesis,0);break;
					case 'sum_lot':$json['display_lot']= number_format($v->valuesis,3);break;
					
					case 'sum_commission_paid':$json['display_commission_1']= number_format($v->valuesis,3);break;
					
					case 'sms_credit':$json['display_sms_credit']= number_format($v->valuesis,0);break;
					case 'sms_used':$json['display_sms_usrd']= number_format($v->valuesis,0);break;
				}
			}
		}
        $mt4net = str_replace(",", "", $json['display_balance_mt4']);
		$ctnet = str_replace(",", "", $json['display_balance_ct']);
		$walletnet = str_replace(",", "", $json['display_wallet_net']);
		$json['total_balance'] = (int)$mt4net+(int)$ctnet+(int)$walletnet;
		$json['total_balance'] = number_format($json['total_balance'],2);
		
		
		$json['display_order_all'] = $json['display_order_close']+$json['display_order_opening'];
		$json['display_order_all'] = number_format($json['display_order_all'],0);
		
		$sql_withdraw = "
			SELECT 
				sum(case when status=0 OR status=10 OR status=11 then money else 0 end) as withdraw_pending,
				sum(case when status=0 OR status=10 OR status=11 then 1 else 0 end) as withdraw_count,
				sum(case when status=1 then money  else 0 end) as withdraw_approve,
				sum(case when status=2  then money  else 0 end) as withdraw_cancel
			FROM
				bk_wallet_transaction 
			WHERE
				trans_type='wallet' and money < 0
				";
		$res_withdraw 	= Registry::get("Database")->first($sql_withdraw);
		$wpen 			= $res_withdraw->withdraw_pending * (-1);
		$wok 			= $res_withdraw->withdraw_approve * (-1);
		$wcc 			= $res_withdraw->withdraw_cancel * (-1);
		$json['withdraw_0'] = number_format($wpen, 2);
		$json['withdraw_0_account'] = number_format($res_withdraw->withdraw_count, 0);
		$json['withdraw_1'] = number_format($wok, 2);
		$json['withdraw_2'] = number_format($wcc, 2);
		
		$sql_deposit = "
			SELECT 
				sum(case when status=0 OR status=10 OR status=11 then money else 0 end) as deposit_pending,
				sum(case when status=0 OR status=10 OR status=11 then 1 else 0 end) as deposit_count,
				sum(case when status=1 then money  else 0 end) as deposit_approve,
				sum(case when status=2  then money  else 0 end) as deposit_cancel
			FROM
				bk_wallet_transaction 
			WHERE
				trans_type='wallet' and money > 0
				";
		$res_deposit = Registry::get("Database")->first($sql_deposit);
		$json['deposit_0'] = number_format($res_deposit->deposit_pending,2);
		$json['deposit_0_account'] = number_format($res_deposit->deposit_count,0);
		$json['deposit_1'] = number_format($res_deposit->deposit_approve,2);
		$json['deposit_2'] = number_format($res_deposit->deposit_cancel,2);

	    print json_encode($json);
	endif;
	
  	if (isset($_POST['doReportAdmin'])):
  		$json 		= Registry::get("TraderSystem") ->report_data_builer_graph_admin();
	    print json_encode($json);
	endif;
  
   	if (isset($_POST['doDeleteTransaction'])):
    	if(isset($_POST['id']) && $_POST['id'] != ''){
    		$pkid = $_POST['id'];
    	}
		$db->delete("bk_wallet_transaction","trans_id='".$pkid."'");
		if($db->affected()) {
			$json['type'] 		= 'success';
			$json['isdirect'] = 'yes';
			$json['message'] = 	Filter::msgOk('success', false);
		} else {
			$json['message'] 	=  Filter::msgError('NOT DELETE', false);
			$json['type'] 		= 'error';
		}
		print json_encode($json);
	endif;
	
	if (isset($_POST['admin_balance_broke'])):
		$acc_login 	= $_POST['dlt_pkid']*1;
		$emaiuser 	= $_POST['dlt_email'];
		$money 		= $_POST['money_modify']*1;
		$aUsr = Registry::get("TraderSystem")->getdata("u.fname,u.lname,u.email,u.id as uid","users u left outer join bk_trader_acc a on a.uid=u.id"
			,"where u.id!='' and (a.trader_acc_login ='".$acc_login."' or u.email='".$emaiuser."')");
		$aDB 		= array();
		$aDB['user_id'] 	= $aUsr['uid'];
		$aDB['status'] 		= 1;
		$aDB['gateway_dtl'] = 'access by admin';
		
		$aDB['deposit_comment'] 	= $_POST['comment'];
		$aDB['complete_by'] 		= $_SESSION['CMSPRO_username'] . "(".$_SESSION['uid'] .")";
		$aDB['money_total'] 		= 0;
		if ($_POST['itypeaction'] == 'deposit') {
			$aDB['money'] 	= $money;
		} elseif($_POST['itypeaction'] == 'withdraw') {
			$aDB['money'] 	= -$money;
		}
		$aDB['trans_type'] 	= (isset($_POST['inputto']) && $_POST['inputto'] == 'wallet' ) ? 'wallet' : ($_POST['inputto'] * 1) ;
		$insertid 			= Registry::get("Database")->insert("bk_wallet_transaction", $aDB);
		$err = '';
		if ($insertid) {
			if ($aDB['trans_type']  == 'wallet') {
				
				$reswallet = Registry::get("TraderSystem")->call_curl('admin_wallet/' . $aUsr['email'], $itype = "POST", array(
										'cmd'=>'wallet','amount' => $aDB['money'],'corr_code' => 2,'tid' => $insertid, 'comment' => 'access by admin'));
				if (isset($reswallet['isSuccess']) && $reswallet['isSuccess'] == 1) {
					$fupdate['comment'] 		= json_encode($reswallet['message']);
					$fupdate['money_total'] 	= $reswallet['message']['current_amount'];
				} else {
					$err = $reswallet['errMessage'];
				}
			} else {
				
				$acc_login 		= ($aDB['trans_type']) * 1;
				
				$aApi 			= Registry::get("TraderSystem")->call_curl("admin_broke_balance_correction","POST",array(
											"cmd"       =>"broke_balance_correction",
											"login"		=>	$acc_login,
											"amount"	=>	$aDB['money'],
											"comment" 	=>	'access by admin ref : '.$insertid
										));
				if (isset($aApi['isSuccess']) && $aApi['isSuccess'] == 1) {
					$aGet 						= Registry::get("TraderSystem")->call_curl_get("admin_broke_get_account/".$acc_login."?cmd=admin_broke_get_account","GET");
					$fupdate['money_total'] 	= $aGet['msg']['BALANCE'];
					$fupdate['ticket']		 	= $aApi['msg']['ticket'];
					$fupdate['comment'] 		= json_encode($aApi['msg']);
				} else {
					$err = $aApi['errMessage'];
				}
			}
		} else {
			$err = 'not insert transaction.';
		}
		
		if ($err == '')  {
			$resupate  = Registry::get("Database")->update("bk_wallet_transaction",$fupdate , "trans_id='" . $insertid."' ");
			$json['type'] 		= 'success';
			$json['isdirect'] 	= 'yes';
			$json['message'] 	= 	Filter::msgOk('success', false);
		} else {
			$json['type'] 		= 'error';
			$json['message'] 	= Filter::msgError($err,false);
		}
		
	  	print json_encode($json);
  endif;
  
  if (isset($_POST['Getaccounttradedtl'])):
	    $acc_login = $_POST['acc'] * 1;
	    $emaiuser = $_POST['email'];
	    $json['data1'] = $json['data2'] = $json['data3'] = "N/A";
	    
		$res = Registry::get("TraderSystem")->call_curl_get("admin_broke_get_account_detail_cfh/".$acc_login."/".$emaiuser,"GET");
		$json['bhnn'] = $res;
		/*
		$json['urlview'] = "admin_broke_get_account_detail/".$acc_login."/".$emaiuser;
		$json['returnapi'] = $res;
		 
		*/
		if (isset($res['isSuccess']) && $res['isSuccess'] == 1) {
			$_SESSION['account_last_view'] =  $acc_login;
			$aData = $res['msg'];
			$aUsr = Registry::get("TraderSystem")->getdata("u.fname,u.lname,u.email","users u left outer join bk_trader_acc a on a.uid=u.id"
			,"where u.id!='' and (a.trader_acc_login ='".$acc_login."' or u.email='".$emaiuser."')");
			$json['type'] = 'success';
			$json['data1'] = "<p class='screen-40 tablet-40 phone-100'><b>name</b></p><p class='screen-50 tablet-40 phone-100'> : ".$aUsr['fname']." ".$aUsr['lname']."</p>";
			$json['data1'] .= "<p class='screen-40 tablet-40 phone-100'><b>username</b></p><p class='screen-40 tablet-40 phone-100'> : ".$aUsr['email']."</p>";
			$json['data1'] .= "<p class='screen-40 tablet-40 phone-100'><b>wallet</b></p><p class='screen-40 tablet-40 phone-100'> : ".number_format($res['money'],2)." USD</p>";
			
			$json['data2'] = "<p class='screen-30 tablet-40 phone-100'><b>LOGIN</b></p><p class='screen-50 tablet-40 phone-100'> : ".$aData['LOGIN']."</p>";
			$json['data2'] .= "<p class='screen-30 tablet-40 phone-100'><b>GROUP:</b></p><p class='screen-50 tablet-40 phone-100'> : ".$aData['GROUP']."</p>";
			$json['data2'] .= "<p class='screen-30 tablet-40 phone-100'><b>NAME</b></p><p class='screen-50 tablet-40 phone-100'> : ".$aUsr['fname']." ".$aUsr['lname']."</p>";
			
			$json['data2'] .= "<p class='screen-30 tablet-40 phone-100'><b>EQUITY</b></p><p class='screen-50 tablet-40 phone-100'> : ".$aData['EQUITY']."</p>";
			$json['data2'] .= "<p class='screen-30 tablet-40 phone-100'><b>BALANCE</b></p><p class='screen-50 tablet-40 phone-100'> : ".$aData['BALANCE']."</p>";
			$json['data2'] .= "<p class='screen-30 tablet-40 phone-100'><b>CURRENCY</b></p><p class='screen-50 tablet-40 phone-100'> : ".$aData['CURRENCY']."</p>";
			
			
			$resOrder 	= $res['order'];
			
			if(isset($resOrder['isSuccess']) && $resOrder['isSuccess'] == 1){
				$json['data3'] = "";
				foreach($resOrder['msg'] as $k=>$v){
					$orderl = isset($v['order']) ? $v['order'] : $v['Ticket'];
					$json['data3'] .= "<p class='screen-30 tablet-40 phone-100'><b>ORDER</b></p><p class='screen-70 tablet-40 phone-100'> : ".$orderl."</p>";
				}
				
			}
			
			
		} else {
			$json['type'] = 'error';
		}
		
		print json_encode($json);
		
  endif;
  
  
  if (isset($_POST['userSearch_all'])):
      $aData = explode(" ",$_POST['userSearch_all']);
      if (count($aData) > 0) :
		$html = '';
		$html .= '<div id="search-results" class="prolific segment celled list">';
		$linkurl = 'index.php?do=modules&amp;action=config&amp;modname=tradersystem&amp;menu=manageuserfund&amp;';
		foreach($aData as $k => $v):
			$sql = "
			SELECT 
				u.id as userid, u.username, u.email, u.created, u.avatar, CONCAT(u.fname,' ',u.lname) as nameuser,
				a.trader_acc_login
			 FROM  
			 	users u LEFT OUTER JOIN bk_trader_acc a ON a.uid = u.id
			 WHERE
			 	(
				 	u.username like '%".$v."%'
				 	OR u.fname like '%".$v."%'
				 	OR u.lname like '%".$v."%'
				 	OR u.email like '%".$v."%'
				 	OR a.trader_acc_login like '%".$v."%'
			 	)
			 ORDER BY 
			 	u.id LIMIT 10";
			 	
			if ($result = $db->fetch_all($sql)):
			
			foreach ($result as $row):
                
                  $html .= '<div class="item">';
                  $html .= '<div class="items">';
                  $html .= '<p><a href="'.$linkurl.'itype=acclogin&amp;txtsearch=' . $row->trader_acc_login . '">'.$row->trader_acc_login .'</a>['.$row->nameuser .']</p>';
                  $html .= '<p><a href="'.$linkurl.'itype=pkid&amp;txtsearch=' . $row->userid . '">username: '.$row->email.'</a></p>';
                  $html .= '</div>';
                  $html .= '</div>';
			endforeach;
			
          	endif;
		endforeach;
		$html .= '</div>';
		print $html;
          
    endif;
  endif;
  //Filter::msgError('Answer incorrect Please send mail to '.Registry::get("Core")->site_email,false);
  