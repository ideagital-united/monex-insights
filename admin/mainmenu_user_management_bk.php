<?php if($user->getAcl("Users") or $user->getAcl("modules") or $user->getAcl("document_control")):?>
	<li><a class="mortar <?php echo (Filter::$do == 'Users' or  (Filter::$do == 'modules' and  $_REQUEST['menu'] == 'member-document')) ? "expanded" : "collapsed";?>"><i class="icon users"></i><span>USERS MANAGEMENT</span></a>
		<ul class="subnav">
			<?php if($user->getAcl("Users")):?>
				<li><a href="index.php?do=users" class="green<?php if (Filter::$do == 'users') echo " active";?>"><i class="icon user"></i><span>USERS</span></a></li>
			<?php endif;?>
			<?php if($user->getAcl("User_group")):?>
				<li><a href="index.php?do=user_group" class="green<?php if (Filter::$do == 'user_group') echo " active";?>"><i class="icon user"></i><span>USERS GROUP</span></a></li>
			<?php endif;?>
		</ul>
	</li>
<?php endif;?>