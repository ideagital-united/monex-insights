<?php
  /**
   * Main
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>

<div class="prolific icon heading message red"> <i class="icon home"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MN_TITLE;?> </div>
    <div class="prolific breadcrumb">
      <div class="active section"><i class="icon dashboard"></i> <?php echo Lang::$word->_N_DASH;?></div>
    </div>
  </div>
</div>
<?php

if (class_exists('TraderSystem')) {
	} else {
	    require_once (MODPATH . "tradersystem/admin_class.php");
	    Registry::set('TraderSystem', new TraderSystem());
	}
$aListSymbol = Registry::get("TraderSystem")->list_about_order_by_symbol();

?>

<div class="prolific-large-content">
    <div class="prolific segment">
    <div class="prolific header">Summary Reports</div>

    <?php if($user->userlevel == 9):?>
    <?php endif;?>
     
    <div class="clearfix small-bottom-space"> 
    <div class="prolific form segment report">
    <div class="columns">
    <h3>Financial</h3>
		<br>
		<div class="screen-50 tablet-50 phone-100">
			<h4>Financial Summary </h4>
			<P>Total Deposits: <span id="display_deposit">0</span></P>
			<P>Total Withdrawals: <span id="display_withdraw">0</span></P>
			<P>MT4 Balance: <span id="display_balance_mt4">0</span></P>
			<P>cTrader Balance: <span id="display_balance_ct">0</span></P>
			<P>Wallet Balance: <b><span id="display_wallet_net">0</span></b></P>
			<P>Total Balance: <b><span id="total_balance">0</span></b></P>
		</div>

		<div class="screen-25 tablet-50 phone-100">
			<h4>Equity</h4>
			<P>Equity: <b><span id="display_eq">0</span></b></P>
			<!--
			<P>Equity (%): <b><span id="display_eq">0</span></b></P>
			<P>Omnibus (%): <b><span id="display_eq">0</span></b></P>
			-->
		</div>

		<div class="screen-25 tablet-50 phone-100">
			<h4>Equity Growth</h4>
			<P>Daily: <span id="display_dd">0</span></P>
			<!--<P>Weekly: <span id="display_mm">0</span></P>-->
			<P>Monthly: <span id="display_mm">0</span></P>
			<!--<P>Yearly: <span id="display_mm">0</span></P>-->
		</div>

	</div>
		<hr>
	<div class="columns">
		<h3>Traders</h3>
		<br>
		<div class="screen-50 tablet-50 phone-100">
			<h4>Account</h4>
			<P>Total Members: <span id="display_total_member">0</span></P>
			<P>Total Accounts: <span id="display_total_acc">0</span></P>
			<P>Active Accounts: <span id="display_total_acc_active">0</span></P>
			<!--<P>Complete Verification: <span id="display_balance">0</span></P>-->
			
		</div>

		<div class="screen-25 tablet-50 phone-100">
			<h4>Growth</h4>
			<P>Daily: <span id="">0</span></P>
			<P>Weekly: <span id="">0</span></P>
			<P>Monthly: <span id="">0</span></P>
			<P>Yearly: <span id="">0</span></P>
		</div>

		<div class="screen-25 tablet-50 phone-100">
			<h4>Order Trades</h4>
			<P>Open Order: <span id="display_order_all">0</span></P>
			<P>Opening Order: <span id="display_order_opening">0</span></P>
			<P>Order Close: <span id="display_order_close">0</span></P>
			<P>Lot Summary: <span id="display_lot">0</span></P>
		</div>
	</div>
	
<hr>

   <div class="columns">
    <h3>Transections Activity</h3>
		<br>
		<div class="screen-25 tablet-50 phone-100">
			<h4>Withdrawal Management</h4>
			<P>Withdrawal Pending<b><span id="withdraw_0">0</span></b></P>
			<P>Withdrawal Pending (Account)<b><span id="withdraw_0_account">0</span></b></P>
			<P>Withdrawal Approved<span id="withdraw_1">0</span></P>
			<P>Withdrawal Canceled<span id="withdraw_2">0</span></P>
		</div>

		<div class="screen-25 tablet-50 phone-100">
			<h4>Deposit Management</h4>
			<P>Deposit Pending<b><span id="deposit_0">0</span></b></P>
			<P>Deposit Pending (Account)<b><span id="deposit_0_account">0</span></b></P>
			<P>Deposit Approved<span id="deposit_1">0</span></P>
			<P>Deposit Canceled<span id="deposit_2">0</span></P>
		</div>
		
		<div class="screen-25 tablet-50 phone-100">
			<h4>Commission Management</h4>
			<P>Pending Approve: <b><span id="">0</span></b></P>
			<P>Commission Approved: <span id="display_commission_1">0</span></P>
			<P>Cancel Approve: <span id="">0</span></P>
		</div>
		
		<div class="screen-25 tablet-50 phone-100">
			<h4>Credit Status</h4>
			<P>Total Paid out: <span id="display_dd">0</span></P>
			<P>Credit Balance: <span id="display_mm">0</span></P>
		</div>
	</div>
	<hr>

    <div class="columns">
    <h3>Other</h3>
		<br>
		<div class="screen-25 tablet-50 phone-100">
			<h4>Report SMS</h4>
			<P>SMS CREDIT<b><span id="display_sms_credit">0</span></b></P>
			<P>SMS USED<b><span id="display_sms_usrd">0</span></b></P>
		</div>
	</div>
</div>


    <!--<div class="prolific form segment">
    <div class="four fields">
    	<div class="field">TOTAL DEPOSIT(<span id="display_deposit">0</span>) </div>
    	<div class="field">TOTAL WITHDRAW(<span id="display_withdraw">0</span>)</div>
    	<div class="field">Daily(<span id="display_dd">0</span>)  Monthly (<span id="display_mm">0</span>) Balance (<span id="display_balance">0</span>) Equity (<span id="display_eq">0</span>)</div>
    	<div class="field">ACCOUNT NET(<span id="display_acctrade">0</span>)</div><div class="field">ORDER NET(<span id="display_ordernet">0</span>)</div>
    </div>
	</div>
    
    <div class="clearfix small-bottom-space"> 
    <div class="prolific form segment">
    	<div class="four fields">
    	<div class="field">Withdraw Pending(<span id="withdraw_0">0</span>) approved(<span id="withdraw_1">0</span>) Cancel(<span id="withdraw_0">0</span>)</div>
    	<div class="field">Deposit Pending(<span id="deposit_0">0</span>) approved(<span id="deposit_1">0</span>) Cancel(<span id="deposit_0">0</span>)</div>
    	<div class="field">Cash Flow(<span id="resultfinance">0</span>)  Wallet (<span id="wallet_net">0</span>) Equity (<span id="equity_net">0</span>) Profit (<span id="profit_net">0</span>)</div>
    </div>
	</div> -->
    
    <div class="clearfix small-bottom-space"> 
    <div class="prolific form segment">
    <h3>Top Symbol Trades</h3>
    <br>
    <div class="fields">
    	 <!-- start list cmd -->
    	 <?php
    	 if ($aListSymbol != 0) {
    	 	?>
    	 	<table class="prolific table"><thead><tr><th>Symbol</th><th>Net Order Buy</th><th>Net Order Sell</th><th>Net Volume Buy</th><th>Net Volume Sell</th></tr></thead>
    	 	<tbody>
    	 	<?php
    	 	foreach ($aListSymbol as $ksymbol => $vsymbol) {
    	 		?>
    	 		<tr>
    	 			<td><?php echo $vsymbol->symbol;?></td>
    	 			<td><?php echo $vsymbol->sum_buy;?></td>
    	 			<td><?php echo $vsymbol->sum_sell;?></td>
    	 			<td><?php echo $vsymbol->volume_buy;?></td>
    	 			<td><?php echo $vsymbol->volume_sell;?></td>
    	 		</tr>
    	 		<?php
    	 	}
			?>
			</tbody>
			</table>
			<?php
    	 }
    	 ?>
    	 <!-- edn list cmd -->
    	</div>
	   </div>
    </div>
    <div class="clearfix small-bottom-space"> 
    <div class="prolific form segment">
    <div class="three fields">
    	<!--
		<div class="field">
        	<label>ACCOUNT TRADE</label>
        	<label class="input"><input type="text" name="account" id= "account" value="<?php echo $_POST['account'];?>"></label>
        </div>
       -->
		<div class="field">
        	<label>DATE START</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpStart" id="dpStart" value="<?php echo $_POST['dpEnd'];?>" class="picker__input picker__input--active"></label>
        </div>
        <div class="field">
        	<label>DATE END</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpEnd" id="dpEnd" value="<?php echo $_POST['dpEnd'];?>" class="picker__input picker__input--active"></label>
        </div>
         <div class="field">
        	<label>&nbsp;</label>
        	<label class="input"><button type="button" name="doSearch" id="btn_search_by_date" class="prolific positive button">Search</button></label>
        </div>
		
        
	   </div>
	  

    <div id="nvd3_cumulativeLine" class="chart" style="width:100%;height:354px"><svg></svg></div>

     </div>
    </div>
  </div>
</div>
<!-- nvd3 charts -->
<!-- nvd3 charts -->
<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/novus-nvd3/nv.d3.min.css">
<script src="<?php echo THEMEURLTRADE;?>assets/lib/d3/d3.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/novus-nvd3/nv.d3.min.js"></script>
<!-- flot charts-->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.tooltip.min.js"></script>

<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
	$.ajax({
	  url: "controller2.php" ,
	  type: 'POST',
	  data:{doReportFinance : 1},
	  dataType :'json'
	}).done(function(obj) {
		if(typeof(obj) == 'object'){
			for(var typei in obj){
				/*alert(typei + '===' + obj[typei]);*/
				$('#'+typei).html(obj[typei]);
			}
		}
		
	});
	
	function get_report() {
		$.ajax({
			  url: "controller2.php" ,
			  type: 'POST',
			  data:{doReportDetail : 1,acc:$('#account').val(),dtstart:$('#dpStart').val(),dtend:$('#dpEnd').val()},
			  dataType :'json'
			}).done(function(obj) {
				$('#display_dd').html(obj.daily);
				$('#display_mm').html(obj.monly);
				$('#display_balance').html('$'+obj.balnace);
				$('#display_eq').html('$'+obj.equity);
				$('#display_deposit').html('$'+obj.deposit);
				$('#display_withdraw').html('$'+obj.withdraw);
				$('#display_acctrade').html('$'+obj.sumtrade);
				$('#display_ordernet').html('$'+obj.sumorder);
			});
	}
	/*get_report();*/
$('#btn_search_by_date').click(function(){
	/*get_report();*/
	cumulativeTestDatadd();
});
  function cumulativeTestDatadd() {
					var chart_browsers_data_new = [];
					$.ajax({
					  url: "controller2.php" ,
					  type: 'POST',
					  data:{doReportAdmin : 1,dtstart:$('#dpStart').val(),dtend:$('#dpEnd').val(),acc:$('#account').val()},
					  dataType :'json'
					}).done(function(obj) {
						if ($('#nvd3_cumulativeLine').length) {
				nv.addGraph(function() {
				    var chart = nv.models.linePlusBarChart()
				      .margin({top: 30, right: 60, bottom: 50, left: 70})
				      .x(function(d,i) { return i })
				      .y(function(d) { return d[1] })
				      .color(["#64B92A","#C0392B","#0000FF"])
				      ;

				    chart.xAxis
				      .showMaxMin(false)
				      .tickFormat(function(d) {
				        var dx = cumulativeTestData()[0].values[d] && cumulativeTestData()[0].values[d][0] || 0;
				        return d3.time.format('%x')(new Date(dx))
				      });

				    chart.y1Axis
				      .tickFormat(function(d) { return '$' + d3.format(',f')(d) });

				    chart.y2Axis
				      .tickFormat(function(d) { return '$' + d3.format(',f')(d) });

				    chart.bars.forceY([0]);

				    d3.select('#nvd3_cumulativeLine svg')
				      .datum(obj)
				      .transition().duration(500)
				      .call(chart)
				      ;

				    nv.utils.windowResize(chart.update);

				    return chart;
				});
			
				function cumulativeTestData() {
					return obj;
					
				}
			}
					});
					}
					cumulativeTestDatadd();
        
       
});
// ]]>
</script> 