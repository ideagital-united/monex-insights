<?php if($user->getAcl("modules") or $user->getAcl("secure_sms_setting") or $user->getAcl("secure_pin_code_setting") or $user->getAcl("secure_ip_check_setting")or $user->getAcl("secure_question_setting")):?>
	<li><a class="mortar <?php echo ($menuTrader == 'ss_sms' or $menuTrader == 'ss_pincode' or $menuTrader == 'ss_ipcheck' or $menuTrader == 'ss_question') ? "expanded" : "collapsed";?>">
	<i class="icon lock "></i><span>SECURITY MANAGEMENT</span></a>
		<ul class="subnav">
		<?php if($user->getAcl("secure_sms_setting")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_sms" class="green<?php if ($menuTrader == 'ss_sms') echo " active";?>">
			<i class="icon tablet"></i><span>SMS SETTING</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("secure_pin_code_setting")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_pincode" class="green<?php if ($menuTrader == 'ss_pincode') echo " active";?>">
			<i class="icon key  "></i><span>PIN CODE SETTING</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("secure_ip_check_setting")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_ipcheck" class="green<?php if ($menuTrader== 'ss_ipcheck') echo " active";?>">
			<i class="icon screenshot "></i><span>IP CHECK SETTING</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("secure_question_setting")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_question" class="green<?php if ($menuTrader== 'ss_question') echo " active";?>">
			<i class="icon question "></i><span>QUESTION SETTING</span></a></li>
		<?php endif;?>
		</ul>
	</li>
<?php endif;?>