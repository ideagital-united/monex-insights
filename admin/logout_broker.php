<?php
  /**
   * Login
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2011
   * @version $Id: login.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  
  define("_VALID_PHP", true);
  require_once("init.php");

	$result = $usernc->broker_logout($_SESSION['admin_secure']['admin_api_key']);
	$access_text = '';
	$user->update_access($access_text, $_SESSION['email']);
	if($result == true){
		print Filter::msgOk(Lang::$word->_N_BLOGOUT);
		redirect_to("index.php");
		exit;
		
	} else {
		print Filter::msgError(Lang::$word->_ERROR);
		unset($_SESSION['admin_secure']);
		redirect_to("index.php");
		exit;
	}

?>
