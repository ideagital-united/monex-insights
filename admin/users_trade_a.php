<?php
if(!$user->getAcl("Users")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

?>
<script>
var var_action='user_trade&itype=A';
var SITEURL = "<?php echo SITEURL;?>";
</script>



<?php

//=== end edit

//=== start view

/*echo BASEPATH."admin/modules/tradersystem/script_datatable_ajax.php";*/

include(MODPATH."tradersystem/script_datatable_ajax.php");

?>

<div class="prolific icon heading message mortar"><a class="helper prolific top right info corner label" data-help="configure"><i class="icon help"></i></a> <i class="setting icon"></i>

  <div class="content">

    <div class="header">User Trade Account</div>

    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>

      <div class="divider"> / </div>

      	<div class="active section">User Trade Account List</div>

    </div>

  </div>

</div>




<div id="msgholder"></div>
<div class="prolific-large-content prolific form">

	<table border="1" class="display table table-bordered table-striped data-table">

<thead>

  <tr>
<th align="center">-</th>
		<th align="center">Edit</th>

  		<th align="center">LOGIN<!--trans_id--></th>

		<th align="center">NAME<!--user_id--></th>
		<th align="center">Group<!--user_id--></th>
<th align="center">REGDATE<!--user_id--></th>
		<th align="center" >Balance (USD)<!--money_in--></th>

		<th align="center">Equity (USD)<!--thai_bank--></th>

		<th align="center">Credit (USD)<!--trans_date--></th>
		<th align="center">Profit (USD)<!--trans_date--></th>

  </tr>



</thead>

</table>

</div>
<script>





(function ($) {
	   var itypehead = {del:"Delete Account Login", pwd:"Change Password"}; 
	   var labelbtn= {del:"Delete", pwd:"Update"}; 
       $('body').on('click', 'a.acctrade', function () {
       		$(".prolific-large-content").addClass("loading");
       		var itype = $(this).attr('itype');
       		var login = $(this).attr('login');
       		var email = $(this).attr('email');
       	    $.ajax({
			  url: SITEURL + "/admin/modules/tradersystem/controller.php" ,
			  type: 'POST',
			  data:{action:'getAccountTrade',login:login,email:email},
			  dataType :'json'
			}).done(function(json) {
				$(".prolific-large-content").removeClass("loading");
				eval("var headtitle = itypehead."+itype);
				eval("var labelbutton = labelbtn."+itype);
				
				
			   //== start modal
            var text = "<div class=\"prolific fluid input\">"
            text += "<div class=\"inline-group\">";
            text += json.msg;
            if (itype == 'pwd') {
            	text += "<label class=\"input\">password : <input name=\"password1\" id=\"password1\" type=\"password\" value=\"\" ></label>";
            	text += "<label class=\"input\">password Confirm : <input name=\"password2\" id=\"password2\" type=\"password\" value=\"\" ></label>";
            	text += "<label class=\"input\">Set Group: <select name=\"setgroup\" id=\"setgroup\"><option value=\"\"></option><option value=\"A\">A</option><option value=\"B\">B</option></select></label>";
            }
           /* text += "<label class=\"radio\"><input name=\"notice_user\" type=\"radio\" value=\"0\" checked ><i></i><span class=\"prolific warning font\">Don't Notice</span></label>";
            text += "<label class=\"radio\"><input name=\"notice_user\" type=\"radio\" value=\"1\" ><i></i><span class=\"prolific success font\">Notice</span></label>";
            */
            text += "<div class=\"prolific divider\"></div>";
            text += "</div>";
            new Messi(text, {
                title: headtitle,
                modal: true,
                zIndex:700,
                closeButton: true,
                buttons: [{
                    id: 0,
                    label:labelbutton,
                    class: 'positive',
                    val: 'Y'
                }],
                callback: function (val) {
                	$(".prolific-large-content").addClass("loading");
                    noticeuser = $("input:radio[name=notice_user]:checked").val();
                    var pwd=pwd2=setgroup='';
                    if (itype == 'pwd') {
                    	pwd = $('#password1').val();
                    	pwd2 = $('#password2').val();
                    	setgroup=$('#setgroup').val();
                    	
                    }
                    api_id = $("#api_id").val();
                    $.ajax({
                        type: 'post',
                        url: SITEURL + "/admin/modules/tradersystem/controller.php" ,
                        dataType: 'json',
                        data: {
                            action : 'accounttrade'+itype,
                            noticeuser: noticeuser,
                            itype: itype,
                            email:email,
                            login:login,
                            pwd:pwd,
                            pwd2:pwd2,
                            setgroup:setgroup
                        },
                        success: function (json) {
                        	$(".prolific-large-content").removeClass("loading");
                            if (json.status == "success") {
                                $("#msgholder").html(json.msg);
                                location.reload(); 
                            } else {
                            	 $("#msgholder").html(json.msg);
                            }
                        }
                    });
                }
            });
        }); 
			   //=== end madal
			});

})(jQuery);

</script>











