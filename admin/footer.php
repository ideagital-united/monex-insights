<?php
  /**
   * Footer
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: footer.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<!-- Start Footer-->
<footer class="clearfix">
  <div class="prolific-content">Copyright &copy;<?php echo date('Y').' '.$core->site_name;?> &bull; Powered by: Prolific-Team <?php echo $core->version;?></div>
</footer>
<!-- End Footer--><?phpif (Filter::$do && ($_GET['menu'] == 'commission-member-tree')) { ?>	<script src="/admin/modules/tradersystem/member_tree/lib/jquery.cookie.js" type="text/javascript"></script>	<script src="/admin/modules/tradersystem/member_tree/jquery.treeview.js" type="text/javascript"></script><?php}?>
<script src="assets/js/jquery.iframe-transport.js"></script> 
<script src="assets/js/jquery.fileupload.js"></script> 
<script src="assets/js/fullscreen.js"></script>
<?php if (Filter::$do && is_file('assets/js/' . Filter::$do.".js")):?>
<script type="text/javascript" src="assets/js/<?php echo Filter::$do;?>.js"></script>
<?php endif;?><script src="assets/js/elevatezoom-master/jquery.elevatezoom.js"></script>
<script type="text/javascript"> 
// <![CDATA[  
$(document).ready(function () {    
    $.Master({
		weekstart: <?php echo ($core->weekstart - 1);?>,
		contentPlugins: {<?php echo Content::getEditorPlugins();?>},
        lang: {
            button_text: "<?php echo Lang::$word->_CHOOSE;?>",
            empty_text: "<?php echo Lang::$word->_NOFILE;?>",
            monthsFull: [<?php echo Core::monthList(false);?>],
            monthsShort: [<?php echo Core::monthList(false, false);?>],
			weeksFull : [<?php echo Core::weekList(false);?>],
			weeksShort : [<?php echo Core::weekList(false, false);?>],
			today : "<?php echo Lang::$word->_MN_TODAY;?>",
			clear : "<?php echo Lang::$word->_CLEAR;?>",
            delMsg1: "<?php echo Lang::$word->_DEL_CONFIRM;?>",
            delMsg2: "<?php echo Lang::$word->_DEL_CONFIRM1;?>",
            working: "<?php echo Lang::$word->_WORKING;?>"
        }
    });                  $('body').on('click', 'a#login_secure', function () {       		            var text = "<div class=\"prolific fluid input\"><input type=\"text\" placeholder=\"Username\" id =\"fixusername\" name=\"fixusername\"></div>";            text += "<div class=\"prolific fluid input\"><input type=\"password\" placeholder=\"********\" id=\"fixpassword\" name=\"fixpassword\"></div>";            text += "<div class=\"prolific fluid input\">";                       new Messi(text, {                title: "Login as broker administrator",                modal: true,                closeButton: true,                buttons: [{                    id: 0,                    label:'LOGIN',                    class: 'positive',                    val: 'Y'                }],                callback: function (val) {                    noteval = $("input[name=note]").val();                    color = $("input:radio[name=color]:checked").val();                                          $.ajax({                        type: 'post',                        url: "controller.php",                        dataType: 'json',                        data: {                            username: $("#fixusername").val(),                            password:  $("#fixpassword").val(),                            doActionLoginBroker: 1                        },                        success: function (json) {                        	alert(json.msg);                                location.reload();                                                         $.sticky(decodeURIComponent(json.message), {                                type: json.type,                                title: json.title                            });                        }                    });                }            });        			  			});			if ($('#loadboxloginbroker').get(0)) {				$('a#login_secure').trigger('click');			}              
});
// ]]>
</script>
</body></html>