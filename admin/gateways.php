<?php
  /**
   * Gateways
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: gateways.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
	  
  if(!$user->getAcl("Gateways")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
  
?>
<?php switch(Filter::$action): case "edit": ?>
<?php 
$row = $member->getGatewaysByDir($_REQUEST['dir']);
$rowdtl = getValues("extra3","gateways","dir='".$_REQUEST['dir']."'");
?>
<div class="prolific icon heading message coral"><a class="helper prolific top right info corner label" data-help="gateway"><i class="icon help"></i></a> <i class="share icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_GW_TITLE2;?> </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=gateways" class="section"><?php echo Lang::$word->_N_GATES;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_GW_TITLE1;?></div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_GW_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="prolific form segment">
    <div class="prolific header"><?php echo Lang::$word->_GW_SUBTITLE1 . $row->displayname;?></div>
    <div class="prolific double fitted divider"></div>
    <form id="prolific_form" name="prolific_form" method="post">
      <div class="fields">
        <div class="field">
          <label><?php echo Lang::$word->_GW_NAME;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row['displayname'];?>" name="displayname">
          </label>
        </div>
        
      </div>
      <div class="fields">
        <div class="field">
          <label>Detail Message</label>
          <label class="input">
          <textarea class="bodypost" placeholder"<?php echo Lang::$word->_ET_BODY;?>" name="extra_txt3"><?php echo $rowdtl->extra3;?></textarea>
          </label>
        </div>
      </div>
      
     
      <div class="prolific double fitted divider"></div>
      <button type="button" name="dosubmit" class="prolific positive button"><?php echo Lang::$word->_GW_UPDATE;?></button>
      <a href="index.php?do=gateways" class="prolific basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="dir" type="hidden" value="<?php echo $row['dir'];?>">
      <input name="processGateway" type="hidden" value="1">
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php default: ?>

<div class="prolific icon heading message coral"> <i class="icon share"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_GW_TITLE2;?> </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_N_GATES;?></div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_GW_INFO2;?></div>
  <div class="prolific segment">
    <div class="prolific header"><?php echo Lang::$word->_GW_SUBTITLE2;?></div>
    <div class="prolific fitted divider"></div>
    <!-- start setup rate exchange -->
    <?php
    
	$sql = "SELECT * FROM bk_exchn_rate_new where status=1";
    $aRate = $db->fetch_all($sql);
	if (isset($_REQUEST['curencyid']) && $_REQUEST['curencyid'] != '') {
		$aRateEdit = Core::getRowById("bk_exchn_rate_new", $_REQUEST['curencyid']);
		$pkid = $aRateEdit->id;
	}
    ?>
    <form id="prolific_form" name="prolific_form" method="post">
    	<input type="hidden" name="pkid" value="<?php echo $pkid;?>">
    <div id="msgholder"></div>
    <div class="prolific form segment">
		<div class="four fields">
	        <div class="field">
	          <label>Exchange Name</label>
	          <label class="input"><i class="icon-append icon asterisk"></i>
	            <input type="text" name="rate_name" placeholder"Display Name" value="<?php echo $aRateEdit->currency;?>" >
	          </label>
	        </div>
	        <div class="field">
	          <label>Exchange Deposit</label>
	          <label class="input"><i class="icon-append icon asterisk"></i>
	            <input type="text" name="rate_deposit" placeholder="0.00" value="<?php echo $aRateEdit->deposit;?>">
	          </label>
	        </div>
			<div class="field">
		          <label>Exchange Withdrawal</label>
		          <label class="input"><i class="icon-append icon asterisk"></i>
		            <input type="text" name="rate_withdraw" placeholder="0.00"  value="<?php echo $aRateEdit->withdraw;?>">
		          </label>
			</div>
			<div class="field">
				 <label>&nbsp;</label>
				 <label class="input">
				<button type="button" name="dosubmit" class="prolific positive button">Update Setting</button>
      			<input name="processGatewayRateExchange" type="hidden" value="1">
      			</label>
			</div>
		</div>
	</div>
    </form>
    <!-- START LIST EXCHANGE -->
      <table class="prolific sortable table">
      <thead>
        <tr>
          <th data-sort="int">#</th>
          <th data-sort="string">Exchange Name</th>
          <th class="disabled">RATE DEPOSIT</th>
          <th class="disabled">RATE WITHDRAW</th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php if(!$aRate):?>
        <tr>
          <td colspan="3"><?php echo Filter::msgSingleAlert("Your are not set abount exchange rate.");?></td>
        </tr>
        <?php else:?>
        <?php foreach ($aRate as $row2):?>
        <tr>
          <td><?php echo $row2->id;?>.</td>
          <td><?php echo $row2->currency;?></td>
          <td><?php echo $row2->deposit;?></td>
          <td><?php echo $row2->withdraw;?></td>
          <td>
          	<a href="index.php?do=gateways&amp;curencyid=<?php echo $row2->id;?>"><i class="rounded inverted success icon pencil link"></i></a>
			<a class="delete" data-title="<?php echo Lang::$word->_DELETE.' Record.'?>" data-option="deleteExchange" data-id="<?php echo $row2->id;?>" data-name="<?php echo $row2->currency;?>"><i class="rounded danger inverted remove icon link"></i></a>
		  </td>
        </tr>
        <?php endforeach;?>
        <?php unset($row2);?>
        <?php endif;?>
      </tbody>
    </table>
    <!-- END LIST EXCHANGE -->
    <!-- end setup rate exchange -->
    <div class="prolific fitted divider"></div>
    <table class="prolific sortable table">
      <thead>
        <tr>
          <th data-sort="int">#</th>
          <th data-sort="string"><?php echo Lang::$word->_GW_NAME;?></th>
          <th class="disabled"><?php echo Lang::$word->_ACTIONS;?></th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $gaterow = $member->getGateways();
		
        if(!$gaterow):
        ?>
        <tr>
          <td colspan="3"><?php echo Filter::msgSingleAlert(Lang::$word->_GW_NOGATEWAY);?></td>
        </tr>
        <?php else:?>
        <?php foreach ($gaterow as $k=>$row):?>
        <tr>
          <td><?php echo $row['id'];?>.</td>
          <td><?php echo $row['displayname'];?></td>
          <td><a href="index.php?do=gateways&amp;action=edit&amp;dir=<?php echo $row['dir'];?>"><i class="rounded inverted success icon pencil link"></i></a></td>
        </tr>
        <?php endforeach;?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<?php break;?>
<?php endswitch;?>