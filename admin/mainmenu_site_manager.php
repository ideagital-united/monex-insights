<?php if($user->getAcl("Menus") or $user->getAcl("Pages") or $user->getAcl("Plugins") or $user->getAcl("modules") or $user->getAcl("Layout") ):?>
	<li><a class="mortar <?php echo ((Filter::$do == 'menus' or Filter::$do == 'pages' or Filter::$do == 'plugins' or Filter::$do == 'modules' or Filter::$do == 'layout' ) && ($menuTrader =='')) ? "expanded" : "collapsed";?>"><i class="icon setting"></i><span>SITE MANAGEMENT</span></a>
		<ul class="subnav">
			<?php if($user->getAcl("Menus")):?>
				<li><a href="index.php?do=menus" class="green<?php if (Filter::$do == 'menus') echo " active";?>"><i class="icon reorder"></i><span><?php echo Lang::$word->_N_MENUS;?></span></a></li>
			<?php endif;?>
			<?php if($user->getAcl("Pages")):?>
				<li><a href="index.php?do=pages" class="green<?php if (Filter::$do == 'pages') echo " active";?>"><i class="icon file"></i><span><?php echo Lang::$word->_N_PAGES;?></span></a></li>
			<?php endif;?>
			<?php if($user->getAcl("Plugins")):?>
				<li><a href="index.php?do=plugins" class="green<?php if (Filter::$do == 'plugins') echo " active";?>"><i class="icon umbrella"></i><span><?php echo Lang::$word->_N_PLUGS;?></span></a></li>
			<?php endif;?>
			<?php if($user->getAcl("modules")):?>
				<li><a href="index.php?do=modules" class="green<?php if (Filter::$do =='modules') echo " active";?>"><i class="icon puzzle piece"></i><span><?php echo Lang::$word->_N_MODS;?></span></a></li>
			<?php endif;?>
			<?php if($user->getAcl("Layout")):?>
				<li><a href="index.php?do=layout" class="green<?php if (Filter::$do == 'layout') echo " active";?>"><i class="icon block layout"></i><span><?php echo Lang::$word->_N_LAYS;?></span></a></li>
			<?php endif;?>
		</ul>
	</li>
<?php endif;?>