<?php if($user->getAcl("modules") or $user->getAcl("trader_configuration")):?>
	<li><a class="mortar <?php echo (( $menuTrader == 'configuration' or $menuTrader == 'config_acc')) ? "expanded" : "collapsed";?>">
	<i class="icon settings"></i><span>TRADER SYSTEM SETTING</span></a>
		<ul class="subnav">
		<?php if($user->getAcl("trader_configuration")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=configuration" class="green<?php if ($menuTrader== 'configuration') echo " active";?>">
			<i class="icon setting"></i><span>TRADER CONFIGURATION</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("account_type_setting")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=config_acc" class="green<?php if ($menuTrader== 'config_acc') echo " active";?>">
			<i class="icon user"></i><span>ACCOUNT TYPE SETTING</span></a></li>
		<?php endif;?>
		</ul>
	</li>
<?php endif;?>