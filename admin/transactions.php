<?php
if(!$user->getAcl("Transactions")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
?>

<script>
var var_action='transaction_all';
</script>
<?php
include(MODPATH."tradersystem/script_datatable_ajax.php");
?>
<div class="prolific icon heading message coral"> <i class="payment icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_TR_TITLE1;?> </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=transactions" class="section"><?php echo Lang::$word->_N_TRANS;?></a>
      <div class="divider"> / </div>
      <div class="active section">View All Transactions</div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  <div class="prolific header"><?php echo Lang::$word->_TR_SUBTITLE1;?></div>



	<table border="1" width="100%"class="display table table-bordered table-striped data-table">
<thead>
  <tr>
		<th align="center" width="5%">Edit</th>
  	    <th align="center" width="10%">ID<!--trans_id--></th>
		<th align="center"width="22%">Customers<!--user_id--></th>
		<th align="center"width="15%">Amount(USD)<!--money_in--></th>
		<th align="center"width="5%">Status</th>
		<th align="center"width="20%">Processor</th>
		<th align="center">Payment Date<!-- save_date --></th>
  </tr>

</thead>
</table>

</div>
