<?php if($user->getAcl("deposit")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-pending" class="red <?php if ($menuTrader == 'deposit-list-pending') echo "active";?>">
			<i class="icon  sign in"></i><span>Deposit Pending</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("withdrawal")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-pending" class="red <?php if ($menuTrader == 'withdraw-list-pending') echo "active";?>">
			<i class="icon  sign out"></i><span>Withdrawal Pending</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("manageuserfund")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=manageuserfund" class="red <?php if ($menuTrader == 'manageuserfund') echo "active";?>">
			<i class="big money icon"></i><span>Manage User Fund</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Gateways")):?>
			<li><a href="index.php?do=gateways" class="red <?php if (Filter::$do == 'gateways') echo "active";?>"><i class="icon dollar"></i><span>Manage Payment Gateways</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Transactions")):?>
			<li><a href="index.php?do=transactions" class="red <?php if (Filter::$do == 'transactions') echo "active";?>"><i class="icon unordered list"></i><span><?php echo Lang::$word->_N_TRANS;?></span></a></li>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=report_export" class="red <?php if ($menuTrader == 'report_export') echo "active";?>"><i class="icon unordered list"></i>
				<span><?php echo Lang::$word->_N_TRANS;?> Export</span></a></li>
		<?php endif;?>
<!--
<?php if($user->getAcl("modules") or $user->getAcl("deposit")or $user->getAcl("withdrawal") or $user->getAcl("Gateways") or $user->getAcl("Transactions")):?>
	<li><a class="red active <?php echo (Filter::$do == 'transactions' or Filter::$do == 'gateways' or $menuTrader == 'deposit-list-pending' or $menuTrader == 'withdraw-list-pending') ? "expanded" : "collapsed";?>">
	<i class=" icon money "></i><span>FINANCIAL CONTROL</span></a>
		<ul class="subnav">
		<?php if($user->getAcl("deposit")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-pending" class="red<?php if ($menuTrader == 'deposit-list-pending') echo " active";?>">
			<i class="icon  sign in"></i><span>DEPOSIT</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("withdrawal")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-pending" class="red<?php if ($menuTrader == 'withdraw-list-pending') echo " active";?>">
			<i class="icon  sign out"></i><span>WITHDRAWAL</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Gateways")):?>
			<li><a href="index.php?do=gateways" class="red<?php if (Filter::$do == 'gateways') echo " active";?>"><i class="icon dollar"></i><span>MANANGE CURRENCIES</span></a></li>
		<?php endif;?>
		<?php if($user->getAcl("Transactions")):?>
			<li><a href="index.php?do=transactions" class="red<?php if (Filter::$do == 'transactions') echo " active";?>"><i class="icon unordered list"></i><span><?php echo Lang::$word->_N_TRANS;?></span></a></li>
		<?php endif;?>
		</ul>
	</li>
<?php endif;?>
-->