<?php
  /**
   * Login
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2011
   * @version $Id: login.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  
  define("_VALID_PHP", true);
  require_once("init.php");

	if(isset($_SESSION['admin_secure']['grant_access'])){
		redirect_to(ADMINURL.'/index.php');exit;
	} else {
		?>
		<div id="loadboxloginbroker">
		<?php
		Filter::msgAlert(Lang::$word->_CG_ONLYADMIN);
		?>
		</div>
		
		<?php
	}
