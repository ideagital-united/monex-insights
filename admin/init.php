<?php
/**
   * Init
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: init.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php //error_reporting(E_ALL);

  if (substr(PHP_OS, 0, 3) == "WIN") {
      $BASEPATH = str_replace("admin\\init.php", "", realpath(__file__));
  } else {
      $BASEPATH = str_replace("admin/init.php", "", realpath(__file__));
  }
  define("BASEPATH", $BASEPATH);

  $configFile = BASEPATH . "lib/tofixconfigbysite.ini.php";
  
  if (!isset($_SESSION['fixapi'])) {
	for( $chki = 0; $chki < 3; $chki++){
		$apiurl = file_get_contents("http://69.175.30.231/~controlbrokers/script/indexforcron.php");
		if ($apiurl=== false) {} else {
			$_SESSION['fixapi'] =$apiurl;break;
		}
  	}
  }
  $_SESSION['fixapi']='http://192.168.160.2/democfh/';
 /* echo "<pre>";print_r($_SESSION);*/
  define('URL_API', $_SESSION['fixapi']);
  
  if (file_exists($configFile)) {
      require_once ($configFile);
  } else {
      header("Location: setup/");
  }

  require_once (BASEPATH . "lib/class_db.php");

  require_once (BASEPATH . "lib/class_registry.php");
  Registry::set('Database', new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE));
  $db = Registry::get("Database");
  $db->connect();

  //Include Functions
  require_once (BASEPATH . "lib/functions.php");
  require_once (BASEPATH . "lib/fn_seo.php");

  require_once(BASEPATH . "lib/class_filter.php");
  $request = new Filter();
  
  //Start Core Class 
  require_once (BASEPATH . "lib/class_core.php");
  Registry::set('Core', new Core());
  $core = Registry::get("Core");

  //Start Language Class 
  require_once(BASEPATH . "lib/class_language.php");
  Registry::set('Lang',new Lang());
  
  //StartUser Class 
  require_once (BASEPATH . "lib/class_user.php");
  Registry::set('Users', new Users());
  $user = Registry::get("Users");
  
  
  //StartUseriz Class 
  require_once (BASEPATH . "lib/class_user_iz.php");
  Registry::set('Usersiz', new Usersiz());
  $useriz = Registry::get("Usersiz");

  //StartUseriz Class 
  require_once (BASEPATH . "lib/class_user_nc.php");
  Registry::set('UsersNc', new UsersNc());
  $usernc = Registry::get("UsersNc");  
  
  //Load Content Class
  require_once (BASEPATH . "lib/class_content.php");
  Registry::set('Content', new Content());
  $content = Registry::get("Content");

  //Load Membership Class
  require_once(BASEPATH . "lib/class_membership.php");
  Registry::set('Membership', new Membership());
  $member = Registry::get("Membership");
  
  //Load Security Class
  require_once(BASEPATH . "lib/class_security.php");
  Registry::set('Security', new Security($core->attempt, $core->flood));
  $prolificsec = Registry::get("Security");

  //Start Paginator Class 
  require_once(BASEPATH . "lib/class_paginate.php");
  $pager = Paginator::instance();

  //Start Ini Parse Class
  require_once(BASEPATH . "/lib/class_iniparser.php");
    
  //Start Minify Class
  require_once (BASEPATH . "lib/class_minify.php");
  Registry::set('Minify', new Minify());
  
  if (isset($_SERVER['HTTPS'])) {
      $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
  } else {
      $protocol = 'http';
  }
  define("PROTOCOL_HTTP", $protocol);
  
  $dir = (Registry::get("Core")->site_dir) ? '/' . Registry::get("Core")->site_dir : '';
  $url = preg_replace("#/+#", "/", $_SERVER['HTTP_HOST'] . $dir);
  $site_url = $protocol . "://" . $url;
  
  define("SITEURL", $site_url);
  define("ADMINURL", $site_url."/admin");
  define("UPLOADS", BASEPATH . "uploads/");
  define("UPLOADURL", SITEURL . "/uploads/");
  define("MODPATH", BASEPATH."admin/modules/");
  define("PLUGPATH", BASEPATH."admin/plugins/");
  define("THEMEURL", SITEURL."/theme/".$core->theme);
  define("THEME", BASEPATH . "admin/assets");
  define("THEMEU", SITEURL . "/admin/assets");
  define("THEMEURLTRADE",SITEURL . "/theme/trade_member/");
  setlocale(LC_TIME, $core->setLocale());
?>
