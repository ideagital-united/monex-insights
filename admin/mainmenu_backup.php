<?php
  /**
   * Mainmenu
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: mainmenu.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
$aaa1 = $user->getAcl("modules");
$aaa2 = Filter::$do;
$menuTrader = (isset($_REQUEST['menu']) && $_REQUEST['menu'] !='' && $_REQUEST['modname'] == 'tradersystem') ? $_REQUEST['menu'] : '';

?>
<div class="prolific styled pushed sidebar active" id="sidemenu">
  <div class="logo"><a href="index.php"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a></div>
  <nav>
    <ul>
      <li><a href="index.php" class="red<?php if (!Filter::$do) echo " active";?>"><i class="icon dashboard"></i><span><?php echo Lang::$word->_N_DASH;?></span></a></li>
      
      <?php if($user->getAcl("Menus") or $user->getAcl("Pages") or $user->getAcl("Plugins") or $user->getAcl("modules") or $user->getAcl("Layout") ):?>
      <li><a class="mortar <?php echo ((Filter::$do == 'menus' or Filter::$do == 'pages' or Filter::$do == 'plugins' or Filter::$do == 'modules' or Filter::$do == 'layout' ) && ($menuTrader =='')) ? "expanded" : "collapsed";?>"><i class="icon setting"></i><span>SITE MANAGEMENT<b>...</b></span></a>
        <ul class="subnav">
          <?php if($user->getAcl("Menus")):?>
	      <li><a href="index.php?do=menus" class="green<?php if (Filter::$do == 'menus') echo " active";?>"><i class="icon reorder"></i><span><?php echo Lang::$word->_N_MENUS;?></span></a></li>
	      <?php endif;?>
	      <?php if($user->getAcl("Pages")):?>
	      <li><a href="index.php?do=pages" class="green<?php if (Filter::$do == 'pages') echo " active";?>"><i class="icon file"></i><span><?php echo Lang::$word->_N_PAGES;?></span></a></li>
	      <?php endif;?>
	      <?php if($user->getAcl("Plugins")):?>
	      <li><a href="index.php?do=plugins" class="green<?php if (Filter::$do == 'plugins') echo " active";?>"><i class="icon umbrella"></i><span><?php echo Lang::$word->_N_PLUGS;?></span></a></li>
	      <?php endif;?>
	      <?php if($user->getAcl("modules")):?>
	      <li><a href="index.php?do=modules" class="green<?php if (Filter::$do =='modules') echo " active";?>"><i class="icon puzzle piece"></i><span><?php echo Lang::$word->_N_MODS;?></span></a></li>
	      <?php endif;?>
	      <?php if($user->getAcl("Layout")):?>
	      <li><a href="index.php?do=layout" class="green<?php if (Filter::$do == 'layout') echo " active";?>"><i class="icon block layout"></i><span><?php echo Lang::$word->_N_LAYS;?></span></a></li>
	      <?php endif;?>
        </ul>
      </li>
      <?php endif;?>
      
      <?php if($user->getAcl("Users") or $user->getAcl("modules") or $user->getAcl("document_control")):?>
      <li><a class="mortar <?php echo (Filter::$do == 'Users' or  (Filter::$do == 'modules' and  $_REQUEST['menu'] == 'member-document')) ? "expanded" : "collapsed";?>"><i class="icon users"></i><span>USERS MANAGEMENT</span></a>
        <ul class="subnav">
          <?php if($user->getAcl("Users")):?>
	      <li><a href="index.php?do=users" class="green<?php if (Filter::$do == 'users') echo " active";?>"><i class="icon user"></i><span>USERS</span></a></li>
	      <?php endif;?>
	      <?php if($user->getAcl("User_group")):?>
          <li><a href="index.php?do=user_group" class="green<?php if (Filter::$do == 'user_group') echo " active";?>"><i class="icon user"></i><span>USERS GROUP</span></a></li>
          <?php endif;?>
	      <?php if($user->getAcl("document_control")):?>
	      <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document" class="green<?php if ($_REQUEST['menu'] == 'member-document') echo " active";?>">
          <i class="icon suitcase"></i><span>DOCUMENT CONTROL</span></a></li>
           <?php endif;?>
        </ul>
      </li>
      <?php endif;?>
      
     
      
      <?php if($user->getAcl("modules") or $user->getAcl("trader_configuration")):?>
      <li><a class="mortar <?php echo (( $menuTrader == 'configuration' or $menuTrader == 'config_acc')) ? "expanded" : "collapsed";?>">
      	<i class="icon settings"></i><span>TRADER SYSTEM SETTING</span></a>
        <ul class="subnav">
			<?php if($user->getAcl("trader_configuration")):?>
			<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=configuration" class="green<?php if ($menuTrader== 'configuration') echo " active";?>">
          	<i class="icon setting"></i><span>TRADER CONFIGURATION</span></a></li>
          	<?php endif;?>
          	
          	<?php if($user->getAcl("account_type_setting")):?>
          	<li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=config_acc" class="green<?php if ($menuTrader== 'config_acc') echo " active";?>">
          	<i class="icon user"></i><span>ACCOUNT TYPE SETTING</span></a></li>
          	<?php endif;?>
         
        </ul>
      </li>
      <?php endif;?>
      
      <?php if($user->getAcl("modules") or $user->getAcl("deposit")or $user->getAcl("withdrawal") or $user->getAcl("Gateways") or $user->getAcl("Transactions")):?>
      <li><a class="mortar <?php echo (Filter::$do == 'transactions' or Filter::$do == 'gateways' or $menuTrader == 'deposit-list-pending' or $menuTrader == 'withdraw-list-pending') ? "expanded" : "collapsed";?>">
      	<i class="icon money "></i><span>FINANCIAL CONTROL</span></a>
        <ul class="subnav">
          <?php if($user->getAcl("deposit")):?>
          <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-pending" class="green<?php if ($menuTrader == 'deposit-list-pending') echo " active";?>">
        	<i class="icon  sign in"></i><span>DEPOSIT</span></a></li>
         <?php endif;?>
          <?php if($user->getAcl("withdrawal")):?>
         <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-pending" class="green<?php if ($menuTrader == 'withdraw-list-pending') echo " active";?>">
        	<i class="icon  sign out"></i><span>WITHDRAWAL</span></a></li>
         <?php endif;?>
          <?php if($user->getAcl("Gateways")):?>
          <li><a href="index.php?do=gateways" class="green<?php if (Filter::$do == 'gateways') echo " active";?>"><i class="icon dollar"></i><span>MANANGE CURRENCIES</span></a></li>
          <?php endif;?>
           <?php if($user->getAcl("Transactions")):?>
          <li><a href="index.php?do=transactions" class="green<?php if (Filter::$do == 'transactions') echo " active";?>"><i class="icon unordered list"></i><span><?php echo Lang::$word->_N_TRANS;?></span></a></li>
          <?php endif;?>
        </ul>
      </li>
      <?php endif;?>
      
      <?php if($user->getAcl("modules")):?>
      <li><a class="mortar <?php echo ($menuTrader== 'aff_config' or $menuTrader == 'aff_control' or $menuTrader == 'aff_tool') ? "expanded" : "collapsed";?>"><i class="icon sitemap "></i><span>IB MANAGEMENT</span></a>
        <ul class="subnav">
         <!-- <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=aff_config" class="green<?php if ($menuTrader == 'aff_config') echo " active";?>">
            <i class="icon sitemap"></i><span>IB COMMISSION</span></a></li> 
          <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=aff_control" class="green<?php if ($menuTrader == 'aff_control') echo " active";?>">
            <i class="icon sitemap"></i><span>IB CONTROL</span></a></li>
         <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=aff_tool" class="green<?php if ($menuTrader == 'aff_tool') echo " active";?>">
            <i class="icon sitemap"></i><span>IB TOOL</span></a></li>   
            -->
            
         <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=commission-rule-configuration" class="green<?php if ($menuTrader == 'commission-rule-configuration') echo " active";?>">
            <i class="icon sitemap"></i><span>IB RULE SETTING</span></a></li>
        
            
         <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration" class="green<?php if ($menuTrader == 'commission-level-configuration') echo " active";?>">
            <i class="icon sitemap"></i><span>IB LEVEL SETTING</span></a></li>
        
        </ul>
      </li>
      <?php endif;?>
      
       <?php if($user->getAcl("modules") or $user->getAcl("secure_sms_setting") or $user->getAcl("secure_pin_code_setting") or $user->getAcl("secure_ip_check_setting")or $user->getAcl("secure_question_setting")):?>
      <li><a class="mortar <?php echo ($menuTrader == 'ss_sms' or $menuTrader == 'ss_pincode' or $menuTrader == 'ss_ipcheck' or $menuTrader == 'ss_question') ? "expanded" : "collapsed";?>">
      	<i class="icon lock "></i><span>SECURITY MANAGEMENT</span></a>
        <ul class="subnav">
        <?php if($user->getAcl("secure_sms_setting")):?>
         <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_sms" class="green<?php if ($menuTrader == 'ss_sms') echo " active";?>">
        	<i class="icon tablet"></i><span>SMS SETTING</span></a></li>
         <?php endif;?>
         <?php if($user->getAcl("secure_pin_code_setting")):?>
         <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_pincode" class="green<?php if ($menuTrader == 'ss_pincode') echo " active";?>">
        	<i class="icon key  "></i><span>PIN CODE SETTING</span></a></li>
        <?php endif;?>
        <?php if($user->getAcl("secure_ip_check_setting")):?>
        <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_ipcheck" class="green<?php if ($menuTrader== 'ss_ipcheck') echo " active";?>">
        	<i class="icon screenshot "></i><span>IP CHECK SETTING</span></a></li>
        <?php endif;?>
        <?php if($user->getAcl("secure_question_setting")):?>
        <li><a href="index.php?do=modules&action=config&modname=tradersystem&menu=ss_question" class="green<?php if ($menuTrader== 'ss_question') echo " active";?>">
        	<i class="icon question "></i><span>QUESTION SETTING</span></a></li>
        <?php endif;?>
        </ul>
      </li>
      <?php endif;?>
        <?php if($user->getAcl("Configuration") or $user->getAcl("Templates") or $user->getAcl("Newsletter") or $user->getAcl("Language") or $user->getAcl("Maintenance") or $user->getAcl("Logs") or $user->getAcl("Backup") or $user->getAcl("FM") or $user->getAcl("Fields") or $user->getAcl("System")):?>
      <li><a class="mortar <?php echo (Filter::$do == 'config' or Filter::$do == 'templates' or Filter::$do == 'newsletter' or Filter::$do == 'language' or Filter::$do == 'maintenance' or Filter::$do == 'logs' or Filter::$do == 'fields' or Filter::$do == 'backup' or Filter::$do == 'filemanager' or Filter::$do == 'system') ? "expanded" : "collapsed";?>"><i class="icon setting"></i><span><?php echo Lang::$word->_N_CONF;?><b>...</b></span></a>
        <ul class="subnav">
          <?php if($user->getAcl("Configuration")):?>
          <li><a href="index.php?do=config" class="green<?php if (Filter::$do == 'config') echo " active";?>"><i class="icon setting"></i><span><?php echo Lang::$word->_CG_TITLE1;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("Templates")):?>
          <li><a href="index.php?do=templates" class="green<?php if (Filter::$do == 'templates') echo " active";?>"><i class="icon mail"></i><span><?php echo Lang::$word->_N_EMAILS;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("Newsletter")):?>
          <li><a href="index.php?do=newsletter" class="green<?php if (Filter::$do == 'newsletter') echo " active";?>"><i class="icon mail reply"></i><span><?php echo Lang::$word->_N_NEWSL;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("Fields")):?>
          <li><a href="index.php?do=fields" class="green<?php if (Filter::$do == 'fields') echo " active";?>"><i class="icon tasks"></i><span><?php echo Lang::$word->_N_FIELDS;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("Language")):?>
          <li><a href="index.php?do=language" class="green<?php if (Filter::$do == 'language') echo " active";?>"><i class="icon chat"></i><span><?php echo Lang::$word->_N_LANGS;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("Maintenance")):?>
          <li><a href="index.php?do=maintenance" class="green<?php if (Filter::$do == 'maintenance') echo " active";?>"><i class="icon wrench"></i><span><?php echo Lang::$word->_N_SMTCN;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("Backup")):?>
          <li><a href="index.php?do=backup" class="green<?php if (Filter::$do == 'backup') echo " active";?>"><i class="icon hdd"></i><span><?php echo Lang::$word->_N_BACK;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("FM")):?>
          <li><a href="index.php?do=filemanager" class="green<?php if (Filter::$do == 'filemanager') echo " active";?>"><i class="icon folder"></i><span><?php echo Lang::$word->_N_FM;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("System")):?>
          <li><a href="index.php?do=system" class="green<?php if (Filter::$do == 'system') echo " active";?>"><i class="icon laptop"></i><span><?php echo Lang::$word->_N_SYSTEM;?></span></a></li>
          <?php endif;?>
          <?php if($user->getAcl("Logs")):?>
          <li><a href="index.php?do=logs" class="green<?php if (Filter::$do == 'logs') echo " active";?>"><i class="icon shield"></i><span><?php echo Lang::$word->_N_LOGS;?></span></a></li>
          <?php endif;?>
        </ul>
      </li>
      <?php endif;?>
     
    </ul>
  </nav>
  <div class="sublist"></div>
</div>