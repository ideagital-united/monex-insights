<?php
  /**
   * Users
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: users.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
      
  if(!$user->getAcl("Users")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

?>
<?php switch(Filter::$action): case "edit": ?>
<?php if($user->userlevel == 8 and $user->userid == 1): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;?>
<?php 

//echo "id=".Filter::$id;

//$row = Core::getRowById(Users::uTable, Filter::$id); 

$rows = $useriz->getUserGroupByTypeId(Filter::$id);

$row = $rows['message'][0];

//echo "<pre>"; print_r($row); echo "</pre>";

$row_limit_access = str_replace("::", ",", $row['limit_access']);

//echo "row_limit=".$row_limit_access;



?>
<?php // $memrow = $member->getMemberships();?>


<div class="prolific icon heading message dust"><a class="helper prolific top right info corner label" data-help="user"><i class="icon help"></i></a> <i class="user icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_URG_TITLE3;?></div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=user_group" class="section"><?php echo Lang::$word->_N_USERS_GROUP;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_URG_TITLE1; ?></div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_URG_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="prolific form segment">
    <div class="prolific header"><?php echo Lang::$word->_URG_SUBTITLE1 . $row['type_name']; ?></div>
    <div class="prolific double fitted divider"></div>
    <form id="prolific_form" name="prolific_form" method="post">
        
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_USER_GROUP_ID;?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row['type_id'];?>" disabled="disabled">
          </label>
        </div>
        
      </div>
      
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_USER_GROUP_NAME; ?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" value="<?php echo $row['type_name'];?>" name="type_name" >
          </label>
        </div>
        
      </div>
     
      <?php if($user->userlevel == 9):?>
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_UR_PERM;?></label>
          <?php echo $user->getPermissionList($row_limit_access);?> 
        </div>
      </div>
      <?php endif;?>
     
      <div class="prolific double fitted divider"></div>
      <button type="button" name="dosubmit" class="prolific positive button"><?php echo Lang::$word->_UR_UPDATE;?></button>
      <a href="index.php?do=user_group" class="prolific basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processUserGroup" type="hidden" value="1">
      <input name="type_id" type="hidden" value="<?php echo $row['type_id']; ?>">
     
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php case"add": ?>
<?php // $memrow = $member->getMemberships();?>
<div class="prolific icon heading message dust"><a class="helper prolific top right info corner label" data-help="user"><i class="icon help"></i></a> <i class="user icon"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_URG_TITLE3;?> </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <a href="index.php?do=user_group" class="section"><?php echo Lang::$word->_N_USERS_GROUP; ?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_URG_TITLE2; ?></div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_URG_INFO2. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="prolific form segment">
    <div class="prolific header"><?php echo Lang::$word->_URG_SUBTITLE2; ?></div>
    <div class="prolific double fitted divider"></div>
    <form id="prolific_form" name="prolific_form" method="post">
      
      <?php if($user->userlevel == 9):?> 
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_USER_GROUP_NAME; ?></label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input type="text" placeholder="<?php echo Lang::$word->_USER_GROUP_NAME; ?>" name="type_name">
          </label>
        </div>
      </div>
      
      <div class="two fields">
        <div class="field">
          <label><?php echo Lang::$word->_UR_PERM; ?></label>
          <?php echo $user->getPermissionList();?> 
        </div>
      </div>
      <?php endif;?>
     
      <div class="prolific double fitted divider"></div>
      <button type="button" name="dosubmit" class="prolific positive button"><?php echo Lang::$word->_UR_ADD_USER_GROUP;?></button>
      <a href="index.php?do=user_group" class="prolific basic button"><?php echo Lang::$word->_CANCEL;?></a>
      <input name="processUserGroup" type="hidden" value="1">
    
    </form>
  </div>
  <div id="msgholder"></div>
</div>
<?php break;?>
<?php default:?>
<?php  
$usergrouprow = $useriz->getUserGroup(); 
//echo "<pre>"; print_r($usergrouprow); echo "</pre>";
?>
<div class="prolific icon heading message dust"> <i class="icon user"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_URG_TITLE3;?></div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_N_USERS_GROUP; ?></div>
    </div>
  </div>
</div>
  <div id="msgholder"></div>
<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_URG_INFO3; ?></div>
  <div class="prolific segment"> <a class="prolific icon positive button push-right" href="index.php?do=user_group&amp;action=add"><i class="icon add"></i> <?php echo Lang::$word->_UR_ADD_USER_GROUP;?></a>
    <div class="prolific header"><?php echo Lang::$word->_URG_SUBTITLE3; ?></div>
    <div class="prolific fitted divider"></div>
    <!-- <div class="prolific small form basic segment">
      <form method="post" id="prolific_form" name="prolific_form">
        <div class="four fields">
          <div class="field">
            <div class="prolific input"> <i class="icon-prepend icon calendar"></i>
              <input name="fromdate" type="text" data-datepicker="true" placeholder="<?php echo Lang::$word->_UR_SHOW_FROM;?>" id="fromdate" />
            </div>
          </div>
          <div class="field">
            <div class="prolific action input"> <i class="icon-prepend icon calendar"></i>
              <input name="enddate" type="text" data-datepicker="true" placeholder="<?php echo Lang::$word->_UR_SHOW_TO;?>" id="enddate" />
              <a id="doDates" class="prolific icon button"><?php echo Lang::$word->_SR_SEARCH_GO;?></a> </div>
          </div>
          <div class="field">
            <div class="prolific icon input">
              <input type="text" name="usersearchfield" placeholder="<?php echo Lang::$word->_UR_FIND_UNAME;?>" id="searchfield"  />
              <i class="search icon"></i>
              <div id="suggestions"> </div>
            </div>
          </div>
          <div class="field">
            <div class="two fields">
              <div class="field"> <?php echo $pager->items_per_page();?> </div>
              <div class="field"> <?php echo $pager->jump_menu();?> </div>
            </div>
          </div>
        </div>
      </form>
      <div class="prolific divider"></div>
      <div id="abc"> <?php echo alphaBits('index.php?do=user_group', "letter");?> </div>
      <div class="prolific fitted divider"></div>
    </div> -->
    <table class="prolific sortable table">
      <thead>
        <tr>
          <th data-sort="int">#</th>
          <th data-sort="string"><?php echo Lang::$word->_USER_GROUP_NAME; ?></th>
          
          <th class="disabled"><?php echo Lang::$word->_URG_PERM; ?></th>
          
         
          <th class="disabled"><?php echo Lang::$word->_ACTIONS; ?></th>
        </tr>
      </thead>
      <tbody>
        <?php if($usergrouprow['status'] != 'success'):?>
        <tr>
          <td colspan="7"><?php echo Filter::msgSingleAlert(Lang::$word->_URG_NOGROUP);?></td>
        </tr>
        <?php else:?>
        <?php 
        $row = $usergrouprow['message'];
       // foreach ($usergrouprow['message'] as $row): 
       for($xx=0;$xx<count($row);$xx++) {
       ?>
        <tr>
          <td><?php echo $row[$xx]['type_id']; ?>.</td>
          
          <td><?php echo $row[$xx]['type_name']; ?></td>
         
          <td><?php echo str_replace("::", "," , $row[$xx]['limit_access']); ?></td>
          
          <td><?php if ($row[$xx]['type_id'] != 9) { ?><a href="index.php?do=user_group&amp;action=edit&amp;id=<?php echo $row[$xx]['type_id']; ?>"><i class="rounded inverted success icon pencil link"></i></a><?php } ?></td>
        </tr>
        <?php } //endforeach; ?>
        <?php unset($row);?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
  <div class="prolific-grid">
    <div class="two columns horizontal-gutters">
      <div class="row"> <span class="prolific label"><?php echo Lang::$word->_PAG_TOTAL.': '.$pager->items_total;?> / <?php echo Lang::$word->_PAG_CURPAGE.': '.$pager->current_page.' '.Lang::$word->_PAG_OF.' '.$pager->num_pages;?></span> </div>
      <div class="row">
        <div id="pagination"><?php echo $pager->display_pages();?></div>
      </div>
    </div>
  </div>
</div>
<?php break;?>
<?php endswitch;?>