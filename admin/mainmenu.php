<?php
  /**
   * Mainmenu
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: mainmenu.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  
$aaa1 = $user->getAcl("modules");
$aaa2 = Filter::$do;
$menuTrader = (isset($_REQUEST['menu']) && $_REQUEST['menu'] !='' && $_REQUEST['modname'] == 'tradersystem') ? $_REQUEST['menu'] : '';

?>
<div class="prolific styled pushed sidebar active" id="sidemenu">
	<div class="logo"><a href="index.php"><?php echo ($core->logo) ? '<img src="'.SITEURL.'/uploads/'.$core->logo.'" alt="'.$core->company.'" />': $core->company;?></a></div>
		<nav>
			<ul>
			<li><a href="index.php" class="mortar <?php if (!Filter::$do) echo "active";?>"><i class="icon dashboard"></i><span><?php echo Lang::$word->_N_DASH;?></span></a></li>
			<?php
				
				include_once('mainmenu_site_manager.php');
				
				
				
               
                /* include_once('mainmenu_trader_system_setting.php'); */
				
				echo "<!--";
				include_once('mainmenu_security_management.php');
				echo "-->";
				
				include_once('mainmenu_configuration.php');				
				?>				
				<li><a href="index.php?do=report" class="mortar <?php if (Filter::$do == 'report') { echo "active"; } ?>"><i class="icon align justify icon"></i><span>Report</span></a></li>
				<?php				
				
				if (isset($_SESSION['admin_secure']['grant_access'])) {
				    
					if ($_SESSION['admin_secure']['grant_access'] == 'all') { ?>
						<li><a href="index.php?do=logout_broker" class="red <?php if (Filter::$do == 'logout_broker') echo "active";?>"><i class="icon eject"></i><span><?php echo Lang::$word->_N_BLOGOUT;?></span></a></li>
						<!--<li><a href="index.php?do=create_admin" class="red <?php if (Filter::$do == 'create_admin') echo "active";?>"><i class="icon certificate"></i><span><?php echo Lang::$word->_N_SET_ADMIN;?></span></a></li>-->
						<?php
						 include_once('mainmenu_user_management.php');
                      include_once('mainmenu_user_group_management.php');
						 include_once('mainmenu_financial_control.php');     			       
                      include_once('mainmenu_document_control.php');						 
                      include_once('mainmenu_document.php');						 
                      include_once('mainmenu_ib_management.php');
						
                     } else {
                         
                              if (strpos($_SESSION['admin_secure']['grant_access'],'user_management') !== false) {
                                   include_once('mainmenu_user_management.php'); 
                           } 
                              if (strpos($_SESSION['admin_secure']['grant_access'],'user_group_management') !== false) {
                                   include_once('mainmenu_user_group_management.php'); 
                           } 
					          if (strpos($_SESSION['admin_secure']['grant_access'],'Transactions') !== false) {
					               include_once('mainmenu_transaction_control.php');  
                           }     
                              if (strpos($_SESSION['admin_secure']['grant_access'],'deposit') !== false) {
                                   include_once('mainmenu_deposit_control.php');                                                            
                           }                                                                
                              if (strpos($_SESSION['admin_secure']['grant_access'],'withdrawal') !== false) {
                                   include_once('mainmenu_withdrawal_control.php');                             
                           }                                                                  
                              if (strpos($_SESSION['admin_secure']['grant_access'],'document') !== false) {
                                   include_once('mainmenu_document.php');                             
                           }                                
                              if (strpos($_SESSION['admin_secure']['grant_access'],'document_control') !== false) {
                                   include_once('mainmenu_document_control.php');                             
                              }                                                                
                              if (strpos($_SESSION['admin_secure']['grant_access'],'manageuserfund') !== false) {
                                   include_once('mainmenu_userfund_control.php'); 
                           }                                                               
                              if (strpos($_SESSION['admin_secure']['grant_access'],'ibmanagement') !== false) {
                                   include_once('mainmenu_ib_management.php');                             
                          }                                                                                                                               
                             ?> 
						<li><a href="index.php?do=logout_broker" class="red<?php if (!Filter::$do) { echo " active"; } ?>"><i class="icon eject"></i><span><?php echo Lang::$word->_N_BLOGOUT;?></span></a></li>
					<?php }
				} else { ?>
					<li><a href="javascript:void(0);" id="login_secure" class="red<?php if (Filter::$do == 'login_broker') { echo " active"; } ?>"><i class="icon sign in"></i><span><?php echo Lang::$word->_N_BLOGIN;?></span></a></li>
				<?php } 
				
				?>
			</ul>
		</nav>
	<div class="sublist"></div>
</div>
<!--
index.php?do=login_broker
-->






