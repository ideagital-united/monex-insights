<?php
  /**
   * Main
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>

<?php $data6 = $core->getDigiShopStats();?>
<?php $data7 = $core->getBookingStats();?>
<div class="prolific icon heading message red"> <i class="icon home"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MN_TITLE;?> </div>
    <div class="prolific breadcrumb">
      <div class="active section"><i class="icon dashboard"></i> <?php echo Lang::$word->_N_DASH;?></div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  
  <div class="prolific segment">
    <div class="prolific header"><?php echo Lang::$word->_MN_SUBTITLE;?></div>

    <?php if($user->userlevel == 9):?>

    <div class="prolific-grid">
      <div class="two columns small-horizontal-gutters">
        <?php if($data6):?>
        <div class="row">
          <div class="prolific purple basic segment">
            <div class="screen-30"><i class="huge dimmed payment icon"></i> </div>
            <div class="screen-70">
              <div class="prolific caps"><?php echo Lang::$word->_MN_DIGITOTAL;?></div>
              <div class="prolific big font"><?php echo $core->formatMoney($data6->totalsale, false)?></div>
            </div>
            <div class="screen-100">
              <div class="prolific caps"><?php echo Lang::$word->_MN_TOTAL_AVERAGE_S;?></div>
              <div><?php echo $core->formatMoney($data6->average, false);?></div>
            </div>
          </div>
        </div>
        <?php endif;?>
        <?php if($data7):?>
        <div class="row">
          <div class="prolific danger basic segment">
            <div class="screen-30"><i class="huge dimmed payment icon"></i> </div>
            <div class="screen-70">
              <div class="prolific caps"><?php echo Lang::$word->_MN_BOOKTOTAL;?></div>
              <div class="prolific big font"><?php echo $core->formatMoney($data7->totalsale, false)?></div>
            </div>
            <div class="screen-100">
              <div class="prolific caps"><?php echo Lang::$word->_MN_TOTAL_AVERAGE_S;?></div>
              <div><?php echo $core->formatMoney($data7->average, false);?></div>
            </div>
          </div>
        </div>
        <?php endif;?>
      </div>
    </div>
    <div class="prolific divider"></div>
    <?php endif;?>
    <div class="clearfix small-bottom-space"> <a href="index.php" class="prolific right labeled icon button push-left sdelete"><i class="trash icon"></i><?php echo Lang::$word->_MN_EMPTY_STATS;?></a>
      <div class="prolific selection dropdown push-right" data-select-range="true">
        <div class="text"><?php echo Lang::$word->_MN_RANGE;?></div>
        <i class="dropdown icon"></i>
        <div class="menu">
          <div class="item" data-value="day"><?php echo Lang::$word->_MN_TODAY;?></div>
          <div class="item" data-value="week"><?php echo Lang::$word->_MN_WEEK;?></div>
          <div class="item" data-value="month"><?php echo Lang::$word->_MN_MONTH;?></div>
          <div class="item" data-value="year"><?php echo Lang::$word->_MN_YEAR;?></div>
        </div>
        <input name="range" type="hidden" value="">
      </div>
    </div>
    <div id="chart" style="height:400px;overflow:hidden"></div>
    
   
   
  

   
   
  </div>
</div>
<script type="text/javascript" src="../assets/jquery.flot.js"></script> 
<script type="text/javascript" src="../assets/flot.resize.js"></script> 
<script type="text/javascript" src="../assets/excanvas.min.js"></script> 

<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
   $("[data-select-range]").on('click', '.item', function () {
            v = $("input[name=range]").val();
            getVisitsChart(v)
        });



        function getVisitsChart(range) {
            $.ajax({
                type: 'GET',
                url: 'controller.php?getVisitsStats=1&timerange=' + range,
                dataType: 'json',
                async: false,
                success: function (json) {
                    var option = {
                        shadowSize: 0,
                        lines: {
                            show: true
                        },
                        points: {
                            show: true
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            borderColor: {
                                top: "#FFF",
                                left: "#FFF"
                            }
                        },
                        xaxis: {
                            ticks: json.xaxis
                        }
                    }
                    $.plot($('#chart'), [json.hits, json.visits], option);
                }
            });
        }
$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");

		$("#chart").bind("plothover", function (event, pos, item) {

			
				
				$("#hoverdata").text('str');
			

			
				if (item) {
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

					$("#tooltip").html(item.series.label + " of " + x + " = " + y)
						.css({top: item.pageY+5, left: item.pageX+5})
						.fadeIn(200);
				} else {
					$("#tooltip").hide();
				}
			
		});

		$("#chart").bind("plotclick", function (event, pos, item) {
			if (item) {
				$("#clickdata").text(" - click point " + item.dataIndex + " in " + item.series.label);
				plot.highlight(item.series, item.datapoint);
			}
		});
        getVisitsChart('year');
});
// ]]>
</script> 