<?php
  /**
   * Main
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: main.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>

<div class="prolific icon heading message red"> <i class="icon home"></i>
  <div class="content">
    <div class="header"> <?php echo Lang::$word->_MN_TITLE;?> </div>
    <div class="prolific breadcrumb">
      <div class="active section"><i class="icon dashboard"></i> <?php echo Lang::$word->_N_DASH;?></div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  
  <div class="prolific segment">
    <div class="prolific header"><?php echo Lang::$word->_MN_SUBTITLE;?></div>

    <?php if($user->userlevel == 9):?>

    <div class="prolific-grid">
      <div class="three columns small-horizontal-gutters">
       
        <div class="row">
          <div class="prolific purple basic segment">
            <div class="screen-30"><i class="huge dimmed payment icon"></i> </div>
            <div class="screen-70">
              <div class="prolific caps">TOTAL DEPOSIT</div>
              <div class="prolific big font" id ="display_deposit">0.00</div>
            </div>
            <div class="screen-100">
              <div class="prolific caps">Data Date</div>
              <div class="inputdatedata"></div>
            </div>
          </div>
        </div>
       
        <div class="row">
          <div class="prolific danger basic segment">
            <div class="screen-30"><i class="huge dimmed payment icon"></i> </div>
            <div class="screen-70">
              <div class="prolific caps">TOTAL WITHDRAW</div>
              <div class="prolific big font" id="display_withdraw">0.00</div>
            </div>
            <div class="screen-100">
              <div class="prolific caps">Data Date</div>
              <div class="inputdatedata"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="prolific purple basic segment">
            <div class="screen-50">
              <div class="prolific caps">Daily</div>
              <div class="prolific big font" id="display_dd">0.00</div>
            </div>
            <div class="screen-50">
              <div class="prolific caps">Monthly</div>
              <div class="prolific big font" id="display_mm">0.00</div>
            </div>
            <div class="screen-50">
              <div class="prolific caps">Balance</div>
              <div class="prolific big font" id="display_balance">0.00</div>
            </div>
            <div class="screen-50">
              <div class="prolific caps">Equity</div>
              <div class="prolific big font" id="display_eq">0.00</div>
            </div>
          </div>
        </div>
       
      </div>
    </div>
    <div class="prolific divider"></div>
    <?php endif;?>
    <div class="clearfix small-bottom-space"> 
    	 <div class="prolific form segment">
    	<div class="four fields">
		<div class="field">
        	<label>ACCOUNT</label>
        	<label class="input"><input type="text" name="account" id= "account" value="<?php echo $_POST['account'];?>"></label>
        </div>
		<div class="field">
        	<label>DATE START</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpStart" id="dpStart" value="<?php echo $_POST['dpEnd'];?>" class="picker__input picker__input--active"></label>
        </div>
        <div class="field">
        	<label>DATE END</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpEnd" id="dpEnd" value="<?php echo $_POST['dpEnd'];?>" class="picker__input picker__input--active"></label>
        </div>
         <div class="field">
        	<label>&nbsp;</label>
        	<label class="input"><button type="button" name="doSearch" id="btn_search_by_date" class="prolific positive button">Search</button></label>
        </div>
		
        
	   </div>
	   </div>
    </div>
    <div id="nvd3_cumulativeLine" class="chart" style="width:100%;height:354px"><svg></svg></div>
    
   
   
  

   
   
  </div>
</div>
<!-- nvd3 charts -->
<!-- nvd3 charts -->
<link rel="stylesheet" href="<?php echo THEMEURLTRADE;?>assets/lib/novus-nvd3/nv.d3.min.css">
<script src="<?php echo THEMEURLTRADE;?>assets/lib/d3/d3.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/novus-nvd3/nv.d3.min.js"></script>
<!-- flot charts-->
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo THEMEURLTRADE;?>assets/lib/flot/jquery.flot.tooltip.min.js"></script>

<script type="text/javascript">
// <![CDATA[
$(document).ready(function () {
	function get_report() {
		$.ajax({
			  url: "controller2.php" ,
			  type: 'POST',
			  data:{doReportDetail : 1,acc:$('#account').val(),dtstart:$('#dpStart').val(),dtend:$('#dpEnd').val()},
			  dataType :'json'
			}).done(function(obj) {
				$('#display_dd').html(obj.daily);
				$('#display_mm').html(obj.monly);
				$('#display_balance').html('$'+obj.balnace);
				$('#display_eq').html('$'+obj.equity);
				$('#display_deposit').html('$'+obj.deposit);
				$('#display_withdraw').html('$'+obj.withdraw);
				$('.inputdatedata').html(obj.showdate);
			});
	}
	get_report();
$('#btn_search_by_date').click(function(){
	get_report();
	cumulativeTestDatadd();
});
  function cumulativeTestDatadd() {
					var chart_browsers_data_new = [];
					$.ajax({
					  url: "controller2.php" ,
					  type: 'POST',
					  data:{doReportAdmin : 1,dtstart:$('#dpStart').val(),dtend:$('#dpEnd').val(),acc:$('#account').val()},
					  dataType :'json'
					}).done(function(obj) {
						if ($('#nvd3_cumulativeLine').length) {
				nv.addGraph(function() {
				    var chart = nv.models.linePlusBarChart()
				      .margin({top: 30, right: 60, bottom: 50, left: 70})
				      .x(function(d,i) { return i })
				      .y(function(d) { return d[1] })
				      .color(["#64B92A","#C0392B","#0000FF"])
				      ;

				    chart.xAxis
				      .showMaxMin(false)
				      .tickFormat(function(d) {
				        var dx = cumulativeTestData()[0].values[d] && cumulativeTestData()[0].values[d][0] || 0;
				        return d3.time.format('%x')(new Date(dx))
				      });

				    chart.y1Axis
				      .tickFormat(function(d) { return '$' + d3.format(',f')(d) });

				    chart.y2Axis
				      .tickFormat(function(d) { return '$' + d3.format(',f')(d) });

				    chart.bars.forceY([0]);

				    d3.select('#nvd3_cumulativeLine svg')
				      .datum(obj)
				      .transition().duration(500)
				      .call(chart)
				      ;

				    nv.utils.windowResize(chart.update);

				    return chart;
				});
			
				function cumulativeTestData() {
					return obj;
					
				}
			}
					});
					}
					cumulativeTestDatadd();
        
       
});
// ]]>
</script> 