<div class="prolific icon heading message dust"> <i class="icon user"></i>
  <div class="content">
    <div class="header"> Manage User Fund. </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
     
    </div>
  </div>
</div>

<div class="prolific-large-content">
  <div class="prolific segment"> 
    <div class="prolific header">Search User.</div>
    <div class="prolific fitted divider"></div>
    <div class="prolific small form basic segment">
      <form method="post" id="prolific_form" name="prolific_form">
        <div class="two fields">
      
          <div class="field">
            <div class="prolific icon input">
              <input type="text" name="usersearchfield" placeholder="<?php echo Lang::$word->_UR_FIND_UNAME;?>" id="searchfieldall"  />
              <i class="search icon"></i>
              <div id="suggestions"> </div>
            </div>
          </div>
          
            <div class="field">
            <div class="prolific icon input">
               <!--<button type="button" name="dosubmit" class="prolific positive button"> <i class="search icon"></i> SEARCH</button>-->
            </div>
          </div>
          </div>
      </form>
        </div>
    <?php if($_REQUEST['itype'] != '' && $_REQUEST['txtsearch'] != '') {?>


<?php
//=== end edit
//=== start view
include(MODPATH."tradersystem/script_datatable.php");
?>
<div>
<h1>Search Result(s)</h1>
</div>

<div>
	<table border="1" width="100%" class="display table table-bordered table-striped data-table">

<thead>
  <tr>
    <th><div  align="center">-</div></th>
    <th><div  align="center">Account Trade </div></th>
    <th><div  align="center">Username</div></th>
    <th><div  align="center">Name</div></th>
  </tr>
</thead>

<tbody>
<?php
$condi = "";
switch($_REQUEST['itype']){
	case 'acclogin' : $condi = " AND a.trader_acc_login like '%".$_REQUEST['txtsearch']."%' "; break;
	case 'pkid' : $condi = " AND u.id ='".$_REQUEST['txtsearch']."' "; break;
}
$sql = "SELECT 
			u.id as userid, u.username, u.email, u.created, u.avatar, CONCAT(u.fname,' ',u.lname) as nameuser,
			a.id as acc_login_id,a.trader_acc_login
		 FROM 
		 	users u LEFT OUTER JOIN bk_trader_acc a ON a.uid = u.id
		 WHERE
		 	u.id !=''
		 	 ".$condi." 
		 ORDER BY 
		 	u.id LIMIT 10";


$data = Registry::get("Database")->fetch_all($sql);

if ($data) {
	$i = 0;
	$view_acc = '';
	foreach ($data as $k => $v)
	{
		$view_acc = '';
		if ($_SESSION['account_last_view'] == $v->trader_acc_login){
			 $view_acc = "setviewopen";
		}
		
		
?>
	
 	<tr class="gradeA">
		<td align="center">
		<a href="javascript:void(0);" id="<?php echo $view_acc;?>" class="mgrdetail" email="<?php echo $v->email;?>" acc="<?php echo $v->trader_acc_login;?>">
		<i class="rounded inverted success icon pencil link"></i>
		</a>
	
		</td>
	   	<td align="center"><?php echo $v->trader_acc_login;?></td>
	   	<td align="center"><?php echo $v->email;?></td>
	   	<td align="center"><?php echo $v->nameuser;?></td>
  	</tr>
<?php 
	$i++;
}
}
?> 	
</tbody>
</table>
 <div class="prolific double fitted divider"></div>
 
<div id="show_detail_data" class="prolific form">
 <div class="prolific small form basic segment">
 	  <div>
 	  	  <div id="msgholder"></div>
      <form method="post" id="prolific_form_detail" name="prolific_form_detail">
      	<input type="hidden" name="dlt_pkid" id="dlt_pkid" value="">
      	<input type="hidden" name="dlt_email" id="dlt_email" value="">
      	<div class="two fields">
      		<div class="field">
      		    <fieldset><legend><div class="prolific header">User Detail:</div></legend>
      			<div id="shwodetaildata1"></div>
      			</fieldset>
      		</div>
      		
      		<div class="field">
      		    <fieldset><legend><div class="prolific header">Trader Account Detail:</div></legend>
      			<div id="shwodetaildata2"></div>
      			</fieldset>
      		</div>
      		
      		
      	</div>
    
        <div class=" two fields">
          <div class="field">
          	<fieldset><legend><div class="prolific header">Deposit/Withdrawal,Account : <span id="mgr_account_trade_login"></span></div></legend>
			<p class="screen-50 tablet-40 phone-100">
      		 amount :<input type="text" id="money_modify" placeholder="0.00" name="money_modify"></p>
      		 <p class="screen-100 tablet-40 phone-100"></p>
      		 <p class="screen-50 tablet-40 phone-100">
      		 To :<label for="inputto_wallet">
      		 	<input type="radio" name="inputto" id="inputto_wallet" value="wallet">wallet</label>
      		 	<span id="inputto_other"></span>
      		</p>
  			<p class="screen-100 tablet-40 phone-100"></p>
  			<p class="screen-50 tablet-40 phone-100">comment :<textarea name="comment"></textarea></p>
			<p class="screen-100 tablet-40 phone-100"></p>
			<p class="screen-100 tablet-40 phone-100">&nbsp;  
				<input type="hidden" name="admin_balance_broke" value="1">
				<input type="hidden" name="itypeaction" id="itypeaction" value="">
		  		<button class="prolific positive button" name="dosubmit_dtl" itype="deposit" type="button"><i class="big circle right icon" data-icon-name="circle right icon"></i>Deposit</button>  &nbsp;
            	<button class="prolific negative  button" name="dosubmit_dtl" itype="withdraw" type="button"><i class="big circle left icon" data-icon-name="circle left icon"></i> Withdrawal</button>
			</p>
      			 	  
			</fieldset>
          
          </div>
          <div class="field">
      		    <fieldset><legend><div class="prolific header">Opening Order(s):</div></legend>
      			<div id="shwodetaildata3"></div>
      			</fieldset>
      		</div>

      </div>
      </form>
      
</div>
</div

</div>
<?php }?>

    
</div>
    
 <!-- div start list tranaaaaction -->
 <?php if($_REQUEST['itype'] != '' && $_REQUEST['txtsearch'] != '') {?>
<div class="prolific double fitted divider"></div>
<div id="show_history_transfer">
<div>
<h1>Transaction List</h1>
</div>
<div id="msgdelete"></div>
	<table border="1" width="100%" class="display table table-bordered table-striped data-table">

<thead>
  <tr>
    <th width="5%"><div  align="center">-</div></th>
     <th width="35%"><div  align="center">Name</div></th>
      <th width="15%"><div  align="center">Operation</div></th>
    <th width="10%"><div  align="center">Amount(USD)</div></th>
    <th width="5%"><div  align="center">Status</div></th>
    <th width="15%"><div  align="center">Date</div></th>
  </tr>
</thead>

<tbody>
<?php
$condi = "";
switch($_REQUEST['itype']){
	case 'acclogin' : $condi = " AND (t.deposit_comment like '%".$_REQUEST['txtsearch']."%' OR trans_type like '%".$_REQUEST['txtsearch']."%') "; break;
	case 'pkid' : $condi = " AND t.user_id ='".$_REQUEST['txtsearch']."' "; break;
}
$sql = "SELECT 
			t.money,t.save_date,t.status,t.trans_id,t.comment,
			u.fname,u.lname
		 FROM  
		 	bk_wallet_transaction t,users u
		 WHERE
		    t.user_id = u.id
		 	
		 	 ".$condi." 
		 ORDER BY 
		 	t.trans_id DESC LIMIT 50";

//echo $sql;
$data = Registry::get("Database")->fetch_all($sql);

if ($data) {
	$i = 0;
	
	foreach ($data as $k => $v)
	{
		$save_date 	= strtotime('+12 hour', strtotime($v->save_date));
		$save_date 	= date('Y-m-d H:i:s',$save_date);
		if ($v->money > 0) {
			$typec = "Deposit";
			$money = "<font color='green'>".number_format($v->money,2)."</font>";
		} else {
			$typec = "Withdrawal";
			$money = "<font color='red'>".number_format(-$v->money,2)."</font>";
		}
		switch($v->status){
			case 1 : $txtsta = "<font color='green'>Approve</font>";break;
			case 0 : $txtsta = "<font color='blue'>Pending</font>";break;
			case 2 : $txtsta = "<font color='red'>Cancel</font>";break;
			case 3 : $txtsta = "<font color='red'>Not Otp</font>";break;
		}
		
?>
	
 	<tr class="gradeA">
		<td align="center">
		<a href="javascript:void(0);" class="mgrdelte" command= "<?php echo $typec;?>" money="<?php echo $money;?>" pkid="<?php echo $v->trans_id;?>">
		<i class="rounded danger inverted remove icon link"></i>
		</a>
	
		</td>
	   	
	   	<td align="center"><?php echo $v->fname." ".$v->lname;?></td>
	   	<td align="center"><?php echo $typec;?></td>
	   	<td align="right"><?php echo $money;?></td>
	   	<td align="center"><?php echo $txtsta;?></td>
	   	<td align="center"><?php echo $save_date;?></td>
  	</tr>
<?php 
	$i++;
}
}
?> 	
</tbody>
</table>
	
</div>
<?php }?>
<!-- div end list transaction -->   
  </div>
<script type="text/javascript"> 
// <![CDATA[
var SITEURL = "<?php echo SITEURL;?>";
var reloadpage = SITEURL+"/admin/index.php?"+"<?php echo str_replace("&amp;", "&", $_SERVER['QUERY_STRING']);?>";
$(document).ready(function () {
 $("#searchfieldall").on('keyup', function () {
        var srch_string = $(this).val();
        var data_string = 'userSearch_all=' + srch_string;
        if (srch_string.length > 4) {
            $.ajax({
                type: "post",
                url: "controller2.php",
                data: data_string,
                beforeSend: function () {

                },
                success: function (res) {
                    $('#suggestions').html(res).show();
                    $("input").blur(function () {
                        $('#suggestions').fadeOut();
                    });
                }
            });
        }
        return false;
    });
$('body').on('click', '.mgrdelte', function () {
			var pkid = $(this).attr('pkid');
       		var itype = $(this).attr('command');
       		var money = $(this).attr('money');
            var text = "<div class=\"messi-warning\"><i class=\"massive icon warn warning sign\"></i></p><p>Are you sure you want to delete this record?" + 
             "<br><strong>" + itype  + " : " +money+" USD</strong></p></div>";
            new Messi(text, {
                title: "Delete Transaction",
                modal: true,
                zIndex:700,
                closeButton: true,
                buttons: [{
                    id: 0,
                    label:'Delete Record',
                    class: 'negative',
                    val: 'Y'
                }],
                 callback: function (val) {
                        $.ajax({
                            type: 'post',
                            url: "controller2.php",
                            dataType: 'json',
                            data: {
                                id: pkid,
                                doDeleteTransaction: 1
                            },
                          
                            success: function (json) {
                               $('#msgdelete').html(json.message);
                               if (json.isdirect) {
									$('body').fadeOut(1000, function () {
										window.location.href = reloadpage;
									});
						        }
                            }

                        });
                    }
            });
       
			   //=== end madal
			});
$('.mgrdetail').click(function(){
	    $("#show_detail_data").addClass("loading");
	    var acc = $(this).attr('acc');
	    var mail = $(this).attr('email');
        var data_string = 'Getaccounttradedtl=1&acc=' + acc+'&email='+mail;
       
            $.ajax({
                type: "post",
                url: "controller2.php",
                data: data_string,
                dataType: 'json',
                beforeSend: function () {

                },
                success: function (obj) {
                	$("#show_detail_data").removeClass("loading");
                    if(obj.type == 'success'){
						$('#shwodetaildata1').html(obj.data1);
						$('#shwodetaildata2').html(obj.data2);
						$('#shwodetaildata3').html(obj.data3);
                    	$('#mgr_account_trade_login').html(acc);
                    	$('#dlt_pkid').val(acc);
                    	$('#dlt_email').val(mail);
                    	
                    	$('#inputto_other').html("<label for='inputto_trade'><input type='radio' checked name='inputto' id='inputto_trade' value='"+acc+"'>"+acc+"</label>");
                    } else {
                    	$('#shwodetaildata1').html('n/a');
						$('#shwodetaildata2').html('n/a');
						$('#shwodetaildata3').html('n/a');
                    	$('#mgr_account_trade_login').html('n/a');
                    	$('#dlt_pkid').val('');
                    	$('#dlt_email').val('');
                    }
                }
            });
});
if($('#setviewopen').get(0)) {
	$('.mgrdetail#setviewopen').trigger('click');
}else if($('.mgrdetail').get(0)){
	$('.mgrdetail:first').trigger('click');
}

$('body').on('click', 'button[name=dosubmit_dtl]', function () {
	var itype = $(this).attr('itype');
	
	$('#itypeaction').val(itype);
    function showResponse(json) {
        $(".prolific.form").removeClass("loading");
        $("#msgholder").html(json.message);
        
        if (json.isdirect) {
			$('body').fadeOut(1000, function () {
				window.location.href = reloadpage;
			});
        }
    }

    function showLoader() {
        $(".prolific.form").addClass("loading");
    }
    
    var text = "<div class=\"messi-warning\"><i class=\"massive icon warn warning sign\"></i></p><p>Are you sure you want to "+itype+" this record?" + 
             "<br><strong>" + itype  + " : " +$('#money_modify').val()+" USD To "+$('input[name=inputto]:checked').val()+"</strong></p></div>";
    new Messi(text, {
        title: "Delete Transaction",
        modal: true,
        zIndex:700,
        closeButton: true,
        buttons: [{
            id: 0,
            label: itype + ' Record',
            class: 'negative',
            val: 'Y'
        }],
         callback: function (val) {
		      var options = {
		        target: "#msgholder",
		        beforeSubmit: showLoader,
		        success: showResponse,
		        type: "post",
		        url: "controller2.php",
		        dataType: 'json'
		    };
		
		    $('#prolific_form_detail').ajaxForm(options).submit();
         }
    });
    
    
    
    
    
});

});



// ]]>
</script>