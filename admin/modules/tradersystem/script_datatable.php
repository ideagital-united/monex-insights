<link href="<?php echo ADMINURL;?>/modules/tradersystem/scritpt_datatable/demo_page.css" rel="stylesheet" type="text/css">
<link href="<?php echo ADMINURL;?>/modules/tradersystem/scritpt_datatable/demo_table_jui.css" rel="stylesheet" type="text/css">
<link href="<?php echo ADMINURL;?>/modules/tradersystem/scritpt_datatable/smoothness/jquery-ui-1.8.4.custom.css" rel="stylesheet" type="text/css">
<style type="text/css" title="currentStyle">
           
            body{
            background-color:white;
            }
            .setfontwhite{color:white;}
</style>
<script src="<?php echo ADMINURL;?>/modules/tradersystem/scritpt_datatable/jquery.dataTables.js"></script>  
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        oTable = $('.data-table').dataTable({
            "bJQueryUI": true,
             "bSort": false,
             "bLengthChange": false,
             "bRetrieve": true,
             "bDestroy": true,
            "sPaginationType": "full_numbers"
        });
    } );
</script>

<?php
/**
 * $aParam['title'] = array('title1','title2');
 * $aParam['sql'] = 'Registry::get("Database")->fetch_all(sql data);';
 * $aParam['listfield'] = array('datafield1','datafield1'); 
 */
function generate_datatable($aParam)
{
	if (isset($aParam['title'])) {
		print <<<EOF
<table border="1" class="display table table-bordered table-striped data-table">
	<thead>
	<tr>
EOF;
		foreach ($aParam['title'] as $k => $v)
		{
			print <<<EOF
<th><div  align="center">{$v}</div></th>
EOF;
		}
		print <<<EOF
</tr></thead><tbody>
EOF;
		//=== start body ===
		
		if (isset($aParam['data'])) {
			foreach ($aParam['data'] as $ksql => $vsql)
			{
				print <<<EOF
<tr class="gradeA">
EOF;
				foreach ($aParam['listfield'] as $kfield => $vfield)
				{
					print <<<EOF
<td>{$vsql->$vfield}</td>
EOF;
				}
				print <<<EOF
</tr>
EOF;
			}
		} else {
			print <<<EOF
Method SQL empty
EOF;
		}
			print <<<EOF
</tbody></table>
EOF;
		//=== end body ===
	} else {
		print <<<EOF
data empty
EOF;
	}
}
?>