<?php
if(!$user->getAcl("deposit")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

Registry::get("TraderSystem")->check_admin_prolific();

$act = (get('sta')) ? get('sta')  : 'approve'; 
?>
<div class="prolific icon heading message sky"><a class="helper prolific top right info corner label" data-help="menu"><i class="icon help"></i></a> <i class="reorder icon"></i>
  <div class="content">
    <div class="header"> Manage Member Personal Document [<?php echo $act;?>] </div>
    
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
     <div class="divider"> / </div>
     <?php if($act!='pending'){?>
     <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document-card&sta=pending" class="section">pending</a>
      <?php }else{echo "pending";}?>
      <div class="divider"> / </div>
      <?php if($act!='approve'){?>
      <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document-card" class="section">approve</a>
      <?php }else{echo "approve";}?>
     <div class="divider"> / </div>
     <?php if($act!='cancel'){?>
     <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document-card&sta=cancel" class="section">Cancel</a>
      <?php }else{echo "cancel";}?>
    </div>
  </div>
</div>

<script>
var var_action='documentcard_<?php echo $act;?>';
</script>

<?php
//=== end edit
//=== start view
/*echo BASEPATH."admin/modules/tradersystem/script_datatable_ajax.php";*/
include(MODPATH."tradersystem/script_datatable_ajax.php");
?>
<div>
<h1>Deposit Pending List</h1>
</div>
<div>
	<table border="1" class="display table table-bordered table-striped data-table">
<thead>
  <tr>
		<th align="center">Edit</th>
  		<th align="center">USERID<!--trans_id--></th>
		<th align="center">Customers<!--user_id--></th>
		<th align="center">Status</th>
		<th align="center">Document</th>
  </tr>

</thead>
</table>
</div>





