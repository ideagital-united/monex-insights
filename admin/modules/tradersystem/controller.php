<?php
  /**
   * Controller
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: controller.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
 
  require_once("../../init.php");
  require_once("admin_class.php");
  Registry::set('TraderSystem', new TraderSystem());
  if (!$user->is_Admin())
    redirect_to("../../login.php");
	
  $action = (isset($_POST['action']))  ? $_POST['action'] : null;
  
?>

<?php
switch($action){
	case 'deposit_by_admin'		: deposit_by_admin();break;
	case 'withdraw_by_admin'	: withdraw_by_admin();break;
	case 'document_by_admin' 	: document_by_admin();break;
	case 'doGetDocUserInfo' 	: doGetDocUserInfo();break;
	case 'getAccountTrade' 		: getAccountTrade();break;
	case 'accounttradepwd' 		: accounttradepwd();break;
	case 'accounttradedel' 		: accounttradedel();break;
}
//darawan
function accounttradedel()
{
	if (!isset($_POST['login']) || $_POST['login'] == '') {
		print json_encode(array('status'=>'warning','msg'=>Filter::msgError('NO DATA',false)));
	} else {
		$acc_login 	= $_POST['login'];
		$res 		= Registry::get("TraderSystem")->call_curl("admin_broke_delete_trade","POST",array('cmd'=>'admin_broke_delete_trade','login'=>$acc_login));
		if (isset($res['isSuccess']) && $res['isSuccess'] == 1) {
			$aa =  Registry::get("Database")->delete("bk_trader_acc", "trader_acc_login='" . $acc_login."'");
			print json_encode(array('status'=>'success','msg'=>Filter::msgOk('Complete',false)));
		} else {
			print json_encode(array('status'=>'warning','msg'=>Filter::msgError($res['msg'],false)));
		}
		
	}
}
function accounttradepwd()
{
	if (!isset($_POST['login']) || $_POST['login'] == '' ) {
		print json_encode(array('status'=>'warning','msg'=>'no data.'));
		exit;
	}	
	
	$aRes = array();
	$aRes['isGroup'] = 0;
	$aRes['isPwd'] = 0;
	if (isset($_POST['setgroup']) && $_POST['setgroup'] !='') {
		$aData['book_a'] = ($_POST['setgroup'] =='A')?1:0;
		Registry::get("Database")->update("bk_trader_acc",$aData, "trader_acc_login='" . $_POST['login']."'");
		if (Registry::get("Database")->affected()) {
			$aRes['isGroup'] = 1;
			$aRes['msg_group'] = 'UPDATE GROUP SUCCESS';
		} else {
			$aRes['isGroup'] = 2;
			$aRes['msg_group']='UPDATE GROUP NOT CHANGE';
		}
	}
    if ($_POST['pwd'] == '' && $_POST['pwd2'] == '') {
    	
    } else {
		if (!isset($_POST['pwd']) || $_POST['pwd'] == '' || !isset($_POST['pwd2']) || $_POST['pwd2'] == '' || $_POST['pwd'] != $_POST['pwd2']) {
			$aRes['isPwd'] = 2;
			$aRes['msg_pwd'] = 'Your input password incorrect.';
			
		} else {
			$acc_login 	= $_POST['login'];
			$res 		= Registry::get("TraderSystem")->call_curl("admin_broke_change_pass","POST",array('cmd'=>'admin_broke_change_pass','login'=>$acc_login,'password'=>$_POST['pwd']));
			if (isset($res['isSuccess']) && $res['isSuccess'] == 1) {
				$aRes['isPwd'] = 1;
				$aRes['msg_pwd'] = 'Your Change password completed.';
				print json_encode(array('status'=>'success','msg'=>Filter::msgOk($res['msg'],false)));
			} else {
				$aRes['isPwd'] = 2;
				$aRes['msg_pwd'] = $res['msg'];
				print json_encode(array('status'=>'warning','msg'=>Filter::msgError($res['msg'],false)));
			}
			
		}
	}
	if ($aRes['isGroup'] == 0 && $aRes['isPwd'] ==0) {
		print json_encode(array('status'=>'warning','msg'=>Filter::msgError('NO DATA',false)));
	} else {
		$txt = (isset($aRes['msg_group'])) ? $aRes['msg_group']."<br>" : '';
		$txt .= (isset($aRes['msg_pwd'])) ? $aRes['msg_pwd'] : '';
		if ($aRes['isGroup'] == 2 || $aRes['isPwd'] == 2) {
			print json_encode(array('status'=>'warning','msg'=>Filter::msgError($txt,false)));
		} else {
			print json_encode(array('status'=>'success','msg'=>Filter::msgOk($txt,false)));
		}
	}
}

function getAccountTrade()
{
	if (!isset($_POST['login']) || $_POST['login'] == '' || !isset($_POST['email']) || $_POST['email'] == '') {
		print json_encode(array('status'=>'warning','msg'=>'no data.'));
	} else {
		$acc_login 	= $_POST['login'];
		$emaiuser 	= $_POST['email'];
		$res 		= Registry::get("TraderSystem")->call_curl_get("admin_broke_get_account_detail/".$acc_login."/".$emaiuser."?cmd=admin_broke_get_account_detail","GET");
		$json['msg'] = '';
		
		$json['status'] = 'success';
		if (isset($res['isSuccess']) && $res['isSuccess'] == 1) {
			$aData = $res['msg'];
			$json['msg'] = "<label>LOGIN : " . $aData['LOGIN'] . "</label><br />";
			$json['msg'] .= "<label>GROUP : " . $aData['GROUP'] . "</label><br />";
			$json['msg'] .= "<label>NAME : " . $aData['NAME'] . "</label><br />";
			$json['msg'] .= "<label>LEVERAGE : " . $aData['LEVERAGE'] . "</label><br />";
			$json['msg'] .= "<label>EQUITY : " . $aData['EQUITY'] . "</label><br />";
			$json['msg'] .= "<label>BALANCE : " . $aData['BALANCE'] . "</label><br />";
		}
		$resOrder 	= $res['order'];
		if(isset($resOrder['isSuccess']) && $resOrder['isSuccess'] == 1 && count($resOrder['msg']) > 0){
			foreach($resOrder['msg'] as $k=>$v){
				$orderl = isset($v['order']) ? $v['order'] : $v['Ticket'];
				$json['msg'] .= "<label>ORDER : ".$orderl."</label><br />";
			}
		}
		print json_encode($json);
	}
}

function doGetDocUserInfo()
{
	if($_POST['itype'] == 'card' || $_POST['itype'] == 'addr'){
	    $aRes = Registry::get("TraderSystem")->get_user_info_by_mail_bak();
	}else{
	$aRes = Registry::get("TraderSystem")->get_user_info_by_mail();
	    
	}
	print json_encode($aRes);
}

function document_by_admin()
{
	if($_POST['itype'] == 'card' || $_POST['itype'] == 'addr'){
		$aRes = Registry::get("TraderSystem")->update_status_document();
	}else{
		$aRes = Registry::get("TraderSystem")->update_status_document_new();
	}
	print json_encode($aRes);
}
function withdraw_by_admin()
{
	$aRes = Registry::get("TraderSystem")->update_transaction_withdraw();
	print json_encode($aRes);
}

function deposit_by_admin()
{
	$aRes = Registry::get("TraderSystem")->update_transaction_deposit();
	print json_encode($aRes);
}

?>