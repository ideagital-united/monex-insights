<?php


if (isset($_POST['but_save']) && ($_POST['but_save'] != '')) {

    //print_r($_POST);

    $fq = array();

    $fq['usd_per_lot'] = $_POST['usd_per_lot'];

    $add_level = Registry::get("TraderSystemCommission")->addLevel($fq);

    //print_r($add_level);

    if($add_level['result'] == "success") {

        ?>

        <script type="text/javascript">

            location.href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration';

        </script>

        <?

        exit;

    } else {

        

    }

} else if (isset($_POST['but_delete']) && ($_POST['but_delete'] != '')) {

    //print_r($_POST);

    if(isset($_POST['last_level_id']) && ($_POST['last_level_id'] != '')) {

        $fq = array();

       $fq['last_level_id'] = $_POST['last_level_id'];

       $del_lev = Registry::get("TraderSystemCommission")->deleteLevel($fq);

       //print_r($del_lev);

       if($del_lev['result'] == "success") {

        ?>

        <script type="text/javascript">

            location.href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration';

        </script>

        <?

        exit;

        } else {

            

        }

    }

} else if (isset($_POST['but_edit']) && ($_POST['but_edit'] != '')) {

    //print_r($_POST);

    $fq = array();

    $fq = $_POST['edit_level_id'];

    //print_r($fq);

    

    $fk = array();

    $fk = $_POST['usd_per_lot'];

   // print_r($fk);

    

    $up_level_list = Registry::get("TraderSystemCommission")->updateLevel($fq,$fk);

    

    if($up_level_list['result'] == "success") {

        ?>

        <script type="text/javascript">

            location.href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration';

        </script>

        <?

        exit;

    } else {

            

     }

    

}







$level_list1 = Registry::get("TraderSystemCommission")->getLevelList();
$level_list = $level_list1['message'];


//$comm_setting = Registry::get("TraderSystemCommission")->getCommSetting();





?>





    

 <div class="prolific icon heading message green"> <i class="file icon"></i>

  <div class="content">

    <div class="header"> Manage Commission Level </div>

    <div class="prolific breadcrumb"><i class="icon home"></i> <a class="section" href="index.php">Dashboard</a>

      <div class="divider"> / </div>

      <div class="active section">Commission Level Configuration</div>

    </div>

  </div>

</div>





    

<div class="prolific-large-content">

    

    

    

    <div class="prolific message">

        <!-- <div class="prolific bottom right attached special label">EN</div> -->

        Here you can manage commission level.

    </div>

    
    <div class="prolific form segment">

        <div class="prolific header">Add Commission Level</div>





        <div class="prolific double fitted divider"></div>



            <form action="" method="POST" name="prolific_form">

                

                <div class="two fields">

                    <div class="field">

                        <label>Commission (USD/Lot)</label>

                        <label class="input">

                            <i class="icon-append icon asterisk"></i>

                            <input type="text" name="usd_per_lot" placeholder="0.00">

                        </label>

                    </div>

                    

                    <div class="prolific double fitted divider"></div>



   

                   <input type="submit" name="but_save" value="Add"  class="prolific positive button" />

                </div>

                   

            </form>



</div>







   <div class="prolific form segment">      







        <div class="prolific header">Commission Level</div>

        

        <div class="prolific fitted divider"></div>





<form action="" method="POST" name="list_form">

    

    <table class="prolific sortable table">

        <thead>

            <tr>

                <th data-sort="int">Level</th>

                <th class="disabled">Commission (USD/Lot)</th>

                <th class="disabled">Percent</th>

                <th class="disabled">Actions</th>

            </tr>

        </thead>

        <tbody>



    <?php



   // echo "<pre>LEVEL_LIST=";

   // print_r($level_list);

   // echo "</pre>";

    $cnt_list = count($level_list);

    for ($ii=0;$ii<$cnt_list;$ii++) {

        if (($cnt_list-$ii)==1) {

           if ($level_list[$ii]['level_no'] > 0) {
           	 $delete = "<input type='submit' name='but_delete' value='Delete'>

            <!--    <a class='delete' data-name='Our Contact Info' data-id='".$level_list[$ii]['level_id']."' 

                data-option='deleteLevel' data-title='Delete Commission Level'>

                    <i class='rounded danger inverted remove icon link'></i>

                </a> -->";
		   }

       } else {

           $delete = "&nbsp;";

       }

        ?>

        <tr>

            <td><input type='hidden' name='last_level_id' value='<?php echo $level_list[$ii]['level_id']; ?>'>

                <input type='hidden' name='edit_level_id[]' value='<?php echo $level_list[$ii]['level_id']; ?>'>

                <?php echo $level_list[$ii]['level_no']; ?>

            </td>

            <td><label class="input"><input type='text' name="usd_per_lot[]" value='<?php echo $level_list[$ii]['usd_per_lot']; ?>'></label></td>

            <td><?php echo number_format((($level_list[$ii]['usd_per_lot']/$comm_setting['profit_per_lot'])*100),2); ?> %</td>

            <td><?php echo $delete; ?></td>

        </tr>

        <?php

        $sum_usd = $sum_usd + $level_list[$ii]['usd_per_lot'];

    }

    ?>

   

    </tbody>

</table>



 <div class="prolific fitted divider"></div>

 

 <div id="msgholder"></div>



<input type="submit" name="but_edit" value="Update Commission Level"  class="prolific positive button" />



</form>



 <div class="prolific fitted divider"></div>

 

 

 

 <div class="prolific-grid">

    <div class="column horizontal-gutters">

      <div class="row"> <span class="prolific label">Total commission: <?php echo number_format($comm_setting['profit_per_lot'],2); ?> USD/Lot</span> </div>

      <div class="row"> <span class="prolific label">Total paid: <?php echo number_format($sum_usd,2); ?> USD/Lot</span> </div>

      <div class="row"> <span class="prolific label">Total commission remaining: <?php echo number_format($comm_setting['profit_per_lot']-$sum_usd,2); ?> USD/Lot</span> </div>

    </div>

  </div>

 


</div> <!-- end < div class="prolific segment" > -->





</div> <!-- end < div class="prolific-large-content" > -->





