<?php
    if (isset($_POST['but_update_freq']) && ($_POST['but_update_freq'] != '')) {

    $fq = array();
        
   //  echo "post=<pre>";   print_r($_POST); echo "</pre>"; 
 
    $fq['paid_status'] = $_POST['paid_status'];
    $fq['profit_per_lot'] = $_POST['profit_per_lot'];
    $fq['ct_id'] = $_POST['ct_id'];   

    $up_freq = Registry::get("TraderSystemCommission")->updateCommSetting($fq);
    
        if($up_freq['result'] == "success") {
            
         /* 
          update head office comm to  $fq['profit_per_lot']
         */
         
         if ($fq['ct_id'] == 2) {
                 
             $up_head = Registry::get("TraderSystemCommission")->upHeadOfficeComm($_POST['profit_per_lot']);
        
             if ($up_head != '') {
                 
                 Registry::get("TraderSystemCommission")->upHeadOfficeCommCMS($_POST['profit_per_lot']);
                 
             }
         
         }
             
            ?>    
            <script type="text/javascript">    
                location.href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-rule-configuration';    
            </script>    
            <?    
            exit;    
            }   
         }
     
     
     if (isset($_POST['but_update_type']) && ($_POST['but_update_type'] != '')) {
                   $fq = array();  
                 $fq['ct_id'] = $_POST['ct_id'];   
                   $up_ct = Registry::get("TraderSystemCommission")->updateCommSetting($fq);    
                 if($up_ct['result'] == "success") {
                                     
                     
              ?>
    
            <script type="text/javascript">
    
               location.href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-rule-configuration';
    
            </script>
    
            <?
    
            exit;    
               }
             }

$comm_setting1 = Registry::get("TraderSystemCommission")->getCommSetting();
$comm_setting = $comm_setting1['message'][0];

// print_r($comm_setting);

if($comm_setting['paid_freq'] == 'never') {
    $sel0 = " selected='selected' ";
    $sel1 = "";
    $sel2 = "";
} else if ($comm_setting['paid_freq'] == 'monthly') {
    $sel0 = "";
    $sel1 = " selected='selected' ";
    $sel2 = "";
} else if ($comm_setting['paid_freq'] == 'daily') {
    $sel0 = "";
    $sel1 = "";
    $sel2 = " selected='selected' ";
}

if($comm_setting['paid_status'] == 'stop') {
    $sels0 = " selected='selected' ";
    $sels1 = "";
} else if ($comm_setting['paid_status'] == 'start') {
    $sels0 = "";
    $sels1 = " selected='selected' ";
} 

if ($comm_setting['ct_id'] == 0) {
     $ct_sel0 = " selected ";    
    $ct_sel = "";
} else {
    $ct_sel0 = " ";
}
?>

 <div class="prolific icon heading message green"> <i class="file icon"></i>
  <div class="content">
    <div class="header"> Manage Commission Rule </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a class="section" href="index.php">Dashboard</a>
      <div class="divider"> / </div>
      <div class="active section">Commission Rule Configuration</div>
    </div>
  </div>
</div>

<div class="prolific-large-content">
  
    <div class="prolific message">
      
        Here you can manage commission rule.
    </div>
    
   
            <?php // if ($comm_setting['ct_id'] > 0) { ?>
    
    
    
     <div class="prolific form segment">    
   
        <div class="prolific header">Commission payment</div>

        <div class="prolific fitted divider"></div>

            <form action="" method="POST" name="prolific_form">
                
                <div class="two fields">                        
                 <div class="field">      
                 <label>Commission type <!-- (Now type id is <? echo $comm_setting['ct_id']; ?>) --></label>   
                 
                       <label class="input">               
                                          
                     <i class="icon-append icon asterisk"></i>  
                        <?php $comm_ct1 = Registry::get("TraderSystemCommission")->getCommTypeList(1);   
                      
                        
                        $comm_ct = $comm_ct1['message'];
                         
                        ?>
                         <select type="select" name="ct_id">  
                             <!-- <option value="0" <?php echo $ct_sel0; ?>>0. Not set commission type</option>   -->  
                             <?php  for ($ct=0;$ct<count($comm_ct);$ct++) {
                                 $ct_sel = "";                                       
                                  if ($comm_ct[$ct]['ct_id'] == $comm_setting['ct_id']) {
                                        $ct_sel = " selected ";   
                                 } 
                         ?>
                          <option value="<?php echo $comm_ct[$ct]['ct_id']; ?>" <?php echo $ct_sel; ?>><?php echo $comm_ct[$ct]['ct_id']; ?>. <?php echo $comm_ct[$ct]['ct_name']; ?></option>
                          <?php } ?>
                           </select>
                           
                               </label>   
         
                          </div>    
                                                                     
                </div>   
                
                <div class="two fields">
                    <!--
                    <div class="field">
                        <label>Commission paid</label>
                        <label class="input">
                            <i class="icon-append icon asterisk"></i>
                            <select type="select" name="paid_freq">
                                <option value="never" <?php echo $sel0; ?>>Never</option>
                                <option value="monthly" <?php echo $sel1; ?>>Monthly</option>
                                <option value="daily" <?php echo $sel2; ?>>Daily</option>
                            </select>
                            
                           
                        </label>
                    </div>
                    -->
                    <div class="field">
                        <label>Commission Payout to IBs<!-- (Now status is <? echo $comm_setting['paid_status']; ?>) --></label>
                        <label class="input">
                            <i class="icon-append icon asterisk"></i>
                            <select type="select" name="paid_status">
                                <option value="stop" <?php echo $sels0; ?>>Disable</option>
                                <option value="start" <?php echo $sels1; ?>>Enable</option>
                            </select>
                            
                           
                        </label>
                    </div>
                    
                   
                    <div class="field">
                        <label>Commission profit per lot (USD/Lot)</label>
                        <label class="input">
                            <i class="icon-append icon asterisk"></i>
                            <input type="text" name="profit_per_lot" value="<?php echo number_format($comm_setting['profit_per_lot'],2); ?>">                            
                        </label>
                    </div>
               
                    
                    
                    <div class="prolific fitted divider"></div>

   
                   <input type="submit" name="but_update_freq" value="Save"  class="prolific positive button" />
                </div>
                   
            </form>

</div>
<?php // } ?>


</div> <!-- end < div class="prolific-large-content" > -->


