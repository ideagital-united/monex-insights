<?php
  /**
   * Configuration
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: config.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
if(!$user->getAcl("deposit")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

Registry::get("TraderSystem")->check_admin_prolific();

?>

<div class="prolific icon heading message sky"><a class="helper prolific top right info corner label" data-help="menu"><i class="icon help"></i></a> <i class="reorder icon"></i>
  <div class="content">
    <div class="header"> Manage Member Address Document</div>
    
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
     <div class="divider"> / </div>

     <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document-card&sta=pending" class="section">pending</a>
    
      <div class="divider"> / </div>
    
      <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document-card" class="section">approve</a>
     
     <div class="divider"> / </div>
    
     <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document-card&sta=cancel" class="section">Cancel</a>
      
    </div>
  </div>
</div>
<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_CG_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="prolific form segment">
    <div class="prolific header">MANAGE ADDRESS DOCUMENT</div>
    <div class="prolific double fitted divider"></div>
<?php
function genHtmlfield($title,$setval,$input_type='text',$input_name='name_beta')
{
	?>
	<div class="field">
		<label><?php echo $title;?></label>
		<label class="input">
			<?php if($input_type=='text') {?>
			<input type="text" value="<?php echo $setval;?>" name="<?php echo $input_name;?>" disabled="disabled">
			<?php } elseif($input_type=='textarea'){ ?>
				<textarea cols='35' rows='3' name="<?php echo $input_name;?>"><?php echo $setval;?></textarea>
			<?php } ?>
		</label>
	</div>
        
	<?php
}
$uid = $_GET['id'];
$aData = Registry::get("TraderSystem")->getdata("*","users","where id='".$uid."'");
$aDataApi = Registry::get("TraderSystem")->call_curl_get("admin_user_info/".$aData['email']."?cmd=user_info","POST","");

if ($aDataApi['isSuccess'] == 1 ) {
	$aUser = $aDataApi['message'];
	$fupdate = array();
	if ($aData['address_verified'] != $aUser['address_verified'] ) {
		$fupdate['address_verified'] = $aUser['address_verified'] ;
		$fupdate['address_file_url'] = $aUser['address_file_url	'] ;
		Registry::get("Database")->update("users", $fupdate, "email='" . $aData['email']."'");
	}
	$link_img = '';
	
	if (is_file(UPLOADS . 'file_doc/' . $aUser['id_card_file_url'])) { 
		$link_img = '<a href="' . UPLOADURL . 'file_doc/' . $aUser['address_file_url'] . '" target="_blank" width="200">'.
					'<img src="'.UPLOADURL.'file_doc/'.$aUser['address_file_url'].'" width="200"></a>';
	}
	
	?>

	<div class="two fields">
		<?php genHtmlfield('Name',$aData['fname'].' '.$aData['lname']);?>
        <?php genHtmlfield('Address Data',$aUser['address_1']);?>
	</div>
	<div class="fields">
		   <div class="field">
		   
          <label>Document File</label>
          <?php echo $link_img;?>
        </div>
	</div>
	
	<div class="showsmtp">
        <div class="prolific thin attached divider"></div>
      </div>
    <?php if ($aUser['id_card_verified'] == 0) {?>
    <form id="prolific_form" name="prolific_form" method="post">
	<input type="hidden" name="action" value="document_by_admin">
	<input type="hidden" name="itype" value="address_verified">
	<input type="hidden" name="email" value="<?php echo $aData['email'];?>">
    <div class="fields">
		   <div class="field">
		   
          <label>Change Status</label>
          <select name="change_status">
			<option value='0'>Pending</option>
			<option value='1'>Approve</option>
			<option value='2'>Cancel</option>
          </select>
        </div>
	</div>
	<div class="prolific double fitted divider"></div>
      <button type="button" name="dosubmit" class="prolific positive button">Update</button>
      </form>
    <?php }else{?>
    	<div class="fields">
		   <div class="field">
		   
          <label>Status</label>
          <?php
          switch($aUser['id_card_verified']){
		  	case 2 : echo "<font color='red'>Cancel</font>";break;
			case 1 : echo "<font color='green'>Approve</font>";break;
			default : echo "-";break;
          }
          ?>
        </div>
		</div>
    <?php }?>
	
	
	<?php
} else {
     Filter::msgSingleAlert("NO DATA");

}
?>   

 </div>     
  <div id="msgholder"></div>
</div>

