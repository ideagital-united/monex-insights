<?php

function get_downline_list($email, $level) 
{
    if(!isset($level))
    {
        $level = 1;
    }
    
    $m_list = "";
    
    $num11 = 0;
 
    $u_list1 = Registry::get("TraderSystemCommission")->getUserByRefEmail($email);
    
    //echo "<pre>"; print_r($u_list1);
                   
    $num11 = count($u_list1);
                           
    if ($num11 > 0 ) {
        
        if ($u_list1[0]->id != '') {
        
           $m_list = "<ul>";
        
        
          for ($ii1=0;$ii1<$num11;$ii1++) {
                        
                  $uu_list1 = $u_list1[$ii1];
              
                  $is_ib = "<font color='#FF0080'><b>Trader</b></font>";
                  if ($uu_list1->can_refer_status == 3) {
                      $is_ib = "<font color='#0000FF'><b>Head office</b></font>";
                  } else if ($uu_list1->can_refer_status == 2) {
                      $is_ib = "<font color='#33CC00'><b>Center</b></font>";
                  } else if ($uu_list1->can_refer_status == 1) {
                      $is_ib = "<font color='#FF0000'><b>IB</b></font>";
                  }
                    
                $m_list .= "<li><span>
                          [ Level:" . $level . " ]
                          [ ".$uu_list1->id." ] 
                          [ ".$uu_list1->email." ] 
                          [ ".$uu_list1->fname." ".$uu_list1->lname." ] 
                          [ ".$is_ib." ] 
                          [ ".$uu_list1->commission." USD/Lot ] 
                          [ <a href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration&uid=".$uu_list1->id."'>Edit</a> ] 
                          ";
                          
                          // [ <a href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration&uid=".$uu_list1->id."'>Edit</a> ] 

              $m_list .= "</span>";

                // get ref list
                $next_level = $level + 1;
                
              if ($uu_list1->id > 0) {
                        $m_list = $m_list . get_downline_list($uu_list1->email, $next_level);
              }
                $m_list .= "</li>";
          } // end for ($ii=0;$ii<$num1;$ii++)
           $m_list .= "</ul>";
        } // end if ($u_list1[0]->id != '') 

    } // end if ($num1 > 0 ) 
    return $m_list;
}




$uid = $_GET['uid'];

$error = "";

if(isset($_POST['but_update_ref']) && ($_POST['but_update_ref'] != '')) {
    
    //print_r($_POST);
    
    $p_uid = $_POST['uid']; // => 1 
    
     $commission = $_POST['commission']; // => 100 
     
     $can_refer_status = $_POST['can_refer_status']; // => 1 
     
    // $comm_percent_per_comm_ref = $_POST['comm_percent_per_comm_ref']; // => 0.00 
    
     $change_ref_id = $_POST['change_ref_id']; // =>
     
    // if (($change_ref_id >= $p_uid) && ($change_ref_id > 0)) {
         
      /*    $error = '<div class="prolific icon message error">
                    <i class="flag icon"></i>
                    <i class="close icon"></i>
                    <div class="content">
                    <div class="header"> Error </div>';

         
          $error .= "<p>You cannot assign ".$change_ref_id." to be refer ".$p_uid."</p>";
         
          $error .= '</div>
                      </div>'; 
       */
         
   //  } else {
         
         $refData = Registry::get("TraderSystemCommission")->getUserByID($change_ref_id);
         
        $memData = Registry::get("TraderSystemCommission")->getUserByID($p_uid);
        
       // print_r($memData);
         
         $rDat = $refData[0];
         
         if ($rDat->commission <= $commission) {
             
              $error = '
                    <div class="prolific icon message error">
                    <i class="flag icon"></i>
                    <i class="close icon"></i>
                    <div class="content">
                    <div class="header"> Error </div>';
             
            $error .=  "You cannot assign higher or equal commission with referral";
            
            $error .= '</div>
                        </div>
                        ';
             
         } else {
             
             $fq['can_refer_status'] = $can_refer_status;
            $fq['commission'] = $commission;
            $fq['id'] = $p_uid;
            $fq['email'] = $memData[0]->email;
            
            if ($change_ref_id != '') {
                $fq['cms_ref_id'] = $change_ref_id;
            }
            
            
            /* center under ib or center under center is invalid */
             if (($can_refer_status == 2) && ($rDat->can_refer_status <= 2)) {
                  
                  $error = '
                    <div class="prolific icon message error">
                    <i class="flag icon"></i>
                    <i class="close icon"></i>
                    <div class="content">
                    <div class="header"> Error </div>';
             
                $error .=  "You cannot assign Center under IB or Center under Center";
                
                $error .= '</div>
                            </div>
                            ';
             } else {
            
            $res = Registry::get("TraderSystemCommission")->updateAccountCommission($fq);
            
            echo "res=".$res;
            
            
            if ($res == 1) {
                 $up_cms = Registry::get("TraderSystemCommission")->updateAccountCommissionCMS($fq);
            }
           
                  
                   $error = '
                    <div class="prolific icon message success">
                    <i class="flag icon"></i>
                    <i class="close icon"></i>
                    <div class="content">
                    <div class="header"> Success </div>';
             
                    $error .=  "Setting commission is completed. ";
                    
                    $error .= "<a href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration'>Back to Find Member</a>";
                    
                    $error .= '</div>
                                </div>
                                ';
              
              }
         }
         
     //}
    
}



$comm_setting = Registry::get("TraderSystemCommission")->getCommSetting();


?>

<link rel="stylesheet" href="modules/tradersystem/member_tree/jquery.treeview.css" />
<!--
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
-->
<script src="modules/tradersystem/member_tree/lib/jquery.cookie.js" type="text/javascript"></script>
<script src="modules/tradersystem/member_tree/jquery.treeview.js" type="text/javascript"></script>

<script type="text/javascript">
        $(function() {
            $("#tree").treeview({
                collapsed: true,
                animated: "medium",
                control:"#sidetreecontrol",
                persist: "location"
            });
        })
</script>

<script>
function showDetails(id,not_uid)
{
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
        document.getElementById("change_ref_name").value=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","/modules/tradersystem/find_user.php?uid="+id+"&not_uid="+not_uid,true);
xmlhttp.send();
}
</script>



 <div class="prolific icon heading message green"> <i class="file icon"></i>

  <div class="content">

    <div class="header"> Manage Commission Level </div>

    <div class="prolific breadcrumb"><i class="icon home"></i> <a class="section" href="index.php">Dashboard</a>

      <div class="divider"> / </div>

      <div class="active section">Commission Level Configuration</div>

    </div>

  </div>

</div>


<div class="prolific-large-content">

   
    <div class="prolific message">

        <!-- <div class="prolific bottom right attached special label">EN</div> -->

        Here you can manage commission level.

    </div>

        
        <?php if ($uid != '') {
            
            $mData = Registry::get("TraderSystemCommission")->getUserByID($uid);
         //  if($mData==0){
			//$fin['uid']=$uid;
			//$ress=Registry::get("TraderSystem")->i_insert('acms_account_dtl', $fin);
			//$mData = Registry::get("TraderSystem")->getUserByID($uid);
		  // }
           $mDat = $mData[0];
           
           if ($mDat->can_refer_status == 3) {
               $chk0 = "  ";
             $chk1 = "  ";
             $chk2 = "  ";
             $chk3 = " checked ";
           } else if ($mDat->can_refer_status == 2) {
               $chk0 = "  ";
             $chk1 = "  ";
             $chk2 = " checked ";
             $chk3 = "  ";
           } else if ($mDat->can_refer_status == 1) {
               $chk0 = "  ";
             $chk1 = " checked ";
             $chk2 = "  ";
             $chk3 = "  ";
           } else {
               $chk0 = " checked ";
             $chk1 = "  ";
             $chk2 = "  ";
             $chk3 = "  ";
           }
           
           $mData_ref = Registry::get("TraderSystemCommission")->getUserByID($mDat->cms_ref_id);
            
          $mDat_ref =  $mData_ref[0];
            
            
         
            
        ?>
        
        <?php if ($error != '') { ?>
                <div id="msgholder"><?php echo $error; ?></div>
        <?php } ?>
        
            
        <div class="prolific form segment">
              
                 
        <div class="prolific header">Set Commission</div>
        
        
        <form name="edit_ref_form" action="" method="POST">
            
            
        <div class="prolific fitted divider"></div>
        
         <!-- User ID / Name / Email <input type="text" style="background-color: #FFFF80" readonly="readonly" value="<?php echo $mDat->id; ?> / <?php echo $mDat->username; ?> / <?php echo $mDat->fname; ?> <?php echo $mDat->lname; ?> / <?php echo $mDat->email; ?>"> -->
         
         User ID / Name / Email <input type="text" style="background-color: #FFFF80" readonly="readonly" value="<?php echo $mDat->id; ?> / <?php echo $mDat->fname; ?> <?php echo $mDat->lname; ?> / <?php echo $mDat->email; ?>">
        
        <input type="hidden" name="uid" value="<?php echo $uid; ?>">
        
        <br />
        
        Commission (USD/Lot) <input type="text" name="commission" value="<?php echo $mDat->commission; ?>">
        
        <br />
        
        Referrer Level 
        
        <br />
        
        <?php if ($mDat->can_refer_status == 3) { ?>
        
        <input type="radio" name="can_refer_status" id="can3" value="3" <?php echo $chk3; ?>> <label for="can3">Head office</label>
        
        <?php } else {  ?>
        
        <input type="radio" name="can_refer_status" id="can2" value="2" <?php echo $chk2; ?>> <label for="can2">Center</label>
        
        <input type="radio" name="can_refer_status" id="can1" value="1" <?php echo $chk1; ?>> <label for="can1">IB</label>
        
        <input type="radio" name="can_refer_status" id="can0" value="0" <?php echo $chk0; ?>> <label for="can0">Trader</label>
        
        <?php } ?>
        <br /> <br />
           
        Referrer ID (Change it if referrer id is wrong) <input type="text" name="change_ref_id" value="<?php if($mDat->cms_ref_id != 0) { echo $mDat->cms_ref_id; } else { echo "0"; } ?>" onchange="showDetails(this.value,<?php echo $uid; ?>)">

        <br />
        
        <?php 
        if ($mDat_ref->can_refer_status == 3) {
            $ref_s = "Head Office";
        } else if ($mDat_ref->can_refer_status == 2) {
            $ref_s = "Center";
        } else if ($mDat_ref->can_refer_status == 1) {
            $ref_s = "IB";
        } else if ($mDat_ref->can_refer_status == 0) {
            $ref_s = "Trader";
        } else {
            $ref_s = "";
        }
?>
        
        <!-- Referrer detail (UserID / Name / Email / Referral Level / Commission (USD/Lot)) <input type="text" value="<?php if($mDat->ref_id != 0) { echo $mDat->ref_id." / ".$mDat_ref->username ." / ".$mDat_ref->fname." ".$mDat_ref->lname." / " . $mDat_ref->email . " / " . $ref_s . " / " . $mDat_ref->comm_percent_per_comm; } else { echo "0 (No referrer)"; } ?>" id="change_ref_name" style="background-color: #FFFF80" > -->
            
            Referrer detail (UserID / Name / Email / Referral Level / Commission (USD/Lot)) 
            <input type="text" value="<?php if($mDat->cms_ref_id != 0) { echo $mDat->cms_ref_id." / ".$mDat_ref->fname." ".$mDat_ref->lname." / " . $mDat_ref->email . " / " . $ref_s . " / " . $mDat_ref->commission; } else { echo "0 (No referrer)"; } ?>" id="change_ref_name" style="background-color: #FFFF80" >
        

        <br />
        

        <div class="prolific fitted divider"></div>
        
       
        <input type="submit" name="but_update_ref" value="Save"  class="prolific positive button" /> <a href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration'>Back to Find Member</a>

        </form>
        
        </div>
        
        <?php } else {  ?>
        
       
        
        
       <div class="prolific segment">
             

        <div class="prolific header">Find Member (Tree view)</div>


        <div class="prolific fitted divider"></div>
        
        
        
        <div id="sidetree">
          
       
          
            <div id="sidetreecontrol"><a href="#">Collapse All</a> | <a href="#">Expand All</a></div>
            
            <ul id="tree">
                
                <li><strong>[ Level ] [ ID ] [ Username ] [ Name ] [ Status ] [ Commission(USD/Lot) ]</strong></li>
                
                <?php 
                   
                 
                 $u_list = Registry::get("TraderSystemCommission")->getUserHeadOffice();
                 
              //   echo "<pre>"; print_r($u_list);echo "</pre>";
                   
                $u_cnt = count($u_list['message']);
                
               // echo "u_cnt=".$u_cnt;
                
                for ($ii=0;$ii<$u_cnt;$ii++) {
                    
                        
                    $uu_list = $u_list['message'][$ii];
                    
                    
                     if ($uu_list['can_refer_status'] == 3) {
                             $is_ib_status = "<font color='#0000FF'><b>Head office</b></font>";
                     } else if ($uu_list['can_refer_status'] == 2) {
                             $is_ib_status = "<font color='#33CC00'><b>Center</b></font>";
                     } else if ($uu_list['can_refer_status'] == 1) {
                             $is_ib_status = "<font color='#FF0000'><b>IB</b></font>";
                     } else {
                             $is_ib_status = "-";
                     }
                   
                ?>

                <li><span>[ Level:0 ] 
                      [ <?php echo $uu_list['id']; ?> ] 
                                    [ <?php echo $uu_list['email']; ?> ]
                                    [ <?php echo $uu_list['name']; ?> ]
                                    [ <?php echo $is_ib_status; ?> ]
                                    [ <?php echo $uu_list['commission']; ?> USD/Lot ]
                                 <!--   [ <a href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration&uid=<?php echo $uu_list->id; ?>'>Edit</a> ] -->
                                    
                    </span>
                    <?php
                        $member_tree_list = get_downline_list($uu_list['email'], 1);
                      echo $member_tree_list;
                    ?>
                </li>
                
                <?php  } ?>
    
    
            </ul>
        </div>
       
      
       
       
       </div>
       
       
    <!--    
       
       
      <div class="prolific segment">
            

       <div class="prolific header">Find Member (List view)</div>


        <div class="prolific fitted divider"></div>

 -->


<?php

//$deposit_list = Registry::get("TraderSystem")->getUserList();

//$de_cnt = count($deposit_list);

?>
<!-- 

<table class="prolific sortable table">
    
    <tr>
        <th class="disabled">Edit</th>
        <th data-sort="int">UserID</th>
        <th data-sort="string">Username</th>
        <th data-sort="string">Name</th>
        <th data-sort="string">Email</th>
        <th class="disabled">Status</th>
        <th class="disabled">Com (USD/Lot)</th>
        <th data-sort="int">Referrer ID</th>
    </tr>
   
    <tbody>
<?php

for ($ii=0;$ii<$de_cnt;$ii++) {
    
    $dd_list = $deposit_list[$ii];
    
    $uData = Registry::get("TraderSystem")->getUserByID($dd_list->id);
    $dd2 = $uData[0];
    
    if($dd2->can_refer_status == 3) {
        $can_sta = "<b><font color='#0000FF'>Head office</font></b>";
    } else if($dd2->can_refer_status == 2) {
        $can_sta = "<b><font color='#33CC00'>Center</font></b>";
    } else if($dd2->can_refer_status == 1) {
        $can_sta = "<b><font color='#FF0000'>IB</font></b>";
    } else {
        $can_sta = "";
    }
    
    echo "<tr>
                <td><a href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration&uid=".$dd_list->id."'>Edit</a></td>
                <td>".$dd_list->id."</td>
                <td>".$dd_list->username."</td>
                
                <td>".$dd_list->fname." ".$dd_list->lname."</td>
                
                <td>".$dd_list->email."</td>
                
                <td align='center'>".$can_sta."</td>
                
                <td align='right'>".$dd2->comm_percent_per_comm."</td>
                
                <td align='center'>".$dd2->ref_id."</td>
                
        </tr>"; 
}


?>
    </tbody>
</table>

-->

<!--
</div> --> <!-- end < div class="prolific segment" > -->


<?php } ?>


</div> <!-- end < div class="prolific-large-content" > -->

