<?php
  /**
   * TraderSystemCommission Class
   *
   * @package CMS Prolific
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: admin_commission_class.php, v2.00 2014-10-25 10:37:05 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
  //include(BASEPATH . 'modules/tradersystem/_class/trade_class.php');
  include_once(BASEPATH . 'admin/modules/tradersystem/admin_class.php');
  
  class TraderSystemCommission extends TraderSystem
  {
      
    // const commLevelTable       = "acms_comm_level";
   // const commSettingTable     = "acms_comm_setting";
   // const commAccDtlTable     = "acms_account_dtl";
	//const commTypeTable     = "acms_comm_type";
     
    function __construct()
    {
        parent::__construct(); 
    }
    
    function getLevelList() 
    {
        $sData = array();
       
       
       Registry::set('TraderSystem', new TraderSystem()); 
       
       $sData = Registry::get("TraderSystem")->call_curl_get("comm_level?cmd=comm", "GET");
       return $sData;
    }
    
    function addLevel($fa)
    {
       
        $fq = array();
        $fq = $fa;
      // $now_level = TraderSystemCommission::getdata("MAX(level_no) AS max_level",self::commLevelTable,"where level_id !='' ");
     
      // $fq['level_no'] = $now_level['max_level'] + 1;
       $res = array(); 
       
       
       Registry::set('TraderSystem', new TraderSystem()); 
	   
	   $fq['cmd'] = "comm";
       
       $sData = Registry::get("TraderSystem")->call_curl("comm_level", "POST", $fq);
       
       if($sData) {
           $res['result'] = "success";
       } else {
           $res['result'] = "error";
       }
        return $res;

    }
    
     function updateLevel($fq,$fk)
    {
        //$aData = array();
        $fa = array();
       $fa = $fq;
       $fb = array();
       $fb = $fk;
       $cnt = count($fa);
       $xx = 0;
       
        Registry::set('TraderSystem', new TraderSystem()); 
        
       for($ii=0;$ii<$cnt;$ii++) {
           $fbb = array( 'cmd'=>'comm','usd_per_lot' => $fb[$ii] , 'level_id' => $fa[$ii]);
          $result =  Registry::get("TraderSystem")->call_curl("update_comm_level", "POST", $fbb);
          if ($result) {
              $xx++;
          }
       }
       $res = array(); 
       if($xx == $cnt) {
           $res['result'] = "success";
       } else {
           $res['result'] = "error";
       }
        return $res;  
    }
    
     function deleteLevel($fa)
    {
       $res = array();
       $fq = array();
       $fq['level_id'] = $fa['last_level_id'];
       $fq['cmd'] = 'comm';
       
       Registry::set('TraderSystem', new TraderSystem()); 
       
       $sData = Registry::get("TraderSystem")->call_curl("delete_comm_level", "POST", $fq);
                
       if($result) {
           $res['result'] = "success";
       } else {
           $res['result'] = "error";
       }
        return $res;
    }
    
    function getCommSetting() 
    {
       $sData = array();
       /* $sql = "SELECT * 
                FROM ".self::commSettingTable." 
                WHERE set_id = 1 
                ";
       $sData = Registry::get("Database")->fetch_all($sql); */
       
       Registry::set('TraderSystem', new TraderSystem()); 
       
      $sData = Registry::get("TraderSystem")->call_curl_get("comm_setting?cmd=comm", "GET");
      return $sData;
    }
    
    function updateCommSetting($arrPost)
    {
        $fb = array();
       $fb = $fq;
       $res = array(); 
       
       Registry::set('TraderSystem', new TraderSystem()); 
	   
	   $arrPost['cmd'] = "comm";
       
       $resup = Registry::get("TraderSystem")->call_curl("update_comm_setting","POST",$arrPost); 
       
       if ($resup) {
           $res['result'] = "success";
       } else {
           $res['result'] = "error";
       }
        return $res;  
    }
    
    function getCommTypeList($status)   
    {
         $lData = array();    
         
        Registry::set('TraderSystem', new TraderSystem()); 
                
        /*$sql = "SELECT *                 
                     FROM ".self::commTypeTable."                 
                 WHERE ct_id != ''                 
                 ORDER BY ct_id ASC ";  
        */     
        
        $arr_status = array('status'=>$status);
       $lData = Registry::get("TraderSystem")->call_curl_get("comm_type/".$status."?cmd=comm","GET");   
	  //print_r($lData);   
       return $lData;   
    }
    
    
    function updateAccountCommission($arrPost) 
    {
        $res = array(); 
       $fqa['commission'] = $arrPost['commission'];
       $fqa['can_refer_status'] = $arrPost['can_refer_status'];
       
       //$cms_uid = $arrPost['id'];
       
       //$userdata = $this->getUserByID($cms_uid);
       
       $fqa['email'] = $arrPost['email'];
       $fqa['cmd'] ='comm';
      // print_r($fqa);
           
       $lData = Registry::get("TraderSystem")->call_curl("update_user_comm","POST",$fqa);  
       
       /*
       if ($lData == 1) {
      
          $res1 = $this->upHeadOfficeCommCMS($comm_usd);
          $res['result'] = "success";
       } else {
           $res['result'] = "error";
       }*/
        return $lData;  
    }
    
    
    function updateAccountCommissionCMS($arrPost) 
    {
           
            $sql = "UPDATE users SET commission = ".$arrPost['commission']." 
                        , can_refer_status = ".$arrPost['can_refer_status']." 
                        WHERE email = '".$arrPost['email']."'  ";
       
            $res = Registry::get("Database")->query($sql);
      
            return $res;
      
    }
    
    
    function upHeadOfficeComm($comm_usd) 
    {
        $res = array(); 
        /* i_update($tab, $pk, $fld, $ww, $debug = false) */
        $fqa['commission'] = $comm_usd;
		$fqa['cmd'] = "comm";
        
       $lData = Registry::get("TraderSystem")->call_curl("update_comm_head_office","POST",$fqa);  
       
       
       if ($lData == 1) {
          // $fqa['commission'] = $comm_usd;
          //Registry::get("TraderSystem")->update("users","can_refer_status",$fqa,3);
          $res1 = $this->upHeadOfficeCommCMS($comm_usd);
          $res['result'] = "success";
       } else {
           $res['result'] = "error";
       }
        return $lData;  
    }
    
    function upHeadOfficeCommCMS($commission) 
    {
           
            $sql = "UPDATE users SET commission = ".$commission." WHERE can_refer_status = 3 ";
       
            $res = Registry::get("Database")->query($sql);
      
            return $res;
      
    }
    
    function getUserHeadOffice() 
    {
        $fq = array();
       $fq = $fa;
     
       $res = array(); 
       
       
       Registry::set('TraderSystem', new TraderSystem()); 
       
       $sData = Registry::get("TraderSystem")->call_curl_get("head_office?cmd=head_offece", "GET");
       
       if($sData) {
           $res['result'] = "success";
          $res['data'] = $sData;
       } else {
           $res['result'] = "error";
       }
        return $sData;
    }
    
   /* function getdata( $colname, $tablename, $wherecondition = '', $debug = false)
    {
        $querystring = "SELECT $colname from $tablename $wherecondition";
        if($debug == true) { echo"<br>query=" . $querystring; }
        $r1 = Registry::get("Database")->query($querystring);
        if (!$r1) {
            return false;
        }
        $return_data = json_decode(json_encode($r1), true);
        return $return_data;
    } */
    
    function getUserByRefEmail($email)
    {
        
       // Registry::set('Database', new Database()); 
        
       //$aData = Registry::get("TraderSystem")->getdata("*","users","where ref_email='".$email."' ");
       $sql = "SELECT * FROM users where ref_email = '".$email."' ";
       
      $row = Registry::get("Database")->fetch_all($sql);
      
       return $row;     
        
    }
    
    function getUserByEmail($email)
    {
        
       $sql = "SELECT * FROM users where email = '".$email."' ";
       
      $row = Registry::get("Database")->fetch_all($sql);
      
       return $row;     
        
    }
    
    
    function getUserByID($uid)
    {
        
      
       $sql = "SELECT * FROM users where id = '".$uid."' ";
       
      $row = Registry::get("Database")->fetch_all($sql);
      
       return $row;     
        
    }
	
	function getUserList()    
	{
	            $sql = "SELECT * "            
	            . "\n FROM users where id!='0' "
	            . "\n order by id ASC ";        
	            $row = Registry::get("Database")->fetch_all($sql);        
	            return ($row) ? $row : 0;        
    } 

    function getListAccTraderByUID($uid)
    {
        
        $sql = "select * from bk_trader_acc where uid='".$uid."'";
        $row = Registry::get("Database")->fetch_all($sql);
        if ($row) {
          
            foreach ($row as $k => $v) {
               
               $list_acc = $v->trader_acc_login;
            
            }
           
        } else {
            $list_acc = '';
        }
        return $list_acc;
    }

    function getUserByRefID($ref_id)    
    {
                $sql = "SELECT A.*  " 
                                   . "\n FROM users A "      
                       . "\n where cms_ref_id ='".$ref_id."' "
                       . "\n order by id ASC";  
                      
                   $row = Registry::get("Database")->fetch_all($sql);       
                   return ($row) ? $row : 0;        
    } 
	
	function updateReferrer($arrPost) 
    {
       $res = array(); 
        $fqa['edit_email'] = $arrPost['edit_email'];
	    $fqa['ref_email'] = $arrPost['ref_email'];
       
      
       $fqa['cmd'] ='comm';
      
       //print_r($fqa);
           
       $lData = Registry::get("TraderSystem")->call_curl("update_user_ref","POST",$fqa);  
       
       
       if ($lData['isSuccess'] == 1) {
      
         // $res1 = $this->updateReferrerCMS($comm_usd);
          $res['result'] = "success";
       } else {
           $res['result'] = "error";
       }
        return $res;  
    }
	
    function updateReferrerCMS($ref_email,$cms_ref_id,$edit_id) 
    {
           
            $sql = "UPDATE users SET ref_email = '".$ref_email."' , cms_ref_id = ".$cms_ref_id."  WHERE id = ".$edit_id;
       
            $res = Registry::get("Database")->query($sql);
      
            return $res;
      
    }
	
	
    
  }