<?php
  /**
   * Gallery
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: gallery.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  if(!$user->getAcl("tradersystem")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;
    
  Registry::set('TraderSystem', new TraderSystem());
  
  include(BASEPATH . 'admin/modules/tradersystem/admin_commission_class.php');
  
  Registry::set('TraderSystemCommission', new TraderSystemCommission());

?>

<center>
	<!--
<strong>
	<a href='index.php?do=modules&action=config&modname=tradersystem'>Dashboard</a> 
	| <a href='index.php?do=modules&action=config&modname=tradersystem&menu=configuration'>Configuration</a> 
	| <a href='index.php?do=modules&action=config&modname=tradersystem&menu=member-list'>Member List</a> 
	| <a href='index.php?do=modules&action=config&modname=tradersystem&menu=member-document'>Member Document</a>
	| <a href='index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-pending'>Deposit Pending List</a> 
	| <a href='index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-approved'>Deposit All List</a> 
	| <a href='index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-pending'>Withdraw Pending List</a>
	| <a href='index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-approved'>Withdraw All List</a>
    | <a href='index.php?do=modules&action=config&modname=tradersystem&menu=member-trade-lot'>Member Trade Lot</a> 
    | <a href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-level-configuration'>Commission Level Configuration</a> 
</strong>
	-->
</center>

<?php

if(isset($_GET['menu'])) {
	$menu = $_GET['menu'];
} else {
	$menu = "";
}

if ($menu == '') {
	include "dashboard.php";
} else { 
	include $menu.".php";
}

?>


