<?php
define("_VALID_PHP", true);
require_once("../../init.php");
require_once("init_ajax.php");
$action = isset($_GET['action']) ? $_GET['action'] : '';if ($action == 'withdraw_pending' || $action  == 'deposit_all') {	ajax_withdraw($action);} elseif ($action  == 'deposit_pending' || $action  == 'deposit_all') {	ajax_deposit($action);} elseif ( $action == 'document_approve' || $action == 'document_cancel' || $action == 'document_pending') {	ajax_ducument($action);} elseif($action == 'transaction_all'){	ajax_transaction_all($action);} elseif ($action == 'document_docs_approve' || $action == 'document_docs_cancel' || $action == 'document_docs_pending') {	ajax_document_docs($action);} elseif ($action == 'user_trade') {
	ajax_user_trade($action);
}
function ajax_user_trade($action)
{
	$res 			= array();
	$res['draw'] 	= isset($_GET['draw'])?$_GET['draw']:1;
	$lengh 			= isset($_GET['length'])?$_GET['length']:50;
	$limit 			= "limit ".$_GET['start'].",".$lengh;
	$res['data'] 	= array();
	$condi	='';
	if (isset($_GET['search']['value']) && $_GET['search']['value'] !='') {
		$txtsearch = $_GET['search']['value'];
		$condi = " AND (";
		$condi .=" u.fname like '%".$txtsearch."%' OR ";
		$condi .=" u.lname like '%".$txtsearch."%' OR ";
		$condi .=" u.email like '%".$txtsearch."%' OR ";
		$condi .=" a.trader_acc_login like '%".$txtsearch."%'";
		$condi .= " )";
	}
    if (isset($_GET['itype']) && $_GET['itype'] == 'A') {
    	$condi .=" AND book_a=1 ";
    } else {
    	$condi .=" AND book_a !=1 ";
    }
	$sql 		= "
			SELECT	
				u.lname,u.lname,u.email,
				a.*
			FROM
				users u,bk_trader_acc a
			WHERE
				u.id=a.uid
				AND a.uid!=9999999 
				".$condi."  ORDER BY a.net_profit DESC,a.uid DESC ".$limit
			;
	$res['sql'] = $sql;
	$row 		= Registry::get("Database")->fetch_all($sql);
	if ($row) {
		$i = 0;
		foreach ($row as $k => $dd_list) 
		{
			$i++;
			
			$icon = '  <a href="javascript:void(0);" login="'.$dd_list->trader_acc_login.'" email="'.$dd_list->email.'" itype="del" class="acctrade"><i class="rounded danger inverted remove icon link"></i></a>';
			$iconedit = '  <a href="javascript:void(0);" login="'.$dd_list->trader_acc_login.'"  email="'.$dd_list->email.'" itype="pwd"class="acctrade"><i class="rounded inverted success icon key link"></i></a>';
			$a = array(
			$i,
			$icon . $iconedit,
			$dd_list->trader_acc_login,
			$dd_list->fname.' '.$dd_list->lname.'['.$dd_list->uid.']',
		    $dd_list->group,
		    $dd_list->reg_date,
			number_format($dd_list->balance,2),
			number_format($dd_list->equlity,2),
			number_format($dd_list->credit,2),
			number_format($dd_list->net_profit,2),

			);

			array_push($res['data'],$a);

		}

	}

	$all =  Registry::get("TraderSystem") -> getdata("count(a.id) as aall","users u,bk_trader_acc a","where u.id=a.uid  ".$condi ,false);

	$res['recordsTotal'] = $all['aall'];

	$res['recordsFiltered'] = $all['aall'];

	print json_encode($res);  

} 
function ajax_document_docs($action)
{	switch($action) {		case 'document_docs_pending':			$condista = " AND (d.is_verified=0)";break;		case 'document_docs_approve':			$condista = " AND (d.is_verified=1 )";break;		case 'document_docs_cancel':			$condista = " AND (d.is_verified=2)";break;
	}
	$res 			= array();
	$res['draw'] 	= isset($_GET['draw'])?$_GET['draw']:1;
	$lengh 			= isset($_GET['length'])?$_GET['length']:50;
	$limit 			= "limit ".$_GET['start'].",".$lengh;
	$res['data'] 	= array();
	$condi	='';
	if (isset($_GET['search']['value']) && $_GET['search']['value'] !='') {
		$txtsearch = $_GET['search']['value'];
		$condi = " AND (";
		$condi .=" u.fname like '%".$txtsearch."%' OR ";
		$condi .=" u.lname like '%".$txtsearch."%' ";
		$condi .= " )";
	}
	$sql 		= "
			SELECT	
				d.*,
				u.id as userid,u.fname,u.lname,u.email
			FROM
				bk_document_user d,
				users u
			WHERE
			    d.uid = u.id
				".$condista."  ".$condi." ".$limit
			;
	$res['sql'] = $sql;
	$row 		= Registry::get("Database")->fetch_all($sql);

	if ($row) {
		$i = 0;
		$aTypeDoc = array("bookbank"=>"Bank Information");
		$aStatus = array(
			0=>'<span class="prolific warning label">Pending</span>',
			1=>'<span class="prolific positive label">Approved</span>',
			2=>'<span class="prolific negative  label">Cancel</span>',
			);
		$fileok = "<i class='rounded inverted icon purple file'></i>";
		$filenot = "<span class='prolific negative label'>None</span>";
		foreach ($row as $k => $dd_list) {
			$i++;
			$aDtl = json_decode($dd_list->data);
			if ($dd_list->is_verified != 2 && is_file(UPLOADS . 'file_doc/' . $aDtl->file_url)) {
				$icon = $aStatus[$dd_list->is_verified]; 
	 			//$iconcard .= '  <a href="' . UPLOADURL . 'file_doc/' . $dd_list->id_card_file_url . '" target="_blank" width="200"><i class="rounded inverted icon purple file"></i></a>';
				$icon = '  <a href="javascript:void(0);" sta="'.$dd_list->is_verified.'" filename= "'.$aDtl->file_url.'" class="document_mgr" itype="'.$dd_list->doc_type.'" email="'.$dd_list->email.'">'.$icon.'</a>';
			} else {
				$icon = $filenot;
			}
			$a = array(
			$i,
			$dd_list->userid,
			$dd_list->fname.' '.$dd_list->lname,
			$aTypeDoc[$dd_list->doc_type],
			$icon
			);
			array_push($res['data'],$a);
		}
	}
	$all =  Registry::get("TraderSystem") -> getdata("count(d.id) as aall","bk_document_user d,users u","where d.uid = u.id ".$condista."  ".$condi ,false);
	$res['recordsTotal'] = $all['aall'];
	$res['recordsFiltered'] = $all['aall'];
	print json_encode($res);  
}
 
function ajax_ducument($action){
	switch($action){
		case 'document_pending':
			$condista = " AND (u.id_card_verified=0  ||  u.address_verified=0)"; 
			break;
		case 'document_approve':
			$condista = " AND (u.id_card_verified=1  ||  u.address_verified=1)"; 
			break;
		case 'document_cancel':
		$condista = " AND (u.id_card_verified=2  ||  u.address_verified=2)"; 
		break;
	}
	
	$res 			= array();
	$res['draw'] 	= isset($_GET['draw'])?$_GET['draw']:1;
	$lengh 			= isset($_GET['length'])?$_GET['length']:50;
	$limit 			= "limit ".$_GET['start'].",".$lengh;
	$res['data'] 	= array();
	$condi	='';
	if (isset($_GET['search']['value']) && $_GET['search']['value'] !='') {
		$txtsearch = $_GET['search']['value'];
		$condi = " AND (";
		$condi .=" u.fname like '%".$txtsearch."%' OR ";
		$condi .=" u.lname like '%".$txtsearch."%'";
		$condi .= " )";
	}
	$sql 		= "
			SELECT	
				u.*
			FROM
				users u
			WHERE
				(u.id_card_file_url != '' || u.address_file_url !='')
				".$condista."  ".$condi." ".$limit
			;
	$res['sql'] = $sql;
	$row 		= Registry::get("Database")->fetch_all($sql);
	
	if ($row) {
		$i = 0;
		$aStatus = array(
		0=>'<span class="prolific warning label">Pending</span>',
		1=>'<span class="prolific positive label">Approved</span>',
		2=>'<span class="prolific negative  label">Cancel</span>',
		);
		$fileok = "<i class='rounded inverted icon purple file'></i>";
		$filenot = "<span class='prolific negative label'>None</span>";//"<i class='rounded inverted icon file'></i>";
		foreach ($row as $k => $dd_list) {
			$i++;
			
			if ($dd_list->id_card_verified != 2 && is_file(UPLOADS . 'file_doc/' . $dd_list->id_card_file_url)) {
				
				$iconcard = $aStatus[$dd_list->id_card_verified]; 
	 			//$iconcard .= '  <a href="' . UPLOADURL . 'file_doc/' . $dd_list->id_card_file_url . '" target="_blank" width="200"><i class="rounded inverted icon purple file"></i></a>';
				$iconcard = '<a href="javascript:void(0);" class="document_mgr" sta="'.$dd_list->id_card_verified.'" itype="card" email="'.$dd_list->email.'">'.$iconcard.'</a>';
							
			} else {
				$iconcard = $filenot;
			}
			
			if ($dd_list->address_verified != 2 &&is_file(UPLOADS . 'file_doc/' . $dd_list->address_file_url)) {
				$iconaddr = $aStatus[$dd_list->address_verified];  
	 			//$iconaddr .= '  <a href="' . UPLOADURL . 'file_doc/' . $dd_list->address_file_url . '" target="_blank" width="200"><i class="rounded inverted icon purple file"></i></a>';
				$iconaddr ='  <a href="javascript:void(0);" sta="'.$dd_list->address_verified.'" class="document_mgr" itype="addr" email="'.$dd_list->email.'">'.$iconaddr.'</a>';
				
			} else {
				$iconaddr = $filenot;
			}
			/*
			if ($dd_list->bank_verified != 2 &&is_file(UPLOADS . 'file_doc/' . $dd_list->bank_file_url)) {
				$iconbank = $aStatus[$dd_list->bank_verified];  
	 			//$iconbank .= '  <a href="' . UPLOADURL . 'file_doc/' . $dd_list->bank_file_url . '" target="_blank" width="200"><i class="rounded inverted icon purple file"></i></a>';
				$iconbank = ($dd_list->bank_verified == 0)?' <a href="javascript:void(0);" class="document_mgr" itype="bank" email="'.$dd_list->email.'">'.$iconbank.'</a>'
				:'  <a href="' . UPLOADURL . 'file_doc/' . $dd_list->bank_file_url . '" target="_blank" width="200">'.$iconbank.'</a>';
			} else {
				$iconbank = ' ' . $filenot;
			}
			*/
			$a = array(
			$i,
			$dd_list->id,
			$dd_list->fname.' '.$dd_list->lname,
			$iconcard,
			$iconaddr
			);
			array_push($res['data'],$a);
		}
	}
	$all =  Registry::get("TraderSystem") -> getdata("count(u.id) as aall","users u","where (u.id_card_file_url != '' || u.address_file_url !='') ".$condista." ".$condi ,false);
	$res['recordsTotal'] = $all['aall'];
	$res['recordsFiltered'] = $all['aall'];
	print json_encode($res);  
} 


function ajax_transaction_all(){
	$res 			= array();
	$res['draw'] 	= isset($_GET['draw'])?$_GET['draw']:1;
	$lengh 			= isset($_GET['length'])?$_GET['length']:50;
	$limit 			= "limit ".$_GET['start'].",".$lengh;
	$res['data'] 	= array();
	$condi	='';
	if (isset($_GET['search']['value']) && $_GET['search']['value'] !='') {
		$txtsearch = $_GET['search']['value'];
		$condi = " AND (";
		$condi .=" u.fname like '%".$txtsearch."%' OR ";
		$condi .=" u.lname like '%".$txtsearch."%' OR ";
		$condi .=" t.trans_date like '%".$txtsearch."%'";
		$condi .= " )";
	}
	$sql 		= "
			SELECT	
				t.*,
				u.fname,u.lname 
			FROM
				bk_wallet_transaction t,users u 
			WHERE
				t.user_id=u.id
				AND t.status !='' ".$condi." ".$limit
			;
	$res['sql'] = $sql;
	$row 		= Registry::get("Database")->fetch_all($sql);
	
	if ($row) {
		$i = 0;
		foreach ($row as $k => $dd_list) {
			$i++;
			$money = 0;
			if ($dd_list->money < 0 )  {
				$money = "<font color='red'><b>".number_format($dd_list->money,2)."</b></font>";
				$link = "<a href='index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-pending-edit&trans_id=".$dd_list->trans_id."'><i class='rounded inverted success icon pencil link'></i></a>";
			} elseif($dd_list->money > 0) {
				$money = "<font color='green'><b>".number_format($dd_list->money,2)."</b></font>"; ;
				$link = "<a href='index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-pending-edit&trans_id=".$dd_list->trans_id."'><i class='rounded inverted success icon pencil link'></i></a>";
			} else {
				$link = "-";
			}
			
			switch($dd_list->status){
				case 1 : $txt = "<font color='green'>Approve</font>";break;
				case 0 : $txt = "<font color='blue'>Pending</font>";break;
				case 11 : $txt = "<font color='blue'>Processing</font>";break;
				case 10 : $txt = "<font color='blue'>Pending</font>";break;
				case 2 : $txt = "<font color='red'>Cancel</font>";break;
			}
			/*
			 $moneyin = 0;
			$moneyout = 0;
			 * */
			$a = array(
			$link,
			$dd_list->trans_id,
			$dd_list->fname." ".$dd_list->lname."[".$dd_list->user_id."]",
			"<p align='right'>".$money."</p>",
			$txt,
			$dd_list->gateway_dtl,
			$dd_list->save_date);
			array_push($res['data'],$a);
		}
	}
	$all =  Registry::get("TraderSystem") -> getdata("count(t.trans_id) as aall","bk_wallet_transaction t,users u","where t.user_id = u.id  and t.trans_id!='' ".$condi ,false);
	$res['recordsTotal'] = $all['aall'];
	$res['recordsFiltered'] = $all['aall'];
	print json_encode($res);  
}
function ajax_withdraw($action){
	switch($action){
		case 'deposit_pending':$status = 0;$statustxt='PENDING';break;
		case 'deposit_all':$status = 1;$statustxt='APPROVE';break;
	}
	$res 			= array();
	$res['draw'] 	= isset($_GET['draw'])?$_GET['draw']:1;
	$lengh 			= isset($_GET['length'])?$_GET['length']:50;
	$limit 			= "limit ".$_GET['start'].",".$lengh;
	$res['data'] 	= array();
	$condi	='';
	if (isset($_GET['search']['value']) && $_GET['search']['value'] !='') {
		$txtsearch = $_GET['search']['value'];
		$condi = " AND (";
		$condi .=" u.fname, like '%".$txtsearch."%' OR ";
		$condi .=" u.lname like '%".$txtsearch."%' OR ";
		$condi .=" t.trans_date like '%".$txtsearch."%'";
		$condi .= " )";
	}
	$sql 		= "
			SELECT	
				t.*,
				u.fname,u.lname 
			FROM
				bk_wallet_transaction t,users u 
			WHERE
				t.user_id=u.id
				AND t.money < 0 and t.status ='".$status."' ".$condi." ".$limit
			;
	$res['sql'] = $sql;
	$row 		= Registry::get("Database")->fetch_all($sql);
	
	if ($row) {
		$i = 0;
		foreach ($row as $k => $dd_list) {
			$i++;
			if ($action == 'withdraw_pending')  {
				$link = "<a href='index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-pending-edit&trans_id=".$dd_list->trans_id."'><i class='rounded inverted success icon pencil link'></i></a>";
			} else {
				$link='-';
			}
			
			switch($dd_list->status){
				case 1 : $txt = "<font color='green'>Approved</font>";break;
				case 0 : $txt = "<font color='blue'>Pending</font>";break;
				case 2 : $txt = "<font color='red'>Cancel</font>";break;
			}
			$dd_list->money = -$dd_list->money;
			$a = array(
			$link,
			$dd_list->trans_id,
			$dd_list->fname." ".$dd_list->lname."[".$dd_list->user_id."]",
			number_format($dd_list->money, 2),
			number_format($dd_list->money_local, 2),
			$dd_list->deposit_comment,
			
			$dd_list->save_date);
			array_push($res['data'],$a);
		}
	}
	$all =  Registry::get("TraderSystem") -> getdata("count(t.trans_id) as aall","bk_wallet_transaction t,users u","where t.user_id = u.id  and t.money < 0  and t.status ='".$status."' ".$condi ,false);
	$res['recordsTotal'] = $all['aall'];
	$res['recordsFiltered'] = $all['aall'];
	print json_encode($res);  
}

function ajax_deposit($action){
	switch($action){
		case 'deposit_pending':$status = 0;$statustxt='PENDING';$status_sql=' ((t.status = 0) OR (t.status = 10)) ';break;
		case 'deposit_all':$status = 1;$statustxt='APPROVE';$status_sql=' t.status=1 ';break;
	}
	$res 			= array();
	$res['draw'] 	= isset($_GET['draw'])?$_GET['draw']:1;
	$lengh 			= isset($_GET['length'])?$_GET['length']:50;
	$limit 			= " ORDER BY t.trans_id DESC limit ".$_GET['start'].",".$lengh;
	$res['data'] 	= array();
	$condi	='';
	if (isset($_GET['search']['value']) && $_GET['search']['value'] !='') {
		$txtsearch = $_GET['search']['value'];
		$condi = " AND (";
		$condi .=" u.fname like '%".$txtsearch."%' OR ";
		$condi .=" u.lname like '%".$txtsearch."%' OR ";
		$condi .=" t.trans_date like '%".$txtsearch."%'";
		$condi .= " )";
	}
	$sql 		= "
			SELECT	
				t.*,
				u.fname,u.lname 
			FROM
				bk_wallet_transaction t,users u 
			WHERE
				t.user_id=u.id
				AND t.money > 0 
				and ".$status_sql." 
				".$condi." ".$limit
			;
	$res['sql'] = $sql;
	$row 		= Registry::get("Database")->fetch_all($sql);
	
	if ($row) {
		$i = 0;
		foreach ($row as $k => $dd_list) {
			$i++;
			if ($action == 'deposit_pending')  {
				$link = "<a href='index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-pending-edit&trans_id=".$dd_list->trans_id."'><i class='rounded inverted success icon pencil link'></i></a>";
			} else {
				$link='-';
			}
			if (is_file(UPLOADS . 'file_slip/' . $dd_list->file_slip)) { 
	 			$link_img = '  <a href="' . UPLOADURL . 'file_slip/' . $dd_list->file_slip . '" target="_blank" width="200"><i class="camera icon" data-icon-name="camera icon"></i></a>';
			}else{				$link_img='';			}
			switch($dd_list->status){
				case 1 : $txt = "<font color='green'>Approved</font>";break;
				case 0 : $txt = "<font color='blue'>Pending</font>";break;
				case 2 : $txt = "<font color='red'>Cancel</font>";break;
              case 10 : $txt = "<font color='blue'>Pending Approval</font>";break;
              case 11 : $txt = "<font color='blue'>Pending</font>";break;
			}
			$a = array(
			$link,
			$dd_list->trans_id,
			$dd_list->fname." ".$dd_list->lname."[".$dd_list->user_id."]",
		    '<p align="right">'.number_format($dd_list->money, 2).'</p>',
		     '<p align="right">'.number_format($dd_list->money_local, 2).'</p>',
			$dd_list->bank_code. $link_img ,
			$dd_list->trans_date);
			array_push($res['data'],$a);
		}
	}
	$all =  Registry::get("TraderSystem") -> getdata("count(t.trans_id) as aall","bk_wallet_transaction t,users u","where t.user_id = u.id  and t.money > 0  and ".$status_sql." ".$condi ,false);
	$res['recordsTotal'] = $all['aall'];
	$res['recordsFiltered'] = $all['aall'];
	print json_encode($res);  
}



?>