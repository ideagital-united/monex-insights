<?php
  /**
   * Configuration
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: config.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
if(!$user->getAcl("deposit")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

Registry::get("TraderSystem")->check_admin_prolific();

?>
<div class="prolific icon heading message mortar"><a class="helper prolific top right info corner label" data-help="configure"><i class="icon help"></i></a> <i class="setting icon"></i>
  <div class="content">
    <div class="header">PAYMENT DETAIL</div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_N_CONF;?></div>
    </div>
  </div>
</div>
<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_CG_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="prolific form segment">
    <div class="prolific header">MANAGE PAYMENT</div>
    <div class="prolific double fitted divider"></div>
<?php
function genHtmlfield($title,$setval,$input_type='text',$input_name='name_beta')
{
	?>
	<div class="field">
		<label><?php echo $title;?></label>
		<label class="input">
			<?php if($input_type=='text') {?>
			<input type="text" value="<?php echo $setval;?>" name="<?php echo $input_name;?>" disabled="disabled">
			<?php } elseif($input_type=='textarea'){ ?>
				<textarea cols='35' rows='3' name="<?php echo $input_name;?>"><?php echo $setval;?></textarea>
			<?php } ?>
		</label>
	</div>
        
	<?php
}
$trans_id = $_GET['trans_id'];
//$deposit_list = Registry::get("TraderSystem")->getdata("*","acms_wallet_transaction","where trans_type='deposit_from_bank_to_wallet' and status ='0' and trans_id='".$trans_id."'");

//$deposit_list = Registry::get("TraderSystem")->get_transaction_by_id();
$deposit_list = Registry::get("TraderSystem")->getdata("*","bk_wallet_transaction","where trans_id='".$trans_id."' ");//"get_transaction_by_id","POST",array('pkid'=>$trans_id));
if ($deposit_list) {
	$aDtl = Registry::get("TraderSystem")->getdata("id,fname,lname,id_card_verified,address_verified","users","where id='".$deposit_list['user_id']."'");
	$iconStatus = ($aDtl['id_card_verified'] != 1 || $aDtl['address_verified'] != 1 ) 
				? "<a href='index.php?do=users&action=edit&id=".$deposit_list['user_id']."'><i class='rounded danger inverted remove icon link'></i> Not Verify</a>" 
				: "<a href='index.php?do=users&action=edit&id=".$deposit_list['user_id']."'><i class='rounded inverted success icon check link'></i> Approval</a>";
	$iconBank  = Registry::get("TraderSystem")->getdata("id","bk_document_user","where doc_type='bookbank' and uid='".$deposit_list['user_id']."' and is_verified=1");
	$iconBank  = ($iconBank) ? "<i class='rounded inverted success icon check link'></i> Approval":"<i class='rounded danger inverted remove icon link'></i> Not Verify";
	$aRate = Registry::get("TraderSystem")->getdata("withdraw,currency","bk_exchn_rate","where id=1");
	$link_img = '';
	
	if (is_file(UPLOADS . 'file_slip/' . $deposit_list['file_slip'])) { 
		$link_img = '<a href="' . UPLOADURL . 'file_slip/' . $deposit_list['file_slip'] . '" target="_blank" width="200">'.
					'<img src="'.UPLOADURL.'file_slip/'.$deposit_list['file_slip'].'" width="200"></a>';
	}
	$deposit_list['money'] = -$deposit_list['money'];
	?>
	<div class="two fields">
		<?php genHtmlfield('TransactionID',$deposit_list['trans_id']);?>
        <?php genHtmlfield('UserID',$aDtl['id']);?>
	</div>
	<div class="four fields">
		<?php genHtmlfield('Customer Name',$aDtl['fname'].' '.$aDtl['lname']);?>
		<div class="field">
        	<label>Verify the person's status</label>
        	<label class="input"><?php echo $iconStatus;?></label>
        </div>
		 <?php genHtmlfield('Amount (USD)',number_format($deposit_list['money'],2));?>
        <?php genHtmlfield('Gateway',$deposit_list['gateway_dtl']);?>
        
	</div>
	<?php if((isset($deposit_list['curency']) && $deposit_list['curency'] != '' && $deposit_list['rate_exchange'] > 0) ) { ?>
	<div class="two fields">
		<?php genHtmlfield('Currency Rate '.$deposit_list['curency'].'[1USD = '.$deposit_list['rate_exchange'].' '.$deposit_list['curency'].']'
						,number_format($deposit_list['money_local'],2));?>
        
	</div>
	<?php } ?>
	<div class="three fields">
		<div class="field">
        	<label>Bank Transfer</label>
        	<label><div style="border:1px solid;padding:5px 5px 5px 5px;"><?php echo$deposit_list['deposit_comment'];?></div></label>
        </div>
        <div class="field">
        	<label>Bank documents verify</label>
        	<label class="input"><?php echo $iconBank;?></label>
        </div>
		<?php genHtmlfield('Save Date',$deposit_list['save_date']);?>
       
        
	</div>
	
	<div class="showsmtp">
        <div class="prolific thin attached divider"></div>
      </div>
    <?php if ($deposit_list['status'] == 0) {?>
    <form id="prolific_form" name="prolific_form" method="post">
	<input type="hidden" name="action" value="withdraw_by_admin">
	<input type="hidden" name="trans_id" value="<?php echo $deposit_list['trans_id'];?>">
    <div class="fields">
		   <div class="field">
		   
          <label>Change Status</label>
          <select name="change_status">
			<option value='0' >Pending</option>
			<option value='1'>Approve</option>
			<option value='2'>Cancel</option>
          </select>
        </div>
	</div>
	<div class="fields">
		  <?php genHtmlfield('Comment',"","textarea","comment");?>
	</div>
	<div class="prolific double fitted divider"></div>
      <button type="button" name="dosubmit" class="prolific positive button">Update</button>
      </form>
    <?php }else{?>
    	<div class="fields">
		   <div class="field">
		   
          <label>Status</label>
          <?php
          switch($deposit_list['status']){
		  	case 2 : echo "<font color='red'>Cancel</font>";break;
			case 1 : echo "<font color='green'>Approve</font>";break;
			case 3 : echo "<font color='blue'>Confirm Sms</font>";break;
			default : echo "-";break;
          }
          ?>
        </div>
		</div>
		<div class="fields">
			   <?php genHtmlfield('Deposit Comment',$deposit_list['deposit_comment'],"textarea","deposit_comment");?>
		</div>
		<div class="fields">
			   <?php genHtmlfield('Admin Comment',$deposit_list['comment'],"textarea","comment");?>
		</div>
    <?php }?>
	
	
	<?php
} else {
     Filter::msgSingleAlert("NO DATA");

}
?>   

 </div>     
  <div id="msgholder"></div>
</div>

