<?php
if(!$user->getAcl("deposit")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

Registry::get("TraderSystem")->check_admin_prolific();
?>

<script>
var var_action='withdraw_pending';
</script>

<?php
//=== end edit
//=== start view
/*echo BASEPATH."admin/modules/tradersystem/script_datatable_ajax.php";*/
include(MODPATH."tradersystem/script_datatable_ajax.php");
?>

<div class="prolific icon heading message mortar"><a class="helper prolific top right info corner label" data-help="configure"><i class="icon help"></i></a> <i class="setting icon"></i>
  <div class="content">
    <div class="header">Withdraw Pending</div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      	<div class="active section">Withdraw Pending List</div>
    </div>
  </div>
</div>



<div class="prolific-large-content">
	

	<table border="1" class="display table table-bordered table-striped data-table">
<thead>
  <tr>
		<th align="center">Edit</th>
  	<th align="center">ID<!--trans_id--></th>
		<th align="center">Customers<!--user_id--></th>
		<th align="center">Amount (USD)<!--money_in--></th>
<th align="center">Amount (THB)<!--money_in--></th>
		<th align="center">To Bank<!--thai_bank--></th>
		<th align="center">Save Date<!-- save_date --></th>
  </tr>

</thead>
</table>
</div>





