<?php
  /**
   * TraderSystem Class
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: class_admin.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  class TraderSystem
  {
	  
	 
 	private static $db;

	/**
	* TraderSystem::__construct()
	* 
	* @param bool $galid
	* @return
	*/
	function __construct($galid = false)
	{
		self::$db = Registry::get("Database");
	}
	function buildata_post($fields)
	{
		$postvars = '';
	  	foreach($fields as $key => $value) {
	    	$postvars .= $key . "=" . $value . "&";
	  	}
		
	  	return $postvars;
	}
	function call_curl($fn='',$itype="POST",$aPost = array())
	{
	    $api_key = '';
	    if (isset($_SESSION['admin_secure']['admin_api_key'])) {
		$api_key = $_SESSION['admin_secure']['admin_api_key'];
	    } else if (isset($_SESSION['securekey'])) {
		$api_key = $_SESSION['securekey'];
	    }
			
            $string = TraderSystem::buildata_post($aPost);
			 $ch = curl_init();
          
            curl_setopt($ch, CURLOPT_URL,  URL_API.$fn);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $itype);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
            $r = curl_exec($ch);
            curl_close($ch);
			
            $obj = json_decode($r,true);
		
            return $obj;
			
		// }	
	}
	
	function get_send_data_notice_admin($itypedb,$aParam=array())
	{
		$sqlpass = "select * from bk_mail where status = 1 and group_mail like '%".$itypedb."%'";
		$objpass = Registry::get("Database")->fetch_all($sqlpass);
		if ($objpass) {
			foreach ($objpass as $kpass => $vpass) 
			{
				if($vpass->email != '') {
					$idmail = $aParam['mailid']*1;
				
					$isSend = TraderSystem::send_mail($aParam['typemail'],$idmail,$aParammail=array(
							'typedoc'=>$aParam['typedoctxt'],
							'toname'=>$vpass->email_name,
							'tomail'=>$vpass->email,
							'nameuser' => $_SESSION['name'],
							'iduser' => $_SESSION['uid'],
							'emailuser' => $_SESSION['email']
							));
				}
				if($vpass->mobile != '') {
					$res = TraderSystem::send_sms_infobip("user send ".$aParam['typedoctxt'].".",$vpass->mobile);
				}
			}
		}
	}
	
	function send_notice_to_admin($aParam,$itype=array())
	{
		
		if (isset($aParam['id_card_file_url']) && $aParam['id_card_file_url'] != '') {
			$check_send_notice = TraderSystem::get_send_data_notice_admin("docpass",
			array("typemail"=>"membersenddoc","mailid"=>30,"typedoctxt"=>"Identity Document"));
		}
		if (isset($aParam['address_file_url']) && $aParam['address_file_url'] != '') {
			$check_send_notice = TraderSystem::get_send_data_notice_admin("docaddr",
			array("typemail"=>"membersenddoc","mailid"=>30,"typedoctxt"=>"Address Document"));
		}
		if (isset($aParam['bank_account_number']) && $aParam['bank_account_number'] != '') {
			$check_send_notice = TraderSystem::get_send_data_notice_admin("docbank",
			array("typemail"=>"membersenddoc","mailid"=>30,"typedoctxt"=>"Bank Document"));
		}
		
	}
	/**
	 * ตรวจสบว่า ถ้ายังไม่ได้ทำการ verify bank และบัตรประชาชน ก็ไม่ให้ทำการถอนเงิน
	 */
	function validate_show_frm_withdraw()
	{
		$aRes 		= array();
		$isAdminFix = $_SESSION['adminconf']['fix_verify'];
		$aErr 		= array();
		if ($isAdminFix == 1) {
			$isIdCardVerify = $_SESSION['usr_dtl']['user_info']['id_card_verified'];
			$isAddrVerify 	= $_SESSION['usr_dtl']['user_info']['address_verified'];
			
			if ($isIdCardVerify != 1){
				array_push($aErr, Lang::$word->_PLEASE_VERIFY_ID_CARD);
			}
			if ($isAddrVerify != 1) {
				array_push($aErr, Lang::$word->_PLEASE_VERIFY_ADDR);
			}
		}
		if (count($_SESSION['usr_dtl']['user_bank_list']) > 0) {
			$aBank 		= $_SESSION['usr_dtl']['user_bank_list'];
			$isBankOk 	= false;
			foreach($aBank as $kBank => $vBank){
				if ($vBank['is_verified'] == 1)  
					$isBankOk	=	true;
				break;
			}
			if ($isBankOk == false) {
				array_push($aErr, Lang::$word->_PLEASE_WAIT_VERIFY_BANK);
			}
		}
		if (count($aErr) > 0) {
			$aRes['status'] = false;
			$aRes['Msg'] 	= $aErr;
		} else {
			$aRes['status'] = true;
		}
		return $aRes;
	}
	
	function user_request_curl($fn,$http_req_type,$data)
	{
		$string = http_build_query($data);
		$api_key = $_SESSION['securekey'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API.$fn);	
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $itype);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		return $obj;
	}
	
	function admin_request_curl($fn,$http_req_type,$data)
	{
		$string = http_build_query($data);
		$api_key = $_SESSION['admin_secure']['admin_api_key'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,  URL_API.$fn);	
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $itype);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$r = curl_exec($ch);
		curl_close($ch);
		$obj = json_decode($r,true);
		return $obj;
	}	
	
	function send_sms($text, $phones, $is_unicode)
	{ 
		if (FIX_TEST == true) {
			$res['res'] = true;
		} else {
			if (API_SMS == 'thaibulksms') {
				if (isset($_SESSION['country']) && $_SESSION['country'] !='TH / THA' && api_sms_textmagic_user != '' && api_sms_textmagic_pwd!='') {
					$res = TraderSystem::send_sms_textmagic('Your opt is : ' . $text, $phones, $is_unicode);
				} else {
					$res = TraderSystem::send_sms_thaibulksms('Your opt is : ' . $text, $phones, $is_unicode);
				}
			} elseif (API_SMS == 'textmagic') {
				$res = TraderSystem::send_sms_textmagic('Your opt is : ' . $text, $phones, $is_unicode);
			} else {
				$res = false;
			}
		}
		return $res;
	}
	
	function send_sms_thaibulksms($text, $phones, $is_unicode)
	{
		$res 		= array();
		$res['res'] = true;
			
		include MODPATHF . "tradersystem/thaibulksms/sms.class.php";
		$username 		= api_sms_thaibulk_user;///"0955204848";
		$password 		= api_sms_thaibulk_pwd;//"221671";
		$msisdn 		= $phones;//"0971299600";
		$message 		= $text;//"test thai bulk";
		$sender 		= api_sms_thaibulk_sender;//"THAIBULKSMS";
		$ScheduledDelivery = 0;
		$force 			= api_sms_thaibulk_force;//"standard";premium
		$sms 			= new sms();
		$result 		= $sms->send_sms($username, $password, $msisdn, $message, $sender, $ScheduledDelivery, $force);
		if ($result && isset($result['status']) && $result['status'] == true) {
			
		} else {
			$res['res'] = false;
			$msg 		= isset($result['msg']) ? $result['msg'] : 'ERROR';
		  	$res['message'] = $msg;
		}
		return $res;
	}
	
	function send_sms_textmagic($text, $phones, $is_unicode)
	{
		include MODPATHF . "tradersystem/textmagic/TextMagicAPI.php";
		$api = new TextMagicAPI(array(
		   	 	"username" => api_sms_textmagic_user,
		    	"password" => api_sms_textmagic_pwd, 
			));
		//$phones = '66971299600';
		$phones 	= array($phones);
		$text 		= $text;
		$is_unicode = true;
		$res 		= array();
		$res['res'] = true;
		try {
		  	$res['res'] 	= true;//$api->send($text, $phones, $is_unicode);
		} catch(Exception $e) {
			$res['res'] 	= false;
		  	$res['message'] = $e->getMessage();
		}
		return $res;
	}
    
    function send_sms_infobip($text, $phones)
    {
        include MODPATHF . "tradersystem/infobip/config.php";
        include MODPATHF . "tradersystem/infobip/fn_std.php";
        


        $res        = array();
        $res['res'] = true;
        try {
            $result     = send_sms(SENDER_NAME,$phones,$text,'');
            $result = json_decode(json_encode($result,true),true);
            
            if (isset($result['clientCorrelator']) && $result['clientCorrelator'] !='') {
                $res['res'] = true;
            } else {
                $res['res']     = false;
                $res['message'] = 'NOT FOUND.';
            }
        } catch(Exception $e) {
            $res['res']     = false;
            $res['message'] = $e->getMessage();
        }
        return $res;
    }
	/**
	 * (string )type = all,open,working
	 */
	function report_get_report_mt4($type = 'all',$login='')
	{
		$condi = '';
		if ($type == 'open') {
			$condi .= " AND c.cmd != 'OP_BALANCE' AND c.close_time =0 ";
		} elseif ($type == 'working') {
			$condi .= " AND c.cmd in('OP_BUY_LIMIT','OP_SELL_LIMIT') AND c.close_time =0 ";
		}
		$sql 	= "
				SELECT 
					FROM_UNIXTIME( c.close_time ) as viewclosetime, FROM_UNIXTIME( c.open_time ) as viewopentime, c.* 
				FROM 
					`bk_closed_ordermt4` c
				WHERE
					c.login = '" . $login . "' ".
					$condi.
					" 
				ORDER BY 
					c.order_id DESC
				LIMIT 0,20
			";
		$data			= Registry::get("Database")->fetch_all($sql);
		return ($data) ? $data : 0;
	}
	
	function report_numnet_by_datetime($by="DATE",$findtime,$acc='')
	{
		$aData = array();
		if (!$findtime) return $aData; 
		$condi = ($acc != '') ? " AND trader_acc_login='".$acc."' " : "";
		$sql = "
			SELECT 
			    balance,login
			FROM 
				bk_trade_acc_balance_history
			WHERE 
				login in (select  trader_acc_login from bk_trader_acc where uid='".$_SESSION['uid']."' ". $condi .") 
				AND ".$by."( FROM_UNIXTIME( close_time ) ) =".$by."( FROM_UNIXTIME( ".$findtime ." ) )
			ORDER BY
				close_time DESC
			";
		$aData['sql'] = $sql;
		$obj = Registry::get("Database")->fetch_all($sql);
		if ($obj) {
			$aNet = array();
			$net = 0;
			foreach ($obj as $k => $v) 
			{
				if (isset($aNet[$v->login])) {
					continue;
				} else {
					$aNet[$v->login] = $v->balance*1;
					$net = $net + $aNet[$v->login];
				}
				
			}
			$aData['net'] =  $net;
		}
		
		return $aData;
	}
	
	function report_numnet_by_datetime_admin($by="DATE",$findtime,$acc='')
	{
		$aData = array();
		if (!$findtime) return $aData; 
		$condi = ($acc != '') ? " AND trader_acc_login='".$acc."' " : "";
		$sql = "
			SELECT 
			    balance,login
			FROM 
				bk_trade_acc_balance_history
			WHERE 
				close_time > 0 ". $condi ."
				AND ".$by."( FROM_UNIXTIME( close_time ) ) =".$by."( FROM_UNIXTIME( ".$findtime ." ) )
			ORDER BY
				close_time DESC
			";
		$aData['sql'] = $sql;
		$obj = Registry::get("Database")->fetch_all($sql);
		if ($obj) {
			$aNet = array();
			$net = 0;
			foreach ($obj as $k => $v) 
			{
				if (isset($aNet[$v->login])) {
					continue;
				} else {
					$aNet[$v->login] = $v->balance*1;
					$net = $net + $aNet[$v->login];
				}
				
			}
			$aData['net'] =  $net;
		}
		
		return $aData;
	}
	
	function list_about_order_by_symbol()
	{
		$sql = "
		SELECT 
			*
		 FROM  
		 	bk_report_orderindex
		 WHERE 
		 	symbol !=''
		 	
		ORDER BY 
			sum_buy DESC
		";
		/*echo $sql;*/
		$obj = Registry::get("Database")->fetch_all($sql);
		return ($obj) ? $obj : 0;
	}
	
	function report_get_detail_admin()
	{
		date_default_timezone_set('UTC'); 
		
		$sql = "
		SELECT 
			*
		 FROM  
		 	bk_custom_detail
		 WHERE 
		 	id!=''
		";
		/*echo $sql;*/
		
		$obj = Registry::get("Database")->fetch_all($sql);
		if ($obj) {
			foreach ($obj as $k => $v) 
			{
				switch($v->fieldname)
				{
					case 'sum_account_trade_deposit':$aRes['deposit']= number_format($v->valuesis,2);break;
					case 'sum_account_trade_withdraw':$aRes['withdraw']= number_format($v->valuesis,2);break;
					case 'percent_trade_daily':$aRes['daily']= number_format($v->valuesis,2)."%";break;
					case 'percent_trade_month':$aRes['monly']= number_format($v->valuesis,2)."%";break;
					case 'sum_equity':$aRes['balnace']= number_format($v->valuesis,2);break;
					case 'sum_balance':$aRes['equity']= number_format($v->valuesis,2);break;
					case 'count_account_trade':$aRes['sumtrade']= number_format($v->valuesis,2);break;
					case 'count_order_trade':$aRes['sumorder']= number_format($v->valuesis,2);break;
				}
			}
		}
		return $aRes;
	}
    
	function report_get_detail()
	{
		date_default_timezone_set('UTC'); 
		$aData = array();	
		$acc = isset($_POST['acc']) ? $_POST['acc'] : '';
		$getMaxDate = $this->getdata("max(close_time)as close_time"," bk_trade_acc_balance_history",
					"where login in (select  trader_acc_login from bk_trader_acc where uid='".$_SESSION['uid']."' and trader_acc_login='".$acc."')");
		// DATE Max 30  mAY 2015
		//=== 1. หาวันที่ล่าสุด
		//echo "<br>1. DATE CLOSE LAST =".$getMaxDate['close_time'] . "[".date("Y-m-d H:i:s",$getMaxDate['close_time'])."]";
		if ($getMaxDate) {
			$aData['datemax'] = $getMaxDate['close_time'];
			$aData['datemax_day']  = date("Ymd",$getMaxDate['close_time']);
			$aData['datemax_day_timestamp'] = strtotime($aData['datemax_day']);
			
			
			$aData['datemax_mm'] = date("Y-m-01",$aData['datemax_day_timestamp']);
			//$aData['datemax_mm'] = 01 mAY 2015
			$aData['datemax_mm_timestamp'] = strtotime($aData['datemax_mm']);
			

			$aNow = $this->report_numnet_by_datetime("DATE",$aData['datemax'],$acc);
			//===2. วัน ณ ปัจจุบันมียอดเงิน เรียกว่า MAIN MONEY
			//echo "<br>2. DATE NOW money=".$aNow['net'];
			$aData['sqlnow'] = isset($aNow['sql']) ? $aNow['sql'] : '';
			$aData['now']  = isset($aNow['net']) ? $aNow['net'] : '';
			

			$getDaily= $this->getdata("max(close_time)as close_time","bk_trade_acc_balance_history",
				"where login in (select  trader_acc_login from bk_trader_acc where uid='".$_SESSION['uid']."' and trader_acc_login='".$acc."') and close_time < '".$aData['datemax_day_timestamp']."'");
			//===3. วันที่ก่อนหน้า
			//echo "<br>3. DATE DAY =".$getDaily['close_time']. "[".date("Y-m-d H:i:s",$getDaily['close_time'])."]";
			if ($getDaily) {
				$aData['datemax2'] = $getDaily['close_time'];
				
				$aDD = $this->report_numnet_by_datetime("DATE",$aData['datemax2'], $acc);
				//===4. วัน ณ ปัจจุบันมียอดเงิน เรียกว่า MAIN MONEY
			    //echo "<br>4. DATE DAY money==".$aDD['net'];
				$aData['sqlDD'] = isset($aDD['sql']) ? $aDD['sql'] : '';
				$aData['day']  = isset($aDD['net']) ? $aDD['net'] : '';
			} else {
				
			}
			
			$getMonly= $this->getdata("max(close_time)as close_time","bk_trade_acc_balance_history",
			"where login in (select  trader_acc_login from bk_trader_acc where uid='".$_SESSION['uid']."' and trader_acc_login='".$acc."') and close_time < '".$aData['datemax_mm_timestamp']."'",false);
			if (isset($getMonly['close_time'])) {
				
				$aData['MMmax2'] = $getMonly['close_time'];
				$aMM = $this->report_numnet_by_datetime("MONTH",$aData['MMmax2'], $acc);
				$aData['sqlMM'] = isset($aMM['sql']) ? $aMM['sql'] : '';
				$aData['month']  = isset($aMM['net']) ? $aMM['net'] : '';
			} else {
				$aData['month']  = 0;
			}
		} else  {
			
		}
		$now 		= isset($aData['now']) ? ($aData['now']*1) : 0;
		$day 		= isset($aData['day']) ? ($aData['day']*1) : 0;
		$month 		= isset($aData['month']) ? ($aData['month']*1) : 0;
		
		$aRes['daily']  = $this->format_percent($now,$day);
		$aRes['monly']  = $this->format_percent($now,$month);
		
		$aSum  			= $this->getdata("balance,equlity","bk_trader_acc",
							"where trader_acc_login in (select  trader_acc_login from bk_trader_acc where uid='".$_SESSION['uid']."'  and trader_acc_login='".$acc."' )"); 
		$aRes['balnace']= isset($aSum['balance']) ? number_format($aSum['balance'],2) : 0.00;
		$aRes['equity'] = isset($aSum['equlity']) ? number_format($aSum['equlity'],2) : 0.00;
		
		$aDepositWithdraw	= $this->getdata("sum(deposit) as deposit,sum(withdraw) as withdraw","bk_trade_acc_balance_history",
							"where login in (select  trader_acc_login from bk_trader_acc where uid='".$_SESSION['uid']."'  and trader_acc_login='".$acc."')"); 
		$aRes['deposit'] = isset($aDepositWithdraw['deposit']) ? number_format($aDepositWithdraw['deposit'],2) : 0.00;
		$aRes['withdraw'] = isset($aDepositWithdraw['withdraw']) ? number_format(-$aDepositWithdraw['withdraw'],2) : 0.00;

		/*echo "<pre>";print_r($aRes);*/
		return $aRes;
	}

	function format_percent($main,$secon)
	{
		if ($main == 0) {
			return "0.00%";
		}
		$cal = (($secon*1)*100)/($main*1);
		if ($cal > 100) {
			$per = $cal-100;
			return number_format($per,2)."%";
		} elseif($cal < 100){
			$per = 100-$cal;
			return "-".number_format($per,2)."%";
		} else {
			return "0.00%";
		}	
	}
	
	function report_data_builer_graph_admin($debug = false)
	{
	     date_default_timezone_set('UTC'); 
	
	    $condi = "";
	    if (isset($_POST['dtend'])  && $_POST['dtend'] != '') {
	    	$_POST['dtend']=str_replace("/","-", $_POST['dtend']);
	    	$dateend = strtotime($_POST['dtend']);
	    	$condi .= " AND c.date_ct <= ".$dateend;
	    }
		if (isset($_POST['dtstart'])  && $_POST['dtstart'] != '') {
	    	$datestart = strtotime($_POST['dtstart']);
	    	$condi .= " AND c.date_ct >= ".$datestart;
	    }
		 /*
		if (isset($_POST['acc'])  && $_POST['acc'] != '') {
	    	// TODO:: ตรวจว่า คนนนี้เป็นเต้าของ ฟแแนีืะ จิงไหม
	    	$condi .= " AND c.login = '".$_POST['acc']."'";
	    }
        */ 
		$sql = "
		SELECT 
			c.date_ct as close_time,c.*
		 FROM  
		 	bk_report_admin_daily c
		 WHERE 
		 	c.date_ct != '' ". $condi ."
		 	
		ORDER BY 
			c.date_ct
		";
		/*echo $sql;*/
		$obj = Registry::get("Database")->fetch_all($sql);
		$aData = array();
		$aDeposit 	= array();
		$aWithdraw 	= array();
		$aBalance 	= array();
		if ($obj) {
			
			foreach ($obj as $k => $v) 
			{
				$v->close_time = $v->close_time*1000;
				array_push($aBalance, array($v->close_time, $v->balance*1));
			}
			return array(
				
				array('key'	=> 'Balance', 'values' => $aBalance)
			);
		} else {
			return array();
		}
		
	}
	
	function report_data_builer_graph_user_new($debug = false)
	{
	    date_default_timezone_set('UTC'); 
	    $condi = "";
	    if (isset($_POST['dtend'])  && $_POST['dtend'] != '') {
	    	$_POST['dtend']=str_replace("/","-", $_POST['dtend']);
	    	$dateend = strtotime($_POST['dtend']);
	    	$condi .= " AND c.date_ct <= ".$dateend;
	    }
		if (isset($_POST['dtstart'])  && $_POST['dtstart'] != '') {
	    	$datestart = strtotime($_POST['dtstart']);
	    	$condi .= " AND c.date_ct >= ".$datestart;
	    }
		if (isset($_POST['acc'])  && $_POST['acc'] != '') {
	    	// TODO:: ตรวจว่า คนนนี้เป็นเต้าของ ฟแแนีืะ จิงไหม
	    	$condi .= " AND c.login = '".$_POST['acc']."'";
	    } else {
	    	$condi .= " AND c.login = 'AAAAAAAA'";
	    }

		$sql = "
		SELECT 
			c.*
		 FROM  
		 	bk_report_account_trade c
		 WHERE 
		 	c.date_ct != '' ". $condi ."
		 	
		ORDER BY 
			c.date_ct
		";
		/*echo $sql;*/
		$obj = Registry::get("Database")->fetch_all($sql);
		$aData = array();
		$aDeposit 	= array();
		$aWithdraw 	= array();
		$aBalance 	= array();
		if ($obj) {
			
			foreach ($obj as $k => $v) 
			{
				$v->date_ct = $v->date_ct*1000;
				array_push($aBalance, array($v->date_ct, $v->balance*1));
			}
			/*
			return array(
				array('key'	=> 'Deposits', 'bar' => true, 'values' => $aDeposit),
				array('key'	=> 'Withdrawals', 'bar' => true, 'values' => $aWithdraw),
				array('key'	=> 'Balance', 'values' => $aBalance)
			);
			 */
			 return array(
				array('key'	=> 'Balance', 'values' => $aBalance)
			);
		} else {
			return array();
		}
		
	}
	
	function report_data_builer_graph_user($debug = false)
	{
	    date_default_timezone_set('UTC'); 
	    $condi = "";
	    if (isset($_POST['dtend'])  && $_POST['dtend'] != '') {
	    	$_POST['dtend']=str_replace("/","-", $_POST['dtend']);
	    	$dateend = strtotime($_POST['dtend']);
	    	$condi .= " AND c.close_time <= ".$dateend;
	    }
		if (isset($_POST['dtstart'])  && $_POST['dtstart'] != '') {
	    	$datestart = strtotime($_POST['dtstart']);
	    	$condi .= " AND c.close_time >= ".$datestart;
	    }
		if (isset($_POST['acc'])  && $_POST['acc'] != '') {
	    	// TODO:: ตรวจว่า คนนนี้เป็นเต้าของ ฟแแนีืะ จิงไหม
	    	$condi .= " AND c.login = '".$_POST['acc']."'";
	    } else {
	    	$condi .= " AND c.login = 'AAAAAAAA'";
	    }

		$sql = "
		SELECT 
			c.*
		 FROM  
		 	bk_trade_acc_balance_history c
		 WHERE 
		 	c.close_time != '' ". $condi ."
		 	
		ORDER BY 
			c.close_time
		";
		/*echo $sql;*/
		$obj = Registry::get("Database")->fetch_all($sql);
		$aData = array();
		$aDeposit 	= array();
		$aWithdraw 	= array();
		$aBalance 	= array();
		if ($obj) {
			
			foreach ($obj as $k => $v) 
			{
				$v->close_time = $v->close_time*1000;
				array_push($aDeposit, array($v->close_time, $v->deposit*1));
				array_push($aWithdraw, array($v->close_time, $v->withdraw*1));
				array_push($aBalance, array($v->close_time, $v->balance*1));
			}
			/*
			return array(
				array('key'	=> 'Deposits', 'bar' => true, 'values' => $aDeposit),
				array('key'	=> 'Withdrawals', 'bar' => true, 'values' => $aWithdraw),
				array('key'	=> 'Balance', 'values' => $aBalance)
			);
			 */
			 return array(
				array('key'	=> 'Balance', 'values' => $aBalance)
			);
		} else {
			return array();
		}
		
	}
	
	function getdata( $colname, $tablename, $wherecondition = '', $debug = false)
	{
		$querystring = "SELECT $colname from $tablename $wherecondition";
		if($debug == true) { echo"<br>query=" . $querystring; }
    	$r1 = Registry::get("Database")->first($querystring);
		if (!$r1) {
			return false;
		}
   		$return_data = json_decode(json_encode($r1), true);
		return $return_data;
	}
	function check_admin_prolific(){
		/*$_SESSION['admin_secure']['admin_api_key']='680f502afd92949fda2ccd14dd73b4d3';*/
		if (empty($_SESSION['admin_secure']['admin_api_key'])) { 
			redirect_to(ADMINURL.'/index.php?do=login_broker');exit;
		}
	}
	function list_history($type="",$debug=false)
	{
		$condi  =' AND user_id='.$_SESSION['uid'];	
		if ($type == 'deposit' ) {
			$condi .= " AND money >0";
		} elseif($type == 'withdraw'){
			$condi .= " and money <0";
		} elseif($type =='internal') {
			$condi .= " AND trans_type!='wallet' ";
		}
		$sql = "SELECT * "
		  . "\n FROM bk_wallet_transaction"
		  . "\n WHERE trans_id != '' ".$condi
		  ;
		 if ($debug == true) echo $sql; 
  		$sql .= "\n ORDER BY trans_id DESC";
  		$row = Registry::get("Database")->fetch_all($sql);
  		return ($row) ? $row : 0;
	}
	
    function call_curl_get($fn='',$itype="GET",$var='') 
    {
		
        
		if (isset($_SESSION['admin_secure']['admin_api_key'])) {
			$api_key = $_SESSION['admin_secure']['admin_api_key'];
		} else {
			$api_key = $_SESSION['securekey'];
		}
		if ($var != '') {
			$var = "/".$var;
		}
        $url 		=URL_API.$fn.$var;
        
        // Open connection
        $ch = curl_init();
        
        // Set the url, number of GET vars, GET data
        curl_setopt($ch, CURLOPT_URL, $url);
       	curl_setopt($ch, CURLOPT_POST, false);
       	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8'));
       
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        // Execute request
        $result = curl_exec($ch);
        
        // Close connection
        curl_close($ch);
        
        return json_decode($result, true);
    }


	
    function getBalanceByLogin($login,$pid=1)
    {
    	if ($pid==2) {
    		$aRes = $this->call_curl_get("broke_get_account_ct","GET",$login."?cmd=broke_get_account_ct");
    	} else {
    		$aRes = $this->call_curl_get("broke_get_account","GET",$login."?cmd=broke_get_account");
    	}
    	
		
    	if ($aRes['isSuccess'] == 1 && isset($aRes['msg']['EQUITY'])) {
    		$b = $aRes['msg']['EQUITY']*1;
			return $b;
    	} elseif ($aRes['isSuccess'] == 0 ) {
    		$aRes['message'] = $aRes['errMessage'];
    		return $aRes;
    	} else {
    		return 0;
    	}
    }
	
	function trader_getlevel($group = false,$isType='mt4')
	{
		if ($group == false) return '500'; 

		if ($isType == 'mt4') {
			$aLevel =  TraderSystem::getdata("mt4_group_micro,mt4_group_mini,mt4_group_standard,mt4_group_pro","acms_admin_config","WHERE id!=''");
			if (preg_match("/".$group."/i", $aLevel['mt4_group_micro'])) {
				$val = $aLevel['mt4_group_micro'];
			} elseif (preg_match("/".$group."/i", $aLevel['mt4_group_mini'])) {
				$val = $aLevel['mt4_group_mini'];
			} elseif (preg_match("/".$group."/i", $aLevel['mt4_group_standard'])) {
				$val = $aLevel['mt4_group_standard'];
			} elseif (preg_match("/".$group."/i", $aLevel['mt4_group_pro'])) {
				$val = $aLevel['mt4_group_pro'];
			}
			if (isset($val)) {
				$a = explode(",",$val);
				if (isset($a[2])) {
					return $a[2];
				}
			}
		}
		return '500';
		
	}
	// ตรวจสอบว่า จะ lock field เมื่อไหร่
	/**
	 * return 1=lock field,0=edit ready
	 */ 
	function chk_verify()
	{
		$usr_dtl 			= $_SESSION['usr_dtl']['user_info'];		
		if ($usr_dtl['id_card_verified'] == 1 || $usr_dtl['address_verified']==1 || $_SESSION['bank_verified'] == 1) {
			return 1;
		} else {
			return 0;
		}
	}

	function check_premission_traderroom()
	{
		if (isset($_SESSION['securekey']) && ($_SESSION['securekey'] != '')) {
			return 1;
		} else {
			return 0;
		}
	}
    function list_select_option_by_array($aList,$current_value,$id="id",$name="c_name",$ext_list="")
    {
		$sx ="";
		if ($ext_list != "") {
            if ($current_value == $ext_list) $s1 = "selected";
            else $s1 = "";
           $sx .="<option value=\"\" $s1>$ext_list</option>\n";
        }
		$ischk = '';  
        foreach ($aList as $k => $v) {
			if ($v[$id] == $current_value) {
				$ischk = 'OK';
           		$s1 ="selected";
          	} else {
          		$s1 ="";
			}
          	$sx .= "<option value=\"" . $v[$id] . "\" $s1>" . $v[$name] . "</option>\n";
        }
		if ($current_value != '' && $ischk!='OK') {
			 $sx .= "<option value=\"" . $current_value . "\" selected>" . $current_value. "</option>\n";
		}
        return $sx;
    }
	
	function list_select_option_by_table($table, $current_value, $id = "id", $name = "c_name", $condition = "", $ext_list = "", $fn = "", $style = "")
	{   
		$sx ="";
		if ($condition != "") $condition = "  $condition";
		if ($ext_list != "")
		{
			if ($current_value == $ext_list) $s1 = "selected";
			else $s1 = "";
			$sx .="<option value=\"\" $s1>$ext_list</option>\n";
		}
		$sql = "select * from $table $condition";
		$row = Registry::get("Database")->fetch_all($sql);
		if ($row) {
			foreach ($row as $k => $v) {
				if ($v->$id == $current_value) $s1 ="selected";
				else $s1 ="";
				$sx .= "<option value=\"" . $v->$id . "\" $s1>" . $v->$name . "</option>\n";
			}
		} else {
		}
		$sx .="</select>\n";
		return $sx;
	}
	function getmaillist($typelist)
	{
		$aRes = array();
		$sql = "select * from bk_mail where group_mail like '%".$typelist."%'";
		$row = Registry::get("Database")->fetch_all($sql);
		if ($row) {
			foreach ($row as $k => $v) {
				$aRes[$v->email] = $v->email_name;
			}
		} else {
			$aRes['darawantaorong@gmail.com'] = 'darawan';
		}
		return $aRes;
			
	}
	function list_account_trade()
	{	
		$txt 		= "";	
		$listAcc 	= $_SESSION['usr_dtl']['trade_acc_list'];
		if (count($listAcc) > 0) {
			$listmt4 = $listmt4_demo = $listct = $listct_demo= '';
			foreach ($listAcc as $k => $v) {
				if ($v['trader_product_type'] == 1) {
					if($v['is_demo'] == 0) {$listmt4 .="MT4:".$v['trader_acc_login']."<br>";} else {$listmt4_demo .= "MT4DEMO:".$v['trader_acc_login']."<br>";}
				} elseif($v['trader_product_type'] == 2) {
					if($v['is_demo'] == 0) {$listct .="MT4:".$v['trader_acc_login']."<br>";} else {$listct_demo .= "MT4DEMO:".$v['trader_acc_login']."<br>";}
				}
			}
			if ($listmt4 != '') {
				$txt .="<br>---- LIST MT4 ACCOUNT ---<br>".$listmt4;
			}
			if ($listmt4_demo != '') {
				$txt .="<br>---- LIST MT4 DEMO ACCOUNT ---<br>".$listmt4_demo;
			}
			if ($listct != '') {
				$txt .="<br>---- LIST CTrade ACCOUNT ---<br>".$listct;
			}
			if ($listct_demo != '') {
				$txt .="<br>---- LIST CTrade DEMO ACCOUNT ---<br>".$listct_demo;
			}
		} else {
			$txt = "NO DATA";
		}
		return $txt;
	}
	function send_mail($itypemail,$mailid,$aParam=array()) 
	{
		require_once (BASEPATH . "lib/class_mailer.php");
		$aMail 		= TraderSystem::getdata("*", "email_templates", "where id=" . $mailid);
		if ($itypemail == 'deposit') {
			$aMail_list = TraderSystem::getmaillist("deposit");
			$moneylocal = '';
		 	if (isset($aParam['curency']) && $aParam['curency'] != '') {
				$moneylocal = "Amount LOCAL ".$aParam['money_local'] . " ".$aParam['curency'] ;
			}
			$body 		= str_replace(
			array('[NAME]', '[AMOUNTUSD]','[AMOUNTLOCAL]','[BANKNAME]','[TRANSFERDATE]', '[DATESAVE]','[INFO]', '[SITENAME]', '[URL]'), 
			array($_SESSION['name'],  $aParam['money'], $moneylocal,  $aParam['bank_code'],  $aParam['trans_date'] ,date("d-M-Y h:i:sa") ,$aParam['info'],$_SERVER['HTTP_HOST'],SITEURL)
			, $aMail['body_en']);
			
		} elseif($itypemail == 'optsend') {
			$aMail_list = TraderSystem::getmaillist("deposit");
			$aMail_list[$_SESSION['email']] =$_SESSION['name'];
			$body 		= str_replace(
			array('[NAME]', '[OTPKEY]','[OTPCODE]', '[SITE_NAME]', '[URL]', '[IP]'), 
			array($_SESSION['name'],  $aParam['key_token'],  $aParam['key_code'], Registry::get("Core")->site_name ,$_SERVER['HTTP_HOST'],$_SERVER['REMOTE_ADDR'])
			, $aMail['body_en']);
		} elseif($itypemail=='withdraw'){
			$aMail_list = TraderSystem::getmaillist("withdraw");
			$moneylocal = '';
		 	if (isset($aParam['curency']) && $aParam['curency'] != '') {
				$moneylocal = "Amount LOCAL ".$aParam['money_local'] . " ".$aParam['curency'] ;
			}
			$acctrader	= TraderSystem::list_account_trade(); 
			$aMail_list[Registry::get("Core")->site_email] = Registry::get("Core")->site_name;
			$timesave 	= date("Y-m-d H:i:s");
			$body 		= str_replace(
						array('[NAME]', '[MEMBERID]','[PHONE]','[AMOUNTUSD]','[AMOUNTLOCAL]','[BANKNAME]', '[EMAIL]','[ACCOUNTTRADER]', '[DATESAVE]','[SITE_NAME]','[URL]'), 
						array( $_SESSION['name'],  $_SESSION['uid'],$_SESSION['usr_dtl']['user_info']['mobile'],  -$aParam['money'],$moneylocal,  $aParam['deposit_comment'],  $_SESSION['email'],$acctrader,$aDtl['email'], $timesave,$_SERVER['HTTP_HOST'],SITEURL)
					  , $aMail['body_en']);
		} elseif($itypemail=='document_cancel'){
			$aMail_list[$aParam['email']] = $aParam['fname'];
			$timesave 	= date("Y-m-d H:i:s");
			$body 		= str_replace(
						array('[NAME]', '[DOCUMENTNAME]','[SITE_NAME]','[URL]'), 
						array( $aParam['fname'],  $aParam['documentname'],$_SERVER['HTTP_HOST'],SITEURL)
					  , $aMail['body_en']);
		} elseif($itypemail == 'admin_approve_deposit') {
			$aMail_list[$aParam['email']] = $aParam['fname'];
			$moneylocal = '';
			$fixdisplay = 'none;';
			if (isset($aParam['curency']) && $aParam['curency'] != '') {
				$moneylocal = $aParam['money_local'] . " ".$aParam['curency'] ;
				$fixdisplay = ";";
			}
			$timesave 	= date("Y-m-d H:i:s");
			$body 		= str_replace(
						array('[NAME]', '[AMOUNTUSD]','#FIXDISPLAYNONE#','[AMOUNTTHB]','[BANKNAME]','[TRANSDATE]','[SITE_NAME]','[URL]'), 
						array( $aParam['fname'],  $aParam['money'],$fixdisplay,$moneylocal,$aParam['bank_code'],$aParam['trans_date'],$_SERVER['HTTP_HOST'],SITEURL)
					  , $aMail['body_en']);
		} elseif ($itypemail == 'admin_approve_withdraw'){
			$aMail_list[$aParam['email']] = $aParam['fname'];
			$moneylocal = '';
			$fixdisplay = 'none;';
			if (isset($aParam['curency']) && $aParam['curency'] != '') {
				$moneylocal = $aParam['money_local'] . " ".$aParam['curency'] ;
				$fixdisplay = ";";
			}
			$timesave 	= date("Y-m-d H:i:s");
			$aParam['money'] = -$aParam['money'];
			$body 		= str_replace(
						array('[NAME]', '[AMOUNTUSD]','#FIXDISPLAYNONE#','[AMOUNTTHB]','[BANKNAME]','[FULLNAME]','[SAVEDATE]','[COMMENT]','[SITE_NAME]','[URL]'), 
						array( $aParam['fname'],  $aParam['money'],$fixdisplay,$moneylocal,$aParam['deposit_comment'],$aParam['fname']." ".$aParam['lname'],$aParam['save_date'],$aParam['comment'],$_SERVER['HTTP_HOST'],SITEURL)
					  , $aMail['body_en']);
		} elseif ($itypemail == 'membersenddoc') {
			$aMail_list[$aParam['tomail']] = $aParam['toname'];
			$body 		= str_replace(
						array('[NAME]', '[MEMBERNAME]','[MEMBERID]','[MEMBEREMAIL]','[DOCUMENTTYPE]','[SITE_NAME]','[URL]'), 
						array( $aParam['toname'], $aParam['nameuser'], $aParam['iduser'],$aParam['emailuser'],$aParam['typedoc'],$_SERVER['HTTP_HOST'],SITEURL)
					  , $aMail['body_en']);
		}
	
		$newbody 	= cleanOut($body);
		$mailer 	= Mailer::sendMail();
		
		$message 	= Swift_Message::newInstance()
						->setSubject($aMail['subject_en'])
						->setTo($aMail_list)
						->setFrom(array(Registry::get("Core")->site_email => Registry::get("Core")->site_name))
						->setBody($newbody, 'text/html');
		$mailer->send($message);	
	}
	/**
	 * used admin approve withdraw
	 */
	function update_transaction_withdraw()
	{
		Filter::checkPost('change_status', Lang::$word->_SYS_DBSTATUS);
		Filter::checkPost('trans_id', Lang::$word->_TRANSECTION_NUMBER);
		if (empty(Filter::$msgs)) {
			$aData = TraderSystem::getdata("b.*,u.email,u.fname,u.lname","bk_wallet_transaction b,users u","where b.user_id=u.id and b.trans_id='".$_POST['trans_id']."' ");
			if ($aData['status'] == 0) {
				$fup['comment'] = $_POST['comment'];
				$fup['status'] 	= $_POST['change_status'];
				if ($_POST['change_status'] == 2) {
					$aData['money'] = $aData['money']*1;
					$a 				= TraderSystem::call_curl('admin_wallet/'.$aData['email'],$itype="POST",array(
										'cmd'=>'wallet','amount' => -$aData['money'],'corr_code'=>2,'tid'=>$_POST['trans_id'],'comment'=>'REFUND MONEY: '.$aData['trans_id']));
					
					if (isset($a['isSuccess']) && $a['isSuccess'] == 1) {
						$fup['comment'] = $fup['comment'] . " " . json_encode($a['message']);
						$res = self::$db->update("bk_wallet_transaction", $fup, "trans_id='" . $aData['trans_id'] . "'");
						$json['status'] 		= 'success';
						$json['message'] 	= Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
					} else {
						$json['status'] 		= 'error';
						$json['message'] 	= Filter::msgError('ERROR REFUND TO WALLET',false);
					}
				} elseif ($_POST['change_status'] == 1) {
					$res = self::$db->update("bk_wallet_transaction", $fup, "trans_id='" . $aData['trans_id'] . "'");
					/**** sendmail ******/
					$aData['comment'] = $_POST['comment'];
					$res =  TraderSystem::send_mail("admin_approve_withdraw",28,$aData);
					$json['status'] 		= 'success';
					$json['message'] 	= Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
				}
			} else{
				$json['status'] 	= 'info';
				$json['message'] 	= Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			}
		} else {
			$json['message'] = Filter::msgStatus();
		}
		return $json;
	}
	function check_file_document($filename)
	{
		if (is_file(UPLOADS . 'file_doc/' . $filename)) {
			return UPLOADURL . 'file_doc/' . $filename;
		} else {
			return "no";
		}
	}

	function get_user_info_by_mail()
	{
		Filter::checkPost('email',"DATA NOT FOUND");
		Filter::checkPost('itype',"DATA NOT FOUND");
		Filter::checkPost('filename',"DATA FILENAME NOT FOUND");
		if (empty(Filter::$msgs)) {
			$aData 	= TraderSystem::call_curl_get("admin_ducument_user_byfile/".$_POST['email']."/".$_POST['itype']."/".$_POST['filename']."?cmd=admin_ducument_user_byfile","GET");
			$aDb 	= TraderSystem::getdata("d.id,d.uid,d.is_verified,u.fname,u.lname","bk_document_user d, users u",
					" where u.email = '".$_POST['email']."' and d.doc_type='".$_POST['itype']."' and d.data like '%\"file_url\":\"".$_POST['filename']."\"%'");
			
			if ($aData['isSuccess'] ==1 ) {
				$aDtlData 			= json_decode($aData['message'][0]['data']);
				$json['status'] 	= 'success';
					$json['file_url'] 		= TraderSystem::check_file_document($aDtlData->file_url);
					$json['detail'] 		= $aDtlData;
					$json['detail']->fnamelname = $aDb['fname']." ".$aDb['lname'];
					$json['detail']->api_id 	= $aData['message'][0]['id'];
					$json['detail']->client_id 	= $aDb['id'];
					if ($aData['message'][0]['is_verified'] != $aDb['is_verified'] ){
						$last_id = self::$db->update("bk_document_user", array('is_verified'=>0) , "uid='" . $aDb['id']."'");
					} 
				
			} else {
				$resclear =  self::$db->delete("bk_document_user", "uid='" . $aDb['uid']."' and id='".$aDb['id']."'");
				/*$json['isRedirect'] = 'yes';*/
				$json['returnapi'] =$aData;
				$json['status'] 	= 'success';
				$json['message'] 	= Filter::msgError($aData['message'],false);
			}
		} else {
			$json['message'] = Filter::msgStatus();
		}
		return $json;
	}
	
	function get_user_info_by_mail_bak()
	{
		Filter::checkPost('email',"DATA NOT FOUND");
		Filter::checkPost('itype',"DATA NOT FOUND");
		
		if (empty(Filter::$msgs)) {
			$aData 	= TraderSystem::call_curl_get("admin_user_info/".$_POST['email']."?cmd=user_info","GET");
			$aDb 	= TraderSystem::getdata("*","users"," where email = '".$_POST['email']."'");
			if ($aData['isSuccess']==1) {
				$json['status'] 	= 'success';
				$json['api_id']        =   $aData['message']['id'];
				if ($_POST['itype'] == 'card') {
					$json['file_url'] 		= TraderSystem::check_file_document($aData['message']['id_card_file_url']);
					$json['is_verify'] 		=  $aData['message']['id_card_verified'];
					$json['id_card_data'] 	=  $aData['message']['id_card_data'];
                    $json['date_of_birth'] 	=  isset($aData['message']['date_of_brith']) ?date("d-M-Y",strtotime($aData['message']['date_of_brith'])) : "-";
					if ($aData['message']['id_card_file_url'] != $aDb['id_card_file_url'] || $aData['message']['id_card_verified']!=$aDb['id_card_verified'] ){
						$last_id = self::$db->update("users", array('id_card_file_url'=>$aData['message']['id_card_file_url'],'id_card_verified'=>$aData['message']['id_card_verified']) , "email='" . $_POST['email']."'");
					} 
				}elseif($_POST['itype'] == 'addr'){
					$contry = ( $aDb['country'] != '') ? TraderSystem::getdata("*","bk_country_code","where country_abb='".$aDb['country'] ."'"):'';
					$contry = ($contry != '') ? $contry['country'] : '';
					$json['address_1'] 		=  $aData['message']['address_1'] . ' ' . $aData['message']['address_2'] 
											. ' '. $aData['message']['address_city'] . ' ' .$aData['message']['address_postcode'] .' ' . $contry ;
					
					$json['file_url'] 		= TraderSystem::check_file_document($aData['message']['address_file_url']);
					$json['is_verify'] 		=  $aData['message']['address_verified'];
					if ($aData['message']['address_file_url'] != $aDb['address_file_url'] || $aData['message']['address_verified']!=$aDb['address_verified'] ){
						$last_id = self::$db->update("users", array('address_file_url'=>$aData['message']['address_file_url'],'address_verified'=>$aData['message']['address_verified']) , "email='" . $_POST['email']."'");
					} 
				}
			} else {
				$json['status'] 	= 'error';
				$json['message'] 	= Filter::msgError($aData['message'],false);
			}
		} else {
			$json['message'] = Filter::msgStatus();
		}
		return $json;
	}

	function update_status_document_new()
	{
		
		Filter::checkPost('change_status', Lang::$word->_SYS_DBSTATUS);
		Filter::checkPost('itype', "DATA ITYPE NOT FOUND");
		Filter::checkPost('api_id',"DATA PKID NOT FOUND");
		Filter::checkPost('client_id',"DATA PKID NOT FOUND");
		Filter::checkPost('email',"DATA EMAIL NOT FOUND");
		if (empty(Filter::$msgs)) {
			if ($_POST['change_status'] == 1 || $_POST['change_status'] ==2) {
			
				$aData = TraderSystem::call_curl("admin_verified_document_user","POST",array('cmd'=>'document_user','id'=>$_POST['api_id'],'is_verified'=>$_POST['change_status']));
				if ($aData['isSuccess']==1) {
					$aUser 	= TraderSystem::getdata("d.id,d.uid,d.is_verified,u.fname,u.lname,u.email","bk_document_user d, users u",
					" where u.email = '".$_POST['email']."' and d.doc_type='".$_POST['itype']."' and d.id ='".$_POST['client_id']."'");
                    
					$json['status'] 		= 'success';
					$json['message'] 	= Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
					if ($_POST['change_status'] ==2) {
						
						switch($_POST['itype']){
							case 'bookbank' : $aUser['documentname'] = 'Bank Document'; break;
							
						}
						$res =  TraderSystem::send_mail("document_cancel",22,$aUser);
						//=== start delete file document
						$sql = "select data from bk_document_user where uid='".$aUser['id']."' and doc_type='".$_POST['itype']."' and id='".$_POST['client_id']."'";
						$row = Registry::get("Database")->fetch_all($sql);
						if ($row) {
							foreach ($row as $k => $v) {
								$aDtl = json_decode($v->data);
								if (is_file(UPLOADS . 'file_doc/' . $aDtl->file_url)) {@unlink(UPLOADS . 'file_doc/' . $aDtl->file_url);}
							}
						}
						//=== end delete file document
						$resclear =  self::$db->delete("bk_document_user", "uid='" . $aUser['uid']."' and id='".$_POST['client_id']."'");
					} else {
						$last_id = self::$db->update("bk_document_user", array('is_verified'=>1) , "uid='" . $aUser['uid']."' and id='".$_POST['client_id']."'");
					}
							
				} else {
					$json['status'] 	= 'error';
					$json['message'] 	= Filter::msgError('ERROR UPDATE DATA',false);
				}
			}else {
				
				$json['message'] 	= Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
				
			}
			/**/
		} else {
			$json['message'] = Filter::msgStatus();
		}
		return $json;
	}
	function update_status_document()
	{
		
		Filter::checkPost('change_status', Lang::$word->_SYS_DBSTATUS);
		Filter::checkPost('itype', "NOT FOUND DOCUMENT TYPE");
       	Filter::checkPost('api_id',"NOT FOUND DOCUMENT");
		Filter::checkPost('email',"DATA EMAIL NOT FOUND");
		
		if (empty(Filter::$msgs)) {
			    
			if ($_POST['change_status'] == 1 || $_POST['change_status'] == 2) {
				$aData = TraderSystem::call_curl("admin_verified_document_user_new","POST",array('cmd'=>'document_user','id'=>$_POST['api_id'],'is_verified'=>$_POST['change_status'],'doc_type'=>$_POST['itype']));
				
				if ($aData['isSuccess']==1) {
					$aUser 			=  TraderSystem::getdata("id_card_file_url, address_file_url, fname,email,id","users","where email='".$_POST['email']."'");
					$json['status'] = 'success';
					$json['message']= Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
                    
					if ($_POST['change_status'] == 2) {
						switch($_POST['itype']) {
                            case 'card' : $aUser['documentname'] = 'Personal ID Document '; break;
                         	case 'addr' : $aUser['documentname'] = 'Personal Address Document'; break;
						}
						$res =  TraderSystem::send_mail("document_cancel",22,$aUser);
						if ($_POST['itype'] == 'card') {
						    $del_file 	= $aUser['id_card_file_url'];
							$last_id 	= self::$db->update("users", array('id_card_file_url'=>'','id_card_verified'=>0) , "email='" . $_POST['email']."'");
							
						} else if ($_POST['itype'] == 'addr') {
						    $del_file 	= $aUser['address_file_url'];
							$last_id 	= self::$db->update("users",array('address_file_url'=>'','address_verified'=>0)  , "email='" . $_POST['email']."'");
						} 
						if (is_file(UPLOADS . 'file_doc/' . $del_file))
						{
							@unlink(UPLOADS . 'file_doc/' . $del_file);
						}
                   } else {
                     if ($_POST['itype'] == 'card') {
						  $last_id = self::$db->update("users", array('id_card_verified'=>1) , "id='" . $aUser['id']."' ");
                     } else if ($_POST['itype'] == 'addr') {
                          $last_id = self::$db->update("users", array('address_verified'=>1) , "id='" . $aUser['id']."' ");
                     }
					
					}
							
				} else {
					$json['status'] 	= 'error';
					$json['message'] 	= Filter::msgError('ERROR UPDATE DATA',false);
				}
			} else {
				
				$json['message'] 	= Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
				
			}
			/**/
		} else {
			$json['message'] = Filter::msgStatus();
		}
		return $json;
	}
/**
 * DESC: admin ใช้ approve deposit
 * $aData['status'] == 0 : ยยังไม่รู้ว่าใช้ตอนไหน
 */
	function update_transaction_deposit()
	{
		Filter::checkPost('change_status', Lang::$word->_SYS_DBSTATUS);
		Filter::checkPost('trans_id', Lang::$word->_TRANSECTION_NUMBER);
		if (empty(Filter::$msgs)) {
			$aData = TraderSystem::getdata("b.*,u.email,u.fname,u.lname","bk_wallet_transaction b,users u","where b.user_id=u.id and b.trans_id='".$_POST['trans_id']."' ");
			$fup['comment'] = $_POST['comment'];
			if ($aData['status'] == 0) {
				
				$fup['status'] 	= $_POST['change_status'];
				if ($_POST['change_status'] == 1) {
					$fup['status'] 	= 1;
					
					$a 	= TraderSystem::call_curl('admin_wallet/'.$aData['email'],$itype="POST",array(
						'cmd'=>'wallet','amount' => $aData['money'],'corr_code'=>2,'tid'=>$_POST['trans_id'],'comment'=>'REF TRANS ID : '.$aData['trans_id']));
					if (isset($a['isSuccess']) && $a['isSuccess'] == 1) {
						$fup['comment'] = $fup['comment'] . " " . json_encode($a['message']);
						if ($aData['trans_type'] != 'wallet' ) {
							$fadd 				= $aData;
							$fadd['status'] 	= 4;
							
							$fadd['deposit_comment'] = "REF ID:".$aData['trans_id'];
							unset($fadd['trans_id']);
							unset($fadd['email']);
							/*echo "<pre>";print_r($fadd);*/
							$last_id = self::$db->insert("bk_wallet_transaction", $fadd);
							if (self::$db->affected()) {
								$json['status'] 		= 'success';
								$json['message'] 	= Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
							} else {
								$json['status'] 		= 'error';
								$json['message'] 	= Filter::msgError('ERROR WALLET DEPOSIT WAITING TO TRADE',false);
							}
						}
						//TODO:: ต้องส่งเมลล์ด้วย
						$res = self::$db->update("bk_wallet_transaction", $fup, "trans_id='" . $aData['trans_id'] . "'");
					} else {
						$json['status'] 		= 'error';
						$json['message'] 	= Filter::msgError('ERROR ADD TO WALLET',false);
					}
					
				} else {
					$json['status'] 		= 'success';
					$json['message'] 	= Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
					$res = self::$db->update("bk_wallet_transaction", $fup, "trans_id='" . $aData['trans_id'] . "'");
				}
			}  else if ($aData['status'] == 10 && $_POST['change_status'] == 11 && $aData['trans_type'] == "wallet") {
				/*
				 * $aData['status'] = 10=pending
				 * $_POST['change_status']  = 11 = admin change approve
				 */
			    // deposit to wallet [10 pending admin approve => 11 admin approve]
			   
			    $a 				= TraderSystem::call_curl('admin_wallet/'.$aData['email'],$itype="POST",array(
										'cmd'=>'wallet','amount' => $aData['money'],'corr_code'=>2,'tid'=>$_POST['trans_id'],'comment'=>'REF TRANS ID : '.$aData['trans_id']));
				if (isset($a['isSuccess']) && $a['isSuccess'] == 1) {
					$fup['comment'] = $_POST['comment'] . " " . json_encode($a['message']);
					$fup['status']  = 1;
				} else {
					$fup['status']  = 0;
					$fup['comment'] = $_POST['comment'] . " " . json_encode($a['message']);
				}
                
             	$res = self::$db->update("bk_wallet_transaction", $fup, "trans_id='" . $aData['trans_id'] . "'"); 
                if ($res) {
                    $json['status']         = 'success';
                  $json['message']    = Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
				
					$res =  TraderSystem::send_mail("admin_approve_deposit",27,$aData);
                }  
				/*
                $fup['status']  = 1;
				$res = self::$db->update("bk_wallet_transaction", $fup, "trans_id='" . $aData['trans_id'] . "'"); 
                if ($res) {
                    $json['status']         = 'success';
                  	$json['message']    = Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
					
					$res =  TraderSystem::send_mail("admin_approve_deposit",27,$aData);
                }  
				 **/
                   
            } else if ($aData['status'] == 10) {
			    
                $fup['status']  = $_POST['change_status'];
             	$res = self::$db->update("bk_wallet_transaction", $fup, "trans_id='" . $aData['trans_id'] . "'"); 
                if ($res) {
                    $json['status']         = 'success';
                    $json['message']    = Filter::msgOk(Lang::$word->_UA_UPDATEOK,false);
					$res =  TraderSystem::send_mail("admin_approve_deposit",27,$aData);
                }     
                    
            }  else {
				 $json['status'] 	= 'info';
				 $json['message'] 	= Filter::msgAlert(Lang::$word->_SYSTEM_PROCCESS, false);
			}
		} else {
			$json['message'] = Filter::msgStatus();
		}
		return $json;
		
	}
	function gen_bank_user()
	{
		$aRes = array();
		if (isset($_SESSION['usr_dtl']['user_bank_list'][$_POST['bank_id']])) {
			$bankDtl = $_SESSION['usr_dtl']['user_bank_list'][$_POST['bank_id']];
			$aDtl = json_decode($bankDtl['data']);
			$aRes['data']= "BANKNAME : ".$aDtl->bank_name."<br>
					Account Number : ".$aDtl->bank_account_number;
			$aRes['bank_detail'] = $aDtl;
		} else {
			$aRes['data']= false;
		}
		return $aRes;
	}
	function gen_bank_user_bak()
	{
		$userdtl = $_SESSION['usr_dtl']['user_info'];
		$gentext = "
		BANKNAME : ".$userdtl['bank_name']."<br>
		Account Number : ".$userdtl['bank_account_number'];
		return $gentext;
	}
	
	function check_verify_otp()
	{
		
		if (isset($_SESSION['adminconf']['transfer_internal_otp']) && $_SESSION['adminconf']['transfer_internal_otp'] == 1) {
			$aRes = TraderSystem::get_secure_personal();
			return $aRes;
		} else {
			return 0;
		}	
	}
	
	function gen_detail_transfer_internal()
	{
		if ($_POST['itype'] == 'deposit') {
			$amount = "<font color='green'>".$_POST['amount']."</font>";
			$txtFrm = "Transfer From";
			$txtTo = "Transfer To";
		} else {
			$amount = "<font color='red'>".$_POST['amount']."</font>";
			$txtFrm = "Transfer To";
			$txtTo = "Transfer From";
		}

		$txt ='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">From Account</label>';	
		$txt .='<div class="col-md-8"><p>'.$_SESSION['name'].'</p></div></div>';
		
		$txt .='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">Amount</label>';	
		$txt .='<div class="col-md-8"><p>'.$amount.' USD</p></div></div>';	
		
		$txt .='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">Transfer On</label>';	
		$txt .='<div class="col-md-8"><p>'.date('F j, Y').'</p></div></div>';	
		
		$txt .='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">'.$txtFrm.' </label>';	
		$txt .='<div class="col-md-8"><p>Wallet</p></div></div>';		
		
		$txt .='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">'.$txtTo.'</label>';	
		$txt .='<div class="col-md-8"><p>'.$_POST['trans_type'].'</p></div></div>';		
			
		return $txt;
	}
	function check_list_pending()
	{
		$aList = TraderSystem::getdata("count(trans_id) as listnet","bk_wallet_transaction","where user_id='".$_SESSION['uid']."' and status in (0,11) and gateway_dtl='internal_transfer'");
		
		if (isset($aList['listnet'])) {
			$net = $aList['listnet']*1;
			if ($net > 0) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}
	function save_transaction()
	{
		Filter::checkPost('bank_code', Lang::$word->_BANK_NAME);
		Filter::checkPost('trans_type', Lang::$word->_SELECT_ACCOUNT_TO_DEPOSIT);
		$_POST['amount'] = str_replace(",", "", $_POST['amount']);
		$money = $_POST['amount'] *1 ;
		if ($money == 0) {
			 Filter::$msgs['amount'] = Lang::$word->_TR_AMOUNT;
		}

		if ($_POST['itype'] == 'deposit') {
			$money_all = ($_SESSION['usr_dtl']['wallet_amt']['amount']*1);
			if ($money > $money_all) {
				Filter::$msgs['amount'] = Lang::$word->_MONEY_NOT_ENAOUGH;
			}
			$money = -$money;
			$comment = "Deposit to:". $_POST['trans_type'];
			$fin['gateway_code'] 		= '007';
			$fin['gateway_note'] 		= 'TF-WL-TA';
		} elseif ($_POST['itype'] == 'withdraw') {
			$money = $money;
			$comment = "Withdrawal from:". $_POST['trans_type'];
			$fin['gateway_code'] 		= '006';
			$fin['gateway_note'] 		= 'TF-TA-WL';
		} elseif ($_POST['itype'] == 'deposittradetoway') {
			$money = -$money;
			$comment = "Deposit to:". $_POST['trans_type'];
			$fin['gateway_code'] 		= '006';
			$fin['gateway_note'] 		= 'TF-TA-WL';
		} else {
			Filter::$msgs['operation'] = "NOT OPERATION";
		}
		if (!isset($_SESSION['uid'])) {
			 /*echo "<pre>";print_r($_SESSION);*/
			 Filter::$msgs['users'] = Lang::$word->_UR_USERNOAVAIL;
		}
		$aCheckVerify = TraderSystem::check_verify_otp();
		$isListPending = TraderSystem::check_list_pending();
		if ($isListPending == 1) {
			 Filter::$msgs['listpending'] = 'Please your waiting some processing.';
		}
		if ($aCheckVerify != 0) {
			if ($aCheckVerify['step'] == 'no') {
				Filter::$msgs['ss_setting'] = Lang::$word->_SECURITY_SETTINGS;
				$json['message'] = Filter::msgStatus_gob();
			} else {
				$fin['status'] = 3;
				$json['dtl'] = $aCheckVerify;
				
				$json['message_dtl'] = TraderSystem::gen_detail_transfer_internal();
			}
		} else {
			$fin['status'] = 0;
		}
		
		if (empty(Filter::$msgs)) {
			
			
			$fin['user_id'] 	= $_SESSION['uid'];
			$fin['money'] 		= $money;
			//$fin['money_total'] = $_POST['wallet_total'] + $money;
            $fin['money_total'] = $money_all + $money;
			$fin['money_local'] = $_POST['amount_local'];
			
			$fin['bank_code'] 	= $_POST['bank_code'];
			$fin['gateway_dtl'] 	= 'internal_transfer';
			$fin['deposit_comment'] = $comment;
			$fin['trans_type'] = $_POST['trans_type'];
			$last_id = self::$db->insert("bk_wallet_transaction", $fin);
			if (self::$db->affected()) {
				
			 	$json['status'] 		= 'success';
				$json['isRedirect'] = 'yes';
				$json['id'] = $last_id;
				$json['message'] 	= Filter::msgOk_gob(Lang::$word->_TRANSACTION_SUCCESS, false);
			} else {
			  	$json['status'] 		= 'success';
			  	$json['message'] 	= Filter::msgAlert_gob(Lang::$word->_SYSTEM_PROCCESS, false);
				  
			}
		} else {
			$json['message'] = Filter::msgStatus_gob();
		}
		return $json;	
	}

	function save_transaction_withdraw_bak()
	{
		Filter::checkPost('amount', Lang::$word->_TR_AMOUNT);
		if ($_SESSION['is_otp'] == 1) {
			Filter::checkPost('key_verify', "LOST KEY TOKEN");
			Filter::checkPost('code_verify', 'OTP');
		}
		$_POST['amount'] = str_replace(",", "", $_POST['amount']);
		$money = $_POST['amount'] *1 ;
		$money_all = ($_SESSION['usr_dtl']['wallet_amt']['amount']*1);
		if ($money <= 0) {
			 Filter::$msgs['amount'] = Lang::$word->_TR_AMOUNT;
		} elseif ($money > $money_all) {
			 Filter::$msgs['amount'] = Lang::$word->_MONEY_NOT_ENAOUGH;
		}
		if (!isset($_SESSION['uid'])) {
			 /*echo "<pre>";print_r($_SESSION);*/
			 Filter::$msgs['users'] = Lang::$word->_UR_USERNOAVAIL;
		}
		
		if (empty(Filter::$msgs)) {
			$fin['user_id'] 	= $_SESSION['uid'];
			$fin['money'] 		= -$money;
			$fin['money_total'] = $money_all - $money;
			$fin['money_local'] = $_POST['amount_local'];
			$fin['status'] 		= 0;
			$fin['gateway_dtl'] 	= $_POST['gateway_dtl'];
			
			
			$fin['deposit_comment'] = TraderSystem::gen_bank_user();
			$fin['trans_type'] = $_POST['trans_type'];
			$last_id = self::$db->insert("bk_wallet_transaction", $fin);
			if (self::$db->affected()) {
				$is_chkopt = $_SESSION['is_otp'];
				$aUpdatewallet = TraderSystem::call_curl('user_wallet','POST',array(
										'cmd'=>'wallet','amount' => -$money,'corr_code'=>2,'tid'=>$last_id,'comment'=>'WITHDRAW','is_otp'=>$is_chkopt,'key_token'=>$_POST['key_verify'],'key_code'=>$_POST['code_verify']));
				/*echo "<pre>";print_r($aUpdatewallet);*/
				if ($aUpdatewallet['isSuccess'] == 1) {
					$_SESSION['usr_dtl']['wallet_amt']['amount'] = $aUpdatewallet['message']['current_amount'];
					$sendmail 			= TraderSystem::send_mail("withdraw",16,$fin);
					$json['status'] 		= 'success';
					$json['isRedirect'] = 'yes';
					$json['message'] 	= Filter::msgOk_gob(Lang::$word->_WITHDRAW_REC, false);
				} else {
					$resclear =  self::$db->delete("bk_wallet_transaction", "trans_id=" . $last_id);
					$json['status'] ='error';
					$json['message'] = Filter::msgError_gob($aUpdatewallet['errMessage'], false);
				}
			} else {
			  	$json['status'] 		= 'success';
			  	$json['message'] 	= Filter::msgAlert_gob(Lang::$word->_SYSTEM_PROCCESS, false);
				  
			}
		} else {
			$json['message'] = Filter::msgStatus_gob();
		}
		return $json;	 
	}
	
	function calculator_rate($itype,$moneyUSD)
	{
		if (isset($_POST['amount_local']) && $_POST['amount_local'] != '') {
			return ($_POST['amount_local']*1);
		} else {
			return ($moneyUSD*1);
		}
	}
	
	function get_secure_personal()
	{
		$aRes 			= array();
		
		$aRes['step'] 	= 'step2';
		$aRes['type'] 	= '';
		$secure_type 	= (isset($_SESSION['usr_dtl']['user_info']['secure_tool_id']) && $_SESSION['usr_dtl']['user_info']['secure_tool_id'] > 0) ? $_SESSION['usr_dtl']['user_info']['secure_tool_id'] : 0;
		if ($secure_type == 1) {
			$aRes['type'] 		= 'email';
			$aRes['is_verify'] 	= (isset($_SESSION['usr_dtl']['user_info']['email_verified']) && $_SESSION['usr_dtl']['user_info']['email_verified'] > 0) ? 1 : 0;
		} elseif ($secure_type == 2) {
			$aRes['type'] = 'google';
			$aRes['is_verify'] 	= (isset($_SESSION['usr_dtl']['user_info']['google_auth_verified']) && $_SESSION['usr_dtl']['user_info']['google_auth_verified'] > 0) ? 1 : 0;
		} elseif ($secure_type == 3) {
			$aRes['type'] = 'sms';
			$aRes['is_verify'] 	= (isset($_SESSION['usr_dtl']['user_info']['sms_verified']) && $_SESSION['usr_dtl']['user_info']['sms_verified'] > 0) ? 1 : 0;
		} else {
			$aRes['step'] 	= 'no';
		}
		return $aRes;
		
	}
	function save_transaction_internal_step2()
	{
		Filter::checkPost('code_verify', 'OTP');
		Filter::checkPost('list_pkid', 'DATA NOT FOUND.');
		$is_chkopt 		= (isset($_SESSION['usr_dtl']['user_info']['secure_tool_id']) && $_SESSION['usr_dtl']['user_info']['secure_tool_id'] > 0) ? $_SESSION['usr_dtl']['user_info']['secure_tool_id'] : 0;
		$_POST['key_verify'] = isset($_POST['key_verify']) ? $_POST['key_verify'] : 'NO';
		if($is_chkopt != 2) Filter::checkPost('key_verify', "LOST KEY TOKEN");
		if (empty(Filter::$msgs)) {
			$aT =  TraderSystem::getdata('*',"bk_wallet_transaction","where trans_id='".$_POST['list_pkid']."'");
			$money 			= $aT['money']*1;
			if (isset($aT['trans_id']) && $aT['user_id'] == $_SESSION['uid']) {
				
					$res = self::$db->update("bk_wallet_transaction", array('status'=>0), "trans_id='" . $aT['trans_id']. "'");
					
					
					$json['status'] 	= 'success';
					$json['isRedirect'] = 'yes';
				
					$json['message'] 	= Filter::msgOk_gob(Lang::$word->_WITHDRAW_REC, false);
				
			} else {
				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob("DATA NOT FOUND.", false);
			}
		} else {
			$json['message'] 		= Filter::msgStatus_gob();
		}

		return $json;	
	}
	
	
	
	
	/**
	 * [MEMBER] using member after input OTP
	 */
	function save_transaction_withdraw_step2()
	{
			
		Filter::checkPost('code_verify', 'OTP');
		Filter::checkPost('list_pkid', 'DATA NOT FOUND.');
		$is_chkopt 		= (isset($_SESSION['usr_dtl']['user_info']['secure_tool_id']) && $_SESSION['usr_dtl']['user_info']['secure_tool_id'] > 0) ? $_SESSION['usr_dtl']['user_info']['secure_tool_id'] : 0;
		$_POST['key_verify'] = isset($_POST['key_verify']) ? $_POST['key_verify'] : 'NO';
		if($is_chkopt != 2) Filter::checkPost('key_verify', "LOST KEY TOKEN");
		if (empty(Filter::$msgs)) {
			$aT =  TraderSystem::getdata('*',"bk_wallet_transaction","where trans_id='".$_POST['list_pkid']."'");
			$money 			= $aT['money']*1;
			if (isset($aT['trans_id']) && $aT['user_id'] == $_SESSION['uid'] && $money < 0) {
				
				
				$aUpdatewallet 	= TraderSystem::call_curl('user_wallet','POST',array(
										'cmd'=>'wallet','amount' => $money,'corr_code'=>2,'tid'=>$aT['trans_id'],'comment'=>'WITHDRAW','is_otp'=>$is_chkopt,'key_token'=>$_POST['key_verify'],'key_code'=>$_POST['code_verify']));
				
				if ($aUpdatewallet['isSuccess'] == 1) {
					$fupwi['status'] =0;
					$fupwi['comment'] =json_encode($aUpdatewallet);
					$fupwi['money_total'] =($aUpdatewallet['message']['current_amount']*1);
					$res = self::$db->update("bk_wallet_transaction", $fupwi, "trans_id='" . $aT['trans_id']. "'");
					$_SESSION['usr_dtl']['wallet_amt']['amount'] = $aUpdatewallet['message']['current_amount'];
					$sendmail 			= TraderSystem::send_mail("withdraw",16,$aT);
					$json['status'] 	= 'success';
					$json['isRedirect'] = 'yes';
					$json['return_api'] = $aUpdatewallet;
					$json['message'] 	= Filter::msgOk_gob(Lang::$word->_WITHDRAW_REC, false);
				} else {
					$resclear =  self::$db->delete("bk_wallet_transaction", "trans_id=" . $_POST['list_pkid']);
					$json['status'] ='error';
					$json['message'] = Filter::msgError_gob($aUpdatewallet['errMessage'], false);
				}
			} else {
				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob("DATA NOT FOUND.", false);
			}
		} else {
			$json['message'] 		= Filter::msgStatus_gob();
		}

		return $json;	 
	}
	
	function save_transaction_withdraw()
	{
		Filter::checkPost('amount', Lang::$word->_TR_AMOUNT);
		
		$_POST['money'] = str_replace(",", "", $_POST['amount']);
		$money 			= $_POST['money'] *1 ;
		$money_all 		= ($_SESSION['usr_dtl']['wallet_amt']['amount']*1);
		$adminset_min	= ($_SESSION['adminconf']['mininum_withdraw']*1);
		
		if ($money <= 0) {
			 Filter::$msgs['money'] = "amount more than 0.";
		} elseif ($money > $money_all) {
			 Filter::$msgs['money'] = Lang::$word->_MONEY_NOT_ENAOUGH;
		} elseif ($adminset_min > 0 && $money < $adminset_min) {
			Filter::$msgs['money'] = "Mininum withdrawal ".$adminset_min."$";
		}
		
		if (!isset($_SESSION['uid'])) {
			 /*echo "<pre>";print_r($_SESSION);*/
			 Filter::$msgs['users'] = Lang::$word->_UR_USERNOAVAIL;
		}
		if ($_SESSION['is_otp'] == 1) {
			$aSecure 				= TraderSystem::get_secure_personal();
			if ($aSecure['step'] == 'no') {
				 Filter::$msgs['ss_setting'] = Lang::$word->_SECURITY_SETTINGS;
			} else {
				$_POST['status'] = 3;
			}
		} else {
			$_POST['status'] = 0;
		}
		
		
		if (empty(Filter::$msgs)) {
			$aGet_bank_detail 		= TraderSystem::gen_bank_user();
			if (isset($aGet_bank_detail['bank_detail']->bank_currency_local)) {
				$_POST['txt_currency'] = $aGet_bank_detail['bank_detail']->bank_currency_local;
			}
			
			
			$_POST['user_id'] 		= $_SESSION['uid'];
			$_POST['money_total'] 	= $money_all - $money;
			$_POST['money_local'] 	= TraderSystem::calculator_rate('withdraw',$money);
			$_POST['deposit_comment'] 	= $aGet_bank_detail['data'];
		   
			$_POST['type'] = 'withdraw';
			$aRes = TraderSystem::post_transaction($_POST);
			if (isset($aRes['status']) &&  $aRes['status'] == 1) {
				$json['status'] 	= 'success';
				$json['message_dtl']= TraderSystem::gen_detail_about_transfer($_POST);
				$json['id'] 	 	= $aRes['id'];
				$json['dtl'] 		= $aSecure;
				$json['message'] 	= Filter::msgOk_gob(Lang::$word->_WITHDRAW_REC, false);
			} else {
				$json['status'] 	='error';
				$json['message'] 	= Filter::msgError_gob($aRes['message'], false);
			}
		} else {
			$json['message'] 		= Filter::msgStatus_gob();
		}
		return $json;	 
	}

	function gen_detail_about_transfer($aData)
	{
		$txt ='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">From Account</label>';	
		$txt .='<div class="col-md-8"><p>'.$_SESSION['name'].'</p></div></div>';
		
		$txt .='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">Amount</label>';	
		$txt .='<div class="col-md-8"><p>'.$aData['money'].' USD</p></div></div>';	
		
		$txt .='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">Transfer On</label>';	
		$txt .='<div class="col-md-8"><p>'.date('F j, Y').'</p></div></div>';	
		
		$txt .='<div class="form-group">';	
		$txt .='<label class="col-md-4 control-label">Withdraw To</label>';	
		$txt .='<div class="col-md-8"><p>'.$aData['deposit_comment'].'</p></div></div>';		
			
		return $txt;
	}
    /*
	 $aData = array(
	 * 
	 * )
	  
	 * */
	function post_transaction($aData)
	{
		$aRes 			= array();
		$aRes['status'] = 1;
		$money 			= str_replace(",", "", $aData['money']);
		$money 			= $money *1 ;
		
		
		
		if ($aData['itype']=='deposit') {
			if (isset($aData['rate_exchange_id']) && $aData['rate_exchange_id'] != '') {
				$aGetRate = TraderSystem::getdata("*","bk_exchn_rate_new","where id='".$aData['rate_exchange_id']."'");
			} else {
				$aGetRate = false;
			}
			$fin['file_slip'] 	= isset($aData['file_slip']) ? $aData['file_slip']:'';
			$fin['money'] 		= $money;
			$fin['file_slip'] 	= isset($aData['file_slip']) ? $aData['file_slip'] : '';
			$fin['deposit_comment'] = isset($aData['gateway_dtl']) 	? $aData['gateway_dtl']	:	'';
			$fin['deposit_comment'] = isset($aData['deposit_comment'])? $fin['deposit_comment'].":".$aData['deposit_comment']	:	''; 
			if ($aGetRate != false) {
				$fin['curency'] = $aGetRate['currency'];
				$fin['rate_exchange'] = $aGetRate['deposit'];
			}
		} elseif ($aData['type'] == 'withdraw') {
			if (isset($aData['txt_currency']) && $aData['txt_currency'] != '') {
				$aGetRate = TraderSystem::getdata("*","bk_exchn_rate_new","where currency='".$aData['txt_currency']."'");
			} else {
				$aGetRate = false;
			}
			$fin['money'] 		= -$money;
			$fin['deposit_comment'] 	= isset($aData['deposit_comment']) 		? $aData['deposit_comment']:'';
			if ($aGetRate != false) {
				$fin['curency'] = $aGetRate['currency'];
				$fin['rate_exchange'] = $aGetRate['withdraw'];
			}
		}
		
        $fin['user_id'] 	= isset($aData['user_id']) 		? $aData['user_id']:'';
		$fin['money_total'] = isset($aData['money_total']) 	? $aData['money_total']	:	0 ;
		$fin['money_local'] = isset($aData['money_local']) 	? $aData['money_local']	:	0 ;
		
		$fin['money_local'] = isset($aData['rate_exchange']) 	? $aData['rate_exchange']	:"";
		$fin['money_local'] = isset($aData['money_local']) 	? $aData['money_local']	:	0 ;
		$fin['money_local'] = isset($aData['money_local']) 	? $aData['money_local']	:	0 ;
		
		$fin['gateway_dtl'] = isset($aData['gateway_dtl']) 	? $aData['gateway_dtl']	:	'' ;
        $fin['status']      = isset($aData['status']) 		? $aData['status']		:	0 ;
		$fin['bank_code'] 	= isset($aData['bank_code']) 	? $aData['bank_code']	:	'' ;
		$fin['trans_date'] 	= isset($aData['trans_date']) 	? $aData['trans_date']	:	'' ;
		
		$fin['trans_type'] 	= isset($aData['trans_type']) 	? $aData['trans_type']	:	'' ;
		$last_id = self::$db->insert("bk_wallet_transaction", $fin);
		if (self::$db->affected()) {
			$aRes['status'] 	= 1;
			$aRes['id']  		= $last_id;
			$aRes['data'] = $fin;
		} else {
			$aRes['status'] 	= 0;
			$aRes['message'] 	= "INSERT NOT";
		}
		return $aRes;
	}
	
	function save_transaction_deposit()
	{
		Filter::checkPost('bank_code', Lang::$word->_BANK_NAME);
		Filter::checkPost('trans_type', Lang::$word->_SELECT_ACCOUNT_TO_DEPOSIT);
		Filter::checkPost('trans_date', Lang::$word->_LG_WHEN);
		Filter::checkPost('trans_time', Lang::$word->_TIME);
		$money 				= str_replace(",", "", $_POST['amount']);
		$_POST['money'] 	= $money *1 ;
		if ($money <= 0) {
			 Filter::$msgs['amount'] = Lang::$word->_TR_AMOUNT;
		}
		if (!isset($_SESSION['uid'])) {
			 /*echo "<pre>";print_r($_SESSION);*/
			 Filter::$msgs['users'] = Lang::$word->_UR_USERNOAVAIL;
		}
		if (!empty($_FILES['file_slip']['name'])) {
              if (!preg_match("/(\.jpg|\.png|\.gif)$/i", $_FILES['file_slip']['name'])) {
                  Filter::$msgs['file_slip'] = Lang::$word->_CG_LOGO_R;
              }
              if ($_FILES["file_slip"]["size"] > 3072033330) {
                  Filter::$msgs['file_slip'] = Lang::$word->_UA_AVATAR_SIZE;
              }
              $file_info = getimagesize($_FILES['file_slip']['tmp_name']);
              if(empty($file_info))
                  Filter::$msgs['file_slip'] = Lang::$word->_CG_LOGO_R;
        }
		
		if (empty(Filter::$msgs)) {
			if (!empty($_FILES['file_slip']['name'])) {
				$thumbdir = UPLOADS . "file_slip/";
				if (!is_dir($thumbdir)) mkdir($thumbdir, 0755);
				$tName 		= "IMG_" . time().rand(111,222);
				$text 		= substr($_FILES['file_slip']['name'], strrpos($_FILES['file_slip']['name'], '.') + 1);
				$thumbName 	= $thumbdir . $tName . "." . strtolower($text);
 
          		move_uploaded_file($_FILES['file_slip']['tmp_name'], $thumbName);
  				$_POST['file_slip'] = $tName . "." . strtolower($text);
            }
			
			$_POST['user_id'] 		= $_SESSION['uid'];
			
			$_POST['money_total'] 	= $_SESSION['usr_dtl']['wallet_amt']['amount'] + $_POST['money'];
			$_POST['money_local'] 	= TraderSystem::calculator_rate('deposit',$_POST['amount']);
            $_POST['status']      	= 0;
			if($_POST['gateway_dtl'] == 'Wire Transfer') {
				$_POST['status']  	= 10;
			}
			$_POST['trans_date'] 	= $_POST['trans_date'] . ' ' . $_POST['trans_time'];
			$_POST['itype']='deposit';
			$aRes = TraderSystem::post_transaction($_POST);
			if (isset($aRes['status']) &&  $aRes['status'] == 1) {
				$_POST['info']      = $_POST['trans_type'];
				$_POST['info']      .= "\n Comment :".$_POST['deposit_comment'];
				if (isset($_POST['file_slip'])) {
					$_POST['info'] .= "\n"; 
					$_POST['info'] .= '<a href="' . UPLOADURL . 'file_slip/' . $_POST['file_slip'] . '" target="_blank">Document Slip</a>';
				}
				if (isset($_POST['rate_exchange_id']) && $_POST['rate_exchange_id'] !='') {
					$_POST['curency'] = $aRes['data']['curency'];
				}
				$sendmail 			= TraderSystem::send_mail("deposit",15,$_POST);
			 	$json['status'] 		= 'success';
				$json['isRedirect'] = 'yes';
				$json['message'] 	= Filter::msgOk_gob(Lang::$word->_DEPOSIT_REC, false);
			} else {
				$json['status'] ='error';
				$json['message'] = Filter::msgError_gob($aRes['message'], false);
			}
		} else {
			$json['message']		= Filter::msgStatus_gob();
		}
		return $json;	
	}
	function checkOwnAccounttrade($uid,$AccTrade)
	{
		$aData = getValues("uid","bk_trader_acc","trader_acc_login='".$AccTrade."' and uid='".$uid."'");
		if($aData !=0){
			return 1;
		} else{
			$aListAcc = $_SESSION['usr_dtl']['trade_acc_list'];
		    foreach ($aListAcc as $k => $v){
		    	if ($v['trader_acc_login'] == $AccTrade) {
		    		return 1;	
		    	}
		    }
			return 0;
		}
	}
	function getDataAccountGroup()
	{

		$condi = " AND mt4_country = 'EX' " ;
		if ($_SESSION['country']) {
			$aData = getValues("id","bk_account_group","mt4_country='".$_SESSION['country']."'");
			if ($aData != 0) {
				$condi = " AND mt4_country = '".$_SESSION['country']."' " ;
			}
		}
		$sql = "
			SELECT 
				*
			FROM 
				bk_account_group
			WHERE 
				id != '' ". $condi ."
			ORDER BY id
			";
		$obj = Registry::get("Database")->fetch_all($sql);
		$_SESSION['account_group'] = array();
		foreach ($obj as $k => $v)
		{
			$_SESSION['account_group'][$v->mt4_display] = $v;
		}
		return ;
				
	}  
	
	
      
}
?>
