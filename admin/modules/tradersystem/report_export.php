<?php
include(MODPATH."tradersystem/script_datatable_export.php");
?>
<div class="prolific icon heading message dust"> <i class="icon user"></i>
  <div class="content">
    <div class="header"> Manage User Fund. </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
     
    </div>
  </div>
</div>

<div class="prolific-large-content">
  <div class="prolific segment"> 
    <div class="prolific header">Search User.</div>
    <div class="prolific fitted divider"></div>
    <!-- START FRM SERARCH -->
    <form method="POST" name="bu_search" action="">
    	 <div class="prolific form segment">
    	<div class="fields">
    		<div class="field">
    			<label>GATEWAY</label>
    	<label class="input"><input type="checkbox" name="getway_paypal" value="1">Paypal
    	<input type="checkbox" name="getway_skill" value="1">Skill
    	<input type="checkbox" name="getway_offline" value="1">Wire Transfer
    	<input type="checkbox" name="getway_Paysbuy" value="1">PAYSBUY
    	<input type="checkbox" name="getway_creditcard" value="1">Credit Card Online
    	<input type="checkbox" name="getway_wiretransfer" value="1">Wire Transfer Inter
    	<input type="checkbox" name="smsrealtime" value="1">SMS REALTIME
    	<input type="checkbox" name="commission" value="1">Commission
    	<input type="checkbox" name="accessbyadmin" value="1">Access by admin
    	</label>
    	</div>
    	</div>
    	
    	<div class="fields">
    		<div class="field">
        	<label>STATUS</label>
        	<label class="input">
        		<input type="checkbox" name="status_0" value="1">PENDING
        		<input type="checkbox" name="status_1" value="1">APPROVE
        		<input type="checkbox" name="status_2" value="1">CANCEL
        		<input type="checkbox" name="status_3" value="1">PROCESSING
        		</label>
        	</div>
        </div>
        <div class="four fields">
    		<div class="field">
        	<label>DATE START SAVE</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpStart" id="dpStart" value="<?php echo $_POST['dpStart'];?>" class="picker__input picker__input--active"></label>
        </div>
        <div class="field">
        	<label>DATE END SAVE</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpEnd" id="dpEnd" value="<?php echo $_POST['dpEnd'];?>" class="picker__input picker__input--active"></label>
        </div>
        	<div class="field">
        	<label>DATE START TRANS</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpStart_trans" id="dpStart_trans" value="<?php echo $_POST['dpStart_trans'];?>" class="picker__input picker__input--active"></label>
        </div>
        <div class="field">
        	<label>DATE END TRANS</label>
        	<label class="input"><input data-datepicker="true" type="text" name="dpEnd_trans" id="dpEnd_trans" value="<?php echo $_POST['dpEnd_trans'];?>" class="picker__input picker__input--active"></label>
        </div>
        </div>
       
    <div>
    <input type="submit" name="bu_save" value="SEARCH">	
    </div>
    </div>
    </form>
    <!-- END FRM SERARCH -->
    <div class="prolific fitted divider"></div>
   


<div>
	<table border="1" width="100%" class="display table table-bordered table-striped data-table">

<thead>
  <tr>
    <th><div  align="center">-</div></th>
    <th><div  align="center">name</div></th>
    <th><div  align="center">money(USD)</div></th>
    <th><div  align="center">money(THB)</div></th>
    <th><div  align="center">status</div></th>
    <th><div  align="center">gateway</div></th>
    <th><div  align="center">transfer date</div></th>
    <th><div  align="center">save date</div></th>
  </tr>
</thead>

<tbody>
<?php
$condi='';

/*** START CONDITION SQL ***/
if (isset($_POST['dpStart_submit']) && $_POST['dpStart_submit']!='') {
	$savedatestart = $_POST['dpStart_submit']." 00:00:01";
	$condi .= " AND t.save_date > '".$savedatestart."'";
}
if (isset($_POST['dpEnd_submit']) && $_POST['dpEnd_submit']!='') {
	$savedateend = $_POST['dpEnd_submit']." 23:59:59";
	$condi .= " AND t.save_date < '".$savedateend."'";
}
if (isset($_POST['dpStart_trans_submit']) && $_POST['dpStart_trans_submit']!='') {
	$savedatestart = $_POST['dpStart_trans_submit']." 00:00:01";
	$condi .= " AND t.trans_date > '".$savedatestart."'";
}
if (isset($_POST['dpEnd_trans_submit']) && $_POST['dpEnd_trans_submit']!='') {
	$savedateend = $_POST['dpEnd_trans_submit']." 23:59:59";
	$condi .= " AND t.trans_date < '".$savedateend."'";
}
$condigateway = array();
if (isset($_POST['getway_paypal'])) {
	array_push($condigateway,'"paypal"');
}
if (isset($_POST['getway_skill'])) {
	array_push($condigateway,'"skill"');
}
if (isset($_POST['getway_offline'])) {
	array_push($condigateway,'"Wire Transfer"');
}
if (isset($_POST['getway_Paysbuy'])) {
	array_push($condigateway,'"paysbuy"');
}
if (isset($_POST['getway_creditcard'])) {
	array_push($condigateway,'"creditcard"');
}
if (isset($_POST['getway_wiretransfer'])) {
	array_push($condigateway,'"Wire Transfer Inter"');
}
if (isset($_POST['smsrealtime'])) {
	array_push($condigateway,'"Localbank Auto"');
}
if (isset($_POST['commission'])) {
	array_push($condigateway,'"access by commission"');
}
if (isset($_POST['accessbyadmin'])) {
	array_push($condigateway,'"access by admin"');
}
if (count($condigateway) > 0) {
	$condigate = join(",",$condigateway);
	$condi .= " AND t.gateway_dtl in (".$condigate.")";
}
$condistatus = array();
if (isset($_POST['status_0'])) {
	array_push($condistatus,0);
}
if (isset($_POST['status_1'])) {
	array_push($condistatus,1);
}
if (isset($_POST['status_2'])) {
	array_push($condistatus,2);
}
if (isset($_POST['status_3'])) {
	array_push($condistatus,11);
}
if (count($condistatus) > 0) {
	$condistatus = join(",",$condistatus);
	$condi .= " AND t.status in (".$condistatus.")";
}
/*** END CONDITION SQL ***/
$sql = "
	SELECT	
		t.*,
		u.fname,u.lname 
	FROM
		bk_wallet_transaction t,users u 
	WHERE
		t.user_id=u.id
		AND t.status !='' ".$condi."
	ORDER BY 
		t.trans_id"
	;
$data = Registry::get("Database")->fetch_all($sql);

if ($data) {
	$i = 1;
	$aStatus = array(
	0=>"PENDING",
	1=>"APPROVE",
	2=>"CANCEL",
	11=>"PROCESSING"
	);
	$depo_net = 0;
	$withdaw_net = 0;
	foreach ($data as $k => $v)
	{
		if ($v->money >0) {
			$depo_net = $depo_net+$v->money;
		}else {
			$withdaw_net = $withdaw_net+$v->money;
		}
?>
	
 	<tr class="gradeA">
		<td align="center"><?php echo $v->trans_id;?></td>
	   	<td align="center"><?php echo $v->fname." ".$v->lname;?></td>
	   	<td align="center"><?php echo $v->money;?></td>
	   	<td align="center"><?php echo $v->money_local;?></td>
	   	<td align="center"><?php echo isset($aStatus[$v->status]) ?$aStatus[$v->status]: $v->status;?></td>
	   	<td align="center"><?php echo $v->gateway_dtl;?></td>
	   	<td align="center"><?php echo $v->trans_date;?></td>
	   	<td align="center"><?php echo $v->save_date;?></td>
  	</tr>
<?php 
	$i++;
}
}
?> 	
</tbody>
</table>

</div>
<div>
	<?php 
	$nettoal = $depo_net-($withdaw_net*-1);
	echo "<br>DEPOSIT NET : ".$depo_net;
	echo "<br>WITHDRAW NET : ".$withdaw_net;
	echo "<br>DEPOSIT - WITHDRAW : ".$nettoal;
	?>
</div>
</div>
</div>
