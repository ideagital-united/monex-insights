<?php
  /**
   * Configuration
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: config.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
if(!$user->getAcl("deposit")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

Registry::get("TraderSystem")->check_admin_prolific();

function genHtmlfield($title,$setval,$input_type='text',$input_name='name_beta')
{
	?>
	<div class="field">
		<label><?php echo $title;?></label>
		<label class="input">
			<?php if($input_type=='text') {?>
			<input type="text" value="<?php echo $setval;?>" name="<?php echo $input_name;?>" disabled="disabled">
			<?php } elseif($input_type=='textarea'){ ?>
				<textarea cols='35' rows='3' name="<?php echo $input_name;?>"><?php echo $setval;?></textarea>
			<?php } ?>
		</label>
	</div>
        
	<?php
}
$trans_id 		= $_GET['trans_id'];
//$deposit_list = Registry::get("TraderSystem")->getdata("*","acms_wallet_transaction","where trans_type='deposit_from_bank_to_wallet' and status ='0' and trans_id='".$trans_id."'");

//$deposit_list = Registry::get("TraderSystem")->get_transaction_by_id();
$deposit_list 	= Registry::get("TraderSystem")->getdata("*","bk_wallet_transaction","where trans_id='".$trans_id."' ");//"get_transaction_by_id","POST",array('pkid'=>$trans_id));

?>
<div class="prolific icon heading message mortar">
	
  <div class="content">
    <div class="header">PAYMENT DETAIL</div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
      <div class="divider"> / </div>
      <div class="active section"><?php echo Lang::$word->_N_CONF;?></div>
    </div>
  </div>
</div>





<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_CG_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="prolific form segment">
    <div class="prolific header"><?php if($deposit_list['status'] == 0 || $deposit_list['status'] == 10)echo "Approve"; else echo "Detail";?> Deposit</div>
    <div class="prolific double fitted divider"></div>


<?php

if ($deposit_list) {
	$aDtl 	= Registry::get("TraderSystem")->getdata("id,fname,lname,id_card_verified,address_verified","users","where id='".$deposit_list['user_id']."'");
	$iconStatus = ($aDtl['id_card_verified'] != 1 || $aDtl['address_verified'] != 1 ) 
				? "<a href='index.php?do=users&action=edit&id=".$deposit_list['user_id']."'><i class='rounded danger inverted remove icon link'></i> Not Verify</a>" 
				: "<a href='index.php?do=users&action=edit&id=".$deposit_list['user_id']."'><i class='rounded inverted success icon check link'></i> Approval</a>";
	$aRate 	= Registry::get("TraderSystem")->getdata("deposit,currency","bk_exchn_rate","where id!=''");
	$link_img = '';
	
	if (is_file(UPLOADS . 'file_slip/' . $deposit_list['file_slip'])) { 
		$link_img = '<a href="' . UPLOADURL . 'file_slip/' . $deposit_list['file_slip'] . '" target="_blank" width="200">'.
					'<img src="'.UPLOADURL.'file_slip/'.$deposit_list['file_slip'].'" width="200"></a>';
	}
	?>
	<div class="two fields">
		<?php genHtmlfield('TransactionID',$deposit_list['trans_id']);?>
        	<?php genHtmlfield('UserID',$aDtl['id']);?>
	</div>
	<div class="four fields">
		
		<?php genHtmlfield('Customer Name',$aDtl['fname'].' '.$aDtl['lname']);?>
		<div class="field">
        	<label>Verify the person's status</label>
        	<label class="input"><?php echo $iconStatus;?></label>
        </div>
		
		<?php genHtmlfield('Gateway',$deposit_list['gateway_dtl']);?>
		<?php genHtmlfield('To Bank',$deposit_list['bank_code']);?>
       
	</div>
	<div class="two fields">
		<?php genHtmlfield('Deposit Amount (USD)',number_format($deposit_list['money'],2));?>
		<?php genHtmlfield('deposit to [account trade/wallet]',$deposit_list['trans_type']);?>
	</div>
	<div class="two fields">
		<?php genHtmlfield('Transfer Date',$deposit_list['trans_date']);?>
		<?php genHtmlfield('Save Date',$deposit_list['save_date']);?>
	</div>
	 <?php if((isset($deposit_list['curency']) && $deposit_list['curency'] != '' && $deposit_list['rate_exchange'] > 0) ) { ?>
	<div class="two fields">
		<?php genHtmlfield('Currency Rate '.$deposit_list['currency'].'[1USD = '.$deposit_list['rate_exchange']." ".$deposit_list['curency'] .']'
						,number_format($deposit_list['money_local'],2));?>
        
	</div>
	<?php } ?>
	<div class="two fields">
         <?php genHtmlfield('Deposit Comment',$deposit_list['deposit_comment'],"textarea","deposit_comment");?>
		<div class="field">
        	<label>Bank Slip</label>
        	<label><?php echo $link_img;?></label>
        </div>
	</div>
	<div class="showsmtp">
        <div class="prolific thin attached divider"></div>
      </div>

	<?php if (($deposit_list['status'] == 0) || ($deposit_list['status'] == 10)) {?>
		  <form id="prolific_form" name="prolific_form" method="post">
	<input type="hidden" name="action" value="deposit_by_admin">
	<input type="hidden" name="trans_id" value="<?php echo $deposit_list['trans_id'];?>">
    <div class="fields">
		   <div class="field">
		   
          <label>Change Status</label>
          <select name="change_status">
              <?php if ($deposit_list['status'] == 0) { ?>
			<option value='0' >Pending</option>
			<option value='1'>Approved</option>
			<option value='2'>Cancel</option>
			  <?php } else if ($deposit_list['status'] == 10) { ?>
			<option value='10' >Pending Approval</option>
			
			<option value='11' >Approved</option>
            
            <option value='2'>Cancel</option>
			<?php } ?>
          </select>
        </div>
	</div>
	<div class="fields">
		  <?php genHtmlfield('Comment',"","textarea","comment");?>
	</div>
	<div class="prolific double fitted divider"></div>
    <button type="button" name="dosubmit" class="prolific positive button">Update</button>
    </form>
	 <?php }else{?>
		<div class="fields">
		   <div class="field">
		   
          <label>Status</label>
          <?php
          switch($deposit_list['status']){
		  	case 2 : echo "<font color='red'>Cancel</font>";break;
			case 1 : echo "<font color='green'>Approved</font>";break;
			case 3 : echo "<font color='blue'>Confirm Sms</font>";break;
          case 4 : echo "<font color='blue'>Pending Approval</font>";break;
			default : echo "-";break;
          }
          ?>
        </div>
		</div>
		<div class="fields">
			   <?php genHtmlfield('Deposit Comment',$deposit_list['deposit_comment'],"textarea","deposit_comment");?>
		</div>
		<div class="fields">
			   <?php genHtmlfield('Admin Comment',$deposit_list['comment'],"textarea","comment");?>
		</div>
	<?php } ?>
	<?php
} else {
	 Filter::msgSingleAlert("NO DATA");

}
?>






 </div>     
  <div id="msgholder"></div>
</div>


<!-- START LIST -->
<?php 
if (($deposit_list['status'] == 0) || ($deposit_list['status'] == 10)) {
include(MODPATH."tradersystem/script_datatable.php");
?>
<div class="prolific-large-content">
  
  <div class="prolific segment">
    <div class="prolific header">View Transaction</div>
   
    <div class="prolific small form basic segment">
   
      <div class="prolific divider"></div>
      
     
    </div>
    <table border="1" class="display table table-bordered table-striped data-table">

<thead>
  <tr>
    <th><div  align="center">-</div></th>
    <th><div  align="center"> Customers </div></th>
    <th><div  align="center"> Amount(USD)</div></th>
    <th><div  align="center"> Status</div></th>
    <th><div  align="center"> Gateway</div></th>
    <th ><div  align="center"> Payment Date </div></th>
  </tr>
</thead>
<tbody>
<?php
$aTransdate = explode(" ",$deposit_list['trans_date']);

$sql = "SELECT 
			t.money,t.trans_id,t.user_id,t.trans_date,t.status,t.bank_code,
			u.fname,u.lname "
		  . "\n FROM bk_wallet_transaction t,users u"
		  . "\n WHERE t.user_id=u.id and t.trans_date like '%".$aTransdate[0]."%' limit 0,5"		  
		  ;
	
$row = Registry::get("Database")->fetch_all($sql);
if ($row) {
	$i = 0;
	foreach ($row as $k => $dd_list)
	{
		$dd_list->money = number_format($dd_list->money,2);
		if ($dd_list->money < 0 )  {
			$money 	= "<font color='red'><b>".($dd_list->money)."</b></font>";
			$link 	= "<a href='index.php?do=modules&action=config&modname=tradersystem&menu=withdraw-list-pending-edit&trans_id=".$dd_list->trans_id."'><i class='rounded inverted success icon pencil link'></i></a>";
		} elseif($dd_list->money > 0) {
			$money 	= "<font color='green'><b>".($dd_list->money)."</b></font>";
			$link 	= "<a href='index.php?do=modules&action=config&modname=tradersystem&menu=deposit-list-pending-edit&trans_id=".$dd_list->trans_id."'><i class='rounded inverted success icon pencil link'></i></a>";
		} else {
			$link 	= "-";
			$money 	= "0.00";
		}
		switch($dd_list->status){
			case 1 : $txt = '<i class="big checkmark icon" data-icon-name="checkmark icon"></i>';break;
			case 0 : $txt = '<i class="big time icon" data-icon-name="time icon"></i>';break;
			case 11 : $txt = '<i class="big lab icon" data-icon-name="lab icon"></i>';break;
			case 10 : $txt = '<i class="big time icon" data-icon-name="time icon"></i>';break;
			case 2 : $txt = '<i class="big remove icon" data-icon-name="remove icon"></i>';break;
		}
		
?>
 	<tr class="gradeA">
		<td align="center"><?php echo $link;?></td>
	   	<td align="center"><?php echo $dd_list->fname." ".$dd_list->lname."[".$dd_list->user_id."]";?></td>
	   	<td align="right"><?php echo $money;?></td>
	   	<td align="center"><?php echo $txt;?></td>
	   	<td align="center"><?php echo $dd_list->bank_code;?></td>
	   	<td align="center"><?php echo $dd_list->trans_date;?></td>
  	</tr>
  	<?php
	}
} 
?>
</tbody>
</table>
  </div>

</div>
<?php } ?>

