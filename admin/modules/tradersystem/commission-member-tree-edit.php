<?php
    
     if (isset($_POST['but_edit_ref']) && ($_POST['but_edit_ref'] != '')) {
                  
				/*  print_r($_POST); */
		 
		 if ($_POST['new_cms_ref_id'] >= $_POST['edit_id']) {
		 	
			$err = "<br /><font color='#FF0000'>Cannot change to new referrer because new referrer id is higher than edit id.</font><br />";
			
		 } else {
		 
		          $u_list3 = Registry::get("TraderSystemCommission")->getUserByID($_POST['new_cms_ref_id']); //$_SESSION['uid']
                   
	              $uu_list3 = $u_list3[0];
				  
				  $fq = array();
					
				  $fq['ref_email'] = $uu_list3->email;
				  
				//  echo $fq['ref_email'];
				  
				  
				if ($fq['ref_email'] != '') {
					
				  $fq['edit_email'] = $_POST['edit_email'];
				  	
                  $up_ct = Registry::get("TraderSystemCommission")->updateReferrer($fq); 
				  
				//  print_r($up_ct);  
					
					 
                  if($up_ct['result'] == 'success') {
                  	
					
					$up_dd = Registry::get("TraderSystemCommission")->updateReferrerCMS($fq['ref_email'],$_POST['new_cms_ref_id'],$_POST['edit_id']);
					
					
              ?>
    
		            <script type="text/javascript">
		    
		               location.href='index.php?do=modules&action=config&modname=tradersystem&menu=commission-member-tree';
		    
		            </script>
    
              <?
                	exit;    
                	
                   } else {
                		$err = "<br /><font color='#FF0000'>Cannot change to new referrer, please try again.</font><br />";
                   }
              } else {
              			
						$err = "<br /><font color='#FF0000'>Cannot find new referrer data from user id ".$_POST['new_cms_ref_id'].", please try again.</font><br />";
              }
		 }
  }

?>

 <div class="prolific icon heading message green"> <i class="file icon"></i>
  <div class="content">
    <div class="header"> Edit Member Referrer </div>
    <div class="prolific breadcrumb"><i class="icon home"></i> <a class="section" href="index.php">Dashboard</a>
      <div class="divider"> / </div>
      <div class="active section">Edit Member Referrer</div>
    </div>
  </div>
</div>

<div class="prolific-large-content">
  
    <div class="prolific message">
      
        Here you can edit member referrer.
    </div>
    
   

    
    
     <div class="prolific form segment">    
   
        <div class="prolific header">Edit Member Referrer <?php echo Lang::$word->_AFFLILIATE_EDIT_REFERRER;?></div>

        <div class="prolific fitted divider"></div>
        
        
        
        
        <!-- start tree view -->




   <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                	
                   
                    

<div class="panel-body">


                
                <?php 
                
                
                if ($err != '') {
                		
                	echo $err;
                }
                   
                 
                $u_list = Registry::get("TraderSystemCommission")->getUserByID($_GET['edit_id']); //$_SESSION['uid']
                   
                $uu_list = $u_list[0];

                    
				/*	echo "<pre>";
                    print_r($uu_list);
                    echo "</pre>"; 
     			*/
                ?>

                      <br /><strong>Edit member detail:</strong>
                      <br />ID: [ <?php echo $uu_list->id; ?> ] 
                      <br />Email: [ <?php echo $uu_list->email; ?> ] 
                      <br />Name: [ <?php echo $uu_list->fname; ?> <?php echo $uu_list->lname; ?> ]
                      <br /><hr />             
                  
                    <?php
                    
                    $u_list2 = Registry::get("TraderSystemCommission")->getUserByID($uu_list->cms_ref_id); //$_SESSION['uid']
                   
	                $uu_list2 = $u_list2[0];
	                
 
                    
                    	echo "<br /><strong>Now referrer detail:</strong>";
						
						echo "<br />Referrer ID: ".$uu_list2->id;
						
						echo "<br />Referrer Email: ".$uu_list2->email;
						
						echo "<br />Referrer Name: ".$uu_list2->fname." ".$uu_list2->lname;
						
						echo "<br /><hr>";
					
                    ?>
                    <form action="" method="post">
                    	
                    	<input type="hidden" name="edit_id" value="<?php echo $_GET['edit_id']; ?>" />
                    	<input type="hidden" name="edit_email" value="<?php echo $uu_list->email; ?>" />
                    
                    <br /><strong>Change to new referrer id:</strong>
                    
                    <br /><input type="text" name="new_cms_ref_id">
                    
                    <br /><input type="submit" name="but_edit_ref" value="Save">
                    
                    
                   </form>
               
        
       </div>
      </div>
      </div>
      </div>  
        
        
        
        
        
        
        
        
        
        
		<!-- end tree view -->
            

	</div> <!-- <div class="prolific form segment">  -->


</div> <!-- end < div class="prolific-large-content" > -->


