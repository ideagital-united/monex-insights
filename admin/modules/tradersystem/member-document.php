<?php
if(!$user->getAcl("document_control")): print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN); return; endif;

Registry::get("TraderSystem")->check_admin_prolific();

$act = (get('sta')) ? get('sta')  : 'approve'; 
?>
<div class="prolific icon heading message sky"><a class="helper prolific top right info corner label" data-help="menu"><i class="icon help"></i></a> <i class="reorder icon"></i>
  <div class="content">
    <div class="header"> Manage Member Personal Document [<?php echo $act;?>] </div>
    
    <div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php" class="section"><?php echo Lang::$word->_N_DASH;?></a>
     <div class="divider"> / </div>
     <?php if($act!='pending'){?>
     <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document&sta=pending" class="section">pending</a>
      <?php }else{echo "pending";}?>
      <div class="divider"> / </div>
      <?php if($act!='approve'){?>
      <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document" class="section">approved</a>
      <?php }else{echo "approve";}?>
     <div class="divider"> / </div>
     <?php if($act!='cancel'){?>
     <a href="index.php?do=modules&action=config&modname=tradersystem&menu=member-document&sta=cancel" class="section">cancelled</a>
      <?php }else{echo "cancel";}?>
    </div>
  </div>
</div>

<script>
var var_action='document_docs_<?php echo $act;?>';
var SITEURL = "<?php echo SITEURL;?>";
</script>

<?php
//=== end edit
//=== start view
/*echo BASEPATH."admin/modules/tradersystem/script_datatable_ajax.php";*/
include(MODPATH."tradersystem/script_datatable_ajax.php");
?>
<div>
<h1>Document Infomation</h1>
</div>
<div id="msgholder"></div>
<div>
	<table border="1" class="display table table-bordered table-striped data-table">
<thead>
  <tr>
		<th align="center">NO</th>
  		<th align="center">USERID<!--trans_id--></th>
  		<th align="center">NAME<!--trans_id--></th>
		<th align="center">Doc Type<!--user_id--></th>
		<th align="center">Document Detail</th>
  </tr>

</thead>
</table>
</div>
<script>
function viewimg(obj){
	$('#darawantest').elevateZoom({tint:true, tintColour:'#F90', tintOpacity:0.5}); 
	
	
	/*
	var imgurl = obj.find('img').attr('src');
	var dialog = new Messi(
    '<img src="'+imgurl+'">',
    {closeButton: true,title: 'View Document'}
);
*/
	/*new Messi.img(
    imgurl,{ closeButton: true}
);
*/
}
function gendata_by_type(itype,obj)
{
	txt = "";
	if (itype == 'bookbank') {
		txt += "<label>name : "+(obj.detail.fnamelname || 'n/a')+"</label><br>";
		txt += "<label>bank name : "+(obj.detail.bank_name || 'n/a')+"</label><br>";
		txt += "<label>account no : "+(obj.detail.bank_account_number || 'n/a')+"</label><br>";
		if (obj.detail.bank_currency_local) {
			txt += "<label>currency local : "+(obj.detail.bank_currency_local || 'n/a')+"</label><br>";
		}
		txt += "<label>bank country : "+(obj.detail.bank_country|| 'n/a')+"</label><br>"
		txt += "<input type='hidden' id='api_id' name='api_id' value='"+ obj.detail.api_id +"'>"
		txt += "<input type='hidden' id='client_id' name='client_id' value='"+ obj.detail.client_id +"'>";
		
	}
	
	if (obj.file_url != 'no') {
		txt += "<label>Document Data : <br><span id=\"viewimg\" onmouseover=\"viewimg($(this));\"><img src=\""+obj.file_url+"\" width=\"200\" id=\"darawantest\"></span></label>";
	} else {
		txt += "<label>Document Data : "+obj.file_url+"</label>";
	}
	return txt;
	
}

(function ($) {
	   var itypehead = {card:"Personal ID", addr:"Personal Address", bookbank:"Bank Document"}; 
	  
       $('body').on('click', 'a.document_mgr', function () {
       		var itype = $(this).attr('itype');
       		var mailinfo = $(this).attr('email');
       		var sta = $(this).attr('sta');
       		var chkapp=chkpening="";
       		if (sta == 1) {
       			chkapp = " checked ='checked'";
       		} else {
       			chkpening = " checked ='checked'";
       		}
       		var filename = $(this).attr('filename');
       	    $.ajax({
			  url: SITEURL + "/admin/modules/tradersystem/controller.php" ,
			  type: 'POST',
			  data:{action:'doGetDocUserInfo',itype:itype,email:mailinfo,sta:sta,filename:filename},
			  dataType :'json'
			}).done(function(json) {				
			if (json.isRedirect) {			  	  	
				location.reload(); 			  	 
				 }
				eval("var headtitle = itypehead."+itype);
				var dtldata = gendata_by_type(itype,json);
			   //== start modal
			
            var text = "<div class=\"prolific fluid input\">"
            text += "<div class=\"inline-group\">";
            text += dtldata;
            text += "<div class=\"prolific divider\"></div>";
            text += "<label class=\"radio\"><input name=\"change_status\" type=\"radio\" value=\"0\" "+ chkpening +"><i></i><span class=\"prolific warning font\">Pending</span></label>";
            text += "<label class=\"radio\"><input name=\"change_status\" type=\"radio\" value=\"1\" "+ chkapp +"><i></i><span class=\"prolific success font\">Approve</span></label>";
            text += "<label class=\"radio\"><input name=\"change_status\" type=\"radio\" value=\"2\"><i></i><span class=\"prolific danger font\">Cancel</span></label>";
            text += "</div>";
            new Messi(text, {
                title: headtitle,
                modal: true,
                zIndex:700,
                width: '500px',
                closeButton: true,
                buttons: [{
                    id: 0,
                    label:'UPDATE',
                    class: 'positive',
                    val: 'Y'
                }],
                callback: function (val) {
                    changestrtus = $("input:radio[name=change_status]:checked").val();
                    $.ajax({
                        type: 'post',
                        url: SITEURL + "/admin/modules/tradersystem/controller.php" ,
                        dataType: 'json',
                        data: {
                            action : 'document_by_admin',
                            change_status: changestrtus,
                            itype: itype,
                             email: mailinfo,
                            api_id:json.detail.api_id,
                            client_id:json.detail.client_id
                            
                        },
                        success: function (json) {
                            if (json.status == "success") {
                                $("#msgholder").html(json.message);
                                location.reload();
                            } else {
                            	 $("#msgholder").html(json.message);
                            }
                            
                            
                        }

                    });

                }
            });
        }); 
			   //=== end madal
			});
           
})(jQuery);

</script>





