<?php

	define("_VALID_PHP", true);
	require_once("init.php");

	$user_group_list = $usernc->list_user_group($_SESSION['securekey']);
	
	if($user_group_list == false) {
		print Filter::msgAlert(Lang::$word->_CG_ONLYADMIN);
	} else {
		if (isset($_POST['dosubmit'])){
			//print_r($_POST);
			$data['email'] = $_POST['email'];
			$data['admin_user'] = $_POST['user'];
			$data['admin_pass'] = $_POST['pass'];
			$data['grant_id'] = $_POST['grant_id'];
			
			$result = $usernc->set_admin($data);
			$user->update_userlevel($data['grant_id'], $data['email']);
			print_r($result);
		} else {
			$x = count($user_group_list['message']);
			$y = 0;
			$new_array = array();
			do {
				$new_array[$user_group_list['message'][$y]['type_name']] = $user_group_list['message'][$y]['type_id'];
				$y++;
			} while ($y < $x);
			foreach($new_array as $key=>$value){
				$html .= "<option value='$value'>$key</option>";
			}
		?>
<div class="prolific icon heading message dust"><a class="helper prolific top right info corner label" data-help="user"><i class="icon help"></i></a> <i class="user icon"></i>
	<div class="content">
		<div class="header"> <?php echo Lang::$word->_BRA_SET_ADMIN;?></div>
		<div class="prolific breadcrumb"><i class="icon home"></i> <a href="index.php?do=create_admin" class="section"><?php echo Lang::$word->_BRA_CREATE_ADMIN;?></a>
		</div>
	</div>
</div>

<div class="prolific-large-content">
  <div class="prolific message"><?php echo Core::langIcon();?><?php echo Lang::$word->_URG_INFO1. Lang::$word->_REQ1 . '<i class="icon asterisk"></i>' . Lang::$word->_REQ2;?></div>
  <div class="prolific form segment">
    <div class="prolific header">Create Super user</div>
    <div class="prolific double fitted divider"></div>
    <form id="admin_form" name="admin_form" method="post">
        
      <div class="two fields">
        <div class="field">
          <label>Email</label>
          <label class="input"><i class="icon-append icon asterisk"></i>
		  <input name="email" placeholder="email" type="text">
          </label>
        </div>
        
      </div>
      
      <div class="two fields">
        <div class="field">
          <label>User for SU</label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input id="user" name="user" placeholder="New username" type="text">
          </label>
        </div>    
      </div>

      <div class="two fields">
        <div class="field">
          <label>Pass for SU</label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <input id="pass" name="pass" type="password">
          </label>
        </div>    
      </div>
	  
      <div class="two fields">
        <div class="field">
          <label>SU Type</label>
          <label class="input"><i class="icon-append icon asterisk"></i>
            <select name="grant_id"><?php echo $html; ?></select>
          </label>
        </div>    
      </div>
     
      <div class="prolific double fitted divider"></div>
      <button type="submit" name="dosubmit" class="prolific positive button"><?php echo Lang::$word->_UR_UPDATE;?></button>
     
    </form>
  </div>
  <div id="msgholder"></div>
</div>
		<?php
		}
	}
?>

