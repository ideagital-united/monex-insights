<?php
  /**
   * Index
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2010
   * @version $Id: index.php, v2.00 2011-04-20 10:12:05 gewa Exp $
   */
  define("_VALID_PHP", true);
  require_once("init.php");

  if (is_dir("../setup"))
      : die("<div style='text-align:center'>" 
		  . "<span style='padding: 5px; border: 1px solid #999; background-color:#EFEFEF;" 
		  . "font-family: Verdana; font-size: 11px; margin-left:auto; margin-right:auto'>" 
		  . "<b>Warning:</b> Please delete setup directory!</span></div>");
  endif;
    

 if (!$user->is_Admin()){
 	  if (isset($_SESSION['old'])) {
 	  	$isadmin = $_SESSION['old'];
		unset($_SESSION['CMSPRO_username']);
		  unset($_SESSION['email']);
		  unset($_SESSION['name']);
          unset($_SESSION['membership_id']);
		  unset($_SESSION['memused']);
		  unset($_SESSION['access']);
          unset($_SESSION['uid']);
		  unset($_SESSION['old']);
		  $_SESSION['CMSPRO_username'] = $isadmin['CMSPRO_username'];
		 $user->login_admin_again();
 	  } else {
      	redirect_to("login.php");
		exit;
	  }
 
 }
 
 
  
 //echo'<pre>';print_r($_SESSION);
?>
<?php include("header.php");?>
<!-- Start Content-->
  <?php include("mainmenu.php");?>
  <div class="wrapper"><?php (Filter::$do && file_exists(Filter::$do.".php")) ? include(Filter::$do.".php") : include("main.php");?></div>
<!-- End Content/-->
<?php include("footer.php");?>