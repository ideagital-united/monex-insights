<?php

  /**

   * Login

   *

   * @package CMS Pro

   * @author prolificscripts.com

   * @copyright 2011

   * @version $Id: login.php, v2.00 2011-04-20 10:12:05 gewa Exp $

   */

  

  define("_VALID_PHP", true);
  require_once("init.php");
  
  ///print_r(get_defined_constants(true));
 if (!$user->is_Admin()){
 	  if (isset($_SESSION['old'])) {
 	  	$isadmin = $_SESSION['old'];
		unset($_SESSION['CMSPRO_username']);
		  unset($_SESSION['email']);
		  unset($_SESSION['name']);
          unset($_SESSION['membership_id']);
		  unset($_SESSION['memused']);
		  unset($_SESSION['access']);
          unset($_SESSION['uid']);
		  unset($_SESSION['old']);
		  $_SESSION['CMSPRO_username'] = $isadmin['CMSPRO_username'];
		 $user->login_admin_again();
 	  } else {
      	redirect_to("login.php");
		exit;
	  }
 
 } else {
	
 }
 define("MODPATHF", BASEPATH . "modules/");
/*echo '=====================>'.MODPATH . "tradersystem/admin_class.php";*/
require_once (MODPATH . "tradersystem/admin_class.php");
Registry::set('TraderSystem', new TraderSystem($content -> module_data));

$action = $_POST['action'];
switch($action){
	case 'request_opt': fn_request_opt();break;
	case 'check_opt': fn_check_opt();break;
}
function fn_request_opt(){
	include BASEPATH . "/modules/tradersystem/define_by_api.php";
	$mobilephone = (isset($_SESSION['usr_dtl']['user_info']['mobile']) && $_SESSION['usr_dtl']['user_info']['mobile'] != '') 
				? $_SESSION['usr_dtl']['user_info']['mobile']
				: "XXXX";
	$param['key_token'] 	= rand_strb(6);
	$param['key_code']  	= rand_int(6);
	$param['secure_tool_id']= 3;
	$param['send_to']  		= $mobilephone;
	$param['activity']      = 'admin login level2';
	$res 	= Registry::get("TraderSystem") ->call_curl("secureotp","POST",$param);
	
	$res2 	= Registry::get("TraderSystem")->send_sms_infobip("Two-Step Verifycation Ref:".$param['key_token']." is ".$param['key_code'],$param['send_to']);
	
	print json_encode(array('otpcode' => $param['key_token']));
}

function fn_check_opt(){
	$aRes = Registry::get("TraderSystem") ->call_curl_get("get_check_otp/".$_POST['optcode']."/admin login level2"."?cmd=otp");
	
	if (isset($aRes['isSuccess']) && $aRes['isSuccess']==1) {
		$cookie_name 	= "otpss";
		$cookie_value 	= $_POST['optcode'];
		setcookie($cookie_name, $cookie_value, strtotime( '+14 days' )); // 86400 = 1 day
		print json_encode(array('status' =>1,'msg'=>Filter::msgOk("Success Please Wait..",false)));
	} else {
		$msg = isset($aRes['Message']) ? $aRes['Message'] : 'INCORRECT.';
		print json_encode(array('status' =>0,'msg'=>Filter::msgError($msg,false)));
	}
}

		
?>