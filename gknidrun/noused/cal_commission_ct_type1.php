<?php
/*****************************************
 * aData : 

 * FOR CHECK LOT MT4 FOR COMMISSION
 * CRATE BY ISARA 12/01/2558
 ****************************************/
 
$BASEPATH = realpath(dirname(__FILE__) . '/../lib');
define('_VALID_PHP', true);
define('istest', true);

include ($BASEPATH.'/tofixconfigbysite.ini.php');
$dbhost = DB_SERVER;
$dbname = DB_DATABASE;
$dbuser = DB_USER;
$dbpass = DB_PASS;

global $con;
$con = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if (!$con) {
    die("Conectare imposibila: " . mysqli_error($con));
}
mysqli_query($con, "SET NAMES UTF8");

include "fn_mysqli_con_new.php";

/* change to get from api */
function get_comm_level() 
{
    // find ref
    global $con;
    $sqlu1 = "SELECT count(level_id) as cnt_level FROM `acms_comm_level` ";
    
    echo "<br />sqlu1=".$sqlu1;
    $resu1 = mysqli_query($con,$sqlu1);
    $rowu1 = mysqli_fetch_array($resu1,MYSQL_ASSOC);
    mysqli_free_result($resu1);
    return $rowu1['cnt_level'];
}


/* api function */
function get_comm_level_api()
{
    $get_level_api = call_curl("comm_level" , "GET");
    
    $level = count($get_level_api['message']);
    
    //print_riz($level);
    
    if ($level <= 0) {
        return 0;
    } else {
        return $level;
    }
}


/* change to get from api */

function get_comm_level_value($level_no) 
{
    // find ref
    global $con;
    $sqlu1 = "SELECT * FROM `acms_comm_level` where level_no = ".$level_no." ";
    
    echo "<br />sqlu1=".$sqlu1;
    $resu1 = mysqli_query($con,$sqlu1);
    $rowu1 = mysqli_fetch_array($resu1,MYSQL_ASSOC);
    mysqli_free_result($resu1);
    return $rowu1;
}

function get_comm_level_value_api($level_no) 
{
     $get_level_value_api = call_curl("comm_level_value/".$level_no , "GET");
    
    $usd_per_lot = $get_level_value_api['message'][0]['usd_per_lot'];
    
    //print_riz($level);
    
    if ($usd_per_lot <= 0) {
        return 0;
    } else {
        return $usd_per_lot;
    }
}


function get_ref($member_email,$next_level,$aTrade) 
{
    // find ref
    global $con;
    global $comm_level;
    $sqlu1 = "SELECT * FROM `users` 
                   WHERE email = '".$member_email."'
               ";
    
    echo "<br /><br />get_ref level = $next_level sqlu1=".$sqlu1;
    
    $resu1 = mysqli_query($con,$sqlu1);
    $rowu1 = mysqli_fetch_array($resu1,MYSQL_ASSOC);
       // print_r($rowu1);
    mysqli_free_result($resu1);
    
   
        // function find_ref($rowu1['ref_id'],$next_level)
        
        $usd_per_lot = get_comm_level_value_api($next_level);
       // $usd_per_lot = $level_value['usd_per_lot'];
        
        echo "<br />order id : ".$aTrade['order_id']." commission for ref_id ".$rowu1['cms_ref_id']." (".$rowu1['ref_email'].") is ".$usd_per_lot." usd_per_lot";
        
        $fqa['cal_datetime'] = time();
        $fqa['from_user'] = $aTrade['from_user'];
        $fqa['trader_acc_login'] = $aTrade['trader_acc_login'];
        $fqa['product_name'] = $aTrade['product_name'];
        $fqa['lot_trade'] = $aTrade['lot_trade'];
        $fqa['usd_per_lot'] = $usd_per_lot;
        $fqa['to_user'] = $rowu1['ref_email'];
        $fqa['paid_status'] = 1;
        $fqa['paid_date'] = time();
        $fqa['paid_datetime'] = date("Y-m-d H:i:s");
        $fqa['order_id'] = $aTrade['order_id'];
        
        $comms = $fqa['lot_trade'] * $usd_per_lot;
        
        echo " commisssion=".$comms;
        
        //$fq['paid_status'] = 1;
        if ($fqa['to_user'] != '') {
            $insert_fqa = i_insert("bk_comm_paid_history" , $fqa);
        }
            
       
        if($insert_fqa) {
                
                // insert to waiting cron wallet
                
/*           
CREATE TABLE IF NOT EXISTS `bk_wallet_transaction` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `money` decimal(9,2) NOT NULL DEFAULT '0.00',
  `money_total` decimal(9,2) NOT NULL DEFAULT '0.00',
  `save_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `money_local` decimal(10,2) NOT NULL COMMENT 'ค่าเงินตาม local เช่นไทยก็บาท',
  `status` tinyint(1) NOT NULL COMMENT '0=pending,1=approve,2=cancel,3=sms',
  `code_verify` varchar(6) NOT NULL,
  `gateway_dtl` varchar(50) NOT NULL,
  `bank_code` varchar(100) NOT NULL,
  `trans_date` varchar(18) NOT NULL,
  `comment` text NOT NULL,
  `deposit_comment` text NOT NULL,
  `complete_by` varchar(20) NOT NULL,
  `trans_type` varchar(50) NOT NULL COMMENT 
 
 * 'deposit_from_bank_to_wallet
 * , deposit_from_trade_to_wallet
 * , withdraw_from_wallet_to_bank
 * , withdraw_from_wallet_to_trade',
 
  `file_slip` varchar(250) NOT NULL,
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ticket` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=109 ;
*/


                $money_wallet = call_curl("wallet_by_cron/".$aTrade['user_id']);
                
              $wallet_now = $money_wallet['message']['amount'];
               
              $fqz['user_id'] = $rowu1['cms_ref_id'];
              $fqz['money'] = $comms;
              $fqz['money_total'] = $wallet_now + $comms;
              $fqz['status'] = 0;
              $fqz['trans_date'] = date("d-m-Y G:i");
              $fqz['gateway_dtl'] = 'Commission';
              $fqz['comment'] = 'Trade commission order id '.$fqa['order_id'];
              $fqz['trans_type'] = 'wallet';
              i_insert("bk_wallet_transaction" , $fqz);
              
              
                // update that calculated
                
                $fq['is_calc'] = 1;
              $fq['cal_datetime'] = time();
              i_update("bk_closed_ctrade", $fq , "deal_id=".$fqa['order_id']." ");
                
          }
            
       
        echo "<br />--------------------<br />";
         
     if ($rowu1['cms_ref_id'] != 0) {
           
         $next_level++;
         
         if($next_level <= $comm_level) {
        
            $get_ref = get_ref($rowu1['ref_email'],$next_level,$aTrade);
         }
     }
}


/*
$sql = "
    SELECT
        *
    FROM
        bk_comm_setting
    ";
$sql2       = mysqli_query($con, $sql);
$dataConfig = mysqli_fetch_array($sql2, MYSQL_ASSOC);
mysqli_free_result($sql2);
*/

//$aLogin = call_curl("admin_login","POST",array("admin_user"=>"secret","admin_pass"=>"qwerty258987"));
    
//echo print_riz($aLogin);
    
    
$cal_comm_status = call_curl("comm_setting","GET");

//print_riz($cal_comm_status);

  
$dataConfig['paid_status'] = $cal_comm_status['message'][0]['paid_status'];

/*

if (!$dataConfig) {
    // === ไม่มี database
    echo "NO SELECT DATABASE : acms_comm_setting";
    return false;
} elseif (!isset($dataConfig['set_id']) || $dataConfig['set_id']=='') {
    // === ไม่มี API MT4
    echo "NO DATA acms_comm_setting";
    return false;
} else {
    // === start process ====
    
    print_r($dataConfig);
   */ 
   
   
   // get dataConfig from api
    
    if($dataConfig['paid_status'] == 'start') {
    
  
   
    
    global $comm_level;
    
    $comm_level = get_comm_level_api();
    
    print_riz($comm_level,"comm_level=");
    
   // echo "<br />comm_level=".$comm_level;
    
   /* $sql0 = "SELECT DISTINCT login FROM `acms_closed_ordermt4` where profit != 0 
and from_unixtime(close_time) 
between '".$yesterday." 00:00:00' 
and '".$yesterday." 23:59:59' 
order by close_time asc"; */

$sql0 = "SELECT * 
FROM `bk_closed_ctrade` 
WHERE is_calc = 0
AND trade_side >= 1 
AND trade_side <= 2
AND is_demo = 0
ORDER BY UTC_LAST_UPDATE_DATETIME ASC";

echo "<br />sql0=".$sql0;


$res0 = mysqli_query($con,$sql0);
while($row0 = mysqli_fetch_array($res0,MYSQL_ASSOC)) {
    
    //echo "<br />row0=".$row0['login'];
    
    // get user data where login 
    // GET FROM API
    
    $user_email = call_curl("email_by_trade_acc_login/".$row0['LOGIN'], "GET");
    
    
    $trade_login_dtl = call_curl("trader_acc_login_dtl/".$row0['LOGIN'], "GET");
    
   print_riz($trade_login_dtl,"trade_login_dtl=");
   
    $sqlu0 = "SELECT * 
                FROM `users`
                WHERE email = '".$user_email."' 
           ";
    echo "<br />sqlu0=".$sqlu0;
    $resu0 = mysqli_query($con,$sqlu0);
    $rowu0 = mysqli_fetch_array($resu0,MYSQL_ASSOC);
    //  print_riz($rowu0);
    mysqli_free_result($resu0);
   
   
    //=================================== end get ref_id==================//
    
        
        $lot = $row0['CLOSED_VOLUME']/10000000;
        
        echo "<br />row1=".$row0['LOGIN']."|close_datetime=".$row0['UTC_LAST_UPDATE_DATETIME']."|".$row0['CLOSED_VOLUME']."|lot=".$lot;
        //$sum_lot[$row0['login']] = $sum_lot[$row1['login']] + $row1['volume'];
        
            $aTrade = array();
           $aTrade['from_user'] = $rowu0['email'];
           $aTrade['trader_acc_login'] = $row0['LOGIN'];
           $aTrade['product_name'] = $trade_login_dtl[0]['product_name'];
           $aTrade['lot_trade'] = $lot; // $row0['CLOSED_VOLUME']/100;
           $aTrade['order_id'] = $row0['DEAL_ID'];
           $aTrade['user_id'] = $rowu0['id'];
            
            // cal comm
            $get_ref = get_ref($rowu0['email'],1,$aTrade);

    echo "<hr />";
    
    
    
}
mysqli_free_result($res0);   


    }

//}
    
?>