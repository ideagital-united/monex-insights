<?php

function print_riz($array,$label="")
{
    echo "<pre>".$label;
    
    print_r($array);
    
    echo "</pre>";
}

function buildata_post($fields)
    {
        $postvars = '';
        foreach($fields as $key => $value) {
            $postvars .= $key . "=" . $value . "&";
        }
        return $postvars;
    }
    
function call_curl($fn='',$itype="POST",$aPost = array())
    {
    
        $api_key = API_KEYTOKEN;
        
        $string = buildata_post($aPost);
         $ch = curl_init();
      
        curl_setopt($ch, CURLOPT_URL,  "http://qr-easy.net/broker_api_v2/trunk/v1/".$fn);
        
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $itype);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
        $r = curl_exec($ch);
        curl_close($ch);
        $obj = json_decode($r,true);
        return $obj;
    }
    
    
function call_curl_get($fn='',$itype="GET",$var='') 
{
       
        if (isset($_SESSION['admin_secure']['admin_api_key'])) {
            $api_key = $_SESSION['admin_secure']['admin_api_key'];
        } else {
            $api_key = $_SESSION['securekey'];
        }
        if ($var != '') {
            $var = "/".$var;
        }
        $url        ="http://qr-easy.net/broker_api_v2/trunk/v1/".$fn.$var;

        // Open connection
        $ch = curl_init();
        
        // Set the url, number of GET vars, GET data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        //curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.$api_key.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
       
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        // Execute request
        $result = curl_exec($ch);
        
        // Close connection
        curl_close($ch);
        
        return json_decode($result, true);
    }

function get_max_timestamp()
{
    global $con;
    $sql = "select max(close_time) as mclose from acms_closed_ordermt4";
    $res = mysqli_query($con,$sql);
    $row = mysqli_fetch_array($res,MYSQL_ASSOC);
    mysqli_free_result($res);   
    if (isset($row['mclose'])) {
        return $row['mclose'];
    } else {
        return 0;
    }
}

function getdata($selectdata = "*", $table = "" , $where="")
{
    global $con;
    $sql = "select " . $selectdata . " from " . $table . " " . $where;
    $res = mysqli_query($con,$sql);
    $row = mysqli_fetch_array($res,MYSQL_ASSOC);
    mysqli_free_result($res);   
    if ($row) {
        return $row;
    } else {
        return 0;
    }
}

function getorderbyaccount($aParam)
    {
            
        include_once("../_class/thinkmt4.php");
        $api = new SoapApi;
        $aParam['accounts'] = $aParam['accounts'] *1;
        $aParam['start']=$aParam['start'] *1;
        $aParam['end']= $aParam['end'] *1;
        $r = $api->get_orders_by_date($aParam['accounts'],$aParam['start'],$aParam['end']);
        //echo "<pre>";print_r($r);
        /*
        $a = json_encode($aParam);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, URL_API_MT4 . "/orders?format=json");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $a);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'charset: utf-8',
                'Content-Length: '.strlen($a)
            )
        );
        $r = curl_exec($ch);
        curl_close($ch);
        $r = json_decode($r,true);
        */
        return $r;
    }
function i_insert($table = null, $data)
      {
        global $con;
          if ($table === null or empty($data) or !is_array($data)) {
         
          return false;
          }
          $q = "INSERT INTO `" . $table . "` ";
          $v = '';
          $k = '';
          
          foreach ($data as $key => $val) :
              $k .= "`$key`, ";
              if (strtolower($val) == 'null')
                  $v .= "NULL, ";
              elseif (strtolower($val) == 'now()')
                  $v .= "NOW(), ";
              else
                  $v .= "'" . escape($val) . "', ";
          endforeach;
          
          $q .= "(" . rtrim($k, ', ') . ") VALUES (" . rtrim($v, ', ') . ");";
          
          echo "<br />q=".$q;
          
          if (mysqli_query($con,$q)) {
              return true;
          } else
              return false;
      }
function i_update($table = null, $data, $where = '1')
      {
          global $con;
          if ($table === null or empty($data) or !is_array($data)) {
          return false;
          }
          
          $q = "UPDATE `" . $table . "` SET ";
          foreach ($data as $key => $val) :
              if (strtolower($val) == 'null')
                  $q .= "`$key` = NULL, ";
              elseif (strtolower($val) == 'now()')
                  $q .= "`$key` = NOW(), ";
              elseif (strtolower($val) == 'default()')
                  $q .= "`$key` = DEFAULT($val), ";
              elseif(preg_match("/^inc\((\-?[\d\.]+)\)$/i",$val,$m))
                  $q.= "`$key` = `$key` + $m[1], ";
              else
                  $q .= "`$key`='" . escape($val) . "', ";
          endforeach;
          $q = rtrim($q, ', ') . ' WHERE ' . $where . ';';
          //echo '<br>sql update ===>'.$q;
          return mysqli_query($con,$q);
      }
     function escape($string)
      {
          if (is_array($string)) {
              foreach ($string as $key => $value) :
                  $string[$key] = escape_($value);
              endforeach;
          } else 
              $string = escape_($string);
          
          return $string;
      }
      

       function escape_($string)
      {
        global $con;
          return mysqli_real_escape_string($con, $string);
      }
 
 ?>