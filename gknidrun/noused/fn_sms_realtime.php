<?php
define('ACC_KBANK', '7092527381');//7972078535
function validate_kbank($aParam)
{
    $res            = array();
    $res['result']  = true;
    $phone          = $aParam['smscenter'];
    $pattern        = '/^\+66/';
    $aBank = array('KBank','Krungsri','TMBBank');
    if (in_array($aParam['phone'],$aBank)) {
        if (preg_match($pattern, $phone, $matches)) {
            if (strlen($phone) == 12) {
                
            } else {
                $res['result']  = false;
                $res['msg']     = "number not length 12 digit";
           }
        } else {
            $res['result']  = false;
            $res['msg']     = "sender number not is Thailand code";
        }
    } else {
        $res['result']  = false;
        $res['msg']     = "Sender name is not KBank";
    }
    return $res;
}

function get_type_system_frm_wsfx($uid)
{
    global $con;
    $aRes = array();
    if (!isset($con)) {
        try {
           $con = new PDO('mysql:host='.DB_SERVER.';dbname='.DB_DATABASE, DB_USER, DB_PASS);
           $con->exec("SET CHARACTER SET utf8"); 
        } catch (Exception $e) {
           return false;//"Failed: " . $e->getMessage();
        }
    } else {
         $sql = "SELECT * FROM `acms_trader_acc` where trader_acc_login = '".$uid."' ";
        $sql = $con->prepare($sql);
        $sql->execute(array());
        $aData = $sql->fetch();
        if ($aData) {
            $aRes['web'] = "wsfx";
            $type_trade = chk_type_mt4_ctrade($aData['trader_acc_login']);
            if ($type_trade === false) {
                $aRes['type_trade'] = "-";
            }else{
                $aRes['type_trade'] = $type_trade;
            }
        } else {
            return false;
        }
        return$aRes;
    }       
    
}

function chk_type_mt4_ctrade($acc_login)
{
    if(strlen($acc_login) == 10) {
        return 'mt4';
    }elseif(strlen($acc_login) == 7){
        return 'ctrade';
    }else{
        return false;
    }
}

function format_money($money,$rate_deposit)
{
    $money=str_replace(" ", "", $money);
    $money = str_replace(",", "", $money);
    $money = intval($money);
    if ($money <= 0) {
        return 0;
    } else {
        $money = $money/$rate_deposit;
        $money = number_format($money,2);
        $money = str_replace(",", "", $money);
        return $money;
    }
}

function get_sms($text,$rate_deposit){
    
    if (!defined('ACC_KBANK')) {return false;}
    
    $pattern ='(TMB ATM|ผ่านกรุงศรีATM|ผ่าน K-Cyber|via K-Cyber Banking|via K-ATM|ผ่านK-ATM)';
    preg_match($pattern, $text, $matches);
    //echo '<pre>';print_r($matches);
    if (isset($matches[0])) {
        switch($matches[0]){
             case 'TMB ATM'      : $aRes = get_format_TMB_ATM_EN($text,$rate_deposit);       break;
            case 'ผ่านกรุงศรีATM'       : $aRes = get_format_KRUNGSRI_ATM_TH($text,$rate_deposit);  break;
            case 'ผ่าน K-Cyber' : $aRes = get_format_KBANK_ONLINE_TH($text,$rate_deposit);  break;
            case 'via K-Cyber Banking'  : $aRes = get_format_KBANK_ONLINE_EN($text,$rate_deposit);  break;
            case 'via K-ATM'    : $aRes = get_format_KBANK_ATM_EN($text,$rate_deposit); break;
            case 'ผ่านK-ATM'    : $aRes = get_format_KBANK_ATM_TH($text,$rate_deposit); break;
            default : $aRes=array();break;
        }
        return $aRes;
    } else {
        return false;
    }
}

// text = "เงินโอนเข้า KBank7092527381 ผ่านK-ATM 16,750.00บ จาก 1014641";
function get_format_KBANK_ATM_TH($text,$rate_deposit)
{
    $aGet = explode("เงินโอนเข้า KBank". ACC_KBANK ." ผ่านK-ATM", $text);
    if (count($aGet) == 2) {
        $smstext        = trim($aGet[1]);
        $aSMS           = explode("บ จาก", $smstext);
        $aRes['money']  = format_money($aSMS[0],$rate_deposit);
        $aRes['sms_ref_no']= trim($aSMS[1]);
        
        return $aRes;
    }
    return false;
}

// text = "Account KBank 7092527381 received THB11,725.00 via K-ATM from 1001502";
function get_format_KBANK_ATM_EN($text,$rate_deposit)
{
    $aGet = explode("Account KBank ". ACC_KBANK ." received THB", $text);
    if (count($aGet) == 2) {
        $smstext        = trim($aGet[1]);
        $aSMS           = explode("via K-ATM from ", $smstext);
        $aRes['money']  = format_money($aSMS[0],$rate_deposit);
        $aRes['sms_ref_no']= trim($aSMS[1]);
        
        return $aRes;
    }
    return false;
}

// text = "Account KBank 7092527381 received THB34.00 via K-Cyber Banking from 1014641";
function get_format_KBANK_ONLINE_EN($text,$rate_deposit)
{
    $aGet = explode("Account KBank ". ACC_KBANK ." received THB", $text);
    if (count($aGet) == 2) {
        $smstext        = trim($aGet[1]);
        $aSMS           = explode("via K-Cyber Banking from ", $smstext);
        $aRes['money']  = format_money($aSMS[0],$rate_deposit);
        $aRes['sms_ref_no']= trim($aSMS[1]);
        
        return $aRes;
    }
    return false;
}

// text = "เงินโอนเข้าKBank 7092527381 ผ่าน K-Cyber 1000.00บ จาก 1014641";
function get_format_KBANK_ONLINE_TH($text,$rate_deposit)
{
    $aGet = explode("เงินโอนเข้าKBank ". ACC_KBANK ." ผ่าน K-Cyber ", $text);
    if (count($aGet) == 2) {
        $smstext        = trim($aGet[1]);
        $aSMS           = explode("บ จาก ", $smstext);
        $aRes['money']  = format_money($aSMS[0],$rate_deposit);
        $aRes['sms_ref_no']= trim($aSMS[1]);
        
        return $aRes;
    }
    return false;
}

// text = "มีเงินโอนเข้า KBNK xxx252738x ผ่านกรุงศรีATM 3,500.00บ จาก 1013921";
function get_format_KRUNGSRI_ATM_TH($text,$rate_deposit)
{
    $bankACC = substr(ACC_KBANK, 3, -1);
    $bankACC = 'xxx'.$bankACC.'x';
    $aGet = explode("xxx252738x ผ่านกรุงศรีATM ", $text);
    if (count($aGet) == 2) {
        $smstext        = trim($aGet[1]);
        $aSMS           = explode("บ จาก ", $smstext);
        $aRes['money']  = format_money($aSMS[0],$rate_deposit);
        $aRes['sms_ref_no']= trim($aSMS[1]);
        
        return $aRes;
    }
    return false;
}

// text = "TMB ATM....THB1,000.00 transferred to your KBNK A/C no. 7092527381 from 0000 ";
function get_format_TMB_ATM_EN($text,$rate_deposit)
{
    $aGet = explode("transferred to your KBNK A/C no. " . ACC_KBANK . " from ", $text);
    if (count($aGet) == 2) {
        $smstext        = trim($aGet[0]);
        $aSMS           = explode("TMB ATM....THB", $smstext);
        $aRes['money']  = format_money($aSMS[1],$rate_deposit);
        $aRes['sms_ref_no']= trim($aGet[1]);
        
        return $aRes;
    }
    return false;
}


function send_mail_sms_realtime_success($aParam)
{
    $debug = false;
    $aDtlSms = isset($aParam['aSms']) ? $aParam['aSms'] : array();
    $tradeID = isset($aParam['trade_login']) ? $aParam['trade_login'] : 0;
    $type = isset($aParam['itype']) ? $aParam['itype'] : 0;
    $money_thai = (isset($aParam['rate_deposit']) && $aParam['money']) ? ($aParam['rate_deposit']*$aParam['money']) : 0;
    
    
    
    $header2 = "From: admin@brokers.solutions\r\n";
    $header2 .= "Content-type: text/html; charset=utf-8\r\n";
    if ($debug == true) {
        $subject2 = "มีรายการฝากเงินจากสมาชิก ".$opt['mt4id']." brokers.solutions";
    } elseif(isset($aParam['type_err']) && $aParam['type_err']!='') {               $mm = "[".$aDtlSms['money_before'] ."+".$aDtlSms['money_inn']."=".$aDtlSms['money_after']."]";
        $subject2 = "SMS REALTIME [TRADE: " . $tradeID . "]".$mm;
    } else {
        $subject2 = "มีรายการผากเงิน SMS REALTIME COMPLETE [TRADE: " . $tradeID . "]";
    }
    if (isset($aParam['toweb'])) {
        $subject2 .=" FRM : ".$aParam['toweb'] ;
    }
    
    
    $body2    = "
            <html>
            <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            <title>Registing Confirmation</title>
            </head>
            <body>
            <p>เรียนผู้ดูแลการเงิน,

            <p>มีรายการฝากเงินจากสมาชิก ตามรายละเอียดดังนี้
            
            <br />cTrader : ".$tradeID."
            <br />ประเภท : " . $type . "
            <br />จำนวนเงิน : " . number_format($money_thai,2) . " บาท
            <br />จำนวนเงินเข้า : " . $aParam['money'] . " USD
            <br />วันเวลาที่เข้าโดย process : " . $aDtlSms['sms_time'] . "
            <br />note : " . $aDtlSms['note'] . "

            
    
            <br />
            <br />ทำรายการเมื่อ : ".$aDtlSms['deposit_datetime']."
            <br />
            
            
            brokers.solutions Team                  
            
            </body>
            </html>";
            $aRes = array();
            $aRes['complete'] = array();
            $aRes['error'] = array();
            if (isset($aParam['mailadmin'])){
                if (mail($aParam['mailadmin'], $subject2, $body2, $header2)) {
                    array_push($aRes['complete'],$v);
                }else{
                    array_push($aRes['error'],$v);
                }
            } else {
                $aMail = array('naraaknaraak@gmail.com','izaraus@hotmail.com');
                foreach($aMail as $k => $v){
                    
                    if (mail($v, $subject2, $body2, $header2)) {
                        array_push($aRes['complete'],$v);
                    }else{
                        array_push($aRes['error'],$v);
                    }
                }
            }
            return $aRes;
}
?>