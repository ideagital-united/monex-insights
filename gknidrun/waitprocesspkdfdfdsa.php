<meta charset="UTF-8">
<?php
$BASEPATH 	= realpath(dirname(__FILE__) . '/../lib');
define('_VALID_PHP', true);
define('istest', true);
date_default_timezone_set('UTC');

if (isset($_SERVERxxxxx['REMOTE_ADDR'])) {
	// แสดงว่าเรียกตรงๆ ไม่ใช cron
	//echo "NOOOOOOOOOOOOOOOOOOOO";
} else {
	global $con;
	
	date_default_timezone_set('UTC'); 
	include ($BASEPATH.'/tofixconfigbysite.ini.php');
	include $BASEPATH.'/../gknidrun/fn_connect_pdo.php';
	$db = new DbHandler();
	
	
	function balance_trade_wallet($email,$login,$money,$tid,$comment){
		
    	$aBAlnace = curl_post("admin_internal_trade_wallet/".$email,"POST",array(
							'cmd'       => 'broke_balance_correction',
							'login' =>$login,
							'amount'	=>	$money,
							'corr_code' => 	6,
							'tid'		=>	$tid,
							'comment' 	=> 	$comment
							)
						);
		return $aBAlnace;
    }
	function balance_wallet_trade($email,$login,$money,$tid,$comment){
    	$aBAlnace = curl_post("admin_internal_wallet_trade/".$email,"POST",array(
							'cmd'       => 'broke_balance_correction',
							'login' =>$login,
							'amount'	=>	$money,
							'corr_code' => 	6,
							'tid'		=>	$tid,
							'comment' 	=> 	$comment
							)
						);
		return $aBAlnace;
    }
	function balance_wallet_admin($email,$login,$money,$tid,$comment){
    	$aBAlnace = curl_post("admin_wallet/".$email,"POST",array(
							'cmd'       => 'wallet',
							'login' =>$login,
							'amount'	=>	$money,
							'corr_code' => 	6,
							'tid'		=>	$tid,
							'comment' 	=> 	$comment
							)
						);
		return $aBAlnace;
    }
    
    
   // echo "<br />api_key after admin_login=".$apikey;
	/*
	 u;secret
	 p: qwerty258987
	 * 
	 * */
	if (debugtime == 1 ) $msc0 = microtime(true);
	//echo "<br> === 1. START PROCESS NOT WALLET";
	$sqlt = "
	SELECT
		b.gateway_dtl,b.money,b.bank_code,b.deposit_comment,b.trans_id,b.gateway_note,b.trans_type,
		u.email
	FROM
		bk_wallet_transaction b,users u
	WHERE
		b.user_id = u.id
		AND b.status = 0
		AND b.gateway_dtl = 'internal_transfer'
		AND b.bank_code = 'ACCOUNT_TRADE'
	ORDER BY
		b.trans_id 
	LIMIT 0,10
	";
	
	$sth 	=  $db->conn->prepare($sqlt);
	$sth->execute();
	$sqldata 	= $sth->fetchAll(PDO::FETCH_ASSOC);
	$sth = null;
	$dateupdate = date("Y-m-d H:i:s");
	foreach ($sqldata as $k => $data) 
	{
		$dataamount = $data['money']*1;
		/*echo "<pre>";print_r($data);*/
		if ($dataamount > 0) {
       		//=== TRADE TO WALLET
       		$aRes = balance_trade_wallet($data['email'],$data['trans_type'],$dataamount,$data['trans_id'],$data['gateway_note']);
       	} elseif ($dataamount < 0) {
       		//=== WALLET TO TRADE
       		$dataamount = $dataamount*-1;
       		$aRes 	= balance_wallet_trade($data['email'],$data['trans_type'],$dataamount,$data['trans_id'],$data['gateway_note']);
       	}
       /*	echo "<pre>";print_r($aRes);*/
		# 2. result API
	   	if ($aRes['isSuccess']==1) {
			$isOk 			= true;
			$current_amount = $aRes['message']['current_amount']*1;
			$add_comment = '';
		} else {
			$isOk 			= false;
			$current_amount = $dataamount*1;
			$add_comment = isset($aRes['errMessage']) ? '['.$aRes['errMessage'].']' : '';
		}
       	
		#3. update db portal
		if ($isOk == true) {
			$statusupdate 	= 1;
		} else {
			$statusupdate 	= 2;
		}
		$sqlupdate = "
		UPDATE 
			bk_wallet_transaction 
		SET
			money_total=:moneytotal, status=:status, comment=:comment, deposit_comment = :desitcomment, update_date=:dt_update
		WHERE
			trans_id=:tid
		";
		$comment 				= isset($aRes) 	? json_encode($aRes) : '';
		$deposit_comment_new 	= $data['deposit_comment'] . $add_comment;
		
		$qup = $db->conn->prepare($sqlupdate); 
		$qup->bindValue(':moneytotal', $current_amount);
		$qup->bindValue(':status', $statusupdate);
		$qup->bindValue(':comment', $comment);
		$qup->bindValue(':desitcomment', $deposit_comment_new);
		$qup->bindValue(':dt_update', $dateupdate);
		$qup->bindValue(':tid', $data['trans_id']);
		$qup->execute();
		$qup = null;
		//=== start update acc ==
		$qup2 = $db->conn->prepare("UPDATE bk_trader_acc set is_reload=1 where trader_acc_login=:login"); 
		$qup2->bindValue(':login', $data['trans_type']);
		$qup2->execute();
		$qup2 = null;
	}

     //=== 2. SMS REALTime btC
     $sqlt = "
	SELECT
		b.gateway_dtl,b.money,b.bank_code,b.deposit_comment,b.trans_id,b.gateway_note,b.trans_type,
		u.email
	FROM
		bk_wallet_transaction b,users u
	WHERE
		b.user_id = u.id
		AND b.status = 11
		AND b.trans_type = 'wallet'
		AND b.gateway_dtl = 'Localbank Auto'
	ORDER BY
		b.trans_id 
	LIMIT 0,10
	";
	
	$sth 	=  $db->conn->prepare($sqlt);
	$sth->execute();
	$sqldata 	= $sth->fetchAll(PDO::FETCH_ASSOC);
	$sth = null;
	$dateupdate = date("Y-m-d H:i:s");
	foreach ($sqldata as $k => $data) 
	{
		$dataamount = $data['money']*1;
		/*echo "<pre>";print_r($data);*/
		if ($dataamount > 0) {
       		//=== TRADE TO WALLET
       		$aRes = balance_wallet_admin($data['email'],$data['trans_type'],$dataamount,$data['trans_id'],$data['gateway_note']);
       	}
     
		# 2. result API
	   	if ($aRes['isSuccess']==1) {
			$isOk 			= true;
			$current_amount = $aRes['message']['current_amount']*1;
			$add_comment = '';
		} else {
			$isOk 			= false;
			$current_amount = $dataamount*1;
			$add_comment = isset($aRes['errMessage']) ? '['.$aRes['errMessage'].']' : '';
		}
       	
		#3. update db portal
		if ($isOk == true) {
			$statusupdate 	= 1;
		} else {
			$statusupdate 	= 2;
		}
		$sqlupdate = "
		UPDATE 
			bk_wallet_transaction 
		SET
			money_total=:moneytotal, status=:status, comment=:comment, deposit_comment = :desitcomment, update_date=:dt_update
		WHERE
			trans_id=:tid
		";
		$comment 				= isset($aRes) 	? json_encode($aRes) : '';
		$deposit_comment_new 	= $data['deposit_comment'] . $add_comment;
		
		$qup = $db->conn->prepare($sqlupdate); 
		$qup->bindValue(':moneytotal', $current_amount);
		$qup->bindValue(':status', $statusupdate);
		$qup->bindValue(':comment', $comment);
		$qup->bindValue(':desitcomment', $deposit_comment_new);
		$qup->bindValue(':dt_update', $dateupdate);
		$qup->bindValue(':tid', $data['trans_id']);
		$qup->execute();
		$qup = null;
		//=== start update acc ==
		$qup2 = $db->conn->prepare("UPDATE bk_trader_acc set is_reload=1 where trader_acc_login=:login"); 
		$qup2->bindValue(':login', $data['trans_type']);
		$qup2->execute();
		$qup2 = null;
	}
}
?>
