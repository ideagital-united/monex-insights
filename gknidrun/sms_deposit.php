<?php
$BASEPATH = realpath(dirname(__FILE__) . '/../lib');
define('BASEPATHCRON',$BASEPATH."/../");
define('BASEPATH',$BASEPATH."/../");
define('_VALID_PHP', true);
define('istest', true);


include ($BASEPATH.'/tofixconfigbysite.ini.php');
$dbhost = DB_SERVER;
$dbname = DB_DATABASE;
$dbuser = DB_USER;
$dbpass = DB_PASS;

global $con;
$con = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if (!$con) {
    die("Conectare imposibila: " . mysqli_error($con));
}
mysqli_query($con, "SET NAMES UTF8");

include "fn_mysqli_con_new.php";
include "fn_sms_realtime.php";



//=== start define config admin
// ref file : tradersystem/define_modules
// $aAdminConfig = getdata("*","acms_admin_config","where id!=''");
define('URL_API_CT', (isset($aAdminConfig['ctrade_api_url'])?$aAdminConfig['ctrade_api_url']:''));
define('URL_API_MT4', (isset($aAdminConfig['mt4_api_url'])?$aAdminConfig['mt4_api_url']:''));

//=== end defind config admin


//define('ACC_KBANK', '7092527381');//7972078535
//define('ACC_KBANK', '7972078535');
define('ACC_KBANK', '0078296208');
?>
<meta charset="utf-8">
<?php





$sql = "
    SELECT
        sms_id,
        phone,
        smscenter,
        smstext,
        deposit_status,
        deposit_datetime,
        sms_create_time
    FROM
        bk_sms_log 
    WHERE
        deposit_status='PENDING'
    ";
    
echo $sql;
    
    
$sql = mysqli_query($con,$sql);
while($data = mysqli_fetch_array($sql, MYSQL_ASSOC)){
    
    $valid = validate_kbank($data);
	
	print_r($valid);
    
    if (isset($valid['result']) && (($valid['result'] === true) || ($valid['result'] == 1))) {
                    
        $res = save_to_transaction($data);
		
		 echo "<pre>res=";
         print_r($res);
         echo "</pre>";
		
		if (isset($res['res']) && $res['res'] == true) {
			$valid['status']    = 'COMPLETED';
            $valid['msg']       = isset($res['msg']) ? $res['msg'] : 'insert table wallet_transaction';
		} elseif (isset($res['msg']) && $res['msg']!='' ) {
           $valid['status']    = 'CANCELLED';
           $valid['msg']       = $res['msg'];
        } else {
            $valid['status']    = 'CANCELLED';
            $valid['msg']       = 'NOT FOUND.';
         }
    } else {
        $valid['status'] = 'CANCELLED';
        $valid['msg'] = 'The sender number is not Thailand Bank';
    } 
    
    echo "<pre>valid=";
    print_r($valid);
    echo "</pre>";
	
    $date_now = date("Y-m-d H:i:s");
    $res_update_log = update_sms_log($data['sms_id'],$valid['msg'],$valid['status'],$date_now);

}
mysqli_free_result($sql);


/*************************** end process ************************************/
 


function update_sms_log($id, $note='', $status,$datenow = "")
{
    $fup['deposit_status']  = $status;
    $fup['note']            = $note;
    $fup['deposit_datetime']= $datenow;

    $res_up                 = i_update("bk_sms_log",$fup, "sms_id=".$id,false);

}

function get_rate_deposit_new ($cur) {
    $data_rate = getdata("deposit,id,currency", "bk_exchn_rate_new", "where currency='THB' ");
    if ($data_rate) {
        return ($data_rate['deposit']*1);
    } else {
        return 36;
    }
}

function check_currency ($text) {
    $pattern ='(TMB ATM|ผ่านกรุงศรีATM|ผ่าน K-Cyber|via K-Cyber Banking|via K-ATM|ผ่านK-ATM)';
        preg_match($pattern, $text, $matches);
        //echo '<pre>';print_r($matches);
        if (isset($matches[0])) {
            switch($matches[0]){
                  case 'TMB ATM'                  : $cur = "THB";      break;
                case 'ผ่านกรุงศรีATM'           : $cur = "THB";      break;
                case 'ผ่าน K-Cyber'             : $cur = "THB";      break;
                case 'via K-Cyber Banking'      : $cur = "THB";      break;
                case 'via K-ATM'                : $cur = "THB";      break;
                case 'ผ่านK-ATM'                : $cur = "THB";      break;
                default : $cur=''; break;
            }
            return $cur;
        } else {
            return false;
        }
}
 
 // ตรวจดูเวลาเดียวกันด้วย
/**
 * 
Param
(
    [sms_id] => 4
    [phone] => KBank
    [smscenter] => +66938011000
    [smstext] => เงินโอนเข้าKBank 7972078535 ผ่าน K-Cyber 12.00บ จาก 14
    [deposit_status] => PENDING
    [deposit_datetime] => 2015-12-11 04:27:00
    [sms_create_time] => 2015-12-11 04:35:15
)
 */
function save_to_transaction($aParam)
{
	$cur 					= check_currency($aParam['smstext']);
    $rate_dep 				= get_rate_deposit_new($cur);
	if ($rate_dep <= 0) {
		return array('res'=>false,'msg'=>'step 1: rate deposit='.$rate_dep);
	} else {
		 $aDtl 					= get_sms($aParam['smstext'],$rate_dep);
		 echo "<pre>aDtl=";print_r($aDtl);
		 if (isset($aDtl['money']) && $aDtl['money'] != '' && isset($aDtl['sms_ref_no']) && $aDtl['sms_ref_no'] != '') {
		 	 $res_update_log =		 update_sms_log($aParam['sms_id'],"","PROCESSING",date("Y-m-d H:i:s"));
			 $aPost['sms_ref_no'] 	= $aDtl['sms_ref_no'];
			 $aPost['cmd'] 			= 'sms';
			 $aResult  = call_curl("sms_broker_referent","POST",$aPost);
			 echo "<pre>aResult=";print_r($aResult);
			 if (isset($aResult['isSuccess']) && $aResult['isSuccess'] == 1) {
			 	if (isset($aResult['ref_type']) && $aResult['ref_type'] == 'account') {
			 		$aData 					= getdata("id,email,fname,lname","users","where email='".$aResult['ref_data_owner']."'",false);
					$finsert['trans_type'] 	= $aResult['ref_data'];
			 	} elseif(isset($aResult['ref_type']) && $aResult['ref_type'] == 'user'){
			 		$aData 					= getdata("id,email,fname,lname","users","where email='".$aResult['ref_data']."'",false);
					$finsert['trans_type'] 	= 'wallet';
			 	}
				$aData['id'] = $aData['id']*1;
				if (isset($aData['id']) && $aData['id'] > 0) {
					$finsert['user_id'] 		= $aData['id'];
					$finsert['money'] 			= $aDtl['money']*1;
					$finsert['money_local'] 	= $aDtl['money_local'];
					$finsert['rate_exchange'] 	= $rate_dep;
					$finsert['curency'] 		= 'THB';
					$finsert['status'] 			= 11;
					$finsert['gateway_dtl'] 	= 'Localbank Auto';
					$finsert['bank_code'] 		= $aParam['smstext'];
					$finsert['trans_date'] 		= $aParam['sms_create_time'];
					$finsert['comment'] 		= json_encode($aResult);
					$finsert['deposit_comment'] = 'deposit realtime to:'.$finsert['trans_type'];
					$aaaa = i_insert("bk_wallet_transaction", $finsert,false);
					
					$aSendmail 				= $finsert;
					$aSendmail['tomail'] 	= getmailfinancebyHost();
					$aSendmail['toname'] 	= 'Finance System';
					$aSendmail['namecustomer'] =  $aData['fname']." " .$aData['lname']; 
					if ($aaaa) {
						$aResult['resultsave'] = 'save complete';
						$sendmail = send_mail('depositauto',31,$aSendmail);
						return array('res'=>true,'msg'=>json_encode($aResult));
		
					} else {
						$sendmail = send_mail('depositauto',32,$aSendmail);
						
						return array('res'=>false,'msg'=>"no insert to db transaction");
					}
				} else {
					$aSendmail['resultapi'] = json_encode($aResult);
					$aSendmail['smstext'] = json_encode($aDtl);
					$sendmail = send_mail('depositauto_problem',33,$aSendmail);
				}
			 } else {
			 	 $aSendmail['resultapi'] = json_encode($aResult);
				 $aSendmail['smstext'] = json_encode($aDtl);
			 	 $sendmail = send_mail('depositauto_problem',33,$aSendmail);
			 	 return array('res'=>false,'msg'=>json_encode($aResult)); 
			 }
			
		}
		return array('res'=>false,'msg'=>json_encode($aDtl)); 
	}

}

/**
 * array('trade_login'=>$user_id,'money'=>$money_in,'sms_id'=>$aParam['sms_id'],'itype'=>$dep_type)
 */
function update_smslog_and_sendmail($aParam)
{
    if (isset($aParam['sms_id']) && $aParam['sms_id'] !='') {
        $fup['trade_login'] = isset($aParam['trade_login']) ? $aParam['trade_login']:'';
        $fup['money'] = isset($aParam['money']) ? $aParam['money']:'';
        $fup['itype'] = isset($aParam['itype']) ? $aParam['itype']:'';
        $res = i_update("acms_sms_log",$fup,"sms_id='".$aParam['sms_id']."'",$debug=false);
    }
}

//2,10,2100022018,MT4
//deposit_trade(2,10,2100022018,'MT4');


?>