<?php

/*****************************************
 * aData : 
 * [id] => 3
    [api_ctrade] => 1
    [ctrade_group] => Wallstreet Forex B Book
    [api_sms_thaibulksms_force] => premium
    [api_sms_thaibulk_sender] => WSFX
    [api_mt4] => 1
    [mt4_api_url] => http://54.255.130.70:61400
    [ctrade_api_url] => https://live-wallstreetcapital.webapi.ctrader.com:8443
    [comission_level] => 5
    [api_sms] => thaibulksms
    [api_sms_textmagic_user] => 
    [api_sms_textmagic_pwd] => 
    [api_sms_thaibulksms_user] => 0955204848
    [api_sms_thaibulksms_pwd] => 221671
 * FOR CHECK LOT MT4 FOR COMMISSION
 * CRATE BY DARAWAN 22/10/2557
 * เก็บ data lot ของ mt4 เอาไว้คิดค่าคอม
 ****************************************/
 
$BASEPATH = realpath(dirname(__FILE__) . '/../lib');
define('_VALID_PHP', true);
define('istest', true);

date_default_timezone_set('UTC'); 
include ($BASEPATH.'/tofixconfigbysite.ini.php');

$dbhost = DB_SERVER;
$dbname = DB_DATABASE;
$dbuser = DB_USER;
$dbpass = DB_PASS;

global $con,$apikey;
$con = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if (!$con) {
    die("Conectare imposibila: " . mysqli_error($con));
}
mysqli_query($con, "SET NAMES UTF8");

include "fn_mysqli_con_new.php";

if ($_GET['email'] != '')
{
	$condi = " and email = '".$_GET['email']."' ";
}

if (debugtime == 1 ) $mscstartcron = microtime(true); 
	$sqlGet     = "
			SELECT 
				* 
			FROM 
				users 
			WHERE 
				active='y' 
				AND email != ''
				$condi
			ORDER BY 
				timelast ASC
			limit 0,10";
    /*echo "<br />sqlGet=" . $sqlGet;*/
    $sqlGet2    = mysqli_query($con,$sqlGet);
    if (debugtime == 1 ) {
		$msc1	= microtime(true) - $mscstartcron;
		debug_log($sqlGet, "sql", $msc1, "cron");
	}
    /*
     * get trade account from api
     * 
     */   

	$maxtime 	= time() - (90 * 24 * 60 * 60); //  hour * 30 minutes * 60 seconds
	$end 		= time() + (50 * 24 * 60 * 60);
	$listUser 	= array();
	echo "<br />maxtime=".$maxtime;
	echo "<br />end=".$end;
    
    while ($dataUser = mysqli_fetch_array($sqlGet2, MYSQL_ASSOC)) 
	{
		array_push($listUser, $dataUser['id']);
        echo "<br />email=" . $dataUser['email'];
		
		$acc_email 	= $dataUser['email'];
		$achk 		= call_curl("get_trader_list_by_email/" . $acc_email."?cmd=get_trader_list_by_email", "GET");
        /*echo "<pre>"; print_r($achk); echo "</pre>";*/

        $uuu 		= $achk['message'];
        
        for ($cc = 0; $cc<count($uuu); $cc++) 
        {
			echo "<br />trade_login=" . $uuu[$cc]['trader_acc_login']; 
            $acc_login 	= $uuu[$cc]['trader_acc_login'] * 1; 
            $is_demo 	= $uuu[$cc]['is_demo']; 
            $aList 		= call_curl("cron_get_orders_by_date", "POST", array('cmd'=>'cron_get_orders_by_date','start' => $maxtime, 'end' => $end, 'login' => $acc_login));
		
			$aint       = 0;
        	
			if ($aList['isSuccess'] == 1) {
				$aint     = count($aList['msg']);
	            /*echo "<pre>get order from api="; print_r($aList); echo "</pre>";*/
			}
        
        	/*echo "<br />count=".$aint;*/
			if ($aint > 0) {
            	$data = array();
            	for ($a = 0; $a < $aint; $a++) 
            	{
                	$data['is_demo'] 		= $is_demo;
	                $data['order_id']       = $aList['msg'][$a]['order'];
	                $data['login']          = $aList['msg'][$a]['login'];
	                $data['symbol']         = $aList['msg'][$a]['symbol'];
	                $data['digits']         = $aList['msg'][$a]['digits'];
                
                	$cmd = "";
                
				    $typecmd = gettype($aList['msg'][$a]['cmd']);
					if ($typecmd != 'string') {
	  					if ($aList['msg'][$a]['cmd'] == 0) {
							$cmd 		= 'OP_BUY';
	  					} else if ($aList['msg'][$a]['cmd'] == 1) {
							$cmd 		= 'OP_SELL';
	  					} else if ($aList['msg'][$a]['cmd'] == 2) {
							$cmd 		= 'OP_BUY_LIMIT';
	  					} else if ($aList['msg'][$a]['cmd'] == 3) {
							$cmd 		= 'OP_SELL_LIMIT';
	  					} else if ($aList['msg'][$a]['cmd'] == 4) {
							$cmd 		= 'OP_BUY_STOP';
	  					} else if ($aList['msg'][$a]['cmd'] == 5) {
							$cmd 		= 'OP_SELL_STOP';
	  					} else if ($aList['msg'][$a]['cmd'] == 6) {
							$cmd 		= 'OP_BALANCE';
	  					} else if ($aList['msg'][$a]['cmd'] == 7) {
							$cmd 		= 'OP_CREDIT';
	  					} else {
	         				$cmd 		= $aList['msg'][$a]['cmd'];
	  					} 
					} else {
						$cmd 		= $aList['msg'][$a]['cmd'];
					}
					
                
               	   $data['cmd']    = $cmd;
	                $data['volume']         = $aList['msg'][$a]['volume'];
	                $data['open_time']      = $aList['msg'][$a]['open_time'];
	                $data['state']          = $aList['msg'][$a]['state'];
	                $data['open_price']     = $aList['msg'][$a]['open_price'];
	                $data['sl']             = $aList['msg'][$a]['sl'];
	                $data['tp']             = $aList['msg'][$a]['tp'];
	                $data['close_time']     = $aList['msg'][$a]['close_time'];
	                $data['value_date']     = $aList['msg'][$a]['value_date'];
	                $data['expiration']     = $aList['msg'][$a]['expiration'];
	                $data['conv_reserv']    = $aList['msg'][$a]['conv_reserv'];
	                $data['conv_rates_0']   = $aList['msg'][$a]['conv_rates']['0'];
	                $data['conv_rates_1']   = $aList['msg'][$a]['conv_rates']['1'];
	                $data['commission']     = $aList['msg'][$a]['commission'];
	                $data['commission_agent'] = $aList['msg'][$a]['commission_agent'];
	                $data['storage']        = $aList['msg'][$a]['storage'];
	                $data['close_price']    = $aList['msg'][$a]['close_price'];
	                $data['profit']         = $aList['msg'][$a]['profit'];
	                $data['taxes']          = $aList['msg'][$a]['taxes'];
	                $data['magic']          = $aList['msg'][$a]['magic'];
	                $data['comment']        = $aList['msg'][$a]['comment'];
	                $data['activation']     = $aList['msg'][$a]['activation'];
	                $data['spread']         = $aList['msg'][$a]['spread'];
	                $data['margin_rate']    = $aList['msg'][$a]['margin_rate'];
	                $data['timestamp']      = time();/*$aList['msg'][$a]['timestamp'];*/
	                
	          		$data['equity']    		= $aList['EQUITY'];
	                $data['balance']      	= $aList['BALANCE'];
               
               
                	if ($data['order_id'] != '') 
                	{
                		if (debugtime == 1 ) $mscstartinn = microtime(true);
						$dataAcc3 				= getdata("order_id,cmd","bk_closed_ordermt4","WHERE order_id=" . $data['order_id']);
                 		$res_update_trade_acc 	= i_update("bk_trader_acc",array('equlity'=>$data['equity'],'balance'=>$data['balance']),"trader_acc_login='".$data['login']."'");
               			if ($dataAcc3['order_id'] == $data['order_id']) 
               			{
                      		echo "<br /><font color='#FF0000'>".$data['order_id']." already record.</font>";
                       		if ($dataAcc3['cmd'] != $data['cmd']) {
								$sqlup = "update bk_closed_ordermt4 set cmd = '".$data['cmd']."' ,equity = '".$data['equity']."', balance = '".$data['balance']."' where order_id = ".$data['order_id'];
	                         	if (mysqli_query($con,$sqlup)) {
									echo "<br /><font color='#00FF00'>Update cmd =".$sqlup."</font>";
								}
                        	}
                 		} else {
							if ($data['close_time'] > 0) 
							{
                           		echo "<pre>insert data to db="; print_r($data); echo "</pre>";
                         		$res_insert = i_insert("bk_closed_ordermt4", $data);
								if ($res_insert) {
                            		echo "<br /><font color='#00FF00'>Insert record ".$data['order_id']."</font>";
                        		} else {
                        		    echo "<br /><font color='#FF0000'>No Insert record ".$data['order_id']."</font>";
                        		}
                      		}
                 		}
						//=== start cron report
						$depoist = $withdraw = 0;
						if ($data['cmd'] == 'OP_BALANCE') {
							if ($data['profit'] > 0) {
								$depoist = $data['profit'];
							} elseif ($data['profit'] < 0) {
								$withdraw = $data['profit'];
							}
						}
						$timeclose = ($data['close_time'] > 0 ) ? $data['close_time'] : $data['open_time'];

						$aReport = array(
							'order_id' 		=> $data['order_id'],
							'close_time' 	=> $timeclose,
							'login' 		=> $data['login'],
							'equity' 		=> $data['equity'],
							'balance' 		=> $data['balance'],
							'deposit' 		=> $depoist,
							'withdraw' 		=> $withdraw
						);
						
						echo "<br>***** START SAVE HISTORY *****<br>";
						echo "<br>order_id=".$dataAcc3['order_id'] ." = ".$data['order_id'];
						echo "<br>CMD=".$data['cmd'];
				 		if ($dataAcc3['order_id'] == $data['order_id']) {
							$aReport['deposit'] 	= $depoist;
							$aReport['withdraw'] 	= $withdraw;
							$res_update_history 	= i_update("bk_trade_acc_balance_history",$aReport,"order_id='".$data['order_id'] ."'",true);
							echo "<br> CASE UPDATE";
						} else {
							$res_update_history = i_insert("bk_trade_acc_balance_history",$aReport,true);
							echo "<br> CASE INSERT";
						}
						if ($res_update_history) {
							echo "<br><font color='green'>Report OK =".$data['login']."</font>";
						} else {
							echo "<br><font color='red'>Report ERROR =".$data['login']."</font>";
						}
						echo "<br>***** EMD SAVE HISTORY *****<br>";
						//=== end cron report
						if (debugtime == 1 ) {
							$mscinnn	= microtime(true) - $mscstartinn;
							debug_log('closed_order_mt4_new', "file", $mscinnn, "cron");
						} 
                 
                } else {
                    echo "<br /><font color='#FF0000'>NO ORDER ID</font>";
                }
                
            }
        } 
	}
}

mysqli_free_result($sqlGet2); 
    //echo "listUserID=".$listUserID;
    
echo "<pre>"; print_r($listUser); echo "</pre>";
        
$aa = implode(",", $listUser);
if ($aa !='') {
    echo "<br>sql= trader_acc_login in (".$aa.")"; 
    $resup = i_update("users", array('timelast' => time()), " id in (" . $aa . ")");
}
if (debugtime == 1 ) {
	$msc1	= microtime(true) - $mscstartcron;
	debug_log('closed_order_mt4_new', "file", $msc1, "cron");
} 
echo '<br>TIME ALL='.$msc1;
?>