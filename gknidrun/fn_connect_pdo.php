<?php
/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 * file
 * gknidrun/clean_order_mt4_cals.php
 * gknidrun/22_get_closed_order_mt4_bk.php
 * 
 */
define("API_CMS","http://192.168.160.2/democfh/");
define("APIKEY","75720e409f63ac3857468fb2d1b3d961");
define("TABLE_CLOSE_ORDER_MT4","bk_closed_ordermt4");
define("TABLE_CLOSE_ORDER_MT4_LOG","bk_closed_ordermt4_log");
define("TABLE_MT4_CAL_OK","bk_order_cal_complete");






class DbHandler {

    public $conn;

    function __construct() 
    {
        $servername = DB_SERVER;
		$username = DB_USER;
		$password = DB_PASS;
		$dbname = DB_DATABASE;
        $this->conn =  new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->conn->exec("SET CHARACTER SET utf8");  //  return all sql requests as UTF-8
       $this->conn->exec("SET NAMES utf8");
        $this->conn->exec("SET character_set_results=utf8");
       $this->conn->exec("SET character_set_client=utf8");
        $this->conn->exec("SET character_set_connection=utf8");
		 
    }
	
	
	public function getValues($what, $table, $where)
  	{
      $sql = "SELECT $what FROM $table WHERE $where";
	  $sth =  $this->conn->prepare($sql);
	  $sth->execute();
      $row = $sth->fetch(PDO::FETCH_ASSOC);
	  
      return ($row) ? $row : 0;
  	}
  	public function getmaillist($typelist)
{
	
	$aRes = array();
	$sql = "select * from bk_mail where group_mail like '%".$typelist."%' and status =1";
	$sth = $this->conn->prepare($sql);
	$sth->execute();
    $row = $sth->fetchAll(PDO::FETCH_ASSOC);
	$sth = null;
	
	foreach ($row as $k => $data) 
	{
		$aRes[$data['email']] = $data['email_name'];
	}
	
	if (count($aRes) == 0) {
		$aRes['darawantaorong@gmail.com'] = 'darawan';
	}
	return $aRes;
		
}
  	public function getmailfinancebyHost()
{
	$mailmaster = $_SERVER['SERVER_ADMIN'];
	return str_replace("webmaster", "finance", $mailmaster);
}

public function send_mail($itypemail,$mailid,$aParam=array()) 
{
	require_once (BASEPATH . "lib/class_registry.php");
	require_once (BASEPATHCRON . "lib/class_mailer.php");
	$aMail 		= $this->getValues("*", "email_templates", "id=" . $mailid);
	
	$aSetting = $this->getValues("site_email,site_name,site_url","settings","site_name!=''");
	if ($itypemail == 'depositauto') {
		$aMail_list = $this->getmaillist('smsrealtime');
		$body 		= str_replace(
					array('[NAME]','[NAMECUSTOMER]', '[AMOUNTUSD]','[AMOUNTTHB]','[EXCHANGERATE]','[TRANSDATE]','[SMSTEXT]','[SITE_NAME]','[URL]'), 
					array( $aParam['toname'],$aParam['namecustomer'], $aParam['money'], $aParam['money_local'],$aParam['rate_exchange'],$aParam['trans_date'],$aParam['bank_code'],$aSetting['site_name'],$aSetting['site_url'])
				  , $aMail['body_en']);
	} elseif ($itypemail == 'depositauto_problem') {
		$aMail_list = $this->getmaillist('smstechrealtime');
		$body 		= str_replace(
					array('[DATAAPI]','[DATASMS]', '[AMOUNTUSD]','[SITE_NAME]','[URL]'), 
					array( $aParam['resultapi'],$aParam['smstext'],$aSetting['site_name'],$aSetting['site_url'])
				  , $aMail['body_en']);
	}

	$newbody 	= $this->cleanOutCron($body);
	$mailer 	= Mailer::sendMail();

	$message 	= Swift_Message::newInstance()
					->setSubject($aMail['subject_en'])
					->setTo($aMail_list)
					->setFrom(array($aSetting['site_email']=>$aSetting['site_name']))
					->setBody($newbody, 'text/html');
   
	$mailer->send($message);	
}
public function cleanOutCron($text)
 {
      $text = strtr($text, array(
          '\r\n' => "",
          '\r' => "",
          '\n' => ""));
      $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
      $text = str_replace('<br>', '<br />', $text);
      return stripslashes($text);
 }
	
	
    
}
function buildata_post($fields)
	{
		$postvars = '';
	  	foreach($fields as $key => $value) {
	    	$postvars .= $key . "=" . $value . "&";
	  	}
	  	return $postvars;
	}
	
 function curl_post($fn='',$itype="POST",$aPost = array())
	{
		$string 	= buildata_post($aPost);
		echo $string;
		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL, API_CMS.$fn);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,$itype);
		curl_setopt($ch,CURLOPT_POST, count($aPost));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.APIKEY.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$result = curl_exec($ch);
		curl_close($ch);
		$resultobj = json_decode($result, true);
		
		return $resultobj;
	}
function curl_post_order($fn='',$itype="POST",$aPost = array())
	{
		$string 	= buildata_post($aPost);
		echo $string;
		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL, API_ORDER_MT4.$fn);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,$itype);
		curl_setopt($ch,CURLOPT_POST, count($aPost));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: '.APIKEY_ORDER.'','Content-Type: application/x-www-form-urlencoded','charset: utf-8','Content-Length: '.strlen($string)));
		$result = curl_exec($ch);
		curl_close($ch);
		$resultobj = json_decode($result, true);
		
		return $resultobj;
	}
?>
