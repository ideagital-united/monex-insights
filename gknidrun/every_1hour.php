<meta charset="UTF-8">
<?php

$BASEPATH 	= realpath(dirname(__FILE__) . '/../lib');
define('_VALID_PHP', true);
define('istest', true);
date_default_timezone_set('UTC');

function datetimedaily($timestmp, $deldate = 1) 
{
	$datemain = strtotime($deldate . " day", $timestmp);
	$aDay['start'] 	= date("Y",$datemain)."-".date("m",$datemain)."-".date("d",$datemain)." 00:00:00";
	$aDay['start'] 	= strtotime($aDay['start']);
	$aDay['end'] 	= date("Y",$datemain)."-".date("m",$datemain)."-".date("d",$datemain)." 23:59:59";
	$aDay['end'] 	= strtotime($aDay['end']);
	return $aDay;
}
function datetimeMOunth($timestmp, $deldate = 1) 
{
	$datemain = strtotime($deldate . " month", $timestmp);
	$aDay['start'] 	= date("Y",$datemain)."-".date("m",$datemain)."-01 00:00:00";
	$aDay['start'] 	= strtotime($aDay['start']);
	$datelastmonth 	= cal_days_in_month(CAL_GREGORIAN, date("m",$datemain), date("Y",$datemain)); 
	$aDay['end'] 	= date("Y",$datemain)."-".date("m",$datemain)."-".$datelastmonth." 23:59:59";
	$aDay['end'] 	= strtotime($aDay['end']);
	return $aDay;
}
function format_percent($now,$secon)
	{
		$now = $now*1;
		$secon = $secon*1;
		echo "<br>now=".$now;
		echo "<br>secon=".$secon;
		if ($now == 0 && $secon == 0) {
			return "0.00";
		} 
		if ($now > $secon){
			$cal = (($secon*1)*100)/($now*1);
		}
		echo "<br>clal=".$cal;
		if ($cal < 100) {
			$per = 100-$cal;
			return number_format($per,2)."%";
		} elseif($cal > 100) {
			$per = $cal-100;
			return "-".number_format($per,2)."%";
		}
		
	}
if (isset($_SERVERxxxxx['REMOTE_ADDR'])) {
	// แสดงว่าเรียกตรงๆ ไม่ใช cron
	echo "NOOOOOOOOOOOOOOOOOOOO";
} else {
	global $con,$apikey;
	include ($BASEPATH.'/tofixconfigbysite.ini.php');
	$con = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
	if (!$con) {
		die("Conectare imposibila: " . mysqli_error($con));
	}
	mysqli_query($con, "SET NAMES UTF8");
	
	include "fn_mysqli_con_new.php";

	

	$aDay 		= datetimedaily(time(),-1);
	$aData0 	= getdata("max(close_time)as dailynow ","bk_trade_acc_balance_history","where close_time < ".$aDay['end'] ,false);
	
	echo "<br>1.START List Cron Report Daily";
	$aDayst 		= datetimedaily($aData0['dailynow'],0);
	$aData1 		= getdata("max(close_time)as dailynow,sum(deposit) as deposit ","bk_trade_acc_balance_history","where close_time >=".$aDayst['start'] ." and close_time <=".$aDayst['end'] ,false);
	if (isset($aData1['dailynow'])) {
		$aData2 		= getdata("max(close_time)as dailynow ","bk_trade_acc_balance_history","where close_time < ".$aDayst['start']  ,false);
		$dailylast 		= $aData1['deposit'];
		if (isset($aData2['dailynow'])) {
			$aDay2 		= datetimedaily($aData2['dailynow'],0);
			$aData3 	= getdata("max(close_time)as dailynow,sum(deposit) as deposit ","bk_trade_acc_balance_history","where close_time >=".$aDay2['start'] ." and close_time <=".$aDay2['end'] ,false);
			$dailyold 	= $aData3['deposit'];
		}
		$aa 		= format_percent($dailylast,$dailyold);
		$aDayUpdate = i_update("bk_custom_detail",array('valuesis'=>$aa),"fieldname='percent_trade_daily'");
	}
	
	echo "<br>2.START List Cron Report Monthly";
	$aMounth 		= datetimeMOunth(time(),-1);
	$aData0 		= getdata("max(close_time)as dailynow ","bk_trade_acc_balance_history","where close_time < ".$aMounth['end'] ,false);
	$aDayst 		= datetimeMOunth($aData0['dailynow'],0);
	$aData1 		= getdata("max(close_time)as dailynow,sum(deposit) as deposit ","bk_trade_acc_balance_history","where close_time >=".$aDayst['start'] ." and close_time <=".$aDayst['end'] ,false);
	if (isset($aData1['dailynow'])) {
		$aData2 		= getdata("max(close_time)as dailynow ","bk_trade_acc_balance_history","where close_time < ".$aDayst['start']  ,false);
		$dailylast 		= $aData1['deposit'];
		if (isset($aData2['dailynow'])) {
			$aDay2 		= datetimeMOunth($aData2['dailynow'],0);
			$aData3 	= getdata("max(close_time)as dailynow,sum(deposit) as deposit ","bk_trade_acc_balance_history","where close_time >=".$aDay2['start'] ." and close_time <=".$aDay2['end'] ,false);
			$dailyold 	= $aData3['deposit'];
		}
		$aa = format_percent($dailylast,$dailyold);
		$aDayUpdate = i_update("bk_custom_detail",array('valuesis'=>$aa),"fieldname='percent_trade_month'");
	}
	
	echo "<br>3.START List Detail Report";
	echo "<br>3.1. REPOST BOX ONE";
	$tradeDtl 			= getdata("count(order_id) as netorder,sum(deposit) as sumdepo,sum(withdraw) as withdraw","bk_trade_acc_balance_history","where login!=''",false);
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$tradeDtl['sumdepo']),"fieldname='sum_account_trade_deposit'");
	$tradeDtl['withdraw'] = $tradeDtl['withdraw']*-1;
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$tradeDtl['withdraw']),"fieldname='sum_account_trade_withdraw'");
	
	$acctrade 			= getdata("count(id) as totalacc,sum(equlity) as netequlity,sum(balance) as netbalance,sum(net_profit) as netprofit","bk_trader_acc","where trader_product_type=1");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade['netbalance']),"fieldname='sum_mt4_balance'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade['netequlity']),"fieldname='sum_mt4_equlity'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade['netprofit']),"fieldname='sum_mt4_profit'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade['totalacc']),"fieldname='total_mt4_account'");
	
	$acctrade_ct 		= getdata("count(id) as totalacc,sum(equlity) as netequlity,sum(balance) as netbalance,sum(net_profit) as netprofit","bk_trader_acc","where trader_product_type=2");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade_ct['netbalance']),"fieldname='sum_ct_balance'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade_ct['netequlity']),"fieldname='sum_ct_equlity'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade_ct['netprofit']),"fieldname='sum_ct_profit'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$acctrade_ct['totalacc']),"fieldname='total_ct_account'");
	
	$netacctrace 		= $acctrade['totalacc']+$acctrade_ct['totalacc'];
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$netacctrace),"fieldname='total_account_trade'");
	
	echo "<br>3.2. REPOST BOX TWO";
	$aMember 			= getdata("count(id) as memberall,sum(user_wallet)as netwallet","users","where active ='y'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$aMember['memberall']),"fieldname='total_member'");
	$resupdate 			= i_update("bk_custom_detail",array('valuesis'=>$aMember['netwallet']),"fieldname='sum_wallet'");
	
	echo "<br>3.3. REPOST BOX OPENING ORDER";
	$aGEt  				= call_curl("getall_open_order_trade?cmd=get_open_order_trade","GET");
	echo "<pre>";print_r($aGEt);
	$aAccActive 		= array();
	if (isset($aGEt['isSuccess']) && $aGEt['isSuccess'] == 1) {
		$netOrderOpen 	= count($aGEt['msg']);
		$resupdate 		= i_update("bk_custom_detail",array('valuesis'=>$netOrderOpen),"fieldname='total_order_opening'");
		foreach ($aGEt['msg'] as $k => $v) {
			if (isset($aAccActive[$v['login']])) {
				continue;
			} else {
				$aAccActive[$v['login']] = $v['login'];
			}
		}
	}
	$accountactive = count($aAccActive);
	$resupdate 		= i_update("bk_custom_detail",array('valuesis'=>$accountactive),"fieldname='total_account_active'");
	
	echo "<br>3.4. REPOST BOX ORDER ALL";
	$aOrderall 		= getdata("count(order_id) as netorder,sum(volume) as netvolumn","bk_closed_ordermt4","where cmd in ('OP_SELL','OP_BUY')");
	$resupdate 		= i_update("bk_custom_detail",array('valuesis'=>$aOrderall['netorder']),"fieldname='total_order_all'");
	$sumlot 		= ($aOrderall['netvolumn']*1)/100;
	$resupdate 		= i_update("bk_custom_detail",array('valuesis'=>$sumlot),"fieldname='sum_lot'");
	
	echo "<br>3.5. REPOST BOX COMMISSION";
	$acomission 	= getdata("sum(money) as metcomm","bk_wallet_transaction","where gateway_dtl ='Commission'");
	$resupdate 		= i_update("bk_custom_detail",array('valuesis'=>$acomission['metcomm']),"fieldname='sum_commission_paid'");
	
	
	echo "<br>3.6. REPOST BOX SMS";
	$aSMS  			= call_curl("get_about_sms?cmd=otp","GET");
	$smscredit  	= isset($aSMS['creditall']) ? $aSMS['creditall'] : 0;
	$smsused  		= isset($aSMS['sms_used']) ? $aSMS['sms_used'] : 0;
	$resupdate 		= i_update("bk_custom_detail",array('valuesis'=>$smscredit),"fieldname='sms_credit'");
	$resupdate 		= i_update("bk_custom_detail",array('valuesis'=>$smsused),"fieldname='sms_used'");
}


?>
	