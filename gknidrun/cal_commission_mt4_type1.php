<?php
/*****************************************
 * aData : 

 * FOR CHECK LOT MT4 FOR COMMISSION
 * CRATE BY ISARA 12/01/2558
 ****************************************/
 
$BASEPATH = realpath(dirname(__FILE__) . '/../lib');
define('_VALID_PHP', true);
define('istest', true);

include ($BASEPATH.'/tofixconfigbysite.ini.php');
$dbhost = DB_SERVER;
$dbname = DB_DATABASE;
$dbuser = DB_USER;
$dbpass = DB_PASS;

global $con;
$con = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if (!$con) {
    die("Conectare imposibila: " . mysqli_error($con));
}
mysqli_query($con, "SET NAMES UTF8");

include "fn_mysqli_con_new.php";


/* api function */
function get_comm_level_api()
{
    $get_level_api = call_curl("comm_level?cmd=comm" , "GET");
	
	/* -1 เพราะ มี level 0 ขึ้นมาด้วย */ 
    $level 			= count($get_level_api['message']) - 1; 
	//print_riz($level);
    
    if ($level <= 0) {
        return 0;
    } else {
        return $level;
    }
}

function get_comm_level_value_api($level_no) 
{
	$get_level_value_api 	= call_curl("comm_level_value/".$level_no."?cmd=comm" , "GET");
    $usd_per_lot 			= $get_level_value_api['message'][0]['usd_per_lot'];
    
    //print_riz($level);
    
    if ($usd_per_lot <= 0) {
        return 0;
    } else {
        return $usd_per_lot;
    }
}


function get_ref($member_email,$next_level,$aTrade) 
{
    // find ref
    global $con;
    global $comm_level;
	
	if (debugtime == 1 ) { $msc=microtime(true); }
	
	
    $sqlu1 = "
    
    	SELECT 
    		* 
    	FROM 
    		`users` 
		WHERE 
			email = '".$member_email."'
		";
		
	echo "<br />sqlu1=".$sqlu1;
    
    echo "<br /><br />get_ref level = $next_level sqlu1=".$sqlu1;
    
    $resu1 = mysqli_query($con,$sqlu1);
    $rowu1 	= mysqli_fetch_array($resu1,MYSQL_ASSOC);
    mysqli_free_result($resu1);
	
	if (debugtime == 1 ) {
		$msc=microtime(true)-$msc;
		debug_log($sqlu1,"sql",$msc,"cron");
	}
	
	$usd_per_lot = get_comm_level_value_api($next_level);
	
	echo "<br />order id : ".$aTrade['order_id']." commission for ref_id ".$rowu1['cms_ref_id']." (".$rowu1['ref_email'].") is ".$usd_per_lot." usd_per_lot";
        
    $fqa['cal_datetime'] 	= time();
    $fqa['from_user'] 		= $aTrade['from_user'];
    $fqa['trader_acc_login']= $aTrade['trader_acc_login'];
    $fqa['product_name'] 	= $aTrade['product_name'];
    $fqa['lot_trade'] 		= $aTrade['lot_trade'];
    $fqa['usd_per_lot'] 	= $usd_per_lot;
    $fqa['to_user'] 		= $rowu1['ref_email'];
    $fqa['paid_status'] 	= 0;
    $fqa['paid_date'] 		= time();
    $fqa['paid_datetime'] 	= date("Y-m-d H:i:s");
    $fqa['order_id'] 		= $aTrade['order_id'];
    $fqa['from_level_no']  = $next_level;
    
    $comms 					= $fqa['lot_trade'] * $usd_per_lot;
	
	echo "<pre>paid to next level = ".$next_level;
	print_r($fqa);
	echo "</pre>";
    
    echo " commisssion=" . $comms;
    if ($fqa['to_user'] != '') {
    	
        $insert_fqa = i_insert("bk_comm_paid_history" , $fqa);
		
    }
	
	
	
	if ($rowu1['ref_email'] != '') {
		
		$sqlxx = "select * from bk_user_comm_condition where email = '".$rowu1['ref_email']."'";
		$resxx = mysqli_query($con,$sqlxx);
		$rowxx = mysqli_fetch_array($resxx,MYSQL_ASSOC);
		mysqli_free_result($resxx);
			
		if ($rowxx['email'] == '') {
			
			$sqlxx_in = "insert into bk_user_comm_condition (email) values ('".$rowu1['ref_email']."') ";
			mysqli_query($con,$sqlxx_in);
			
		}
			
			
		if ($next_level == 1) {
			// update total downline ที่ติดตัว ให้กับผู้แนะนำ (ชั้นที่ 1)
			$sqluplot = "update bk_user_comm_condition 
							set sum_downline_volume = sum_downline_volume + ".$fqa['lot_trade']."
							where email = '".$rowu1['ref_email']."'  ";
			
			mysqli_query($con,$sqluplot);
		}
		
	}
	
	
	/*
	if($insert_fqa) {
		$money_wallet 	= call_curl("wallet_by_cron/".$aTrade['user_id']);
		$wallet_now 	= $money_wallet['message']['amount'];
		
		$fqz['user_id'] 	= $rowu1['cms_ref_id'];
  		$fqz['money'] 		= $comms;
  		$fqz['money_total'] = $wallet_now + $comms;
  		$fqz['status'] 		= 0;
  		$fqz['trans_date'] 	= date("d-m-Y G:i");
  		$fqz['gateway_dtl'] = 'Commission';
  		$fqz['comment'] 	= 'Trade commission order id '. $fqa['order_id'];
  		$fqz['trans_type'] = 'wallet';
  		i_insert("bk_wallet_transaction" , $fqz);
		
		// update that calculated
		
		$fq['is_calc'] = 1;
		$fq['cal_datetime'] = time();
		i_update("bk_closed_ordermt4", $fq , "order_id=" . $fqa['order_id'] . " ");
                
	} */
	
	// update that calculated
		
	$fq['is_calc'] = 1;
	$fq['cal_datetime'] = time();
	i_update("bk_closed_ordermt4", $fq , "order_id=" . $fqa['order_id'] . " ");
		
		
		
	echo "<br />--------------------<br />";   
	if ($rowu1['cms_ref_id'] != 0) {
		$next_level++;
		if($next_level <= $comm_level) {
			$get_ref = get_ref($rowu1['ref_email'],$next_level,$aTrade);
		}
	}
}
if (debugtime == 1 ) $mscstartcron = microtime(true); 


$cal_comm_status = call_curl("comm_setting?cmd=comm","GET");

//print_riz($cal_comm_status);

$dataConfig['paid_status'] = $cal_comm_status['message'][0]['paid_status'];
// get dataConfig from api

if ($dataConfig['paid_status'] == 'start') {
	
	global $comm_level;
	$comm_level = get_comm_level_api();
	
	
	
	print_riz($comm_level, "comm_level=");

	$sql0 = "
	SELECT 
		* 
	FROM 
		`bk_closed_ordermt4` 
	WHERE 
		profit != 0 
		and (cmd = 'OP_BUY' or cmd = 'OP_SELL')
		and is_calc = 0
		and close_time > 0
		and is_demo = 0
	order by 
		close_time asc";

	echo "<br />sql0=".$sql0;


	$res0 = mysqli_query($con,$sql0);
	while($row0 = mysqli_fetch_array($res0,MYSQL_ASSOC)) {
		
    	$user_email 		= call_curl("email_by_trade_acc_login/".$row0['login']."?cmd=email_by_trade_acc_login", "GET");
    	$trade_login_dtl 	= call_curl("trader_acc_login_dtl/".$row0['login']."?cmd=trader_acc_login_dtl", "GET");
    
   		print_riz($trade_login_dtl,"trade_login_dtl=");
   		$rowu0 = getdata("id,email","users","WHERE email = '".$user_email."'");
    	//=================================== end get ref_id==================//
        $lot 	= $row0['volume']/100;
        echo "<br />row1=".$row0['login']."|close_datetime=".$row0['close_time']."|".$row0['volume']."|lot=".$lot;
        //$sum_lot[$row0['login']] = $sum_lot[$row1['login']] + $row1['volume'];
		$aTrade 					= array();
   		$aTrade['from_user'] 		= $rowu0['email'];
   		$aTrade['trader_acc_login'] = $row0['login'];
   		$aTrade['product_name'] 	= $trade_login_dtl[0]['product_name'];
   		$aTrade['lot_trade'] 		= $row0['volume']/100;
   		$aTrade['order_id'] 		= $row0['order_id'];
   		$aTrade['user_id'] 			= $rowu0['id'];
		
		// ============== manage commission condition ======================
		
		$sqlxx = "select * from bk_user_comm_condition where email = '".$aTrade['from_user']."'";
		$resxx = mysqli_query($con,$sqlxx);
		$rowxx = mysqli_fetch_array($resxx,MYSQL_ASSOC);
		mysqli_free_result($resxx);
		
		if ($rowxx['email'] == '') {
			
			$sqlxx_in = "insert into bk_user_comm_condition (email) values ('".$rowu0['email']."') ";
			mysqli_query($con,$sqlxx_in);
			
		}
			
		$sqlxx_up = "update bk_user_comm_condition set own_volume = own_volume + ".$aTrade['lot_trade'] ." where email = '".$rowu0['email']."'";
		$resxx_up = mysqli_query($con,$sqlxx_up);
			
		// ============== end manage commission condition ==================
			
		
		// ============== start cal commission for own ====================
		
		$level_zero = 0;
	    $comm_at_level_zero = call_curl('comm_level_value/'.$level_zero."?cmd=comm","GET");
		$usd_per_lot_level_zero = $comm_at_level_zero['message'][0]['usd_per_lot'];
		
		echo "<br />usd_per_lot_level_zero=".$usd_per_lot_level_zero;
		echo "<br />volume=".$aTrade['lot_trade'];
		
		
		
		 $fqa['cal_datetime'] 	= time();
	    $fqa['from_user'] 		= $aTrade['from_user'];
	    $fqa['trader_acc_login']= $aTrade['trader_acc_login'];
	    $fqa['product_name'] 	= $aTrade['product_name'];
	    $fqa['lot_trade'] 		= $aTrade['lot_trade'];
	    $fqa['usd_per_lot'] 	= $usd_per_lot_level_zero;
	    $fqa['to_user'] 		= $aTrade['from_user'];
	    $fqa['paid_status'] 	= 0;
	    $fqa['paid_date'] 		= time();
	    $fqa['paid_datetime'] 	= date("Y-m-d H:i:s");
	    $fqa['order_id'] 		= $aTrade['order_id'];
		 $fqa['from_level_no']  = 0;
	    
	    $comms 					= $fqa['lot_trade'] * $usd_per_lot_level_zero;
	    
	    echo " commisssion=" . $comms;
		echo "<pre>";
		print_r($fqa);
		echo "</pre>";
		// exit;
		
	    if ($fqa['to_user'] != '') {
	        $insert_fqa = i_insert("bk_comm_paid_history" , $fqa);
	    }
		
		// exit;
		
		
		
		
		// start cal commission for upline 
		$get_ref = get_ref($rowu0['email'], 1, $aTrade);
		echo "<hr />"; 
	}
	mysqli_free_result($res0);   
}
if (debugtime == 1) {
	$mscstartcron	=	microtime(true)-$mscstartcron;
	debug_log('cal_commission_mt4_type1',"file",$mscstartcron,"cron");
}
//}
    
?>